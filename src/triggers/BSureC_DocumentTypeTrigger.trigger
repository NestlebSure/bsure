trigger BSureC_DocumentTypeTrigger on BSureC_Document_Types__c (before insert, before update) 
{
    Set<ID> DocsID = new set<ID>();
    Set<String> DocumentNames = new Set<String>();
    

    for(BSureC_Document_Types__c objDocType : trigger.new)
    {
        
        DocumentNames.add('%'+objDocType.Document_Type_Name__c+'%');
        DocsID.add(objDocType.Id);
    }
    if(BSureC_Document_Types__c.SObjectType.getDescribe().isQueryable()){
	    List<BSureC_Document_Types__c>  lstDocstypes=[select Id,Document_Type_Name__c from BSureC_Document_Types__c 
	                                                  where Document_Type_Name__c Like : DocumentNames
	                                                  and Id != : DocsID];
	    
	        
	        
	       for(BSureC_Document_Types__c ExistC:lstDocstypes)
	        {
	            for(BSureC_Document_Types__c Doc:Trigger.New){
	                if(ExistC.Document_Type_Name__c == Doc.Document_Type_Name__c )
	                Doc.addError('Record already exists with this name');
	            }
	        }
    }       
                        
}

/*trigger BSureC_DocumentTypeTrigger on BSureC_Document_Types__c (before insert, before update) 
{
    for(BSureC_Document_Types__c objDocType : trigger.new)
    {
        if(trigger.isInsert)
        {
            Integer intCount = [select count() from BSureC_Document_Types__c 
                                where Document_Type_Name__c =: objDocType.Document_Type_Name__c];
            if(intCount > 0)
            {
                objDocType.addError('Record already exists with the same Document Type Name');
            }
            if(trigger.isAfter)
            {
                objDocType.addError('Record Saved Successfully!');
            } 
        }
        if(trigger.isUpdate)
        {
            Integer intCount = [select count() from BSureC_Document_Types__c 
                                where Document_Type_Name__c =: objDocType.Document_Type_Name__c 
                                and Id != : objDocType.Id];
            if(intCount > 0)
            {
                objDocType.addError('Record already exists with the same Document Type Name');
            }
        }
    }
}*/