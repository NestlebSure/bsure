/***********************************************************************************************
*       Trigger Name    : BSureS_ZoneTrigger
*       Date            : 18/11/2012 
*       Author          : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       18/11/2012               Kishorekumar A               Initial Version
        01/04/2013               Veereandranath jella         To avoid soql inside loops 
**************************************************************************************************/
trigger BSureS_CountryTrigger on BSureS_Country__c (before insert,before update) 
{
    
    public boolean isSuppUpdate = false;
    
    /*public integer intCount = 0;
    
    Set<String> CountryNames = new Set<String>();
    Set<ID> CountryID = new Set<ID>();  
    if(trigger.IsBefore)
    {   
        for(BSureS_Country__c Coun:Trigger.New)
        {   
            CountryNames.add('%'+Coun.Name+'%');
            CountryID.add(Coun.Id);
        } 
        
        list<BSureS_Country__c> LstCountries = [SELECT Id,Name 
                                                  FROM   BSureS_Country__c
                                                  WHERE  Name like:CountryNames and id!=:CountryID];                                          
        system.debug('Test'+CountryNames);                                                        
        for(BSureS_Country__c ExistCoun:LstCountries)
        {
            for(BSureS_Country__c Coun:Trigger.New){
                if(ExistCoun.Name == Coun.Name )
                Coun.addError('Record already exists with this name');
            }
        }
        list<BSureS_Basic_Info__c> lstSupplierBasicInfo;
        //BSureS_Basic_Info__c.Supplier_Countries__c
        String strCountryName = '';
        if(intCount < trigger.new.size())
        {
            BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
            if(trigger.new != null && trigger.old != null)
            {
                if(trigger.new[intCount].Name != trigger.old[intCount].Name)
                {
                    strCountryName  = '%'+ trigger.old[intCount].Name +'%';
                    system.debug('strCountryName++++++++' + strCountryName);
                    lstSupplierBasicInfo = new list<BSureS_Basic_Info__c>([select id,Supplier_Countries__c 
                                                                            from BSureS_Basic_Info__c 
                                                                            where Supplier_Countries__c != null 
                                                                            and Supplier_Countries__c like : strCountryName]);
                    for(BSureS_Basic_Info__c sObj:lstSupplierBasicInfo)
                    {
                        sObj.Supplier_Countries__c = sObj.Supplier_Countries__c.replace(trigger.old[intCount].Name, trigger.new[intCount].Name);
                    }
                    update lstSupplierBasicInfo;
                }
            }
        }   
    }
    BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate;*/
    
    
    
    list<string> CountryNames = new list<string>();
    list<string> oldNames = new list<string>();
    list<string> oldCountries = new list<string>();
    map<string,string> mapOldandNewZone = new map<string,string>();
    
    list<BSureS_Country__c> Countries = new list<BSureS_Country__c>();
    list<BSureS_Basic_Info__c> suppliers = new list<BSureS_Basic_Info__c>();
    
    for(BSureS_Country__c szoneObj:trigger.new)
    {
        CountryNames.add(szoneObj.Name);
    }
    
    Countries = [select Id,Name from BSureS_Country__c where Name in:CountryNames];
    
    if(Trigger.isInsert){
        for(BSureS_Country__c eZone: Countries){
            for(BSureS_Country__c newZone:trigger.new)
            {
                if(eZone.Name == newZone.Name )
                    newZone.addError('Record already Exists with the same Country Name');
            }
        }
    }
    if(Trigger.IsUpdate){
        
        for(BSureS_Country__c newSZone:trigger.new)
        {   
            oldNames.add('%'+Trigger.OldMap.get(newSZone.Id).Name+'%'); 
            oldCountries.add(Trigger.OldMap.get(newSZone.Id).Name);
            mapOldandNewZone.put(Trigger.OldMap.get(newSZone.Id).Name,newSZone.Name);
            for(BSureS_Country__c extSZone: Countries){
                if(extSZone.Name == newSZone.Name && extSZone.Id != newSZone.Id  )
                    newSZone.addError('Record already Exists with the same Country Name');
            }
        }
        
        suppliers = [SELECT id,Supplier_Countries__c 
                     FROM BSureS_Basic_Info__c 
                     WHERE Supplier_Countries__c != null 
                     AND Supplier_Countries__c Like: oldNames];
                                 
        for(string country:oldCountries){
            for(BSureS_Basic_Info__c sup:suppliers){ 
                if(sup.Supplier_Countries__c.Contains(country)){
                    sup.Supplier_Countries__c = sup.Supplier_Countries__c.replace(country,mapOldandNewZone.get(country));
                }
            }
        }
        BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
        if(!suppliers.isEmpty())
            update suppliers;
                    
        BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate;
         
    } 
    
    
    
    
}


/*
    for(BSureS_Country__c countryObj : Trigger.new)
    {
        if(Trigger.isInsert)
        {
            Integer countryRecCount = [select count() from BSureS_Country__c where name =: countryObj.name];
            if(countryRecCount > 0)
            {
                countryObj.addError('Record already Exists with the same Country Name');
            }
        }
        if(Trigger.isUpdate)
        {
            Integer countryRecCount = [select count() from BSureS_Country__c where name =: countryObj.name and id !=: countryObj.id];
            if(countryRecCount > 0)
            {
                countryObj.addError('Record already Exists with the same Country Name');
            }
        }
    }
*/