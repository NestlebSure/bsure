trigger BSureC_Countrytrigger on BSureC_Country__c (before insert, before update) {
	
	Set<String> Country = new Set<String>();
	Set<ID> CountryID = new set<ID>();		
	if(trigger.IsBefore){	
		for(BSureC_Country__c Cntry:Trigger.New)
		{	
			Country.add('%'+Cntry.Name+'%');
			CountryID.add(Cntry.Id);
		} 
		
		if(BSureC_Country__c.SObjectType.getDescribe().isQueryable()){
			list<BSureC_Country__c> lstCountry = [SELECT Id,Name 
													  FROM   BSureC_Country__c
													  WHERE  Name like:Country and id!=:CountryID]; 										  
			system.debug('Test'+Country);														  
			for(BSureC_Country__c ExistC:lstCountry)
			{
				for(BSureC_Country__c Cntry:Trigger.New){
					if(ExistC.Name == Cntry.Name )
					Cntry.addError('Record already exists with this name');
				}
			}
		}
	}
}


/*
for(BSureC_Country__c BSureCCountry:trigger.new){
	if(trigger.isBefore && trigger.isInsert)
	{
		Integer DuplicateBsureCountry = [SELECT count() from BSureC_Country__c where name=:BSureCCountry.name];
		if(DuplicateBsureCountry>0)
		BSureCCountry.adderror('Record already exists with this name');
	}
	if(trigger.isBefore && trigger.isUpdate)
	{
		Integer DuplicateBsureCountry = [SELECT count() from BSureC_Country__c where name=:BSureCCountry.name and id!=:BSureCCountry.id];
		if(DuplicateBsureCountry>0)
		BSureCCountry.adderror('Record already exists with this name');
	}
}
*/