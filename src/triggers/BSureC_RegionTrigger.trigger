trigger BSureC_RegionTrigger on BSureC_Region__c(before insert, before update) {

    Set<String> Region = new Set<String>();
    Set<ID> RegionID = new set<ID>();       
    if(trigger.IsBefore){   
        for(BSureC_Region__c Rgn:Trigger.New)
        {   
            Region.add(Rgn.Name);
            RegionID.add(Rgn.Id);
        } 
        if(BSureC_Region__c.SObjectType.getDescribe().isQueryable()){
	        list<BSureC_Region__c> LstRegion = [SELECT Id,Name 
	                                                  FROM   BSureC_Region__c
	                                                  WHERE  Name in :Region// ];
	                                                  and id!=:RegionID];                                         
	                                                              
	        for(BSureC_Region__c ExistC:LstRegion)
	        {
	            for(BSureC_Region__c Regn:Trigger.New){
	                if(ExistC.Name == Regn.Name )
	                Regn.addError('Record already exists with this name');
	            }
	        }
        }
        //if(LstRegion!=null && LstRegion.size()>0){
         //Regn.addError('Record already exists with this name');
        //}
    }

}

/*
    for(BSureC_Region__c BSureRegion:trigger.new){
        
      if(trigger.isBefore && trigger.isInsert)
      {
        Integer DuplicateBsure = [SELECT count() from BSureC_Region__c where name=:BSureRegion.name];
        if(DuplicateBsure>0)
        BSureRegion.adderror('Record already exists with this name');
      }
      
      if(trigger.isBefore && trigger.isUpdate)
      {
        Integer DuplicateBsure = [SELECT count() from BSureC_Region__c where name=:BSureRegion.name and id!=:BSureRegion.id];
        if(DuplicateBsure>0)
        BSureRegion.adderror('Record already exists with this name');
      }
      
    }
*/