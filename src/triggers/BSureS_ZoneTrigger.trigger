/***********************************************************************************************
*       Trigger Name 	: BSureS_ZoneTrigger
*       Date            : 18/11/2012 
*       Author          : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       18/11/2012               Kishorekumar A               Initial Version
		01/04/2013				Veereandranath jella		  To avoid soql inside loops 
**************************************************************************************************/
trigger BSureS_ZoneTrigger on BSureS_Zone__c (before insert,before update) 
{
	
	public boolean isSuppUpdate = false;
	/*public integer intCount = 0;
	
	
	for(BSureS_Zone__c zoneObj:trigger.new)
	{
		if(trigger.isInsert)
		{
			Integer zoneRecCount = [select count() from BSureS_Zone__c where name =: zoneObj.name];
			if(zoneRecCount > 0)
			{
				zoneObj.addError('Record already Exists with the same Zone Name');
			}
		}
		if(trigger.isUpdate)
		{
			list<BSureS_Basic_Info__c> lstSupplierBasicInfo;
			String strZoneOldName = '';
			Integer zoneRecCount = [select count() from BSureS_Zone__c where name =: zoneObj.name and id!=:zoneObj.id ];
			if(zoneRecCount > 0)
			{
				zoneObj.addError('Record already Exists with the same Zone Name');
			} 
			if(intCount < trigger.new.size())
			{
				BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
				if(trigger.new[intCount].Name != trigger.old[intCount].Name)
				{
					strZoneOldName	= '%'+ trigger.old[intCount].Name +'%';
					system.debug('strZoneOldName++++++++' + strZoneOldName);
					lstSupplierBasicInfo = new list<BSureS_Basic_Info__c>([select id,Supplier_Zones__c 
																			from BSureS_Basic_Info__c 
																			where Supplier_Zones__c != null 
					   														and Supplier_Zones__c like : strZoneOldName]);
					for(BSureS_Basic_Info__c sObj:lstSupplierBasicInfo)
					{
						sObj.Supplier_Zones__c = sObj.Supplier_Zones__c.replace(trigger.old[intCount].Name, trigger.new[intCount].Name);
					}
					update lstSupplierBasicInfo;
				}
			}
		}  
	}
	BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate;
	*/
	
	
	// Modifed Code
	
	list<string> zoneNames = new list<string>();
	list<string> oldNames = new list<string>();
	list<string> oldZones = new list<string>();
	map<string,string> mapOldandNewZone = new map<string,string>();
	
	list<BSureS_Zone__c> zones = new list<BSureS_Zone__c>();
	list<BSureS_Basic_Info__c> suppliers = new list<BSureS_Basic_Info__c>();
	
	for(BSureS_Zone__c zoneObj:trigger.new)
	{
		zoneNames.add(zoneObj.Name);
	}
	
	zones = [select Id,Name from BSureS_Zone__c where Name in:zoneNames];
	
	if(Trigger.isInsert){
		for(BSureS_Zone__c eZone: zones){
			for(BSureS_Zone__c newZone:trigger.new)
			{
				if(eZone.Name == newZone.Name )
					newZone.addError('Record already Exists with the same Zone Name');
			}
		}
	}
	if(Trigger.IsUpdate){
		
		for(BSureS_Zone__c newZone:trigger.new)
		{	
			oldNames.add('%'+Trigger.OldMap.get(newZone.Id).Name+'%');	
			oldZones.add(Trigger.OldMap.get(newZone.Id).Name);
			mapOldandNewZone.put(Trigger.OldMap.get(newZone.Id).Name,newZone.Name);
			for(BSureS_Zone__c eZone: zones){
				if(eZone.Name == newZone.Name && eZone.Id != newZone.Id  )
					newZone.addError('Record already Exists with the same Zone Name');
			}
		}
		
		suppliers = [SELECT id,Supplier_Zones__c 
					 FROM BSureS_Basic_Info__c 
					 WHERE Supplier_Zones__c != null 
   					 AND Supplier_Zones__c Like: oldNames];
   					 			 
		for(string zname:oldZones){
			for(BSureS_Basic_Info__c sup:suppliers){ 
				if(sup.Supplier_Zones__c.Contains(zname)){
					sup.Supplier_Zones__c = sup.Supplier_Zones__c.replace(zname,mapOldandNewZone.get(zname));
				}
			}
		}
		BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
		if(!suppliers.isEmpty())
			update suppliers;
					
		BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate;
		 
	} 
}