/***********************************************************************************************
*       Trigger Name    : BSureS_UpdateSupplierNotification
*       Date            : 28/12/2012 
*       Author          : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       28/12/2012               Kishorekumar A                Initial Version
*       13/06/2013               Sridhar Bonagiri              Added 'RunTriggers' condition
*       27/06/2013               kishorekumar A                Update Notification purpose added 'SendSupplierInfoUpdates'
**************************************************************************************************/
trigger BSureS_UpdateSupplierNotification on BSureS_Basic_Info__c (before update, after update, after insert) 
{
    public integer intCount = 0;
    public string strEmailCSV {get; set;}
    public string strSubject {get; set;}
    public string strEmailBody {get; set;}
    public set<Id> setEmailRcpts {get; set;}
    public list<Id> commasplitIds{get;set;}
    public boolean sendMail = false;
    public boolean isSuppUpdate {get; set; } 
    public list<BSureS_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureS_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureS_Country_Manager__c> lstCountryManager{get;set;}
    public list<BSureS_Assigned_Buyers__c> lstAssignedBuyers{get;set;}
    public list<BSureS_Assigned_Buyers__c> lstUploadBuyers{get;set;}
    //Region Ids
    string strZoneId='';
    string strSubZoneId='';
    string strCountryId='';
    string strSupplierId='';
    //BackupAnalyst
    list<BSureS_Basic_Info__c> ListSupplier= new list<BSureS_Basic_Info__c>();
    list<string> backupAnalysts = new list<string>();
    list<string> lstStringBackUpAnalyst = new list<string>();
    map<Id,list<string>>  maplistSupp = new map<Id,list<string>>();
    list<BSureS_Basic_Info__Share> lstSupplierShare = new list<BSureS_Basic_Info__Share>();
    public Event EventObj{get;set;}
    string strNames=''; 
    string strAnalystRole='';
    string strAnalystIDold ='';
    string strAnalystIDnew = '';
    //string strSupplierID = '';
    string strPlannedReviewDate ='';
    string strSupplierName = '';
    list<Event> lstEventdelete{get;set;}
    string strrunTriggers=''; 
    string strSendUpdates = '';
    
    if(!BSureS_CommonUtil.getConfigurationValues('BSureS_Run_Triggers').isEmpty())
        strrunTriggers = BSureS_CommonUtil.getConfigurationValues('BSureS_Run_Triggers').get(0);
    if(!BSureS_CommonUtil.getConfigurationValues('BSureS_SendSupplierInfoUpdates').isEmpty())
        strSendUpdates = BSureS_CommonUtil.getConfigurationValues('BSureS_SendSupplierInfoUpdates').get(0);
    
    if (strrunTriggers.equalsIgnoreCase('TRUE'))
    {
    
    if(trigger.isBefore && trigger.isUpdate)  
    {
        //BSureS_EmailReminders.CreateEmailReminders(trigger.newMap,trigger.oldMap);
        
        //strAnalystRole = BSureS_CommonUtil.getConfigurationValues('BSureS_AnalystRole').get(0);
        //map<id,BSureS_User_Additional_Attributes__c> mapObj = new map<id,BSureS_User_Additional_Attributes__c>([select id,User__r.name from BSureS_User_Additional_Attributes__c where User__r.ProfileId =:getprofileId.id]);
        //map<id,User> mapObj = new map<id,User>([select id,name from user where Profile.Name = 'BSureS_Analyst'and IsActive = true]);
        map<id,User> mapObj = new map<id,User>([select id,name from user  where IsActive = true limit 5000]);
        /*
        for(Id supplierObjBAEvent :trigger.oldMap.keySet())
        {
            BSureS_Basic_Info__c rec_Suppl_Info = trigger.oldMap.get(supplierObjBAEvent);
            lstEventdelete1.add(rec_Suppl_Info.Analyst__c);
            lstSupplierId.add(rec_Suppl_Info.Id);
        }
        list<Event> lstEventdelete = new list<Event>([select id from Event where OwnerId  IN :lstEventdelete1 and WhatId IN : lstSupplierId ]);
        system.debug('lstEventdelete==========size===='+lstEventdelete.size());
        if(!lstEventdelete.isEmpty()) database.delete(lstEventdelete,false);  
        */    
        
        for(BSureS_Basic_Info__c suppObjTest : trigger.old)
        {
            strSupplierID = suppObjTest.Id;
            if(suppObjTest.Analyst__c != null)
            {
                strAnalystIDold = suppObjTest.Analyst__c;
            }   
        }   
        if( (strSupplierID != null && strSupplierID !='' ) && (strAnalystIDold != null && strAnalystIDold != '') )
        {
            lstEventdelete = new list<Event>([select id from Event where OwnerId =: strAnalystIDold and WhatId =: strSupplierID ]);
            if(!lstEventdelete.isEmpty()) database.delete(lstEventdelete,false);  
        }  
              
        for(BSureS_Basic_Info__c supplierObjBA :trigger.new)
        {
            if(supplierObjBA.Bakup_Analysts__c!= null && supplierObjBA.Bakup_Analysts__c.contains(',')) 
            {
                lstStringBackUpAnalyst = supplierObjBA.Bakup_Analysts__c.split(',');
                maplistSupp.put(supplierObjBA.Id,lstStringBackUpAnalyst);
            }
            else
            {
                lstStringBackUpAnalyst.add(supplierObjBA.Bakup_Analysts__c);
                maplistSupp.put(supplierObjBA.Id,lstStringBackUpAnalyst);
            }
        }
        list<BSureS_Basic_Info__Share> lstdelete = [select id from BSureS_Basic_Info__Share where UserOrGroupId in:lstStringBackUpAnalyst and ParentId IN: maplistSupp.keySet()];
                        if(!lstdelete.isEmpty())database.delete(lstdelete,false);
        
        for(BSureS_Basic_Info__c supplierObj:trigger.new)
        {
            strSupplierID = supplierObj.Id;
            strNames = '';
            system.debug('Check  point 22'+supplierObj.Next_Review_Date__c);
            if(supplierObj.Next_Review_Date__c != null)
            {
                supplierObj.Planned_Review_Date__c = supplierObj.Next_Review_Date__c;
            } 
            else
            {
                if(supplierObj.Rating__c =='Inactive' ||  supplierObj.Rating__c =='Out of Scope' || supplierObj.Rating__c =='')
                {
                    supplierObj.Planned_Review_Date__c = null;
                }
            }
             system.debug('Check  point 2442'+supplierObj.Planned_Review_Date__c);
            if(supplierObj.Bakup_Analysts__c!= null && supplierObj.Bakup_Analysts__c.contains(','))  
            {
                backupAnalysts = supplierObj.Bakup_Analysts__c.split(',');
                if(backupAnalysts.size() > 0)
                {
                    //system.debug('backupAnalysts.....'+backupAnalysts);
                    
                    for(string sObj:backupAnalysts)
                    {
                        //system.debug('mapObj.get(sObj).name========'+mapObj.get(sObj).name);
                        if((sObj != null && sObj != '') && mapObj.containskey(sObj)){
                            if(mapObj.get(sObj).name != null){
                                strNames += mapObj.get(sObj).name+',';  
                            }
                            if(sObj != null && sObj != ''){
                                system.debug('sObj==========1====='+sObj);
                                BSureS_Basic_Info__Share shareObj = new BSureS_Basic_Info__Share();
                                shareObj.AccessLevel = 'Edit';
                                //shareObj.RowCause = 'Manual';
                                shareObj.ParentId = supplierObj.Id;
                                shareObj.UserOrGroupId = sObj;
                                lstSupplierShare.add(shareObj);
                            }
                        }    
                    } 
                    strNames=strNames.left(strNames.length()-1);
                    
                }
            }
            else
            {
                if(supplierObj.Bakup_Analysts__c != null && (mapObj.containskey(supplierObj.Bakup_Analysts__c) && mapObj.get(supplierObj.Bakup_Analysts__c) != null) )
                {
                    //system.debug('Share========22===');
                    strNames = mapObj.get(supplierObj.Bakup_Analysts__c).name;
                    if(supplierObj.Bakup_Analysts__c != null && supplierObj.Bakup_Analysts__c != ''){
                        /*
                        list<BSureS_Basic_Info__Share> lstdelete = [select id from BSureS_Basic_Info__Share where UserOrGroupId =:supplierObj.Bakup_Analysts__c];
                        if(!lstdelete.isEmpty()) database.delete(lstdelete,false);  
                        */
                        //system.debug('sObj==============2'+ supplierObj.Bakup_Analysts__c);
                        BSureS_Basic_Info__Share shareObj = new BSureS_Basic_Info__Share();
                        shareObj.AccessLevel = 'Edit';
                       //shareObj.RowCause = 'Manual'; 
                        shareObj.ParentId = supplierObj.Id;
                         shareObj.UserOrGroupId = supplierObj.Bakup_Analysts__c;
                        lstSupplierShare.add(shareObj);
                    }
                }
            }
            supplierObj.Bakup_Analysts_Names__c = strNames;
         
            if(supplierObj.Analyst__c != null)
            {
                strAnalystIDnew = supplierObj.Analyst__c ;
            } 
            if(supplierObj.Planned_Review_Date__c != null)
            {
                strPlannedReviewDate = string.valueOf(supplierObj.Planned_Review_Date__c);
            }
            if( supplierObj.Supplier_Name__c != null)
            {
                strSupplierName =  supplierObj.Supplier_Name__c;
            }
            
        }
        //update ListSupplier;
    }
    if( (strAnalystIDnew != null && strAnalystIDnew != '') && (strPlannedReviewDate != null && strPlannedReviewDate !='') )
    {
        EventObj = new Event();
        EventObj.StartDateTime = Date.valueOf(strPlannedReviewDate);
        EventObj.EndDateTime =  Date.valueOf(strPlannedReviewDate);
        EventObj.Subject = 'Create a New Review ';
        EventObj.Description  = 'Create a New Review for this Supplier : '+ strSupplierName +' ; ';
        EventObj.Description  += 'Planned Review Date :' + strPlannedReviewDate ;
        EventObj.OwnerId = strAnalystIDnew;
        EventObj.WhatId = strSupplierID;
        if(EventObj != null)
        {
            insert EventObj;
        }
    }
    
    //system.debug('lstSupplierShare=========='+lstSupplierShare);
    if(!lstSupplierShare.isEmpty())
     database.upsert (lstSupplierShare,false);
    
    //Backup Analyst End
    if(trigger.isAfter && trigger.isUpdate)
    {
        isSuppUpdate = BSureS_CommonUtil.isSupplierUpdate;
        if(isSuppUpdate)
            {
                if(strSendUpdates.equalsIgnoreCase('TRUE'))
                {
                
                list<BSureS_Basic_Info__c> lstSupplier = [select id,Supplier_Name__c,Supplier_Category_Name__c, Owner_Ship__c,F_S__c,
                                                        Supplier_Contact__c,Sole_Sourced__c,Analyst__c,Manager__c,Bakup_Analysts__c,
                                                        Fiscal_Year_End__c,Financial_Information__c,Review_Status__c,Notification_Flag__c,
                                                        Spend__c,Last_Financial_Statement_Received__c,Has_Parent__c,Parent_Supplier_Name__c,
                                                        Email_address__c,Phone_Number__c,Fax__c,Website_Link__c,Zone_Name__c,Sub_Zone_Name__c,
                                                        Country_Name__c,Street_Name_Address_Line_1__c,Address_Line_2__c,Postal_Code__c,City__c,Planned_Review_Date__c,
                                                        Zone__c,Sub_Zone__c,BSureS_Country__c,Next_Review_Date__c,Analyst_Name__c,Manager_Name__c,Bakup_Analysts_Names__c
                                                        from BSureS_Basic_Info__c  where  id in:trigger.newmap.keyset()];
            
                if(lstSupplier.get(0).Supplier_Name__c != null)
                {
                    strSubject = 'Supplier Information Update for : ' + lstSupplier.get(0).Supplier_Name__c;
                }
                strEmailCSV ='';
                strEmailBody = '';
                
                for(BSureS_Basic_Info__c supplierObj:lstSupplier)
                {
                    //system.debug('Analyst_Name__c========='+supplierObj.Analyst_Name__c);
                    setEmailRcpts = new set<Id>();
                    lstZoneManager = new list<BSureS_Zone_Manager__c>();
                    lstSubZoneManager = new list<BSureS_Sub_Zone_Manager__c>();
                    lstCountryManager = new list<BSureS_Country_Manager__c>();
                    lstAssignedBuyers = new list<BSureS_Assigned_Buyers__c>();
                    if(supplierObj.Id != null)
                    {
                        strSupplierId = supplierObj.Id; 
                    }
                    if(intCount < trigger.new.size()) 
                    {
                        
                        strEmailBody += '<table width="800px" border="1">';
                        strEmailBody += '<tr style="border:thin solid;"><td width="34%" height="21px"><b>Field Name</b></td><td width="33%" height="21px"><b>Old Value</b></td><td width="33%" height="21px"><b>New Value</b></td></tr>';
                        if(trigger.new[intCount].Supplier_Name__c != trigger.old[intCount].Supplier_Name__c && 
                                (trigger.new[intCount].Supplier_Name__c != null && trigger.new[intCount].Supplier_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Supplier Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Supplier_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Supplier_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Supplier_Category_Name__c != trigger.old[intCount].Supplier_Category_Name__c && 
                                (trigger.new[intCount].Supplier_Category_Name__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Category Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Supplier_Category_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Supplier_Category_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Sole_Sourced__c != trigger.old[intCount].Sole_Sourced__c && 
                                (trigger.new[intCount].Sole_Sourced__c != null && trigger.new[intCount].Sole_Sourced__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Sole Sourced</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Sole_Sourced__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Sole_Sourced__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Owner_Ship__c != trigger.old[intCount].Owner_Ship__c && 
                                (trigger.new[intCount].Owner_Ship__c != null && trigger.new[intCount].Owner_Ship__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Owner Ship</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Owner_Ship__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Owner_Ship__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].F_S__c != trigger.old[intCount].F_S__c && 
                                (trigger.new[intCount].F_S__c != null && trigger.new[intCount].F_S__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">F/S</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].F_S__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].F_S__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].IsPotential_Supplier__c != trigger.old[intCount].IsPotential_Supplier__c && 
                                (trigger.new[intCount].IsPotential_Supplier__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">IsPotential Supplier</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].IsPotential_Supplier__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].IsPotential_Supplier__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        if(trigger.new[intCount].Notification_Flag__c != trigger.old[intCount].Notification_Flag__c && 
                                (trigger.new[intCount].Notification_Flag__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Notification Flag</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Notification_Flag__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Notification_Flag__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].Review_Status__c != trigger.old[intCount].Review_Status__c && 
                                (trigger.new[intCount].Review_Status__c != null && trigger.new[intCount].Review_Status__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Review Status</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Review_Status__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Review_Status__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].Financial_Information__c != trigger.old[intCount].Financial_Information__c && 
                                (trigger.new[intCount].Financial_Information__c != null && trigger.new[intCount].Financial_Information__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px"> Financial Information</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Financial_Information__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Financial_Information__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }  
                        if(trigger.new[intCount].Fiscal_Year_End__c != trigger.old[intCount].Fiscal_Year_End__c && 
                                (trigger.new[intCount].Fiscal_Year_End__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Fiscal Year End</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Fiscal_Year_End__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Fiscal_Year_End__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        if(trigger.new[intCount].Spend__c != trigger.old[intCount].Spend__c && 
                                (trigger.new[intCount].Spend__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Spend</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Spend__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Spend__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        if(trigger.new[intCount].Last_Financial_Statement_Received__c != trigger.old[intCount].Last_Financial_Statement_Received__c && 
                                (trigger.new[intCount].Last_Financial_Statement_Received__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Last Financial Statement Received</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Last_Financial_Statement_Received__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Last_Financial_Statement_Received__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }       
                        if(trigger.new[intCount].Has_Parent__c != trigger.old[intCount].Has_Parent__c && 
                                (trigger.new[intCount].Has_Parent__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Has Parent</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Has_Parent__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Has_Parent__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].Parent_Supplier_Name__c != trigger.old[intCount].Parent_Supplier_Name__c && 
                                (trigger.new[intCount].Parent_Supplier_Name__c != null && trigger.new[intCount].Parent_Supplier_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Parent Suplier Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Parent_Supplier_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Parent_Supplier_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }  
                        if(trigger.new[intCount].Supplier_Contact__c != trigger.old[intCount].Supplier_Contact__c && 
                                (trigger.new[intCount].Supplier_Contact__c != null && trigger.new[intCount].Supplier_Contact__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Supplier Contact</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Supplier_Contact__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Supplier_Contact__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        if(trigger.new[intCount].Email_address__c != trigger.old[intCount].Email_address__c && 
                                (trigger.new[intCount].Email_address__c != null && trigger.new[intCount].Email_address__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Email Address</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Email_address__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Email_address__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        
                        if(trigger.new[intCount].Phone_Number__c != trigger.old[intCount].Phone_Number__c && 
                                (trigger.new[intCount].Phone_Number__c != null && trigger.new[intCount].Phone_Number__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Postal Code</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Phone_Number__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Phone_Number__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].Fax__c != trigger.old[intCount].Fax__c && 
                                (trigger.new[intCount].Fax__c != null && trigger.new[intCount].Fax__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Fax</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Fax__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Fax__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Website_Link__c != trigger.old[intCount].Website_Link__c && 
                                (trigger.new[intCount].Website_Link__c != null && trigger.new[intCount].Website_Link__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Website Link</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Website_Link__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Website_Link__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Zone_Name__c != trigger.old[intCount].Zone_Name__c && 
                                (trigger.new[intCount].Zone_Name__c != null && trigger.new[intCount].Zone_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Zone Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Zone_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Zone_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Sub_Zone_Name__c != trigger.old[intCount].Sub_Zone_Name__c && 
                                (trigger.new[intCount].Sub_Zone_Name__c != null && trigger.new[intCount].Sub_Zone_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Sub Zone Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Sub_Zone_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Sub_Zone_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Country_Name__c != trigger.old[intCount].Country_Name__c && 
                                (trigger.new[intCount].Country_Name__c != null && trigger.new[intCount].Country_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Country Name</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Country_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Country_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Street_Name_Address_Line_1__c != trigger.old[intCount].Street_Name_Address_Line_1__c && 
                                (trigger.new[intCount].Street_Name_Address_Line_1__c != null && trigger.new[intCount].Street_Name_Address_Line_1__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Street Name/Address Line 1</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Street_Name_Address_Line_1__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Street_Name_Address_Line_1__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Address_Line_2__c != trigger.old[intCount].Address_Line_2__c && 
                                (trigger.new[intCount].Address_Line_2__c != null && trigger.new[intCount].Address_Line_2__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Address Line 2</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Address_Line_2__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Address_Line_2__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Postal_Code__c != trigger.old[intCount].Postal_Code__c && 
                                (trigger.new[intCount].Postal_Code__c != null && trigger.new[intCount].Postal_Code__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Postal Code</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Postal_Code__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Postal_Code__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }  
                        if(trigger.new[intCount].City__c != trigger.old[intCount].City__c && 
                                (trigger.new[intCount].City__c != null && trigger.new[intCount].City__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">City</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].City__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].City__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }  
                        if(trigger.new[intCount].Planned_Review_Date__c != trigger.old[intCount].Planned_Review_Date__c && 
                                (trigger.new[intCount].Planned_Review_Date__c != null ))
                        {
                            strEmailBody += '<tr><td height="21px">Planned Review Date</td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Planned_Review_Date__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Planned_Review_Date__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        } 
                        if(trigger.new[intCount].Analyst_Name__c != trigger.old[intCount].Analyst_Name__c && 
                                (trigger.new[intCount].Analyst_Name__c != null && trigger.new[intCount].Analyst_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Analyst </td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Analyst_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Analyst_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        if(trigger.new[intCount].Manager_Name__c != trigger.old[intCount].Manager_Name__c && 
                                (trigger.new[intCount].Manager_Name__c != null && trigger.new[intCount].Manager_Name__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Manager </td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Manager_Name__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Manager_Name__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }   
                        if(trigger.new[intCount].Bakup_Analysts_Names__c != trigger.old[intCount].Bakup_Analysts_Names__c && 
                                (trigger.new[intCount].Bakup_Analysts_Names__c != null && trigger.new[intCount].Bakup_Analysts_Names__c != ''))
                        {
                            strEmailBody += '<tr><td height="21px">Backup Analysts </td><td height="21px">';
                            strEmailBody += trigger.old[intCount].Bakup_Analysts_Names__c;
                            strEmailBody += '</td><td height="21px">';
                            strEmailBody += trigger.new[intCount].Bakup_Analysts_Names__c;
                            strEmailBody += '</td></tr>';
                            sendMail = true;
                        }
                        strEmailBody += '</table>';            
                    }
                    if(supplierObj.Analyst__c != null)
                    {
                        setEmailRcpts.add(supplierObj.Analyst__c);
                    }
                    if(supplierObj.Manager__c != null)
                    {
                        setEmailRcpts.add(supplierObj.Manager__c);
                    }
                    if(supplierObj.Bakup_Analysts__c != null)
                    {
                        //commasplitIds = supplierObj.Bakup_Analysts__c.split(',');
                        //setEmailRcpts.addall(commasplitIds);
                    }
                    
                    //Add region Ids
                    if(supplierObj.Zone__c != null)
                    {
                        strZoneId = supplierObj.Zone__c;
                    }
                    if(supplierObj.Sub_Zone__c != null)
                    {
                        strSubZoneId = supplierObj.Sub_Zone__c;
                    }
                    if(supplierObj.BSureS_Country__c != null)
                    {
                        strCountryId = supplierObj.BSureS_Country__c;
                    }
                   
                }
                if(strZoneId != null && strZoneId != '')
                {
                    lstZoneManager = [select Zone_Manager__c from BSureS_Zone_Manager__c where Zone__c =: strZoneId];
                }
                if(strSubZoneId != null && strSubZoneId != '')
                {
                    lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureS_Sub_Zone_Manager__c where Sub_Zone__c =: strSubZoneId];
                }
                if(strCountryId != null && strCountryId != '')
                {
                    lstCountryManager = [select Country_Manager__c from BSureS_Country_Manager__c where Country__c =: strCountryId];
                }
                if(strSupplierId != null && strSupplierId != '')
                {
                    //lstAssignedBuyers = [select id,Buyer_Id__c,Supplier_ID__c from BSureS_Assigned_Buyers__c where Supplier_ID__c =: strSupplierId];
                }
                if(lstZoneManager != null && lstZoneManager.size() > 0)
                {
                    for(BSureS_Zone_Manager__c objZM: lstZoneManager)
                    {
                        if(objZM.Zone_Manager__c != null)
                        {
                            setEmailRcpts.add(objZM.Zone_Manager__c);
                        }   
                    }
                }
                if(lstSubZoneManager != null && lstSubZoneManager.size() > 0)
                {
                    for(BSureS_Sub_Zone_Manager__c objSZM :lstSubZoneManager )
                    {
                        if(objSZM.Sub_Zone_Manager__c != null)
                        {
                            setEmailRcpts.add(objSZM.Sub_Zone_Manager__c);
                        }   
                    }
                }
                if(lstCountryManager != null && lstCountryManager.size() > 0)
                {
                    for(BSureS_Country_Manager__c objCM : lstCountryManager)
                    {
                        if(objCM.Country_Manager__c != null)
                        {
                            setEmailRcpts.add(objCM.Country_Manager__c);
                        }   
                    }
                } 
                /*
                if(lstAssignedBuyers != null && lstAssignedBuyers.size() > 0)
                {
                    for(BSureS_Assigned_Buyers__c buyerObj : lstAssignedBuyers)
                    {
                        if(buyerObj.Buyer_ID__c != null)
                        {
                            setEmailRcpts.add(buyerObj.Buyer_ID__c);
                        }
                    }
                }*/
                if(sendMail)
                {
                    BSureS_CommonUtil.SendEmailToQueue(setEmailRcpts, strSubject, strEmailBody, 'High', system.today(),false);
                }
            }
         }
       }  
       
        if(trigger.isInsert && trigger.isAfter)
        {
            lstUploadBuyers = new list<BSureS_Assigned_Buyers__c>(); 
            for(BSureS_Basic_Info__c objEachSupplier : trigger.new)
            {
                if(objEachSupplier.BulkUpload_BuyerID__c != null)
                {
                    BSureS_Assigned_Buyers__c objEachBuyer = new BSureS_Assigned_Buyers__c();
                    objEachBuyer.Buyer_Category__c = objEachSupplier.Supplier_Category_Name__c;
                    objEachBuyer.Buyer_ID__c = objEachSupplier.BulkUpload_BuyerID__c;
                    objEachBuyer.Buyer_Contact_ID__c=objEachSupplier.BulkUpload_Buyer_Contact_ID__c;
                    objEachBuyer.Buyer_Name__c = objEachSupplier.BulkUpload_BuyerName__c;
                    objEachBuyer.Supplier_ID__c = objEachSupplier.Id;
                    objEachBuyer.Supplier_Name__c = objEachSupplier.Supplier_Name__c;
                    lstUploadBuyers.add(objEachBuyer);
                }   
            }
            if(lstUploadBuyers != null && lstUploadBuyers.size() > 0)
            {
                insert lstUploadBuyers;
            }   
        }
    }
}