trigger BSureC_CountryManagerTrigger on BSureC_Country_Manager__c (before insert, before update) 
{
	/*
    for(BSureC_Country_Manager__c countryManagerObj:trigger.new)
    {
        if(trigger.isInsert)
        {
            Integer countryManagerRecCount = [select count() from BSureC_Country_Manager__c where Country__c =: countryManagerObj.Country__c and Country_Manager__c =:countryManagerObj.Country_Manager__c];
            if(countryManagerRecCount > 0 )
            {
                countryManagerObj.addError('Record already Exists with the same Country Name and Country Manager');
            }
            if(countryManagerObj.Country_Manager__c == null)
            {
                countryManagerObj.addError('Please Select Country Manager.');
            }
        }
        if(trigger.isUpdate)
        {
            Integer countryManagerRecCount = [select count() from BSureC_Country_Manager__c where Country__c =: countryManagerObj.Country__c and Country_Manager__c =:countryManagerObj.Country_Manager__c and id !=:countryManagerObj.id];
            if(countryManagerRecCount > 0 )
            {
                countryManagerObj.addError('Record already Exists with the same Country Name and Country Manager');
            }
            if(countryManagerObj.Country_Manager__c == null)
            {
                countryManagerObj.addError('Please Select Country Manager.');
            }
        }
    }
    */
    Set<ID> Region = new Set<ID>();
    Set<ID> setCountry =new Set<ID>();
    Set<ID> RegionID = new set<ID>();
           
    if(trigger.IsBefore){
    	
    	for(BSureC_Country_Manager__c countryManagerObj:trigger.new)
        {   
        	setCountry.add(countryManagerObj.Country__c);
            Region.add(countryManagerObj.Country_Manager__c);
            RegionID.add(countryManagerObj.Id);
        }
         
        if(BSureC_Country_Manager__c.SObjectType.getDescribe().isQueryable()){
	        list<BSureC_Country_Manager__c> LstRegion = [select Id,Country_Manager__c,Country__c 
	        											 from BSureC_Country_Manager__c 
	        											 where Country__c =: setCountry 
	        											 and Country_Manager__c =:Region  
	        											 and Id !=:RegionID];
	        											 
	         for(BSureC_Country_Manager__c ExistC:LstRegion)
	        {
	            for(BSureC_Country_Manager__c Cat:Trigger.New){
	                if(ExistC.Country_Manager__c == Cat.Country_Manager__c )
	                Cat.addError('Record already exists with this name');
	            }
	        }
        }											                                          
 
    }
}