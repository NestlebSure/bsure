trigger BSureC_updateCollectorMngr on BSureC_AdditionalAttributes__c (after update) {
	
	list<id> listCollectors =new list<id>();
	map<id,id> mapMngr =  new map<id,id>();
    map<id,string> mapMngrName =  new map<id,string>();
    map<id,id> mapNewMngr = new map<id,id>();
    map<id,string> mapNewMngrName = new map<id,string>();
    map<string,String> mapCustInfo =new map<String,String>();
    map<id, string> mapUname = new map<id, string>();
    List<BSureC_Customer_Basic_Info__c> lstCustInfo = new List<BSureC_Customer_Basic_Info__c>();
    Id newUserID;
    String strMngr;
    String strMngrName;
	String strUserID;
                            
	if(Trigger.isAfter || Trigger.isUpdate)
	{
		list<BSureC_AdditionalAttributes__c> lstAddAtt = [SELECT User__c,Manager__c,Manager__r.name FROM BSureC_AdditionalAttributes__c];
		
		list<User> lstUserInfo = [SELECT Id,Name FROM User];
		
		for(User u:lstUserInfo){
			mapUname.put(u.Id,u.name);
		}
		
		for(BSureC_AdditionalAttributes__c addAttExist:lstAddAtt){
			mapMngr.put(addAttExist.User__c,addAttExist.Manager__c);
			mapMngrName.put(addAttExist.User__c,addAttExist.Manager__r.name);
		}
		for(BSureC_AdditionalAttributes__c addAttObj:trigger.new)
	    {
	    	mapNewMngr.put(addAttObj.User__c,addAttObj.Manager__c);
	    	mapNewMngrName.put(addAttObj.User__c,mapUname.get(addAttObj.Manager__c));
	    	newUserID = addAttObj.User__c;
	    }
	    system.debug(mapMngr.size()+'mapuser$$'+mapNewMngr.size());
	    User user =  [SELECT Id,Name,UserRole.Name,Profile.Name FROM User where id=: newUserID];
	    strUserID = string.valueOf(newUserID);
	    system.debug(newUserID+'strUserID@@@@'+strUserID);
	    if(strUserID.length() == 18)
	    strUserID = strUserID.left(strUserID.length()-3);
	    
	    newUserID = ID.valueOf(strUserID);
	    system.debug(strUserID+'newUserID@@@@'+newUserID);
    	 //If user profile and role is Collector 
     	 if(user.Profile.Name == 'BSureC_Collector' && user.UserRole.Name == 'Collector'){
     	 	
     	 	String searchquery = 'FIND \'' + strUserID + '\' IN ALL FIELDS RETURNING BSureC_Customer_Basic_Info__c(id)';
			List<List<SObject>> searchList = search.query(searchquery);
			
			List<BSureC_Customer_Basic_Info__c> lstCust = (list<BSureC_Customer_Basic_Info__c>) searchList[0];
			set<string> setID = new set<string>();
			
			for(BSureC_Customer_Basic_Info__c obj: lstCust){
				setID.add(obj.id);
			}
			system.debug('setID$$$$$$$$'+setID.size());
     	 	
     	 	for(BSureC_Customer_Basic_Info__c updatecust : [select id,Collector__c,Collector_Name__c,Collector_Manager__c,Collector_Manager_Name__c from BSureC_Customer_Basic_Info__c where id in: setID])	                	
            {	
            	strMngr = '';
            	strMngrName = '';
            	              				
    			if(updatecust.Collector__c !=null && updatecust.Collector__c !='' && updatecust.Collector__c.contains(newUserID))
    			{
    				if(updatecust.Collector__c.contains(',')){
    					
    					listCollectors = updatecust.Collector__c.split(',');
    				}else{
        				listCollectors.add(updatecust.Collector__c);
        			}
        			for(id col:listCollectors){
        				
        				if(mapNewMngr.containskey(col)){
        					
        					strMngr = mapNewMngr.get(col)+',';
        					strMngrName = mapNewMngrName.get(col)+';';
        				}else
        				{
        					if(!strMngr.contains(mapMngr.get(col)))
        					strMngr +=mapMngr.get(col)+',';
        					
        					if(!strMngrName.contains(mapMngrName.get(col)))
        					strMngrName += mapMngrName.get(col)+';';
        				}
        				
        			}
        			updatecust.Collector_Manager__c = strMngr.left(strMngr.length()-1);
    				updatecust.Collector_Manager_Name__c = strMngrName.left(strMngrName.length()-1);
    				system.debug(updatecust.Collector_Manager__c+'updatecust.Collector_Manager__c$$$$'+updatecust.Collector_Manager_Name__c);
    				lstCustInfo.add(updatecust);
    			}
            }
            Database.Update(lstCustInfo,false);
         }
	}
	
	}