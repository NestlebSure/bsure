/************************************************************************************************       
Controller Name         : BSureS_Spend_History_Stage_update       
Date                    : 11/15/2012        
Author                  : Praveen Sappati       
Purpose                 : To validate the stage history record when manually edited.       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/15/2012      Praveen Sappti            Initial Version
                          11/29/2012      Praveen Sappati            Changes done on the feedback of demo
                          12/10/2012      Praveen Sappati            Changes done on the feedback of demo
**************************************************************************************************/
trigger BSureS_Spend_History_Stage_update on BSureS_Spend_History_Stage__c (before update) 
{
    Map<String,Id> map_update_supplierid = new Map<String,Id>();//initializing map 
    Map<Date,String> map_val_spendata_publish = new Map<Date,String>();//initializing map 
    boolean bool_unique_supplier=false;//assiging boolean value 
    boolean bool_groupID_exists=false;//assiging boolean value 
    boolean bool_date_mandatory=false;//assiging boolean value 
    boolean bool_spend_period_mandatory=false;//assiging boolean value 
    boolean bool_spend_amount_mandatory=false;//assiging boolean value 
    boolean bool_groupID_mandatory=false;//assiging boolean value 
    
    List<BSureS_Basic_Info__c> lst_supllier_data = [SELECT Id,Globe_ID__c FROM BSureS_Basic_Info__c WHERE Globe_ID__c!=NULL ];//querying all records from supplier basic info.
    List<BSureS_Spend_History_Publish__c> lst_spendhistory_data= [SELECT Globe_ID__c,date__c FROM BSureS_Spend_History_Publish__c WHERE date__c!=NULL];//querying all records from spend history publish.
    for(BSureS_Basic_Info__c reccustinfo : lst_supllier_data)
    {
        map_update_supplierid.put(reccustinfo.Globe_ID__c,reccustinfo.Id);//adding globe id and supplier basic info recordID to the list map
    }
    
    for(BSureS_Spend_History_Publish__c spendhistory_data_publish : lst_spendhistory_data)
    {
        map_val_spendata_publish.put(spendhistory_data_publish.date__c,spendhistory_data_publish.Globe_ID__c);//adding date and globe id to the list map.
    }
    
    for(BSureS_Spend_History_Stage__c spend_history_stage:trigger.new)
    {
        if(spend_history_stage.Publish_Flag__c==true)
        {
            spend_history_stage.Exception__c='';//making exception field as null.
            //spend_history_stage.Status__c='Published';
        }
        
        else
        {
        spend_history_stage.Exception__c='';//making exception field as null.
        
        try
        {
         Double groupid=null;//double variable declaration
         groupid=Double.valueof(spend_history_stage.Globe_ID__c);//converting String GroupID to double type to validate the format of globe id.
         spend_history_stage.Status__c='Initial Upload';
         spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Yellow';
        
        }
        catch(Exception e)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=String.valueof(e)+' in Globe ID';//assiging value to exception field
                spend_history_stage.Status__c='Exception';
                spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
            }
            else
            {
                spend_history_stage.Exception__c+=','+String.valueof(e)+' in Globe ID';//assiging value to exception field
                spend_history_stage.Status__c='Exception';
                spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
            }   
        }
        
        if(map_update_supplierid!=NULL)
        {
            if(map_update_supplierid.get(spend_history_stage.Globe_ID__c)!=NULL)//getting the supplierID based on globe id
            {
                spend_history_stage.Supplier_ID__c=ID.valueof(map_update_supplierid.get(spend_history_stage.Globe_ID__c));//updating the supplierID in the spend history stage 
                
            }
            else
            {
                bool_unique_supplier=true;//if no SupplierID on the given globe id,then setting value to true to set an exception value below.
            }
        }
        
        if(map_val_spendata_publish!=NULL)
        {
            if(map_val_spendata_publish.get(spend_history_stage.date__c)!=NULL)//checking if with same date and globe id any record in spend history publish object.
            {
                bool_groupID_exists=true;//setting value to true to set an exception value below.
            }
            
        }
        
        if(spend_history_stage.Date__c==NULL)
        {
            bool_date_mandatory=true;//checking date value for null and setting the value to true to set an exception value below.
        }
        if(spend_history_stage.Spend_Period__c==NULL)
        {
            bool_spend_period_mandatory=true;//checking Spend period value for null and setting the value to true to set an exception value below.
        }
        if(spend_history_stage.Spend_Amount__c==NULL)
        {
            bool_spend_amount_mandatory=true;//checking Spend amount value for null and setting the value to true to set an exception value below.
        }
        if(spend_history_stage.Globe_ID__c==NULL)
        {
            bool_groupID_mandatory=true;//checking globe id value for null and setting the value to true to set an exception value below.
        }
        
        //below assigning all the exception values ,which we met in the validation and storing in the exception field.
        if(bool_unique_supplier==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhisnosup;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhisnosup;
            }   
        }

        
        if(bool_groupID_exists==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhiserrormssgalert;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhiserrormssgalert;
            }   
        }

        
        if(bool_date_mandatory==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhisdatemand;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhisdatemand;
            }   
        }

        
        if(bool_spend_period_mandatory==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhisspendperiodmand;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhisspendperiodmand;
            }   
        }

        
        if(bool_spend_amount_mandatory==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhisspendamtmand;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhisspendamtmand;
            }
        }

        
        if(bool_groupID_mandatory==true)
        {
            if(spend_history_stage.Exception__c==NULL)
            {
                spend_history_stage.Exception__c=Label.BSureS_spendhisglobeidmand;
            }
            else
            {
                spend_history_stage.Exception__c+=','+Label.BSureS_spendhisglobeidmand;
            }
        }
    
       if(bool_unique_supplier==true || bool_groupID_exists==true || bool_date_mandatory==true || bool_spend_period_mandatory==true || bool_spend_amount_mandatory==true || bool_groupID_mandatory==true)
       {
        spend_history_stage.Status__c='Exception';
        spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
       }
    }
        
    }   
}