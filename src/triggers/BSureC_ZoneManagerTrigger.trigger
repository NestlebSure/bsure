trigger BSureC_ZoneManagerTrigger on BSureC_Zone_Manager__c (before insert, before update) {

	Set<string> Zone = new set<string>();
	Set<id> Zonemanager = new set<id>();	
	Set<ID> ZoneID = new set<ID>();
	if(trigger.isBefore)
	{
		for(BSureC_Zone_Manager__c Zm:trigger.new)
		{
			zone.add(Zm.Zone_Name__c);
			ZoneID.add(Zm.Id);			
			Zonemanager.add(Zm.Zone_Manager__c);
		}
		if(BSureC_Zone_Manager__c.SObjectType.getDescribe().isQueryable()){
			List<BSureC_Zone_Manager__c> ListSubmngr = [SELECT name,Zone_Name__c,Zone_Manager__c
														from BSureC_Zone_Manager__c 
														where Zone_Name__c =:Zone and id!=:ZoneID];													
			
			system.debug('List***'+ListSubmngr);
			for(BSureC_Zone_Manager__c Lstsubmngr:ListSubmngr)
			{
				for(BSureC_Zone_Manager__c Zmnewtrigg:trigger.new){
					if(Lstsubmngr.Zone_Name__c == Zmnewtrigg.Zone_Name__c && Lstsubmngr.Zone_Manager__c == Zmnewtrigg.Zone_Manager__c )
					Zmnewtrigg.addError('Record already exists with this name');
				}
			}
		}													
	}
}

/*
	for(BSureC_Zone_Manager__c BSureCZoneMngr:trigger.new){
	if(trigger.isBefore && trigger.isInsert)
	{
		Integer DuplicateBsureCZoneMngr = [SELECT count() from BSureC_Zone_Manager__c where Zone__c=:BSureCZoneMngr.Zone__c and Zone_Manager__c=:BSureCZoneMngr.Zone_Manager__c];
		if(DuplicateBsureCZoneMngr>0)
		BSureCZoneMngr.adderror('Record already exists with this name');
	}
	if(trigger.isBefore && trigger.isUpdate)
	{
		Integer DuplicateBsureCZoneMngr = [SELECT count() from BSureC_Zone_Manager__c where Zone__c=:BSureCZoneMngr.Zone__c and Zone_Manager__c=:BSureCZoneMngr.Zone_Manager__c and id!=:BSureCZoneMngr.Id];
		if(DuplicateBsureCZoneMngr>0)
		BSureCZoneMngr.adderror('Record already exists with this name');
	}
}
*/