/***********************************************************************************************
* Controller Name   : BSureC_BackUpAnalystsTrigger
* Date              : 29/11/2012
* Author            : Prashanth K
* Purpose           : Trigger to update Backup analysts names
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 29/11/2012            Prashanth K         Initial Version
* 21/06/2013            Sridhar Bonagiri    Added 'RunTriggers' condition
* 26/06/2013			Aditya V			Added Custom Setting Flag to send Customer Updates' Emails
* 02/07/2013            kishorekumar A      Planned Review Date Update on Customer Information Edit
* 26/03/2014			satish.c			Modified for backup analysts assignment changes
**************************************************************************************************/
trigger BSureC_BackUpAnalystsTrigger on BSureC_Customer_Basic_Info__c (before update, after update) {
    
    List<string> backupAnalysts = new List<string>(); 
    string strNames=''; 
    string strBackupAnalystNames='';
    string strZoneId='';
    string strSubZoneId='';
    string strCountryId='';
    //public String[] strBackupManagersProfiles=new String[] {'BSureC_Analyst','BSureC_Country Manager','BSureC_Sub Zone Manager','BSureC_Zone Manager'};
    public string strAnalystrole{get;set;}
    public string strCountryManagerRole{get;set;}
    public string strSubZoneManagerRole{get;set;}
    public string strZoneManagerrole{get;set;}    
    public list<string> lstMultipleBuyerRoles = new list<String>();
    
    List<BSureC_Customer_Basic_Info__Share> lstCustomerShare = new List<BSureC_Customer_Basic_Info__Share >();
    list<BSureC_Customer_Basic_Info__Share> lstdelete = new list<BSureC_Customer_Basic_Info__Share>();
    
    public integer intCount = 0;
    //public set<Id> strEmailCSV {get; set;}
    public string strSubject {get; set;}
    public string strEmailBody {get; set;}
    public set<Id> setEmailRcpts {get; set;}
    public list<Id> commasplitIds{get;set;}
    public list<BSureC_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureC_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureC_Country_Manager__c> lstCountryManager{get;set;}
    list<Event> lstEventdelete{get;set;}
    public Event EventObj{get;set;}
    public String str_analyst_new{get;set;}
    public String str_analyst_old{get;set;}
    public Date dt_planneddate_new{get;set;}
    public Date dt_planneddate_old{get;set;}
    public String str_customer_Id{get;set;}
    public String str_customer_name{get;set;}
    
    string strrunTriggers=''; 
    string strSendUpdates = '';
    
    string strCustomerId='';
    string strAnalystIDold ='';
    string strAnalystIDnew = '';
    string strPlannedReviewDate ='';
    string strCustomerName = '';
    
    strrunTriggers = BSureC_CommonUtil.getConfigurationValues('BSureC_Run_Triggers').get(0);
    strSendUpdates = BSureC_CommonUtil.getConfigurationValues('BSureC_SendCustInfoUpdates').get(0);
    
    if (strrunTriggers.equalsIgnoreCase('TRUE'))
    {
    	if(trigger.isBefore) 
	    { 
	        if(BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole') != null )
	        {
	            strAnalystrole=BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole').get(0);
	        }
	        strZoneManagerrole=BSureC_CommonUtil.getConfigurationValues('BSureC_ZoneManager').get(0);
	        strSubZoneManagerRole=BSureC_CommonUtil.getConfigurationValues('BSureC_SubZoneManager').get(0);
	        strCountryManagerRole=BSureC_CommonUtil.getConfigurationValues('BSureC_CountryManager').get(0);
	        
	        lstMultipleBuyerRoles.add(strAnalystrole);
	        lstMultipleBuyerRoles.add(strZoneManagerrole);
	        lstMultipleBuyerRoles.add(strSubZoneManagerRole);
	        //system.debug('----strCountryManagerRole--'+strCountryManagerRole);
	    	//system.debug('----lstMultipleBuyerRoles--'+lstMultipleBuyerRoles);
	        if(strCountryManagerRole != null && strCountryManagerRole != '')
	        {
	             lstMultipleBuyerRoles.add(strCountryManagerRole);
	        }
	       
	        //for(BSureC_Customer_Basic_Info__c CusSynch:trigger.new)
	        //{
	         	/*if(CusSynch.Planned_Review_Date__c != null && CusSynch.Next_Review_Date__c == null)
	            {
	              	CusSynch.Next_Review_Date__c=CusSynch.Planned_Review_Date__c;
	              	//system.debug('NextReviewDateafter.....'+CusSynch.Next_Review_Date__c); 
	            }           
	            else if(CusSynch.Next_Review_Date__c != null )
	            {
	              	CusSynch.Planned_Review_Date__c=CusSynch.Next_Review_Date__c;
	              	//system.debug('PlannedReviewDateafter.....'+CusSynch.Planned_Review_Date__c);  
	            }*/
	       	//}
	       	
	        for(BSureC_Customer_Basic_Info__c suppObjTest : trigger.old)
	        {
	            strCustomerId = suppObjTest.Id;
	            if(suppObjTest.Analyst__c != null)
	            {
	                strAnalystIDold = suppObjTest.Analyst__c;
	            }   
	        }   
	        if( (strCustomerId != null && strCustomerId !='' ) && (strAnalystIDold != null && strAnalystIDold != '') && Event.SObjectType.getDescribe().isQueryable() )
	        {
	            lstEventdelete = new list<Event>([select id from Event where OwnerId =: strAnalystIDold and WhatId =: strCustomerId ]);
	            if(!lstEventdelete.isEmpty()) database.delete(lstEventdelete,false);  
	        }
	        /*map<id,User> mapObj=new map<id,User>();  
	        if(User.SObjectType.getDescribe().isQueryable())
	        mapObj = new map<id,User>([select id,Name from User where IsActive = true and (UserRole.Name IN:lstMultipleBuyerRoles Or profile.Name='BSureC_CreditAdmin')]);
	        system.debug('test=====');*/
	        //for loop Entry - BA Name update              
	        for(BSureC_Customer_Basic_Info__c custObj:trigger.new)
	        {
	        	if(custObj.Planned_Review_Date__c != null)
	            {
	            	custObj.Next_Review_Date__c = custObj.Planned_Review_Date__c;
	            }   
	            
	        	strNames = '';
	        	backupAnalysts = new List<string>();
	            /*if(custObj.Backup_Analysts__c!= null && custObj.Backup_Analysts__c.contains(',')) 
	            {
	                backupAnalysts = custObj.Backup_Analysts__c.split(',');
	            }
	            
	            if(backupAnalysts.size() > 0)
	            {
	            	system.debug(custObj.Backup_Analysts__c+'backupAnalysts&&&&&&&'+backupAnalysts);
	            	for(string sObj:backupAnalysts)
	                {
	                	if(sObj !=null && sObj !='' && sObj.length() == 21)
			        		sObj = sObj.left(sObj.length()-3);
			        	else
			        		sObj = sObj;
			        	
	                    if(mapObj != null && mapObj.get(sObj) != null)
	                    {
	                        strNames += mapObj.get(sObj).Name + ';'; 
	                    }
	                    if(sObj != null && sObj != '')
	                    {
	                    	BSureC_Customer_Basic_Info__Share  shareObj = new BSureC_Customer_Basic_Info__Share();
	                        shareObj.AccessLevel = 'Edit';
	                        shareObj.ParentId = custObj.Id;
	                        shareObj.UserOrGroupId = sObj;
	                        lstCustomerShare.add(shareObj);
	                    }
	            	}
	                if(strNames.length()>0)
	                {
	                    strNames = strNames.left(strNames.length()-1); 
	                }
	            }
	            else
	            {
	                if(custObj.Backup_Analysts__c != null && mapObj.get(custObj.Backup_Analysts__c) != null)
	                {
	                	system.debug(custObj.Backup_Analysts__c+'backupAnalysts********'+backupAnalysts);
	                    strNames = mapObj.get(custObj.Backup_Analysts__c).Name;
	                    if(custObj.Backup_Analysts__c != null && custObj.Backup_Analysts__c != '')
	                    {
	                    	strBackupAnalystNames=custObj.Backup_Analysts__c;
	                        BSureC_Customer_Basic_Info__Share shareObj = new BSureC_Customer_Basic_Info__Share();
	                        shareObj.AccessLevel = 'Edit';
	                       	shareObj.ParentId = custObj.Id;
	                        shareObj.UserOrGroupId = custObj.Backup_Analysts__c;
	                        lstCustomerShare.add(shareObj);
	                    }
	                }
	            }
	            //Assigning Names to Backup_Analyst_Names__c
	            system.debug('strNames****'+strNames);
	            if(strNames.length() > 255)
	            {
	            	strNames = strNames.substring(0, 254);
	            }
	            custObj.Backup_Analyst_Names__c = strNames;*/
	            if(custObj.Planned_Review_Date__c != null)
	            
	            {
	                strPlannedReviewDate = string.valueOf(custObj.Planned_Review_Date__c);
	            }
	            if(custObj.Analyst__c != null)
	            {
	                strAnalystIDnew = custObj.Analyst__c ;
	            }
	            if( custObj.Customer_Name__C != null)
	            {
	                strCustomerName =  custObj.Customer_Name__C;
	            }
	            system.debug(custObj.Backup_Analysts__c+'custObj.Backup_Analyst_Names__c======'+custObj.Backup_Analyst_Names__c);
	        }//for loop Exits - BA Name update
	        if(strBackupAnalystNames != null && BSureC_Customer_Basic_Info__Share.SObjectType.getDescribe().isQueryable()) {
	         	lstdelete = [select Id from BSureC_Customer_Basic_Info__Share 
         					 where UserOrGroupId =: strBackupAnalystNames]; 
	        }
	    }
     	if(!lstdelete.isEmpty()) { 
     		database.delete(lstdelete,false); 
     	}
     	
     	if(!lstCustomerShare.isEmpty())	{
     		if (BSureC_Customer_Basic_Info__Share.SObjectType.getDescribe().isCreateable() || BSureC_Customer_Basic_Info__Share.SObjectType.getDescribe().isUpdateable())
     		database.upsert (lstCustomerShare,false);
     	}
     	
   		if( (strAnalystIDnew != null && strAnalystIDnew != '') && (strPlannedReviewDate != null && strPlannedReviewDate !='') )
	    {
	        EventObj = new Event();
	        EventObj.StartDateTime = Date.valueOf(strPlannedReviewDate);
	        EventObj.EndDateTime =  Date.valueOf(strPlannedReviewDate);
	        EventObj.Subject = 'Create a New Review - ('+strCustomerName +')' ;
	        EventObj.Description  = 'Create a New Review for this Customer : '+ strCustomerName +' ; ';
	        EventObj.Description  += 'Planned Review Date :' + strPlannedReviewDate ;
	        EventObj.OwnerId = strAnalystIDnew;
	        EventObj.WhatId = strCustomerId;
	        if(EventObj != null)
	        {
	        	if (Event.sObjectType.getDescribe().isCreateable())
	            insert EventObj;
	        }
	    }
	    
   		//To send notification to the subcribers about the changes in Customer Information
    	if(Trigger.isAfter || Trigger.isUpdate)
	    {
	    	if(strSendUpdates.equalsIgnoreCase('TRUE') && BSureC_Customer_Basic_Info__c.SObjectType.getDescribe().isQueryable())
	    	{
		        List<BSureC_Customer_Basic_Info__c> lst_cust_info = [SELECT Id,Zone_Name__c,Website_Link__c,Sub_Zone_Name__c,Street_Name_Address_Line_1__c,
		                                                             State__c,State_Province__c,Sole_Sourced__c,Risk_Category__c,Reviewed_By__c,
		                                                             Postal_Code__c,Phone_Number__c,Parent_Customer_Name__c,Next_Review_Analyst_Name__c,
		                                                             NAICS__c,Manager_Name__c,Has_Parent__c,GMC__c,Fiscal_Year_End__c,Fax__c,Facility_s__c,
		                                                             Facility_Expiration__c,F_S__c,Email_address__c,DND_Financial_Information__c,
		                                                             Customer_Region_Name__c,Customer_Name__c,Customer_Group_Id__c,Customer_Detail__c,
		                                                             Customer_Desc__c,Customer_Contact__c,Customer_Category_Name__c,Credit_Limit__c,
		                                                             Credit_Account__c,Country_Name__c,Contact_Name__c,City__c,Bill_to_Account__c,
		                                                             Backup_Analyst_Names__c,Analyst_Name__c,Address_Line_2__c,Owner_Ship__c,
		                                                             Last_Financial_Statement_Received__c,Backup_Analysts__c,Analyst__c,Manager__c,
		                                                             Zone__c,Sub_Zone__c,Country__c,Planned_Review_Date__c
		                                                             FROM BSureC_Customer_Basic_Info__c
		                                                             WHERE Id IN:Trigger.newmap.keyset()];
		        
		        strSubject = 'Customer Information Update for : ' + lst_cust_info.get(0).Customer_Name__c;
		        //strEmailCSV =new set<Id>();
		        strEmailBody= '';       
		            
		        lstZoneManager = new list<BSureC_Zone_Manager__c>();
		        lstSubZoneManager = new list<BSureC_Sub_Zone_Manager__c>();
		        lstCountryManager = new list<BSureC_Country_Manager__c>();
		        for(BSureC_Customer_Basic_Info__c rec_cust_info : lst_cust_info)
		        {
		            setEmailRcpts = new set<Id>();
		            commasplitIds = new list<Id>();
		            Boolean bl_flag = false;
		            str_customer_Id = String.valueOf(rec_cust_info.Id);
		            if(trigger.new[intCount].Customer_Name__c != null)
		            {
		                str_customer_name = trigger.new[intCount].Customer_Name__c;
		            }   
		            
		            if(intCount < trigger.new.size())
		            {
		                strEmailBody += '<table width="800px" border="1">';
		                strEmailBody += '<tr style="border:thin solid;"><td width="34%" height="21px"><b>Field Name</b></td><td width="33%" height="21px"><b>Old Value</b></td><td width="33%" height="21px"><b>New Value</b></td></tr>';
		                if(trigger.new[intCount].Customer_Name__c != trigger.old[intCount].Customer_Name__c && 
		                    (trigger.new[intCount].Customer_Name__c != null && trigger.new[intCount].Customer_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Customer Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Customer_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Customer_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Customer_Category_Name__c != trigger.old[intCount].Customer_Category_Name__c && 
		                        (trigger.new[intCount].Customer_Category_Name__c != null ))
		                {
		                    strEmailBody += '<tr><td height="21px">Category Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Customer_Category_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Customer_Category_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Sole_Sourced__c != trigger.old[intCount].Sole_Sourced__c && 
		                    (trigger.new[intCount].Sole_Sourced__c != null && trigger.new[intCount].Sole_Sourced__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Sole Sourced</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Sole_Sourced__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Sole_Sourced__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Owner_Ship__c != trigger.old[intCount].Owner_Ship__c && 
		                        (trigger.new[intCount].Owner_Ship__c != null && trigger.new[intCount].Owner_Ship__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Owner Ship</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Owner_Ship__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Owner_Ship__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                } 
		                if(trigger.new[intCount].F_S__c != trigger.old[intCount].F_S__c && 
		                        (trigger.new[intCount].F_S__c != null && trigger.new[intCount].F_S__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">F/S</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].F_S__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].F_S__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		            
		                if(trigger.new[intCount].DND_Financial_Information__c != trigger.old[intCount].DND_Financial_Information__c && 
		                        (trigger.new[intCount].DND_Financial_Information__c != null && trigger.new[intCount].DND_Financial_Information__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px"> Financial Information</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].DND_Financial_Information__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].DND_Financial_Information__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                    
		                if(trigger.new[intCount].Fiscal_Year_End__c != trigger.old[intCount].Fiscal_Year_End__c && 
		                        (trigger.new[intCount].Fiscal_Year_End__c != null ))
		                {
		                    strEmailBody += '<tr><td height="21px">Fiscal Year End</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Fiscal_Year_End__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Fiscal_Year_End__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }   
		                if(trigger.new[intCount].State_Province__c != trigger.old[intCount].State_Province__c && 
		                        (trigger.new[intCount].State_Province__c != null ))
		                {
		                    strEmailBody += '<tr><td height="21px">State</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].State_Province__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].State_Province__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }   
		                if(trigger.new[intCount].Last_Financial_Statement_Received__c != trigger.old[intCount].Last_Financial_Statement_Received__c && 
		                        (trigger.new[intCount].Last_Financial_Statement_Received__c != null ))
		                {
		                    strEmailBody += '<tr><td height="21px">Last Financial Statement Received</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Last_Financial_Statement_Received__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Last_Financial_Statement_Received__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }       
		                if(trigger.new[intCount].Has_Parent__c != trigger.old[intCount].Has_Parent__c && 
		                        (trigger.new[intCount].Has_Parent__c != null ))
		                {
		                    strEmailBody += '<tr><td height="21px">Has Parent</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Has_Parent__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Has_Parent__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                } 
		                if(trigger.new[intCount].Parent_Customer_Name__c != trigger.old[intCount].Parent_Customer_Name__c && 
		                    (trigger.new[intCount].Parent_Customer_Name__c != null && trigger.new[intCount].Parent_Customer_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Parent Customer Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Parent_Customer_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Parent_Customer_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }  
		                if(trigger.new[intCount].Contact_Name__c != trigger.old[intCount].Contact_Name__c && 
		                        (trigger.new[intCount].Contact_Name__c != null && trigger.new[intCount].Contact_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Customer Contact</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Contact_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Contact_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }   
		                if(trigger.new[intCount].Email_address__c != trigger.old[intCount].Email_address__c && 
		                        (trigger.new[intCount].Email_address__c != null && trigger.new[intCount].Email_address__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Email Address</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Email_address__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Email_address__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }   
		                
		                if(trigger.new[intCount].Phone_Number__c != trigger.old[intCount].Phone_Number__c && 
		                        (trigger.new[intCount].Phone_Number__c != null && trigger.new[intCount].Phone_Number__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Postal Code</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Phone_Number__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Phone_Number__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                } 
		                if(trigger.new[intCount].Fax__c != trigger.old[intCount].Fax__c && 
		                        (trigger.new[intCount].Fax__c != null && trigger.new[intCount].Fax__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Fax</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Fax__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Fax__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Website_Link__c != trigger.old[intCount].Website_Link__c && 
		                    (trigger.new[intCount].Website_Link__c != null && trigger.new[intCount].Website_Link__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Website Link</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Website_Link__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Website_Link__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Zone_Name__c != trigger.old[intCount].Zone_Name__c && 
		                        (trigger.new[intCount].Zone_Name__c != null && trigger.new[intCount].Zone_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Zone Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Zone_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Zone_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Sub_Zone_Name__c != trigger.old[intCount].Sub_Zone_Name__c && 
		                        (trigger.new[intCount].Sub_Zone_Name__c != null && trigger.new[intCount].Sub_Zone_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Sub Zone Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Sub_Zone_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Sub_Zone_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Country_Name__c != trigger.old[intCount].Country_Name__c && 
		                        (trigger.new[intCount].Country_Name__c != null && trigger.new[intCount].Country_Name__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Country Name</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Country_Name__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Country_Name__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Street_Name_Address_Line_1__c != trigger.old[intCount].Street_Name_Address_Line_1__c && 
		                        (trigger.new[intCount].Street_Name_Address_Line_1__c != null && trigger.new[intCount].Street_Name_Address_Line_1__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Street Name/Address Line 1</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Street_Name_Address_Line_1__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Street_Name_Address_Line_1__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Address_Line_2__c != trigger.old[intCount].Address_Line_2__c && 
		                        (trigger.new[intCount].Address_Line_2__c != null && trigger.new[intCount].Address_Line_2__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Address Line 2</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Address_Line_2__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Address_Line_2__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }
		                if(trigger.new[intCount].Postal_Code__c != trigger.old[intCount].Postal_Code__c && 
		                        (trigger.new[intCount].Postal_Code__c != null && trigger.new[intCount].Postal_Code__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">Postal Code</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].Postal_Code__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].Postal_Code__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }  
		                if(trigger.new[intCount].City__c != trigger.old[intCount].City__c && 
		                        (trigger.new[intCount].City__c != null && trigger.new[intCount].City__c != ''))
		                {
		                    strEmailBody += '<tr><td height="21px">City</td><td height="21px">';
		                    strEmailBody += trigger.old[intCount].City__c;
		                    strEmailBody += '</td><td height="21px">';
		                    strEmailBody += trigger.new[intCount].City__c;
		                    strEmailBody += '</td></tr>';
		                    bl_flag = true;
		                }  
		                    
		                strEmailBody += '</table>';  
		                
		                if(trigger.new[intCount].Planned_Review_Date__c != null)
		                {
		                    dt_planneddate_new = trigger.new[intCount].Planned_Review_Date__c;
		                    dt_planneddate_old = trigger.old[intCount].Planned_Review_Date__c;
		                }
		                if(trigger.new[intCount].Analyst__c != null )
		                {
		                    str_analyst_new = trigger.new[intCount].Analyst__c;
		                    str_analyst_old = trigger.old[intCount].Analyst__c;
		                }
	             	}
		            
		            if(bl_flag)
		            {
		                if(rec_cust_info.Analyst__c != null)
		                {
		                    setEmailRcpts.add(rec_cust_info.Analyst__c);
		                }
		                if(rec_cust_info.Manager__c != null)
		                {
		                    setEmailRcpts.add(rec_cust_info.Manager__c);
		                }
		                if(rec_cust_info.Backup_Analysts__c != null)
		                {
		                	if(rec_cust_info.Backup_Analysts__c.contains(','))
		                	{
		                		commasplitIds = rec_cust_info.Backup_Analysts__c.split(',');
		                	}
		                	else
		                	{
		                		commasplitIds.add(rec_cust_info.Backup_Analysts__c);	
		                	}
		                    
		                    setEmailRcpts.addAll(commasplitIds);
		                }
		                
		                if(rec_cust_info.Zone__c != null)
		                {
		                    strZoneId= rec_cust_info.Zone__c;                    
		                }
		               
		                if(rec_cust_info.Sub_Zone__c != null)
		                {
		                    strSubZoneId=rec_cust_info.Sub_Zone__c;
		                }
		                
		                if(rec_cust_info.Country__c != null)
		                {
		                    strCountryId=rec_cust_info.Country__c;
		                    
		                }                
	             	}
		        }	//Close For Loop
		        
		        /*if( Trigger.isBefore )
		        {
		            if( Trigger.isUpdate )
		            {
		                if((str_analyst_new != null && str_analyst_new != str_analyst_old) ||
		                   (dt_planneddate_new != null && dt_planneddate_new != dt_planneddate_old)
		                   )
		                { 
		                    if(str_analyst_new != null)
		                    {
		                        lstEventdelete = new list<Event>([select id from Event where OwnerId =: str_analyst_new and WhatId =: str_customer_Id ]);
		                        
		                        if(!lstEventdelete.isEmpty()){ database.delete(lstEventdelete,false);}
		                    }
		                    if(str_analyst_new != null && dt_planneddate_new != null)
		                    {
		                        EventObj = new Event();
		                        EventObj.StartDateTime = Date.valueOf(String.valueOf(dt_planneddate_new));
		                        EventObj.EndDateTime =  Date.valueOf(String.valueOf(dt_planneddate_new));
		                        EventObj.Subject = 'Create a New Review ';
		                        EventObj.Description  = 'Create a New Review for this Customer : '+ str_customer_name +' ; ';
		                        EventObj.Description  += 'Planned Review Date :' + dt_planneddate_new ;
		                        EventObj.OwnerId = str_analyst_new;
		                        EventObj.WhatId = str_customer_Id;
		                        if(EventObj != null)
		                        {
		                            insert EventObj;
		                        }
		                    }   
	                	}
		            }
		        }*/
		        
		        if(strZoneId!=null && BSureC_Zone_Manager__c.SObjectType.getDescribe().isQueryable())
		        lstZoneManager = [select Zone_Manager__c from BSureC_Zone_Manager__c where Zone__c =: strZoneId ];
		        if(strSubZoneId !=null && BSureC_Sub_Zone_Manager__c.SObjectType.getDescribe().isQueryable())
		        lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureC_Sub_Zone_Manager__c where Sub_Zone__c =: strSubZoneId];
		        if(strCountryId!=null && BSureC_Country_Manager__c.SObjectType.getDescribe().isQueryable())
		        lstCountryManager = [select Country_Manager__c from BSureC_Country_Manager__c where Country__c =: strCountryId];
		        
		        if(lstZoneManager != null && lstZoneManager.size() > 0)
		        {
		            for(BSureC_Zone_Manager__c objZM: lstZoneManager)
		            {
		                setEmailRcpts.add(objZM.Zone_Manager__c);
		            }
		        }
		        if(lstSubZoneManager != null && lstSubZoneManager.size() > 0)
		        {
		            for(BSureC_Sub_Zone_Manager__c objSZM :lstSubZoneManager )
		            {
		                setEmailRcpts.add(objSZM.Sub_Zone_Manager__c);
		            }
		        }
		        if(lstCountryManager != null && lstCountryManager.size() > 0)
		        {
		            for(BSureC_Country_Manager__c objCM : lstCountryManager)
		            {
		                setEmailRcpts.add(objCM.Country_Manager__c);
		            }
		        }   
		        /*
		        if(setEmailRcpts != null && setEmailRcpts.size() > 0)
		        {
		            list<User> objUserEmails = [select Id, Email, Name From User Where Id IN: setEmailRcpts];
		            for(User objEachUser : objUserEmails)
		            {
		                strEmailCSV.add(objEachUser.Id);
		            }
		        }    */      
		        BSureC_CommonUtil.SendEmailToQueue(setEmailRcpts, strSubject,strEmailBody,'High',system.today(),true);
		    }
	    }
	}
}