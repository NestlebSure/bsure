/***********************************************************************************************
* Trigger Name      : BSureC_AssignNonCredit_User
* Date              : 21/02/2013
* Author            : suresh v
* Purpose           : Trigger to Buyers can view Assigned Customer Information.
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 21/02/2013            KishoreKumar A         Initial Version
**************************************************************************************************/

trigger BSureC_AssignNonCredit_User on BSureC_Assigned_Buyers__c (before delete, before insert, before update) 
{
	     
	list<BSureC_Customer_Basic_Info__Share> lstBuyershare = new list<BSureC_Customer_Basic_Info__Share>();	
	list<BSureC_Customer_Basic_Info__Share> lstShareDelete =new list<BSureC_Customer_Basic_Info__Share>();
	//list<BSureC_NonCreit_Users__c> lstCustmStng = BSureC_NonCreit_Users__c.getAll().values();	
	String strBuyerShare =BSureC_CommonUtil.getConfigurationValues('BSureC_BuyerShare').get(0);
	String strUserId='';
	String strCustmerId='';
	system.debug('strBuyerShare========='+strBuyerShare);
	//if(lstCustmStng.get(0).Name == 'False')
	if(strBuyerShare.equalsIgnoreCase('FALSE'))
	{  
		
        	if(Trigger.isInsert || Trigger.isUpdate )
			{	
				system.debug('after insert===========');
				for(BSureC_Assigned_Buyers__c bObj:trigger.new)
				{
					BSureC_Customer_Basic_Info__Share shareObj = new  BSureC_Customer_Basic_Info__Share();
					shareObj.AccessLevel = 'Edit';
					shareObj.ParentId = bObj.Customer_Id__c;
					shareObj.UserOrGroupId = bObj.User_Id__c;
				    lstBuyershare.add(shareObj);
				}
			}
						
			if(Trigger.isDelete)
			{
				system.debug('after delete===========');
				
				for(BSureC_Assigned_Buyers__c bObj:trigger.old)
				{
					if(bObj.User_Id__c != null)
					{
						strUserId=bObj.User_Id__c;
						strCustmerId=bObj.Customer_Id__c;											
					}	
				}
				if(strUserId!=null && strCustmerId!=null && BSureC_Customer_Basic_Info__Share.SObjectType.getDescribe().isQueryable())
				lstShareDelete = new list<BSureC_Customer_Basic_Info__Share>([select id from BSureC_Customer_Basic_Info__Share where UserOrGroupId =:strUserId and ParentId =: strCustmerId]);
				
			}
			if(!lstShareDelete.isEmpty())
			Database.delete(lstShareDelete,false);
				
			system.debug('lstBuyershare==========='+lstBuyershare);
			if(!lstBuyershare.IsEmpty() ){
			 if(Schema.Sobjecttype.BSureC_Customer_Basic_Info__Share.isCreateable() || BSureC_Customer_Basic_Info__Share.SObjectType.getDescribe().isUpdateable())
				Database.upsert(lstBuyershare,false); }     
        
	}
}