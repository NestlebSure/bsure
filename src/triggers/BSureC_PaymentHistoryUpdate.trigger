/************************************************************************************************       
Controller Name         : BSureC_PaymentHistoryUpdate       
Date                    : 12/10/2012        
Author                  : Santhosh Palla      
Purpose                 : To validate the stage Payment History record when manually edited.       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/15/2012      Santhosh Palla            Initial Version
**************************************************************************************************/
trigger BSureC_PaymentHistoryUpdate on BSureC_Payment_History_Stage__c (before update) 
{
    Map<String,Id> map_payment_history_stage = new Map<String,Id>();//holds the
    
    if(BSureC_Customer_Basic_Info__c.SObjectType.getDescribe().isQueryable()){
	    List<BSureC_Customer_Basic_Info__c> lst_customer_info = [SELECT Id,Customer_Group_Id__c,Credit_Account__c FROM BSureC_Customer_Basic_Info__c WHERE id != null];
	    
	    for(BSureC_Customer_Basic_Info__c reccustinfo : lst_customer_info)
	    {
	        map_payment_history_stage.put(reccustinfo.Credit_Account__c,reccustinfo.Id);
	    }
    }
    for(BSureC_Payment_History_Stage__c payment_history_stage:trigger.new)
    {
    	if(payment_history_stage.Publish_Flag__c != true)
    	{
	        if(payment_history_stage.Credit_Account__c==NULL)
	        {
	            if(payment_history_stage.Exception__c==NULL)
	            {
	                payment_history_stage.Exception__c='Credit Account is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	            else
	            {
	                payment_history_stage.Exception__c+=','+'Credit Account is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	        }
	        else
	        {
	        	if(map_payment_history_stage != null && map_payment_history_stage.get( payment_history_stage.Credit_Account__c ) != null )
		        {
		            payment_history_stage.Customer_Basic_Info_Id__c = String.valueOf(map_payment_history_stage.get( payment_history_stage.Credit_Account__c ));
		            payment_history_stage.Exception__c = NULL;
		            payment_history_stage.Status__c='Initialized';
					payment_history_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
		        }
		        else
		        {
		        	if(payment_history_stage.Exception__c==NULL)
		        	{
		        		payment_history_stage.Exception__c='Credit Account is not Exists';
		            	payment_history_stage.Status__c='Exception';
		            	payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		        	}
		            else
		            {
		                payment_history_stage.Exception__c+=','+'Credit Account is not Exists';
		                payment_history_stage.Status__c='Exception'; 
		                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		            } 
		        }
	           
	        }
	        
	        if(payment_history_stage.High_Credit__c == NULL)
	        {
	            if(payment_history_stage.Exception__c==NULL)
	            {
	                payment_history_stage.Exception__c='High Credit is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	            else
	            {
	                payment_history_stage.Exception__c+=','+'High Credit is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	        }
	        if(payment_history_stage.Total_Due__c == NULL)
	        {
	            if(payment_history_stage.Exception__c==NULL)
	            {
	                payment_history_stage.Exception__c='Total Due is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	            else
	            {
	                payment_history_stage.Exception__c+=','+'Total Due is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	        }
	        if( payment_history_stage.Past_Due__c == NULL )
	        {
	            if(payment_history_stage.Exception__c==NULL)
	            {
	                payment_history_stage.Exception__c='Past Due is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	            else
	            {
	                payment_history_stage.Exception__c+=','+'Past Due is Mandatory';
	                payment_history_stage.Status__c='Exception';
	                payment_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            }
	        }
	    }
   }
}