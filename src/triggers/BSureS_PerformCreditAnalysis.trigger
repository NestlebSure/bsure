/***********************************************************************************************
*       Trigger Name : BSureS_PerformCreditAnalysis
*       Date                    : 25/11/2012 
*       Author                  : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Developer                     Reason
*       --------------------      -------------------    -------------------------
*       25/11/2012               Kishorekumar A                Initial Version
*       18/03/2012               KishoreKumar A                'Not Started' Review Status is Not there in CA
*       11/07/2013               Sridhar Bonagiri              Added 'BSureS_Run_Triggers' condition.
*       01/08/2013               Kishorekumar A                InActive related changes 
**************************************************************************************************/
trigger BSureS_PerformCreditAnalysis on BSureS_Credit_Analysis__c (after Insert,after Update,after delete) {
    set<Id> Supplierid = new set<Id>();
    public Integer count=0;
    public boolean isAsgnChanged = false;
    list<BSureS_Basic_Info__c> ListSupplier= new list<BSureS_Basic_Info__c>();
    public list<BSureS_Basic_Info__c> lstgetParentSID{get;set;}
    public Task lst_create_task{get;set;}
    string strParentSupplId = '';
    
    public Task lst_create_task_RSStarted{get;set;}
    public list<Task> lstTaskRSStarted = new list<Task>(); 
    public list<Task> lsttskUpdate{get;set;}
    public list<Task> deletePendingATask{get;set;}
    public list<String> lstSuppManagers{get;set;}
    public list<String> lstSuppManagersPA{get;set;}
    public list<String> lstCAId{get;set;}
    public list<Task> UpdateManagerS_Completed{get;set;}
    map<id,BSureS_Basic_Info__c> mapSupplier = new map<id,BSureS_Basic_Info__c>();
    
    lstCAId = NEW List<String>();
    lstSuppManagersPA = new List<String>();
    lstSuppManagers = new List<String>();
    UpdateManagerS_Completed = new list<Task>();
    public list<Task> lsttaskupdate{get;set;}
    string strrunTriggers='';
    lsttaskupdate = new list<Task>();
    lstgetParentSID = new  list<BSureS_Basic_Info__c>();
    if(!BSureS_CommonUtil.getConfigurationValues('BSureS_Run_Triggers').isEmpty())     
        strrunTriggers = BSureS_CommonUtil.getConfigurationValues('BSureS_Run_Triggers').get(0);
    
    if (strrunTriggers.equalsIgnoreCase('TRUE'))
    {
        if(Trigger.isInsert || Trigger.isUpdate){
            for(BSureS_Credit_Analysis__c caObj:Trigger.New)
            {
                if(caObj.Id != null)
                {
                    lstCAId.add(caObj.Id);
                }
                if(caObj.Supplier_ID__c != null)
                {
                    Supplierid.add(caObj.Supplier_ID__c);
                }
            } 
            //map<id,BSureS_Basic_Info__c> mapSupplier = new map<id,BSureS_Basic_Info__c>([select id,Spend__c,Parent_Supplier_ID__c,Parent_Supplier_ID__r.Rating__c,Analyst__c,Manager__c,Supplier_Name__c,Reviewed_by__c from 
            //   BSureS_Basic_Info__c where id IN: Supplierid ]);
             mapSupplier = new map<id,BSureS_Basic_Info__c>([select id,Parent_Supplier_ID__c,Parent_Supplier_ID__r.Rating__c,Analyst__c,
                                                        Manager__c,Supplier_Name__c,Reviewed_by__c,Parent_Company_s_Rating__c,Rating__c,Spend_End_Date__c,Review_Status__c,Spend__c,
                                                        Risk_Level__c,Next_Review_Date__c,Review_Complete_Date__c,Spend_Start_Date__c,Latest_Rating__c,Latest_Expected_Review_End_Date__c, 
                                                        Latest_Reviewed_By__c,Latest_Review_Start_Date__c,Latest_Risk_Level__c, Latest_Spend__c from //Added to add the Latest Review info for Risk
                                                        BSureS_Basic_Info__c where id IN: Supplierid ]);
            //system.debug('mapSupplier=========='+mapSupplier);
            for(BSureS_Credit_Analysis__c CAParentObj:Trigger.new)
            {
                if(CAParentObj.Review_Status__c == 'Completed')
                {
                    if(mapSupplier.get(CAParentObj.Supplier_ID__c).Parent_Supplier_ID__c != null)
                    {
                        strParentSupplId = mapSupplier.get(CAParentObj.Supplier_ID__c).Parent_Supplier_ID__c;
                    }
                }  
                if(CAParentObj.Review_Status__c == 'Completed')
                {
                    if(mapSupplier.get(CAParentObj.Supplier_ID__c).Manager__c != null && UserInfo.getUserId() == mapSupplier.get(CAParentObj.Supplier_ID__c).Manager__c  )
                    {
                        lstSuppManagers.add(mapSupplier.get(CAParentObj.Supplier_ID__c).Manager__c);
                    }
                } 
                if(CAParentObj.Review_Status__c == 'Pending Approval')
                {
                    if((mapSupplier != null && mapSupplier.size() > 0) && mapSupplier.get(CAParentObj.Supplier_ID__c).Manager__c != null)
                    {
                        lstSuppManagersPA.add(mapSupplier.get(CAParentObj.Supplier_ID__c).Manager__c);
                    }
                }    
            }
            if( (lstSuppManagersPA != null && lstSuppManagersPA.size() > 0) && (lstCAId != null && lstCAId.size() >0))
            {
                deletePendingATask = new list<Task>([select id,OwnerId from Task where OwnerId IN : lstSuppManagersPA and whatId IN : lstCAId and Status = 'Not Started' ]);
            }
            if( (lstSuppManagers != null && lstSuppManagers.size() > 0) && (lstCAId != null && lstCAId.size() >0) )
            {
                lsttskUpdate = new list<Task>([select id,OwnerId from Task where OwnerId IN : lstSuppManagers and whatId IN : lstCAId and Status = 'Not Started' ] );
                if(lsttskUpdate != null && lsttskUpdate.size() > 0)
                {
                    for(Task t : lsttskUpdate)
                    {
                        t.Status = 'Completed';
                        UpdateManagerS_Completed.add(t);
                    }
                    try{
                        update UpdateManagerS_Completed;
                    }catch(Exception ex){
                        
                    }   
                }
            }
            
            if(strParentSupplId != null && strParentSupplId != '')
            {
                lstgetParentSID = new  list<BSureS_Basic_Info__c>([select Rating__c from BSureS_Basic_Info__c where id =: strParentSupplId]);
            }
        
        }    
    if(trigger.isinsert)
    { 
        //lsttskUpdate = new  list<Task>();
        for(BSureS_Credit_Analysis__c sObj:Trigger.new)
        {
            if(sObj.Review_Status__c == 'Completed') 
            {
                if(sObj.Spend__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Spend__c = sObj.Spend__c;
                }
                //system.debug('*******'+mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c);
                //system.debug('parent record rating===='+mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__r.Rating__c);
                //mapSupplier .get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__r.Rating__c;
                
                if(mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c != null)
                {
                    //BSureS_Basic_Info__c getParentSID = [select Rating__c from BSureS_Basic_Info__c where id =: mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c];
                    //if(getParentSID.Rating__c == null || getParentSID.Rating__c =='')
                    //{
                    //    getParentSID.Rating__c = 'N/A';
                    //}
                    //mapSupplier .get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = getParentSID.Rating__c;
                    if(lstgetParentSID != null && lstgetParentSID.size() > 0)
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = lstgetParentSID.get(0).Rating__c;
                    }
                    else
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = 'N/A';
                    }
                                        
                }
                if(sObj.Rating__c != null)
                { 
                    mapSupplier.get(sObj.Supplier_ID__c).Rating__c = sObj.Rating__c;
                }
                if(sObj.Risk_Level__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Risk_Level__c = sObj.Risk_Level__c;
                }
                mapSupplier.get(sObj.Supplier_ID__c).Reviewed_by__c = UserInfo.getName(); 
                
                if(sObj.Next_Review_Date__c != null)
                {
                    //system.debug('sObj.Next_Review_Date__c====='+sObj.Next_Review_Date__c);
                    mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c;
                    /*if(sObj.Rating__c =='Inactive')
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    }
                    else
                    {
                        mapSupplier .get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c;
                    }*/ 
                }
                else
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    /*
                    if(sObj.Rating__c =='Inactive' && sObj.Next_Review_Date__c == null)
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    }*/
                }
                if(sObj.Review_Complete_Date__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Complete_Date__c = sObj.Review_Complete_Date__c;
                }
                if(sObj.Spend_Start_Date__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Spend_Start_Date__c = sObj.Spend_Start_Date__c;
                }
                if(sObj.Spend_End_Date__c != null)
                {
                    mapSupplier .get(sObj.Supplier_ID__c).Spend_End_Date__c = sObj.Spend_End_Date__c;
                }
                if(sObj.Rating__c =='Inactive' || sObj.Rating__c =='Out of Scope')
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Not Scheduled';
                }
                else
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c =  'Scheduled';
                }   
                /*if(sobj.Rating__c != 'Inactive')
                {
                    BSureS_Rating__c csObj = BSureS_Rating__c.getValues(sobj.Rating__c);
                    mapSupplier .get(sObj.Supplier_ID__c).Next_Review_Date__c = date.today().addMonths(Integer.valueOf(csObj.months__c));
                }
                else 
                {
                    system.debug('sObj.Next_Review_Date__c====='+sObj.Next_Review_Date__c);
                    mapSupplier .get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c;
                }*/
                /*if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null && UserInfo.getUserId() == mapSupplier.get(sObj.Supplier_ID__c).Manager__c )
                {
                    lsttskUpdate = new list<Task>([select id,OwnerId from Task where OwnerId =: mapSupplier.get(sObj.Supplier_ID__c).Manager__c and whatId =: sObj.Id and Status = 'Not Started' ] );
                    if(lsttskUpdate != null && lsttskUpdate.size() > 0){
                        lsttskUpdate.get(0).Status = 'Completed';
                    }
                }*/ 
             }
            else if(sObj.Review_Status__c == 'Rejected')
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Rejected';
            }
            else if(sObj.Review_Status__c == 'Scheduled')
            {
                //else if(sObj.Review_Status__c == 'Not Started')
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Scheduled';
            }
            else if(sObj.Review_Status__c == 'Started' && sObj.Spend__c == null) 
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Started';
                
                lst_create_task_RSStarted = new Task();
                if(mapSupplier.get(sObj.Supplier_ID__c).Analyst__c != null)
                {
                    lst_create_task_RSStarted.OwnerId = mapSupplier.get(sObj.Supplier_ID__c).Analyst__c;
                    lst_create_task_RSStarted.Subject = 'Review has started and not been completed yet - ('+mapSupplier.get(sObj.Supplier_ID__c).Supplier_Name__c +')';
                    //lst_create_task_RSStarted.Subject = 'call';
                    lst_create_task_RSStarted.ActivityDate = system.today();
                    lst_create_task_RSStarted.Status = 'Not Started';
                    lst_create_task_RSStarted.whatId =  sObj.Id;
                }
                lstTaskRSStarted.add(lst_create_task_RSStarted);
            }
            else if(sObj.Review_Status__c == 'Started' && sObj.Spend__c != null) 
            {
                mapSupplier .get(sObj.Supplier_ID__c).Review_Status__c = 'Started';
            }
            else if(sObj.Review_Status__c == 'Pending Approval')
            {
                /*if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null)
                {
                    deletePendingATask = new list<Task>([select id,OwnerId from Task where OwnerId =: mapSupplier.get(sObj.Supplier_ID__c).Manager__c and whatId =: sObj.Id and Status = 'Not Started' limit 1]);
                }*/ 
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Pending Approval';
                //Create New Task to Manager.
                if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null)
                {
                    lst_create_task = new Task();
                    lst_create_task.OwnerId = mapSupplier.get(sObj.Supplier_ID__c).Manager__c;
                    lst_create_task.Subject = 'Review is pending for Approval - ('+mapSupplier.get(sObj.Supplier_ID__c).Supplier_Name__c +')';
                    //lst_create_task.Subject = 'call';
                    lst_create_task.ActivityDate = system.today();
                    lst_create_task.Status = 'Not Started';
                    lst_create_task.whatId = sObj.Id;   
                    lsttaskupdate.add(lst_create_task);
                }
            }
            else if(sObj.Review_Status__c == 'Overdue')
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Overdue';
            }
            
            if(mapSupplier.get(sObj.Supplier_ID__c) != null)
            {  
                //adding latest review to supplier
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Rating__c = sObj.Rating__c;
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Expected_Review_End_Date__c = sObj.Expected_Review_End_Date__c;
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Risk_Level__c = sObj.Risk_Level__c;
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Reviewed_By__c = mapSupplier.get(sObj.Supplier_ID__c).Reviewed_by__c;
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Review_Start_Date__c = sObj.Review_Start_Date__c;
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Spend__c = sObj.Spend__c;
                ListSupplier.add(mapSupplier.get(sObj.Supplier_ID__c));                
            }   
        }  
         
    }
    if(trigger.isupdate)
    {
        //lsttskUpdate = new  list<Task>(); 
        for(BSureS_Credit_Analysis__c sObj:Trigger.new)
        {
            
            if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Completed')
            {
                if(sObj.Spend__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Spend__c = sObj.Spend__c;
                }
                //system.debug('*******'+mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c);
                //system.debug('parent record rating===='+mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__r.Rating__c);
                //mapSupplier .get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__r.Rating__c;
                if(mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c != null)
                {
                    /*
                    BSureS_Basic_Info__c getParentSID = [select Rating__c from BSureS_Basic_Info__c where id =: mapSupplier.get(sObj.Supplier_ID__c).Parent_Supplier_ID__c];
                    if(getParentSID.Rating__c == null || getParentSID.Rating__c =='')
                    {
                        getParentSID.Rating__c = 'N/A';
                    }
                    mapSupplier .get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = getParentSID.Rating__c;
                    */
                    if(lstgetParentSID != null && lstgetParentSID.size() > 0)
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = lstgetParentSID.get(0).Rating__c;
                    } 
                    else
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Parent_Company_s_Rating__c = 'N/A';
                    }                   
                }
                if(sObj.Rating__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Rating__c = sObj.Rating__c;
                }
                if(sObj.Risk_Level__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Risk_Level__c = sObj.Risk_Level__c;
                }
                mapSupplier.get(sObj.Supplier_ID__c).Reviewed_by__c = UserInfo.getName(); 
                
                if(sObj.Next_Review_Date__c != null)
                {
                    //system.debug('sObj.Next_Review_Date__c====='+sObj.Next_Review_Date__c);
                    mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c;
                    /*if(sObj.Rating__c =='Inactive')
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    }
                    else
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c;
                    }*/ 
                }
                else
                {
                    
                    mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    /*
                    if(sObj.Rating__c =='Inactive' && sObj.Next_Review_Date__c == null)
                    {
                        mapSupplier.get(sObj.Supplier_ID__c).Next_Review_Date__c = null;
                    }*/
                }
                if(sObj.Review_Complete_Date__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Complete_Date__c = sObj.Review_Complete_Date__c;
                }
                if(sObj.Spend_Start_Date__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Spend_Start_Date__c = sObj.Spend_Start_Date__c;
                }
                if(sObj.Spend_End_Date__c != null)
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Spend_End_Date__c = sObj.Spend_End_Date__c;
                }
                if(sObj.Rating__c =='Inactive')
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Not Scheduled';
                }
                else
                {
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c =  'Scheduled';
                }   
                /*if(sobj.Rating__c != 'Inactive')
                { 
                    BSureS_Rating__c csObj = BSureS_Rating__c.getValues(sobj.Rating__c);
                    mapSupplier .get(sObj.Supplier_ID__c).Next_Review_Date__c = date.today().addMonths(Integer.valueOf(csObj.months__c));
                }
                else
                {
                    system.debug('sObj.Next_Review_Date__c====='+sObj.Next_Review_Date__c);
                    mapSupplier .get(sObj.Supplier_ID__c).Next_Review_Date__c = sObj.Next_Review_Date__c; 
                }*/
                /*if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null && UserInfo.getUserId() == mapSupplier.get(sObj.Supplier_ID__c).Manager__c )
                {
                    lsttskUpdate = new list<Task>([select id,OwnerId from Task where OwnerId =: mapSupplier.get(sObj.Supplier_ID__c).Manager__c and whatId =: sObj.Id and Status = 'Not Started' ] );
                    if(lsttskUpdate != null && lsttskUpdate.size() > 0 )
                    {
                        lsttskUpdate.get(0).Status = 'Completed';
                    }   
                }*/ 
             }
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Rejected')
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Rejected';
            }
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Scheduled')
            {
                //else if(sObj.Review_Status__c == 'Not Started')
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Scheduled';
            }  
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Started' && sObj.Spend__c == null) 
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Started';
                
                lst_create_task_RSStarted = new Task();
                if(mapSupplier.get(sObj.Supplier_ID__c).Analyst__c != null)
                {
                    lst_create_task_RSStarted.OwnerId = mapSupplier.get(sObj.Supplier_ID__c).Analyst__c;
                    lst_create_task_RSStarted.Subject = 'Review has started and not been completed yet - ('+mapSupplier.get(sObj.Supplier_ID__c).Supplier_Name__c +')';
                    //lst_create_task_RSStarted.Subject = 'call';
                    lst_create_task_RSStarted.ActivityDate = system.today();
                    lst_create_task_RSStarted.Status = 'Not Started';
                    lst_create_task_RSStarted.whatId =  sObj.Id;
                }
                lstTaskRSStarted.add(lst_create_task_RSStarted);
                
            }
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Started' && sObj.Spend__c != null) 
            {
                mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Started';
            }
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Pending Approval')
            {
                /*if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null)
                {
                    deletePendingATask = new list<Task>([select id,OwnerId from Task where OwnerId =: mapSupplier.get(sObj.Supplier_ID__c).Manager__c and whatId =: sObj.Id and Status = 'Not Started' limit 1]);
                }*/
                if((mapSupplier != null && mapSupplier.size() > 0) && mapSupplier.get(sObj.Supplier_ID__c) != null){
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Pending Approval';
                    //Create New Task to Manager.
                    if(mapSupplier.get(sObj.Supplier_ID__c).Manager__c != null)
                    {
                        lst_create_task = new Task();
                        lst_create_task.OwnerId = mapSupplier.get(sObj.Supplier_ID__c).Manager__c;
                        lst_create_task.Subject = 'Review is Pending for Approval - ('+mapSupplier.get(sObj.Supplier_ID__c).Supplier_Name__c +')';
                        //lst_create_task.Subject ='call';
                        lst_create_task.ActivityDate = system.today();
                        lst_create_task.Status = 'Not Started';
                        lst_create_task.whatId = sObj.Id;  
                        lsttaskupdate.add(lst_create_task);
                    }
                }    
            }
            //Overdue
            else if(sObj.Review_Status__c != null && sObj.Review_Status__c == 'Overdue')
            {
                if(mapSupplier.get(sObj.Supplier_ID__c) != null){
                    mapSupplier.get(sObj.Supplier_ID__c).Review_Status__c = 'Overdue';
                }   
            }
            //system.debug('mapSupplier======='+mapSupplier.get(sObj.Supplier_ID__c));
            if(mapSupplier.get(sObj.Supplier_ID__c) != null)
            { 
                //adding latest review to supplier
                mapSupplier.get(sObj.Supplier_ID__c).Latest_Rating__c = sObj.Rating__c;
                 mapSupplier.get(sObj.Supplier_ID__c).Latest_Expected_Review_End_Date__c = sObj.Expected_Review_End_Date__c;
                 mapSupplier.get(sObj.Supplier_ID__c).Latest_Risk_Level__c = sObj.Risk_Level__c;
                 mapSupplier.get(sObj.Supplier_ID__c).Latest_Reviewed_By__c = mapSupplier.get(sObj.Supplier_ID__c).Reviewed_by__c;
                 mapSupplier.get(sObj.Supplier_ID__c).Latest_Review_Start_Date__c = sObj.Review_Start_Date__c;
                 mapSupplier.get(sObj.Supplier_ID__c).Latest_Spend__c = sObj.Spend__c;
                ListSupplier.add(mapSupplier.get(sObj.Supplier_ID__c));
            }   
            //system.debug('lst======='+ListSupplier);
            //system.debug('lst_create_task=======1===='+lst_create_task);
        }
    }
    if(lstTaskRSStarted != null && lstTaskRSStarted.size() > 0)
    {
        Database.Saveresult[] objCreateTaskRSStarted = Database.insert(lstTaskRSStarted);
    } 
    if(lsttaskupdate != null && lsttaskupdate.size() > 0)
    {
        //system.debug('lst_create_task======2====='+lst_create_task);
        try{
            upsert lsttaskupdate;
        }catch(Exception ex){
            
        }   
    } 
    if(ListSupplier != null && ListSupplier.size() > 0)
    {
        upsert ListSupplier;
    } 
    /*if(lsttskUpdate != null && lsttskUpdate.size() > 0)
    {
        update lsttskUpdate;
    }*/
    if(deletePendingATask != null && deletePendingATask.size() > 0)
    {
        try{
            delete deletePendingATask;
        }   
        catch(Exception ex){
            
        }
    }
    
    if(trigger.isDelete){
        list<BSureS_Basic_Info__c> suppupdate = new list<BSureS_Basic_Info__c>();
        set<String> setdsuppid = new set<String>();
        string updatesupid;// by srinivas
        map<String,BSureS_Credit_Analysis__c> mapcsobj = new map<String,BSureS_Credit_Analysis__c>();
        map<String,BSureS_Basic_Info__c> mapsuppobj = new map<String,BSureS_Basic_Info__c>();
        set<String> setlasyid = new set<String>();
        map<Id,String> mapuser = new map<Id,String>();
        map<string,String> deletedRecStatus = new map<string,String>();
        for(BSureS_Credit_Analysis__c cdobj: trigger.old){
            //if(cdobj.Review_Status__c == 'Completed'){
                setdsuppid.add(cdobj.Supplier_ID__c);
                updatesupid=cdobj.Supplier_ID__c; // srinivas
                system.debug('updatesupid====>'+updatesupid);//srinivas
                setlasyid.add(cdobj.lastModifiedById);
                deletedRecStatus.put(cdobj.Supplier_ID__c,cdobj.Review_Status__c);
            //} 
        }   
        system.debug('setdsuppid====>'+setdsuppid);//srinivas
        system.debug('deletedRecStatus====>'+deletedRecStatus);//srinivas
        if(!setdsuppid.isEmpty()){
            list<BSureS_Credit_Analysis__c> lstca = new list<BSureS_Credit_Analysis__c>([Select Id,lastModifiedById,Supplier_ID__c,Name,Spend__c,Rating__c,Risk_Level__c,Next_Review_Date__c,Review_Complete_Date__c,Spend_Start_Date__c,Spend_End_Date__c,Review_Status__c,Expected_Review_End_Date__c,Review_Start_Date__c from BSureS_Credit_Analysis__c where Review_Status__c = 'Completed' and Supplier_ID__c IN: setdsuppid order by Credit_Analysis_ID__c ASC]);
            for(BSureS_Credit_Analysis__c cobj : lstca){
                mapcsobj.put(cobj.Supplier_ID__c,cobj);
            }
            list<BSureS_Basic_Info__c> lstsupp= new list<BSureS_Basic_Info__c>([Select Id,lastModifiedById,Parent_Company_s_Rating__c,Name,Spend__c,Rating__c,Risk_Level__c,Next_Review_Date__c,Review_Complete_Date__c,Spend_Start_Date__c,Spend_End_Date__c,Review_Status__c,Latest_Rating__c,Latest_Expected_Review_End_Date__c,Latest_Reviewed_By__c,Latest_Review_Start_Date__c,Latest_Risk_Level__c,Latest_Spend__c,Planned_Review_Date__c from BSureS_Basic_Info__c where Id IN: setdsuppid]);
            for(BSureS_Basic_Info__c sobj: lstsupp){
                mapsuppobj.put(sobj.Id,sobj);
            }
            list<User> lstuser = new list<User>([select id,name from user where id in: setlasyid]);
            for(user uobj : lstuser){
                mapuser.put(uobj.Id,uobj.Name);
            }
        }
        system.debug('mapcsobj.size()530====>' + mapcsobj.size() ); //srinivas
        if(mapcsobj != null && mapcsobj.size() > 0){
            for(String smap: mapcsobj.keyset()){
                if(mapsuppobj.containskey(smap)){
                system.debug('mapsuppobj 534====>' + mapsuppobj); //srinivas
                    BSureS_Basic_Info__c smainobj = new BSureS_Basic_Info__c();
                    smainobj = mapsuppobj.get(smap);
                    if(mapcsobj.get(smap).Spend__c != null){
                        smainobj.Spend__c = mapcsobj.get(smap).Spend__c;
                        smainobj.Latest_Spend__c=mapcsobj.get(smap).Spend__c;
                    } 
                    if(mapcsobj.get(smap).Rating__c != null){
                        smainobj.Rating__c = mapcsobj.get(smap).Rating__c;
                        smainobj.Latest_Rating__c = mapcsobj.get(smap).Rating__c;
                    }
                    if(mapcsobj.get(smap).Expected_Review_End_Date__c != null){
                        smainobj.Latest_Expected_Review_End_Date__c = mapcsobj.get(smap).Expected_Review_End_Date__c;
                    }
                    if(mapcsobj.get(smap).Risk_Level__c != null){
                        smainobj.Risk_Level__c = mapcsobj.get(smap).Risk_Level__c;
                        smainobj.Latest_Risk_Level__c = mapcsobj.get(smap).Risk_Level__c;
                    }
                    if(mapcsobj.get(smap).Review_Complete_Date__c != null){
                        smainobj.Review_Complete_Date__c = mapcsobj.get(smap).Review_Complete_Date__c;
                    }
                    if(mapcsobj.get(smap).Spend_Start_Date__c != null){
                        smainobj.Spend_Start_Date__c = mapcsobj.get(smap).Spend_Start_Date__c;
                    }
                    if(mapcsobj.get(smap).Spend_End_Date__c != null){
                        smainobj.Spend_End_Date__c = mapcsobj.get(smap).Spend_End_Date__c;
                    }
                    if(mapcsobj.get(smap).Spend_End_Date__c != null){
                        smainobj.Spend_End_Date__c = mapcsobj.get(smap).Spend_End_Date__c;
                    }
                     if(mapcsobj.get(smap).Review_Start_Date__c != null){
                        smainobj.Latest_Review_Start_Date__c = mapcsobj.get(smap).Review_Start_Date__c;
                    }
                    if(mapcsobj.get(smap).Next_Review_Date__c != null){
                        smainobj.Planned_Review_Date__c = mapcsobj.get(smap).Next_Review_Date__c;
                        if(deletedRecStatus.get(smainobj.id) == 'Completed' && (!(smainobj.Review_Status__c=='Pending Approval' ||
                        smainobj.Review_Status__c=='Started'))){
                        date tempDt = Date.valueof (system.today());
                        smainobj.Review_Status__c =  mapcsobj.get(smap).Next_Review_Date__c <  tempDt? 'Overdue' : 'Scheduled' ;
                        }
                        if(deletedRecStatus.get(smainobj.id) == 'Pending Approval' || deletedRecStatus.get(smainobj.id)=='Started'){
                        date tempDt = Date.valueof (system.today());
                        smainobj.Review_Status__c =  mapcsobj.get(smap).Next_Review_Date__c <  tempDt? 'Overdue' : 'Scheduled' ;
                        }
                    }
                    else{
                        smainobj.Planned_Review_Date__c = null;
                        smainobj.Review_Status__c ='Not Scheduled';
                        system.debug('smainobj.Review_Status__c=====>'+smainobj.Review_Status__c);
                    }
                    smainobj.Reviewed_by__c = mapuser.containskey(mapcsobj.get(smap).lastModifiedById) ? mapuser.get(mapcsobj.get(smap).lastModifiedById) : '';
                    smainobj.Latest_Reviewed_By__c=smainobj.Reviewed_by__c;
                    suppupdate.add(smainobj);
                }
            }
        }
       /* if(mapcsobj == null || mapcsobj.size() == 0){
        system.debug('updatesupid===empty====>'+updatesupid);
             if(mapsuppobj.containskey(updatesupid)){
             BSureS_Basic_Info__c smainobj = new BSureS_Basic_Info__c();
             smainobj = mapsuppobj.get(updatesupid);
             system.debug('smainobj====>'+smainobj);
             smainobj.Review_Status__c ='Not Scheduled';
             system.debug('smainobj.Review_Status__c====>'+smainobj.Review_Status__c);
             //suppupdate.add(smainobj);
             }
        }// added by srinivas if we delete all credit status records then supplier review is'Not scheduled'
        */
        if(!setdsuppid.isEmpty()){
            for(String ss :setdsuppid ){
                if(!mapcsobj.containskey(ss)){
                system.debug('test cs610====>');
                    BSureS_Basic_Info__c smainobj = new BSureS_Basic_Info__c();
                    smainobj = mapsuppobj.get(ss);
                    smainobj.Spend__c = null;
                    smainobj.Rating__c = '';
                    smainobj.Risk_Level__c = '';
                    smainobj.Review_Complete_Date__c = null;
                    smainobj.Spend_Start_Date__c = null;
                    smainobj.Spend_End_Date__c = null;
                    smainobj.Parent_Company_s_Rating__c = '';
                   // smainobj.Next_Review_Date__c = System.today().addMonths(6);
                   smainobj.Next_Review_Date__c =null;
                    smainobj.Reviewed_by__c = '';
                    smainobj.Latest_Rating__c = '';
                    smainobj.Latest_Expected_Review_End_Date__c = null;
                    smainobj.Latest_Reviewed_By__c='';
                    smainobj.Latest_Review_Start_Date__c= null;
                    smainobj.Latest_Spend__c = null;
                    smainobj.Latest_Risk_Level__c = '';
                    smainobj.Planned_Review_Date__c = null;
                    if(deletedRecStatus.get(smainobj.id) == 'Completed' && (!(smainobj.Review_Status__c=='Pending Approval' ||
                    smainobj.Review_Status__c=='Started'))){
                       System.Debug('Clearing review status for completed');
                        //smainobj.Review_Status__c ='';   //commented on 19.02.19(Reg:case15506) due to Review Status is required field in supplier
                        smainobj.Review_Status__c ='Not Scheduled';// added by srinivas on 19.02.19(Reg:case15506) due to Review Status is required field in supplier
                    }
                    if(deletedRecStatus.get(smainobj.id) == 'Pending Approval' || deletedRecStatus.get(smainobj.id)=='Started'){
                       System.Debug('Clearing review status');
                        //smainobj.Review_Status__c =''; //commented it due to Review Status is required field in supplier
                        smainobj.Review_Status__c ='Not Scheduled';// added by srinivas on 19.02.19(Reg:case15506) due to Review Status is required field in supplier
                    }
                    
                    suppupdate.add(smainobj);
                }
            }
        }
        if(!suppupdate.isEmpty()){
            update suppupdate;
        }
    }
   }
}