/***********************************************************************************************
*       Trigger Name : BSureS_SubZoneManagerTrigger
*       Date                    : 27/11/2012 
*       Author                  : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       27/11/2012               Kishorekumar A                Initial Version
**************************************************************************************************/
trigger BSureS_SubZoneManagerTrigger on BSureS_Sub_Zone_Manager__c (before insert,before update) 
{
	Set<ID> SZM = new Set<ID>();
    Set<ID> setSZ =new Set<ID>();
    Set<ID> SZManID = new set<ID>();
           
    if(trigger.IsBefore){
    	
    	for(BSureS_Sub_Zone_Manager__c SZManagerObj:trigger.new)
        {   
        	setSZ.add(SZManagerObj.Sub_Zone__c);
            SZM.add(SZManagerObj.Sub_Zone_Manager__c);
            SZManID.add(SZManagerObj.Id);
        } 
        
        list<BSureS_Sub_Zone_Manager__c> LstSZMan = [select Id,Sub_Zone_Manager__c,Sub_Zone__c 
        											 from BSureS_Sub_Zone_Manager__c 
        											 where Sub_Zone__c =: setSZ 
        											 and Sub_Zone_Manager__c =:SZM  
        											 and Id !=:SZManID];
        											 
         for(BSureS_Sub_Zone_Manager__c ExistSZ:LstSZMan)
        {
            for(BSureS_Sub_Zone_Manager__c Cat:Trigger.New){
                if(ExistSZ.Sub_Zone_Manager__c == Cat.Sub_Zone_Manager__c )
                Cat.addError('Record already exists with the same Country Name and Country Manager');
            }
        }											                                          
 
    }
	
	
	/*for(BSureS_Sub_Zone_Manager__c subZoneManagerObj:trigger.new)
	{
		if(trigger.isInsert)
		{ 
			Integer subZoneManagerRecCount = [select count() from BSureS_Sub_Zone_Manager__c where 	Sub_Zone_Manager__c	=:subZoneManagerObj.Sub_Zone_Manager__c and Sub_Zone__c =:subZoneManagerObj.Sub_Zone__c ];
			if(subZoneManagerRecCount > 0)
			{
				subZoneManagerObj.addError('Record already Exists with the same SubZone Name and SubZone Manager');
			}
			if(subZoneManagerObj.Sub_Zone_Manager__c == null)
			{
				subZoneManagerObj.adderror('Please Select Sub Zone Manager.');
			}
		}
		if(trigger.isUpdate)
		{
			Integer subZoneManagerRecCount = [select count() from BSureS_Sub_Zone_Manager__c where 	Sub_Zone_Manager__c	=:subZoneManagerObj.Sub_Zone_Manager__c and Sub_Zone__c =:subZoneManagerObj.Sub_Zone__c and id !=:subZoneManagerObj.id];
			if(subZoneManagerRecCount > 0)
			{
				subZoneManagerObj.addError('Record already Exists with the same SubZone Name and SubZone Manager');
			}
			if(subZoneManagerObj.Sub_Zone_Manager__c == null)
			{
				subZoneManagerObj.adderror('Please Select Sub Zone Manager.');
			}
		}     
	}*/
}