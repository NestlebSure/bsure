/*************************************************************************************************
*       Author           : B.Anupreethi
*       Purpose          : Trigger to insert the count in the parent object(Credit Analysis Section)when ever records inderted in discussion object
*       Change History   :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       13/02/2013               B.Anupreethi                Initial Version
**************************************************************************************************/
trigger BSureS_InsertCntInCreditAnalysisSection on BSureS_Credit_Analysis_Discussion__c (after insert) 
{
    set<Id> Ids = new set<Id>();
    
        for(BSureS_Credit_Analysis_Discussion__c objDiscussion: trigger.new)
        {   
            if(objDiscussion.Parent_ID__c == null || objDiscussion.Parent_ID__c=='')
            {               
                Ids.add(objDiscussion.BSureS_Credit_Analysis_Section__c);
            }
        }
        if(!Ids.isEmpty())
        {
            list<BSureS_Credit_Analysis_Section__c> lstSection = new list<BSureS_Credit_Analysis_Section__c>(); 
            lstSection = [SELECT id, DiscussionsCount__c 
                          FROM BSureS_Credit_Analysis_Section__c 
                          WHERE id in:Ids];
            for(BSureS_Credit_Analysis_Section__c section:lstSection){
                if(section.DiscussionsCount__c != null )
                {
                    section.DiscussionsCount__c = section.DiscussionsCount__c + 1;  
                }else
                {
                    section.DiscussionsCount__c = 1;
                }
            }
            if(!lstSection.IsEmpty())
                update lstSection;
        }
    
    /*for(BSureS_Credit_Analysis_Discussion__c objDiscussion: trigger.new)
    {
        if(objDiscussion.Parent_ID__c==null || objDiscussion.Parent_ID__c=='')
        {
            list<BSureS_Credit_Analysis_Section__c> lstSection = new list<BSureS_Credit_Analysis_Section__c>();
            
            lstSection = [SELECT id, DiscussionsCount__c 
                          FROM BSureS_Credit_Analysis_Section__c 
                          WHERE id=:objDiscussion.BSureS_Credit_Analysis_Section__c];
            
            decimal iCnt = lstSection[0].DiscussionsCount__c;
            if(iCnt == null)
            iCnt = 0;
            iCnt++;
            lstSection[0].DiscussionsCount__c = iCnt;
            
            update lstSection;
        }
    }*/
}