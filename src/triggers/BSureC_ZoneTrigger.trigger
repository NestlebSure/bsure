trigger BSureC_ZoneTrigger on BSureC_Zone__c (before insert, before update) {

    Set<string> Subzone = new set<string>();
    Set<ID> SubzoneID = new set<ID>();  
    if(trigger.isBefore)
    {
        for(BSureC_Zone__c szm:trigger.new)
        {
            Subzone.add('%'+szm.Name+'%');          
            SubzoneID.add(szm.Id);          
        }
        if(BSureC_Zone__c.SObjectType.getDescribe().isQueryable()){
	        List<BSureC_Zone__c> Listsubzone = [SELECT id,Name 
	                                                    from BSureC_Zone__c
	                                                    WHERE name like:Subzone and id!=:SubzoneID ];
	        
	        system.debug('List***'+Listsubzone);
	        for(BSureC_Zone__c Lstsz:Listsubzone)
	        {
	            for(BSureC_Zone__c szm:trigger.new){
	                if(Lstsz.Name == szm.Name)  
	                szm.addError('Record already exists with this name');
	            }
	        }
        }                                                   
    }
}

/*
for(BSureC_Zone__c BSureZone:trigger.new){
    if(trigger.isBefore && trigger.isInsert)
    {
        Integer DuplicateBsure = [SELECT count() from BSureC_Zone__c where name=:BSureZone.name];
        if(DuplicateBsure>0)
        BSureZone.adderror('Record already exists with this name');
    }
    if(trigger.isBefore && trigger.isUpdate)
    {
        Integer DuplicateBsure = [SELECT count() from BSureC_Zone__c where name=:BSureZone.name and id!=:BSureZone.id];
        if(DuplicateBsure>0)
        BSureZone.adderror('Record already exists with this name');
    }
}
*/