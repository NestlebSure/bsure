/***********************************************************************************************
* Trigger Name      : BSureS_BackUpAnalystsTrigger
* Date              : 24/11/2012 
* Author            : KishoreKumar A
* Purpose           : Trigger to update Backup analysts names
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 24/11/2012            KishoreKumar A         Initial Version
**************************************************************************************************/
trigger BSureS_BackUpAnalystsTrigger on BSureS_Basic_Info__c (before update) {
    list<BSureS_Basic_Info__c> ListSupplier= new list<BSureS_Basic_Info__c>();
    list<string> backupAnalysts = new list<string>();
    list<string> lstStringBackUpAnalyst = new list<string>();
    map<Id,list<string>>  maplistSupp = new map<Id,list<string>>();
    list<BSureS_Basic_Info__Share> lstSupplierShare = new list<BSureS_Basic_Info__Share>();
    //public Event EventObj{get;set;}
    string strNames=''; 
    string strAnalystRole='';
    //list<string> lstEventdelete1= new list<string>();
    //list<string> lstSupplierId = new  list<string>();
    if(trigger.isBefore) 
    {
    	system.debug('triggernew@@@@');
        //BSureS_EmailReminders.CreateEmailReminders(trigger.newMap,trigger.oldMap);
        
        //strAnalystRole = BSureS_CommonUtil.getConfigurationValues('BSureS_AnalystRole').get(0);
        //map<id,BSureS_User_Additional_Attributes__c> mapObj = new map<id,BSureS_User_Additional_Attributes__c>([select id,User__r.name from BSureS_User_Additional_Attributes__c where User__r.ProfileId =:getprofileId.id]);
        //map<id,User> mapObj = new map<id,User>([select id,name from user where Profile.Name = 'BSureS_Analyst'and IsActive = true]);
        map<id,User> mapObj = new map<id,User>([select id,name from user  where IsActive = true limit 1000]);
        /*
        for(Id supplierObjBAEvent :trigger.oldMap.keySet())
        {
            BSureS_Basic_Info__c rec_Suppl_Info = trigger.oldMap.get(supplierObjBAEvent);
            lstEventdelete1.add(rec_Suppl_Info.Analyst__c);
            lstSupplierId.add(rec_Suppl_Info.Id);
        }
        list<Event> lstEventdelete = new list<Event>([select id from Event where OwnerId  IN :lstEventdelete1 and WhatId IN : lstSupplierId ]);
        system.debug('lstEventdelete==========size===='+lstEventdelete.size());
        if(!lstEventdelete.isEmpty()) database.delete(lstEventdelete,false);  
        */       
        for(BSureS_Basic_Info__c supplierObjBA :trigger.new)
        {
        	strNames = '';
            backupAnalysts = new list<string>();
        	system.debug('triggernew@@@@####');
            if(supplierObjBA.Backup_Analysts__c!= null && supplierObjBA.Backup_Analysts__c.contains(',')) 
            {
                lstStringBackUpAnalyst = supplierObjBA.Backup_Analysts__c.split(',');
                maplistSupp.put(supplierObjBA.Id,lstStringBackUpAnalyst);
            }
            else
            {
                lstStringBackUpAnalyst.add(supplierObjBA.Backup_Analysts__c);
                maplistSupp.put(supplierObjBA.Id,lstStringBackUpAnalyst);
            }
        }
        list<BSureS_Basic_Info__Share> lstdelete = [select id from BSureS_Basic_Info__Share where UserOrGroupId in:lstStringBackUpAnalyst and ParentId IN: maplistSupp.keySet()];
                        if(!lstdelete.isEmpty())database.delete(lstdelete,false);
        for(BSureS_Basic_Info__c supplierObj:trigger.new)
        {
            //EventObj = new Event();
            //system.debug('trigger'+supplierObj.Backup_Analysts_Names__c); 
            //system.debug('trigger BA ids'+supplierObj.Backup_Analysts__c);
            //system.debug('supplierObj.Backup_Analysts__c==='+supplierObj.Backup_Analysts__c);
            strNames = '';
            backupAnalysts = new list<string>();
            system.debug('triggernew@@@@####*****');
            if(supplierObj.Next_Review_Date__c != null)
            {
                supplierObj.Planned_Review_Date__c = supplierObj.Next_Review_Date__c;
            } 
            if(supplierObj.Backup_Analysts__c!= null && supplierObj.Backup_Analysts__c.contains(','))  
            {
                backupAnalysts = supplierObj.Backup_Analysts__c.split(',');
                if(backupAnalysts.size() > 0)
                {
                    //system.debug('backupAnalysts.....'+backupAnalysts);
                    
                    for(string sObj:backupAnalysts)
                    {
                        //system.debug('mapObj.get(sObj).name========'+mapObj.get(sObj).name);
                        if(mapObj.get(sObj).name != null)
                        { 
                            strNames += mapObj.get(sObj).name+',';  
                            
                        }
                        if(sObj != null && sObj != ''){
                            system.debug('sObj==========1====='+sObj);
                            BSureS_Basic_Info__Share shareObj = new BSureS_Basic_Info__Share();
                            shareObj.AccessLevel = 'Edit';
                            //shareObj.RowCause = 'Manual';
                            shareObj.ParentId = supplierObj.Id;
                            shareObj.UserOrGroupId = sObj;
                            lstSupplierShare.add(shareObj);
                        }
                    } 
                    strNames=strNames.left(strNames.length()-1);
                    
                }
            }
            else
            {
                if(supplierObj.Backup_Analysts__c != null && mapObj.get(supplierObj.Backup_Analysts__c) != null )
                {
                    //system.debug('Share========22===');
                    strNames = mapObj.get(supplierObj.Backup_Analysts__c).name;
                    if(supplierObj.Backup_Analysts__c != null && supplierObj.Backup_Analysts__c != ''){
                        /*
                        list<BSureS_Basic_Info__Share> lstdelete = [select id from BSureS_Basic_Info__Share where UserOrGroupId =:supplierObj.Backup_Analysts__c];
                        if(!lstdelete.isEmpty()) database.delete(lstdelete,false);  
                        */
                        //system.debug('sObj==============2'+ supplierObj.Backup_Analysts__c);
                        BSureS_Basic_Info__Share shareObj = new BSureS_Basic_Info__Share();
                        shareObj.AccessLevel = 'Edit';
                       //shareObj.RowCause = 'Manual'; 
                        shareObj.ParentId = supplierObj.Id;
                         shareObj.UserOrGroupId = supplierObj.Backup_Analysts__c;
                        lstSupplierShare.add(shareObj);
                    }
                }
            }
            
            //system.debug('strNames========'+strNames);
            /*if(supplierObj.Analyst__c!=null)
            {
                supplierObj.ownerID=supplierObj.Analyst__c;  
            }*/
            system.debug('strNames@@@@@@@'+strNames);
            supplierObj.Backup_Analysts_Names__c = strNames;
            
            //system.debug('backupAnalysts====='+backupAnalysts);
            //system.debug('supplierObj.Backup_Analysts_Names__c========'+supplierObj.Backup_Analysts_Names__c);
            //ListSupplier.add(supplierObj);
        
            if(supplierObj.Analyst__c != null)
            {
                //system.debug('Trigger=====New ===='+trigger.new[0].Analyst__c);
                //system.debug('Trigger=====Old ===='+trigger.old[0].Analyst__c);
                /*
                list<Event> lstEventdelete = new list<Event>([select id from Event where OwnerId =: trigger.old[0].Analyst__c and WhatId =: supplierObj.Id]);
                if(!lstEventdelete.isEmpty()) database.delete(lstEventdelete,false);  
                
                EventObj.StartDateTime = supplierObj.Planned_Review_Date__c;
                EventObj.EndDateTime =  supplierObj.Planned_Review_Date__c;
                EventObj.Subject = 'Create a New Review ';
                EventObj.Description = 'Create a New Review for this Supplier : '+ supplierObj.Supplier_Name__c;
                EventObj.OwnerId = supplierObj.Analyst__c;
                EventObj.WhatId = supplierObj.Id;
                */
            } 
            
        }
        //update ListSupplier;
    }
    /*if(EventObj != null)
    {
        insert EventObj;
    }*/
    //system.debug('lstSupplierShare=========='+lstSupplierShare);
    if(!lstSupplierShare.isEmpty())
     database.upsert (lstSupplierShare,false);
    
}