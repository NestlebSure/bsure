/************************************************************************************************       
Controller Name 		: BSureC_CreditDataStageUpdate       
Date                    : 11/15/2012        
Author                  : Santhosh Palla      
Purpose         		: To validate the stage history record when manually edited.       
Change History 			: Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
						  11/15/2012      Santhosh Palla            Initial Version
**************************************************************************************************/
trigger BSureC_CreditDataStageUpdate on BSureC_Credit_Data_Stage__c (before insert, before update) 
{
	set<string> CreditAccount =new set<string>();
	for(BSureC_Credit_Data_Stage__c credit_data_stage:trigger.new)
	{
		if(credit_data_stage.CE__c!=NULL && credit_data_stage.CE__c!='')
		{
		  CreditAccount.add(credit_data_stage.CE__c.trim());
		}
	}
	    system.debug('----CreditAccount---'+CreditAccount);
		Map<String,Id> mapcstinfo = new Map<String,Id>();
		List<BSureC_Customer_Basic_Info__c> lst_customer_info;
		list<BSureC_Credit_Data_Publish__c> lstDataPublish;
		
		if(BSureC_Customer_Basic_Info__c.SObjectType.getDescribe().isQueryable())
		lst_customer_info = [SELECT Id,Customer_Group_Id__c,Credit_Account__c FROM BSureC_Customer_Basic_Info__c where Credit_Account__c in:CreditAccount limit 5000];
		
		map<string,string> CEnowithCrDate=new map<string,string>();
		
		if(BSureC_Credit_Data_Publish__c.SObjectType.getDescribe().isQueryable())
        lstDataPublish=[Select CreatedDate, CE__c From BSureC_Credit_Data_Publish__c  where CE__c in:CreditAccount and CreatedDate=TODAY];
		
        system.debug('---lstDataPublish---'+lstDataPublish);
        for(BSureC_Credit_Data_Publish__c objDataPublish:lstDataPublish){
            	CEnowithCrDate.put(objDataPublish.CE__c,'Record already Published With this Credit Account');
            	
         }
		for(BSureC_Customer_Basic_Info__c reccustinfo : lst_customer_info)
		{
			mapcstinfo.put(reccustinfo.Credit_Account__c,reccustinfo.Id);
		}
		
		
	for(BSureC_Credit_Data_Stage__c credit_data_stage:trigger.new)
	{
		if(credit_data_stage.CE__c==NULL)
		{
			if(credit_data_stage.Exception__c==NULL)
		 	{
		 		credit_data_stage.Exception__c='Credit Account is Mandatory';
		 		credit_data_stage.Status__c='Exception';
		 	}
		 	else
		 	{
		 		credit_data_stage.Exception__c+=','+'Credit Account is Mandatory';
		 		credit_data_stage.Status__c='Exception';
		 	}
		}
		else
		{
		
			if(mapcstinfo != null && mapcstinfo.get( credit_data_stage.CE__c )!= null )
			{
				credit_data_stage.Customer_Basic_Info_Id__c = String.valueOf(mapcstinfo.get( credit_data_stage.CE__c ));
				
				if(credit_data_stage.Publish_Flag__c != true)
				{
					if(Trigger.isUpdate)
					{
						credit_data_stage.Exception__c = NULL;
						credit_data_stage.Status__c=NULL;
						credit_data_stage.Status__c='Initialized';
						credit_data_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
					}
					
				}
			}
			else
			{
				if(credit_data_stage.Exception__c==NULL)
	        	{
	        		credit_data_stage.Exception__c='Credit Account does not Exist';
	            	credit_data_stage.Status__c='Exception';
	            	credit_data_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	        	}
	            else
	            {
	                credit_data_stage.Exception__c+=','+'Credit Account does not Exist';
	                credit_data_stage.Status__c='Exception'; 
	                credit_data_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	            } 
			}
			
		
		}
		if( credit_data_stage.Publish_Flag__c != true && credit_data_stage.CE__c!=null && credit_data_stage.CE__c!='' && CEnowithCrDate.get(credit_data_stage.CE__c)!=null && CEnowithCrDate.get(credit_data_stage.CE__c)!='' ){
			if(credit_data_stage.Exception__c==NULL)
	        {
			    credit_data_stage.Exception__c=CEnowithCrDate.get(credit_data_stage.CE__c);
			}else{
				credit_data_stage.Exception__c+=','+CEnowithCrDate.get(credit_data_stage.CE__c);
			}
		}
		
	}
}