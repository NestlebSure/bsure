trigger BSureC_CustomerInfoUpload on BSureC_Customer_Basic_Info_Stage__c (before update) 
{
	//public boolean isCustStageUpdate {get; set; } 
	public static map<String,Id> map_customer_info{get;set;}
	//isCustStageUpdate = BSureC_CustomerinfoIntermediate.isCustomerStageUpdate;
	
	//if(isCustStageUpdate==null || !isCustStageUpdate)
	{
		if( Trigger.isBefore )
		{
			if( Trigger.isUpdate )
			{
				map_customer_info = new Map<String,Id>();
		    	List<BSureC_Customer_Basic_Info__c> lst_customer_info = new List<BSureC_Customer_Basic_Info__c>([SELECT Id,Customer_Group_Id__c,Credit_Account__c 
		                                                                 FROM BSureC_Customer_Basic_Info__c
		                                                                 WHERE id!=null AND Credit_Account__c != null ]);
		        if(lst_customer_info != null && lst_customer_info.size() > 0)
		        {
		        	for(BSureC_Customer_Basic_Info__c reccustinfo : lst_customer_info)
			        {
			            map_customer_info.put(reccustinfo.Credit_Account__c,reccustinfo.Id);
			        }
		        } 
				for(BSureC_Customer_Basic_Info_Stage__c cust_info_stage:trigger.new)
				{
					system.debug('cust_info_stage.Credit Account--main'+cust_info_stage.Credit_Account__c);
					if(cust_info_stage.Publish_Flag__c != true && 
						(
							(cust_info_stage.City__c == null || cust_info_stage.City__c == '') ||
							(cust_info_stage.Customer_Name__c == null || cust_info_stage.Customer_Name__c == '') ||
							(cust_info_stage.Street_Name_Address_Line_1__c == null || cust_info_stage.Street_Name_Address_Line_1__c == '') ||
							(cust_info_stage.Customer_Category__c == null || cust_info_stage.Credit_Account__c == null) ||
							(cust_info_stage.Customer_Group_Id__c == null || cust_info_stage.Customer_Group_Id__c == '') ||
							(cust_info_stage.DND_Financial_Information__c == null || cust_info_stage.DND_Financial_Information__c == '') ||
							(cust_info_stage.Credit_Account__c != null
								&& map_customer_info.get(cust_info_stage.Credit_Account__c) != null )
						 )
					  )
					{
						system.debug('---cust_info_stage.City__c--'+cust_info_stage.Credit_Account__c);
					
						cust_info_stage.Exception__c=NULL;
						//Customer Name
						if(cust_info_stage.Customer_Name__c == null || cust_info_stage.Customer_Name__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Customer Name is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'Customer Name is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						//Credit Account
						if(cust_info_stage.Credit_Account__c == null || cust_info_stage.Credit_Account__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Credit Account is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'Credit Account is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						
						//Group ID
						if(cust_info_stage.Customer_Group_Id__c == null || cust_info_stage.Customer_Group_Id__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Group Id is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'Group Id is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						//DND Financial Information
						if(cust_info_stage.DND_Financial_Information__c == null || cust_info_stage.DND_Financial_Information__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='DND Financial Information is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'DND Financial Information is mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						
						//City
						if(cust_info_stage.City__c == null || cust_info_stage.City__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='City is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		system.debug('---cust_info_stage.City__c--'+cust_info_stage.City__c);
						 		//cust_info_stage.Exception__c+=','+'City is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						// planned review date 
						if(cust_info_stage.Planned_Review_Date__c == null)
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Planned Review Date is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		//system.debug('---cust_info_stage.City__c--'+cust_info_stage.City__c);
						 		cust_info_stage.Exception__c+=','+'Planned Review Date is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						/*else
						{
							if(cust_info_stage.Publish_Flag__c != true)
							{
								cust_info_stage.Exception__c = NULL;
								cust_info_stage.Status__c='Initialized';
								cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
							}
							
						}*/
						
						/*else
						{
							if(cust_info_stage.Publish_Flag__c != true)
							{
								cust_info_stage.Exception__c = NULL;
								cust_info_stage.Status__c='Initialized';
								cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
								
							}
						}*/
						if(cust_info_stage.Customer_Category__c == null)
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Category is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'Category is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						/*else
						{
							if(cust_info_stage.Publish_Flag__c != true)
							{
								cust_info_stage.Exception__c = NULL;
								cust_info_stage.Status__c='Initialized';
								cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
							}
							
						}*/
						if(cust_info_stage.Street_Name_Address_Line_1__c == null || cust_info_stage.Street_Name_Address_Line_1__c == '')
						{
							if(cust_info_stage.Exception__c==NULL)
							{
								cust_info_stage.Exception__c='Street Name Address Line 1 is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						 	else
						 	{
						 		cust_info_stage.Exception__c+=','+'Street Name Address Line 1 is Mandatory';
						 		cust_info_stage.Status__c='Exception';
						 		cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Red';
						 	}
						}
						//Credit Account duplicate Check
						if(cust_info_stage.Credit_Account__c != null
						&& map_customer_info.get(cust_info_stage.Credit_Account__c) != null )
						{
							system.debug('cust_info_stage.Credit Account--'+cust_info_stage.Credit_Account__c);
	                    	if(cust_info_stage.Exception__c==NULL)
	                    	{
	                        	cust_info_stage.Exception__c='Credit Account already exists';
		                        cust_info_stage.Status__c='Exception';
		                        cust_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	                    	}
	                    	else
	                    	{
	                    		cust_info_stage.Exception__c +=','+'Credit Account already exists';
		                        cust_info_stage.Status__c='Exception';
		                        cust_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
	                        }
						}
					}
					else
					{
						if(cust_info_stage.Publish_Flag__c != true)
						{
							cust_info_stage.Exception__c = NULL;
							cust_info_stage.Status__c='Initialized';
							cust_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
						}
					}
				}
			}
		}	
   	}	
}