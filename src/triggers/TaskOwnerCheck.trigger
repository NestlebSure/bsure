/***********************************************************************************************
*       Trigger Name : TaskOwnerCheck
*       Date                    : 27/11/2012 
*       Author                  : veereandrnath J
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
		09/08/2013      		J.Veereandranath            Code Review 
**************************************************************************************************/
trigger TaskOwnerCheck on Task (before insert, before update) {
	
	string SPrefix = '';
	string CPrefix = '';
	map<Id,User> mapUser = new map<Id,User>();
	
	if(Schema.SObjectType.BSureS_Basic_Info__c.isAccessible())
	 	SPrefix = Schema.SObjectType.BSureS_Basic_Info__c.getKeyPrefix();
 	if(Schema.SObjectType.BSureC_Customer_Basic_Info__c.isAccessible())
		CPrefix = Schema.SObjectType.BSureC_Customer_Basic_Info__c.getKeyPrefix();
	if(Schema.SObjectType.User.isQueryable())
		mapUser = new map<Id,User>([select Id,Profile.Name from User limit 10000]);
	
	for(Task t:Trigger.new){
		
		if(SPrefix != '' && t.WhatId != null &&  string.valueof(t.WhatId).startsWith(SPrefix) && mapUser.get(t.Ownerid).Profile.Name.Contains('BSureC') ){
			t.OwnerId.addError(system.Label.the_owner_should_be_Supplier_User_or_Admin);
		}else if(CPrefix !='' && t.WhatId != null && string.valueof(t.WhatId).startsWith(CPrefix) && mapUser.get(t.Ownerid).Profile.Name.Contains('BSureS') ){
			t.OwnerId.addError(system.Label.the_owner_should_be_Customer_User_or_Admin);
		}
	}
}