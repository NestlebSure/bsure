trigger bSureC_deletedcreditreviewupdate on BSureC_Credit_Increase__c (after delete) {
    set<String> setcids = new set<String>();
    map<String,BSureC_Credit_Increase__c> mapobj = new map<String,BSureC_Credit_Increase__c>();
    list<BSureC_Credit_Increase__c> lstca = new list<BSureC_Credit_Increase__c>();
    map<String,BSureC_Customer_Basic_Info__c> mapcust = new map<String,BSureC_Customer_Basic_Info__c>();
    list<BSureC_Customer_Basic_Info__c> lstcustupdate = new list<BSureC_Customer_Basic_Info__c>();
    list<BSureC_Customer_Basic_Info__c> lstcustinfo = new list<BSureC_Customer_Basic_Info__c>();
    for(BSureC_Credit_Increase__c ciobj : trigger.old)
    {
        System.debug('ciobj==============='+ciobj);
        if(ciobj.Status__c == 'Completed' && ciobj.Customer_Basic_Info_Id__c != null){
            setcids.add(ciobj.Customer_Basic_Info_Id__c);
        }
    }
    if(setcids != null && setcids.size() > 0)
    {   lstcustinfo = [Select Id,Review_Complete_Date__c,Reviewed_By__c,Name,Next_Review_Date__c,Planned_Review_Date__c,Risk_Category__c,Review_Status__c,Credit_Limit__c from BSureC_Customer_Basic_Info__c where Id In: setcids];
	    if(lstcustinfo != null && lstcustinfo.size() > 0)
	    {
	        for(BSureC_Customer_Basic_Info__c cust : lstcustinfo)
	        {
	            mapcust.put(cust.Id,cust);
	        }
	    }  
        lstca = [Select Id,Review_Complete_Date__c,LastModifiedById,Customer_Basic_Info_Id__c,Next_Review_Date__c,Credit_Limit__c,New_Credit_Limit__c,New_Risk_Category__c from BSureC_Credit_Increase__c where Status__c = 'Completed' and Customer_Basic_Info_Id__c IN : setcids order by Credit_Increase_ID__c ASC ];
    }
    if(lstca != null && lstca.size() > 0)
    {
        for(BSureC_Credit_Increase__c cobj : lstca)
        {
            mapobj.put(cobj.Customer_Basic_Info_Id__c, cobj);
        }
    }
    
    System.debug('mapobj=========='+mapobj);
    if(mapobj != null && mapobj.size() > 0)
    {
        for(String cobj: mapobj.keySet())
        {
            if(mapcust.containskey(cobj))
            {
                BSureC_Customer_Basic_Info__c customerobj = new BSureC_Customer_Basic_Info__c();
                customerobj = mapcust.get(cobj);
                if(mapobj.get(cobj).New_Credit_Limit__c != null){
                    customerobj.Credit_Limit__c = mapobj.get(cobj).New_Credit_Limit__c;
                }  
                if(mapobj.get(cobj).New_Risk_Category__c != null){
                    customerobj.Risk_Category__c  = mapobj.get(cobj).New_Risk_Category__c;
                }    
                if(mapobj.get(cobj).Next_Review_Date__c != null){
                    customerobj.Planned_Review_Date__c = mapobj.get(cobj).Next_Review_Date__c;
                    customerobj.Next_Review_Date__c = mapobj.get(cobj).Next_Review_Date__c;
                }  
                if(mapobj.get(cobj).Review_Complete_Date__c != null){
                    customerobj.Review_Complete_Date__c = mapobj.get(cobj).Review_Complete_Date__c;
                }    
                customerobj.Reviewed_By__c = mapobj.get(cobj).LastModifiedById;
                lstcustupdate.add(customerobj);  
            }   
            
        }        
    }
    if(setcids != null && setcids.size() > 0)
    {
        for(String ss : setcids)
        {
            if(!mapobj.containskey(ss))
            {
                BSureC_Customer_Basic_Info__c customerobj = new BSureC_Customer_Basic_Info__c();
                customerobj = mapcust.get(ss);
                customerobj.Credit_Limit__c = null;
                customerobj.Risk_Category__c  = '';
                customerobj.Planned_Review_Date__c = System.today().addmonths(6);
                customerobj.Review_Complete_Date__c = null;
                customerobj.Next_Review_Date__c = System.today().addmonths(6); 
                customerobj.Reviewed_By__c = null;
                lstcustupdate.add(customerobj);
            }
        }
    }
    if(!lstcustupdate.isEmpty()){
        update lstcustupdate;
    } 
}