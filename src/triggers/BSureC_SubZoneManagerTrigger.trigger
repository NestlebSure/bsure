trigger BSureC_SubZoneManagerTrigger on BSureC_Sub_Zone_Manager__c (before insert, before update) {

    /*for(BSureC_Sub_Zone_Manager__c subZoneManagerObj:trigger.new)
    {
        if(trigger.isInsert)
        { 
            Integer subZoneManagerRecCount = [select count() from BSureC_Sub_Zone_Manager__c where  Sub_Zone_Manager__c =:subZoneManagerObj.Sub_Zone_Manager__c and Sub_Zone__c =:subZoneManagerObj.Sub_Zone__c ];
            if(subZoneManagerRecCount > 0)
            {
                subZoneManagerObj.addError('Record already Exists with the same SubZone Name and SubZone Manager');
            }
            if(subZoneManagerObj.Sub_Zone_Manager__c == null)
            {
                subZoneManagerObj.adderror('Please Select Sub Zone Manager.');
            }
        }
        if(trigger.isUpdate)
        {
            Integer subZoneManagerRecCount = [select count() from BSureC_Sub_Zone_Manager__c where  Sub_Zone_Manager__c =:subZoneManagerObj.Sub_Zone_Manager__c and Sub_Zone__c =:subZoneManagerObj.Sub_Zone__c and id !=:subZoneManagerObj.id];
            if(subZoneManagerRecCount > 0)
            {
                subZoneManagerObj.addError('Record already Exists with the same SubZone Name and SubZone Manager');
            }
            if(subZoneManagerObj.Sub_Zone_Manager__c == null)
            {
                subZoneManagerObj.adderror('Please Select Sub Zone Manager.');
            }
        }     
    }*/
    
    Set<ID> SubZonemgr = new Set<ID>();
    Set<ID> szonename =new Set<ID>();
    Set<ID> szoneID = new set<ID>();
           
    if(trigger.IsBefore){
    	
    	for(BSureC_Sub_Zone_Manager__c countryManagerObj:trigger.new)
        {   
        	szonename.add(countryManagerObj.Sub_Zone__c);
            SubZonemgr.add(countryManagerObj.Sub_Zone_Manager__c);
            szoneID.add(countryManagerObj.Id);
        } 
        if(BSureC_Sub_Zone_Manager__c.SObjectType.getDescribe().isQueryable()){
	        list<BSureC_Sub_Zone_Manager__c> Lstszone = [select Id,Sub_Zone_Manager__c,Sub_Zone__c 
	        											 from BSureC_Sub_Zone_Manager__c 
	        											 where Sub_Zone__c =: szonename 
	        											 and Sub_Zone_Manager__c =:SubZonemgr  
	        											 and Id !=:szoneID];
	        											 
	         for(BSureC_Sub_Zone_Manager__c ExistC:Lstszone)
	        {
	            for(BSureC_Sub_Zone_Manager__c Cat:Trigger.New){
	                if(ExistC.Sub_Zone_Manager__c == Cat.Sub_Zone_Manager__c )
	                Cat.addError('Record already exists with this name');
	            }
	        }
        }
 
    }    
}