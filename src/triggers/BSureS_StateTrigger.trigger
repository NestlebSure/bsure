/***********************************************************************************************
*       Trigger Name : BSureS_StateTrigger
*       Date                    : 03/12/2012 
*       Author                  : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       27/11/2012               Kishorekumar A                Initial Version
* 		02/07/2013               kishorekuamr A                State Validation 
**************************************************************************************************/
trigger BSureS_StateTrigger on BSureS_State__c (before insert,before update) 
{
	
	Set<string> State = new set<string>();
    Set<ID> StateID = new set<ID>();  
    list<string> countryIds = new list<string>();
    if(trigger.isBefore)
    {
        for(BSureS_State__c st:trigger.new)
        {
            State.add('%'+st.Name+'%');          
            StateID.add(st.Id);  
            countryIds.add(st.Supplier_Country__c);        
        }       
        List<BSureS_State__c> ListState = [SELECT id,Name 
                                                    from BSureS_State__c
                                                    WHERE name like:State and id!=:StateID and Supplier_Country__c =: countryIds];
        
        system.debug('List***'+ListState);
        for(BSureS_State__c Lsts:ListState)
        {
            for(BSureS_State__c statem:trigger.new){
                if(Lsts.Name == statem.Name)  
                statem.addError('Record already exists with this name');
            }
        }                                                   
    }
	
	
	/*for(BSureS_State__c stateObj: Trigger.new)
	{
		if(Trigger.isInsert)
		{
			Integer stateRecCount = [select count() from BSureS_State__c where name =: stateObj.name];
			if(stateRecCount > 0)
			{
				stateObj.addError('Record already Exists with the same State Name'); 
			}
		}
		if(Trigger.isUpdate)
		{
			Integer stateRecCount = [select count() from BSureS_State__c where name =: stateObj.name and id !=:stateObj.id];
			if(stateRecCount > 0)
			{
				stateObj.addError('Record already Exists with the same State Name'); 
			}
		}	
	}*/
}