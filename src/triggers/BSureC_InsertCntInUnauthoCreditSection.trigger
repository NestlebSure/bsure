/*************************************************************************************************
*       Author           : B.Anupreethi
*       Purpose          : Trigger to insert the count in the parent object(Unauthorized deduction Section)when ever records inderted in discussion object
*       Change History   :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       14/02/2013               B.Anupreethi                Initial Version
**************************************************************************************************/
trigger BSureC_InsertCntInUnauthoCreditSection on BSureC_Unauthorized_Deduction_Discussion__c (after insert) 
{
    Set<String> lst_docsectionids=new Set<String>();
    List<BSureC_Unauthorized_Deduction_Section__c> Objconfsection=new List<BSureC_Unauthorized_Deduction_Section__c>();
    
    for(BSureC_Unauthorized_Deduction_Discussion__c objDiscussion:trigger.New)
    {
        if(objDiscussion.Parent_ID__c==null || objDiscussion.Parent_ID__c=='')
        {
        lst_docsectionids.add(objDiscussion.BSureC_Unauthorized_Deduction_Section__c);
        }
        /*if(objDiscussion.Parent_ID__c==null || objDiscussion.Parent_ID__c=='')
        {
            list<BSureC_Unauthorized_Deduction_Section__c> lstSection = new list<BSureC_Unauthorized_Deduction_Section__c>();
            
            lstSection = [SELECT id, DiscussionsCount__c 
                          FROM BSureC_Unauthorized_Deduction_Section__c 
                          WHERE id=:objDiscussion.BSureC_Unauthorized_Deduction_Section__c];
            
            decimal iCnt = lstSection[0].DiscussionsCount__c;
            if(iCnt == null)
            iCnt = 0;
            iCnt++;
            lstSection[0].DiscussionsCount__c = iCnt;
            update lstSection;
        }*/
    }
    if(BSureC_Unauthorized_Deduction_Section__c.SObjectType.getDescribe().isQueryable()){
    for(BSureC_Unauthorized_Deduction_Section__c ObjDocdiscussion:[SELECT id, DiscussionsCount__c 
                                                                          FROM BSureC_Unauthorized_Deduction_Section__c 
                                                                          WHERE id in : lst_docsectionids])
      {
            decimal iCnt = ObjDocdiscussion.DiscussionsCount__c;
            if(iCnt == null)
            iCnt = 0;
            iCnt++;
            ObjDocdiscussion.DiscussionsCount__c = iCnt;
            
            Objconfsection.add(ObjDocdiscussion);
        
      }
    }
      if (BSureC_Unauthorized_Deduction_Discussion__c.sObjectType.getDescribe().isUpdateable())
      update Objconfsection;
    
}