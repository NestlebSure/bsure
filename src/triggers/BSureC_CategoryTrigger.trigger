trigger BSureC_CategoryTrigger on BSureC_Category__c (before insert, before update) 
{
    Set<String> CategoryNames = new Set<String>();
    Set<ID> CategoryID = new Set<ID>(); 
    if(trigger.IsBefore){   
        for(BSureC_Category__c Cat:Trigger.New)
        {   
            CategoryNames.add('%'+Cat.Name+'%');
            CategoryID.add(Cat.Id);
        } 
        if(BSureC_Category__c.SObjectType.getDescribe().isQueryable()){
	        list<BSureC_Category__c> lstCategories = [SELECT Id,Name 
	                                                  FROM   BSureC_Category__c         
	                                                  WHERE  Name like:CategoryNames and id!=:CategoryID]; 
	                                                  
	                                                                                         
	        system.debug('Test'+CategoryNames);                                                       
	        for(BSureC_Category__c ExistC:lstCategories)
	        {
	            for(BSureC_Category__c Cat:Trigger.New){
	                if(ExistC.Name == Cat.Name )
	                Cat.addError('Record already exists with this name');
	            }
	        }
        }
    }       
}