/***********************************************************************************************
*       Trigger Name    : BSureS_ZoneTrigger
*       Date            : 18/11/2012 
*       Author          : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       18/11/2012               Kishorekumar A               Initial Version
        01/04/2013              Veereandranath jella          To avoid soql inside loops 
**************************************************************************************************/
trigger BSureS_SubZoneTrigger on BSureS_SubZone__c (before insert,before update) 
{
    
    public boolean isSuppUpdate = false;
    /*public integer intCount = 0;
    for(BSureS_SubZone__c subzoneObj: trigger.new)
    {
        if(trigger.isInsert)
        {
            Integer subZoneRecCount = [select count() from BSureS_SubZone__c where name =: subzoneObj.name];
            if(subZoneRecCount > 0)
            {
                subzoneObj.addError('Record already Exists with the same SubZone Name');
            }
        }
        if(trigger.isUpdate)
        {
            list<BSureS_Basic_Info__c> lstSupplierBasicInfo;
            String strZoneOldName = '';
            Integer subZoneRecCount = [select count() from BSureS_SubZone__c where name =: subzoneObj.name and id !=: subzoneObj.id];
            if(subZoneRecCount > 0)
            {
                subzoneObj.addError('Record already Exists with the same SubZone Name');
            } 
            if(intCount < trigger.new.size())
            {
                BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
                if(trigger.new[intCount].Name != trigger.old[intCount].Name)
                {
                    strZoneOldName  = '%'+ trigger.old[intCount].Name +'%';
                    system.debug('strZoneOldName++++++++' + strZoneOldName);
                    lstSupplierBasicInfo = new list<BSureS_Basic_Info__c>([select id,Supplier_SubZones__c 
                                                                            from BSureS_Basic_Info__c 
                                                                            where Supplier_SubZones__c != null 
                                                                            and Supplier_SubZones__c like : strZoneOldName]);
                    for(BSureS_Basic_Info__c sObj:lstSupplierBasicInfo)
                    {
                        sObj.Supplier_SubZones__c = sObj.Supplier_SubZones__c.replace(trigger.old[intCount].Name, trigger.new[intCount].Name);
                    }
                    update lstSupplierBasicInfo;
                }
            }
        }
    }
    BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate; */
    
    // Modified Code
    
    list<string> subzoneNames = new list<string>();
    list<string> oldNames = new list<string>();
    list<string> oldsubZones = new list<string>();
    map<string,string> mapOldandNewZone = new map<string,string>();
    
    list<BSureS_SubZone__c> subzones = new list<BSureS_SubZone__c>();
    list<BSureS_Basic_Info__c> suppliers = new list<BSureS_Basic_Info__c>();
    
    for(BSureS_SubZone__c szoneObj:trigger.new)
    {
        subzoneNames.add(szoneObj.Name);
    }
    
    subzones = [select Id,Name from BSureS_SubZone__c where Name in:subzoneNames];
    
    if(Trigger.isInsert){
        for(BSureS_SubZone__c eZone: subzones){
            for(BSureS_SubZone__c newZone:trigger.new)
            {
                if(eZone.Name == newZone.Name )
                    newZone.addError('Record already Exists with the same Sub Zone Name');
            }
        }
    }
    if(Trigger.IsUpdate){
        
        for(BSureS_SubZone__c newSZone:trigger.new)
        {   
            oldNames.add('%'+Trigger.OldMap.get(newSZone.Id).Name+'%'); 
            oldsubZones.add(Trigger.OldMap.get(newSZone.Id).Name);
            mapOldandNewZone.put(Trigger.OldMap.get(newSZone.Id).Name,newSZone.Name);
            for(BSureS_SubZone__c extSZone: subzones){
                if(extSZone.Name == newSZone.Name && extSZone.Id != newSZone.Id  )
                    newSZone.addError('Record already Exists with the same Sub Zone Name');
            }
        }
        
        suppliers = [SELECT id,Supplier_SubZones__c 
                     FROM BSureS_Basic_Info__c 
                     WHERE Supplier_SubZones__c != null 
                     AND Supplier_SubZones__c Like: oldNames];
                                 
        for(string szname:oldsubZones){
            for(BSureS_Basic_Info__c sup:suppliers){ 
                if(sup.Supplier_SubZones__c.Contains(szname)){
                    sup.Supplier_SubZones__c = sup.Supplier_SubZones__c.replace(szname,mapOldandNewZone.get(szname));
                }
            }
        }
        BSureS_CommonUtil.isSupplierUpdate = isSuppUpdate;
        if(!suppliers.isEmpty())
            update suppliers;
                    
        BSureS_CommonUtil.isSupplierUpdate = !isSuppUpdate;
         
    } 
    
}