trigger BSureS_TaskOwnerCheck on Task (before insert, before update) {
  
  string SPrefix = Schema.SObjectType.BSureS_Basic_Info__c.getKeyPrefix();
    
  map<Id,User> mapUser = new map<Id,User>([select Id,Profile.Name from User limit 10000]);
  
  for(Task t:Trigger.new){
    if(t.WhatId != null &&  string.valueof(t.WhatId).startsWith(SPrefix) && mapUser.get(t.Ownerid).Profile.Name.Contains('BSureC') ){
      t.OwnerId.addError(system.Label.the_owner_should_be_Supplier_User_or_Admin);
    }    
  }
}