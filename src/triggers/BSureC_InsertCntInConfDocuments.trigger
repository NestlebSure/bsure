/*************************************************************************************************
*       Author           : B.Anupreethi
*       Purpose          : Trigger to insert the count in the parent object(Confidential document Section)when ever records inderted in discussion object
*       Change History   :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       13/02/2013               B.Anupreethi                Initial Version
**************************************************************************************************/
trigger BSureC_InsertCntInConfDocuments on BSureC_Confidential_Document_Discussion__c (after insert) 
{
    Set<String> lst_docsectionids=new Set<String>();
    List<BSureC_Confidential_Documents_Section__c> Objconfsection=new List<BSureC_Confidential_Documents_Section__c>();
    for(BSureC_Confidential_Document_Discussion__c objDiscussion: trigger.new)
    {
        if(objDiscussion.Parent_ID__c==null || objDiscussion.Parent_ID__c=='')
        {
        lst_docsectionids.add(objDiscussion.BSureC_Confidential_Documents_Section__c);
        }
        /*if(objDiscussion.Parent_ID__c==null || objDiscussion.Parent_ID__c=='')
        {
            list<BSureC_Confidential_Documents_Section__c> lstSection = new list<BSureC_Confidential_Documents_Section__c>();
            
            lstSection = [SELECT id, DiscussionsCount__c 
                          FROM BSureC_Confidential_Documents_Section__c 
                          WHERE id=:objDiscussion.BSureC_Confidential_Documents_Section__c];
            
            decimal iCnt = lstSection[0].DiscussionsCount__c;
            if(iCnt == null)
            iCnt = 0;
            iCnt++;
            lstSection[0].DiscussionsCount__c = iCnt;
            
            update lstSection;
        }*/
    }
    if(BSureC_Confidential_Documents_Section__c.SObjectType.getDescribe().isQueryable()){
	    for(BSureC_Confidential_Documents_Section__c ObjDocdiscussion:[SELECT id, DiscussionsCount__c 
	                                                                          FROM BSureC_Confidential_Documents_Section__c 
	                                                                          WHERE id in : lst_docsectionids])
	      {
	            decimal iCnt = ObjDocdiscussion.DiscussionsCount__c;
	            if(iCnt == null)
	            iCnt = 0;
	            iCnt++;
	            ObjDocdiscussion.DiscussionsCount__c = iCnt;
	            
	            Objconfsection.add(ObjDocdiscussion);
	        
	      }
    }
      
	  if (BSureC_Confidential_Documents_Section__c.sObjectType.getDescribe().isUpdateable())
      update Objconfsection;
    
}