/************************************************************************************************       
Controller Name         : BSureC_CreditDataSectionBatch       
Date                    : 11/19/2012        
Author                  : Santhosh Palla       
Purpose                 : To Inser the Credit Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/08/2012      Santhosh.Palla            Initial Version
                          23/01/2013      VINU PRATHAP          Modified to get the logged in user Email address
**************************************************************************************************/

global with sharing class BSureC_CreditDataSectionBatch implements Database.Batchable<SObject> 
{
    global String str_credit_data_stage=null;
    public list<BSureC_Credit_Data_Stage__c> lst_Credit_Data_Stage = new list<BSureC_Credit_Data_Stage__c>();
    public List<BSureC_Credit_Data_Publish__c> lst_Credit_Data_Publish = new List<BSureC_Credit_Data_Publish__c>();
    /// <summary>
    /// constructor
    /// </summary>
    public BSureC_CreditDataSectionBatch()
    {
	  str_credit_data_stage='SELECT Id,of_Limit__c,Slow__c,Risk_Category__c,Customer_Basic_Info_Id__c,Record_Date__c,Prompt__c,Last_Review__c,Group_Id__c,Exposure__c,Discount__c,Average_Days_Slow__c,Credit_Limit__c,CE__c,Bill_to_Account__c FROM BSureC_Credit_Data_Stage__c WHERE Exception__c=NULL AND Publish_Flag__c = false';
    }
    
    public class Wrapcustinfo
    {
        public String custname;
        public decimal creditlimit;
        public decimal Applimit;
    }
    /// <summary>
    /// start method fires when class get executes
    /// </summary>
    /// <param name="BC"></param>
     global Iterable<sObject> start (Database.BatchableContext ctx)
     {         
     	  if(Schema.sObjectType.BSureC_Credit_Data_Stage__c.isAccessible())
     	  {
        	return Database.query(str_credit_data_stage);
     	  }
     	  return null;	
     }  
     /// <summary>
    /// execute method executes after start method to insert records in batch into publish object
    /// </summary>
    /// <param name="BC"></param>
    /// <param name="scope"></param>
     global void execute(Database.BatchableContext BC, list<BSureC_Credit_Data_Stage__c> scope)
     {
        lst_Credit_Data_Stage = new list<BSureC_Credit_Data_Stage__c>();
        Set<String> setCustIds=new set<String>();
        map<String,Decimal> map_custidlimit=new map<String,Decimal>();
        List<Wrapcustinfo> lst_custwarp=new List<Wrapcustinfo>();
        
        for(BSureC_Credit_Data_Stage__c  obj : scope)
        {
            BSureC_Credit_Data_Publish__c rec_crdit_data_publish = new BSureC_Credit_Data_Publish__c();
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.CE__c.isUpdateable() || Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.CE__c.isCreateable()){
                rec_crdit_data_publish.CE__c = obj.CE__c;
            }

            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Average_Days_Slow__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Average_Days_Slow__c.isCreateable()){           
                rec_crdit_data_publish.Average_Days_Slow__c = obj.Average_Days_Slow__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Bill_to_Account__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Bill_to_Account__c.isCreateable()){           
                rec_crdit_data_publish.Bill_to_Account__c = obj.Bill_to_Account__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Credit_Limit__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Credit_Limit__c.isCreateable()){                       
                rec_crdit_data_publish.Credit_Limit__c = obj.Credit_Limit__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Discount__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Discount__c.isCreateable()){                                   
                rec_crdit_data_publish.Discount__c = obj.Discount__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Exposure__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Exposure__c.isCreateable()){                                   
                rec_crdit_data_publish.Exposure__c = obj.Exposure__c;
            }

            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Group_Id__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Group_Id__c.isCreateable()){                                               
                rec_crdit_data_publish.Group_Id__c = obj.Group_Id__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Last_Review__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Last_Review__c.isCreateable()){                                               
                rec_crdit_data_publish.Last_Review__c = obj.Last_Review__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.of_Limit__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.of_Limit__c.isCreateable()){                                               
                rec_crdit_data_publish.of_Limit__c = obj.of_Limit__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Prompt__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Prompt__c.isCreateable()){                                               
                rec_crdit_data_publish.Prompt__c = obj.Prompt__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Record_Date__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Record_Date__c.isCreateable()){                                               
                rec_crdit_data_publish.Record_Date__c = obj.Record_Date__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Risk_Category__c.isUpdateable()|| 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Risk_Category__c.isCreateable()){                                               
                rec_crdit_data_publish.Risk_Category__c = obj.Risk_Category__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Slow__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.Slow__c.isCreateable()){                                               
                rec_crdit_data_publish.Slow__c = obj.Slow__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.BSureC_Customer_Basic_Info__c.isUpdateable() || 
                Schema.sObjectType.BSureC_Credit_Data_Publish__c.fields.BSureC_Customer_Basic_Info__c.isCreateable()){                                               
                rec_crdit_data_publish.BSureC_Customer_Basic_Info__c = obj.Customer_Basic_Info_Id__c;
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Publish_Flag__c.isUpdateable() ||
                Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Publish_Flag__c.isCreateable()){ 
            obj.Publish_Flag__c=true;//for inserted records making flag as true to update in stgae
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Status__c.isUpdateable() ||
                Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Status__c.isCreateable()){
                obj.Status__c='Published';
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Publish_Date__c.isUpdateable() ||
                Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Publish_Date__c.isCreateable()){            
                obj.Publish_Date__c=Date.valueof(system.now());
            }
            
            if (Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Status_Resource_Value__c.isUpdateable() ||
                Schema.sObjectType.BSureC_Credit_Data_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
                obj.Status_Resource_Value__c='/resource/1242640894000/Green';
            }
            setCustIds.add(obj.Customer_Basic_Info_Id__c);
            map_custidlimit.put(obj.Customer_Basic_Info_Id__c,obj.Credit_Limit__c);
            lst_Credit_Data_Publish.add(rec_crdit_data_publish);
            lst_Credit_Data_Stage.add(obj);
        }
        //system.debug('-----lst_Credit_Data_Publish---'+lst_Credit_Data_Publish);
        //NAGA Check if the user has read access on the BSureC_Customer_Basic_Info__c object
        if (Schema.sObjectType.BSureC_Customer_Basic_Info__c.isAccessible())
        {
	        for(BSureC_Customer_Basic_Info__c Objcust:[SELECT id,Credit_Limit__c,Customer_Name__c FROM BSureC_Customer_Basic_Info__c WHERE id in:setCustIds])
	        {
	            Wrapcustinfo Objwrap=new Wrapcustinfo();
	            
	            if(Objcust.Credit_Limit__c!=map_custidlimit.get(String.valueOf(Objcust.id)))
	            {
	                Objwrap.Applimit=Objcust.Credit_Limit__c;
	                Objwrap.custname=Objcust.Customer_Name__c;
	                Objwrap.creditlimit=map_custidlimit.get(String.valueOf(Objcust.id));
	                lst_custwarp.add(Objwrap);
	            }
	            
	        }
        }
        
        //Database.insert(lst_Credit_Data_Publish);
        try
        {
        	if (BSureC_Credit_Data_Publish__c.sObjectType.getDescribe().isCreateable())
            Database.insert(lst_Credit_Data_Publish);
            try
            {
            	if (BSureC_Credit_Data_Stage__c.sObjectType.getDescribe().isUpdateable())
                Database.update(lst_Credit_Data_Stage);
            }
            catch(Exception e)
            {
                //system.debug('--Exception-- in Update-'+e);
            }
        }
        catch(Exception e)
        { 
            //system.debug('--Exception-- in insert-'+e);
        }
        
     }
     /// <summary>
    /// finish method executes after completing all batches execution
    /// </summary>
    /// <param name="BC"></param>
     global void finish(Database.BatchableContext BC)
     {
       /* AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                                TotalJobItems, CreatedBy.Email
                                                    FROM AsyncApexJob 
                                                    WHERE Id =:BC.getJobId()];
      
      //Modified by VINU PRATHAP    
      String userId = UserInfo.getUserId();
      User activeUser = [Select Email From User where Id = : userId limit 1];
      String userEmail = activeUser.Email;      
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();      
      //Modified By VINU PRATHAP
      //String[] toAddresses = new String[] {'santhosh.p@vertexcs.com'};
      string[] toAddresses = new string[]{userEmail};
      mail.setToAddresses(toAddresses);
      //system.debug('TestAdd----'+toAddresses);      
      mail.setSubject('Borrowed book batch job for  ' + system.today()+ a.Status);
      mail.setPlainTextBody
       ('The batch Apex job processed on spend history publish list '+'Number of records' + a.TotalJobItems +
       ' inserted during batch job'+ a.NumberOfErrors + ' number of failures.');
       
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
     } 
}