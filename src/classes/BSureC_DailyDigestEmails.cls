/************************************************************************************************       
Controller Name         : BSureC_DailyDigestEmails       
Date                    : 02/14/2013        
Author                  : Aditya Vemuri       
Purpose                 : Scheduler To send Daily Digest emails      
Change History          : Date           Programmer              Reason       
						--------------   -------------------     -------------------------       
                          02/14/2013     Aditya Vemuri           Initial Version
**************************************************************************************************/
global with sharing class BSureC_DailyDigestEmails implements Schedulable  
{
    string strNSubject = '';
    string strNBody = '';
    string strDDSubject = '';
    string strDDBody = '';    
    string strForumName = '';
    
    //list<BSureC_Email_Queue__c> lstEmailMsgs;
    
	global BSureC_DailyDigestEmails()
	{
	} 
	
	global void execute(SchedulableContext sc)
	{
		string strNew = 'NEW';
		string strYes = 'YES';
		
    	strForumName = BSureC_CommonUtil.getConfigurationValues('BSureC_ForumName').get(0);
        transient list<Messaging.SingleEmailMessage> objMailsList = new list<Messaging.SingleEmailMessage>(); 
        map<string, list<BSureC_Email_Queue__c>> mapEmailQueue = new map<string, list<BSureC_Email_Queue__c>>();
        
        GetEmailTemplates();
        
		list<BSureC_Email_Queue__c> lstEmailMsgs = new list<BSureC_Email_Queue__c>([SELECT Id, Recipient_Address__c, Is_Daily_Digest__c, Email_Subject__c, Email_Status__c, 
							        	Email_Priority__c, Email_Body__c, Recipient_Name__c, Send_Immediate__c 
							     	FROM BSureC_Email_Queue__c 
							        WHERE Email_Status__c = :strNew
							        AND Send_On_Date__c <= TODAY 
							    	AND Is_Daily_Digest__c = :strYes 
							    	AND Send_Immediate__c = false 
							    	ORDER BY CreatedDate limit 1000]);
				
		if(lstEmailMsgs != null && lstEmailMsgs.size() > 0){
												  	
			for(BSureC_Email_Queue__c objEachMail : lstEmailMsgs)
	        {
	        	string strUniqueKey = objEachMail.Recipient_Address__c.trim() + '~' +
	                	 (objEachMail.Is_Daily_Digest__c != null && objEachMail.Is_Daily_Digest__c != '' 
	                	 	? objEachMail.Is_Daily_Digest__c.trim() + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase() 
	                	 	: 'YES' + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase());
	            
	            if(mapEmailQueue.containsKey(strUniqueKey))
	            {
	            	list<BSureC_Email_Queue__c> objListEQ = mapEmailQueue.get(strUniqueKey);
	            	objListEQ.add(objEachMail);
	            	mapEmailQueue.put(strUniqueKey, objListEQ);
	            }
	            else
	            {
	            	list<BSureC_Email_Queue__c> objListEQ = new list<BSureC_Email_Queue__c>();
	            	objListEQ.add(objEachMail);
	            	mapEmailQueue.put(strUniqueKey, objListEQ);
	            }
	            strUniqueKey='';
	        }
	        //system.debug('mapEmailQueue+++++++++' + mapEmailQueue.size());
	        if(mapEmailQueue != null && mapEmailQueue.size() > 0)
	        {
	        	set<string> setUniqueKey = mapEmailQueue.keySet();
	        	for(string toAddressKey : setUniqueKey)
	        	{
	        		if(toAddressKey.contains('YESFALSE'))
	        		{
	        			objMailsList.add(GenerateConsolidatedMail(mapEmailQueue.get(toAddressKey)));
	        		}	        		
	        	}
	        	setUniqueKey = null;
	        }
	        Messaging.sendEmail(objMailsList);
	        UpdateEmailQueue(lstEmailMsgs);
	        
	        mapEmailQueue = null;
	        objMailsList = null;
	        lstEmailMsgs = null;
		}
	}
	
	public void GetEmailTemplates()
    {
    	set<string> setTemplateNames = new set<string>{'BSureC_Notifications', 'BSureC_DailyDigest'};
    	 list<EmailTemplate> lstEmailTemplates = [SELECT Subject, Name, IsActive, Body
												 FROM EmailTemplate
												 WHERE Name IN: setTemplateNames
												 AND IsActive =: true];
    	
    	if(lstEmailTemplates != null && lstEmailTemplates.size() > 0)
    	{
    		for(EmailTemplate objEach : lstEmailTemplates)
    		{
    			if(objEach.Name.contains('DailyDigest'))
    			{
    				strDDBody = objEach.Body;
    				strDDSubject = objEach.Subject;
    			}
    			else if(objEach.Name.contains('Notifications'))
    			{
    				strNBody = objEach.Body;
    				strNSubject = objEach.Subject;
    			}
    		}
    	}
    }
    
    public Messaging.SingleEmailMessage GenerateConsolidatedMail(list<BSureC_Email_Queue__c> objListEQ)
    {
    	string strEmailAdds = '';
    	string strRecipientName = '';
    	//string strSubject = '';
    	
    	list<string> lstEmailAdd = new list<string>();
    	string strSubject = strDDSubject.replace('%%DATE%%', System.today().format()); 
    	transient string strEmailBody = '<table width="100%" border="0" cellpadding="10" cellspacing="10">';
    	
    	Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
    	
    	for(BSureC_Email_Queue__c objEachMsg : objListEQ)
    	{
    		strEmailAdds = objEachMsg.Recipient_Address__c;
    		strRecipientName = objEachMsg.Recipient_Name__c;
    		strEmailBody += '<tr><td width="100%" style="border-bottom: 1px solid;">'
    		            + '<b>' + objEachMsg.Email_Subject__c + '</b></br>'
    		            + '______________________________________________________</br></br>'
    		            + objEachMsg.Email_Body__c +'</br></td></tr>';
    	}
    	strEmailBody += '</table>';
    	transient string strFinalBody = strDDBody.replace('%%USER%%', strRecipientName != null && strRecipientName != '' ? strRecipientName : '');
    	strFinalBody = strFinalBody.replace('%%EMAILBODY%%', strEmailBody).replace('\r\n', '</br></br>');
    	//strFinalBody = strFinalBody.replace('\r\n', '</br></br>');
    	lstEmailAdd.add(strEmailAdds);
    	
    	objMail.saveAsActivity = false;
        objMail.setToAddresses(lstEmailAdd);
        objMail.setUseSignature(false);
        objMail.setSaveAsActivity(false);
        objMail.setSubject(strSubject); 
        objMail.setSenderDisplayName(strForumName);
        objMail.setHtmlBody(strFinalBody);
    	return objMail;
    }
    
    public void UpdateEmailQueue(list<BSureC_Email_Queue__c> lstEmailMsgs)
    {
    	try
    	{
    		for(BSureC_Email_Queue__c objEachMail : lstEmailMsgs)
	    	{
	    		objEachMail.Email_Status__c = 'SENT';
	    		objEachMail.Delivered_Date__c = Datetime.now();
	    	}
	    	Database.update(lstEmailMsgs);
    	}
    	catch(Exception ex)
    	{
    		//system.debug('Exception in UpdateEmailQueue ' + ex);
    	}
    }
}