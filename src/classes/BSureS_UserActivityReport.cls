/***********************************************************************************************
* Controller Name :  BSureS_UserActivityReport
* Date : 27 Dec 2012
* Author : Veereandrnath Jella
* Purpose : This class written to generate report for which suppliers has been done for review  
* Change History :
* Date Programmer Reason
* -------------------- ------------------- -------------------------
* 12/27/2012 Veereandranath.j Initial Version
* 08/11/2013 Veereandranath.j Code Review
**************************************************************************************************/
global with sharing class BSureS_UserActivityReport { 
    public string   strZoneId{get;set;} // to store Zone Id
    public string   strSubZoneId{get;set;} // to store Sub Zone Id
    public string   strCountryId{get;set;}  // to store Country Id
    public string   strAnalystId{get;set;}  // to store Country Id
    public string   strSupplierName{get;set;}   // to hold supplier Name
    public string   strSupplierId{get;set;} 
    public Integer  pageNumber;//used for pagination
    public Integer  pageSize;//used for pagination
    public Integer  totalPageNumber;//used for pagination
    public Integer  PrevPageNumber {get;set;}//used for pagination 
    public Integer  NxtPageNumber {get;set;}//used for pagination
    public Integer  NlistSize {get;set;}//used for pagination
    public Integer  Endlst {get;set;}//used for pagination
    public integer  totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public string   strFromDate{get;set;} // to hold from date
    public string   strToDate{get;set;} // to hold to Date
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records  
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public list<SelectOption> AnalystOptions{get;set;} // selectOptins for list of User(Analyst) Records
    public list<User> lstUsers{get;set;}
    public list<UserHistory> lstUserHistory{get;set;}
    public list<UserHistory> lstUserHistoryPagination{get;set;}
    public BSureS_Basic_Info__c objSupplier{get;set;} // to display from and to dates with standard datepicker   
    private string  sortDirection = 'ASC'; 
    private string  sortExp = 'Name';
    private string strPM = 'Procurement Manager';
    
    public  string  strAnalystName {get;set;}
    public  boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public  string strAnalystSearch{get;set;}
    //public  String[] strAnalystProfiles = new String[] {'BSureS_Analyst','BSureS_CreditAdmin','BSureS_Manager'};//Profiles to be allowed
    public  String[] strAnalystProfiles = new String[] {'BSureS_Analyst','BSureS_CreditAdmin','BSureS_Manager','BSureS_Buyer'};
    public  list<User> lstAnalystUser{get;set;} 
    public  string  strTodayDate{get;set;}
    public Integer UARpageSize{get;set;}
    
    public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() {  
        Search();
    }
    public pagereference Cancel(){
        return page.BSureS_Reports;
    }
    
    public BSureS_UserActivityReport()
    {
    	if(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size') != null )
        UARpageSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').get(0));
        list<BSureS_User_Additional_Attributes__c> AAttributes = new list<BSureS_User_Additional_Attributes__c>();
        AAttributes = BSureS_CommonUtil.DefaultUserAttributes(UserInfo.getUserId()); 
        
        if(AAttributes != null && AAttributes.size() > 0){
            strZoneId       = AAttributes[0].BSureS_Zone__c;
            strSubZoneId    = AAttributes[0].BSureS_Sub_Zone__c;
            strCountryId    = AAttributes[0].BSureS_Country__c;
        }
        strTodayDate = Date.today().format();
        objSupplier = new BSureS_Basic_Info__c();
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
        lstUserHistory = new list<UserHistory>();
        
        lstUserHistoryPagination = new list<UserHistory>(); 
        Date dtYear = Date.Today().addDays(-365);           
        Date dtToday = Date.Today();        
        getZones();
        getSubZones();
        getCountries();
    } 
   // <Summary> 
   // Method Name : getZones
   // </Summary>
   // Description : query all the zones 
    public void getZones()
    {  
        BSureS_CommonUtil.isReport = true;
        zoneOptions = new list<SelectOption>();
        zoneOptions = BSureS_CommonUtil.getZones();
        getCountries();
    }
     // <Summary> 
   // Method Name : getSubZones
   // </Summary>
   // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones
    public void getSubZones()
    {   
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
        getCountries();
        BSureS_CommonUtil.isReport = true;        
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        
    }
     // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries()
    {       
       strCountryId = (strZoneId == 'ALL' || strSubZoneId == 'ALL'?'ALL':strCountryId );
            
        //getAnalysts();
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();            
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId); 
       
    }
    /* public void getAnalysts()
    {   
        AnalystOptions = new list<selectOption>();
        if(strCountryId == 'ALL')
            strAnalystId = 'ALL';
            
        AnalystOptions  = BSureS_CommonUtil.getAnalysts(strZoneId, strSubZoneId, strCountryId);
       
    }*/
     // <Summary> 
    //Method Name : ExportToExcel
    // </Summary>
    //Description : To export all records into csv file
    public Pagereference ExportToExcel(){
        Pagereference pageref = page.BSureS_UserActivityReportExport;
        return pageref; 
    }
      public void showBuyerDetails()
    {
        showeditrecs=true;        
        ////system.debug('category==ShowDetail===Supplier=Name=='+ strsupplierCategoryName.Name);  
       // profile getprofileId = [select id from profile where name = 'BSureS_Buyer'];
       //Selecting users with Profile BSureC_Analyst
       //Checking Query permession
       if(Schema.Sobjecttype.User.isQueryable())
       lstAnalystUser=[Select Id, Name from User where Profile.Name IN:strAnalystProfiles and UserRole.Name !=: strPM  and IsActive=true order By Name Asc];
        
    }
     public void closepopup() 
    {
        string strSelectedAnalyst = apexpages.currentpage().getparameters().get('Analystid');
        strAnalystName=strSelectedAnalyst;        
        showeditrecs=false; 
    }
  
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        string strAnalSrch = '%'+strAnalystSearch+'%';
        if(strAnalystSearch != null && strAnalystSearch != '' && Schema.Sobjecttype.User.isQueryable()) 
        {
            lstAnalystUser = [Select Id, Name from User where Name like: strAnalSrch AND Profile.Name IN:strAnalystProfiles and UserRole.Name !=: strPM AND IsActive=true order By Name Asc];
        }
        else
        {
            lstAnalystUser = [Select Id, Name from User where IsActive=true AND Profile.Name IN:strAnalystProfiles and UserRole.Name !=: strPM order By Name Asc];           
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
     // <Summary> 
    //Method Name : Search
    // </Summary>
    //Description : its Search the records each user how many topics,documents and reviews has been created
    public void Search(){
        
        try
        {   
            
            if(!BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').isEmpty())
            	UARpageSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').get(0));
            map<Id,list<LoginHistory>>                          mapLoginHistory     = new map<Id,list<LoginHistory>>();
            map<Id,list<BSureS_Credit_Review_Section__c>>       maptopicOnReview    = new map<Id,list<BSureS_Credit_Review_Section__c>>();
            map<Id,list<BSureS_Confidential_Info_Section__c>>   maptopicOnConf      = new map<Id,list<BSureS_Confidential_Info_Section__c>>();
            map<Id,list<BSureS_Credit_Review_Section__c>>       mapFileOnReview     = new map<Id,list<BSureS_Credit_Review_Section__c>>();
            map<Id,list<BSureS_Confidential_Info_Section__c>>   mapFileOnConf       = new map<Id,list<BSureS_Confidential_Info_Section__c>>();
            list<BSureS_User_Additional_Attributes__c>          lstAddAttributes    = new list<BSureS_User_Additional_Attributes__c> ();
            map<Id,list<BSureS_Credit_Analysis_Section__c>>     mapTopicOnCA        = new map<Id,list<BSureS_Credit_Analysis_Section__c>>();
            map<Id,list<BSureS_Credit_Analysis_Section__c>>     mapFileOnCA         = new map<Id,list<BSureS_Credit_Analysis_Section__c>>();
            
            map<Id,list<BSureS_Credit_Analysis__c>>             mapReviewsDone      = new map<Id,list<BSureS_Credit_Analysis__c>>();
            list<BSureS_Credit_Analysis__c>                     lstCreditAnalysis   = new list<BSureS_Credit_Analysis__c>();
            
            Date dtFrom;
            Date dtTo;
         
            dtFrom  = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
            dtTo    = ((strToDate != null && strToDate != '')?date.parse(strToDate):null);  
             
            set<Id> setUserIds = new set<Id>();
            lstUsers = new list<User>();
            lstUsers.Clear(); 
            lstUserHistory.clear();
            list<User> allUsers = new list<User>();
            
            
            
           list<string> doNotUser = new list<string>(); 
           BSure_Configuration_Settings__c bc = new BSure_Configuration_Settings__c();
           // Checking null condition
           if(BSure_Configuration_Settings__c.getInstance('BSureS Admin Id') != null)
           	bc = BSure_Configuration_Settings__c.getInstance('BSureS Admin Id');
           if(bc != null && bc.Parameter_Value__c != null ){
                if(bc.Parameter_Value__c.contains(','))
                    doNotUser = bc.Parameter_Value__c.split(',');
                else
                    doNotUser.add(bc.Parameter_Value__c);       
           }
           
                
              
            string strBSure='BSureS%';
            string strQuery = 'select   id,User__c '+   
                                        'FROM BSureS_User_Additional_Attributes__c  '+ 
                                        'WHERE Id != null and User__r.Profile.Name like : strBSure and User__r.IsActive = true ';
            if(strZoneId != null && strZoneId != 'ALL')
                strQuery+=' AND BSureS_Zone__c =:strZoneId  ';
            if(strSubZoneId != null && strSubZoneId != 'ALL')
                strQuery+=' AND BSureS_Sub_Zone__c =:strSubZoneId  ';
            if(strCountryId != null && strCountryId != 'ALL')
                strQuery+=' AND BSureS_Country__c =:strCountryId  '; 
            if(!doNotUser.isEmpty())
            	strQuery+=' AND User__c Not In:doNotUser ';
                
                
            
            
            if(strZoneId != null && strZoneId != 'ALL' || strSubZoneId != null && strSubZoneId != 'ALL' || strCountryId != null && strCountryId != 'ALL' ){
            	// Checking Query permession
            	if(Schema.Sobjecttype.BSureS_User_Additional_Attributes__c.isQueryable())
                	lstAddAttributes = database.query(strQuery);            
                
                for(BSureS_User_Additional_Attributes__c BSureA:lstAddAttributes)
                {
                    setUserIds.add(BSureA.User__c);
                }   
            }else if(Schema.Sobjecttype.user.isQueryable()){
                allUsers = [select Id,Name from user where Profile.Name in:strAnalystProfiles  and UserRole.Name !=: strPM and Id Not In:doNotUser and IsActive= true];
                for(User u:allUsers){
                    setUserIds.add(u.Id);
                }
            }
            
            string userQuery = ' SELECT Id,Name '+ 
                               ' FROM User Where IsActive = true  ' ;        
            
            if(strAnalystName != null && strAnalystName != '')
            {
                 strAnalystName = strAnalystName;
                 string Analyst = strAnalystName.trim() + '%';
                 userQuery += ' AND Name like:Analyst AND profile.Name in:strAnalystProfiles and Id In:setUserIds';
            }else {
                userQuery+=  ' AND profile.Name in:strAnalystProfiles  and UserRole.Name !=: strPM and Id In:setUserIds ';
            }  
            
            string sortFullExp = sortExpression  + ' ' + sortDirection; 
            userQuery+= ' order by ' + sortFullExp +' limit 2000'; 
            if(Schema.Sobjecttype.user.isQueryable()) 
            	lstUsers = database.query(userQuery);
           
           
            
            maptopicOnReview    =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Review_Section__c',dtFrom,dtTo,'Topic');
            //maptopicOnStatus  =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Status_Section__c',dtFrom,dtTo,'Topic');
            maptopicOnConf      =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Confidential_Info_Section__c',dtFrom,dtTo,'Topic');
            mapTopicOnCA        =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Analysis_Section__c',dtFrom,dtTo,'Topic');
            mapFileOnReview     =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Review_Section__c',dtFrom,dtTo,'File');
            //mapFileOnStatus   =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Status_Section__c',dtFrom,dtTo,'File');
            mapFileOnConf       =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Confidential_Info_Section__c',dtFrom,dtTo,'File');
            mapFileOnCA         =   BSureS_CommonUtil.getUserMapwithlist(setUserIds,'BSureS_Credit_Analysis_Section__c',dtFrom,dtTo,'File');
                                                                    
            string strReviewStatus='Completed';
            string strCAQuery = ' SELECT    id,createdById,CreatedBy.Name  '+
                                ' FROm BSureS_Credit_Analysis__c '+
                                ' WHERE Review_Complete_Date__c >=:dtFrom and Review_Complete_Date__c <=:dtTo ';
                strCAQuery   += ' AND Review_Status__c =: strReviewStatus ';
            // Checking Query permession    
            if(Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())    
            	lstCreditAnalysis = database.query(strCAQuery);
             
            for(BSureS_Credit_Analysis__c objCA:lstCreditAnalysis) 
            {
                list<BSureS_Credit_Analysis__c> lstCA = new list<BSureS_Credit_Analysis__c>();
                lstCA = mapReviewsDone.get(objCA.createdById);
                if(lstCA == null )
                {
                    lstCA = new list<BSureS_Credit_Analysis__c>();
                    lstCA.add(objCA);
                    mapReviewsDone.put(objCA.createdById,lstCA);
                }else 
                {
                    lstCA.add(objCA);
                }   
                
            }
            
            list<LoginHistory> userHistory;
            if(Schema.Sobjecttype.LoginHistory.isQueryable()) 
             userHistory= [SELECT   Id,
                                    UserId,
                                    LoginTime 
                                    FROM LoginHistory 
                                    WHERE UserId in:setUserIds 
                                    AND LoginTime >=:dtFrom  
                                    AND LoginTime <=:dtTo limit 40000 ];
                                                        
            
            for(LoginHistory LH:userHistory)
            {
                list<LoginHistory> History = new list<LoginHistory>();  
                History = mapLoginHistory.get(LH.UserId);
                if(History  == null ){
                    History = new list<LoginHistory>(); 
                    History.add(LH);
                    mapLoginHistory.put(LH.UserId,History);
                }else{  
                    History.add(LH);
                }   
            } 
            
            
            
            for(User u:lstUsers){
                UserHistory wc = new UserHistory();
                wc.u                =   u;
                wc.nLogis           =   (mapLoginHistory.get(u.Id) != null ? mapLoginHistory.get(u.Id).size():0);
                wc.Commentsmade     =    (maptopicOnReview.get(u.Id) != null? maptopicOnReview.get(u.Id).Size():0)+ (maptopicOnConf.get(u.Id) != null? maptopicOnConf.get(u.Id).Size():0)+(mapTopicOnCA.get(u.Id) != null? mapTopicOnCA.get(u.Id).Size():0);
                wc.DocumentsPosted  =    (mapfileOnReview.get(u.Id) != null? mapfileOnReview.get(u.Id).Size():0)+ (mapfileOnConf.get(u.Id) != null? mapfileOnConf.get(u.Id).Size():0)+(mapFileOnCA.get(u.Id) != null? mapFileOnCA.get(u.Id).Size():0);
                wc.ReviewsDone      =   (mapReviewsDone.get(u.Id)  != null ? mapReviewsDone.get(u.Id).size():0); 
                lstUserHistory.add(wc); 
            }           
        }Catch(Exception ex){
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage());
            ApexPages.addMessage(errormsg);
        }
        totallistsize= lstUserHistory.size();// this size for pagination
        
        pageSize = UARpageSize;
       
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber= lstUserHistoryPagination.size();
            PrevPageNumber=0;
        } 
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber= lstUserHistoryPagination.size();
            PrevPageNumber=1;
        }  
             
    }
     // <Summary> 
    //Method Name : UserHistory
    // </Summary>
    //Description : To collect Data from different objects
    public class UserHistory {
        public User         u               {get;set;}
        public integer      nLogis          {get;set;}
        public integer      Commentsmade    {get;set;}
        public integer      DocumentsPosted {get;set;}
        public integer      ReviewsDone     {get;set;}
        
    }
    public void BindData(Integer newPageIndex)
    {
       
        try
        {
            lstUserHistoryPagination = new list<UserHistory>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 2;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            if(lstUserHistory != NULL)
            {
                for(UserHistory b : lstUserHistory)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstUserHistoryPagination.add(b);// here adding files list
                    }
                }
            }
            pageNumber = newPageIndex;
            NlistSize = lstUserHistoryPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
     // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
       
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }


    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }


    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
   
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {   
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
   
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;
        }
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstUserHistoryPagination=new list<UserHistory>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(UserHistory a : lstUserHistory)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstUserHistoryPagination.add(a);// here adding the folders list      
                }
            } 
            pageNumber = newPageIndex;
            NlistSize=lstUserHistoryPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }

    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
}