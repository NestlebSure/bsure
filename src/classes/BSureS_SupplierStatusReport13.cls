public class BSureS_SupplierStatusReport13
{
    private limitWrapper[] thousandBlocks = new limitWrapper[]{};
   
    private final integer listLimit;
    
    public List<BSureS_Basic_Info__c> lstOfTest{get;set;} 
	public string testjson{get;set;}

   
    public BSureS_SupplierStatusReport13()
    {
        listLimit = 999;
        lstOfTest = [select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c,
        			Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,
        			Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c, Owner_Ship__c ,Contact_Name__c 
        			from BSureS_Basic_Info__c where id != null limit 1000];
        lstOfTest.sort();
		testjson = json.serialize(lstOfTest);
    }
   
    public limitWrapper[] thousandBlocks()
    {
        thousandBlocks = new limitWrapper[]{};
       
        integer counter = 0;
        integer loopCount = 0;
        BSureS_Basic_Info__c[] tmpcase = new BSureS_Basic_Info__c[]{};
       	
       	string strQuery = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c,'+ 
                            'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
                            'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
                             ', Owner_Ship__c ,Contact_Name__c '+
                            ' from BSureS_Basic_Info__c where id != null ';
                            
        for(BSureS_Basic_Info__c c:[select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c,
        			Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,
        			Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c, Owner_Ship__c ,Contact_Name__c 
        			from BSureS_Basic_Info__c where id != null ])
        {
            if(counter < listLimit)
            {
                tmpcase.add(c);
                counter++;
            }
            else
            {
                loopCount++;
                thousandBlocks.add(new limitWrapper(tmpcase,loopCount));
                tmpcase = new BSureS_Basic_Info__c[]{};
                tmpcase.add(c);
                counter = 0;
            }           
        }
       
        if(thousandBlocks.size() == 0)
        {
            loopCount++;
            thousandBlocks.add(new limitWrapper(tmpcase,loopCount));
        }
       
        return thousandBlocks;
    }
   
    public class limitWrapper
    {
        public BSureS_Basic_Info__c[] cases {get;set;}
        public integer blockNumber {get;set;}
        public limitWrapper(BSureS_Basic_Info__c[] accs, integer i)
        {
            cases = accs;
            blockNumber = i;
        }
       
    }
}