//Controller used for the AutoComplete Enhancement
global with sharing class BSureS_AutoComplete {
    @RemoteAction 
    global static SObject[] findSObjects(string obj, string qry, string addFields, string profilename,string UserLookup) 
    {
        
         //obj = 'Vertex_BSureS__'+Obj;
         //addFields = 'Vertex_BSureS__'+addFields;
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        //string[] lstprofiles = new string[]{'System Administrator','BSureS_Analyst','BSureS_Manager'};
        string[] lstRoles = new string[]{'CEO','Zone Manager','Sub-Zone Manager','Country Manager','Region Manager','Analyst','Buyer'};
        String strUser = 'User';
        List<String> fieldList=new List<String>(); 
        if (addFields != '')  
        fieldList = addFields.split(',');
        
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj.toLowerCase());
        if (sot == null) 
        {
            return null;
        }
        //system.debug('qry---'+qry+'--addFields='+addFields);
        /* Creating the filter text */
        
       
        String filter = ' like \'' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Id';
        
        /* If any additional field was passed, adding it to the soql */
        if (fieldList.size()>0) 
        {
            for (String s : fieldList)  
            {
                //s='Vertex_BSureS__'+s;
                soql += ', ' + s;
            }
        }
        soql += ' from ' + obj + ' where '+ addFields + filter;
        if(obj.equalsIgnoreCase(strUser))
        {
        	soql += ' and isActive = true AND Profile.Name like \'BSureS%\'';
        }
        if(profilename != null && profilename != '')
        {
           soql += ' and Profile.Name =\'' + profilename + '\''; 
           //soql += ' and UserRole.Name in:lstRoles';
        }
        if(UserLookup != null && UserLookup != '')
        { 
           soql += ' and UserRole.Name in:lstRoles';
        }
        
        soql += ' order by '+addFields +' limit 20';
        
        //system.debug('Qry======== '+soql);
        
        List<sObject> L = new List<sObject>();
        try 
        {
        	//Anu: For Dynamic SObject Query 08/14/2013
        	if(sot.getDescribe().isAccessible())
        	{
            	L = Database.query(soql);
        	}
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        return L;
   }
}