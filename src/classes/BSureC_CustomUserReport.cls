/***********************************************************************************************
* Controller Name : BSureC_CustomUserReport
* Date : 11th Nov,2012
* Author : Neha Jaiswal
* Purpose : To generate the report of users on the basis of some filter criteria.

* -------------------- ------------------- -------------------------
 */
 
global with sharing class BSureC_CustomUserReport 
 {
      public string strZoneId{get;set;}//to get zone id
      public list<User> listUser{get;set;}//to get list of user
      public list<CommonUserClass> objUserList{get;set;} //commonclass to get filed values from seprate objects
      public list<BSureC_AdditionalAttributes__c> listUserAttributes{get;set;}//list of user from BSureC_AdditionalAttributes__c
      public list<BSureC_UserCategoryMapping__c> listUsers=new list<BSureC_UserCategoryMapping__c>();
      public list<SelectOption> lstZoneOptions{get;set;}//list of selectoption for zones
      public string strSubZoneId{get;set;}//to pass subzoneId
      public list<SelectOption> lstSubZoneOptions{get;set;}//list of subzoneOptions
      public string strCountryId{get;set;}//to pass country id
      public list<selectOption> lstCoutryOptions{get;set;}//to pass list of options for country
      public string  sortDirection = 'ASC';  
      public string  sortExp = 'Full_Name__c';
      public set<string> strSubZone1{get;set;}//to hold sunZone id
      public set<string> strZone1{get;set;}//to hold zone id
      public set<string> strCountry1{get;set;}//to hold country id
      public string userView{get;set;}//to assign from param
      public ID userId{get;set;}//to hold user id
      public string strMainQuery {get;set;}
      public Boolean rensearch{get;set;}       
      public Boolean isexport{get;set;}
      public Boolean displayPanel{get;set;}
      public String[] strProfilesIncluded=new String[] {'System Administrator','BSureC_Analyst','BSureC_CreditAdmin','BSureC_Manager','BSureC_Collector','BSureC_Visitor'};
      //for pagination
      public integer counter=0;  //keeps track of the offset
      public integer list_size{get;set;} //sets the page size or number of rows
      public integer total_size{get;set;} //used to show user the total size of the list
      public Integer totalPageNumber;//used for pagination
      public Integer PrevPageNumber {get;set;}//used for pagination
      public Integer NxtPageNumber {get;set;}//used for pagination
      public Integer NlistSize {get;set;}//used for pagination
      public Integer Endlst {get;set;}//used for pagination                
      public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
      public List<BSureC_AdditionalAttributes__c> Pagenationlist {get;set;}         
      public string loginUserProfile {get;set;}
  
     //constructor
      public BSureC_CustomUserReport()
        {  
            displayPanel=false;
            rensearch=true;
           //  NxtPageNumber=0;
           //  PrevPageNumber=0;
            list_size = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));
            listUserAttributes = new list<BSureC_AdditionalAttributes__c>();
            Profile objCurProfile=[SELECT name FROM Profile WHERE id =:userInfo.getProfileId()];
            loginUserProfile = objCurProfile.name;
            if(loginUserProfile.equalsIgnoreCase('System Administrator'))
            {
                strZoneId='ALL';
                strSubzoneId='ALL';
                strCountryId='ALL';
                
            }
            else{          
            listUserAttributes = BSureC_CommonUtil.DefaultUserAttributes(UserInfo.getUserId()); 
            if(listUserAttributes != null && listUserAttributes.size() > 0){
            strZoneId       = listUserAttributes[0].BSureC_Zone__c;
            strSubZoneId    = listUserAttributes[0].BSureC_Sub_Zone__c;                                             
            strCountryId    = listUserAttributes[0].BSureC_Country__c;
            //idsflag=true;
                }
            }
            //displayrecords(); 
            BSureC_CommonUtil.blnflag=true;
            lstZoneOptions=BSureC_CommonUtil.getZones();
            lstSubZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
            lstCoutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
            
         }
         
      //For sorting grid in ascending and descending order
      public string sortExpression
      {
        get
        {
            return sortExp;
        }
        set
        {
            if(value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
    
     //For sorting grid in ascending and descending order
     public String getSortDirection()
     {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    //For sorting grid in ascending and descending order
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }    
    
    //For sorting grid in ascending and descending order
    public Pagereference SortData()
    {
         displayrecords();
         return null;
    }
    
   /// <summary>  
   /// getZones: get the list of zones from BSureC_Zone__c
   /// </summary>
   /// <returns>get the list of zones</returns>
    public pagereference getZone()
    {
         lstZoneOptions=new list<selectOption>();
         BSureC_CommonUtil.blnflag=true;
         lstZoneOptions=BSureC_CommonUtil.getZones();
         getSubZones();
         return null;
     }
         
   /// <summary>
   /// getSubZones: get the list of zonmes from BSureC_Zone__c
   /// </summary>
   /// <returns>get the list of zones</returns>
    public pagereference getSubZones()
    {
         strSubzoneId='ALL';
         strCountryId='ALL';
         lstSubZoneOptions=new list<selectoption>();
         BSureC_CommonUtil.blnflag=true;
         lstSubZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
         getCountries();
         return null;
    }
    
    /// <summary>
   /// getCountries: get the list of zonmes from BSureC_Zone__c
   /// </summary>
   /// <returns>get the list of countries</returns>
   public pagereference getCountries()    
     {   
             strCountryId='ALL';
             lstCoutryOptions=new list<selectOption>();
             lstCoutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
             return null;
     }
    
   /// <summary>
   /// CommonUserClass: common wrapper class from get the filed values from different objects
   /// </summary>
   /// <returns>get the filed values from different objects</returns>
    public class CommonUserClass
    {
        public string strFirstName {get; set;}
        public string strLastName {get; set;}
        public string strUserId {get; set;}
        public string strBusiness {get; set;}
        public string strcountry{get;set;}
        public string strSubZone{get;set;}
        public string strZone{get;set;}
        public string strFullName{get;set;}
        public string strEmail{get;set;}
        
    }
    
         
   /// <summary>
   /// displayrecords():  get the filed values from different objects
   /// </summary>
   /// <returns>get the filed values from different objects</returns>
    public pagereference displayrecords(){
        displayPanel=true;
        NxtPageNumber=0;
        PrevPageNumber=0;
        counter = 0;
        objUserList = new list<CommonUserClass>();
        map<id,CommonUserClass> objMapUserDetails = new map<id, CommonUserClass>(); 
        map<id,CommonUserClass> objMapUserDetails1=new  map<id, CommonUserClass>(); 
        if(strZoneId=='ALL'){
            strZoneId=null;
            
        }
        if(strSubZoneId=='ALL'){
            strSubZoneId=null;
        }
        
        if(strCountryId=='ALL'){
            strCountryId=null;
        } 
        
        listUserAttributes = new list<BSureC_AdditionalAttributes__c>();
        strMainQuery='SELECT BSureC_Country__c,BSureC_Sub_Zone__c,BSureC_Zone__c,Country_Name__c,Sub_Zone_Name__c,'+
                          'User__c,User__r.FirstName,User__r.LastName,User__r.Email,Zone_Name__c,id,Name,BSureC_Region__c,Full_Name__c FROM BSureC_AdditionalAttributes__c where User__r.IsActive = true AND User__r.Profile.Name Like : strProfilesIncluded';
          
        if(strZoneId!=null && strZoneId!='ALL'){ 
            
                strMainQuery+= ' AND BSureC_Zone__c=:strZoneId';
        }
        
        if(strSubZoneId!=null && strSubZoneId!='ALL'){
                if(strZoneId!=null && strZoneId!='ALL'){
                    strMainQuery+= ' AND BSureC_Sub_Zone__c=:strSubZoneId';
                }else{
                    strMainQuery+=' AND BSureC_Sub_Zone__c =:strSubZoneId';
                }
                
        }
        
        if(strCountryId!=null){
                if((strZoneId!=null && strZoneId!='ALL') || (strSubZoneId!=null && strSubZoneId!='ALL')){
                    strMainQuery+=' AND BSureC_Country__c=:strCountryId';
                }else{
                    strMainQuery+=' AND BSureC_Country__c=:strCountryId';
                }
            
        }
       strMainQuery+= ' ORDER BY  '+ sortExpression+' '+sortDirection ;
       listUserAttributes=Database.query(strMainQuery);
       total_size = listUserAttributes.size();
       
       if(listUserAttributes!=null && listUserAttributes.size()>0){
         rensearch=false;
          pagenation();//method called for pagination
       }else{
        rensearch=true;
          ApexPages.Message myEMsg=new ApexPages.Message(ApexPages.severity.INFO,System.label.BSureC_No_Record_Found);
          ApexPages.addMessage(myEMsg);
         
       }
       return null;
   }
   
    //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    public pagereference pagenation( ){
        displayPanel=true;
        objUserList = new list<CommonUserClass>();
        map<id,CommonUserClass> objMapUserDetails = new map<id, CommonUserClass>(); 
        map<id,CommonUserClass> objMapUserDetails1=new  map<id, CommonUserClass>(); 
        string strQuery;
        if(isexport==true){
            strQuery=strMainQuery;
        }else{
        strQuery=strMainQuery+'   limit '+ list_size + ' offset '+counter;
        }
        listUserAttributes=Database.query(strQuery);
        CommonUserClass objUserClass;
        set<Id> usid=new set<Id>();  
        try{  
         for(BSureC_AdditionalAttributes__c addObj:listUserAttributes)
            {    
                usid.add(addObj.User__c);
                //system.debug('usidusid'+usid.size());
            }                     
          
         listUsers=[Select Id, CategoryID__c,Name, User__c, CategoryName__c From BSureC_UserCategoryMapping__c where Id!=null];
         map<id,string> userids=new map<id,string> ();
       
         for(BSureC_UserCategoryMapping__c u:listUsers)
         {
            if(userids.size()==0||userids.get(u.User__c)==null){
               userids.put(u.User__c,u.CategoryName__c);
            
            }else{
                string addcate=userids.get( u.User__c);
                addcate+='; '+u.CategoryName__c;
                userids.put( u.User__c,addcate);
            }
         }
           
         for(BSureC_AdditionalAttributes__c addObj:listUserAttributes)
            {
                
                objUserClass = new CommonUserClass();
                
                objUserClass.strcountry=addObj.Country_Name__c;
                objUserClass.strSubZone=addObj.Sub_Zone_Name__c;
                objUserClass.strZone=addObj.Zone_Name__c;
                objUserClass.strFullName=addObj.Full_Name__c;
                objUserClass.strFirstName = addObj.User__r.FirstName;
                objUserClass.strLastName = addObj.User__r.LastName;
                objUserClass.strUserId = addObj.User__c;
                objUserClass.strBusiness=userids.get(addObj.User__c); 
                objUserClass.strEmail=addObj.User__r.Email;
                objUserList.add(objUserClass); 
            }
           
                //system.debug('-----objUserList---'+objUserList);
                //system.debug('-----listUserAttributes.size()----'+listUserAttributes.size());
                
            if(objUserList != null && objUserList.size()>0)
            {
                totallistsize=objUserList.size();
                //total_size = objUserList.size();
                PrevPageNumber=counter+1;
                //system.debug('-----counter---'+counter);
                //system.debug('-----total_size ---total_size' +total_size );
                //system.debug('-----listUserAttributes.size()---'+listUserAttributes.size());
                NxtPageNumber=counter+objUserList.size();
            }
          }
         catch(Exception ex){
       }
       return null;
    }
    
    /// <summary>
    /// userViewPage():  navigate the user to its detail
   /// </summary>
   /// <returns>navigate the user t its detail</returns>
    public pageReference userViewPage()
       {
        pagereference pageView = new pagereference('/apex/BSureC_AdditionalAttribute?Id='+userView); 
        return pageView; 
       }
   
    public pageReference ExportCSV(){
        isexport=true;
        pagenation();
        pageReference pRef = new pageReference('/apex/BSureC_CustomUserReportExport');
        return pRef;
     }
  
   /// <summary>
   /// This method is for pagination, if atleast one record found then passing 1 to the pagination method
   /// <param name="PageSize"></param>
   /// <returns>Integer</returns>  
   /// </summary>   
    public PageReference FirstbtnClick() { //user clicked beginning
      counter = 0;
      pagenation();
      return null;
           }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() { //user clicked previous button
      counter -= list_size;
      pagenation();
      return null;
      }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() { //user clicked next button
      counter += list_size;
      //system.debug('******total_size**'+total_size);
      //system.debug('******list_size**'+list_size);
      //system.debug('******counter**'+counter);
      pagenation();
      return null;
       }
       
      
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference LastbtnClick() 
    {   //user clicked end
        if(math.mod(total_size, list_size) == 0)
        {
            counter = total_size - list_size;
        }
        else
        {
            counter = total_size - math.mod(total_size, list_size);
        }
        pagenation();
        return null;
    }
       
       
   /// <summary>  
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getPreviousButtonEnabled() {  
          //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
       }
       
   public pageReference cancel()
   {
    pagereference pageView = new pagereference('/apex/BSureC_Reports'); 
    return pageView;
   }   
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
      }
 

 
   public Integer getPageSize() {
      return total_size;
      }
 
 
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
      }
       
       
  /// <summary>
  /// Below method gets the total no.of pages.
  /// <param name="TotalPageNumber"></param>
  /// <returns>Integer</returns>    
  /// </summary>
   public Integer getTotalPageNumber() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;  
      } else {
         return (total_size/list_size);
      }
   } 
  }