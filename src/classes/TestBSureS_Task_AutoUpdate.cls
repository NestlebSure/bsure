/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 //kishore
@isTest(SeeAllData = true)
private class TestBSureS_Task_AutoUpdate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        insert supplierObj;
        
        BSureS_Credit_Analysis__c cObj = new BSureS_Credit_Analysis__c();
        cObj.Review_Name__c = 'test Review';
        cObj.Review_Status__c = 'Completed';
        cObj.Supplier_ID__c = supplierObj.Id;
        insert cObj;
        
        system.Assertequals('test Review',cObj.Review_Name__c); 
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.WhatId = cObj.Id;
        insert t;
        
        System.test.starttest();
        BSureS_Task_AutoUpdate BOS = new BSureS_Task_AutoUpdate();
        system.schedule('ScheduleTest','0 0 0 * * ?',BOS);
        System.test.stoptest();
    }
}