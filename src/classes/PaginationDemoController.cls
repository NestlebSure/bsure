/**This is a sample controller with sample data of how to use the
    Paginate class*/
public with sharing class PaginationDemoController {
    public PaginateController paginater {get;set;}
    public List<CustomClass> genericList{get;set;}
    public List<List<CustomClass>> fullGenericList{get;set;}

    public PaginationDemoController(){
        //Get the data we need to paginate
        List<CustomClass> resultsList = populateData();

        //Set the page size
        Integer pageSize = 10;

        //Create a new instance of Paginate passing in the overall size of
        //the list of data and the page size you want
        this.paginater = new PaginateController(resultsList.size(), pageSize);

        //These lists hold the data
        this.fullGenericList = new List<List<CustomClass>>();
        this.genericList = new List<CustomClass>();

        //Break out the full list into a list of lists
        if(resultsList.size() > 0){
            List<CustomClass> tempCC = new List<CustomClass>();        
            Integer i = 0;
            for(CustomClass cc : resultsList){
                tempCC.add(cc);
                i++;
                if(i == pageSize){
                    this.fullGenericList.add(tempCC);
                    tempCC = new List<CustomClass>();
                    i = 0;
                }
            }
            if(!tempCC.isEmpty()){
                //this.fullGenericList.add(tempCC);
            }
			
			system.debug('fullGenericList===='+fullGenericList.size());
            //Gets the correct list of data to show on the page
            this.genericList = this.fullGenericList.get(this.paginater.index);
            system.debug('==genericList==='+genericList.size());
        }
    }

    public PageReference previousPage(){
        this.paginater.decrement();
        return changeData();
    }

    public PageReference nextPage(){
        this.paginater.increment();
        return changeData();
    }

    public PageReference updatePage(){
        this.paginater.updateNumbers();
        return changeData();
    }

    public PageReference changeData(){
        this.genericList = this.fullGenericList.get(this.paginater.index);
        return null;
    }

    public List<CustomClass> populateData(){
        List<CustomClass> customClassList = new List<CustomClass>();
        /*
        for(Integer i = 1; i < 50; i++){
            customClassList.add(new CustomClass(i, 'Name:  ' + String.valueOf(i)));
        }
        */
        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
        string strQuery1 = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c, '+ 
                        'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
                        'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
                        ', Owner_Ship__c ,Contact_Name__c '+
                        ' from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
		
		string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id != null LIMIT 20000 ';
                        
		AllContacts = Database.query(strQuery);
		
		/*
		for(Integer i = 1; i < AllContacts.size(); i++)
		{
			customClassList.add(new CustomClass(i, AllContacts[i].Supplier_Name__c));
			
		}
		*/
		Integer i = 1;
		for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
		{
			customClassList.add(new CustomClass(i, sObj.Supplier_Name__c));
			i++;
		}
        return customClassList;
    }

    public class CustomClass{
        public Integer num{get;set;}
        public String name{get;set;}

        public CustomClass(Integer num, String name){
            this.num = num;
            this.name = name;
        }
    }
}