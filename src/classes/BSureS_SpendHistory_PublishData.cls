/************************************************************************************************       
Controller Name 		: BSureS_SpendHistory_PublishData       
Date                    : 11/12/2012        
Author                  : Praveen Sappati       
Purpose         		: To Inser the Spend history File into a Custom Object       
Change History 			: Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
						  11/08/2012      Praveen Sappati            Initial Version
**************************************************************************************************/
global with sharing class BSureS_SpendHistory_PublishData 
{
	/// <summary>
	/// loadbutton method fires when user clicks on publish button 
	/// in spend history stage list view to execute records and insert as batch 
	/// </summary>
	webService static void loadbutton()
	{
		 BSureS_SpendHistoryDataPublish_Scheduler b = new BSureS_SpendHistoryDataPublish_Scheduler (); 
      	 database.executebatch(b,200);//executing batch class
	}
}