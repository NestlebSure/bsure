global with sharing class BSureS_EmailManagement implements Database.Batchable<sObject>,
                                                            Database.stateful, schedulable{
    string strEmailQueueQuery;
    string strNSubject = '';
    string strNBody = '';
    string strDDSubject = '';
    string strDDBody = '';
    string strForumName = '';
    map<string, list<BSureS_Email_Queue__c>> mapEmailQueue = new map<string, list<BSureS_Email_Queue__c>>();
    list<EmailTemplate> lstEmailTemplates;
    set<string> setTemplateNames = new set<string>{'BSureS_Notifications', 'BSureS_DailyDigest'};
    map<String,Id> mapuserobj = new map<String,Id>();
    map<String,Id> mapcontactobj = new map<String,Id>();
    
    global BSureS_EmailManagement()
    {  
        //system.debug('Current Time : ' + System.now().time());
        strEmailQueueQuery  = 'SELECT Id, Recipient_Address__c, Is_Daily_Digest__c, Email_Subject__c, ';
        strEmailQueueQuery += 'Email_Status__c, Email_Priority__c, Email_Body__c, Recipient_Name__c, Send_Immediate__c ';
        strEmailQueueQuery += 'FROM BSureS_Email_Queue__c ';
        strEmailQueueQuery += 'WHERE Email_Status__c = \'NEW\' ';
        strEmailQueueQuery += 'AND Send_On_Date__c <= TODAY ';
        strEmailQueueQuery += 'AND ((Is_Daily_Digest__c = \'NO\' ';
        strEmailQueueQuery += 'AND Send_Immediate__c = false) ';
        strEmailQueueQuery += 'OR (Is_Daily_Digest__c = \'YES\' ';
        strEmailQueueQuery += 'AND Send_Immediate__c = true) ';
        strEmailQueueQuery += 'OR (Is_Daily_Digest__c = \'NO\' ';
        strEmailQueueQuery += 'AND Send_Immediate__c = true)) ';
        strEmailQueueQuery += 'ORDER BY CreatedDate limit 1000';
        system.debug('strEmailQueueQuery+++' + strEmailQueueQuery);
        for(User obj:[Select Id,Name,Email from User where isActive = true and
                         Receive_Daily_Digests__c=true limit 5000]){//need to verify the Field before moving the package :Dheeraj
            mapuserobj.put(obj.Email,obj.Id);
        }
        
         for(Contact obj:[Select Id,Name,Email,isActive__c from Contact where isActive__c=true limit 5000]){
            mapcontactobj.put(obj.Email,obj.Id);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext Bc)
    {
        return Database.getQueryLocator(strEmailQueueQuery);
    }
    
    global void Execute(Database.BatchableContext Bc, list<BSureS_Email_Queue__c> lstEmailMsgs)    
    {  
        string strUniqueKey = '';
        strForumName = BSureS_CommonUtil.getConfigurationValues('BSureS_ForumName').get(0);
        list<string> lstIndEmailAdd = new list<string>();
        list<Messaging.SingleEmailMessage> objMailsList = new list<Messaging.SingleEmailMessage>(); 
        
        GetEmailTemplates();
        if(lstEmailMsgs != null && lstEmailMsgs.size() > 0)
        {
            for(BSureS_Email_Queue__c objEachMail : lstEmailMsgs)
            {
                if(objEachMail.Recipient_Address__c != null)
                {
                    strUniqueKey = objEachMail.Recipient_Address__c.trim() + '~' +
                         (objEachMail.Is_Daily_Digest__c != null && objEachMail.Is_Daily_Digest__c != '' 
                            ? objEachMail.Is_Daily_Digest__c.trim() + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase() 
                            : 'YES' + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase());
                }   
                if(mapEmailQueue.containsKey(strUniqueKey))
                {
                    list<BSureS_Email_Queue__c> objListEQ = mapEmailQueue.get(strUniqueKey);
                    objListEQ.add(objEachMail);
                    mapEmailQueue.put(strUniqueKey, objListEQ);
                }
                else
                {
                    list<BSureS_Email_Queue__c> objListEQ = new list<BSureS_Email_Queue__c>();
                    objListEQ.add(objEachMail);
                    mapEmailQueue.put(strUniqueKey, objListEQ);
                }
            }
            //system.debug('mapEmailQueue+++++++++' + mapEmailQueue.size());
            if(mapEmailQueue != null && mapEmailQueue.size() > 0)
            {
                for(string toAddressKey : mapEmailQueue.keySet())
                {
                    if(toAddressKey.contains('YESFALSE'))
                    {
                        objMailsList.add(GenerateConsolidatedMail(mapEmailQueue.get(toAddressKey)));
                    }
                    else
                    {
                        for(BSureS_Email_Queue__c objEachMsg : mapEmailQueue.get(toAddressKey))
                        {
                            if(mapuserobj.containskey(objEachMsg.Recipient_Address__c) && mapuserobj.get(objEachMsg.Recipient_Address__c) != null )
                            {
                                string strSubject = strNSubject.replace('%%SUBJECT%%', objEachMsg.Email_Subject__c);
                                string strEmailBody = strNBody.replace('%%USER%%', objEachMsg.Recipient_Name__c);
                                strEmailBody = strEmailBody.replace('%%EMAILBODY%%', objEachMsg.Email_Body__c);
                                strEmailBody = strEmailBody.replace('\r\n', '</br>');
                                Messaging.SingleEmailMessage objIndividualMail = new Messaging.SingleEmailMessage();
                                
                                lstIndEmailAdd.clear();
                                lstIndEmailAdd.add(objEachMsg.Recipient_Address__c);
                                
                                //objIndividualMail.setToAddresses(lstIndEmailAdd);
                                objIndividualMail.settargetObjectId(mapuserobj.get(objEachMsg.Recipient_Address__c));
                                objIndividualMail.setUseSignature(false);
                                objIndividualMail.setSaveAsActivity(false);
                                objIndividualMail.setSubject(strSubject); 
                                objIndividualMail.setSenderDisplayName(strForumName);
                                objIndividualMail.setHtmlBody(strEmailBody);
                                
                                objMailsList.add(objIndividualMail);
                            } 
                            
                            else if(mapcontactobj.containskey(objEachMsg.Recipient_Address__c) && mapcontactobj.get(objEachMsg.Recipient_Address__c) != null )
                            {  //added on 31/05/2019
                                string strSubject = strNSubject.replace('%%SUBJECT%%', objEachMsg.Email_Subject__c);
                                string strEmailBody = strNBody.replace('%%USER%%', objEachMsg.Recipient_Name__c);
                                strEmailBody = strEmailBody.replace('%%EMAILBODY%%', objEachMsg.Email_Body__c);
                                strEmailBody = strEmailBody.replace('\r\n', '</br>');
                                Messaging.SingleEmailMessage objIndividualMail = new Messaging.SingleEmailMessage();
                                
                                lstIndEmailAdd.clear();
                                lstIndEmailAdd.add(objEachMsg.Recipient_Address__c);
                                
                                //objIndividualMail.setToAddresses(lstIndEmailAdd);
                                objIndividualMail.settargetObjectId(mapcontactobj.get(objEachMsg.Recipient_Address__c));
                                objIndividualMail.setUseSignature(false);
                                objIndividualMail.setSaveAsActivity(false);
                                objIndividualMail.setSubject(strSubject); 
                                objIndividualMail.setSenderDisplayName(strForumName);
                                objIndividualMail.setHtmlBody(strEmailBody);
                                
                                objMailsList.add(objIndividualMail);
                            }      
                        }
                    }
                }
            }
            if(objMailsList != null && objMailsList.size() > 0){
                Messaging.sendEmail(objMailsList);
            }   
            if(lstEmailMsgs != null && lstEmailMsgs.size() > 0){
                UpdateEmailQueue(lstEmailMsgs);
            }   
        }
    }

    public Messaging.SingleEmailMessage GenerateConsolidatedMail(list<BSureS_Email_Queue__c> objListEQ)
    {
        string strEmailAdds = '';
        string strRecipientName = '';
        list<string> lstEmailAdd = new list<string>();
        string strSubject = strDDSubject.replace('%%DATE%%', System.today().format()); 
        string strEmailBody = '<table width="100%" border="0" cellpadding="10" cellspacing="10">';
        
        Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
        
        for(BSureS_Email_Queue__c objEachMsg : objListEQ)
        {
            strEmailAdds = objEachMsg.Recipient_Address__c;
            strRecipientName = objEachMsg.Recipient_Name__c;
            strEmailBody += '<tr><td width="100%" style="border-bottom: 1px solid;">';
            strEmailBody += '<b>' + objEachMsg.Email_Subject__c + '</b></br>';
            strEmailBody += '______________________________________________________</br></br>';
            strEmailBody += objEachMsg.Email_Body__c;
            strEmailBody += '</br></td></tr>';
        }
        strEmailBody += '</table>';
        string strFinalBody = strDDBody.replace('%%USER%%', strRecipientName != null && strRecipientName != '' ? strRecipientName : '');
        strFinalBody = strFinalBody.replace('%%EMAILBODY%%', strEmailBody);
        strFinalBody = strFinalBody.replace('\r\n', '</br></br>');
        lstEmailAdd.add(strEmailAdds);
        
        objMail.saveAsActivity = false;
        objMail.setToAddresses(lstEmailAdd);
        objMail.setUseSignature(false);
        objMail.setSaveAsActivity(false);
        objMail.setSubject(strSubject); 
        objMail.setSenderDisplayName(strForumName);
        objMail.setHtmlBody(strFinalBody);
        return objMail;
    }
    
    public void UpdateEmailQueue(list<BSureS_Email_Queue__c> lstEmailMsgs)
    {
        try
        {
            for(BSureS_Email_Queue__c objEachMail : lstEmailMsgs)
            {
                objEachMail.Email_Status__c = 'SENT';
                objEachMail.Delivered_Date__c = Datetime.now();
            }
            Database.update(lstEmailMsgs);
        }
        catch(Exception ex)
        {
            //system.debug('Exception in UpdateEmailQueue ' + ex);
        }
    }
    
    public void GetEmailTemplates()
    {
        lstEmailTemplates = new list<EmailTemplate>();
        lstEmailTemplates = [SELECT Subject, Name, IsActive, Body
                             FROM EmailTemplate
                             WHERE Name IN: setTemplateNames
                             AND IsActive =: true];
        
        if(lstEmailTemplates != null && lstEmailTemplates.size() > 0)
        {
            for(EmailTemplate objEach : lstEmailTemplates)
            {
                if(objEach.Name.contains('DailyDigest'))
                {
                    strDDBody = objEach.Body;
                    strDDSubject = objEach.Subject;
                }
                else if(objEach.Name.contains('Notifications'))
                {
                    strNBody = objEach.Body;
                    strNSubject = objEach.Subject;
                }
            }
            //system.debug('strDDBody++++++++ ' + strDDBody);
            //system.debug('strNBody++++++++ ' + strNBody);
        }
    }
    
    global void execute(SchedulableContext SC)
    {
        BSureS_EmailManagement  objEmailMngmt =  new BSureS_EmailManagement();
        ID batchprocessid = Database.executeBatch(objEmailMngmt);  
    }
    
    global void finish(Database.BatchableContext Bc){       
                 
    }

}