/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureC_CustomerinfoIntermediate {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
     	BSureC_Category__c Category1 = new BSureC_Category__c(Name = 'Category1');
        BSureC_Category__c Category2 = new BSureC_Category__c(Name = 'Category2');
        list<BSureC_Category__c> lstCategores = new  list<BSureC_Category__c>{Category1,Category2};
        insert lstCategores; 
        
        
        BSureC_AdditionalAttributes__c BUser = new BSureC_AdditionalAttributes__c(); 
        BUser.User__c = userInfo.getUserId();
        insert BUser;
        
        BSureC_UserCategoryMapping__c UserCategory1 = new BSureC_UserCategoryMapping__c (
            User__c =UserInfo.getUserId(),
            CategoryId__c = Category1.Id
            );
         BSureC_UserCategoryMapping__c UserCategory2 = new BSureC_UserCategoryMapping__c (
            User__c =UserInfo.getUserId(),
            CategoryId__c = Category2.Id
            );
        list<BSureC_UserCategoryMapping__c> lstUserC = new list<BSureC_UserCategoryMapping__c>{UserCategory1,UserCategory2};
        insert lstUserC;
        
        system.assertEquals(2,lstUserC.size());
      
    	
        BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        BSzone= [select id,Name from BSureC_Sub_Zone__c where id=:BSzone.Id];
        
        BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
        
        system.assertEquals('Test Supplier Country',BsCountry.Name);
        
        BSureC_State__c BsState = new BSureC_State__c(Name='Cincinnati', Customer_Country__c = BsCountry.Id);
        insert BsState;
        
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
        
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Analyst'];
        
        User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
       
       List<BSureC_AdditionalAttributes__c> AAttributes=new List<BSureC_AdditionalAttributes__c>();
       BSureC_AdditionalAttributes__c objAdditionalAttributes1=new BSureC_AdditionalAttributes__c();
       objAdditionalAttributes1.BSureC_Zone__c=Bzone.id;
       objAdditionalAttributes1.BSureC_Sub_Zone__c=BSzone.id;
       //objAdditionalAttributes1.BSureC_Region__c=RegRef.id;
       objAdditionalAttributes1.BSureC_Country__c=BsCountry.id;
       objAdditionalAttributes1.User__c=objUser.Id;
       insert objAdditionalAttributes1;    
       AAttributes.add(objAdditionalAttributes1);        
                    
        list<BSureC_Customer_Basic_Info_Stage__c> listStage= new list<BSureC_Customer_Basic_Info_Stage__c>();
        BSureC_Customer_Basic_Info_Stage__c lstStage= new BSureC_Customer_Basic_Info_Stage__c();
        lstStage.State__c = BsState.Id;
        lstStage.Country__c =BsCountry.Id;
        lstStage.Customer_Category__c=Category1.Id;
        lstStage.Zone__c=Bzone.Id;
        lstStage.Sub_Zone__c=BSzone.Id;
        lstStage.Customer_Name__c='dummy';
        lstStage.Planned_Review_Date__c = system.today();
        lstStage.Analyst__c = objUser.Id;
        lstStage.Collectors__c = objUser.Id; 
        insert lstStage;
        system.assertEquals('dummy',lstStage.Customer_Name__c);
        listStage.add(lstStage);
        BSureC_CustomerinfoIntermediate objInter = new BSureC_CustomerinfoIntermediate();
        //objInter.AAttributes = lstAditionalAttrib1;
       	objInter.lstStage=listStage;
        objInter.getSupPendingRecords();
        objInter.getSStateList();
        objInter.getSAnalystList();
        objInter.UpdateSupStage();
        objInter.getCollectorsList();
    }
}