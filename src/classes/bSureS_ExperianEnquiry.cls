public class bSureS_ExperianEnquiry {
    public string expResString{get;set;}
    public string rptName{get;set;}
    public bSureS_ExperianEnquiry(){
        string strId = '';
        string recId = '';
        string fName = '';
        if(ApexPages.currentPage().getParameters().get('parentId')!=null && ApexPages.currentPage().getParameters().get('parentId')!=''){
        	strId = ApexPages.currentPage().getParameters().get('parentId');
        }
        
        if(ApexPages.currentPage().getParameters().get('recId')!=null && ApexPages.currentPage().getParameters().get('recId')!=''){
        	recId = ApexPages.currentPage().getParameters().get('recId');
        }
        
        if(ApexPages.currentPage().getParameters().get('attachName')!=null && ApexPages.currentPage().getParameters().get('attachName')!=''){
        	fName = ApexPages.currentPage().getParameters().get('attachName');
        }
        
        system.debug(fName+'====strId==='+strId);
        Attachment objAttach = new Attachment();
        if(strId!=null && strId!='' && fName!=null && fName!=''){
            objAttach = [Select id,Name,parentId,Body From Attachment Where parentId=:strId and Name=:fName];
        }else if(recId!=null && recId!='' && fName!=null && fName!=''){
        	objAttach = [Select id,Name,parentId,Body From Attachment Where Id=:recId and Name=:fName];
        }
        if(objAttach!=null){
        	rptName = objAttach.Name;
            Blob fileBody = objAttach.body;
            expResString = fileBody.toString() ;
            system.debug('****:::'+expResString);
        }
    }
}