/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_UserActivityReport {

    static testMethod void myUnitTest() {
        
        BSure_Configuration_Settings__c objCSettingsUA = new BSure_Configuration_Settings__c();
        objCSettingsUA.Name = 'BSureS_Report_Page_Size7';
        objCSettingsUA.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsUA.Parameter_Value__c = '10';
        insert objCSettingsUA;
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        //system.assertEquals('TESTAOA', szone.Name);
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true;
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        
        
        BSureS_Country__c sCountry = new BSureS_Country__c();
        sCountry.IsActive__c = true;
        sCountry.Name = 'test united States';
        sCountry.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountry; 
        
        BSureS_User_Additional_Attributes__c uaddObj = new BSureS_User_Additional_Attributes__c();
        uaddobj.User__c = standard.Id;
        uaddobj.BSureS_Zone__c = szone.Id;
        uaddobj.BSureS_Sub_Zone__c = sSubZone.Id;
        uaddobj.BSureS_Country__c = sCountry.Id;
        insert uaddobj;
        
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
        insert objSupplier;
        BSureS_Credit_Status_Section__c Cs = new BSureS_Credit_Status_Section__c();
        Cs.DiscussionType__c = 'Topic';
        Cs.Supplier_ID__c = objSupplier.Id;
        insert cs;
        system.assertEquals('Topic',Cs.DiscussionType__c );
        
        system.Test.startTest();
        BSureS_UserActivityReport BUAR = new BSureS_UserActivityReport();
        //BUAR.dtFromDate = system.today().addDays(-30);
        //BUAR.dtFromDate = system.today();
        //BUAR.strFromDate = string.valueof(Date.Today().addDays(-50));
        //BUAR.strToDate     = string.valueof(Date.Today());
        BUAR.Search();
        BUAR.pageSize=10;
        BUAR.sortExpression ='ASC';
        BUAR.strZoneId = szone.Id;
        BUAR.strSubZoneId = sSubZone.Id;
        BUAR.strCountryId  = sCountry.Id;
        BUAR.strSupplierId = objSupplier.Id;
        pagereference pgrefnxtbtn = BUAR.nextBtnClick();
        pagereference pgrefprvbtn = BUAR.previousBtnClick();
        integer i=BUAR.getTotalPageNumber();
        integer j=1;
        BUAR.BindData(j);
        BUAR.pageData(j);
        BUAR.LastpageData(j);
        BUAR.getSortDirection();
        BUAR.Sort();
        BUAR.Cancel();
        BUAR.showBuyerDetails();
        integer k=BUAR.getPageNumber();
        integer l=BUAR.getPageSize();
        boolean a=BUAR.getPreviousButtonEnabled();
        boolean b=BUAR.getNextButtonDisabled();
        pagereference pgreflastbtn =BUAR.LastbtnClick();
        pagereference pgreffrstbtn =BUAR.FirstbtnClick();
        BUAR.searchBtn();
        BUAR.ExportToExcel();
        BUAR.closepopup();
        BUAR.closepopupBtn();
        system.Test.stopTest();
               
        // TO DO: implement unit test
    }
}