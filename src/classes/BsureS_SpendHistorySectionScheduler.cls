/************************************************************************************************       
Controller Name         : BsureS_SpendHistorySectionScheduler       
Date                    : 11/12/2012        
Author                  : Praveen Sappati       
Purpose                 : To validate and insert the records in the spend history stage object.       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/12/2012      Praveen Sappati            Initial Version
                          11/21/2012      Praveen Sappati            added trim method to date fields
                          11/29/2012      Praveen Sappati            Changes done on the feedback of demo
                          12/10/2012      Praveen Sappati            Changes done on the feedback of demo
                          12/08/2013	  Veereandranath J			Code Review Changes		
**************************************************************************************************/
global with sharing class BsureS_SpendHistorySectionScheduler implements Database.Stateful,Schedulable 
{
    
    global static String strfiledata{get;set;}// Holds the CSV file Data
    global static String[] filelines = new String[]{};// Holds the each row of the CSV file
    global static List<BSureS_Spend_History_Stage__c> lst_spend_history_stage = new List<BSureS_Spend_History_Stage__c>();
    
    
    global void execute(SchedulableContext sc) {}
    
    /// <summary>
    /// createBatchesFromCSVFile method fires when user uploads a csv file
    /// </summary>
    /// <param name="bflcontent"></param>
    /// <param name="strObjectName"></param>
    global static void createBatchesFromCSVFile(blob bflcontent,String strObjectName)
    {
        strfiledata=null;
        filelines=null;
        strfiledata = bflcontent.toString();//converting file blob to string.
        filelines = strfiledata.split('\n');//splitting the rows.
        
        Map<String,Id> map_update_supplierid = new Map<String,Id>();
        Map<Date,String> map_val_spendata_publish = new Map<Date,String>();
        // Added for Security Review
        if(Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable()){
	        for(BSureS_Basic_Info__c reccustinfo : [SELECT Id,Globe_ID__c FROM BSureS_Basic_Info__c WHERE Globe_ID__c!=NULL ])
	        {
	            map_update_supplierid.put(reccustinfo.Globe_ID__c,reccustinfo.Id);//adding the globe id and supplier basic info id to map
	        }
        }
        if(Schema.Sobjecttype.BSureS_Spend_History_Publish__c.isQueryable()){
	        for(BSureS_Spend_History_Publish__c spendhistory_data_publish : [SELECT Globe_ID__c,date__c FROM BSureS_Spend_History_Publish__c WHERE date__c!=NULL])
	        {
	            map_val_spendata_publish.put(spendhistory_data_publish.date__c,spendhistory_data_publish.Globe_ID__c);//adding the date  and globe id to map
	        }
        }
        

        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};//initializing the string array
            inputvalues = filelines[i].split(',');//split values with ',' .
            
            BSureS_Spend_History_Stage__c rec_spend_history_stage = new BSureS_Spend_History_Stage__c();
            if (Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()){
                rec_spend_history_stage.Status__c='Initial upload';
                }
            if (Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable() || Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isUpdateable()){
                rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Yellow';
               } 
            //below validating the date field
            if(inputvalues[0]!=NULL && inputvalues[0]!=' ' && inputvalues[0]!='')
            {
                try
                {
                    if (Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Date__c.isCreateable()){
                        rec_spend_history_stage.Date__c= Date.parse(inputvalues[0].trim()); //assigning values
                    }
                }
                catch(Exception e)
                {
                    if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                       && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                       && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
	                     //if the date is not in format then adding exception value to the exception field
		                    if(rec_spend_history_stage.Exception__c==NULL)
		                    {
		                        rec_spend_history_stage.Exception__c=String.valueof(e)+''+Label.BSureS_spendhisindate;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                        
		                    }
		                    else
		                    {
		                        rec_spend_history_stage.Exception__c+=','+String.valueof(e)+''+Label.BSureS_spendhisindate;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    } 
	                	}  
                }
            }
            else
            {
            	    if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                       && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                       && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
		                //if date is null in csv file the assigning the exception to the field
			                 if(rec_spend_history_stage.Exception__c==NULL)
				             {
				                  rec_spend_history_stage.Exception__c=Label.BSureS_spendhisdatemand;
				                  rec_spend_history_stage.Status__c='Exception';
				                  rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
				             }
			                 else
				             {
				                  rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhisdatemand;
				                  rec_spend_history_stage.Status__c='Exception';
				                  rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
				             }   
		                }
            }
            
            
            //validating the spend amount
            if(inputvalues[1]!=NULL && inputvalues[1]!=' ' && inputvalues[1]!='')   
            {
                try
                 {
	                 if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Spend_Amount__c.isCreateable()){
	                    rec_spend_history_stage.Spend_Amount__c = Decimal.valueof(inputvalues[1]); //assigning values
	                    }
                 }
                catch(Exception e)
                {
                     if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
	                    //if the spen amount is not in format then adding exception value to the exception field
		                    if(rec_spend_history_stage.Exception__c==NULL)
		                    {
		                        rec_spend_history_stage.Exception__c=String.valueof(e)+''+Label.BSureS_spendhisinspendamt;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }
		                    else
		                    {
		                        rec_spend_history_stage.Exception__c+=','+String.valueof(e)+''+Label.BSureS_spendhisinspendamt;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }   
	                       }
                }
            }
            else
            {
                     if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
		                //if spend amount is null in csv file the assigning the exception to the field
			                if(rec_spend_history_stage.Exception__c==NULL)
			                {
			                    rec_spend_history_stage.Exception__c=Label.BSureS_spendhisspendamtmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                }
			                else
			                {
			                    rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhisspendamtmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                }
	                     }   
            }
            
             //validating the spend period
            if(inputvalues[2]!=NULL && inputvalues[2]!=' ' && inputvalues[2]!='')
            {
                try
                {
                 	 if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Spend_Period__c.isCreateable()){
                        rec_spend_history_stage.Spend_Period__c = Date.parse(inputvalues[2].trim()); //assigning values
                        }
                }
                catch(Exception e)
                {
                	 if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                        && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
	                    //if the spen period is not in format then adding exception value to the exception field    
		                    if(rec_spend_history_stage.Exception__c==NULL)
		                    {
		                        rec_spend_history_stage.Exception__c=String.valueof(e)+''+Label.BSureS_spendhisinspendperiod;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }
		                    else
		                    {
		                        rec_spend_history_stage.Exception__c+=','+String.valueof(e)+''+Label.BSureS_spendhisinspendperiod;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }   
		                 }
                }
            }
            else
            { 
            	    if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                      && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                      && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
		                //if spend period is null in csv file the assigning the exception to the field
			                if(rec_spend_history_stage.Exception__c==NULL)
			                {
			                    rec_spend_history_stage.Exception__c=Label.BSureS_spendhisspendperiodmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                }
			                else
			                {
			                    rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhisspendperiodmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                } 
			              }  
            }
            
            //validating the globe id
            if(inputvalues[3]!=NULL && inputvalues[3]!=' ' && inputvalues[3]!='')
            {
                boolean val_groupid=false;
                try
                {
                 Double groupid=null;
                 groupid=Double.valueof(inputvalues[3]);
                
                }
                catch(Exception e)
                {
                    //if the GroupID is not in format then adding exception value to the exception field
                      val_groupid=true;
                      if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                         && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                         && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
			                   if(rec_spend_history_stage.Exception__c==NULL)
			                   {
			                       rec_spend_history_stage.Exception__c=String.valueof(e)+''+Label.BSureS_spendhisinglobeid;
			                       rec_spend_history_stage.Status__c='Exception';
			                       rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                   }
			                   else
			                   {
			                       rec_spend_history_stage.Exception__c+=','+String.valueof(e)+''+Label.BSureS_spendhisinglobeid;
			                       rec_spend_history_stage.Status__c='Exception';
			                       rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                   } 
	                   }  
                }
                
                   if(val_groupid==false)
                    {
                      if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Globe_ID__c.isCreateable()){
                         rec_spend_history_stage.Globe_ID__c = inputvalues[3];
                         }
                    }
               //assigning values
            }
            else
            {
                  if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                     && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                     && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
			                //if globe id is null in csv file the assigning the exception to the field
			                if(rec_spend_history_stage.Exception__c==NULL)
			                {
			                    rec_spend_history_stage.Exception__c=Label.BSureS_spendhisglobeidmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                }
			                else
			                {
			                    rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhisglobeidmand;
			                    rec_spend_history_stage.Status__c='Exception';
			                    rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
			                }  
			             } 
            }
            
            
           if(map_update_supplierid != null && map_update_supplierid.get(rec_spend_history_stage.Globe_ID__c)!=NULL && rec_spend_history_stage.Globe_ID__c!=NULL)
           {
         	     if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Supplier_ID__c.isCreateable()){
                    rec_spend_history_stage.Supplier_ID__c = String.valueOf(map_update_supplierid.get(rec_spend_history_stage.Globe_ID__c));//getting the supplier id from map
                    }
           }
         
         //below validating if the globe id doesnt exists in the supplier basic info
          if(map_update_supplierid.get(rec_spend_history_stage.Globe_ID__c)==NULL)
          {
                 if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                    && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                    && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
				            if(rec_spend_history_stage.Exception__c==NULL)
				            {
				                rec_spend_history_stage.Exception__c=Label.BSureS_spendhisnosup;
				                rec_spend_history_stage.Status__c='Exception';
				                rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
				            }
				            else
				            {
				                rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhisnosup;
				                rec_spend_history_stage.Status__c='Exception';
				                rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
				            } 
                         }               
         }  
            
        //below validating if the same date the publish record with same supplier exists
        if(map_val_spendata_publish != null && rec_spend_history_stage.Globe_ID__c!=NULL &&  rec_spend_history_stage.Date__c!=NULL && map_val_spendata_publish.get(rec_spend_history_stage.Date__c)!=NULL)
         {
                if(map_val_spendata_publish.get(rec_spend_history_stage.Date__c)==rec_spend_history_stage.Globe_ID__c)
                {
                  if(Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Exception__c.isCreateable() 
                     && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status__c.isCreateable()
                     && Schema.sObjectType.BSureS_Spend_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
		                    if(rec_spend_history_stage.Exception__c==NULL)
		                    {
		                        rec_spend_history_stage.Exception__c=Label.BSureS_spendhiserrormssgalert;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }
		                    else
		                    {
		                        rec_spend_history_stage.Exception__c+=','+Label.BSureS_spendhiserrormssgalert;
		                        rec_spend_history_stage.Status__c='Exception';
		                        rec_spend_history_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                    }   
		                  }
	                    }
         }  

            lst_spend_history_stage.add(rec_spend_history_stage);//adding to lists
         }
         if(Schema.Sobjecttype.BSureS_Spend_History_Stage__c.isCreateable())
         	Database.insert(lst_spend_history_stage);// inserting the list
    }
}