/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_SupplierInfoIntermediate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         BSureS_Category__c Category1 = new BSureS_Category__c(Name = 'Category1');
        BSureS_Category__c Category2 = new BSureS_Category__c(Name = 'Category2');
        list<BSureS_Category__c> lstCategores = new  list<BSureS_Category__c>{Category1,Category2};
        insert lstCategores; 
        
        BSure_Configuration_Settings__c objCSettings9B = new BSure_Configuration_Settings__c();
        objCSettings9B.Name = 'BSureS_BuyerRole9B';
        objCSettings9B.Parameter_Key__c = 'BSureS_BuyerRole';
        objCSettings9B.Parameter_Value__c = 'Buyer';
        insert objCSettings9B;
        
        BSureS_User_Additional_Attributes__c BUser = new BSureS_User_Additional_Attributes__c(); 
        BUser.User__c = userInfo.getUserId();
        insert BUser;
        
        BSureS_UserCategoryMapping__c UserCategory1 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category1.Id
            );
         BSureS_UserCategoryMapping__c UserCategory2 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category2.Id
            );
        list<BSureS_UserCategoryMapping__c> lstUserC = new list<BSureS_UserCategoryMapping__c>{UserCategory1,UserCategory2};
        insert lstUserC;
        
        system.assertEquals(2,lstUserC.size());
      
        
        BSureS_Zone__c Bzone = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureS_Zone__c where id=:Bzone.Id];
        BSureS_SubZone__c BSzone = new BSureS_SubZone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        BSzone= [select id,Name from BSureS_SubZone__c where id=:BSzone.Id];
        
        BSureS_Country__c BsCountry = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureS_Country__c  where id=:BsCountry.Id];
        
        system.assertEquals('Test Supplier Country',BsCountry.Name);
        
        BSureS_Basic_Info_Stage__c lstStage= new BSureS_Basic_Info_Stage__c();
        lstStage.BSureS_Country__c=BsCountry.Id;
        lstStage.Supplier_Category__c=Category1.Id;
        lstStage.Zone__c=Bzone.Id;
        lstStage.Sub_Zone__c=BSzone.Id;
        lstStage.SUPPLIER_NAME__C='dummy';
        insert lstStage;
        system.assertEquals('dummy',lstStage.SUPPLIER_NAME__C);
        
        system.Test.startTest();
        BSureS_SupplierInfoIntermediate objInter = new BSureS_SupplierInfoIntermediate();
       // BSureS_SupplierInfoIntermediate.lstStage=lstStage;
        objInter.getSupPendingRecords();
        objInter.getSStateList();
        objInter.getSAnalystList();
        objInter.UpdateSupStage();
        system.Test.stopTest();
    }
}