global with sharing class BSureC_Dashboard 
{
	public list<Dashboard> lstDashboard{get;set;}
	public list<Dashboard> lstDashboardAnalyst{get;set;}
    public boolean hideanalystlink{get;set;}
    public boolean hideadminlink{get;set;}    
    public Id DashboardID;
    public Id DashboardIDAnal;    
    public Id currentUserId{get;set;}  
    public string strANALYST{get;set;} 
    
    public BSureC_Dashboard()
    {
    	
	    currentUserId = UserInfo.getUserId();
        strANALYST = BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole').get(0);      
        
    	list<User> AnalystRole = [select id from User where Userrole.Name =:strANALYST and Id =:currentUserId];
    	if((AnalystRole != null && AnalystRole.size() > 0 ) )//&& AnalystRole.get(0).id == currentUserId)
		    {
		    hideanalystlink = false;
		    hideadminlink = true;	 	
		    getDashboardIDAnal();                     
		    }
		   
		else
		    {		    	
		    hideadminlink = false;
		    hideanalystlink = true;		
		    getDashboardID();
		    } 
    }   		    	
		    
    	public Id getDashboardID()
	    {	    			
	        lstDashboard = new list<Dashboard>();
	        lstDashboard = [Select Id From Dashboard where title = 'Customer Status Dashboard'];
	        if(lstDashboard != null && lstDashboard.size() > 0)
	        {
	            DashboardID = lstDashboard.get(0).Id;
	        }
	        return DashboardID;
	    }
	    
	    public id getDashboardIDAnal()
	    {			
			lstDashboardAnalyst = new list<Dashboard>();
	        lstDashboardAnalyst = [Select Id From Dashboard where title = 'Customer Status Dashboard for Analyst Custom Tab'];
	        if(lstDashboardAnalyst != null && lstDashboardAnalyst.size() > 0)
	        {
	            DashboardIDAnal = lstDashboardAnalyst.get(0).Id;
	        }
	        return DashboardIDAnal;
	    }    
    
}