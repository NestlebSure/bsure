/************************************************************************************************       
Controller Name         : BSureC_CustomerinfoIntermediate       
Date                    : 03/27/2013        
Author                  : Santhosh Palla       
Purpose                 : This Controller will handle the bulk Customer Upload Intermediate page      
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/26/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_CustomerinfoIntermediate 
{
	public list<BSureC_Customer_Basic_Info_Stage__c> lstStage{get;set;}//lst_cust_info_stage{get;set;}
	public string strState{get;set;}
	public map<string,string> mapAnalystManger{get;set;}
	public map<string, string> mapManagerAnalysts {get;set;}
	public map<string, string> mapAnalystNames {get;set;}
	public string strAnalystManger{get;set;} //To hold the category name
    public string strAnalystMangerId{get;set;}
    public string strBkpAnlyNames{get;set;}
    public string strBkpAnlyIds{get;set;}
    public string strAnalystRole {get; set;}    
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    public string strCategoryFlagForAnalyst{get;set;}
    public string strLocationFlagForAnalyst{get;set;}
    public string strManagerFlagForBkpAnalyst{get;set;}
    public String strGetRegion{get;set;} 
    list<BSureC_AdditionalAttributes__c> AAttributes = new list<BSureC_AdditionalAttributes__c>();
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    
    public static boolean isCustomerStageUpdate {get;set;}//= true;Boolean value for not invoking Supplier stage Update Trigger 
    
    public BSureC_CustomerinfoIntermediate()
	{
        strCategoryFlagForAnalyst = BSureC_CommonUtil.getConfigurationValues('BSureC_CategoryFlagForAnalyst').get(0);
        strLocationFlagForAnalyst = BSureC_CommonUtil.getConfigurationValues('BSureC_LocationFlagForAnalyst').get(0);
        strManagerFlagForBkpAnalyst = BSureC_CommonUtil.getConfigurationValues('BSureC_ManagerFlagForBkpAnalyst').get(0);
        strGetRegion = BSureC_CommonUtil.getConfigurationValues('BSureC_IncludeRegionforManagerSelection').get(0);
        
		getSupPendingRecords();
	}
	public void getSupPendingRecords()
	{
		lstStage = new list<BSureC_Customer_Basic_Info_Stage__c>();
		lstStage = [SELECT ID,Customer_ID__c,Customer_Name__c,State__c,State_Province__c,Manager__c,
					   	Manager_Name__c,Analyst__c,Analyst_Name__c,Planned_Review_Date__c,
					   	Publish_Flag__c,Credit_Limit__c,Customer_Group_Id__c,Country__c, Zone__c, Sub_Zone__c,
					   	Customer_Category__c,City__c, Collectors__c
				    FROM BSureC_Customer_Basic_Info_Stage__c 
					WHERE ID!=NULL 
					AND Publish_Flag__c=:false
					order BY Customer_Name__c];
		//system.debug('--intial--'+lstStage);
	}
	
	/// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    public List<SelectOption> getSStateList()
    {
        List<SelectOption> lstAvailableStates = new List<SelectOption>();
        lstAvailableStates.add(new Selectoption('',System.label.BSureS_VSelect));
        List<BSureC_State__c> lstSupplierStateInfo;
        string strSCICountryId='';
        if(lstStage.size()>0)
       	strSCICountryId= lstStage[0].Country__c;
       	//system.debug('---lstStage[0]---'+lstStage[0]);
        if(strSCICountryId != null && strSCICountryId != '')
        {
            lstSupplierStateInfo = [select id, Name from BSureC_State__c where IsActive__c = true and Customer_Country__c=:lstStage[0].Country__c order by Name];
        }
        
        if(lstSupplierStateInfo != null && lstSupplierStateInfo.size() > 0)
        {
            for(BSureC_State__c objStateEach:lstSupplierStateInfo)
            {
                if(objStateEach.Name != null)
                {
                    lstAvailableStates.add(new Selectoption(objStateEach.id,objStateEach.Name));
                }
            }
        }   
         //system.debug('---lstAvailableStates---'+lstAvailableStates);
        return lstAvailableStates;   
    }

	/// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    public List<SelectOption> getSAnalystList()
    {
    	
    	string strCategory= '';
        string strSCIZoneId= '';
        string strSCISubZoneId= '';
        string strSCICountryId= '';
        strAnalystRole = BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole').get(0);
        List<SelectOption> lstAnalysts = new List<SelectOption>(); 
           
        set<Id> userIds = new set<Id>(); 
        if(lstStage.size()>0)
        {
    		strCategory= lstStage[0].Customer_Category__c;
	    	strSCIZoneId= lstStage[0].Zone__c;
	    	strSCISubZoneId= lstStage[0].Sub_Zone__c;
	    	strSCICountryId= lstStage[0].Country__c;
    	    
	     	mapAnalystManger = new map<string,string>();
	     	mapManagerAnalysts = new map<string, string>();
	     	mapAnalystNames = new map<string, string>();
            
            if(!strCategoryFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Category is Required
            {
	     		list<BSureC_UserCategoryMapping__c> Categories = [select id,CategoryId__c,User__c 
	     													  from BSureC_UserCategoryMapping__c 
	     													  where CategoryId__c=:strCategory ];
				userIds.clear();
				for(BSureC_UserCategoryMapping__c Ucat:Categories){
					userIds.add(Ucat.User__c);
				}
            }
	        //system.debug('--strCategory--'+strCategory+'-strSCIZoneId-'+strSCIZoneId+'-strSCISubZoneId-'+strSCISubZoneId+'-strSCICountryId-'+strSCICountryId); 
	       
	        lstAnalysts.add(new Selectoption('',System.label.BSureS_VSelect));
	       	
            string strAnalystProfile = 'BSureC_Analyst';
            string strAnalystQuery = '';
            strAnalystQuery += 'SELECT id, User__c, Full_Name__c, BSureC_Zone__c, ';
            strAnalystQuery += 'BSureC_Sub_Zone__c, BSureC_Country__c, Manager__c, ';
            strAnalystQuery += 'Manager__r.Name ';
            strAnalystQuery += 'FROM BSureC_AdditionalAttributes__c ';
            strAnalystQuery += 'WHERE Id != null ';
            strAnalystQuery += 'AND User__r.IsActive = true ';
            strAnalystQuery += 'AND User__r.UserRole.Name =: strAnalystRole ';
            strAnalystQuery += 'AND User__r.Profile.Name =: strAnalystProfile ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))
            {
                strAnalystQuery += 'AND BSureC_Zone__c =: strSCIZoneId ';
                strAnalystQuery += 'AND BSureC_Sub_Zone__c =: strSCISubZoneId ';
                strAnalystQuery += 'AND BSureC_Country__c =: strSCICountryId ';
            }
            if(!strCategoryFlagForAnalyst.equalsIgnoreCase('FALSE')
                && userIds != null && userIds.size() > 0)   //If Category is Required
            {
                strAnalystQuery += 'AND User__c IN: userIds ';
            }
            
            AAttributes = Database.query(strAnalystQuery);
            /*
	       	list<BSureC_AdditionalAttributes__c> AAttributes = [select id, User__c, Full_Name__c,
									                                BSureC_Zone__c,BSureC_Sub_Zone__c,
									                                BSureC_Country__c,Manager__c,
									                                Manager__r.Name
								                                FROM BSureC_AdditionalAttributes__c 
								                                WHERE BSureC_Zone__c=:strSCIZoneId 
								                                AND BSureC_Sub_Zone__c=:strSCISubZoneId 
								                                AND BSureC_Country__c=:strSCICountryId
								                                AND User__r.IsActive = true
								                                AND User__r.UserRole.Name =: strAnalystRole 
								                                //AND User__c IN: userIds
												                AND User__r.Profile.Name =: 'BSureC_Analyst'];
            */
			if(AAttributes != null && AAttributes.size() > 0)
			{					                                                
				for(BSureC_AdditionalAttributes__c uA:AAttributes)
				{
					//userIds.add(ua.User__c);
					mapAnalystManger.put(ua.User__c,ua.Manager__c+','+ua.Manager__r.Name);
					mapAnalystNames.put(ua.User__c, ua.Full_Name__c);
					if(mapManagerAnalysts.containsKey(ua.Manager__c))
					{
						string objTempStr = mapManagerAnalysts.get(ua.Manager__c);
						objTempStr = ',' + ua.User__c + objTempStr +  ua.Full_Name__c + ';';
						mapManagerAnalysts.put(ua.Manager__c, objTempStr);
					}
					else
					{
						string objTempStr = '##';
						objTempStr = ',' + ua.User__c + objTempStr +  ua.Full_Name__c + ';';
						mapManagerAnalysts.put(ua.Manager__c, objTempStr);
					}
					lstAnalysts.add(new Selectoption(String.valueOf(uA.User__c), ua.Full_Name__c));
				}
			}
			//system.debug('mapManagerAnalysts-----' + mapManagerAnalysts);
			/*	
			if(!userIds.IsEmpty())
			{
				list<BSureC_UserCategoryMapping__c> Categories = [select id,CategoryId__c,User__c from BSureC_UserCategoryMapping__c where User__c in:userIds and CategoryId__c=:strCategory ];
				userIds.clear();
				for(BSureC_UserCategoryMapping__c Ucat:Categories){
					userIds.add(Ucat.User__c);
				}
			}
			
			list<User> analystUser = [select Id,Name from User where Id in:userIds and profile.Name =: 'BSureC_Analyst' and IsActive=true];
			
			for(User u:analystUser)
			{
				lstAnalysts.add(new Selectoption(String.valueOf(u.Id),u.name));
			}	
			*/		
        }
        return lstAnalysts;   
    }
     
 	public list<SelectOption> getCollectorsList()
 	{
 		list<SelectOption> lstCollectors = new list<SelectOption>();
 		
 		list<User> objCollectors = new list<User>([SELECT Id, Name, UserRole.Name
 												   FROM User
 												   WHERE IsActive = true
 												   AND Profile.Name =: 'BSureC_Collector' AND UserRole.Name =: 'Collector']);
 		
 		lstCollectors.add(new Selectoption('',System.label.BSureS_VSelect));
 		if(objCollectors != null && objCollectors.size() > 0)
 		{
 			for(User objEachUser : objCollectors)
 			{
 				lstCollectors.add(new SelectOption(string.valueOf(objEachUser.Id), objEachUser.Name + ' - ' + objEachUser.UserRole.Name));
 			}
 		}
 		return lstCollectors;
 	}
 	
	public pagereference UpdateSupStage()
	{
		pagereference pageref;
		//system.debug('--lstStage---'+lstStage);
		
		list<BSureC_Customer_Basic_Info_Stage__c> lstStageFinal = new list<BSureC_Customer_Basic_Info_Stage__c>();
		for(BSureC_Customer_Basic_Info_Stage__c objstage:lstStage)
		{
			string strState = objstage.State__c;
            string strAnalyst = objstage.Analyst__c;
            string strCollector = objstage.Collectors__c;
            strBkpAnlyIds = '';
            strBkpAnlyNames = '';
            //system.debug('strBkpAnlyNames+++123++' + strBkpAnlyNames);
            //Date dtPlannedRivw = objstage.Planned_Review_Date__c;
            //system.debug('strState--'+strState+'strAnalyst--'+strAnalyst);
            if(strState != '' && strState != null 
            	&& strAnalyst != '' && strAnalyst != null 
            	&& strCollector != '' && strCollector != null
            	&& objstage.Planned_Review_Date__c != null)
            {
            	integer cntSup = [select count() from BSureC_Customer_Basic_Info__c where Customer_Name__c=:objstage.Customer_Name__c AND State_Province__c=:objstage.State_Province__c AND City__c=:objstage.City__c];
	            if(cntSup>0)
	            {
	            	if(objstage.Exception__c==NULL)
				 	{
				 		objstage.Exception__c=Label.BSureS_VNameCityState;
				 		objstage.Status__c='Exception';
				 	}
				 	else
				 	{
				 		objstage.Exception__c+=','+Label.BSureS_VNameCityState;
				 		objstage.Status__c='Exception';
				 	}	
	            }
	        	string sstrAnalystManger = mapAnalystManger.get(objstage.Analyst__c);
		    	//system.debug('sstrAnalystManger--'+sstrAnalystManger);
				if(sstrAnalystManger!=null && sstrAnalystManger!='')
				{	
					string[] strIdName = sstrAnalystManger.split(',');
		    		strAnalystMangerId=strIdName[0];
		    		strAnalystManger=strIdName[1];
                }
                objstage.Manager__c = strAnalystMangerId;
                
                if(strManagerFlagForBkpAnalyst.equalsIgnoreCase('FALSE'))   //If Manager is Not Required 
                {
                    if(AAttributes != null && AAttributes.size() > 0)
                    {
                        for(BSureC_AdditionalAttributes__c objEachUser: AAttributes)
                        {
                            if(!strAnalyst.equalsIgnoreCase(objEachUser.User__c))
                            {
                                strBkpAnlyNames += objEachUser.Full_Name__c + ';';
                                strBkpAnlyIds += objEachUser.User__c + ',';
                            }
                        }
                        if(strBkpAnlyIds != '')
                        {
                            strBkpAnlyIds = strBkpAnlyIds.substring(0,strBkpAnlyIds.length() - 1);
                        }
                        if(strBkpAnlyNames != '')
                        {
                            strBkpAnlyNames = strBkpAnlyNames.substring(0, strBkpAnlyNames.length() - 1);
                        }
                    }
                }
                else
                {
		    		string strSubordinates = mapManagerAnalysts.get(strAnalystMangerId);
		    		//system.debug('strSubordinates------'+strSubordinates);
		    		if(strSubordinates != null)
		    		{
		    			string[] strBkpAnalysts = strSubordinates.split('##');
		    			strBkpAnlyIds = strBkpAnalysts[0].replace(',' + objstage.Analyst__c, '');
		    			if(strBkpAnlyIds != '')
		    			{
		    				strBkpAnlyIds = strBkpAnlyIds.substring(1,strBkpAnlyIds.length());
		    			}
		    			strBkpAnlyNames = strBkpAnalysts[1].replace(mapAnalystNames.get(objstage.Analyst__c) + ';', '');
		    			if(strBkpAnlyNames != '')
		    			{
		    				strBkpAnlyNames = strBkpAnlyNames.substring(0, strBkpAnlyNames.length() - 1);
		    			}
		    		}
				}
				if(strBkpAnlyIds != null && strBkpAnlyIds != '')
				{
					if(strBkpAnlyIds.length() > 255)
		            {
		            	strBkpAnlyIds = strBkpAnlyIds.substring(0, 254);
		            	strBkpAnlyIds = strBkpAnlyIds.substring(0, strBkpAnlyIds.lastIndexOf(','));	
		            }
	            	//objstage.Backup_Analysts__c = strBkpAnlyIds;
				}
				if(strBkpAnlyNames != null && strBkpAnlyNames != '')
				{
					if(strBkpAnlyNames.length() > 255)
		            {
		            	strBkpAnlyNames = strBkpAnlyNames.substring(0, 254);
		            }
					//objstage.Backup_Analyst_Names__c = strBkpAnlyNames;
				}
				//system.debug('strBkpAnlyNames++++++++' + strBkpAnlyNames);
             	lstStageFinal.add(objstage);
            }
			else
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the State Name, Analyst, Collector and Planned Review Date for all the records below.'));
                return null;
			}
           
            isCustomerStageUpdate=true; 
		}
		
		DataBase.update(lstStageFinal);
		isCustomerStageUpdate=false; 
		pageref = new pagereference('/apex/BsureC_CustomerinfouploadStagelistview');
		return pageref;
	}
}