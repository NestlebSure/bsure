/************************************************************************************************       
Controller Name         : BsureC_PaymentHistoryBatch       
Date                    : 11/19/2012        
Author                  : Santhosh Palla       
Purpose                 : To Inser the Payment History records into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/08/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/

global with sharing class BsureC_PaymentHistoryBatch implements Database.Batchable<SObject>
{
    global String str_Payment_History_stage=null;
    public list<BSureC_Payment_History_Stage__c> lst_Payment_History_Stage = new list<BSureC_Payment_History_Stage__c>();
    public List<BSureC_Payment_History_Publish__c> lst_Payment_History_Publish = new List<BSureC_Payment_History_Publish__c>();
   
    /// <summary>  
    /// Constructor  
    /// </summary>
    public BsureC_PaymentHistoryBatch()
    {
        str_Payment_History_stage='SELECT Id,Credit_Account__c,Group_Id__c,High_Credit__c,Total_Due__c,Information_Date__c,Past_Due__c,'+
        						  'Percent_Current__c,Customer_Basic_Info_Id__c,Customer_Division_Id__c FROM BSureC_Payment_History_Stage__c'+
        						  ' WHERE Exception__c = NULL AND Publish_Flag__c = false';
    }
    
    /// <summary>
    /// start method fires when class get executes
    /// </summary>
    /// <param name="BC"></param>
     global Iterable<sObject> start (Database.BatchableContext ctx)
     {         
        return Database.query(str_Payment_History_stage);
     }  
     
     /// <summary>
    /// execute method executes after start method to insert records in batch into publish object
    /// </summary>
    /// <param name="BC"></param>
    /// <param name="scope"></param>
     global void execute(Database.BatchableContext BC, list<BSureC_Payment_History_Stage__c> scope)
     {
        
    	String [] PaymenthistoryUpdateFields = new String [] {'Group_Id__c',
													      'High_Credit__c',
														  'Information_Date__c',
														  'Total_Due__c',
														  'Past_Due__c',
														  'Percent_Current__c',
														  'BSureC_Customer_Basic_Info__c',
														  'BSureC_Divisions__c'
	                                                     };
	                                                     
	    Map<String,Schema.SObjectField> objm = Schema.SObjectType.BSureC_Payment_History_Publish__c.fields.getMap();
	    for (String fieldToCheck : PaymenthistoryUpdateFields) {
			// Check if the user has create access on the each field
			if (!objm.get(fieldToCheck).getDescribe().isCreateable()) {
			  //return null;
			}
	    }
	            
        lst_Payment_History_Stage = new list<BSureC_Payment_History_Stage__c>();
        
        for(BSureC_Payment_History_Stage__c  obj : scope)
        {
            //system.debug('-----obj--'+obj);
             BSureC_Payment_History_Publish__c rec_payment_history_publish = new BSureC_Payment_History_Publish__c();
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Group_Id__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Group_Id__c.isCreateable()){
            rec_payment_history_publish.Group_Id__c = obj.Group_Id__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.High_Credit__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.High_Credit__c.isCreateable()){
            rec_payment_history_publish.High_Credit__c = obj.High_Credit__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Information_Date__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Information_Date__c.isCreateable()){
            rec_payment_history_publish.Information_Date__c = obj.Information_Date__c; //Date.valueof(system.now());
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Total_Due__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Total_Due__c.isCreateable()){
            rec_payment_history_publish.Total_Due__c = obj.Total_Due__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Past_Due__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Past_Due__c.isCreateable()){
            rec_payment_history_publish.Past_Due__c = obj.Past_Due__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Percent_Current__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.Percent_Current__c.isCreateable()){
            rec_payment_history_publish.Percent_Current__c = obj.Percent_Current__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.BSureC_Customer_Basic_Info__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.BSureC_Customer_Basic_Info__c.isCreateable()){
            rec_payment_history_publish.BSureC_Customer_Basic_Info__c = obj.Customer_Basic_Info_Id__c;
            }
            
            if (Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.BSureC_Divisions__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Publish__c.fields.BSureC_Divisions__c.isCreateable()){
            rec_payment_history_publish.BSureC_Divisions__c = obj.Customer_Division_Id__c;
            }
           // rec_payment_history_publish.Credit_Account__c=obj.Credit_Account__c;
            
            if (Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Publish_Flag__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Publish_Flag__c.isCreateable()){          
            obj.Publish_Flag__c=true;//for inserted records making flag as true to update in stgae
            }
            if (Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Status__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Status__c.isCreateable()){
            obj.Status__c='Published';
            }
            if (Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Publish_Date__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Publish_Date__c.isCreateable()){
            obj.Publish_Date__c=Date.valueof(system.now());
            }
            if (Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Status_Resource_Value__c.isUpdateable()
            ||Schema.sObjectType.BSureC_Payment_History_Stage__c.fields.Status_Resource_Value__c.isCreateable()){
            obj.Status_Resource_Value__c='/resource/1242640894000/Green';
            }
            
            lst_Payment_History_Publish.add(rec_payment_history_publish);
            lst_Payment_History_Stage.add(obj);
        }
        try
        {
        	if (BSureC_Payment_History_Publish__c.sObjectType.getDescribe().isCreateable())
            Database.insert(lst_Payment_History_Publish);
            try
            {
            	if (BSureC_Payment_History_Stage__c.sObjectType.getDescribe().isUpdateable())
            	Database.update(lst_Payment_History_Stage);
            }
            catch(Exception e)
            {
                //system.debug('--Exception-- in Update-'+e);
            }
        }
        catch(Exception e)
        {
            //system.debug('--Exception-- in insert-'+e);
        }
        
     }
     
     /// <summary>
    /// finish method executes after completing all batches execution
    /// </summary>
    /// <param name="BC"></param>
     global void finish(Database.BatchableContext BC)
     {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                                TotalJobItems, CreatedBy.Email
                                                    FROM AsyncApexJob 
                                                    WHERE Id =:BC.getJobId()];
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {'santhosh.p@vertexcs.com'};
      mail.setToAddresses(toAddresses);
      mail.setSubject('Borrowed book batch job for  ' + system.today()+ a.Status);
      mail.setPlainTextBody
       ('The batch Apex job processed on spend history publish list '+'Number of records' + a.TotalJobItems +
       ' inserted during batch job'+ a.NumberOfErrors + ' number of failures.');
       
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
     } 
}