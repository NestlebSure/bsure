/************************************************************************************************       
Controller Name         : BSureC_OverdueScheduler       
Date                    : 02/13/2013        
Author                  : Santhosh Palla       
Purpose                 : Scheduler       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          02/13/2013      Santhosh.Palla            Initial Version
                          05/02/2013	  Santhosh Palla			Added few more Email Alertes
                          24/06/2013      kishorekumar              Backup Analyst's are not getting any email on review updates
                          27/08/2014      kishorekumar              Due Date is Passed on this Customer mail Primary analyst not getting an email 
**************************************************************************************************/

global class BSureC_OverdueScheduler implements Schedulable{


    //Credit Increase Section Lists
    public list<BSureC_Credit_Increase__c> lst_credit_analysis{get;set;}
    public list<BSureC_Credit_Increase__c> lst_CAEmailReminder_3days{get;set;}
    public list<BSureC_Credit_Increase__c> lst_CAEndDate_past{get;set;}
    public list<BSureC_Credit_Increase__c> lst_CASubmitForApprovalPending{get;set;}
    public list<BSureC_Credit_Increase__c>  lst_creditAnalysis_insert{get;set;}
    
    public list<BSureC_Credit_Increase__c> lstCAEmailRemainder3daysPendingApprov{get;set;}//Review Status='Pending Approval'
    public list<BSureC_Credit_Increase__c> lstCreditAnalysisReminderEmailIntervel;
    //30,15,7,1 days in advance of the Expected Review End Date stating tha the Expected Review End Date is arriving and the review is still not in the Completed Status.
    public list<BSureC_Credit_Increase__c> lstEmailSubscribeIntervel{get;set;}
    public list<BSureC_Credit_Increase__c> lstCAOverdueAfer7days{get;set;}
    
    
    //Email Queue Lists
    public list<BSureC_Email_Queue__c> objEmailQueueList{get;set;}
    public list<BSureC_Email_Queue__c> objEmailQueueList2{get;set;}
    public list<BSureC_Email_Queue__c> objEmailQueueList3{get;set;}
    public list<BSureC_Email_Queue__c> objEmailQueueList4{get;set;}
    public list<BSureC_Email_Queue__c> objEmailQueueList5{get;set;}
    public list<BSureC_Email_Queue__c> objEmailQueueList6Overdue{get;set;}
    public list<BSureC_Email_Queue__c> lstEmailQueueReminerIntervel{get;set;} 
    public list<BSureC_Email_Queue__c> lstEmailQueueOverdueAfer7days{get;set;}
    
    //Email queue Variables
    public string strEmailSubject ='';
    public string strEmailBody=''; 
    public set<Id> setEmailRcpts {get; set;}
    
    //public map<Id,User> mapUserlist{get;set;}
    
     //Task
    public Task lst_create_task{get;set;}
    public Task lst_create_task_RSStarted{get;set;}
    public list<Task> lstTaskInsert{get;set;}
    public list<Task> lstTaskRSStarted{get;set;}
    public list<BSureC_Customer_Basic_Info__c> lstCustomer15beforePRD{get;set;}
    public list<string> lstString{get;set;}  
    DateTime dateObj;
    public string strPlannedReviewDate{get;set;}
    public string strExceptedReviewEndDate{get;set;}
    
    global BSureC_OverdueScheduler()
    {
        
    }
    
    global void execute(SchedulableContext sc) 
    {
        List<BSureC_Customer_Basic_Info__c> lst_customer_info = new List<BSureC_Customer_Basic_Info__c>();
        List<BSureC_Customer_Basic_Info__c> lst_update_status = new List<BSureC_Customer_Basic_Info__c>();
        Map<Id,BSureC_Customer_Basic_Info__c> map_cust_info = new Map<Id,BSureC_Customer_Basic_Info__c>();
        
        //mapUserlist = new map<Id,user>([select id,Name,Email from User limit 20000]);
        
        
        
        lst_customer_info = [SELECT Id,Analyst__c,Manager__c,Zone__c,Backup_Analyst_Names__c,
        					 Customer_Name__c,Review_Status__c,
                             Manager__r.Receive_Email_Notifications__c,
                             Manager__r.Email,
                             Manager__r.FirstName,
                             Manager__r.LastName,
                             Analyst__r.Email,
                             Analyst__r.FirstName,
                             Analyst__r.LastName   
                             FROM BSureC_Customer_Basic_Info__c 
                             WHERE Analyst__c != NULL
                             AND Next_Review_Date__c != NULL 
                             AND Next_Review_Date__c < TODAY 
                             AND Review_Status__c != 'Started'];
        //system.debug('---------lst_customer_info--'+lst_customer_info);
         
       // lst_CAEmailReminder_3days = new list<BSureC_Credit_Increase__c>();
        lst_CAEmailReminder_3days = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Status__c,Customer_Basic_Info_Id__c,Customer_Name__c,
                                     Customer_Basic_Info_Id__r.Backup_Analysts__c,
                                     Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c,
                                     Customer_Basic_Info_Id__r.Analyst__r.Email,
                                     Customer_Basic_Info_Id__r.Analyst__r.Name,
                                     Customer_Basic_Info_Id__r.Analyst__r.FirstName,
                                     Customer_Basic_Info_Id__r.Analyst__r.LastName
                                     FROM BSureC_Credit_Increase__c 
                                     WHERE Review_End_Date__c =: system.today().addDays(3) 
                                     AND Status__c = 'Started'];                        
        
        //lst_CAEndDate_past = new list<BSureC_Credit_Increase__c>();
        lst_CAEndDate_past = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Status__c,Customer_Basic_Info_Id__c,Customer_Name__c,
        					  Customer_Basic_Info_Id__r.Backup_Analysts__c,
                              Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c,
                              Customer_Basic_Info_Id__r.Analyst__r.Email,
                              Customer_Basic_Info_Id__r.Analyst__r.Name
                              FROM BSureC_Credit_Increase__c 
                              WHERE Review_End_Date__c != NULL 
                              AND Review_End_Date__c < TODAY 
                              AND Status__c = 'Started'];
        
        //lst_CASubmitForApprovalPending = new list<BSureC_Credit_Increase__c>();
        lst_CASubmitForApprovalPending = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Status__c,Customer_Basic_Info_Id__c,Customer_Name__c,
        								  Customer_Basic_Info_Id__r.Backup_Analysts__c,
                                          Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c,  
                                          Customer_Basic_Info_Id__r.Manager__r.Receive_Email_Notifications__c,
                                          Customer_Basic_Info_Id__r.Analyst__r.Email,
                                          Customer_Basic_Info_Id__r.Analyst__r.Name,
                                          Customer_Basic_Info_Id__r.Manager__r.Email,
                                          Customer_Basic_Info_Id__r.Manager__r.Name
                                          FROM BSureC_Credit_Increase__c 
                                          WHERE Review_End_Date__c != null 
                                          AND Review_End_Date__c < TODAY 
                                          AND Status__c = 'Pending Approval'];
        
       lstCAEmailRemainder3daysPendingApprov = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Customer_Basic_Info_Id__c,Customer_Name__c,
                                				Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c, 
                                				Customer_Basic_Info_Id__r.Analyst__r.Name, 
                                				Customer_Basic_Info_Id__r.Analyst__r.Email, 
				                                Customer_Basic_Info_Id__r.Manager__r.Receive_Email_Notifications__c,
				                                Customer_Basic_Info_Id__r.Manager__r.Name,
				                                Customer_Basic_Info_Id__r.Manager__r.Email
				                                FROM BSureC_Credit_Increase__c 
				                                WHERE Review_End_Date__c != null 
				                                AND Review_End_Date__c =: system.today().addDays(3) 
				                                AND Status__c = 'Pending Approval'];
                                
        // reminder Email Intervel 30,15,7,1 days
       lstCreditAnalysisReminderEmailIntervel = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Customer_Basic_Info_Id__c,Status__c,Customer_Name__c,
       											 Customer_Basic_Info_Id__r.Analyst__c,
                                                 Customer_Basic_Info_Id__r.Analyst__r.Name,
                                                 Customer_Basic_Info_Id__r.Analyst__r.Email 
                                                 FROM BSureC_Credit_Increase__c 
                                                 WHERE Status__c = 'Started' 
                                                 AND Review_End_Date__c != null 
                                                 AND (Review_End_Date__c =: system.today().addDays(30) 
                                                 OR Review_End_Date__c =: system.today().addDays(15) 
                                                 OR Review_End_Date__c =: system.today().addDays(7) 
                                				 OR Review_End_Date__c =: system.today().addDays(1) 
                                				 OR Review_End_Date__c = TODAY) ];
                                				 
        //Creating a task Before 15 day's of Planned Review Date 
         lstCustomer15beforePRD = [SELECT id,Planned_Review_Date__c,Analyst__c,Customer_Name__c
         						   FROM BSureC_Customer_Basic_Info__c 
         						   WHERE Planned_Review_Date__c != null 
         						   AND Planned_Review_Date__c =: system.today().adddays(15)];
        ////system.debug('lstCustomer15beforePRD===size======='+lstCustomer15beforePRD.size());
        
        // reminder Email Intervel 30,15,7,1 days
        if(lstCreditAnalysisReminderEmailIntervel != null && lstCreditAnalysisReminderEmailIntervel.size() > 0)
        {
            lstEmailQueueReminerIntervel = new list<BSureC_Email_Queue__c>();
            for(BSureC_Credit_Increase__c cEmailIntervel : lstCreditAnalysisReminderEmailIntervel)
            {
                string strEmailReminderStatus = 'NEW';
                
                string strSubjectEIntervel = '';
                
                if(cEmailIntervel.Review_End_Date__c == system.today() )
                {
                    strSubjectEIntervel = 'Expected Review End Date is arrived and the review process should have been completed by today.';
                }
                else
                {
                    strSubjectEIntervel= 'Expected Review End Date is arriving and the review is still not in the Completed ';                  
                }
                string strBodyEmailIntervel = '';
                
                strBodyEmailIntervel += '<table>';
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date is arriving and the review is still not in the Completed for this Supplier : '+cEmailIntervel.Customer_Name__c + '</td></tr>';
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date :'+ cEmailIntervel.Review_End_Date__c +'</td></tr>';
                strBodyEmailIntervel += '</table>';
                
                if(cEmailIntervel.Customer_Basic_Info_Id__r.Analyst__c != null)
                {
                    BSureC_Email_Queue__c objEmailQueueEIntervel = new BSureC_Email_Queue__c();
                    objEmailQueueEIntervel = BuildEmailQueue(strBodyEmailIntervel, 'High', strEmailReminderStatus,
                                                    strSubjectEIntervel,'NO',cEmailIntervel.Customer_Basic_Info_Id__r.Analyst__r.Email,
                                                    true, system.today(), cEmailIntervel.Customer_Basic_Info_Id__r.Analyst__r.Name);
                    lstEmailQueueReminerIntervel.add(objEmailQueueEIntervel);
                    
                }
            }
            if(lstEmailQueueReminerIntervel != null && lstEmailQueueReminerIntervel.size() > 0)
            {
                Database.Saveresult[] objSaveEmailReminder = Database.insert(lstEmailQueueReminerIntervel);
            } 
        }
         //Creating a task Before 15 day's of Planned Review Date
        if(lstCustomer15beforePRD != null && lstCustomer15beforePRD.size() > 0)
        {
            //system.debug('testtttttttttttt');
            lstTaskInsert = new list<Task>(); 
            for(BSureC_Customer_Basic_Info__c CustObj:lstCustomer15beforePRD)
            {
                lst_create_task = new Task();
                if(CustObj.Analyst__c != null)
                {
                    lst_create_task.OwnerId = CustObj.Analyst__c;
                    lst_create_task.Subject = 'Create a New Review - ( '+CustObj.Customer_Name__c+' )';
                    //lst_create_task.Subject = 'call';
                    lst_create_task.ActivityDate = System.today(); 
                    lst_create_task.Status = 'Not Started';
                    lst_create_task.whatId = CustObj.Id;
                }
                lstTaskInsert.add(lst_create_task); 
            } 
            if(lstTaskInsert != null && lstTaskInsert.size() > 0)
	        {
	            Database.Saveresult[] objCreateTask15daysBefore = Database.insert(lstTaskInsert);
	        }  
        }
        
        if(lst_customer_info != null && lst_customer_info.size()>0)
        {
            for(BSureC_Customer_Basic_Info__c rec_cust_info : lst_customer_info)    
             {
                rec_cust_info.Review_Status__c = 'Overdue';
                if(rec_cust_info.Analyst__c != null)
                {
                    map_cust_info.put( rec_cust_info.Analyst__c, rec_cust_info);
                    lst_update_status.add(rec_cust_info);
                }
             }  
             
            try
            {
                Database.update(lst_update_status);
            }
            catch(Exception e)
            {
                //system.debug('--Exception-- in BSureC_OverdueScheduler-'+e);
            }
            if(map_cust_info != null)
            {
                objEmailQueueList = new list<BSureC_Email_Queue__c>();
                for(BSureC_Customer_Basic_Info__c sObjEmail :lst_customer_info)
                {
                    BSureC_Email_Queue__c objEmailQueue = new BSureC_Email_Queue__c();
                    string strEmailStatus = 'NEW';
                    setEmailRcpts = new set<Id>();
                    strEmailSubject = 'Due Date is Passed on this Customer : '+sObjEmail.Customer_Name__c;
                    strEmailBody = 'Due Date is Passed on this Customer '+sObjEmail.Customer_Name__c +' no Action has been taken. ';
                    setEmailRcpts.add(sObjEmail.Manager__c);
                    setEmailRcpts.add(sObjEmail.Analyst__c);
                    //system.debug('Email Flag==========='+sObjEmail.Manager__r.Receive_Email_Notifications__c);
                    if(sObjEmail.Manager__r.Receive_Email_Notifications__c == false)
                    {
                        strEmailStatus = 'SENT';
                    }
                    objEmailQueue.Email_Body__c = strEmailBody;
                    objEmailQueue.Email_Priority__c = 'High';
                    objEmailQueue.Email_Status__c = strEmailStatus;
                    objEmailQueue.Email_Subject__c = strEmailSubject;
                    objEmailQueue.Is_Daily_Digest__c = 'NO';
                    objEmailQueue.Recipient_Address__c = sObjEmail.Manager__r.Email;
                    objEmailQueue.Send_Immediate__c = true;
                    objEmailQueue.Send_On_Date__c = system.today();
                    objEmailQueue.Recipient_Name__c = sObjEmail.Manager__r.FirstName + ' ' + sObjEmail.Manager__r.LastName;
                    objEmailQueueList.add(objEmailQueue);
                    
                    BSureC_Email_Queue__c  objEmailQueueAnaly = new BSureC_Email_Queue__c();
                    objEmailQueueAnaly.Email_Body__c = strEmailBody;
                    objEmailQueueAnaly.Email_Priority__c = 'High';
                    objEmailQueueAnaly.Email_Status__c = strEmailStatus;
                    objEmailQueueAnaly.Email_Subject__c = strEmailSubject;
                    objEmailQueueAnaly.Is_Daily_Digest__c = 'NO';
                    objEmailQueueAnaly.Recipient_Address__c = sObjEmail.Analyst__r.Email;
                    objEmailQueueAnaly.Send_Immediate__c = true;
                    objEmailQueueAnaly.Send_On_Date__c = system.today();
                    objEmailQueueAnaly.Recipient_Name__c = sObjEmail.Analyst__r.FirstName + ' ' + sObjEmail.Analyst__r.LastName;
                    objEmailQueueList.add(objEmailQueueAnaly);
                    
                }
                if(objEmailQueueList != null && objEmailQueueList.size() > 0)
                {
                    Database.Saveresult[] objSaveResult = Database.insert(objEmailQueueList);
                }
            }
         }
         
        lst_credit_analysis = new list<BSureC_Credit_Increase__c>();
        lst_credit_analysis = [SELECT id,Review_Start_Date__c,Review_End_Date__c,Status__c,Customer_Basic_Info_Id__c,
        					   Customer_Basic_Info_Id__r.Analyst__c,Customer_Name__c 
                               FROM BSureC_Credit_Increase__c 
                               WHERE Review_Start_Date__c != NULL 
                               AND Review_Start_Date__c <= TODAY
                               AND Status__c = 'Not Started'];
         
        if(lst_credit_analysis != null && lst_credit_analysis.size() > 0)
        {
            lst_creditAnalysis_insert = new list<BSureC_Credit_Increase__c>();
            lstTaskRSStarted = new list<Task>();
            for(BSureC_Credit_Increase__c caObj :lst_credit_analysis )
            {
                caObj.Status__c = 'Started';
                lst_creditAnalysis_insert.add(caObj);
                //Create task for Review has started and not been completed yet ( Here Review Status is 'Started' )
                lst_create_task_RSStarted = new Task();
                if(caObj.Customer_Basic_Info_Id__r.Analyst__c != null)
                {
                	lst_create_task_RSStarted.OwnerId = caObj.Customer_Basic_Info_Id__r.Analyst__c;
                    //lst_create_task_RSStarted.Subject = 'Review has started and not been completed yet';
                    lst_create_task_RSStarted.Subject = 'The Expected Review Start Date has arrived. It is time to start the review - ('+caObj.Customer_Name__c+')';
                    //lst_create_task.Subject = 'call';
                    lst_create_task_RSStarted.ActivityDate = system.today();
                    lst_create_task_RSStarted.Status = 'Not Started';
                    lst_create_task_RSStarted.whatId =  caObj.Customer_Basic_Info_Id__c;
                    
                    lstTaskRSStarted.add(lst_create_task_RSStarted);
                }
                
            }
            try
            {
                Database.update(lst_creditAnalysis_insert);
                if(lstTaskRSStarted != null && lstTaskRSStarted.size() > 0)
	            {
	                Database.Saveresult[] objCreateTaskRSStarted = Database.insert(lstTaskRSStarted);
	            }
            }
            catch(Exception e)
            {
                //system.debug('--Exception-- in BSureC_OverdueScheduler-'+e);
            }
        }
        if(lst_CAEmailReminder_3days != null && lst_CAEmailReminder_3days.size() > 0)
        {
            string strEmailStatus = 'NEW';
            objEmailQueueList2 = new list<BSureC_Email_Queue__c>(); 
            for(BSureC_Credit_Increase__c objEmailCA : lst_CAEmailReminder_3days)
            {
                BSureC_Email_Queue__c  objEmailQueue2 = new BSureC_Email_Queue__c();
                lstString = new list<string>();
                if(objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c == false)
                {
                    strEmailStatus = 'SENT';
                }
                String strEBSubject = 'Review has not been submitted for approval ';
                string strEBReaminderBody ='';
                strEBReaminderBOdy = '<table>';
                strEBReaminderBOdy += ' <tr><rd> Review has not been submitted for approval for this Customer :'+objEmailCA.Customer_Name__c +'</td></tr>';
                if(objEmailCA.Review_End_Date__c != null)
                {
                    strEBReaminderBOdy += '<tr><rd> Expected Review End Date is'+ objEmailCA.Review_End_Date__c +'</td></tr>';
                }   
                strEBReaminderBOdy += '</table>'    ;
                
                if(objEmailCA.Customer_Basic_Info_Id__r.Analyst__c != null)
                {
                	objEmailQueue2 = BuildEmailQueue(strEBReaminderBody, 'High', strEmailStatus,
                                                    strEBSubject,'NO',objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.Name);
                    objEmailQueueList2.add(objEmailQueue2);
                }
                //Commented below code : kishore on 24/06/2013
                /*
                if(objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c != null && objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c.contains(','))
                {
                	 lstString = objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c.split(',');
                	 for(string strobj : lstString)
                	 {
                	 	BSureC_Email_Queue__c  objEmailQueueBackupAnaly = new BSureC_Email_Queue__c();
                	 	objEmailQueueBackupAnaly = BuildEmailQueue(strEBReaminderBody, 'High', strEmailStatus,
                                                    strEBSubject,'NO',mapUserlist.get(strobj).Email,
                                                    true, system.today(), mapUserlist.get(strobj).Name);
                        objEmailQueueList2.add(objEmailQueueBackupAnaly);
                	 }
                }
                else if(objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c != null)
                {
                	BSureC_Email_Queue__c  objEmailQueueBackupAnaly = new BSureC_Email_Queue__c();
                	objEmailQueueBackupAnaly = BuildEmailQueue(strEBReaminderBody, 'High', strEmailStatus,
                                                    strEBSubject,'NO',mapUserlist.get(objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c).Email,
                                                    true, system.today(), mapUserlist.get(objEmailCA.Customer_Basic_Info_Id__r.Backup_Analysts__c).Name);
                    objEmailQueueList2.add(objEmailQueueBackupAnaly);
                	
                }*/
              /*objEmailQueue2.Email_Body__c = strEBReaminderB0dy;
                objEmailQueue2.Email_Priority__c = 'High';
                objEmailQueue2.Email_Status__c = strEmailStatus;
                objEmailQueue2.Email_Subject__c = strEBSubject;
                objEmailQueue2.Is_Daily_Digest__c = 'NO'; 
                objEmailQueue2.Recipient_Address__c = objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.Email;
                objEmailQueue2.Send_Immediate__c = true;
                objEmailQueue2.Send_On_Date__c = system.today();
                objEmailQueue2.Recipient_Name__c = objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.FirstName + ' ' + objEmailCA.Customer_Basic_Info_Id__r.Analyst__r.LastName;
                objEmailQueueList2.add(objEmailQueue2);*/
            }
            if(objEmailQueueList2 != null && objEmailQueueList2.size() > 0)
            {
                Database.Saveresult[] objSaveResult2 = Database.insert(objEmailQueueList2);
            }   
        }
        
        if(lst_CAEndDate_past != null && lst_CAEndDate_past.size() > 0)
        {
            string strEmailStatus = 'NEW';
            objEmailQueueList3 = new list<BSureC_Email_Queue__c>(); 
            
            for(BSureC_Credit_Increase__c objEmailCAPast : lst_CAEndDate_past)
            {
                //BSureC_Email_Queue__c  objEmailQueue3 = new BSureC_Email_Queue__c();
                lstString = new list<string>();
                if(objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c == false)
                {
                    strEmailStatus = 'SENT';
                }
                string strSubjectEmailSApast = 'Expected Review End Date has passed';
                string strBodyEmailSApast = '';
                strBodyEmailSApast += '<table>';
                strBodyEmailSApast += '<tr><td> Expected Review End Date has passed fot this Customer :'+ objEmailCAPast.Customer_Name__c  +'</td></tr>';
                if(objEmailCAPast.Review_End_Date__c != null)
                {
                    strBodyEmailSApast += '<tr><td> Expected Review End Date is :' +objEmailCAPast.Review_End_Date__c+ '</td></tr>';
                }
                strBodyEmailSApast += '</table>';
                if(objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__c != null)
                {
                	 BSureC_Email_Queue__c  objEmailQueue3 = new BSureC_Email_Queue__c();
                	 objEmailQueue3 = BuildEmailQueue(strBodyEmailSApast, 'High', strEmailStatus,
                                                    strSubjectEmailSApast,'NO',objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__r.Name);
                     objEmailQueueList3.add(objEmailQueue3); 
                }
                //Commented code kishore on 24/06/2013
                /*
                if(objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c != null && objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c.contains(','))
                {
                	 lstString = objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c.split(',');
                	 for(string sObjBA: lstString)
                	 {
                	 	BSureC_Email_Queue__c objEmailQBackUpAnalyst = new BSureC_Email_Queue__c();
                	 	objEmailQBackUpAnalyst = BuildEmailQueue(strBodyEmailSApast, 'High', strEmailStatus,
                                                    strSubjectEmailSApast,'NO',mapUserlist.get(sObjBA).Email,
                                                    true, system.today(), mapUserlist.get(sObjBA).Name);
                        objEmailQueueList3.add(objEmailQBackUpAnalyst);
                	 }
                }
                else if(objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c != null)
                {
                	BSureC_Email_Queue__c objEmailQBackUpAnalyst = new BSureC_Email_Queue__c();
                	objEmailQBackUpAnalyst = BuildEmailQueue(strBodyEmailSApast, 'High', strEmailStatus,
                                                    strSubjectEmailSApast,'NO',mapUserlist.get(objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c).Email,
                                                    true, system.today(), mapUserlist.get(objEmailCAPast.Customer_Basic_Info_Id__r.Backup_Analysts__c).Name);
                    objEmailQueueList3.add(objEmailQBackUpAnalyst);
                }*/
                /*objEmailQueue3.Email_Body__c = strBodyEmailSApast;
                objEmailQueue3.Email_Priority__c = 'High';
                objEmailQueue3.Email_Status__c = strEmailStatus;
                objEmailQueue3.Email_Subject__c = strSubjectEmailSApast;
                objEmailQueue3.Is_Daily_Digest__c = 'NO'; 
                objEmailQueue3.Recipient_Address__c = objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__r.Email;
                objEmailQueue3.Send_Immediate__c = true;
                objEmailQueue3.Send_On_Date__c = system.today();
                objEmailQueue3.Recipient_Name__c = objEmailCAPast.Customer_Basic_Info_Id__r.Analyst__r.Name;
                objEmailQueueList3.add(objEmailQueue3);*/
            }   
            if(objEmailQueueList3 != null && objEmailQueueList3.size() > 0)
            {
                Database.Saveresult[] objSaveResult3 = Database.insert(objEmailQueueList3);
            }   
        }   
        
        if(lst_CASubmitForApprovalPending != null && lst_CASubmitForApprovalPending.size() > 0)
        {
            string strAnlyEmailStatus = 'NEW';
            string strMngrEmailStatus = 'NEW';
            objEmailQueueList4 = new list<BSureC_Email_Queue__c>(); 
            for(BSureC_Credit_Increase__c objEmailCASAppPast : lst_CASubmitForApprovalPending)
            {
                BSureC_Email_Queue__c  objEmailQueueAnly = new BSureC_Email_Queue__c();
                BSureC_Email_Queue__c  objEmailQueueMngr = new BSureC_Email_Queue__c();
              /*  if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__r.Receive_Email_Notifications__c == false)
                {
                    strAnlyEmailStatus = 'SENT';
                }
                if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__r.Receive_Email_Notifications__c == false)
                {
                    strMngrEmailStatus = 'SENT';
                }*/
                lstString = new list<string>();
                string strSubjectSApprov = 'Review has been submitted for approval and the Expected Review End Date has passed ';
                string strBodySApprove = '';
                
                strBodySApprove = '<table>';
                strBodySApprove += '<tr><td> Review has been submitted for approval and the Expected Review End Date has passed for this Customer : '+objEmailCASAppPast.Customer_Name__c + '</td></tr>';
                strBodySApprove += '<tr><td> Expected Review End Date :'+ objEmailCASAppPast.Review_End_Date__c +'</td></tr>';
                strBodySApprove += '</table>';
                
                if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__c != null)
                {
                	objEmailQueueAnly = BuildEmailQueue(strBodySApprove, 'High', strAnlyEmailStatus,
                                                    strSubjectSApprov,'NO',objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__r.Name);
                    objEmailQueueList4.add(objEmailQueueAnly);
                }
               /* objEmailQueueAnly.Email_Body__c = strBodySApprov;
                objEmailQueueAnly.Email_Priority__c = 'High';
                objEmailQueueAnly.Email_Status__c = strAnlyEmailStatus;
                objEmailQueueAnly.Email_Subject__c = strSubjectSApprov;
                objEmailQueueAnly.Is_Daily_Digest__c = 'NO'; 
                objEmailQueueAnly.Recipient_Address__c = objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__r.Email;
                objEmailQueueAnly.Recipient_Name__c = objEmailCASAppPast.Customer_Basic_Info_Id__r.Analyst__r.Name;
                objEmailQueueAnly.Send_Immediate__c = true;
                objEmailQueueAnly.Send_On_Date__c = system.today();*/
                
                if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__c != null)
                {
                	objEmailQueueMngr = BuildEmailQueue(strBodySApprove, 'High', strMngrEmailStatus,
                                                    strSubjectSApprov,'NO',objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__r.Email,
                                                    true, system.today(), objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__r.Name);
                    objEmailQueueList4.add(objEmailQueueMngr);
                }
                /*objEmailQueueMngr.Email_Body__c = strBodySApprov;
                objEmailQueueMngr.Email_Priority__c = 'High';
                objEmailQueueMngr.Email_Status__c = strMngrEmailStatus;
                objEmailQueueMngr.Email_Subject__c = strSubjectSApprov;
                objEmailQueueMngr.Is_Daily_Digest__c = 'NO'; 
                objEmailQueueMngr.Recipient_Address__c = objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__r.Email;
                objEmailQueueMngr.Recipient_Name__c = objEmailCASAppPast.Customer_Basic_Info_Id__r.Manager__r.Name;
                objEmailQueueMngr.Send_Immediate__c = true;
                objEmailQueueMngr.Send_On_Date__c = system.today();*/
                //Commented below code kishore on 24/06/2013
                /*
                if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c != null && objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c.contains(','))
                {
                	lstString = objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c.split(',');
                	for(string strCApObj : lstString)
                	{
                		BSureC_Email_Queue__c  objEmailQueueBAMngr = new BSureC_Email_Queue__c();
                		objEmailQueueBAMngr = BuildEmailQueue(strBodySApprove, 'High', strMngrEmailStatus,
                                                    strSubjectSApprov,'NO',mapUserlist.get(strCApObj).Email,
                                                    true, system.today(),mapUserlist.get(strCApObj).Name);
                        objEmailQueueList4.add(objEmailQueueBAMngr);
                	}
                }
                else if(objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c != null)
                {
                	BSureC_Email_Queue__c  objEmailQueueBAMngr = new BSureC_Email_Queue__c();
                	objEmailQueueBAMngr =  BuildEmailQueue(strBodySApprove, 'High', strMngrEmailStatus,
                                                    strSubjectSApprov,'NO',mapUserlist.get(objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c).Email,
                                                    true, system.today(),mapUserlist.get(objEmailCASAppPast.Customer_Basic_Info_Id__r.Backup_Analysts__c).Name);
                    objEmailQueueList4.add(objEmailQueueBAMngr);
                }*/
               // objEmailQueueList4.add(objEmailQueueAnly);
               // objEmailQueueList4.add(objEmailQueueMngr);
            }
            if(objEmailQueueList4 != null && objEmailQueueList4.size() > 0)
            {
                Database.Saveresult[] objSaveResult4 = Database.insert(objEmailQueueList4);
            }
        }
        if(lstCAEmailRemainder3daysPendingApprov != null && lstCAEmailRemainder3daysPendingApprov.size() > 0)
        {
        	string strEmailStatusPendingApprov = 'NEW';
            objEmailQueueList5 = new list<BSureC_Email_Queue__c>();
            for(BSureC_Credit_Increase__c objEmailCAPastPendingApprov : lstCAEmailRemainder3daysPendingApprov)
            {
                BSureC_Email_Queue__c  objEmailQueue5 = new BSureC_Email_Queue__c();
                string strBodySpendingApprove = '';
                string strSubjectSApprov = 'Review has been submitted for approval and the Expected Review End Date due completion in 3 days. ';
                if(objEmailCAPastPendingApprov.Review_End_Date__c != null)
                {
                    strBodySpendingApprove += '<table>';
                    strBodySpendingApprove += '<tr><td height="21px"> Review has been submitted for approval and the Expected Review End Date due completion in 3 days for this Supplier : '+objEmailCAPastPendingApprov.Customer_Name__c + '</td></tr>';
                    strBodySpendingApprove += '<tr><td height="21px"> Expected Review End Date :'+ objEmailCAPastPendingApprov.Review_End_Date__c +'</td></tr>';
                    strBodySpendingApprove += '</table>';
                }
                if(objEmailCAPastPendingApprov.Customer_Basic_Info_Id__r.Manager__C != null)
                {
                    objEmailQueue5 = BuildEmailQueue(strBodySpendingApprove, 'High', strEmailStatusPendingApprov,
                                                    strSubjectSApprov,'NO',objEmailCAPastPendingApprov.Customer_Basic_Info_Id__r.Manager__r.Email,
                                                    true, system.today(), objEmailCAPastPendingApprov.Customer_Basic_Info_Id__r.Manager__r.Name);  
                    objEmailQueueList5.add(objEmailQueue5);
                }    
            }
            if(objEmailQueueList5 != null && objEmailQueueList5.size() > 0)
	        {
	            Database.Saveresult[] objSaveResult5 = Database.insert(objEmailQueueList5);
	        }
        }
    }
    public BSureC_Email_Queue__c BuildEmailQueue(string strBody, string strPriority, string strStatus,
                                                 string strSubject, string strDailyDigest, string strRecAdd,
                                                 boolean strSendImm, date strSendDate, string strRecName)
    {
        BSureC_Email_Queue__c objEmailQueue = new BSureC_Email_Queue__c();
        objEmailQueue.Email_Body__c = strBody;
        objEmailQueue.Email_Priority__c = strPriority;
        objEmailQueue.Email_Status__c = strStatus;
        objEmailQueue.Email_Subject__c = strSubject;
        objEmailQueue.Is_Daily_Digest__c = strDailyDigest; 
        objEmailQueue.Recipient_Address__c = strRecAdd;
        objEmailQueue.Send_Immediate__c = strSendImm;
        objEmailQueue.Send_On_Date__c = strSendDate;
        objEmailQueue.Recipient_Name__c = strRecName;
        
        return objEmailQueue;
    }
}