/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestbSureS_CreditSafePDFController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.test.startTest();
        string SuppName ='';
        string AttchId ='';
        BSureS_Basic_Info__c objBasicInfo = new BSureS_Basic_Info__c();
        objBasicInfo.Supplier_Name__c = 'Demo Supplier';
        objBasicInfo.BISFileNumber__c = '7986548975';
        objBasicInfo.Globe_ID__c = '589647';
        objBasicInfo.Company_Registration_ID__c ='US023/X/US1234567';
        objBasicInfo.Planned_Review_Date__c = system.today();
        insert objBasicInfo;
        
        SuppName=objBasicInfo.Supplier_Name__c;
        
        DateTime d = Date.Today() ;
        string dateStr =  d.format('ddMMyy') ;
        BSureS_Credit_Review_Section__c objCreditInfo = new BSureS_Credit_Review_Section__c();
        objCreditInfo.Supplier_ID__c = objBasicInfo.id;
        objCreditInfo.TitleDescription__c = dateStr+'_Creditsafe_Rpt_'+objBasicInfo.Globe_ID__c;
        insert objCreditInfo;
        string XMLString ='<Envelope>'+
							'<Body>'+
								'<RetrieveCompanyOnlineReportResponse>'+
									'<RetrieveCompanyOnlineReportResult>'+
										'<Messages><Message Type="Information" Code="20103">test.</Message><Message Type="Information" Code="20103">OnlineReports</Message></Messages>'+
       									'<Reports>'+
											'<Report CompanyId="US023/X/US81933214" OrderNumber="2315143" Language="EN" ReportCurrency="USD" >'+
											'<CompanySummary>' +
												'<BusinessName>SUN EDISON LLC</BusinessName> <Country>US</Country><Number>US81933214</Number><CompanyRegistrationNumber>201230110146</CompanyRegistrationNumber>'+
												'<MainActivity><ActivityCode>3433</ActivityCode><ActivityDescription>HEATING EQUIPMENT, EXC ELECTRIC</ActivityDescription></MainActivity>'+
												'<CompanyStatus Code="Active">Active</CompanyStatus><LatestTurnoverFigure>2050</LatestTurnoverFigure><LatestShareholdersEquityFigure>4567</LatestShareholdersEquityFigure>'+
												'<CreditRating><CommonValue>D</CommonValue><CommonDescription>High Risk</CommonDescription><CreditLimit>4000</CreditLimit><ProviderValue>21</ProviderValue><ProviderDescription>High Risk</ProviderDescription></CreditRating>'+
											'</CompanySummary>'+
        									'<CompanyIdentification>'+
        										'<BasicInformation>'+
        											'<BusinessName>SUN EDISON LLC</BusinessName><RegisteredCompanyName>SUN EDISON LLC</RegisteredCompanyName><CompanyRegistrationNumber>201230110146</CompanyRegistrationNumber>'+
        											'<Country>US</Country><DateofCompanyRegistration>2012-10-22T00:00:00Z</DateofCompanyRegistration><DateofStartingOperations>2012-10-22T00:00:00Z</DateofStartingOperations><LegalForm>LlcOrLlp</LegalForm><CompanyStatus Code="Active">Active</CompanyStatus>'+
        											'<PrincipalActivity><ActivityCode>3433</ActivityCode><ActivityDescription>HEATING EQUIPMENT, EXC ELECTRIC</ActivityDescription></PrincipalActivity>'+
        											'<ContactAddress><SimpleValue >12500 BALTIMORE AVE, BELTSVILLE, MD, 20705</SimpleValue><Street >12500 BALTIMORE AVE</Street><HouseNumber>1</HouseNumber><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></ContactAddress><ContactTelephoneNumber>4439097200</ContactTelephoneNumber>'+
        										'</BasicInformation>'+
        										'<Activities><Activity><ActivityCode>3433</ActivityCode><ActivityDescription>HEATING EQUIPMENT, EXC ELECTRIC</ActivityDescription></Activity></Activities>'+
        										'<PreviousNames>'+
        											'<PreviousName><DateChanged>2012-10-22T00:00:00Z</DateChanged><Name> test </Name></PreviousName>'+
        										'</PreviousNames>'+
        									'</CompanyIdentification>'+
        									'<CreditScore>'+
        										'<CurrentCreditRating><CommonValue>D</CommonValue><CommonDescription>High Risk</CommonDescription><CreditLimit>4000</CreditLimit><ProviderValue>21</ProviderValue><ProviderDescription>High Risk</ProviderDescription></CurrentCreditRating><CurrentContractLimit> 1</CurrentContractLimit>'+
        										'<PreviousCreditRating><CommonValue>C</CommonValue><CommonDescription>Moderate Risk</CommonDescription><CreditLimit>13700</CreditLimit><ProviderValue>36</ProviderValue><ProviderDescription>Moderate Risk</ProviderDescription></PreviousCreditRating><DateOfLatestRatingChange>2016-06-10T00:00:00Z</DateOfLatestRatingChange>'+
        									'</CreditScore>'+
        									'<ContactInformation>'+
        										'<MainAddress><Address><SimpleValue >12500 BALTIMORE AVE, BELTSVILLE, MD, 20705</SimpleValue><Street >12500 BALTIMORE AVE</Street><HouseNumber>1</HouseNumber><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></Address><Country>US</Country><Telephone>4439097200</Telephone></MainAddress>'+
        										'<OtherAddresses>'+
        											'<OtherAddress>'+'<SimpleValue >12500 BALTIMORE AVE, BELTSVILLE, MD, 20705</SimpleValue><Street >12500 BALTIMORE AVE</Street><HouseNumber>1</HouseNumber><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province>'+'</OtherAddress>'+'<Telephone>4439097200</Telephone>' +
        										'</OtherAddresses>'+
        										'<Websites><Website>SUNEDISON.COM</Website></Websites>'+
        									'</ContactInformation>'+
        									'<ShareCapitalStructure>'+
        										'<IssuedShareCapital>1</IssuedShareCapital><ShareHolders><Shareholder> <Name> test</Name> <SharePercent> 100 </SharePercent></Shareholder></ShareHolders>'+
        										
        									'</ShareCapitalStructure>'+
        									'<Directors>'+
        										'<CurrentDirectors>'+
        											'<Director><Name>AIDAN FOLEY</Name><Address><SimpleValue >12500 BALTIMORE AVE BELTSVILLE MD 20705</SimpleValue><Street >12500 BALTIMORE AVE</Street><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></Address><Gender>0</Gender><DateOfBirth>2006-03-01T00:00:00</DateOfBirth><Position>PROGRAM MANAGER</Position></Director>'+
        										'</CurrentDirectors>'+
        									'</Directors>'+
        									'<OtherInformation>'+
        										'<Bankers>'+
        											'<Banker> <Name> Test</Name> </Banker>'+
        										'</Bankers>'+
        										'<Advisors>'+
        											'<Advisor> <AuditorName> Test</AuditorName> </Advisor>'+
        										'</Advisors>'+
        										'<EmployeesInformation><EmployeeInformation><Year>2016</Year><NumberOfEmployees>375</NumberOfEmployees></EmployeeInformation></EmployeesInformation>'+
        									'</OtherInformation>'+
        									'<GroupStructure> '+
        										'<UltimateParent Country="US" Id="US023/X/US20568026"><Name >SUNEDISON, INC</Name><Status>A</Status><RegistrationNumber>124</RegistrationNumber></UltimateParent>'+
        										'<ImmediateParent Country="US" Id="US023/X/US20568026"><Name >SUNEDISON, INC</Name><Status>A</Status><RegistrationNumber>124</RegistrationNumber></ImmediateParent>'+
        										'<AffiliatedCompanies>'+
													'<AffiliatedCompany>'+
														'<Name> Test</Name> <Status> A</Status> <RegistrationNumber>111</RegistrationNumber>'+
													'</AffiliatedCompany>'+
													'<AffiliatedCompany>'+
														'<Name> Test</Name> <Status> A</Status> <RegistrationNumber>111</RegistrationNumber>'+
													'</AffiliatedCompany>'+
												'</AffiliatedCompanies>'+
        									'</GroupStructure>'+
        									'<FinancialStatements>'+
												'<FinancialStatement>'+
													'<YearEndDate> Test</YearEndDate> <NumberOfWeeks> A</NumberOfWeeks> <Currency>111</Currency> <ConsolidatedAccounts>111</ConsolidatedAccounts>'+
													'<ProfitAndLoss>'+
														'<Revenue> A</Revenue> <OperatingCosts>111</OperatingCosts><OperatingProfit>111</OperatingProfit><WagesAndSalaries>111</WagesAndSalaries>'+
														'<PensionCosts> Test</PensionCosts><Depreciation> A</Depreciation><Amortisation>111</Amortisation>'+ '<FinancialExpenses>111</FinancialExpenses><ProfitBeforeTax>111</ProfitBeforeTax><Tax>111</Tax>'+
														'<ProfitAfterTax>111</ProfitAfterTax><Dividends>111</Dividends><MinorityInterests>111</MinorityInterests>'+
														'<OtherAppropriations>111</OtherAppropriations><RetainedProfit>111</RetainedProfit>'+
													'</ProfitAndLoss>'+
													'<BalanceSheet>'+
														'<TotalTangibleAssets> Test</TotalTangibleAssets><TotalIntangibleAssets> A</TotalIntangibleAssets><TotalOtherFixedAssets>111</TotalOtherFixedAssets>'+ '<TotalFixedAssets>111</TotalFixedAssets><TotalInventories> A</TotalInventories><TradeReceivables>111</TradeReceivables>'+
														'<MiscellaneousReceivables>111</MiscellaneousReceivables><TotalReceivables> A</TotalReceivables><Cash>111</Cash>'+
														'<OtherCurrentAssets>111</OtherCurrentAssets><TotalCurrentAssets> A</TotalCurrentAssets><TotalAssets>111</TotalAssets>'+
														'<TradePayables>111</TradePayables><BankLiabilities> A</BankLiabilities><OtherLoansOrFinance>111</OtherLoansOrFinance>'+
														'<MiscellaneousLiabilities>111</MiscellaneousLiabilities><TotalCurrentLiabilities> A</TotalCurrentLiabilities>'+
														'<BankLiabilitiesDueAfter1Year>111</BankLiabilitiesDueAfter1Year><OtherLoansOrFinanceDueAfter1Year> A</OtherLoansOrFinanceDueAfter1Year>'+
														'<MiscellaneousLiabilitiesDueAfter1Year>111</MiscellaneousLiabilitiesDueAfter1Year><TotalLongTermLiabilities> A</TotalLongTermLiabilities>'+
														'<TotalLiabilities>111</TotalLiabilities><CalledUpShareCapital> A</CalledUpShareCapital>'+
														'<RevenueReserves>111</RevenueReserves><OtherReserves> A</OtherReserves><TotalShareholdersEquity>111</TotalShareholdersEquity>'+
													'</BalanceSheet>'+
													'<OtherFinancials>'+
														'<ContingentLiabilities> Test</ContingentLiabilities><WorkingCapital> A</WorkingCapital><NetWorth>111</NetWorth>'+ 
													'</OtherFinancials>'+
													'<Ratios>'+
														'<PreTaxProfitMargin>T</PreTaxProfitMargin><ReturnOnCapitalEmployed>A</ReturnOnCapitalEmployed><ReturnOnTotalAssetsEmployed>111</ReturnOnTotalAssetsEmployed>'+ 
														'<ReturnOnNetAssetsEmployed>T</ReturnOnNetAssetsEmployed><SalesOrNetWorkingCapital>A</SalesOrNetWorkingCapital><StockTurnoverRatio>111</StockTurnoverRatio>'+
														'<DebtorDays>T</DebtorDays><CreditorDays>A</CreditorDays><CurrentRatio>111</CurrentRatio>'+
														'<LiquidityRatioOrAcidTest>T</LiquidityRatioOrAcidTest><CurrentDebtRatio>A</CurrentDebtRatio><Gearing>111</Gearing>'+
														'<EquityInPercentage>T</EquityInPercentage><TotalDebtRatio>A</TotalDebtRatio>'+
													'</Ratios>'+
												'</FinancialStatement>'+
											'</FinancialStatements>'+
        									'<AdditionalInformation>'+
        										'<PaymentData >'+
        											'<TradePaymentSummary><TotalTradesNumber>1</TotalTradesNumber><ContinuousTradesNumber>1</ContinuousTradesNumber></TradePaymentSummary>'+
        											'<TradeLinesInformation><Continuous><Number>1</Number><Balance>100</Balance><Current>100</Current><Range1to30>0</Range1to30><Range31to60>0</Range31to60><Range61to90>0</Range61to90><Range91plus>0</Range91plus></Continuous><Combined><Number>1</Number><Balance>100</Balance><Current>100</Current><Range1to30>0</Range1to30><Range31to60>0</Range31to60><Range61to90>0</Range61to90><Range91plus>0</Range91plus></Combined><Total><Number>1</Number><Balance>100</Balance><Current>100</Current><Range1to30>0</Range1to30><Range31to60>0</Range31to60><Range61to90>0</Range61to90><Range91plus>0</Range91plus></Total></TradeLinesInformation>'+
        											'<TotalNoofInvoicesAvailable>1</TotalNoofInvoicesAvailable><TotalNoofInvoicesPaidBefore30DaysDue></TotalNoofInvoicesPaidBefore30DaysDue><TotalNoofInvoicesPaidAfter30DaysDue>1</TotalNoofInvoicesPaidAfter30DaysDue>'+
													'<TotalNoofInvoicesOwingBefore30DaysDue>1</TotalNoofInvoicesOwingBefore30DaysDue><TotalNoofInvoicesOwingAfter30DaysDue>1</TotalNoofInvoicesOwingAfter30DaysDue>'+
													'<Dbt>1</Dbt><PaymentTrend>1</PaymentTrend><Balance>1</Balance><BalanceP1>1</BalanceP1><BalanceP2>1</BalanceP2><BalanceP3>1</BalanceP3>'+
													'<BalanceP4>1</BalanceP4><BalanceWithinTerms>1</BalanceWithinTerms>'+
													'<InvoicesBetween3And12monts>'+
														'<PaidP1>2</PaidP1><PaidP2>2</PaidP2><PaidP3>2</PaidP3><PaidP4>2</PaidP4><PaidInTerms> 2</PaidInTerms><TotalPaid> 2</TotalPaid>'+
													'</InvoicesBetween3And12monts>'+
													'<InvoicesLast3Monts>'+
														'<PaidP1>2</PaidP1><PaidP2>2</PaidP2><PaidP3>2</PaidP3><PaidP4>2</PaidP4><PaidInTerms> 2</PaidInTerms><TotalPaid> 2</TotalPaid>'+
													'</InvoicesLast3Monts>'+
													'<TotalTradeLines>2</TotalTradeLines><TotalTradeLinesOutstanding> 2</TotalTradeLinesOutstanding>'+
													'<AnnualDbtWithTrend>'+
														'<Entry>'+
															'<Month>1</Month><DBTValue>1</DBTValue><DBTTrend>1</DBTTrend>'+
														'</Entry>'+
													'</AnnualDbtWithTrend>'+
													'<LastSixMonthsBalance>'+
														'<Entry>'+
															'<Month>1</Month><Balance>1</Balance>'+
														'</Entry>'+
													'</LastSixMonthsBalance>'+
        										'</PaymentData>'+
        										'<Misc>'+
        											'<FirstReportedDate>2006-03-01T00:00:00</FirstReportedDate><PrimaryNAICSCode>333414</PrimaryNAICSCode><PrimaryNAICSCodeDescription>Heating Equipment (except Warm Air Furnaces) Manufacturing</PrimaryNAICSCodeDescription><YearsInBusiness>10 years</YearsInBusiness><AddressType>Street Address</AddressType><LocationType>Headquarters</LocationType><SalesRange>$1 to $499,000</SalesRange>'+
        										'</Misc>'+
        										'<PrimaryCorporateRecord >'+
        											'<BusinessLegalName>SUNE CREST 12, LLC</BusinessLegalName><SOSCharterNumber>201230110146</SOSCharterNumber><Status>A</Status><IncorporatedState>r</IncorporatedState><ForeignOrDomestic>Domestic</ForeignOrDomestic><ProfitOrNonProfit>Undetermined</ProfitOrNonProfit>'+
        											'<PrimaryAddress><Street >12500 BALTIMORE AVE</Street><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></PrimaryAddress>'+
        										'</PrimaryCorporateRecord>'+
        										'<OtherCorporateRecords >'+
        											'<CorporateRecord>'+
        												'<BusinessLegalName>SUNE CREST 5, LLC</BusinessLegalName><SOSCharterNumber>201230110102</SOSCharterNumber><Status>A</Status><ForeignOrDomestic>Domestic</ForeignOrDomestic><ProfitOrNonProfit>Undetermined</ProfitOrNonProfit>'+
        												'<PrimaryAddress><Street >12500 BALTIMORE AVE</Street><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></PrimaryAddress>'+
        												'<AgentName>test</AgentName><AgentAddress> <Street >12500 BALTIMORE AVE</Street><City >BELTSVILLE</City><PostalCode >20705</PostalCode><Province >MD</Province></AgentAddress>'+
        											'</CorporateRecord>'+
        										'</OtherCorporateRecords >'+
        										'<DBT ><CompanyDBT>0</CompanyDBT><IndustryDBT>5</IndustryDBT></DBT>'+
        										'<NegativeInformation >'+
        											'<LegalFilingSummary><Bankruptcy>false</Bankruptcy><TaxLienFilings>1</TaxLienFilings><JudgmentFilings>0</JudgmentFilings><Sum>161</Sum><UCCFilings>19</UCCFilings><CautionaryUCCFilings>1</CautionaryUCCFilings></LegalFilingSummary>'+
        											'<UCCDetails>'+
        												'<UCCDetail>'+
        													'<FiledDate>2014-10-13T00:00:00</FiledDate><FilingNumber>20144102836</FilingNumber><Jurisdiction>DE SECRETARY OF STATE</Jurisdiction><SecuredPartyName>SEMINOLE FUNDING RESOURCES LLC C/O SEMINOLE FIN- ANCIAL SERVICES</SecuredPartyName>'+
        													'<SecuredPartyAddress><Street >455 NORTH INDIAN ROCKS ROAD SUITE B</Street><City >BELLEAIR BLUFFS</City><PostalCode >33770</PostalCode><Province >MD</Province></SecuredPartyAddress>'+
        													'<Collateral>ACCOUNT(S) INCLUDING PROCEEDS AND PRODUCTS</Collateral>'+
        												'</UCCDetail>'+
    												'</UCCDetails>'+
    												'<TaxLienDetails>'+
    													'<TaxLienDetail>'+
    														'<FiledDate>2015-10-21T00:00:00</FiledDate><FilingType>STATE TAX WARRANT</FilingType><FiledBy>STATE OF WASHINGTON</FiledBy><Amount>161</Amount><FilingNumber>105225831</FilingNumber><Jurisdiction>KING SUPERIOR COURT - SEATTLE</Jurisdiction>'+
    													'</TaxLienDetail>'+
													'</TaxLienDetails>'+
													'<JudgmentDetails>'+
														'<JudgmentDetail>'+
    														'<FiledDate>2015-10-21T00:00:00</FiledDate><Plaintiff>STATE TAX WARRANT</Plaintiff><Status>STATE OF WASHINGTON</Status><Amount>161</Amount><FilingNumber>105225831</FilingNumber><Jurisdiction>KING SUPERIOR COURT - SEATTLE</Jurisdiction>'+
    													'</JudgmentDetail>'+
													'</JudgmentDetails>'+
        										'</NegativeInformation>'+
        										'<CompanyHistory>'+
													'<Event>'+
														'<Date>1</Date><Description>1</Description>'+
													'</Event>'+
												'</CompanyHistory>'+
												'<MortgageInformation>'+
													'<MortgageSummary>'+
														'<Outstanding>0</Outstanding><Satisfied>3</Satisfied>'+
													'</MortgageSummary>'+
													'<MortgageDetails>'+
														'<MortgageDetail>'+
															'<MortgageType>0</MortgageType><DateChargeCreated>3</DateChargeCreated>'+
															'<DateChargeRegistered>0</DateChargeRegistered><DateChargeSatisfied>3</DateChargeSatisfied><Status>0</Status>'+
															'<PersonsEntitled>3</PersonsEntitled><AmountSecured>3</AmountSecured><Details>0</Details>'+
														'</MortgageDetail>'+
													'</MortgageDetails>'+
												'</MortgageInformation>'+
												'<AdditionalFinancials>'+
												'<FinancialItems>'+
													'<YearEndDate>0</YearEndDate><Export>3</Export><CostOfSales>3</CostOfSales>'+
													'<DirectorsEmoluments>0</DirectorsEmoluments><RevaluationReserve>3</RevaluationReserve><GrossProfit>3</GrossProfit>'+
													'<ShortTermHPFinanceLeaseLiabilities>0</ShortTermHPFinanceLeaseLiabilities><LongTermHPFinanceLeaseLiabilities>3</LongTermHPFinanceLeaseLiabilities>'+
												'</FinancialItems>'+
											'</AdditionalFinancials>'+
											'<Commentaries>'+
												'<Commentary>'+
													'<CommentaryText>0</CommentaryText><PositiveOrNegative>3</PositiveOrNegative>'+
												'</Commentary>'+
											'</Commentaries>'+
											'<ExtendedGroupStructure>'+
												'<GroupStructure>'+
													'<CompanyInGroup Id="1" Country="us"><CompanyName>3</CompanyName><RegisteredNumber>3</RegisteredNumber>'+
													'<LatestAnnualAccounts>3</LatestAnnualAccounts><Level>0</Level><Status>3</Status></CompanyInGroup>'+
												'</GroupStructure>'+
											'</ExtendedGroupStructure>'+
        									'</AdditionalInformation>'+
        									'</Report>'+
										'</Reports>'+
									'</RetrieveCompanyOnlineReportResult>'+
								'</RetrieveCompanyOnlineReportResponse>'+
							'</Body>'+
						'</Envelope>';
        
        blob Attch = blob.valueof(XMLString);
        Attachment objAttch = new Attachment();
 		objAttch.Body = Attch;
		objAttch.Name = objCreditInfo.TitleDescription__c;
		objAttch.parentId = objCreditInfo.id ;
		Database.insert(objAttch);
		AttchId=objAttch.id;
         
        Test.setCurrentPageReference(new PageReference('Page.bSureC_CreditSafePDF')); 
		System.currentPageReference().getParameters().put('parentName', SuppName);
		System.currentPageReference().getParameters().put('attachName', AttchId);
		
		bSureS_CreditSafePDFController  objPDF = new bSureS_CreditSafePDFController();
		
      	system.test.stopTest();
        
    }
}