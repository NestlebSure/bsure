/*
 *Descritption: This class has been created for the .NET team to do a small POC for the Portal Project
 *              The class will be deleted after its use.
 * Created by: Sankar Krishnan  
 */

@RestResource(urlMapping='/SupplierInfo/*')
global class bSureS_SupplierInformation_TBD {
    
    @httpGet
    global static list<BSureS_Basic_Info__c> returnSupplierListRest(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
    	list<BsureS_Basic_Info__c> supplierlist=[select id, supplier_name__c, risk_level__c from
                                                BsureS_Basic_Info__c 
                                                 //where risk_level__c !=NULL
                                                 limit 500
                                                ];
       
        return supplierlist;   
    }
    
    
}