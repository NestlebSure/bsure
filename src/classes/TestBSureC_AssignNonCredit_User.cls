/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_AssignNonCredit_User {

    static testMethod void myUnitTest() 
    {
    	BSureC_Category__c scategory = new BSureC_Category__c();
        scategory.Name = 'Account123';
        insert scategory;
        
    	Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
		
		
		 BSureC_Customer_Basic_Info__c tstBSureCustomerBasicInfo=new BSureC_Customer_Basic_Info__c();
		 tstBSureCustomerBasicInfo.Customer_Name__c=standard.Name;
		 tstBSureCustomerBasicInfo.Customer_Category__c=scategory.Id;
		 tstBSureCustomerBasicInfo.IsActive__c=true;
		 insert tstBSureCustomerBasicInfo;
		 system.assertEquals(standard.Name, tstBSureCustomerBasicInfo.Customer_Name__c);
		 
		 
        
       BSureC_Assigned_Buyers__c tstBSureCAssignedBuyers=new BSureC_Assigned_Buyers__c();
	   tstBSureCAssignedBuyers.Customer_Id__c=tstBSureCustomerBasicInfo.Id;
	   tstBSureCAssignedBuyers.User_Id__c=standard.Id;//sales user
	   insert tstBSureCAssignedBuyers;
		
				   
    }
}