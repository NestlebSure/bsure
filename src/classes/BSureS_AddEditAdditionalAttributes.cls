/***********************************************************************************************
*       Controller Name : BSureS_AddEditAdditionalAttributes
*       Date                    : 2/04/2013 
*       Author                  : veereandranath.j
*       Purpose                 : based on profile selection to display the User Records.           
*       Change History          :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       2/04/2013                J.Veereandranath          Initial Version
*		8/8/2013				 B.Anupreethi				Code review changes
       
**************************************************************************************************/
global with sharing class BSureS_AddEditAdditionalAttributes {
	public string strUserName{get;set;}//To hold User name
	public string strUserSearch;
    public string strSelectedProfile{get;set;}
    public list<selectOption> lstProfiles{get;set;}
    public List<user> Users{get;set;}
    public string userView{get;set;}//to assign from param
    public list<Profile> lstProfileAll{get;set;}
    public set<string> strProfile{get;set;}
    public integer counter=0;  //keeps track of the offset
    public integer list_size=20; //sets the page size or number of rows
    public integer total_size{get;set;} //used to show user the total size of the list
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination                
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public string strMainQuery {get;set;}
    public BSureS_AddEditAdditionalAttributes()
    {
        strSelectedProfile='ALL';
        getlstProfiles();
        getUsersLst();
    }
    
    public void getlstProfiles(){
        strProfile=new set<string>();
        lstProfiles=new list<selectOption>();
        lstProfiles.add(new SelectOption('ALL','ALL'));
       	//Anu Check if the user has read access on the User & profile object
        if (Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Profile.isAccessible()){ 
        lstProfileAll= [Select Id,Name,(select Id,Name from Users)  from Profile  where Name Like : 'BSureS%' order By Name Asc];
        }
              
        map<Id,list<User>> mapUsers = new map<Id,list<User>>();
        for(Profile p:lstProfileAll){   
            mapUsers.put(p.Id,p.Users);
        }
        
        for(Profile p:lstProfileAll){   
            if(mapUsers.get(p.Id) != null && mapUsers.get(p.Id).size() > 0 ){
                string pname = p.Name.Replace('BSureS_','');
                lstProfiles.add(new SelectOption(p.Id,pname));
                strProfile.add(p.Id);
            }
        }
        
    }
 
    public PageReference getUsersLst()
    {
      //Anu Check if the user has read access on the Users field
      if(!Schema.sObjectType.User.isAccessible())
	  { 
	  	return null;
	  }
     strMainQuery='SELECT Id,Name,LastName,FirstName,phone,UserName,Email,Alias,UserRoleId,'+
                            ' UserRole.Name,Profile.Name,IsActive,EmailEncodingKey,TimeZoneSidKey,'+
                            ' LocaleSidKey,LanguageLocaleKey,CompanyName,Department,City,Country,State'+
                            ' FROM User';
     if(strUserName != null && strUserName!='')
     {
 		strUserSearch = strUserName + '%';            
     	strMainQuery+=' WHERE Name  like : strUserSearch ';  
     	
     	if(strSelectedProfile !='ALL' && strSelectedProfile != '')
     	{
        	strMainQuery= strMainQuery + '  AND ProfileId =:strSelectedProfile AND IsActive=true  order by FirstName';
     	}
     	else
     	{
        	strMainQuery= strMainQuery + '  AND ProfileId IN :strProfile  AND IsActive=true  order by FirstName';
     	}                      
     }
     else
     {
     	
     	if(strSelectedProfile !='ALL' && strSelectedProfile != '')
     	{
        	strMainQuery= strMainQuery + '  WHERE ProfileId =:strSelectedProfile AND IsActive=true  order by FirstName';
     	}
     	else
     	{
        	strMainQuery= strMainQuery + '  WHERE ProfileId IN :strProfile  AND IsActive=true  order by FirstName';
     	}
     }                       
     
	  
     users=Database.query(strMainQuery); 
     total_size = users.size();
    if(users.size()>0 ){
        pagenation();
    }else   
    {
          ApexPages.Message myEMsg=new ApexPages.Message(ApexPages.severity.INFO,System.label.BSureS_No_Records_Found);
          ApexPages.addMessage(myEMsg);
         
    }
    return null;
    
    }
 
    public void pagenation( ){
        PrevPageNumber=counter+1;
        NxtPageNumber=counter+users.size();
        string strQuery;
        strQuery = strMainQuery+' limit :list_size  offset :counter';
        users = Database.query(strQuery);     
        
    }
    public pageReference userViewPage() {
        pagereference pageView = page.BSureS_AdditionalAttributes;
        pageView.getParameters().put('Id',userView);
        pageView.setRedirect(true);
        return pageView; 
    }
       
    public void FirstbtnClick() { //user clicked beginning
      counter = 0;
      pagenation();
    }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public void previousBtnClick() { //user clicked previous button
      counter -= list_size;
      pagenation();
    }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public void nextBtnClick() { //user clicked next button
      counter += list_size;
      pagenation();
    }
      
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public void LastbtnClick() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
      pagenation();
    }  
       
   /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getPreviousButtonEnabled() {
          //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }   
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   }
   public Integer getPageSize() {
      return total_size;
   }
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }   
      /// <summary>
      /// Below method gets the total no.of pages.
      /// <param name="TotalPageNumber"></param>
      /// <returns>Integer</returns>    
      /// </summary>
    public Integer getTotalPageNumber() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
    } 
    public pagereference Cancel(){
        return page.BSureS_Reports;
    }
}