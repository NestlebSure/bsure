global class bSureS_GenerateExperianReport implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts{
	public String query;
   	global Database.QueryLocator start(Database.BatchableContext BC){
		query = 'Select id,Name,Supplier_Name__c,BISFileNumber__c,Globe_ID__c,Planned_Review_Date__c From BSureS_Basic_Info__c '+
				'where BISFileNumber__c!=\'\' and BISFileNumber__c!=null '+
				'and Globe_ID__c!=\'\' and Globe_ID__c!=null and Country_Name__c=\'United States\' '+
				'and (Owner_Ship__c=\'Private\' or Owner_Ship__c=\'Private (Public Debt)\' or Owner_Ship__c=\'Public\')';
      	return Database.getQueryLocator(query);
   	} 
	map<string,string> lstUnSuccessSupplier = new map<string,string>();
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
		for(Sobject s : scope){
			date dNow = system.today();
			date planRewDate = Date.valueOf(s.get('Planned_Review_Date__c'));
			if(planRewDate!=null && planRewDate > dNow && dNow.daysBetween(planRewDate) == 7){
				system.debug('***'+dNow.daysBetween(planRewDate));
				string suppId =  String.valueOf(s.get('Id'));
				string BIN =  String.valueOf(s.get('BISFileNumber__c'));
				string globeId =  String.valueOf(s.get('Globe_ID__c'));
	      		string strExpRes = bSureS_ExperianRequest.generateExperianRpt(suppId,BIN,globeId);
	      		system.debug('strExpRes===='+strExpRes);
	      		if(strExpRes==null || strExpRes=='' || !strExpRes.contains(Label.BSureS_ExpSuccess)){
	  				string suppName = string.valueOf(s.get('Supplier_Name__c'));
      				string strId = string.valueOf(s.get('Id'));
      				string[] arrMsg = new String[] {};  
      				if(strExpRes.contains(',')){
      					arrMsg = strExpRes.split(',');
      				}else{
      					arrMsg.add(strExpRes);
      				}
	  				lstUnSuccessSupplier.put(strId,suppName+' --> '+arrMsg[0]+';');
	  			}
			}
      	}      
	}
	
	global void finish(Database.BatchableContext BC){
		string batchMail = '';
   		string rptFailMail = '';
   		if(BSureS_CommonUtil.getConfigurationValues('ExperianBatchFailMail')!=null & BSureS_CommonUtil.getConfigurationValues('ExperianBatchFailMail').size()>0){
   			batchMail = BSureS_CommonUtil.getConfigurationValues('ExperianBatchFailMail').get(0);
   		}
   		if(BSureS_CommonUtil.getConfigurationValues('ExperianFailMail')!=null & BSureS_CommonUtil.getConfigurationValues('ExperianFailMail').size()>0){
   			rptFailMail = BSureS_CommonUtil.getConfigurationValues('ExperianFailMail').get(0);
   		}

 		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus  
  							From AsyncApexJob Where Id = :BC.getJobId()];  
   		if(batchMail != '' && batchMail != null && a.NumberOfErrors !=null && a.NumberOfErrors != 0){
	 		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
	 		String[] toAddresses = new String[] {};  
	 		if(batchMail.contains(',')){
	 			toAddresses = batchMail.split(',');
	 		}else{
	 			toAddresses.add(batchMail);
	 		}
	 		mail.setToAddresses(toAddresses);  
	 		mail.setSubject('BatchJobXYZ Status: ' + a.Status);  
	 		mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +  
	  		' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);  
	    
	 		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
   		}
 		if(lstUnSuccessSupplier!=null && lstUnSuccessSupplier.size()>0 && rptFailMail!='' && rptFailMail!=null){
 			Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();  
	 		String[] toAddresses1 = new String[] {};  
	 		if(rptFailMail.contains(',')){
	 			toAddresses1 = rptFailMail.split(',');
	 		}else{
	 			toAddresses1.add(rptFailMail);
	 		}
	 		mail1.setToAddresses(toAddresses1);  
	 		mail1.setSubject(Label.BSureS_ExpFail);  
	 		
	 		String strEmailBody =''; 
	        strEmailBody += '<table>';
	        strEmailBody += Label.BSureS_ExpFailSupp;
	        strEmailBody += '<tr></tr>';
	        strEmailBody += '</table>';
	        strEmailBody += '<table cellspacing="2" cellpadding="0" border="1" bordercolor="#9AFEFF" >';
	        strEmailBody += '<tr><td height="30px"><font name="Verdana" ><center> <b>Supplier Name</b></center></font></td>';
	        strEmailBody += '<td height="30px"><font name="Verdana"><center><b>Error Code</b> </center></font></td>';
	        strEmailBody += '<td height="30px"><font name="Verdana"><center><b>Reason</b></center></font></td></tr>';
	        for(string str : lstUnSuccessSupplier.keyset()){
	        	string[] strSuppName = lstUnSuccessSupplier.get(str).split('-->');
	        	strEmailBody += '<tr><td height="21px"><font name="Verdana" ><center>'+ strSuppName[0] +'</center></font></td>';
	        	if(strSuppName[1].contains(Label.BSureS_ExpSpecialChar)){
	        		string[] strCodeReason = strSuppName[1].split(Label.BSureS_ExpSpecialChar);
	        		if(strCodeReason[0].contains(':')){
	        			string[] strCode = strCodeReason[0].split(':');
	        			strEmailBody += '<td height="21px">'+strCode[1]+'</td>';
	        		}
	        		if(strCodeReason[1].contains(';')){
	        			strEmailBody += '<td height="21px">'+strCodeReason[1].replace(';','')+'</td>';
	        		}else{
	        			strEmailBody += '<td height="21px">'+strCodeReason[1]+'</td>';
	        		}
	        	}else{
	        		strEmailBody += '<td height="21px">'+ +'</td>';
		        	if(strSuppName[1].contains(';') && strSuppName[1].contains(Label.BSureS_ExpErrorCode)){
	        			string strReason = strSuppName[1].replace(';','');
	        			strReason = strReason.replace(Label.BSureS_ExpErrorCode,'');
	        			strEmailBody += '<td height="21px">'+strReason+'</td>';
	        		}else if(strSuppName[1].contains(';')){
	        			strEmailBody += '<td height="21px">'+strSuppName[1].replace(';','')+'</td>';
	        		}else{
	        			strEmailBody += '<td height="21px">'+strSuppName[1]+'</td>';
	        		}
	        	}
	        }
	        strEmailBody += '</tr></table>';        
	                
	 		mail1.setHtmlBody(strEmailBody);  
	 		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });  
 		}
   	}
}