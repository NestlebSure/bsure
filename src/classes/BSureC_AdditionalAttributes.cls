/***********************************************************************************************
* Controller Name : BSureC_AdditionalAttributes
* Date : 1st Nov,2012
* Author : Neha Jaiswal
* Purpose : To Save Additional Attributes for User on Customer Application of Nestle.
* -------------------- ------------------- -------------------------
*/

global with sharing class BSureC_AdditionalAttributes {
                     
    
    public Id userId{get;set;}//to pass userId from VFpage
    public User user{get;set;}//to display user details on page  
    public boolean visibilityEdit{get;set;}
    public boolean visibilitySingle{get;set;}
    public string strZoneId {get;set;}//to pass selected Zone from VFPage
    public list<selectOption> lstZoneOptions{get;set;}//Zone options available to select
    public string strSubZoneId {get;set;}//to pass selected subzone from VFpage
    
    public string strprofile {get;set;}
    public list<selectOption> lstSubZoneOptions{get;set;}//options for subzones available
    public string strCountryId {get;set;}//to pass country selected from VFpage
    public list<SelectOption> lstCoutryOptions{get;set;}//to get the list for country
    public string strManagerId{get;set;}//to pass manager selected from VFPage
    public list<SelectOption> lstManagers{get;set;}//options available for list of user records
    public string strRegionId{get;set;}//to pass the regionId from VFPage
    public list<selectOption> lstRegionOptions{get;set;}//to get the ist of regions from BSureC_Region__c object
    public Map<string,BSureC_Category__c> mapBSureCategory  = new map<string,BSureC_Category__c>();
    public Map<id,string> mapBSureRegion = new Map<id,string> ();    
    public BSureC_AdditionalAttributes__c bSureUserAttributes{get;set;}   //to store category id and record
    public List<string> leftselected{get;set;} //  Category values 
    public List<string> rightselected{get;set;}//  Category values 
    public Set<string> leftvalues = new Set<string>(); //list of leftvalues for unselected()
    public Set<string> rightvalues = new Set<string>();//list of rightvalues for selected ()
    public  string rightCategories{get;set;}
    public Boolean view_flag{get;set;}
    public Boolean viewEditButton{get;set;}
     public Boolean view_Edit_flag{get;set;}
    public String strGetRegion ;
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    public string strLocationFlagForAnalyst{get;set;}
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    
    //constructor
    public BSureC_AdditionalAttributes()
     {    
        strLocationFlagForAnalyst = BSureC_CommonUtil.getConfigurationValues('BSureC_LocationFlagForAnalyst').get(0);
          list<BSureC_Category__c> lstCategories = new list<BSureC_Category__c>();  
          list<BSureC_Category__c> lstSelectedCategories = new list<BSureC_Category__c>();
          Map<string,string> mapCategory = new map<string,string>();
          list<Id> lstCategoriesIds = new list<Id>();
          bSureUserAttributes = new BSureC_AdditionalAttributes__c();
          lstZoneOptions = new list<selectOption>();  
          lstSubZoneOptions = new list<selectOption>();
          lstCoutryOptions = new list<selectOption>();
          lstManagers=new list<selectOption>();
          leftselected = new List<string>();
          rightselected = new List<string>();
          visibilityEdit=false;
          visibilitySingle=true;
          view_Edit_flag = true;
          userId  = apexpages.currentpage().getParameters().get('Id');
           //add lines for add/edit additional attribute 
          if(apexpages.currentpage().getParameters().get('viewflag') != null)
          {
            view_flag = Boolean.valueOf(apexpages.currentpage().getParameters().get('viewflag'));
          }
            //add lines for customer detail link in report
          if(apexpages.currentpage().getParameters().get('viewEdit') != null)
          {
            view_Edit_flag = Boolean.valueOf(apexpages.currentpage().getParameters().get('viewEdit'));
          }
          if(userId!=null)
        {
          user =  [SELECT Id,Name,LastName,FirstName,phone,UserName,Email,Alias,UserRoleId,
                            UserRole.Name,/*Profile.Name,*/IsActive,EmailEncodingKey,TimeZoneSidKey,
                            LocaleSidKey,LanguageLocaleKey,CompanyName,Department,City,Country,State,
                            (Select Id, CategoryID__c, User__c, CategoryName__c From BSureC_UserCategoryMapping__r),
                            (SELECT Zone_Name__c, User__c, SystemModstamp,Sub_Zone_Name__c,Id,Name,Country_Name__c,
                            BSureC_Zone__c,BSureC_Sub_Zone__c,BSureC_Country__c,Manager__c,BSureC_Region__c,Region__c                            
                            FROM BSureC_AdditionalAttributes__r),
                            PostalCode,Street,Fax,UserType/*,User.Profile.UserLicense.Name,User.Profile.UserLicenseId  */                          
                            FROM User 
                            WHERE Id =:userId];
        }
        
                           
         
         // strprofile=user.Profile.Name;
          getZones();  
          getManagers();
          getRegions();
          strGetRegion= BSureC_CommonUtil.getConfigurationValues('BSureC_IncludeRegionforManagerSelection').get(0);
          //system.debug('strGetRegion......'+strGetRegion);
          if(user.BSureC_AdditionalAttributes__r != null && user.BSureC_AdditionalAttributes__r.size() > 0 )
              {
                 bSureUserAttributes = user.BSureC_AdditionalAttributes__r[0];
                 if(bSureUserAttributes.Region__c == '' ||  bSureUserAttributes.Region__c == null )
                    bSureUserAttributes.Region__c = 'Select'; 
                 //blnshowEdit = true;
              }
          else{
            visibilityEdit = true;
            visibilitySingle = false;
          }
          if(user.BSureC_UserCategoryMapping__r!=null && user.BSureC_UserCategoryMapping__r.size()>0){
                for(BSureC_UserCategoryMapping__c objUCategory:user.BSureC_UserCategoryMapping__r)
                    {
                        rightvalues.add(objUCategory.CategoryName__c);
                        mapCategory.put(objUCategory.CategoryID__c,objUCategory.CategoryName__c);
                        if(rightCategories != null)
                            rightCategories += objUCategory.CategoryName__c + ';'+'  ';
                        else
                            rightCategories = objUCategory.CategoryName__c + ';'+'  ';
                    }
          }
          if(rightCategories!=null){
            rightCategories=rightCategories.substring(0,rightCategories.length()-3);
            
          }
          
          lstCategories = [select Id,Category_ID__c,Name  from BSureC_Category__c where Name!=null];
             for(BSureC_Category__c category:lstCategories)
                 {
                    mapBSureCategory.put(category.Name,category);
                    if(mapCategory.get(category.Id) == null)
                        {
                            leftvalues.add(category.Name);
                        }
                 }
        
             }
      
   /// <summary>
   /// displaydata(): to display the non-editable additional attributes of user.
   /// </summary>
   /// <returns> records for read only  
    
    public PageReference displaydata()
       {
            visibilityEdit=false;
            visibilitySingle=true;
          
            return null; 
       }
    
    
  /// <summary>
  /// getZones: to get the list of zone from BSureC_Zone__c.
  /// </summary>
  /// <returns>get the list of zones </returns>
    public void getZones()
     {
          lstZoneOptions.add(new selectOption('Select','Select'));
          lstSubZoneOptions.add(new selectOption('Select','Select'));
          lstCoutryOptions.add(new selectOption('Select','Select'));
          list<BSureC_Zone__c> lstZones= new list<BSureC_Zone__c>();
          if(user.BSureC_AdditionalAttributes__r !=null  && user.BSureC_AdditionalAttributes__r.size() > 0 )
                {   
                    strzoneId = user.BSureC_AdditionalAttributes__r[0].BSureC_Zone__c;
                    strSubZoneId = user.BSureC_AdditionalAttributes__r[0].BSureC_Sub_Zone__c;
                    strCountryId = user.BSureC_AdditionalAttributes__r[0].BSureC_Country__c;
                    strManagerId = user.BSureC_AdditionalAttributes__r[0].Manager__c;
                    strRegionId = user.BSureC_AdditionalAttributes__r[0].BSureC_Region__c;
                }      
        lstZoneOptions = BSureC_CommonUtil.getZones();
        lstZoneOptions.sort();
        getSubZones();
        getCountries();
        getManagers();
       }
       
       
   /// <summary>
   /// getSubZones: get the list of subzones from BSureC_Sub_Zone__c
   /// </summary>
   /// <returns>get the list of subzones</returns>
    public void getSubZones()
     {   
         getCountries();
         lstSubZoneOptions = new list<selectOption>();
         lstSubZoneOptions.add(new selectOption('Select','Select'));
         lstCoutryOptions = new list<selectOption>();
         lstCoutryOptions.add(new selectOption('Select','Select'));
         lstSubZoneOptions  = BSureC_CommonUtil.getSubZones(strzoneId);
         lstSubZoneOptions.sort();
       
    } 
    
    
  /// <summary>
  /// getCountries: egt the list of countries from BSureC_Country__c
  /// </summary>
  /// <returns>get the list of countries</returns>
    public void getCountries()
     { 
        lstCoutryOptions = new list<selectOption>();
        lstCoutryOptions.add(new selectOption('Select','Select'));
       
          if(strZoneId == 'select' || strSubZoneId == 'select')
          {
            strSubZoneId = '';            
          }   
        lstCoutryOptions = BSureC_CommonUtil.getCountries(strSubZoneId);  
        lstCoutryOptions.sort();   
      
     }
    
  
    
  /// <summary>
  /// getManagers: According to tthe profile hierarchy
  /// </summary>
  /// <returns>get the list of users</returns>
  /*  public void getManagers()
     {
        list<user> lstUser  = new list<User>(); 
        
        lstmanagers = new list<selectOption>();     
        lstmanagers.add(new selectOption('Select','Select'));    
        set<string> lstProfiles  = new set<string>();
       
        if(strprofile== 'BSureC_Customer Service' || strprofile== 'BSureC_Collector' || strprofile== 'BSureC_Un-authorized Collector' || strprofile=='BSureC_Sales' || strprofile=='BSureC_Un-earned Collector' || strprofile=='BSureC_On Going Collector'){
                    lstProfiles.add('BSureC_Global Manager');
                    lstProfiles.add('BSureC_Zone Manager');
                    lstProfiles.add('BSureC_Sub Zone Manager');
                    lstProfiles.add('BSureC_Country Manager');
                    lstProfiles.add('BSureC_Analyst');
        }else if(strprofile=='BSureC_Analyst'){
         lstProfiles.add('BSureC_Global Manager');
                    lstProfiles.add('BSureC_Zone Manager');
                    lstProfiles.add('BSureC_Sub Zone Manager');
                    lstProfiles.add('BSureC_Country Manager');
                    lstProfiles.add('BSureC_Analyst');
        }else if(strprofile== 'BSureC_Country Manager') {
                    lstProfiles.add('BSureC_Global Manager');
                    lstProfiles.add('BSureC_Zone Manager');
                    lstProfiles.add('BSureC_Sub Zone Manager');
                    lstProfiles.add('BSureC_Country Manager');
        }else if(strprofile=='BSureC_Sub Zone Manager'){
                    lstProfiles.add('BSureC_Global Manager');
                    lstProfiles.add('BSureC_Zone Manager');
                    lstProfiles.add('BSureC_Sub Zone Manager');
        }else if(strprofile=='BSureC_Zone Manager' ){
                    lstProfiles.add('BSureC_Global Manager');
                    lstProfiles.add('BSureC_Zone Manager');
        }else if( strprofile=='BSureC_Global Manager'){
                    lstProfiles.add('BSureC_Global Manager');
        }
        list<string> lstQueryProfiles = new list<string>();
        lstUser = [select Id,Name  from User where IsActive=True and Profile.Name in:lstProfiles  ORDER BY Name ASC];    
                        
        for(User u:lstUser)
        {
            lstmanagers.add(new selectOption(U.Id,U.Name));
        }
       }*/
       
     public void getmanagersonchangeregion()
     {
        strManagerId = ''; 
        getManagers();
     }  
     
     public void getManagers()
     {
        if(strRegionId=='Select'|| strCountryId=='Select')
        {
            strManagerId = '';            
        }   
        list<user> lstuser=new list<user>();
        lstmanagers = new list<selectOption>();        
        lstmanagers.add(new selectOption('Select','Select'));
        set<string> setRoleIds = new set<string>();                
        map<Id,UserRole> mapParentRoleId = new  map<Id,UserRole>([select Id,ParentRoleId from UserRole limit 100]);
           
        string ParentRoleId = mapParentRoleId.get(user.UserRoleId).ParentRoleId; 
              
        for(integer i=0;I<mapParentRoleId.size();I++){
            if(ParentRoleId != null ){
                setRoleIds.add(ParentRoleId);
                setRoleIds.add(mapParentRoleId.get(ParentRoleId).ParentRoleId);
                ParentRoleId = mapParentRoleId.get(ParentRoleId).ParentRoleId;
                   
            }
        }        
        lstUser = getRoleUsers(setRoleIds);
   
        for(User u:lstUser)  
        {
            lstmanagers.add(new selectOption(U.Id,U.Name));
        }      
        
     }  
 
     
      public list<User> getRoleUsers(set<string> pRoleIds)
      {
        list<User>  lstUsers     = new list<User>();
        list<User>  CEOUsers     = new list<User>();
        list<User>  zoneUsers    = new list<User>();
        list<User>  subzoneUsers = new list<User>();
        list<User>  countryUsers = new list<User>();
        list<User>  regionUsers =  new list<User>();
        set<Id>     UserIds      = new set<Id>();
        list<BSureC_AdditionalAttributes__c> Attributes = new list<BSureC_AdditionalAttributes__c>();
        list<BSureC_AdditionalAttributes__c> ALLAttributes = new list<BSureC_AdditionalAttributes__c>();
        strGetRegion= BSureC_CommonUtil.getConfigurationValues('BSureC_IncludeRegionforManagerSelection').get(0);
        //system.debug('strGetRegion......'+strGetRegion);
        list<UserRole> lstRoles = [SELECT Id,Name,(SELECT Id,Name,Profile.Name FROM Users WHERE IsActive = true) FROM UserRole WHERE Id in:pRoleIds ];
        
        for(UserRole ur:lstRoles){
            if(Ur.Name == 'Zone Manager')
                zoneUsers.addALL(ur.Users);
            else if(Ur.Name == 'Sub-Zone Manager')
                subzoneUsers.addALL(ur.Users);
            else if(Ur.Name == 'Country Manager')
                countryUsers.addALL(ur.Users);
            else if(Ur.Name == 'Region Manager')
                regionUsers.addALL(ur.Users);
            else if(Ur.Name != 'CEO')
                lstUsers.addALL(ur.Users);
            else if(Ur.Name == 'CEO')
                CEOUsers.addALL(ur.Users);
        }
       
        string zQuery = 'select Id,Name,BSureC_Zone__c,User__c,BSureC_Sub_Zone__c,BSureC_Country__c from BSureC_AdditionalAttributes__c where Id != null ';
     
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO'){
            set<Id> uIds = getUserIds(zoneUsers);
            string znQuery = '';
            znQuery = zQuery + ' AND User__c in:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Location is Required
            {
                znQuery += ' AND BSureC_Zone__c=:strZoneId ';
            }
            Attributes = database.query(znQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager'){
            set<Id> uIds = getUserIds(subzoneUsers);
            string szQuery = zQuery + ' AND User__c in:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Location is Required
            {
                szQuery += ' AND BSureC_Sub_Zone__c=:strSubZoneId ';
            }
            Attributes = database.query(szQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager' && user.UserRole.Name != 'Country Manager'){
            set<Id> uIds = getUserIds(countryUsers);
            string cQuery = zQuery + ' AND User__c in:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Location is Required
            {
                cQuery += ' AND BSureC_Country__c=:strCountryId ';
            }
            Attributes = database.query(cQuery);
            ALLAttributes.addALL(Attributes);
        }
      
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager' && 
           user.UserRole.Name != 'Country Manager' && user.UserRole.Name != 'Region Manager')
         {
            set<Id> uIds = getUserIds(regionUsers);
            string cRQuery = zQuery + ' AND User__c in:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Location is Required
                {
                cRQuery += ' AND BSureC_Region__c=:strRegionId ';
                }
            Attributes = database.query(cRQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager' && 
           user.UserRole.Name != 'Country Manager' && user.UserRole.Name != 'Region Manager')
        {
            set<Id> uIds = getUserIds(lstUsers);
            string cAllQuery = zQuery + ' AND User__c in:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Location is Required
             {
                cAllQuery += ' AND BSureC_Zone__c=:strZoneId AND BSureC_Sub_Zone__c=:strSubZoneId AND BSureC_Country__c=:strCountryId AND BSureC_Region__c=:strRegionId ';
             }
            Attributes = database.query(cAllQuery);
            ALLAttributes.addALL(Attributes);
        }                  
        
        UserIds.clear();
        lstUsers.clear();
        for(BSureC_AdditionalAttributes__c a:ALLAttributes)         
        {
            UserIds.add(a.User__c);
        }             

        lstUsers =[select Id,Name,Profile.Name from User where Id In:UserIds and Profile.Name Like:'%BSureC%'];
        lstUsers.addALL(CEOUsers);      
                 
        return lstUsers;        
    } 
    
    public set<Id> getUserIds(list<User> lstUsers){
        set<Id> UserIds = new set<Id>(); 
         for(User u:lstUsers){ 
            UserIds.add(u.Id);
        }
        return UserIds;
    }
    
     //Method Name : selectclick
     // Description : selected values removing from the list
    public void selectclick(){
        rightselected.clear();
        for(String strLeft: leftselected){
            leftvalues.remove(strLeft);
            rightvalues.add(strLeft);
        }
    }
    
    //    Method Name : unselectclick
    //    Description : selected values removing from the list
    public void unselectclick(){
        leftselected.clear();
        for(String strRight : rightselected){  
            rightvalues.remove(strRight);
            leftvalues.add(strRight);
        }
    }
    
   
    // Method Name : getunSelectedValues
    // Description : preparing the selectOptions for Unselcted values  
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> CategoriesOptions = new List<SelectOption>();
        List<string> lstSelectedCategories = new List<String>();
        lstSelectedCategories.addAll(leftvalues);
        lstSelectedCategories.sort();
        
        for(string sltCategory : lstSelectedCategories)
        {
        if(sltCategory != null && sltCategory != '')
          {
            CategoriesOptions.add(new SelectOption(sltCategory,sltCategory));
          }
        }
        return CategoriesOptions;
    }
    
    
     //   Method Name : getunSelectedValues
     //   Description : preparing the selectOptions for selcted values    
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> CategoriesOptions1 = new List<SelectOption>();
        List<string> lstSelectedCategories = new List<String>();
        lstSelectedCategories.addAll(rightvalues);
        lstSelectedCategories.sort();
        for(String sltCategory : lstSelectedCategories)
        {
        if(sltCategory != null && sltCategory != '')
         {
           CategoriesOptions1.add(new SelectOption(sltCategory,sltCategory));  
         }
        }
        return CategoriesOptions1;
    }
    
     
     /// <summary>
    /// getRegions: list of regions from BSureC_Region__c
    /// </summary>
    /// <returns>get the list of regions</returns>
     public void getRegions()
      { 
        
         lstRegionOptions=new list<selectOption>();
         list<BSureC_Region__c> listregion=new list<BSureC_Region__c>();
         lstRegionOptions.add(new selectOption('Select','Select'));
         listregion=[select id,Name from BSureC_Region__c where id!=null order by Name ASC];
         //system.debug('&*&*&$$$$$'+listregion);
         if(listregion!=null)
         {
          for(BSureC_Region__c bs:listregion)
                 {
                    lstRegionOptions.add(new selectoption(bs.id,bs.Name));
                    mapBSureRegion.put(bs.id,bs.Name);
                     //system.debug('&*&*&*&*'+lstRegionOptions);
                 }  
         }
       }
       
       
    /// <summary>
    /// Save
    
    /// </summary>
    /// <returns>save the user details</returns>
     public pagereference Save()
     {  
        list<BSureC_UserCategoryMapping__c>  lstUserCategories = new list<BSureC_UserCategoryMapping__c> ();
        User objUser = new User (Id=userId);
        
        if(strZoneId != null && strZoneId != 'Select' && strSubZoneId !=null && strSubZoneId !='Select'&& strCountryId != null &&strCountryId!='Select' && strManagerId != null && strManagerId!='Select' && strRegionId!= null)
         {   
                
                //system.debug('-----bSureUserAttributes----'+bSureUserAttributes);
                bSureUserAttributes.BSureC_Zone__c = strZoneId;
                bSureUserAttributes.BSureC_Sub_Zone__c= strSubZoneId;
                bSureUserAttributes.BSureC_Country__c = strCountryId;
                bSureUserAttributes.User__c = userId;  
                bSureUserAttributes.Manager__c = strManagerId;               
                if(strRegionId != null && strRegionId != 'Select' && mapBSureRegion.get(strRegionId) != null )
                {
                    bSureUserAttributes.BSureC_Region__c=strRegionId;
                    bSureUserAttributes.Region__c= mapBSureRegion.get(strRegionId);
                }
                else
                {
                    bSureUserAttributes.BSureC_Region__c = null;
                    bSureUserAttributes.Region__c = null;
                }
                Upsert bSureUserAttributes;
               
                if(objUser!= null)
                {
                     list<BSureC_UserCategoryMapping__c> lstCategories = [select Id,Name from BSureC_UserCategoryMapping__c where User__c=:objUser.Id];
                    if(lstCategories.size() > 0)
                        Delete lstCategories;
                }
                //system.debug('rightvalues...........'+rightvalues);
                list<BSureC_Category__c> lstBSureCategories =[select Id,Name from  BSureC_Category__c where Name IN:rightvalues];  
                for(BSureC_Category__c objC:lstBSureCategories)
                {  
                    BSureC_UserCategoryMapping__c objUserCat = new BSureC_UserCategoryMapping__c();
                    objUserCat.CategoryID__c = objC.Id;
                    objUserCat.User__c =  objUser.Id;
                    lstUserCategories.add(objUserCat);
                }
                if(lstUserCategories.size() > 0 )
                    insert lstUserCategories;
        }           
                
             pagereference pageView = new pagereference('/apex/BSureC_AdditionalAttribute?Id='+userId); 
             pageView.setRedirect(true);     
             return pageView; 
         
    }      

    /// <summary>
    /// Cancel
    /// </summary>
    /// <returns>redirect to user record page</returns>
    public Pagereference Cancel()
        {  //add lines for add/edit additional attribute     
            pagereference pageref;
            if(view_flag!=null && view_flag == true)
            {
                pageref = new pagereference('/apex/BSureC_AddEditAdditionalAttributes'); 
            }
            else if(view_Edit_flag!=null && view_Edit_flag == false)
            {
                pageref = new pagereference('/apex/BSureC_Reports'); 
            }
            else
            {
                pageref = new pagereference('/'+Schema.SObjectType.User.getKeyPrefix()); 
            }
            
             return pageref;  
        }
        
      /// <summary>
    /// Edit
    /// </summary>
    /// <returns>redirect to user to edit record page</returns>   
    public void Edit()
        {
            visibilityEdit = true; 
            visibilitySingle= false;
            
        }
      
      
}