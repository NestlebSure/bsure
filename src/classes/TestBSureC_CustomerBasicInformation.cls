/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * 
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureC_CustomerBasicInformation {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA12',IsActive__c=true);
        insert ZRef;
        system.Assertequals('AOA12',ZRef.name);
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America12',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America12',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States12',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba12',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state12',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef;
        
        //BSureC_Country_Manager__c Cmngr = new BSureC_Country_Manager__c(Country_Manager__c='005G0000001tDsp',Country__c=CRef.id);
        //insert Cmngr;
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole1';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager1';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager1';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager1';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
         
        insert lstSettings;
        
        BSureC_Customer_Basic_Info__c BPRef=new BSureC_Customer_Basic_Info__c();
                //BPRef.Customer_Name__c='Tset';
                //BPRef.Customer_Category__c=CatRef.id;
                //BPRef.Customer_Region__c=RegRef.id;
                //BPRef.Customer_Group_Id__c='Test group';
                BPRef.Bill_to_Account__c= 'Test bill';
                //BPRef.Credit_Account__c='Test Credit Acc';
                BPRef.Owner_Ship__c='Public';
                BPRef.Has_Parent__c=false;
                insert BPRef;
                
                system.assertEquals('Public', BPRef.Owner_Ship__c);
                
        BSureC_Customer_Basic_Info__c BRef=new BSureC_Customer_Basic_Info__c();
                //BRef.Customer_Name__c='test customer';
                string StrReg=RegRef.id;
                //BRef.Customer_Category__c=CatRef.id;
                //BRef.Customer_Region__c=StrReg;
                //BRef.Customer_Group_Id__c='Test group';
                BRef.Bill_to_Account__c= 'Test bill';
                //BRef.Credit_Account__c='Test Credit Acc';
                BRef.Owner_Ship__c='Public';
                //BRef.Has_Parent__c=true;
                BRef.Parent_Customer_ID__c=BPRef.id;
                //BRef.DND_Financial_Information__c='yes';
                BRef.Last_Financial_Statement_Received__c=system.today();
                BRef.Sole_Sourced__c='yes';
                BRef.Fiscal_Year_End__c=system.today();
                BRef.Review_Status__c='Scheduled';
                BRef.Customers_Zone__c=ZoneRef.id+','+ZRef.id;
                BRef.Customers_Sub_Zone__c=SubZoneRef.id+','+SZoneRef.id;
                BRef.Customer_Validation__c='test';
                BRef.Zone__c=ZoneRef.id;
                BRef.Sub_Zone__c=SubZoneRef.id;
                BRef.Customer_Countries__c=ConRef.id;
                BRef.Country__c=ConRef.id;
                BRef.State__c=StateRef.id;
                BRef.Street_Name_Address_Line_1__c='Test Add1';
                BRef.Street_Name_Address_Line_1__c='';
                BRef.State__c=StateRef.id;
                BRef.City__c='Test City';
                BRef.Postal_Code__c='500081';
                BRef.Analyst__c=Userinfo.getUserId();
                BRef.Backup_Analysts__c=Userinfo.getUserId();
                BRef.Manager__c=Userinfo.getUserId();
                insert BRef;
                
                ApexPages.CurrentPage().getParameters().put('id',BRef.id); 
                ApexPages.StandardController sc = new ApexPages.standardController(BRef);               
                BSureC_CustomerBasicInformation BSureC_CustRef=new BSureC_CustomerBasicInformation(sc);
                BSureC_CustRef.getBSureCustInfo();
				BSureC_CustRef.GetZones(); 
		        BSureC_CustRef.GetSubZones();
		        BSureC_CustRef.GetCountries();
		        BSureC_CustRef.GetStates();
		        BSureC_CustRef.SaveBSureCustomer();         
				BSureC_CustRef.strCountryvalue=CRef.id;
				BSureC_CustRef.lstState();
				BSureC_CustRef.strSubZonevalue = SubZoneRef.id;
				BSureC_CustRef.lstCountry();
				BSureC_CustRef.lstContactSubZone();
				BSureC_CustRef.lstContactZone();
				BSureC_CustRef.blncustomerEdit = false;
				apexpages.currentpage().getParameters().put('Id',BPRef.Id);
				BSureC_CustRef.redirectDetailPage();
				                
                BSureC_CustRef.SaveBSureCustomer();                
                //BRef.Customer_Name__c='test customer';
                BRef.Customer_Category__c = null;
                update BRef;
                
                BRef.Bill_to_Account__c= '';
                update BRef;
                
                BSureC_CustRef.SaveBSureCustomer();
                //BRef.Customer_Region__c=StrReg;
                BRef.Bill_to_Account__c= '';
                update BRef;
                
                BSureC_CustRef.SaveBSureCustomer();
                BRef.Bill_to_Account__c='Test bill' ;
                //BRef.Customer_Group_Id__c='';
                update BRef;
                
                ApexPages.StandardController sc1 = new ApexPages.standardController(BRef);               
                BSureC_CustomerBasicInformation BSureC_CustRef1=new BSureC_CustomerBasicInformation(sc1);
                BSureC_CustRef1.SaveBSureCustomer();
                
                BSureC_CustRef.SaveBSureCustomer();
                
                //BRef.Customer_Group_Id__c='Test group';
                //BRef.Credit_Account__c='';
                update BRef;
                
                BSureC_CustRef.SaveBSureCustomer();
                BRef.Customer_Category__c = null;
                update Bref;
                
                BSureC_CustRef.redirect();
                
                BSureC_CustRef.redirectDetailPage();
    }
    static testMethod void MthdwithOutPageId() {
    	

        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category434');
        insert CatRef;
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region43');
        insert RegRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America23',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA34',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America43',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America434',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States43',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state343',Customer_Country__c=ConRef.id,IsActive__c=true);
        insert StateRef;
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole2';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager2';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager2';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager2';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
       
        insert lstSettings;
        
        BSureC_Customer_Basic_Info__c BPRef=new BSureC_Customer_Basic_Info__c();
                //BPRef.Customer_Name__c='Tset';
                //BPRef.Customer_Category__c=CatRef.id;
                BPRef.Customer_Region__c=RegRef.id;
                BPRef.Customer_Group_Id__c='Test group';
                BPRef.Bill_to_Account__c= 'Test bill';
                BPRef.Credit_Account__c='Test Credit Acc';
                BPRef.Owner_Ship__c='Public';
                BPRef.Has_Parent__c=false;
                insert BPRef;
                system.assertequals('Public',BPRef.Owner_Ship__c);
        
        BSureC_Customer_Basic_Info__c BRef=new BSureC_Customer_Basic_Info__c();
                BRef.Customer_Name__c='test';
                string StrReg=RegRef.id;
                BRef.Customer_Category__c=CatRef.id;
                BRef.Customer_Region__c=RegRef.id;
                BRef.Customer_Group_Id__c='Test group';
                BRef.Bill_to_Account__c= 'Test bill';
                BRef.Credit_Account__c='Test Credit Acc';
                BRef.Owner_Ship__c='Public';
                BRef.Has_Parent__c=false;
                
                BRef.Last_Financial_Statement_Received__c=system.today();
                BRef.Sole_Sourced__c='yes';
                BRef.Fiscal_Year_End__c=system.today();
                BRef.Review_Status__c='Scheduled';
                BRef.Customers_Zone__c=ZoneRef.id;
                BRef.Customers_Sub_Zone__c=SubZoneRef.id;
                BRef.Customer_Validation__c='Test City';
                BRef.Zone__c=ZoneRef.id;
                BRef.Sub_Zone__c=SubZoneRef.id;
                BRef.Country__c=ConRef.id;
                BRef.Street_Name_Address_Line_1__c='Test Add1';
                BRef.State__c=StateRef.id;
                BRef.City__c='Test City';
                BRef.Postal_Code__c='500081';               
                insert BRef;
        

                ApexPages.CurrentPage().getParameters().put('id',''); 
                ApexPages.StandardController sc = new ApexPages.standardController(BRef);               
                BSureC_CustomerBasicInformation BSureC_CustRef=new BSureC_CustomerBasicInformation(sc);
                BSureC_CustRef.CustomerId='12345'; 
                BSureC_CustRef.SaveBSureCustomer();               
                
                
                BSureC_Customer_Basic_Info__c BRefReg=new BSureC_Customer_Basic_Info__c();
                BRefReg.Customer_Name__c='Test Customer';
                //BRefReg.Customer_Region__c=null;
                insert BRefReg;

    }
}