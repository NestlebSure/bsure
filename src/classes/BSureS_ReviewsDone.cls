/***********************************************************************************************
* Controller Name :  BSureS_ReviewsDone
* Date : 17 Dec 2012
* Author : Veereandrnath Jella
* Purpose : This class written to generate report for which suppliers has been done for review  
* Change History :
* Date Programmer Reason
* -------------------- ------------------- -------------------------
* 12/17/2012 Veereandranath.j Initial Version
* 08/12/2013 Veereandranath.j Code Review Changes
*  
**************************************************************************************************/
global with sharing class BSureS_ReviewsDone { 
    public  Transient string  strZoneId{get;set;}         // to store Zone Id
    public  Transient string  strSubZoneId{get;set;}      // to store Sub Zone Id
    public  Transient string  strCountryId{get;set;}      // to store Country Id
    public  Transient string  strAnalystId{get;set;}      // to store Country Id
    public  Transient string  strFromDate{get;set;}       // to hold from date
    public  Transient string  strToDate{get;set;}         // to hold to Date
    public  Transient string  strSupplierName{get;set;}   // to hold supplier Name    
    private  string  sortDirection = 'ASC';      //
    public  Transient string  strSupplierId{get;set;}
    private string  sortExp = 'Supplier_ID__r.Supplier_Name__c';
    public  integer pageNumber;                 //used for pagination
    public  integer pageSize;                   //used for pagination 
    public  integer totalPageNumber;            //used for pagination
    public  integer PrevPageNumber {get;set;}   //used for pagination
    public  integer NxtPageNumber {get;set;}    //used for pagination
    public  integer NlistSize {get;set;}        //used for pagination
    public  integer Endlst {get;set;}           //used for pagination
    public  integer totallistsize{get;set;}     //total size of list SPFolderset and SPWFilesList for pagination
    public  list<SelectOption>  zoneOptions{get;set;}       //selectOptins for list of  Zone Records  
    public  list<SelectOption>  subZoneOptions{get;set;}    //selectOptins for list of sub Zone Records   
    public  list<SelectOption>  coutryOptions{get;set;}     // selectOptins for list of Country Records
    public  list<SelectOption>  AnalystOptions{get;set;}    // selectOptins for list of User(Analyst) Records
    public  BSureS_Basic_Info__c objSupplier{get;set;}
    public  list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;} // to hold credit anaylysis
    public  list<BSureS_Credit_Analysis__c> lstCAPagination{get;set;}
    
    public  string strAnalystName {get;set;}
    public  boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public  string strAnalystSearch{get;set;}
    public  String[] strAnalystProfiles = new String[] {'System Administrator','BSureS_Analyst','BSureS_CreditAdmin','BSureS_Manager','BSureS_Buyer'};//Profiles to be allowed ,'BSureS_Country Manager','BSureS_Sub Zone Manager','BSureS_Zone Manager'
    public  list<User> lstAnalystUser{get;set;}
    public  string  strTodayDate{get;set;}
    private Date dtFrom;
    private Date dtTo;
    public  string strProcurementManager = 'Procurement Manager'; 
    
    public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() {  
        getSuppliers();
    }
    public pagereference Cancel(){
        return page.BSureS_Reports;
    }
      
    public BSureS_ReviewsDone(){
        list<BSureS_User_Additional_Attributes__c> AAttributes = new list<BSureS_User_Additional_Attributes__c>();
        AAttributes = BSureS_CommonUtil.DefaultUserAttributes(UserInfo.getUserId()); 
        
        if(AAttributes != null && AAttributes.size() > 0){
            strZoneId       = AAttributes[0].BSureS_Zone__c;
            strSubZoneId    = AAttributes[0].BSureS_Sub_Zone__c;
            strCountryId    = AAttributes[0].BSureS_Country__c;
        }                                                                       
        
        Date dtYear = Date.Today().addDays(-365);           
        Date dtToday = Date.Today();        
        //strFromDate = dtYear.format();
        //strToDate =  dtToday.format();
        strTodayDate = Date.today().format();
        objSupplier = new BSureS_Basic_Info__c();
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
        lstCAPagination = new list<BSureS_Credit_Analysis__c>();
        lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>();
        getZones();
        getSubZones(); 
        getCountries();      
       
    } 
    // <Summary> 
    //Method Name : getSuppliers
    // </Summary>
    //Description : query all the Suppliers with specific filters
    public void getSuppliers(){
        lstCAPagination = new list<BSureS_Credit_Analysis__c>(); 
        dtFrom  = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        dtTo    = ((strToDate != null && strToDate != '')?date.parse(strToDate):null);  
        //dtTo    = dtTo.addDays(2); 
        
        if(dtFrom != null && dtTo != null) {
        
            string strQuery = 'SELECT   Supplier_ID__c,Supplier_ID__r.Globe_ID__c,Rating_Type__c,Review_Status__c,Name, LastModifiedDate,Supplier_ID__r.Reviewed_by__c, '+ 
                                        ' Supplier_ID__r.Zone__c,Supplier_ID__r.Sub_Zone__c,Supplier_ID__r.BSureS_Country__c,Supplier_ID__r.Analyst__r.Name, '+
                                        ' Supplier_ID__r.Analyst__c,Supplier_ID__r.Supplier_Name__c,Supplier_ID__r.BSureS_Country__r.Name,Spend__c,Rating__c '+
                                        ' From BSureS_Credit_Analysis__c where  Review_Complete_Date__c >=:dtFrom and Review_Complete_Date__c<=:dtTo and Supplier_ID__c != null ';
                                         
                                         
            if(strZoneId != null && strZoneId != 'ALL')
                strQuery +=' AND Supplier_ID__r.Zone__c =: strZoneId ';
            if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
                strQuery +=' AND Supplier_ID__r.Sub_Zone__c =: strSubZoneId ';
            if(strCountryId != null && strCountryId != 'ALL'  )
                strQuery +=' AND Supplier_ID__r.BSureS_Country__c =: strCountryId '; 
                
            if(strAnalystName != null && strAnalystName != '' )
            {
                strAnalystName = strAnalystName;
                string Analyst = strAnalystName.trim() ;
                strQuery +=' AND Supplier_ID__r.Analyst__r.Name Like: Analyst ';
            } 
            
            if(strSupplierName != null && strSupplierName != '')
            {  
                strSupplierName = strSupplierName;
                string sup = strSupplierName.trim();
                strQuery +=' AND Supplier_ID__r.Supplier_Name__c Like: sup ';
            } 
                
                
                 
            string sortFullExp = sortExpression  + ' ' + sortDirection;
              
            strQuery +=' AND Review_Status__c =\'Completed\' ';
            
            strQuery += ' order by ' + sortFullExp +' limit 1000 ';
            if(Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())
            	lstCreditAnalysis = database.query(strQuery);
            
            
        }  
        
        totallistsize= lstCreditAnalysis.size();// this size for pagination
        pageSize = 50;// default page size will be 50
       
       
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber= lstCAPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber= lstCAPagination.size();
            PrevPageNumber=1;
        }  
    }
     // <Summary> 
   // Method Name : getZones
   // </Summary>
   // Description : query all the zones 
    public void getZones()
    {   
        BSureS_CommonUtil.isReport = true;
        zoneOptions = new list<SelectOption>();
        zoneOptions = BSureS_CommonUtil.getZones();
    }
     // <Summary> 
   // Method Name : getSubZones
   // </Summary>
   // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones
    public void getSubZones()
    {    
            
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
        //strCountryId = 'ALL';
        getCountries();     
        BSureS_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        
    }
     // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries()
    {        
        strCountryId = (strZoneId == 'ALL' || strSubZoneId == 'ALL'?'ALL':strCountryId );
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();       
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
    }
   
    
     public void showBuyerDetails() 
    {
        showeditrecs=true;        
        ////system.debug('category==ShowDetail===Supplier=Name=='+ strsupplierCategoryName.Name);  
       // profile getprofileId = [select id from profile where name = 'BSureS_Buyer'];
       //Selecting users with Profile BSureC_Analyst
       if(Schema.Sobjecttype.User.isQueryable())
       lstAnalystUser=[Select Id, Name from User where Profile.Name IN:strAnalystProfiles and UserRole.Name !=:strProcurementManager   and IsActive=true order By Name Asc];
        
    }
     public void closepopup() 
    {
        string strSelectedAnalyst = apexpages.currentpage().getparameters().get('Analystid');
        strAnalystName=strSelectedAnalyst;        
        showeditrecs=false; 
    }
  
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        //system.debug('strSearchBuyer========'+strAnalystSearch);
        string strAnalSrch = '%'+strAnalystSearch+'%';
        if(strAnalystSearch != null && strAnalystSearch != '' && Schema.Sobjecttype.User.isQueryable())  
        {
            lstAnalystUser = [Select Id, Name from User where Name like: strAnalSrch AND Profile.Name IN:strAnalystProfiles and UserRole.Name !=:strProcurementManager AND IsActive=true order By Name Asc];
        }
        else if(Schema.Sobjecttype.User.isQueryable())
        {
            lstAnalystUser = [Select Id, Name from User where IsActive=true AND Profile.Name IN:strAnalystProfiles and UserRole.Name !=:strProcurementManager  order By Name Asc];           
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
    
    public void Search()
    {   
        getSuppliers();     
         
    }
    public Pagereference ExportToExcel(){
        Pagereference pageref = new Pagereference('/apex/BSureS_ReviewsDoneExport');    
        return pageref;
    }
    
    public void BindData(Integer newPageIndex)
    {
       
        try
        {
            lstCAPagination = new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 2;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
        
            if(lstCreditAnalysis != NULL)
            {
                for(BSureS_Credit_Analysis__c b : lstCreditAnalysis)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstCAPagination.add(b);// here adding files list
                    }
                }
            }
            pageNumber = newPageIndex;
            NlistSize = lstCAPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
     // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
       
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }


    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }


    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
   
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {   
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
   
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;
        }
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstCAPagination=new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(BSureS_Credit_Analysis__c a : lstCreditAnalysis)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstCAPagination.add(a);// here adding the folders list      
                }
            } 
            pageNumber = newPageIndex;
            NlistSize=lstCAPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }

    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
 
}