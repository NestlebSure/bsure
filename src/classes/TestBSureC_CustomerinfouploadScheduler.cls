/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomerinfouploadScheduler {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
        BSureC_Customer_Basic_Info_Stage__c cObj = new BSureC_Customer_Basic_Info_Stage__c();
        cObj.Customer_Name__c = 'test Name';
        cObj.Address_Line_2__c ='test Add1';
        cObj.Credit_Account__c = '35405803';
        cObj.Sole_Sourced__c = 'Yes';
        cObj.Status__c='Initialized';
        insert cObj;
        
        BSureC_Customer_Basic_Info_Stage__c cObj1 = new BSureC_Customer_Basic_Info_Stage__c();
        cObj1.Customer_Name__c = 'test Name';
        cObj1.Address_Line_2__c ='test Add1';
        cObj1.Credit_Account__c = '35405803';
        cObj1.Sole_Sourced__c = 'Yes';
        cObj1.Status__c='Exception';
        insert cObj1;
        system.assertEquals('test Name',cObj1.Customer_Name__c);
        
        System.test.starttest();
		BSureC_CustomerinfouploadScheduler BOS = new BSureC_CustomerinfouploadScheduler();
		system.schedule('ScheduleTest','0 0 0 * * ?',BOS);
		System.test.stopTest();
        
    }
}