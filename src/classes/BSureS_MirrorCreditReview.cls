/************************************************************************************************       
Controller Name         : BSureS_MirrorCreditReview       
Date                    : 21/07/2014        
Author                  : satish.c      
Purpose                 : Mirror Credit Review       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          21/07/2014                  Initial Version
**************************************************************************************************/


global with sharing class BSureS_MirrorCreditReview {
    
    public boolean blnTextdisplay{get;set;}
    public boolean blnReviewCompleteDate{get;set;}
    public boolean blnHiddenStyle{get;set;}
    public BSureS_Credit_Analysis__c CAInfo{get;set;} //instance of Object
    public string strplannedReviewDate{get;set;}
    public boolean blnratingVisible{get;set;}
    public boolean blnFutureReviewdateV{get;set;}
    public string strFutureReviewdate{get;set;}
    public boolean blnSavebtn{get;set;}
    public boolean blnSubmitApprovebtns{get;set;}
    public string strRating{get;set;}
    public string strRiskLevelName{get;set;}
    public boolean blnTextdisplayFRD{get;set;}
    public string strSpendEndDate{get;set;}
    public string strSpendStartDate{get;set;}
    public string strSupplierId{get;set;}
    public string creditanalysisId{get;set;} // CreditAnalysis record Id
    public string parentSupName{get;set;}
    public string strParentRevName{get;set;}
    public string strReviewStatusValidation{get;set;}
    public string strCurrentSupplierId{get;set;}
    public list<BSureS_Credit_Analysis__c> lstcreditA{get;set;}
    public list<BSureS_Credit_Analysis_Section__c> lstCreditdoc {get;set;}
    public BSureS_Rating__c sratingObj {get;set;}
    public string strCreatedBy{get;set;}
    public string strSpend{get;set;}
    public string strComments{get;set;}
    public string strReviewStatus{get;set;}
    public string strSupplierreviewSdate{get;set;}
    public list<String> setChildCustID {get;set;}
    public set<String> setChildCreditID {get;set;}
    public set<String> setChildCreditDocID {get;set;}
    public list<String> setChildSupp {get;set;}
    public string strChildCustID {get;set;}
    public string strChildSup {get;set;}
    public boolean blnDocdisplay{get;set;}
    public boolean blnCreditdisplay{get;set;}
    public boolean blnMainPB{get;set;}
    public boolean rensuccessfullassign{get;set;}
    public boolean rensuccessCR{get;set;}
    public map<string,string> mapCreditDoc;
    public map<string,string> mapCreditTopic;
    public map<string,string> mapCreditLink;
    public map<String,String> mapNewSupplierCreditRec;
    public string strNextReviewDate{get;set;}
    public string str{get;set;}
    public string vrating {get;set;}
    public string strTodayDate{get;set;}
    
    DateTime dateObj;
    DateTime d;
    boolean saveCR = null; 
    /// <summary> 
    /// Constructor 
    /// </summary>  
    public BSureS_MirrorCreditReview(ApexPages.StandardController controller)
    {
        blnDocdisplay = false;
        blnCreditdisplay = true;
        blnTextdisplay = true;
        rensuccessfullassign = false;
        rensuccessCR = false;
        blnMainPB = true;
        setChildCreditDocID = new set<string>();
        setChildCustID = new list<string>();
        setChildCreditID =new set<string>();
        setChildSupp = new list<string>();
        mapNewSupplierCreditRec =new map<String,String>();
        strTodayDate = Date.today().format();
        
        if(Apexpages.currentPage().getParameters().get('sid') != null)
        {
            strSupplierId = Apexpages.currentPage().getParameters().get('sid');
            strCurrentSupplierId = strSupplierId;
        }
        else
        {
            
            lstcreditA =  new list<BSureS_Credit_Analysis__c>();
            creditanalysisId = Apexpages.currentPage().getParameters().get('id');
            parentSupName = Apexpages.currentPage().getParameters().get('parentSupp');
            strChildCustID = Apexpages.currentPage().getParameters().get('ChildCustID'); 
			//strChildSup = Apexpages.currentPage().getParameters().get('ChildSuppName');
			
			system.debug(strChildSup+'$$creditanalysisId@@'+creditanalysisId);
			
            if (strChildCustID != null && strChildCustID != '')
            	setChildCustID = strChildCustID.split(',');
            
            /*if (strChildSup != null && strChildSup != '')
            	setChildSupp = strChildSup.split(',');*/
            	
            list<BSureS_Basic_Info__c> lstSupp = [SELECT Supplier_Name__c from BSureS_Basic_Info__c where id IN : setChildCustID];
            
            if(lstSupp != null && lstSupp.size()>0){
            	for (BSureS_Basic_Info__c obj:lstSupp){
            		setChildSupp.add(obj.Supplier_Name__c);
            	}
            }
            
            if(creditanalysisId != null)
            {
                lstcreditA = [select id,Supplier_ID__c,Review_Start_Date__c,Review_End_Date__c,Review_Period__c,Rating__c,Review_Complete_Date__c,Spend_Start_Date__c,Spend_End_Date__c,
                                    Risk_Level__c,Spend__c,Add_Links__c,Review_Status__c,Comment__c,Next_Review_Date__c,Review_Name__c,Rating_Type__c,Risk_Level_Type__c,Created_By__c,
                                    Approve_Comments__c,Actual_Review_Start_Date__c,Expected_Review_End_Date__c from BSureS_Credit_Analysis__c where id =:creditanalysisId ];
                
                if(lstcreditA != null && lstcreditA.size() > 0)
                {
                    strSupplierId = lstcreditA.get(0).Supplier_ID__c;  
                    strReviewStatusValidation = lstcreditA.get(0).Review_Status__c;
                    strParentRevName = lstcreditA.get(0).Review_Name__c;
                }    
            }
            system.debug('lstcreditA@@@'+lstcreditA.size());
            //Edit Functionality
          if(lstcreditA !=null && lstcreditA.size() > 0)
          {
            CAInfo = lstcreditA.get(0);
          }
          if(CAInfo != null){ 
	          CAInfo.Actual_Review_Start_Date__c = system.today();
	          CAInfo.Expected_Review_End_Date__c = system.today();
	          
	          if(CAInfo.Rating__c != null)
	          {
	            sratingObj = BSureS_Rating__c.getValues(CAInfo.Rating__c); 
	            if(sratingObj != null && sratingObj.Months__c != null && CAInfo.Rating__c !='Inactive')
		        {
		            d = date.today().addMonths(Integer.valueOf(sratingObj.Months__c));
		            strNextReviewDate = d.format('MM/dd/yyyy');
		            CAInfo.Next_Review_Date__c = date.today().addMonths(Integer.valueOf(sratingObj.Months__c));
		        }
		        else 
		        {
		            CAInfo.Next_Review_Date__c = null;
		        }
	            strRating = CAInfo.Rating_Type__c;
	          }   
	          if(CAInfo.Risk_Level_Type__c !=  null)
	          {
	            strRiskLevelName = CAInfo.Risk_Level_Type__c;
	          } 
	          if(CAInfo.Spend_Start_Date__c != null)
	          {
	            dateObj = CAInfo.Spend_Start_Date__c;
	            strSpendStartDate = dateObj.format('MM/dd/yyyy');
	          }
	          if(CAInfo.Spend_End_Date__c != null)
	          {
	            dateObj = CAInfo.Spend_End_Date__c;
	            strSpendEndDate = dateObj.format('MM/dd/yyyy');
	          }
	          if(CAInfo.Next_Review_Date__c != null)
	          {
	            dateObj = CAInfo.Next_Review_Date__c;
	            strFutureReviewdate = dateObj.format('MM/dd/yyyy');
	          }
	          if(CAInfo.Spend__c != null)
	          {
	            strSpend = String.valueOf(CAInfo.Spend__c);
	          }
	          if(CAInfo.Comment__c != null)
	          {
	            strComments = CAInfo.Comment__c; 
	            strComments = strComments.replace('\n', '');
	          }
	          if(CAInfo.Review_Status__c != null)
	          {
	            strReviewStatus = CAInfo.Review_Status__c;
	          }
	          if(strSupplierreviewSdate != null && strSupplierreviewSdate !='')
	          {
	            CAInfo.Review_Start_Date__c = date.valueOf(strSupplierreviewSdate);
	          }
          }
           
        }
    }
    
    public pagereference submitApproval()
    {
        mapNewSupplierCreditRec = new map<String,String>();
        list<BSureS_Credit_Analysis__c> lstCA = new list<BSureS_Credit_Analysis__c>();
        
	        for(String supID:setChildCustID){
	        
		        BSureS_Credit_Analysis__c cObj= new BSureS_Credit_Analysis__c();
		        cObj.Review_Name__c = CAinfo.Review_Name__c;
		        cObj.Actual_Review_Start_Date__c = CAinfo.Actual_Review_Start_Date__c;
		        cObj.Expected_Review_End_Date__c = CAinfo.Expected_Review_End_Date__c;
		        
		        if( (CAinfo.Actual_Review_Start_Date__c != null && CAinfo.Actual_Review_Start_Date__c > system.today()) && (CAInfo.Review_Status__c != 'Pending Approval'))
		        {
		            cObj.Review_Status__c = 'Scheduled';
		        }   
		        else if(CAinfo.Actual_Review_Start_Date__c <= system.today() && CAInfo.Review_Status__c != 'Pending Approval')
		        {
		            cObj.Review_Status__c = 'Started';
		        }
		        cObj.Rating__c = CAInfo.Rating__c;
		        cObj.Risk_Level__c = CAInfo.Risk_Level__c;
		        cObj.Next_Review_Date__c = CAInfo.Next_Review_Date__c;
		        cObj.Spend__c = CAInfo.Spend__c;
		        cObj.Spend_Start_Date__c = CAInfo.Spend_Start_Date__c;
		        cObj.Spend_End_Date__c = CAInfo.Spend_End_Date__c;
		        cObj.Comment__c = CAInfo.Comment__c;
		        cObj.Supplier_ID__c = supID.trim();
		        lstCA.add(cObj);
		        
		        /*Database.SaveResult srInsert = Database.insert(cObj);
		        setChildCreditID.add(srInsert.getId());
		        mapNewSupplierCreditRec.put(supID,srInsert.getId());*/
	        }
	        
	        List<Database.SaveResult> insertResults = Database.insert(lstCA);
	        for (Integer i = 0; i < lstCA.size(); i++) {
	       		 
				Database.SaveResult s = insertResults[i];
				if (s.isSuccess()) {
	            	mapNewSupplierCreditRec.put(lstCA[i].Supplier_ID__c,s.getId());
	   		 	}
	        }
	       
	        EditObjectInfo();
        //}
        return null;
    }
    
    public pagereference saveandsubmitApproval()
    {
        
        Integer i=0;
        str =  '<script>funSaveandContinue(\''+ null +'\');</script>';
        mapCreditDoc = new map<String,String>();
        mapCreditTopic = new map<String,String>();
        mapCreditLink = new map<String,String>();
        list<BSureS_Credit_Analysis_Section__c> lstnewSection = new list<BSureS_Credit_Analysis_Section__c>();
        set<id> setExstID=new set<ID>();
        
        for(BSureS_Credit_Analysis_Section__c creAnalSec:lstCreditdoc)
        {
            if(creAnalSec.Mirror_Selection__c==true)
            {
                lstnewSection.add(creAnalSec);
                setExstID.add(creAnalSec.Id);
            }
        }
        
        List<BSureS_Credit_Analysis_Section__c> newOpportunityList = new List<BSureS_Credit_Analysis_Section__c>();
        List<BSureS_Credit_Analysis_Section__c> oppList = lstnewSection;
        
        if(mapNewSupplierCreditRec!=null && !mapNewSupplierCreditRec.isEmpty())
        {
            for(String str :mapNewSupplierCreditRec.keySet())
            {   
                string selectedSctnID='';
                for(BSureS_Credit_Analysis_Section__c opp : oppList){
                    
                    BSureS_Credit_Analysis_Section__c newOpp = opp.clone(false, true); 
                    String creditIncreaseId = mapNewSupplierCreditRec.get(str);
                    newOpp.BSureS_Credit_Analysis__c = creditIncreaseId;
                    newOpp.Supplier_ID__c = str.trim();
                    newOpp.Mirror_Selection__c = false;
                    if(opp.getSObjects('attachments')!=null)
                    newOpp.Mirror_Support__c = string.valueof(opp.getSObjects('attachments')[0].get('Id'));
                    newOpportunityList.add(newOpp);
                }
            }
        }
        
        insert newOpportunityList;
        
        blnDocdisplay = false;
        blnCreditdisplay = false;
        rensuccessfullassign =true;
        rensuccessCR =false;
        blnMainPB = false;
        
        BSureS_MirrorReviewBatch objMR =  new BSureS_MirrorReviewBatch (newOpportunityList, parentSupName, setChildSupp);
        ID batchprocessid = Database.executeBatch(objMR,1);
        
        return null;
    }
    // summary 
    // To get the Info regarding selcted id for Edit functionality
    // summary
    public void EditObjectInfo()
    {
       
        string strEditQuery = ' SELECT  Type__c,Title__c, Name, Link__c, Id,Mirror_Selection__c,Last_Update_By__c,Last_Update__c,Discussions__c, DiscussionType__c, Comment__c, CreatedBy.Name, LastModifiedDate, ' ;
               strEditQuery += ' Supplier_ID__r.Supplier_Name__c,TitleDescription__c,Document_Type__c,Supplier_ID__r.Name,Document_Type_Name__c,Source__c , ';
               strEditQuery += ' (SELECT Id, Name,ParentId,Description From Attachments) FROM  BSureS_Credit_Analysis_Section__c ';
               strEditQuery += ' WHERE BSureS_Credit_Analysis__c =: creditanalysisId ';
        
        lstCreditdoc = database.query(strEditQuery);
        
        system.debug('lstCreditdoc@@@@'+lstCreditdoc.size());
        
        if(lstCreditdoc !=null && lstCreditdoc.size()>0){
        	saveCR = true;
        	str =  '<script>funSaveandContinue(\''+saveCR+'\');</script>';
        	blnDocdisplay = true;
	        blnCreditdisplay = false;
	        rensuccessfullassign = false;
	        rensuccessCR = false;
	        blnMainPB = true;
        }else{
        	saveCR = false;
        	str =  '<script>funSaveandContinue(\''+saveCR+'\');</script>';
        	blnDocdisplay = false;
	        blnCreditdisplay = false;
	        rensuccessfullassign = false;
        	rensuccessCR = true;
        	blnMainPB = false;
        	
        	//Sending Confirmation email to the user
        	String sendMTOJobStatus = BSureS_CommonUtil.getConfigurationValues('BSureS_SendMirrorReviewMail').get(0);
	        string loginUserId = Userinfo.getUserId();
	        User sUser= new User();
	        sUser = [SELECT Id, Email FROM User WHERE Id =: loginUserId];
	        String MTOJobMailID = sUser.Email;
	        system.debug('MTOJobMailID$$$'+MTOJobMailID);
	        
	        if(sendMTOJobStatus.equalsIgnoreCase('TRUE')){
	            
	            List<String> lst_toaddress = new List<String>();
	            lst_toaddress.add(MTOJobMailID);
	            String str_subject = parentSupName+' has been mirrored to its subsidiaries';
	            String str_Email_body = '';
	            str_Email_body += '<table cellspacing="0" cellpadding="0">';
            	str_Email_body += '<tr><td height="50px"><font color="#6600CC">Hello,</font></td></tr><br/><tr><td height="50px"/></tr><tr><td height="50px" width="80%" align="left"><font color="#6600CC">The following subsidiaries have been mirrored to its parent, \''+ parentSupName +'\'</font></td></tr></br>';
            	
            	if(setChildSupp != null && setChildSupp.size() > 0){
	            	for(string obj:setChildSupp){
	            		str_Email_body += '<tr><td height="50px"><font color="#6600CC">'+obj+'</font></td></tr>';
	            	}
            	}
	            str_Email_body += '<br/><tr><td height="50px"/></tr><tr><td height="50px" width="80%" align="left"><font color="#6600CC">powered  by <b> bSure.</b> </font></td></tr>';
	            str_Email_body += '</table>';                 
	
	            if(lst_toaddress != null && lst_toaddress.size() > 0)
	            {
	                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
	                mail1.setToAddresses(lst_toaddress); 
	                mail1.setUseSignature(false);
	                mail1.setSubject(str_subject);
	                mail1.setHtmlBody(str_Email_body); 
	                mail1.setSenderDisplayName('Mirror Status - bSure');
	                //List<Messaging.SendEmailResult> results  = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});
	                
	            }
	        }
        }
    }
    public pagereference cancel() 
    {
        pagereference pageref = new pagereference('/apex/BSureS_ViewPerformCreditAnalysis?Id='+creditanalysisId); 
        return pageref;
    }
    public string visibilityNextRDate()
    {
        sratingObj = BSureS_Rating__c.getValues(vrating); 
        if(sratingObj != null && sratingObj.Months__c != null && CAInfo.Rating__c !='Inactive')
        {
            d = date.today().addMonths(Integer.valueOf(sratingObj.Months__c));
            strNextReviewDate = d.format('MM/dd/yyyy');
            CAInfo.Next_Review_Date__c = date.today().addMonths(Integer.valueOf(sratingObj.Months__c));
        }
        else 
        {
            CAInfo.Next_Review_Date__c = null;
        }
        
        return null;
    }
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
    

}