/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_OverdueScheduler {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Key__c = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Value__c = 'Region Manager';
        insert objCSettings4;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'bsurec_buyerrole';
        objCSettings6.Parameter_Key__c = 'bsurec_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_ShowCustomers';
        objCSettings7.Parameter_Key__c = 'ShowAllCustomersForManagers';
        objCSettings7.Parameter_Value__c = 'TRUE';
        insert objCSettings7;
        
         Profile pfl = [select id from profile where name='BSureC_Analyst' limit 1];
        Profile pf2 = [select id from profile where name='BSureC_Manager' limit 1];
       

        User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='u1@testorg32.com');
        insert testUser;
        
        
        User testUser1 = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg312122.com');
        insert testUser1; 
        
         User Manager = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pf2.Id,  country='United States', CommunityNickname = 'u341321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg34422.com');
        insert Manager;        
        
       
        
        BSureC_Customer_Basic_Info__c objSupplier = new BSureC_Customer_Basic_Info__c
                             (Contact_name__c='KingFisher',
                              Next_Review_Date__c = system.today().addDays(-1),
                              Planned_Review_Date__c = system.today().addDays(15),
                              Analyst__c = testUser1.id,Manager__c = Manager.Id );
        insert objSupplier;
         system.assertEquals('KingFisher',objSupplier.Contact_Name__c);
        
        string BckpAnalysts = testUser.Id+','+testUser1.Id;
        
        BSureC_Customer_Basic_Info__c objSupplier1 = new BSureC_Customer_Basic_Info__c
                             (Contact_name__c='KingFisher2',
                              Next_Review_Date__c = system.today().addDays(-1),
                              Planned_Review_Date__c = system.today().addDays(15));
                               
        insert objSupplier1;
        
        BSureC_Credit_Increase__c BCA = new BSureC_Credit_Increase__c(
                    Customer_Basic_Info_Id__c = objSupplier.Id,Review_Start_Date__c = system.today(),
                    Status__c = 'Not Started');                     
        insert BCA;
        
        
        
        
        BSureC_Credit_Increase__c BCA1 = new BSureC_Credit_Increase__c(
                    Customer_Basic_Info_Id__c = objSupplier.Id,Review_End_Date__c = system.today().addDays(-2),
                    Status__c = 'Pending Approval');
                
        insert BCA1;   
        
        BSureC_Credit_Increase__c BCA2 = new BSureC_Credit_Increase__c(
                    Customer_Basic_Info_Id__c = objSupplier.Id,Review_End_Date__c = system.today().addDays(3),
                    Status__c = 'Started');
                
        insert BCA2;  
        
        BSureC_Credit_Increase__c BCA3 = new BSureC_Credit_Increase__c(
                    Customer_Basic_Info_Id__c = objSupplier.Id,Review_End_Date__c = system.today().addDays(-2),
                    Status__c = 'Completed');
                
        insert BCA3;         
        
        try{
        	BSureC_Credit_Increase__c BCA4 = new BSureC_Credit_Increase__c(
            Customer_Basic_Info_Id__c = objSupplier.Id,Review_End_Date__c = system.today().addDays(30),
            Status__c = 'Started');
        	insert BCA4;
        }catch(Exception e){}
        
        System.test.starttest();
        BSureC_OverdueScheduler BOS = new BSureC_OverdueScheduler();
        system.schedule('ScheduleTest','0 0 0 * * ?',BOS);
        System.test.stopTest();
    }
}