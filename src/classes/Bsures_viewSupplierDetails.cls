/*************************************************************************************************
* Controller Name   : Bsures_viewSupplierDetails
* Date              : 26/11/2012
* Author            : Kishore  
* Purpose           : Class for Supplier Detail Page
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 26/11/2012                                Initial Version
* 08/11/2013      Veereandrnath j           Code Review Changes
**************************************************************************************************/
global with sharing class Bsures_viewSupplierDetails {
    public string strSupplierId{get;set;} //Variable for get Supplier ID
    public string strAnalystName{get;set;} //Variable for get Supplier Alalyst ID
    public string strManagerName{get;set;} //Variable for get Supplier Manager ID
    public string strBackUpAnalystsNames{get;set;} //Variable for get Supplier Backup Alalysts ID
    public boolean visibilityEdit{get;set;} //standard Edit button visibility
    public boolean visibilityCreditTeamAssignment{get;set;}
    public boolean visibilityNewCreditAnalysisBtn{get;set;}
    public boolean visibilityNewSpendhistory{get;set;}
    public Id currentUserId{get;set;} // get current user id
    public boolean deleteBuyerInfo{get;set;}
    
    public boolean visibilityNewBuyerBtn{get;set;}//New Buyer button Visibility
    public list<BSureS_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureS_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureS_Country_Manager__c> lstCountryManager{get;set;}
    public boolean blnSuppplierEdit{get;set;}
    public string strCEO{get;set;}
    
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
     
    /// <summary>
    /// constructor 
    /// </summary>
    public Bsures_viewSupplierDetails(ApexPages.StandardController controller) 
    {
        string strreviewStatus='Started';
        strSupplierId=Apexpages.currentPage().getParameters().get('Id'); //get the current supplier id   
        lstZoneManager = new list<BSureS_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureS_Sub_Zone_Manager__c>(); 
        lstCountryManager = new list<BSureS_Country_Manager__c>();
        lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>();
        blnSuppplierEdit = false;
        visibilityEdit = false;
        visibilityCreditTeamAssignment = false;
        list<Profile> lst_profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
        
        list<BSureS_Basic_Info__c> sDetailsView;
        if(strSupplierId != null && Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable())
        {
             sDetailsView = [select id,Analyst__c,Analyst_Name__c,Manager__c,Backup_Analysts__c,Supplier_ID__c,
                                                Zone__c,Sub_Zone__c,BSureS_Country__c
                                                from BSureS_Basic_Info__c where id =: strSupplierId];
             if(Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())                                   
                lstCreditAnalysis = [select id,Review_Status__c,Supplier_ID__c from BSureS_Credit_Analysis__c where Supplier_ID__c =:strSupplierId and Review_Status__c =: strreviewStatus ];
       
        }
        if(sDetailsView != null && sDetailsView.size() >0 )
        {
            if(sDetailsView.get(0).Analyst__c != null )
            {
                strAnalystName = sDetailsView.get(0).Analyst__c;
            }
            if(sDetailsView.get(0).Manager__c!= null )
            {
                strManagerName = sDetailsView.get(0).Manager__c;
            }
            if(sDetailsView.get(0).Backup_Analysts__c != null)
            {
                strBackUpAnalystsNames = sDetailsView.get(0).Backup_Analysts__c;
            }
            // checking Query permession
            if(sDetailsView.get(0).Zone__c != null && Schema.sobjecttype.BSureS_Zone_Manager__c.isQueryable())
            {
                lstZoneManager = [select Zone_Manager__c from BSureS_Zone_Manager__c where Zone__c =: sDetailsView.get(0).Zone__c];
            }
            // checking Query permession
            if(sDetailsView.get(0).Sub_Zone__c != null && Schema.sobjecttype.BSureS_Sub_Zone_Manager__c.isQueryable())
            {
                lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureS_Sub_Zone_Manager__c where Sub_Zone__c =: sDetailsView.get(0).Sub_Zone__c];
            }
            // checking Query permession
            if(sDetailsView.get(0).BSureS_Country__c != null && Schema.sobjecttype.BSureS_Country_Manager__c.isQueryable())
            {
                lstCountryManager = [select Country_Manager__c from BSureS_Country_Manager__c where Country__c =: sDetailsView.get(0).BSureS_Country__c];
            }
        }   
        
        currentUserId = UserInfo.getUserId();
        system.debug('currentUserId ===>'+currentUserId );
        if(!BSureS_CommonUtil.getConfigurationValues('BSureS_CEORole').isEmpty())
            strCEO = BSureS_CommonUtil.getConfigurationValues('BSureS_CEORole').get(0);     
        
        set<id> setUserIds = new set<id>();
        if(strCEO != null && strCEO != '' && Schema.Sobjecttype.user.isQueryable())
        {
            for(User eachUser : [select id from user where userrole.name =:strCEO])
            {
                setUserIds.add(eachUser.Id);
            }
        } 
        
        
        if(setUserIds.size() > 0 && setUserIds.contains(currentUserId))
        {
            blnSuppplierEdit = false;
            visibilityEdit = true;
            visibilityCreditTeamAssignment = true; 
            visibilityNewBuyerBtn = true;
            visibilityNewCreditAnalysisBtn = true;
            deletebuyerInfo = true;
            visibilityNewSpendhistory = true;
            
        }
        
        if(strBackUpAnalystsNames != null )
        {
            if(strBackUpAnalystsNames.contains(','))
            {
                for(String ba:strBackUpAnalystsNames.split(','))
                {
                    if(currentUserId == ba)
                    {
                        visibilityEdit = true;
                        visibilityNewCreditAnalysisBtn = true;
                        break;
                    }
                }
            }
            else
            {
                if(currentUserId == strBackUpAnalystsNames)
                {
                    visibilityEdit = true;
                    visibilityNewCreditAnalysisBtn = true;
                }
            }   
        }
        
        if(strManagerName != null && currentUserId == strManagerName)
        {
            visibilityCreditTeamAssignment = true;
            visibilityNewBuyerBtn = true;
            visibilityNewCreditAnalysisBtn = true;
            deletebuyerInfo = true;
        }
        if(currentUserId == strAnalystName || currentUserId == strManagerName)
        {
            if(setUserIds.size() > 0 && setUserIds.contains(currentUserId))
            {
                blnSuppplierEdit = true;
            }   
            visibilityEdit = true;
            visibilityNewCreditAnalysisBtn = true;
            visibilityNewSpendhistory = true;
        }
        
        if(lstZoneManager != null && lstZoneManager.size() > 0)
        {
            for(BSureS_Zone_Manager__c ObjZM:lstZoneManager)
            {
                if(ObjZM.Zone_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        
        if(lstSubZoneManager != null && lstSubZoneManager.size() >0)
        {
            for(BSureS_Sub_Zone_Manager__c ObjSZM : lstSubZoneManager)
            {
                if(ObjSZM.Sub_Zone_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        
        if(lstCountryManager != null && lstCountryManager.size() >0)
        {
            for(BSureS_Country_Manager__c ObjCM : lstCountryManager)
            {
                if(ObjCM.Country_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        if(lstCreditAnalysis != null && lstCreditAnalysis.size() > 0)
        {
            blnSuppplierEdit = false;
        }
    }  
   
      
    /// <summary>
    /// Method to view the Supplier Details
    /// </summary>
    public BSureS_Basic_Info__c getSupplierDetails() 
    {
        BSureS_Basic_Info__c sDetails;
        if(strSupplierId != null && Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable())
        {
            sDetails = [select id,Supplier_ID__c from BSureS_Basic_Info__c where id =: strSupplierId limit 1];  
        }
        return sDetails;
    }
    
    
}