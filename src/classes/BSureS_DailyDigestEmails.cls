/************************************************************************************************       
Controller Name         : BSureS_DailyDigestEmails       
Date                    : 02/14/2013        
Author                  : Aditya Vemuri       
Purpose                 : Scheduler To send Daily Digest emails      
Change History          : Date           Programmer              Reason       
						--------------   -------------------     -------------------------       
                          02/14/2013     Aditya Vemuri           Initial Version
**************************************************************************************************/
global with sharing class BSureS_DailyDigestEmails implements Schedulable {
	string strNSubject = '';
    string strNBody = '';
    string strDDSubject = '';
    string strDDBody = '';    
    string strForumName = '';
    map<string, list<BSureS_Email_Queue__c>> mapEmailQueue = new map<string, list<BSureS_Email_Queue__c>>();
    list<EmailTemplate> lstEmailTemplates;
    list<BSureS_Email_Queue__c> lstEmailMsgs;
    set<string> setTemplateNames = new set<string>{'BSureS_Notifications', 'BSureS_DailyDigest'};
    
    global BSureS_DailyDigestEmails()
    {
    }
    
    global void execute(SchedulableContext sc)
	{
		string strNew = 'NEW';
		string strYes = 'YES';
		string strUniqueKey = '';
		strForumName = BSureS_CommonUtil.getConfigurationValues('BSureS_ForumName').get(0);
        list<string> lstIndEmailAdd = new list<string>();
        transient list<Messaging.SingleEmailMessage> objMailsList = new list<Messaging.SingleEmailMessage>(); 
        
        GetEmailTemplates();
        //NAGA Check if the user has read access on the BSureS_Email_Queue__c object
      	if(Schema.sObjectType.BSureS_Email_Queue__c.isAccessible())
	  	{
        	lstEmailMsgs = new list<BSureS_Email_Queue__c>([SELECT Id, Recipient_Address__c, Is_Daily_Digest__c, Email_Subject__c, Email_Status__c, 
									         	Email_Priority__c, Email_Body__c, Recipient_Name__c, Send_Immediate__c 
									     	FROM BSureS_Email_Queue__c
									        WHERE Email_Status__c = :strNew
									        AND Send_On_Date__c <= TODAY 
									    	AND Is_Daily_Digest__c = :strYes
									    	AND Send_Immediate__c = false 
									    	ORDER BY CreatedDate limit 1000]);
    	 
	        if(lstEmailMsgs != null && lstEmailMsgs.size() > 0)
	        {
	        	for(BSureS_Email_Queue__c objEachMail : lstEmailMsgs)
		        {
		            if(objEachMail.Recipient_Address__c != null)
		            {
		                strUniqueKey = objEachMail.Recipient_Address__c.trim() + '~' +
		                	 (objEachMail.Is_Daily_Digest__c != null && objEachMail.Is_Daily_Digest__c != '' 
		                	 	? objEachMail.Is_Daily_Digest__c.trim() + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase() 
		                	 	: 'YES' + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase());
		            }   
		            if(mapEmailQueue.containsKey(strUniqueKey))
		            {
		                list<BSureS_Email_Queue__c> objListEQ = mapEmailQueue.get(strUniqueKey);
		                objListEQ.add(objEachMail);
		                mapEmailQueue.put(strUniqueKey, objListEQ);
		            }
		            else
		            {
		                list<BSureS_Email_Queue__c> objListEQ = new list<BSureS_Email_Queue__c>();
		                objListEQ.add(objEachMail);
		                mapEmailQueue.put(strUniqueKey, objListEQ);
		            }
		        }
		        //system.debug('mapEmailQueue+++++++++' + mapEmailQueue.size());
		        if(mapEmailQueue != null && mapEmailQueue.size() > 0)
		        {
		            for(string toAddressKey : mapEmailQueue.keySet())
		            {
		                if(toAddressKey.contains('YESFALSE'))
		                {
		                    objMailsList.add(GenerateConsolidatedMail(mapEmailQueue.get(toAddressKey)));
		                }
		            }
		        }
		        Messaging.sendEmail(objMailsList);
		        UpdateEmailQueue(lstEmailMsgs);
	        }
	  	}
	}
	
	public Messaging.SingleEmailMessage GenerateConsolidatedMail(list<BSureS_Email_Queue__c> objListEQ)
    {
        string strEmailAdds = '';
        string strRecipientName = '';
        string strSubject = '';
        transient string strEmailBody = '';
        transient string strFinalBody = '';
        
        list<string> lstEmailAdd = new list<string>();
        strSubject = strDDSubject.replace('%%DATE%%', System.today().format()); 
        strEmailBody = '<table width="100%" border="0" cellpadding="10" cellspacing="10">';
        
        Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
        
        for(BSureS_Email_Queue__c objEachMsg : objListEQ)
        {
            strEmailAdds = objEachMsg.Recipient_Address__c;
            strRecipientName = objEachMsg.Recipient_Name__c;
            strEmailBody += '<tr><td width="100%" style="border-bottom: 1px solid;">';
            strEmailBody += '<b>' + objEachMsg.Email_Subject__c + '</b></br>';
            strEmailBody += '______________________________________________________</br></br>';
            strEmailBody += objEachMsg.Email_Body__c;
            strEmailBody += '</br></td></tr>';
        }
        strEmailBody += '</table>';
        strFinalBody = strDDBody.replace('%%USER%%', strRecipientName != null && strRecipientName != '' ? strRecipientName : '');
        strFinalBody = strFinalBody.replace('%%EMAILBODY%%', strEmailBody);
        strFinalBody = strFinalBody.replace('\r\n', '</br></br>');
        lstEmailAdd.add(strEmailAdds);
        
        objMail.saveAsActivity = false;
        objMail.setToAddresses(lstEmailAdd);
        objMail.setUseSignature(false);
        objMail.setSaveAsActivity(false);
        objMail.setSubject(strSubject); 
        objMail.setSenderDisplayName(strForumName);
        objMail.setHtmlBody(strFinalBody);
        return objMail;
    }
    
    public void UpdateEmailQueue(list<BSureS_Email_Queue__c> lstEmailMsgs)
    {
        try
        {
            for(BSureS_Email_Queue__c objEachMail : lstEmailMsgs)
            {
                objEachMail.Email_Status__c = 'SENT';
                objEachMail.Delivered_Date__c = Datetime.now();
            }
            if(BSureS_Email_Queue__c.sObjectType.getDescribe().isUpdateable())
            Database.update(lstEmailMsgs);
        }
        catch(Exception ex)
        {
            //system.debug('Exception in UpdateEmailQueue ' + ex);
        }
    }
    
    public void GetEmailTemplates()
    {
        lstEmailTemplates = new list<EmailTemplate>();
         if(Schema.sObjectType.EmailTemplate.isAccessible())
         {
	        lstEmailTemplates = [SELECT Subject, Name, IsActive, Body
	                             FROM EmailTemplate
	                             WHERE Name IN: setTemplateNames
	                             AND IsActive =: true];
	        
	        if(lstEmailTemplates != null && lstEmailTemplates.size() > 0)
	        {
	            for(EmailTemplate objEach : lstEmailTemplates)
	            {
	                if(objEach.Name.contains('DailyDigest'))
	                {
	                    strDDBody = objEach.Body;
	                    strDDSubject = objEach.Subject;
	                }
	                else if(objEach.Name.contains('Notifications'))
	                {
	                    strNBody = objEach.Body;
	                    strNSubject = objEach.Subject;
	                }
	            }
	            //system.debug('strDDBody++++++++ ' + strDDBody);
	            //system.debug('strNBody++++++++ ' + strNBody);
	        }
         }
    }
}