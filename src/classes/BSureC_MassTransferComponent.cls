//Controller used for the AutoComplete Enhancement
global with sharing class BSureC_MassTransferComponent
{
   @RemoteAction 
    global static SObject[] findSObjects(string obj, string qry, string addFields, string profilename,String ProfileIDs,String uid,String Customerids) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
            List<sObject> L;
                //system.debug('in if==========================='+ProfileIDs);
       if(profilename==NULL || profilename=='')
       {
        //system.debug('in if===========================');
            List<String> fieldList=new List<String>(); 
        if (addFields != '')  
        fieldList = addFields.split(',');
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            if(!System.test.isRunningtest())
            {
            Schema.SObjectType sot = gd.get(obj);
            if (sot == null) 
            {
                return null;
            }
            }
        
        //system.debug('qry---'+qry+'--addFields='+addFields);
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        List<String> Profilelistcust=BSureC_CommonUtil.getConfigurationValues('BSureC_MassTransferAllowedProfiles');
        //set<String> Profilelist=new set<String>{'BSureC_Customer Service','BSureC_Country Manager','BSureC_Sub Zone Manager','BSureC_Zone Manager','BSureC_On Going Collector','BSureC_Sales','BSureC_Un-authorized Collector','BSureC_Un-earned Collector','BSureC_Analyst'};
        set<String> Profilelist=new set<String>();
        Profilelist.addAll(Profilelistcust);
        String soql='select Name,User.Profile.Name,isActive From User Where Name '+Filter;
        soql+=' AND isActive=true';
        soql+=' AND User.Profile.Name  IN:  Profilelist';
        
        soql+=' order by Name';
        
        //system.debug('Qry: '+soql);
        
         L = new List<sObject>();
        try 
        {
            L = Database.query(soql);
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
        
      }
      
     else
       {
        //system.debug('in if===============cat============'+profilename);
        set<String> lstusers=new set<String>();
        set<String> lstfinalusers=new Set<String>();
        String str_catuids='';    
        //Creating the filter text 
        //if(Test.isRunningTest()){}
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        for(BSureC_UserCategoryMapping__c UserCat:[Select id,CategoryId__c,User__c from BSureC_UserCategoryMapping__c where CategoryId__c=:profilename])
        {
            lstusers.add(UserCat.User__c);
            str_catuids+=','+UserCat.User__c;
        }
        //system.debug('in if===============cat=======lstusers====='+lstusers);
        //system.debug('in if===============str_catuids=======lstusers====='+str_catuids);
        List<string> prof=new List<string>();
        List<String> customerid=new List<String>();
        if(ProfileIDs!=null && ProfileIDs!='')
        {
        if(ProfileIDs.contains(','))
        {
        string[] profiles=ProfileIDs.split(',');
        for(string s:profiles)
        {
            prof.add(s);
        }
        }
        else
        {
            prof.add(ProfileIDs);
        }
        }
        set<String> set_managerids=new set<String>();
        set<String> set_adduserids=new set<String>();
        for(BSureC_AdditionalAttributes__c lst_Objadditional:[SELECT Manager__c,User__c FROM BSureC_AdditionalAttributes__c WHERE User__c=:uid])
        {
            set_managerids.add(lst_Objadditional.Manager__c);
        }
        
        for(BSureC_AdditionalAttributes__c lst_Objadditional:[SELECT Manager__c,User__c FROM BSureC_AdditionalAttributes__c WHERE Manager__c in : set_managerids])
        {
            set_adduserids.add(lst_Objadditional.User__c);
        }
        //lstusers  set_adduserids
        //system.debug('set_adduserids============='+set_adduserids);
        //str_catuids+=',005G00000026LcWIAU';
        //system.debug('str_catuids============='+str_catuids);
        
        set<String> set_finaluids=new set<String>();
        for(String str:set_adduserids)
        {
            //system.debug('inside for=========='+str);
            //system.debug('str_catuids====insideif====='+str_catuids);
            if(str_catuids.contains(str))
            {
                //system.debug('=====inside');
                set_finaluids.add(str);
            }
        }
        //system.debug('set_finaluids============='+set_finaluids);
        
        if(Customerids!=NULL)
        {
            if(Customerids.contains(','))
            {
            customerid=Customerids.split(','); 
            }
            else
            {
            customerid.add(Customerids);
            }
        }
        
        for(BSureC_Customer_Basic_Info__c  ObjSup:[SELECT id,Customer_Name__c,Analyst__c,Backup_Analysts__c from BSureC_Customer_Basic_Info__c  where id in :customerid])
        {
            if(ObjSup.Analyst__c!=NULL)
            {
                if(set_finaluids.contains(ObjSup.Analyst__c))
                {
                    //system.debug('in if=====analyst');
                    set_finaluids.remove(ObjSup.Analyst__c);
                }
            }
            
        }
        
        for(BSureC_Customer_Basic_Info__c  ObjSup:[SELECT id,Customer_Name__c,Analyst__c,Backup_Analysts__c from BSureC_Customer_Basic_Info__c  where id in :customerid])
        {
            if(ObjSup.Backup_Analysts__c!=NULL)
            {
                String[] str=ObjSup.Backup_Analysts__c.split(',');
                for(String Objstr:str)
                {
                    if(set_finaluids.contains(Objstr))
                    {
                        set_finaluids.remove(Objstr);
                    }
                }
            }
        }
        for(User u:[select id,name from user where id in: set_finaluids and ProfileID IN:prof])
        {
            lstfinalusers.add(u.id);
        }
        //system.debug('in if===============cat=======lstfinalusers====='+lstfinalusers);
        String soql='select id,Name,User.Profile.Name,isActive From User Where Name '+Filter;
        soql+=' AND isActive=true';
        soql+=' AND id  IN:  lstfinalusers';
        //system.debug('uid==========='+uid);
        if(uid!=null && uid!='')
        {
            ////system.debug('');
            soql+=' AND id!=:uid'; 
        }
        soql+=' order by Name';
        
        //system.debug('Qry: '+soql);
        
         L = new List<sObject>();
        try 
        {
            L = Database.query(soql);
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
        
      }
      //system.debug('L========================'+L);
        return L;
   }
}