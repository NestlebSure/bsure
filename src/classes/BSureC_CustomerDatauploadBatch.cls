/************************************************************************************************       
Controller Name         : BSureC_CustomerDatauploadBatch       
Date                    : 11/19/2012        
Author                  : Santhosh Palla       
Purpose                 : To Inser the Customer Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/08/2012      Santhosh.Palla            Initial Version
                          23/01/2013      VINU PRATHAP          Modified to get the logged in user Email address
                          02/09/2013      kishorekumar A        Added sharing rule for assigned users 
**************************************************************************************************/
global with sharing class BSureC_CustomerDatauploadBatch implements Database.Batchable<SObject> 
{
	global map<string,string> mapUserName = new map<string,string>();
	global map<string,string> mapUserName2 = new map<string,string>();
	global map<string,string> mapMngr  = new map<string,string>();
    global String str_customerInfo_stage = null;
    public list<BSureC_Customer_Basic_Info_Stage__c> lst_customerstage = new list<BSureC_Customer_Basic_Info_Stage__c>();
    public List<BSureC_Customer_Basic_Info__c> lst_customer = new List<BSureC_Customer_Basic_Info__c>();
    
    public BSureC_CustomerDatauploadBatch()
    {
      str_customerInfo_stage = 'SELECT Address_Line_2__c,Analyst__c,Analyst_for_Next_Review__c,Backup_Analysts__c,Backup_Analyst_Names__c,Bill_to_Account__c,'+
                            'City__c,Contact_Name__c,Customer_Countries__c,Country__c,Credit_Account__c,Credit_Card__c,Credit_Limit__c,'+
                            'Customer_Contact__c,Customer_Category__c,Customer_Desc__c,Customer_ID__c,Customer_Name__c,Customer_Region__c,'+
                            'Customer_Validation__c,DND_Financial_Information__c,Email_address__c,F_S__c,Facility_Expiration__c,Facility_s__c,'+
                            'Fax__c,Fiscal_Year_End__c,GMC__c,Customer_Group_Id__c,Has_Parent__c,IsActive__c,Last_Financial_Statement_Received__c,'+
                            'Manager__c,NAICS__c,Next_Review_Date__c,Notification_Flag__c,Owner_Ship__c,Parent_Customer_ID__c,Parent_Customer__c,'+
                            'Phone_Number__c,Planned_Review_Date__c,Postal_Code__c,Rebate_Disc__c,Review_Complete_Date__c,Review_Date__c,'+
                            'Reviewed_By__c,Review_Status__c,Risk_Category__c,Sole_Sourced__c,State__c,Street_Name_Address_Line_1__c,'+
                            'Sub_Zone__c,Customers_Sub_Zone__c,Website_Link__c,Zone__c ,Collectors__c '+
                            'FROM BSureC_Customer_Basic_Info_Stage__c  WHERE Exception__c=NULL and Publish_Flag__c=false order by Customer_ID__c';
    }
    global Iterable<sObject> start (Database.BatchableContext ctx)
    {         
       return Database.query(str_customerInfo_stage);
    }   
    global void execute(Database.BatchableContext BC, list<BSureC_Customer_Basic_Info_Stage__c> scope)
    {
    	getUserName();
    	getUserName2();
    	getUserMngr();
        map<string, string> mpCACollector = new map<string, string>();
        map<String, string> mapbcanlysharing = new map<string, string>();
        
        lst_customerstage=new list<BSureC_Customer_Basic_Info_Stage__c>();
        lst_customer = new List<BSureC_Customer_Basic_Info__c>();
        for(BSureC_Customer_Basic_Info_Stage__c  obj : scope)
        {
            BSureC_Customer_Basic_Info__c rec_customer_publish = new BSureC_Customer_Basic_Info__c();
            
            rec_customer_publish.Address_Line_2__c   = obj.Address_Line_2__c;
            rec_customer_publish.Analyst__c = obj.Analyst__c;
            rec_customer_publish.Collector__c = mapUserName.get(obj.Collectors__c);
            rec_customer_publish.Collector_Name__c = obj.Collectors__c;
            system.debug('-mapUserName----'+mapUserName.get(obj.Collectors__c));
            //system.debug('-rec_customer_publish.Collector__c--'+rec_customer_publish.Collector__c);
            //system.debug('-mapMngr--'+mapMngr);
            //system.debug('-mapUserName--'+mapUserName);
             rec_customer_publish.Collector_Manager__c =  mapMngr.get(mapUserName.get(obj.Collectors__c));
            rec_customer_publish.Collector_Manager_Name__c =  mapUserName2.get(rec_customer_publish.Collector_Manager__c);
            /* 
            rec_customer_publish.Collector__c = obj.Collectors__c;
            rec_customer_publish.Collector_Name__c =  mapUserName.get(obj.Collectors__c);
            rec_customer_publish.Collector_Manager__c =  mapMngr.get(obj.Collectors__c);
            rec_customer_publish.Collector_Manager_Name__c =  mapUserName.get(rec_customer_publish.Collector_Manager__c); */
            //kishore added for record owner
            rec_customer_publish.OwnerId =  obj.Analyst__c   ;
            rec_customer_publish.Analyst_for_Next_Review__c = obj.Analyst_for_Next_Review__c;
            //rec_customer_publish.Backup_Analysts__c =obj.Backup_Analysts__c;//commented by satish on may 21st 2014
            //uncommented below code kishore for Backup Analyst names
            //rec_customer_publish.Backup_Analyst_Names__c =  obj.Backup_Analyst_Names__c;//commented by satish on may 21st 2014
            rec_customer_publish.Bill_to_Account__c =obj.Bill_to_Account__c;
            rec_customer_publish.City__c    =obj.City__c;
            rec_customer_publish.Contact_Name__c    =obj.Contact_Name__c;
            rec_customer_publish.Customer_Countries__c  =obj.Customer_Countries__c;
            rec_customer_publish.Country__c =obj.Country__c;
            rec_customer_publish.Credit_Account__c  = obj.Credit_Account__c;
            rec_customer_publish.Credit_Card__c =obj.Credit_Card__c;
            rec_customer_publish.Credit_Limit__c    =obj.Credit_Limit__c;
            rec_customer_publish.Customer_Contact__c=   obj.Customer_Contact__c;
            rec_customer_publish.Customer_Category__c=  obj.Customer_Category__c;
            rec_customer_publish.Customer_Desc__c   =obj.Customer_Desc__c;
            rec_customer_publish.Customer_Name__c=  obj.Customer_Name__c;
            rec_customer_publish.Customer_Region__c=    obj.Customer_Region__c;
            rec_customer_publish.Customer_Validation__c=    obj.Customer_Validation__c;
            rec_customer_publish.DND_Financial_Information__c=  obj.DND_Financial_Information__c;
            rec_customer_publish.Email_address__c   =obj.Email_address__c;
            rec_customer_publish.F_S__c =obj.F_S__c;
            rec_customer_publish.Facility_Expiration__c=    obj.Facility_Expiration__c;
            rec_customer_publish.Facility_s__c  =obj.Facility_s__c;
            rec_customer_publish.Fax__c =obj.Fax__c;
            rec_customer_publish.Fiscal_Year_End__c=obj.Fiscal_Year_End__c;
            rec_customer_publish.GMC__c=obj.GMC__c;
            rec_customer_publish.Customer_Group_Id__c=  obj.Customer_Group_Id__c;
            rec_customer_publish.Has_Parent__c  =obj.Has_Parent__c;
            rec_customer_publish.Last_Financial_Statement_Received__c=  obj.Last_Financial_Statement_Received__c;
            //rec_customer_publish.Manager__c =obj.Manager__c; 
            rec_customer_publish.Manager__c =mapMngr.get(obj.Analyst__c);
            rec_customer_publish.NAICS__c   =obj.NAICS__c;
            rec_customer_publish.Next_Review_Date__c    =obj.Next_Review_Date__c;
            rec_customer_publish.Notification_Flag__c   =obj.Notification_Flag__c;
            rec_customer_publish.Owner_Ship__c  =obj.Owner_Ship__c;
            rec_customer_publish.Parent_Customer_ID__c= obj.Parent_Customer_ID__c;
            //rec_customer_publish.Parent_Customer__c   =obj.Parent_Customer__c;
            rec_customer_publish.Phone_Number__c    =obj.Phone_Number__c;
            rec_customer_publish.Planned_Review_Date__c=    obj.Planned_Review_Date__c;
            rec_customer_publish.Postal_Code__c =obj.Postal_Code__c;
            rec_customer_publish.Rebate_Disc__c =obj.Rebate_Disc__c;
            rec_customer_publish.Review_Complete_Date__c=   obj.Review_Complete_Date__c;
            //rec_customer_publish.Review_Date__c   =obj.Review_Date__c;
            //rec_customer_publish.Reviewed_By__c   =obj.Reviewed_By__c;
            if(rec_customer_publish.Planned_Review_Date__c != null)
            {
                rec_customer_publish.Review_Status__c='Scheduled';
            }
            rec_customer_publish.Risk_Category__c=  obj.Risk_Category__c;
            rec_customer_publish.Sole_Sourced__c    =obj.Sole_Sourced__c;
            rec_customer_publish.State__c   =obj.State__c;
            rec_customer_publish.Street_Name_Address_Line_1__c= obj.Street_Name_Address_Line_1__c;
            rec_customer_publish.Sub_Zone__c    =obj.Sub_Zone__c;
            rec_customer_publish.Customers_Sub_Zone__c= obj.Customers_Sub_Zone__c;
            rec_customer_publish.Website_Link__c    =obj.Website_Link__c;
            rec_customer_publish.Zone__c=obj.Zone__c;
            
            obj.Publish_Flag__c = true;//for inserted records making flag as true to update in stgae
            obj.Status__c = 'Published';
            obj.Publish_Date__c = Date.valueof(system.now());           
            obj.Status_Resource_Value__c='/resource/1242640894000/Green';
            mpCACollector.put(obj.Credit_Account__c, obj.Collectors__c);
            if(obj.Backup_Analysts__c != null)
            {
            	mapbcanlysharing.put(obj.Credit_Account__c,obj.Backup_Analysts__c);
            }	
            lst_customer.add(rec_customer_publish);
        	lst_customerstage.add(obj);
        }
        //Database.insert(lst_customer);//inserting records in publish
        Database.Saveresult[] lstcustsave = Database.insert(lst_customer,false);
        
    	Database.update(lst_customerstage);//updating the stage records
        //UpdateCollectors(mpCACollector);
       	if(mapbcanlysharing != null && mapbcanlysharing.size() > 0)
        {
        	//assignsharingtoUsers(mapbcanlysharing);	//commented by satish on may 21st 2014
        }        
    }
    
    public void assignsharingtoUsers(map<string,string> mapbacoll)
    {
    	List<BSureC_Customer_Basic_Info__Share> lstCustomerShare = new List<BSureC_Customer_Basic_Info__Share >();
    	if(mapbacoll != null && mapbacoll.size() > 0)
    	{
    		list<BSureC_Customer_Basic_Info__c> lstCustba = new list<BSureC_Customer_Basic_Info__c>([SELECT Id, Credit_Account__c, Backup_Analysts__c
                                                                                                   FROM BSureC_Customer_Basic_Info__c
                                                                                                   WHERE Credit_Account__c IN: mapbacoll.keySet()]);
            if(lstCustba != null && lstCustba.size() > 0)
            {
            	
            	for(BSureC_Customer_Basic_Info__c cobj : lstCustba)
            	{
            		list<string> lststring = new list<string>();
            		if(mapbacoll.get(cobj.Credit_Account__c) != null)
            		{
            			if(mapbacoll.get(cobj.Credit_Account__c).contains(','))
            			{
            				lststring = mapbacoll.get(cobj.Credit_Account__c).split(',');
            				for(string s: lststring)
            				{
            					if(s != null && s !='')
            					{
	            					BSureC_Customer_Basic_Info__Share  shareObj = new BSureC_Customer_Basic_Info__Share();
				                    shareObj.AccessLevel = 'Edit';
				                    shareObj.ParentId = cobj.Id;//.substring(0, 15);
				                    shareObj.UserOrGroupId = s;//.substring(0, 15);
				                    lstCustomerShare.add(shareObj);
            					}   
            				}
            			}
            			else
            			{
            				BSureC_Customer_Basic_Info__Share  shareObj = new BSureC_Customer_Basic_Info__Share();
		                    shareObj.AccessLevel = 'Edit';
		                    shareObj.ParentId = cobj.Id;
		                    shareObj.UserOrGroupId = mapbacoll.get(cobj.Credit_Account__c);
		                    lstCustomerShare.add(shareObj);
            			}
            		}
            	}
            	if(lstCustomerShare != null && lstCustomerShare.size() > 0)
		    	{
		    		insert lstCustomerShare;
		    	}
            }                                                                                       
    	}
    }
    global void UpdateCollectors(map<string,string> mpCAColl)
    {
    	//SELECT Buyer_Name__c,Customer_Id__c,Customer_Name__c,IsDeleted,User_Id__c,User_Name__c,User_Profile__c 
    	//FROM BSureC_Assigned_Buyers__c
    	list<BSureC_Assigned_Buyers__c> objCollectors = new list<BSureC_Assigned_Buyers__c>();
    	BSureC_Assigned_Buyers__c objEachColl;
    	if(mpCAColl != null && mpCAColl.size() > 0)
    	{
    		//lst_customer = new List<BSureC_Customer_Basic_Info__c>();
    		list<BSureC_Customer_Basic_Info__c> lstCust = new list<BSureC_Customer_Basic_Info__c>([SELECT Id, Credit_Account__c, Customer_Name__c
    																							   FROM BSureC_Customer_Basic_Info__c
    																							   WHERE Credit_Account__c IN: mpCAColl.keySet()]);
    		if(lstCust != null && lstCust.size() > 0)
    		{
    			for(BSureC_Customer_Basic_Info__c objCust : lstCust)
    			{
    				objEachColl = new BSureC_Assigned_Buyers__c();
    				objEachColl.Customer_Id__c = objCust.Id;
    				objEachColl.User_Id__c = mpCAColl.get(objCust.Credit_Account__c);
    				objCollectors.add(objEachColl);
    			}
    		}
    		insert objCollectors;
    	}	
    }
    
    global void finish(Database.BatchableContext BC)
    {
    } 
    
    public void getUserName()
    {
    	list<User> uname = [select id,name from User where id != null AND name != null];
    	
    	if (uname != null && uname.size()>0){
    		for(User u:uname){
    			//mapUserName.put(u.id,u.name); // vidya sagar
    			mapUserName.put(u.Name,u.id);
    		}
    	}    	
    }
    public void getUserName2()
    {
    	list<User> uname = [select id,name from User where id != null AND name != null];
    	
    	if (uname != null && uname.size()>0){
    		for(User u:uname){
    			mapUserName2.put(u.id,u.name); // vidya sagar
    		}
    	}    	
    }
     public void getUserMngr(){
    	
    	list<BSureC_AdditionalAttributes__c> lstuserMngr = [SELECT id, User__c, Manager__c FROM BSureC_AdditionalAttributes__c WHERE Id != null AND User__r.IsActive = true];
    	if(lstuserMngr!=null && lstuserMngr.size()>0){
    		
    		for(BSureC_AdditionalAttributes__c um:lstuserMngr){
    			mapMngr.put(um.User__c,um.Manager__c);
    		}
    	}
        
    }
}