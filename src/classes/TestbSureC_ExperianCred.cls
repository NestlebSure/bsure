@isTest
private class TestbSureC_ExperianCred {
	static testMethod void myUnitTest() {
    	System.test.startTest();
    	
		bSureC_ExpCred__c cred = new bSureC_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		insert cred;
		
		bSureC_ExperianCred obj = new bSureC_ExperianCred();
		obj.CustomUpdate();
		
		System.test.stopTest();
	}
	
	static testMethod void myUnitTest1() {
    	System.test.startTest();
    	
		bSureC_ExpCred__c cred = new bSureC_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		
		bSureC_ExperianCred obj = new bSureC_ExperianCred();
		obj.objCred = cred;
		obj.CustomSave();
		
		System.test.stopTest();
	}
}