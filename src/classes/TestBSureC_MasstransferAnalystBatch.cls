@isTest 
private class TestBSureC_MasstransferAnalystBatch {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
       
         //Test.startTest();
         BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
         objCSettings1.Name = 'BSureC_SendMassTransferJobMail';
         objCSettings1.Parameter_Key__c = 'BSureC_SendMassTransferJobMail';
         objCSettings1.Parameter_Value__c = 'TRUE';
         insert objCSettings1;
         
         BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
         objCSettings2.Name = 'BSureC_SendMassTransferJobMailID';
         objCSettings2.Parameter_Key__c = 'BSureC_SendMassTransferJobMailID';
         objCSettings2.Parameter_Value__c = 'satish.c@vertexcs.com';
         insert objCSettings2;
            
         BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
       
        objbasicinfo.Customer_Name__c = 'SAM';
        objbasicinfo.Analyst__c = Userinfo.getUserId();
        objbasicinfo.Manager__c = Userinfo.getUserId();
        //objbasicinfo.Backup_Analysts__c= '2314';
       
        insert objbasicinfo;
              
        Set<String> s = new Set<String>();
    	s.add(objbasicinfo.Id);
      
       //List<BSureC_Credit_Data_Stage__c> objlistq = Database.query(strq);
       ////system.debug('----objlistq---'+objlistq);
       BSureC_MasstransferAnalystBatch b = new BSureC_MasstransferAnalystBatch( 'Analyst','','',null,s);

       //BSureC_MasstransferAnalystBatch b = new BSureC_MasstransferAnalystBatch( 'Analyst','','','',s); 
       Database.executebatch(b,10);
       b.SendJobStatus();
       //Test.stopTest();  
   
}
}