@isTest
private class TestbSureC_ScheduleExperianReport{
	static testMethod void myUnitTest() {
    	System.test.startTest();
		bSureC_ScheduleExperianReport objSchClass = new bSureC_ScheduleExperianReport();
		String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('testScheduledApex', CRON_EXP, new bSureC_ScheduleExperianReport() );
		string assign='1234';
        system.assertEquals('1234',assign);
        System.test.stopTest();
    }
    
	static testMethod void testBatchClass(){
        Database.BatchableContext BC;
        Test.startTest();
        
        BSureC_Customer_Basic_Info__c objBasicInfo = new BSureC_Customer_Basic_Info__c();
    	objBasicInfo.Customer_Name__c = 'Demo Customer';
    	objBasicInfo.BISFileNumber__c = '7986548975';
    	objBasicInfo.Credit_Account__c = '589647';
    	objBasicInfo.Planned_Review_Date__c = system.today()+7;
    	objBasicInfo.Risk_Category__c = '002';
    	insert objBasicInfo;
    	
    	bSureC_ExpCred__c cred = new bSureC_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		insert cred;
		
        bSureC_GenerateExperianReport b = new bSureC_GenerateExperianReport();
        ID myBatchJobID = database.executebatch(b);
        Test.stopTest();
    }
}