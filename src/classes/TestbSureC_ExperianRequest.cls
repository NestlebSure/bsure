@isTest
private class TestbSureC_ExperianRequest {
    static testMethod void myUnitTest() {
    	System.test.startTest();
    	
    	BSureC_Customer_Basic_Info__c objBasicInfo = new BSureC_Customer_Basic_Info__c();
    	objBasicInfo.Customer_Name__c = 'Demo Customer';
    	objBasicInfo.BISFileNumber__c = '7986548975';
    	objBasicInfo.Credit_Account__c = '589647';
    	objBasicInfo.Planned_Review_Date__c = system.today();
    	insert objBasicInfo;
    	
    	bSureC_ExpCred__c cred = new bSureC_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		insert cred;
		
    	DateTime d = Date.Today() ;
		string dateStr =  d.format('ddMMyy') ;
    	
    	BSureC_On_Going_Credit_Section__c objCreditInfo = new BSureC_On_Going_Credit_Section__c();
    	objCreditInfo.Customer_ID__c = objBasicInfo.id;
    	objCreditInfo.TitleDescription__c = dateStr+'_Experian_Rpt_'+objBasicInfo.Credit_Account__c;
    	insert objCreditInfo;
    	
    	bSureC_ExperianRequest objExpReq = new bSureC_ExperianRequest();
    	bSureC_ExperianRequest.generateExperianRpt(objBasicInfo.Id,objBasicInfo.BISFileNumber__c,objBasicInfo.Credit_Account__c);
    	
    	decimal de = bSureC_ExperianRequest.drop_leading_zeros('000125');
    	
    	map<String,String> mapExpAlias = new map<string,string>();
        map<String,String> mapResultBook = new map<string,string>();
        for(BsureExperian__c objBsureExper:BsureExperian__c.getAll().values()){
            if(objBsureExper!=null){
                if(objBsureExper.NodeName__c!='' && objBsureExper.NodeName__c!='' && objBsureExper.AliasName__c!=null 
                																  && objBsureExper.AliasName__c!=''){
                    mapExpAlias.put(objBsureExper.NodeName__c,objBsureExper.AliasName__c);
                }
            }
         }
         HttpResponse res = new HttpResponse();
         res.setHeader('Content-Type', 'application/xml');
		 res.setBody('<NetConnectResponse xmlns="http://www.experian.com/NetConnectResponse">'+
						'<CompletionCode>0000</CompletionCode>'+
						'<ReferenceId>PPR INQF Experian</ReferenceId>'+
						'<Products xmlns="http://www.experian.com/ARFResponse">'+
							'<PremierProfile>'+
								'<ExpandedCreditSummary>'+
									'<BankruptcyFilingCount>0000</BankruptcyFilingCount>'+
									'<TaxLienFilingCount>0000</TaxLienFilingCount>'+
									'<JudgmentFilingCount>0004</JudgmentFilingCount>'+
									'<CurrentDBT>003</CurrentDBT>'+
									'<CommercialFraudRiskIndicatorCount>0000</CommercialFraudRiskIndicatorCount>'+
								'</ExpandedCreditSummary>'+
								'<ExpandedBusinessNameAndAddress>'+
									'<ProfileDate>20120730</ProfileDate>'+
								'</ExpandedBusinessNameAndAddress>'+
								'<ExecutiveSummary>'+
									'<HighCreditAmountExtended>00000124400</HighCreditAmountExtended>'+
								'</ExecutiveSummary>'+
								'<IntelliscoreScoreInformation>'+
									'<PubliclyHeldCompany code="N"/>'+
									'<LimitedProfile code="N"/>'+
									'<Filler>3</Filler>'+
									'<ScoreInfo>'+
										'<Score>0000002900</Score>'+
									'</ScoreInfo>'+
									'<ProfileNumber>I309192566</ProfileNumber>'+
									'<ModelInformation>'+
										'<ModelCode>000223</ModelCode>'+
										'<ModelTitle>FINANCIAL STABILITY RISK</ModelTitle>'+
									'</ModelInformation>'+
									'<PercentileRanking>028</PercentileRanking>'+
									'<Action>TEST LOW-MEDIUM RISK</Action>'+
									'<RiskClass>3</RiskClass>'+
								'</IntelliscoreScoreInformation>'+
								'<IntelliscoreScoreInformation>'+
									'<PubliclyHeldCompany code="N"/>'+
									'<LimitedProfile code="N"/>'+
									'<Filler>01</Filler>'+
									'<ScoreInfo>'+
										'<Score>0000004400</Score>'+
									'</ScoreInfo>'+
									'<ModelInformation>'+
										'<ModelCode>000215</ModelCode>'+
										'<ModelTitle>INTELLISCORE PLUS V2</ModelTitle>'+
										'<CustomModelCode>01</CustomModelCode>'+
									'</ModelInformation>'+
									'<PercentileRanking>043</PercentileRanking>'+
									'<Action>MEDIUM RISK</Action>'+
								'</IntelliscoreScoreInformation>'+
								'<ScoreTrendsCreditLimit>'+
									'<CreditLimitAmount>00000175000</CreditLimitAmount>'+
								'</ScoreTrendsCreditLimit>'+
							'</PremierProfile>'+
						'</Products>'+
					'</NetConnectResponse>');
         res.setStatusCode(200);
         XmlStreamReader reader = res.getXmlStreamReader();
         mapResultBook = objExpReq.parseBooks(reader,mapExpAlias);
    	 string result =  objExpReq.InsertIntoExperain(mapResultBook,objBasicInfo.Id);
    	 string assign='1234';
         system.assertEquals('1234',assign);
        
         System.test.stopTest();
    }
}