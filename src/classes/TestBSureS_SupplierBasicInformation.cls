@isTest
public with sharing class TestBSureS_SupplierBasicInformation {
    static testMethod void myUnitTest() 
    {
    
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_CEORole123';
        objCSettings1.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings1.Parameter_Value__c = 'CEO';
        insert objCSettings1;
        system.assertEquals('CEO', objCSettings1.Parameter_Value__c);
        
        list<string> lstcountry = new list<string>();
        lstcountry.add('TEST');
        set<string> setZones = new set<string>();
        setZones.add('AOA');
        setZones.add('AFRICA');
        
        map<string,string> mapSubZnTest = new map<string,string>();
        mapSubZnTest.put('AOA','AOA');
        mapSubZnTest.put('AFRICA','AFRICA');
        
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        BSureS_Basic_Info__c parent =new BSureS_Basic_Info__c();
        parent.Contact_name__c = 'Steve';
        parent.Supplier_Name__c = 'george M';
        parent.Supplier_Category__c = scategory.id;
        //b.Group_Id__c = '12345'; 
        parent.Review_Status__c='';
        insert parent;
        
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true; 
        sSubZone.ZoneID__c = szone.Id;
        insert sSubZone; 
        
        BSureS_Country__c sCount = new BSureS_Country__c();
        sCount.Name = 'TESTCountry';
        sCount.Sub_Zone_ID__c = sSubZone.Id;
        sCount.IsActive__c = true;
        insert sCount;
        
        BSureS_State__c sObj = new BSureS_State__c();
        sObj.Name = 'test Name';
        sObj.Supplier_Country__c = sCount.id;
        insert sObj;
        
        BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
        b.Contact_name__c = 'Steve';
        b.Supplier_Name__c = 'george M';
        b.Supplier_Category__c = scategory.id;
        b.Parent_Supplier_ID__c = parent.id; 
        //b.Group_Id__c = '12345'; 
        b.Zone__c = szone.Id;
        b.Supplier_Zones__c = 'AOA,TEST2';
        b.Supplier_SubZones__c = 'AFRICA,EUROPE';
        b.Sub_Zone__c = sSubZone.Id;
        b.Supplier_Countries__c = 'TEST,TEST2';
        b.BSureS_Country__c = sCount.Id;
        b.Review_Status__c='';
        insert b;
        system.assertEquals('Steve', b.Contact_name__c);
        
        ApexPages.currentPage().getParameters().put('id',b.id); 
        system.Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(b); 
        BSureS_SupplierBasicInformation bscls = new BSureS_SupplierBasicInformation(controller);
        //bscls.strSupplierId = b.Id;
        //list<BSureS_Basic_Info__c> lb = new list<BSureS_Basic_Info__c>();
        //BSureS_Basic_Info__c bs1 = [select id from BSureS_Basic_Info__c where id = b.id ]; 
        
        //BSureS_Basic_Info__c bssup=new BSureS_Basic_Info__c();
        
        List<string>  LftZone = new List<string>();
        LftZone.add('aoa');
        LftZone.add('america');
        bscls.lstLftZone = LftZone;
        for(String s:LftZone)
        {
            bscls.setRightZonevalues.add(s);
        }
        bscls.lstRight_BSureS_Country = lstcountry;
        bscls.lstLeft_BSureS_Country = lstcountry;
        bscls.setLeftSubZonevalues = setZones;
        bscls.mapSubZone = mapSubZnTest;
        bscls.redirectDetailPage();
        bscls.leftsubzonerender=true;
        bscls.BSureS_AddAvailZone(); 
        bscls.BSureS_AddAssignZone();
        bscls.BSureS_AddAvailCountry();
        bscls.BSureS_AddAssignCountry();
        bscls.getBSureSAssignedZoneList();
        bscls.getBSureSAvailableZoneList();  
        bscls.leftsubzonerender=true;
        //bscls.blnmakerequired = true;
        //bscls.visibilityFR();
        bscls.redirect();
        set<string> setRight_BSureS_SubZoneCon =new set<string>();
        setRight_BSureS_SubZoneCon.add('aoa');
        setRight_BSureS_SubZoneCon.add('aoa1');
        bscls.setrightzonevalues=setRight_BSureS_SubZoneCon;
        //bscls.setrightZonevalues.add('Africa');
        //bscls.setleftZonevalues.add('egypt');
        //bscls.setleftSubZonevalues.add('North America');
        bscls.setrightSubZonevalues.add('Latin America');
        bscls.getBSureSAvailableSubZoneList();
        bscls.getBSureSAssignedSubZoneList();
        bscls.BSureS_AddAvailSubZone(); 
        bscls.BSureS_AddAssignSubZone();
        bscls.visibilityField();
        bscls.getSZoneList();
        bscls.getSSubZoneList();
        bscls.getSCountryList(); 
        bscls.getSStateList();
        
         bscls.BSureSupplierInfo.Supplier_Name__c = 'test Supplier';
        bscls.BSureSupplierInfo.Planned_Review_Date__c = date.today();
        bscls.BSureSupplierInfo.Supplier_Category__c = scategory.id;
        bscls.BSureSupplierInfo.IsPotential_Supplier__c = true;    
        
        bscls.BSureSupplierInfo.Review_Status__c = 'Started';
        bscls.BSureSupplierInfo.Financial_Information__c = 'Yes';
        bscls.BSureSupplierInfo.Fiscal_Year_End__c = date.today();
        bscls.BSureSupplierInfo.Last_Financial_Statement_Received__c = date.today();
        
        bscls.BSureSupplierInfo.Street_Name_Address_Line_1__c = 'Add line 1';
        //bscls.strSCICountryId = sCount.Id;
        //bscls.strState = sObj.id;
        bscls.BSureSupplierInfo.City__c = 'test City';
           
        bscls.BSureS_SupplierSave();
        List<string> lstLeft_BSureS_Zone =new List<string>();
        lstLeft_BSureS_Zone.add('aoa');
        lstLeft_BSureS_Zone.add('aoa1');
        bscls.lstLeft_BSureS_Zone=lstLeft_BSureS_Zone;
        bscls.BSureS_AddAvailZone();
        List<string> lstRight_BSureS_Zone =new List<string>();
        lstRight_BSureS_Zone.add('aoa');
        lstRight_BSureS_Zone.add('aoa1');
        bscls.lstRight_BSureS_Zone=lstRight_BSureS_Zone;
        bscls.BSureS_AddAssignZone();
        List<string> lstLeft_BSureS_SubZone =new List<string>();
        lstLeft_BSureS_SubZone.add('aoa');
        lstLeft_BSureS_SubZone.add('aoa1');
        bscls.lstLeft_BSureS_SubZone=lstLeft_BSureS_SubZone;
        bscls.BSureS_AddAvailSubZone();
        List<string> lstRight_BSureS_SubZone =new List<string>();
        lstRight_BSureS_SubZone.add('aoa');
        lstRight_BSureS_SubZone.add('aoa1');
        bscls.lstRight_BSureS_SubZone=lstRight_BSureS_SubZone;
        bscls.BSureS_AddAssignSubZone();
        set<string> setRight_BSureS_SubZone1 =new set<string>();
        setRight_BSureS_SubZone1.add('aoa');
        setRight_BSureS_SubZone1.add('aoa1');
        bscls.setrightzonevalues=setRight_BSureS_SubZone1;
        bscls.getBSureSAssignedZoneList();
        bscls.showErrorMessage('test data'); 
        //b.Group_Id__c = null;
        //update b;
        //bscls.BSureS_SupplierSave();
        system.Test.stopTest();
        
    }
    static testMethod void myUnitTest2()
    {
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_CEORole88';
        objCSettings1.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings1.Parameter_Value__c = 'CEO';
        insert objCSettings1;
        system.assertEquals('CEO', objCSettings1.Parameter_Value__c);
        
        BSureS_Category__c objCateg = new BSureS_Category__c();
        objCateg.Name = 'Biscuit';
        //objCateg.Category_ID__c = 'CA001';
        insert objCateg; 
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true; 
        sSubZone.ZoneID__c = szone.Id;
        insert sSubZone; 
        
        BSureS_Country__c sCount = new BSureS_Country__c();
        sCount.Name = 'TESTCountry';
        sCount.Sub_Zone_ID__c = sSubZone.Id;
        sCount.IsActive__c = true;
        insert sCount;
        
        BSureS_State__c sObj = new BSureS_State__c();
        sObj.Name = 'test Name';
        sObj.Supplier_Country__c = sCount.id;
        insert sObj;
        
        BSureS_Basic_Info__c b1=new BSureS_Basic_Info__c();
        b1.Contact_name__c = 'Steve';
        b1.Supplier_Name__c = 'george M';
        b1.Planned_Review_Date__c = date.today();
        b1.IsPotential_Supplier__c = true;
        insert b1;
        system.assertEquals('Steve',b1.Contact_name__c);
        
        system.Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(b1);  
        BSureS_SupplierBasicInformation b = new BSureS_SupplierBasicInformation(controller);
        b.BSureSupplierInfo.Supplier_Name__c = 'test Supplier';
        b.BSureSupplierInfo.Planned_Review_Date__c = date.today();
        b.BSureSupplierInfo.Supplier_Category__c = objCateg.Id;
        b.BSureSupplierInfo.IsPotential_Supplier__c = true;
        
        b.BSureSupplierInfo.Review_Status__c = 'Started';
        b.BSureSupplierInfo.Financial_Information__c = 'Yes';
        b.BSureSupplierInfo.Fiscal_Year_End__c = date.today();
        b.BSureSupplierInfo.Last_Financial_Statement_Received__c = date.today();
        
        b.BSureSupplierInfo.Street_Name_Address_Line_1__c = 'Add line 1';
        b.strSCICountryId = sCount.Id;
        b.strState = sObj.id;
        b.BSureSupplierInfo.City__c = 'test City';
        
        
        Set<string> setRightZonevalues1 = new Set<string>();
        setRightZonevalues1.add('AoA');
        setRightZonevalues1.add('America');
        b.setRightZonevalues = setRightZonevalues1;
        Set<string> setRightSubZonevalues1 = new Set<string>();
        setRightSubZonevalues1.add('AoA');
        setRightSubZonevalues1.add('America');
        b.setRightSubZonevalues = setRightSubZonevalues1;
        Set<string> setRightCountryValues1 = new Set<string>();
        setRightCountryValues1.add('AoA');
        setRightCountryValues1.add('America');
        b.setRightCountryValues = setRightCountryValues1;
        system.Test.stopTest();
       // b.BSureS_SupplierSave(); 
    } 
     
}