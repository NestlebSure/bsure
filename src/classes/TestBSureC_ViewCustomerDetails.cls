@isTest
public with sharing class TestBSureC_ViewCustomerDetails {
    //public Id currentUserId{get;set;}
    
    static testMethod void myUnittest()
    {
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Zone Manager'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name Like 'BSureC_Manager'];
        
        User objUser = new User(Alias = 'PKnox', 
                                Email='nestlelocal@vertexcs.com', 
                                EmailEncodingKey='UTF-8', 
                                LastName='Analyst_CustUser', 
                                LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', 
                                ProfileId = objProf.Id, 
                                UserRoleId = objRole.Id, 
                                TimeZoneSidKey='America/Los_Angeles', 
                                UserName='cust_analyst@bsurenestle.com');
        system.runAs(objUser)
        {       
                
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        User standard = new User(
                alias = 'standts',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', 
                languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Americas34',IsActive__c=true);
        insert ZoneRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America34',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United State123',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;        
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state456456',Customer_Country__c=ConRef.id,IsActive__c=true);
        insert StateRef;       
       
        System.Assertequals('Test state456456',StateRef.name);
        Id currentUserId; 
        currentUserId = UserInfo.getUserId();
        BSureC_Customer_Basic_Info__c b=new BSureC_Customer_Basic_Info__c();
        b.Contact_Name__c = 'Steves';
        b.Customer_Name__c = standard.Name;
        b.Analyst__c = UserInfo.getUserId();
        b.Manager__c = UserInfo.getUserId();
        b.Backup_Analysts__c = UserInfo.getUserId();
        b.Zone__c = ZoneRef.id; 
        b.Sub_Zone__c = SubZoneRef.id;
        b.Country__c = ConRef.id;
        b.State__c = StateRef.id;       
        insert b;
        system.Assertequals('Steves',b.Contact_Name__c);
        BSureC_Zone_Manager__c zonemgr = new BSureC_Zone_Manager__c();
        zonemgr.Zone_Manager__c = UserInfo.getUserId();
        zonemgr.Zone__c = ZoneRef.Id;
        insert zonemgr;
        
        //BSureC_Sub_Zone_Manager__c subzonemgr = new BSureC_Sub_Zone_Manager__c();
        //subzonemgr.Sub_Zone_Manager__c = UserInfo.getUserId();
        //subzonemgr.Sub_Zone__c = SubZoneRef.id;
        //insert subzonemgr;
        
        //BSureC_Country_Manager__c counzonemgr = new BSureC_Country_Manager__c();
        //counzonemgr.Country_Manager__c = UserInfo.getUserId();
        //counzonemgr.Country__c = ConRef.Id;
        //insert  counzonemgr;
        
        Apexpages.currentPage().getParameters().put('id',b.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(b);
        BSureC_ViewCustomerDetails sviewObj= new BSureC_ViewCustomerDetails(controller);
        sviewObj.currentUserId = standard.id;
        sviewObj.getCustomerDetails();             
        }
        
    }
    
static testMethod void myUnittest1()
    {
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Sub-Zone Manager'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name Like 'BSureC_Manager'];
        
        User objUser = new User(Alias = 'PKnox', 
                                Email='nestlelocal@vertexcs.com', 
                                EmailEncodingKey='UTF-8', 
                                LastName='Analyst_CustUser', 
                                LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', 
                                ProfileId = objProf.Id, 
                                UserRoleId = objRole.Id, 
                                TimeZoneSidKey='America/Los_Angeles', 
                                UserName='cust_analyst@bsurenestle.com');
        system.runAs(objUser)
        {       
                
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        User standard = new User(
                alias = 'standts',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', 
                languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Americas34',IsActive__c=true);
        insert ZoneRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America34',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United State123',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;        
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state456456',Customer_Country__c=ConRef.id,IsActive__c=true);
        insert StateRef;       
       
        System.Assertequals('Test state456456',StateRef.name);
        Id currentUserId; 
        currentUserId = UserInfo.getUserId();
        BSureC_Customer_Basic_Info__c b=new BSureC_Customer_Basic_Info__c();
        b.Contact_Name__c = 'Steves';
        b.Customer_Name__c = standard.Name;
        b.Analyst__c = UserInfo.getUserId();
        b.Manager__c = UserInfo.getUserId();
        b.Backup_Analysts__c = UserInfo.getUserId();
        b.Zone__c = ZoneRef.id; 
        b.Sub_Zone__c = SubZoneRef.id;
        b.Country__c = ConRef.id;
        b.State__c = StateRef.id;       
        insert b;
        system.Assertequals('Steves',b.Contact_Name__c);
        //BSureC_Zone_Manager__c zonemgr = new BSureC_Zone_Manager__c();
        //zonemgr.Zone_Manager__c = UserInfo.getUserId();
        //zonemgr.Zone__c = ZoneRef.Id;
        //insert zonemgr;
        
        BSureC_Sub_Zone_Manager__c subzonemgr = new BSureC_Sub_Zone_Manager__c();
        //subzonemgr.Sub_Zone_Manager__c = UserInfo.getUserId();
        //subzonemgr.Vertex_BSureC__Sub_Zone_Manager__c=UserInfo.getUserId();       
        subzonemgr.Sub_Zone__c = SubZoneRef.id;
        insert subzonemgr;
        
        //BSureC_Country_Manager__c counzonemgr = new BSureC_Country_Manager__c();
        //counzonemgr.Country_Manager__c = UserInfo.getUserId();
        //counzonemgr.Country__c = ConRef.Id;
        //insert  counzonemgr;
        
        Apexpages.currentPage().getParameters().put('id',b.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(b);
        BSureC_ViewCustomerDetails sviewObj= new BSureC_ViewCustomerDetails(controller);
        sviewObj.currentUserId = standard.id;
        sviewObj.getCustomerDetails();             
        }
        
    }

static testMethod void myUnittest2()
    {
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Country Manager'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name Like 'BSureC_Manager'];
        
        User objUser = new User(Alias = 'PKnox', 
                                Email='nestlelocal@vertexcs.com', 
                                EmailEncodingKey='UTF-8', 
                                LastName='Analyst_CustUser', 
                                LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', 
                                ProfileId = objProf.Id, 
                                UserRoleId = objRole.Id, 
                                TimeZoneSidKey='America/Los_Angeles', 
                                UserName='cust_analyst@bsurenestle.com');
        system.runAs(objUser)
        {       
                
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        User standard = new User(
                alias = 'standts',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', 
                languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Americas34',IsActive__c=true);
        insert ZoneRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America34',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United State123',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;        
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state456456',Customer_Country__c=ConRef.id,IsActive__c=true);
        insert StateRef;       
       
        System.Assertequals('Test state456456',StateRef.name);
        Id currentUserId; 
        currentUserId = UserInfo.getUserId();
        BSureC_Customer_Basic_Info__c b=new BSureC_Customer_Basic_Info__c();
        b.Contact_Name__c = 'Steves';
        b.Customer_Name__c = standard.Name;
        b.Analyst__c = UserInfo.getUserId();
        b.Manager__c = UserInfo.getUserId();
        b.Backup_Analysts__c = UserInfo.getUserId();
        b.Zone__c = ZoneRef.id; 
        b.Sub_Zone__c = SubZoneRef.id;
        b.Country__c = ConRef.id;
        b.State__c = StateRef.id;       
        insert b;
        system.Assertequals('Steves',b.Contact_Name__c);
        //BSureC_Zone_Manager__c zonemgr = new BSureC_Zone_Manager__c();
        //zonemgr.Zone_Manager__c = UserInfo.getUserId();
        //zonemgr.Zone__c = ZoneRef.Id;
        //insert zonemgr;
        
        //BSureC_Sub_Zone_Manager__c subzonemgr = new BSureC_Sub_Zone_Manager__c();
        //subzonemgr.Sub_Zone_Manager__c = UserInfo.getUserId();
        //subzonemgr.Sub_Zone__c = SubZoneRef.id;
        //insert subzonemgr;
        
        BSureC_Country_Manager__c counzonemgr = new BSureC_Country_Manager__c();
        counzonemgr.Country_Manager__c = UserInfo.getUserId();
        counzonemgr.Country__c = ConRef.Id;
        insert  counzonemgr;
        
        Apexpages.currentPage().getParameters().put('id',b.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(b);
        BSureC_ViewCustomerDetails sviewObj= new BSureC_ViewCustomerDetails(controller);
        sviewObj.currentUserId = standard.id;
        sviewObj.getCustomerDetails();             
        }
        
    }

    

}