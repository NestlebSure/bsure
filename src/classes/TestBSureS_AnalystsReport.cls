/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @isTest (SeeAllData=true)
public with sharing class TestBSureS_AnalystsReport 
{
    static testMethod void myUnitTest()
    {
    	
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        BSureS_SubZone__c SSubZone = new BSureS_SubZone__c();
        SSubZone.Name = 'Test1 SubZone';
        SSubZone.IsActive__c = true;
        SSubZone.ZoneID__c = szone.id;
        insert SSubZone;
        
        BSureS_Country__c SCountry = new BSureS_Country__c();
        SCountry.Name = 'Test1 Country';
        SCountry.IsActive__c = true;
        SCountry.Sub_Zone_ID__c = SSubZone.id;
        insert SCountry;
        
        BSureS_User_Additional_Attributes__c objUseradd = new BSureS_User_Additional_Attributes__c();
        objUseradd.User__c = standard.id;
        objUseradd.BSureS_Zone__c = szone.id;
        objUseradd.BSureS_Sub_Zone__c = sSubZone.id;
        objUseradd.BSureS_Country__c = sCountry.id;
        insert objUseradd;
        
        BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_Report_Page_Size1';
        objCSettingsB.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsB.Parameter_Value__c = '10';
        insert objCSettingsB;
        
        BSureS_Basic_Info__c SupplObj=new BSureS_Basic_Info__c();
        SupplObj.Supplier_Category__c = scategory.Id;
        SupplObj.Contact_name__c = 'Steve';
        SupplObj.Supplier_Name__c = 'george M';
        SupplObj.Has_Parent__c = true;
        SupplObj.IsPotential_Supplier__c = true;
        SupplObj.Zone__c = szone.id; 
        SupplObj.Sub_Zone__c = sSubZone.id;
        SupplObj.BSureS_Country__c = sCountry.Id;
        SupplObj.Analyst__c = standard.id;
        SupplObj.Manager__c = standard.id;
        SupplObj.Backup_Analysts__c = standard.id;
        SupplObj.Next_Review_Date__c = date.today();
        insert SupplObj; 
        system.assertEquals('Steve',SupplObj.Contact_name__c);
       
        system.Test.startTest();
        BSureS_AnalystsReport anlyObj = new BSureS_AnalystsReport();
        BSureS_AnalystsReport.SupplierWrapperList wrpObj = new BSureS_AnalystsReport.SupplierWrapperList();
        anlyObj.Search();
        anlyObj.getAnalysts();
        anlyObj.getSupplierInfo(SupplObj.Analyst__c);
        
        
        anlyObj.BindData(1);
        anlyObj.LastpageData(1);
        anlyObj.getPageNumber();
        anlyObj.getPageSize();
        anlyObj.getPreviousButtonEnabled();
        //anlyObj.nextBtnClick();
        //anlyObj.previousBtnClick();
        anlyObj.getPageNumber();
        anlyObj.getPageSize();
        anlyObj.getPreviousButtonEnabled();
        anlyObj.getNextButtonDisabled();
        anlyObj.getTotalPageNumber();
        
        anlyObj.LastbtnClick();
        anlyObj.FirstbtnClick();
        system.Test.stopTest();
        
    }
}