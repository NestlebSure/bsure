public class BSure_SiteForgotPassword {
    public static string username {set;get;}
   // public static String valMsg { get; set; }
   // public static contact contactrec;
   // public static boolean isValMsg {set;get;}
    public BSure_SiteForgotPassword () {
    }
    
    @RemoteAction
    public static String sendEmailToUser(String UName){
    	system.debug('@@@UName'+UName);
    	String StatusResult;
        if(UName !=null && UName!=''){
        List<contact> contactrecs = [select email,Password__c,Name from contact where email=:UName];
        if(contactrecs.size()>0){
            if(contactrecs[0].Password__c!=null && contactrecs[0].Password__c!=''){
               contact contactrec = contactrecs[0];
                List<string> lstToAdd = new List<String>();
                lstToAdd.add(UName);
                 EmailTemplate emailTemplate=[select id, Subject,name,HtmlValue from EmailTemplate where name='Forgot Password Template'];
                  List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    string body = emailTemplate.HtmlValue;
                    body=body.replace('{!Contact.LastName}',contactrec.Name);
                    body=body.replace('{!Contact.Email}',contactrec.Email);
                    body=body.replace('{!Contact.Password__c}',contactrec.Password__c);
                    string subject = emailTemplate.Subject;
                    mail.setSenderDisplayName('bSure Support');
                    mail.setSubject(subject);
                    mail.setHtmlBody(body);
                    mail.SetToAddresses(lstToAdd);       
                    mail.setSaveAsActivity(false);
                    emails.add(mail); 
                      if(emails.size()>0){
                      List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);
                      if (results.get(0).isSuccess()) {
                      	 StatusResult= 'true';
                       }
                      }
                 }
                 else{
                       StatusResult= 'Password Not Exist';
                   }
            }
            else{
               StatusResult = 'Invalid User Email';
            }
          }
          return StatusResult;
    }
    
}