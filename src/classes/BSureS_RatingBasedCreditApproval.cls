global with sharing class BSureS_RatingBasedCreditApproval {

 public BSureS_Credit_Analysis__c CAInfo{get;set;} //instance of Object
 public list<BSureS_Credit_Analysis__c> lstcreditA{get;set;}
 public list<BSureS_Credit_Analysis__c> lstSelectedRecords{get;set;}
 public list<BSureS_Credit_Analysis__c> lstSelectedApprove{get;set;}
 public List<CreditWrapper> listCreditWrap{get;set;}
 public CreditWrapper creditWrap {get;set;}
 public boolean displayrecords {get;set;}
    public BSureS_RatingBasedCreditApproval(ApexPages.StandardController controller) {
    displayrecords=false;
lstcreditA =new list<BSureS_Credit_Analysis__c>();
lstSelectedRecords=new list<BSureS_Credit_Analysis__c>();
lstSelectedApprove =new list<BSureS_Credit_Analysis__c>();
CAInfo =new BSureS_Credit_Analysis__c();
//creditWrap  CreditW=new CreditWrapper();
listCreditWrap =new List<CreditWrapper>();
string id= UserInfo.getUserId() ;
system.debug('Current User Id' + id);
    }
    public void Approve(){
     
    listCreditWrap.clear();
    for(BSureS_Credit_Analysis__c creditA : [select id,name,Review_Name__c,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Rating__c,Risk_Level__c,Spend__c,Review_Status__c from BSureS_Credit_Analysis__c where Rating__c =:CAInfo.Rating__c AND Review_Status__c='Pending Approval'])
    {
       listCreditWrap.add(New CreditWrapper(creditA));
       
    }
    if(listCreditWrap.size()>0 && listCreditWrap!=Null){
   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Records with this Rating'));
    system.debug('listCreditWrap1stif====>' + listCreditWrap.size());
    system.debug('listCreditWrap1ssRecord====>' + listCreditWrap);
        displayrecords =true;}
        
       if(listCreditWrap.size()==0){
             
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Sorry! No Records found with this Rating'));
            
             displayrecords =false;
              
             system.debug('listCreditWrap2ndif====>' + listCreditWrap);
             system.debug('listCreditWrap2ndRecord====>' + listCreditWrap);
         }
    }
   
      public pageReference approveSelected(){
      
      if(listCreditWrap!=null){
      
      
      for(CreditWrapper creditA :  listCreditWrap){
      if(creditA.selected==true){
        
        lstSelectedRecords.add(creditA.CA);
        }
      }
      
      }
      
      string CAids='';
      if(lstSelectedRecords  !=Null){
      for(BSureS_Credit_Analysis__c ids :lstSelectedRecords ){
      CAids+=ids.id;
      
      }
      }
      system.debug('CAids=====>' + CAids);
      system.debug('lstSelectedRecords =====>' + lstSelectedRecords);
       system.debug('lstSelectedRecords size()=====>' + lstSelectedRecords.size());
    
    pagereference ref = new pagereference('/apex/BSureS_RatingBasedCreditApproval_2?value='+CAids);
    ref.setredirect(true);
     String url = apexpages.currentpage().getparameters().get('value');
            List<String>  ids ;
        if(CAids!=null){
        
        ids = CAids.split(':');
        }
                     
       system.debug('ids====>' + ids );
        lstSelectedApprove= [select id,name,Review_Name__c,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Rating__c,Risk_Level__c,Spend__c,Review_Status__c from BSureS_Credit_Analysis__c where  id in : ids ];
        system.debug('lstSelectedApprove' + lstSelectedApprove);
        
       // return lstSelectedApprove;
    return ref;
      

      
      }  
    
        // ----------------
     //List<BSureS_Credit_Analysis__c> lsttest = new List<BSureS_Credit_Analysis__c>();
    
   
    
    
    

public class CreditWrapper{
public boolean selected{get;set;}
public BSureS_Credit_Analysis__c CA {get;set;}

public CreditWrapper(BSureS_Credit_Analysis__c CreditA){
CA=CreditA;
selected=false;

}

    }

}