/***********************************************************************************************
* Controller Name   : BSureS_Create_SpendHistoryPublish 
* Date              : 20/12/2012
* Author            : Praveen S
* Purpose           : To create the Spend History Publish
* Change History    : 
*                           Date           Programmer         Reason
* -------------------- ------------------- -------------------------
*                       20/12/2012      Praveen Sappati     Initial Version
*                       07/19/2013      Kishorekumar A      Added Security Review condtions
*                       13/08/2013      B.Anupreethi        Code revie changes
**************************************************************************************************/
global with sharing class BSureS_Create_SpendHistoryPublish 
{
    public BSureS_Spend_History_Publish__c ObjSpendhistorypublish{get;set;}//publish object
    public String str_suppliername{get;set;}//holds the supplier name
    public String  Objspendid;
    Map<Date,String> map_val_spendata_publish = new Map<Date,String>();//initializing map 
    //Map<String,Id> map_update_supplierid = new Map<String,Id>();//initializing map
     transient Map<String,Id> map_update_supplierid = new Map<String,Id>();//initializing map
    public BSureS_Create_SpendHistoryPublish(ApexPages.StandardController controller) 
    {
        Objspendid=Apexpages.currentPage().getParameters().get('id');//Holds Spend History publish record ID
        String Objid=Apexpages.currentPage().getParameters().get('recid');//Holds supplier record ID
        String supname=Apexpages.currentPage().getParameters().get('sname');//Holds supplier Name
        ObjSpendhistorypublish=new BSureS_Spend_History_Publish__c();
         //Anu Check if the user has read access on the BSureS_Basic_Info__c,BSureS_Spend_History_Publish__c object
        if(Schema.sObjectType.BSureS_Basic_Info__c.isAccessible() && Schema.sObjectType.BSureS_Spend_History_Publish__c.isAccessible())    
        {
            if(Objid!=NULL)
            {
                BSureS_Basic_Info__c ObjSupplier=[select id,Globe_ID__c,Supplier_Name__c  from BSureS_Basic_Info__c where id=:Objid];
                if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Supplier_ID__c.isCreateable()){
                    ObjSpendhistorypublish.Supplier_ID__c=ObjSupplier.id;
                }
                if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Globe_ID__c.isCreateable()){   
                    ObjSpendhistorypublish.Globe_ID__c=ObjSupplier.Globe_ID__c;
                }   
            }
            if(Objspendid!=NULL)
            {   
                ObjSpendhistorypublish=[select id,Date__c,Globe_ID__c,Spend_Amount__c,Spend_Period__c,Supplier_ID__c,Supplier_Name__c FROM BSureS_Spend_History_Publish__c WHERE id=:Objspendid];
                BSureS_Basic_Info__c ObjSupplier=[select id,Globe_ID__c,Supplier_Name__c  from BSureS_Basic_Info__c where id=:ObjSpendhistorypublish.Supplier_ID__c];
            }
    
            
            
            for(BSureS_Spend_History_Publish__c spendhistory_data_publish : [SELECT Globe_ID__c,date__c FROM BSureS_Spend_History_Publish__c limit 50000])
            {
                map_val_spendata_publish.put(spendhistory_data_publish.date__c,spendhistory_data_publish.Globe_ID__c);//adding date and GroupID to the list map.
            }
            
            for(BSureS_Basic_Info__c reccustinfo : [SELECT Id,Globe_ID__c FROM BSureS_Basic_Info__c limit 50000])
            {
                map_update_supplierid.put(reccustinfo.Globe_ID__c,reccustinfo.Id);//adding groupID and supplier basic info recordID to the list map.
            }
        }

    }
    
    public pagereference Save()
    {
         //Anu Check if the user has read access on the BSureS_Spend_History_Publish__c object
        if(!Schema.sObjectType.BSureS_Spend_History_Publish__c.isAccessible())    
        {
            return null;
        }
        boolean bool_flag=false;
        pagereference pageRef;
        
        if(map_val_spendata_publish!=NULL)
        {   
            if(ObjSpendhistorypublish.Globe_ID__c==map_val_spendata_publish.get(ObjSpendhistorypublish.Date__c))//checking if with same date and Globe ID any record in spend history publish object.
            {
                //spendhistory.addError('*********Spend History for the date of this supplier already exists******');//adding error message to show on page.
                bool_flag=true;
                //system.debug('bool_flag===============insideif============='+bool_flag);
            } 
        }
            Database.SaveResult spendins;
            BSureS_Spend_History_Publish__c ObjSpendPublish=new BSureS_Spend_History_Publish__c();
            if(Objspendid==NULL)
            {
                if(bool_flag==false && ObjSpendhistorypublish.Globe_ID__c!=null)
                {
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Date__c.isCreateable()){
                        ObjSpendPublish.Date__c=ObjSpendhistorypublish.Date__c;
                    }   
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Globe_ID__c.isCreateable()){
                        ObjSpendPublish.Globe_ID__c=ObjSpendhistorypublish.Globe_ID__c;
                    }   
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Amount__c.isCreateable()){
                        ObjSpendPublish.Spend_Amount__c=ObjSpendhistorypublish.Spend_Amount__c;
                    }
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Period__c.isCreateable()){   
                        ObjSpendPublish.Spend_Period__c=ObjSpendhistorypublish.Spend_Period__c;
                    }   
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Supplier_ID__c.isCreateable()){
                        ObjSpendPublish.Supplier_ID__c=ObjSpendhistorypublish.Supplier_ID__c;
                    }   
                try
                {
                    if(BSureS_Spend_History_Publish__c.sObjectType.getDescribe().isCreateable()){
                        spendins=Database.Insert(ObjSpendPublish);
                    }   
                }
                catch(Exception e)
                {
                    //system.debug('e============='+e);
                }
                if(spendins.isSuccess())
                {
                    pageRef=new pagereference('/apex/Bsures_viewSupplierDetails?Id='+ObjSpendhistorypublish.Supplier_ID__c);
                }
                }
                else
                {
                    if(bool_flag==true && ObjSpendhistorypublish.Globe_ID__c!=null)
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BSureS_spendhiserrormssgalert);
                        ApexPages.addMessage(myMsg);
                    }
                    if(ObjSpendhistorypublish.Globe_ID__c==null)
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Globe ID is not assigned to this supplier');
                        ApexPages.addMessage(myMsg);
                    }
                }
            }
            else
            {
                BSureS_Spend_History_Publish__c ObjSpend=[select id,Date__c,Globe_ID__c,Spend_Amount__c,Spend_Period__c,Supplier_ID__c,Supplier_Name__c FROM BSureS_Spend_History_Publish__c WHERE id=:Objspendid];
                if(ObjSpend.Date__c==ObjSpendhistorypublish.Date__c)
                {
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Date__c.isUpdateable()){
                        ObjSpend.Date__c=ObjSpendhistorypublish.Date__c;
                    }
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Amount__c.isUpdateable()){   
                        ObjSpend.Spend_Amount__c=ObjSpendhistorypublish.Spend_Amount__c;
                    }
                    if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Period__c.isUpdateable()){   
                        ObjSpend.Spend_Period__c=ObjSpendhistorypublish.Spend_Period__c;
                    }   
                    if(BSureS_Spend_History_Publish__c.sObjectType.getDescribe().isUpdateable()){
                        spendins=Database.update(ObjSpend);
                    }   
                    if(spendins.isSuccess())
                    {
                        pageRef=new pagereference('/apex/Bsures_viewSupplierDetails?Id='+ObjSpend.Supplier_ID__c);
                    }
                    
                }
                else
                {
                    if(ObjSpendhistorypublish.Globe_ID__c==map_val_spendata_publish.get(ObjSpendhistorypublish.Date__c))
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BSureS_spendhiserrormssgalert);
                        ApexPages.addMessage(myMsg);
                    }
                    else
                    {
                        if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Date__c.isUpdateable()){
                            ObjSpend.Date__c=ObjSpendhistorypublish.Date__c;
                        }   
                        if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Amount__c.isUpdateable()){
                            ObjSpend.Spend_Amount__c=ObjSpendhistorypublish.Spend_Amount__c;
                        }   
                        if(Schema.sObjectType.BSureS_Spend_History_Publish__c.fields.Spend_Period__c.isUpdateable()){
                            ObjSpend.Spend_Period__c=ObjSpendhistorypublish.Spend_Period__c;
                        }   
                        if(BSureS_Spend_History_Publish__c.sObjectType.getDescribe().isUpdateable()){
                            spendins=Database.update(ObjSpend);
                        }    
                        if(spendins.isSuccess())
                        {
                            pageRef=new pagereference('/apex/Bsures_viewSupplierDetails?Id='+ObjSpend.Supplier_ID__c);
                        }
                    }
                }
                
            }

        return pageRef;
    }
    
    public pagereference Cancel()
    {
        pagereference pageRef=new pagereference('/apex/Bsures_viewSupplierDetails?Id='+ObjSpendhistorypublish.Supplier_ID__c);
        return pageRef;
    }
    
    public pagereference redirectSupplier()
    {
        String Objid=Apexpages.currentPage().getParameters().get('supid');
        //system.debug('Objid=================='+Objid);
        pagereference pageRef=new Pagereference('/apex/Bsures_viewSupplierDetails?Id='+Objid);
        return pageRef;
    }

}