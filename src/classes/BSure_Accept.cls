public with sharing class BSure_Accept 
{
    public string strSessionID{get;set;}
    public string sessCokid{get;set;}
    public string strC{get;set;}
    public Cookie myCookies{get;set;}
    
    public BSure_Accept()
    {
        strSessionID = UserInfo.getSessionId();
    }
    
    public void SaveException(string strClassName, string strModName, string strStackTrace, string strMessage,
    						  string strCause, string strTypeName, string strLoginId)
    {
    	BSureC_ErrorLog__c objErrorLog = new BSureC_ErrorLog__c();
    	objErrorLog.Class_Name__c = strClassName;
		objErrorLog.Method_Name__c = strModName;
		objErrorLog.Error_Cause__c = strCause;
		objErrorLog.Error_Message__c = strMessage;
		objErrorLog.Error_StackTrace__c = strStackTrace;
		objErrorLog.Error_TypeName__c = strTypeName;
		objErrorLog.Login_User_Id__c = strLoginId;
		
    	insert objErrorLog;
    }
}