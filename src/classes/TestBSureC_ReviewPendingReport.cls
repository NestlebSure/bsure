/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_ReviewPendingReport {

    static testMethod void myUnitTest() 
    {
         // TO DO: implement unit test
         BSureC_Customer_Basic_Info__c objcust = new BSureC_Customer_Basic_Info__c(Contact_name__c='KingFisher');    	
	   		insert objcust;
	   		BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
	        insert Bzone;  
	        
	        
	        Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
	        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
	        BSzone.Name = 'Test customer Sub Zone';
	        BSzone.ZoneID__c = Bzone.Id;
	        insert BSzone;
	        
	         list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
	        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
	        cSettings.Name = 'BSureC_Report_Page_Size33';
	        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size33';
	        cSettings.Parameter_Value__c = '10';
	        lstSettings.add(cSettings);
	        insert lstSettings;
	        
	        system.assertEquals('Test customer Sub Zone',BSzone.Name);
	        
          BSureC_ReviewsPendingReport objBSureC_ReviewRpt=new BSureC_ReviewsPendingReport();
            Date dtYear = Date.Today().addDays(-365);           
	        Date dtToday = Date.Today();        
	        objBSureC_ReviewRpt.strFromDate = dtYear.format();
	        objBSureC_ReviewRpt.strToDate =  dtToday.format();
	        objBSureC_ReviewRpt.showCollectorDetails();
	        objBSureC_ReviewRpt.searchCollector();
	        objBSureC_ReviewRpt.closepopupColl();
	        objBSureC_ReviewRpt.closepopupCollector();
	        objBSureC_ReviewRpt.cancel();
           objBSureC_ReviewRpt.getSortDirection();
		   objBSureC_ReviewRpt.setSortDirection('Asc');
		   objBSureC_ReviewRpt.Sort();
	       objBSureC_ReviewRpt.showBuyerDetails();
	       objBSureC_ReviewRpt.closepopup();
	       objBSureC_ReviewRpt.searchBtn();
	       objBSureC_ReviewRpt.closepopupBtn();
	       objBSureC_ReviewRpt.BindData(1);
	       
	       objBSureC_ReviewRpt.PrevPageNumber=1;
		   objBSureC_ReviewRpt.NxtPageNumber=2;
	       objBSureC_ReviewRpt.nextBtnClick();
	       //objBSureC_ReviewRpt.previousBtnClick();
	       objBSureC_ReviewRpt.getPageNumber();
	       objBSureC_ReviewRpt.getPageSize();
	       objBSureC_ReviewRpt.getPreviousButtonEnabled();
	       objBSureC_ReviewRpt.getNextButtonDisabled();
	       objBSureC_ReviewRpt.getTotalPageNumber();
	       objBSureC_ReviewRpt.pageData(2);
	       objBSureC_ReviewRpt.LastpageData(2);
	       objBSureC_ReviewRpt.FirstbtnClick();
	       objBSureC_ReviewRpt.LastbtnClick();
	       objBSureC_ReviewRpt.ExportCSV();
	       objBSureC_ReviewRpt.previousBtnClick();
    }
}