public class CustomPagination {
    
    //List to store all the contacts according to requirement
    private List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
    
    //List to show the limited records on the page
    public List<BSureS_Basic_Info__c> ContactsToShow{get;set;}
    
    //Navigation variables
    Integer counter = 0;//TO track the number of records parsed
    Integer limitSize = 10;//Number of records to be displayed
    Integer totalSize =0; //To Store the total number of records available
    
    //Constructor
    public CustomPagination(){
        
         ContactsToShow = new list<BSureS_Basic_Info__c>();
        
        //Get all the contacts which are from Public realations lead source
        //AllContacts = [select id , name, LeadSource from contact where LeadSource='Public Relations'];
        string strQuery = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c, '+ 
                        'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
                        'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
                        ', Owner_Ship__c ,Contact_Name__c '+
                        ' from BSureS_Basic_Info__c where id != null ';
		AllContacts = Database.query(strQuery);
        //Store the total size
        totalSize = AllContacts.size();
        system.debug('totalSize===='+totalSize);
        //Intial adding of contacts to ContactsToShow
        //check the total records are more than limitSize and assign the records
        if((counter+limitSize) <= totalSize){
            for(Integer i=0;i<limitSize;i++){
                ContactsToShow.add(AllContacts.get(i));
            }
        }
        
        system.debug('ContactsToShow==='+ContactsToShow.size());
        
        /*else{
            for(Integer i=0;i<totalSize;i++){
                ContactsToShow.add(AllContacts.get(i));
            }
        }
        */
        
    }
    
    //Navigation methods
    
    
	public void beginning(){
   
        ContactsToShow.clear();
        counter=0;
        if((counter + limitSize) <= totalSize){
       
            for(Integer i=0;i<limitSize;i++){
                ContactsToShow.add(AllContacts.get(i));
            }   
           
        } else{
       
            for(Integer i=0;i<totalSize;i++){
                ContactsToShow.add(AllContacts.get(i));
            }       
           
        }
       
    }
   
    public void next(){
   
        ContactsToShow.clear();
        counter=counter+limitSize;
       
        if((counter+limitSize) <= totalSize){
            for(Integer i=counter-1;i<(counter+limitSize);i++){
                ContactsToShow.add(AllContacts.get(i));
            }
        } else{
            for(Integer i=counter;i<totalSize;i++){
                ContactsToShow.add(AllContacts.get(i));
            }
        }
    }
   
    public void previous(){
   
        ContactsToShow.clear();

        counter=counter-limitSize;       
       
        for(Integer i=counter;i<(counter+limitSize); i++){
            ContactsToShow.add(AllContacts.get(i));
        }
    }

    public void last (){
   
        ContactsToShow.clear();
       
        if(math.mod(totalSize , limitSize) == 0){
            counter = limitSize * ((totalSize/limitSize)-1);
        } else if (math.mod(totalSize , limitSize) != 0){
            counter = limitSize * ((totalSize/limitSize));
        }
       
        for(Integer i=counter-1;i<totalSize-1;i++){
                ContactsToShow.add(AllContacts.get(i));
        }
       
    }
   
    public Boolean getDisableNext(){
   
        if((counter + limitSize) >= totalSize )
            return true ;
        else
            return false ;
    }
   
    public Boolean getDisablePrevious(){
   
        if(counter == 0)
            return true ;
        else
            return false ;
    } 
}