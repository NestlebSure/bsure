/************************************************************************************************       
Controller Name         : BSureC_PaymentHistorySectionStage       
Date                    : 11/09/2012        
Author                  : Santhosh Palla       
Purpose                 : To Inser the Credit Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/05/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_PaymentHistorySectionStage 
{
    
 /// <summary>  
 /// void methode will insert the alle the recodrs form stage to publish Sandbox  
 /// </summary>
    webService static void loadbutton()
    {
        BsureC_PaymentHistoryBatch b = new BsureC_PaymentHistoryBatch (); 
      	database.executebatch(b,100);
    }
    
    
  /// <summary>  
 /// void methode will insert the alle the recodrs form stage to Customer Info Object  
 /// </summary>
    webService static void loadcutomerdata()
    {
        BSureC_CustomerDatauploadBatch b1 = new BSureC_CustomerDatauploadBatch (); 
      	database.executebatch(b1,10);
    }
}