public class ExportAsCSVController 
{
	 public List<BSureS_Basic_Info__c> objLst {get;set;} 
	 transient public List<List<BSureS_Basic_Info__c >> wrapperCollection  {get; private set;}
	 public String obj;
	 public String soql;
	 public String fieldNames {get;set;}
	 public List<String> flds {get;set;}
	 public String coma {get;set;}
	 public String newLine {get;set;}
	 
	 public ExportAsCSVController()
	 {
	 	/*
		  coma = ',';
		  newLine = '\n';
		  obj = System.currentPageReference().getParameters().get('objName');
		  soql = System.currentPageReference().getParameters().get('soql');
		  System.debug('Selected Object is ' + obj);
		  */  
	 }
	 
	 public void exportToCSV()
	 {
	  
		   string strQuery1 = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c, '+ 
		                    'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
		                    'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
		                    ', Owner_Ship__c ,Contact_Name__c '+
		                    ' from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
			
			string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
		                    
			objLst = Database.query(strQuery1);
			wrapperCollection=collectionOfCollections(objLst);
	 }
	 
	 public List<List<BSureS_Basic_Info__c>> collectionOfCollections(List<BSureS_Basic_Info__c> coll){ 
        List<List<BSureS_Basic_Info__c >> mainList = new List<List<BSureS_Basic_Info__c >>();
        List<BSureS_Basic_Info__c > innerList;
        Integer idx = 0;
        if(coll!=null)
        {
            innerList = new List<BSureS_Basic_Info__c >();
            innerList.clear();
            for(BSureS_Basic_Info__c  sObj:coll)
            {
                
                if (Math.mod(idx++, 1000) == 0 ) 
                {
                    mainList.add(innerList); 
                    innerList = new List<BSureS_Basic_Info__c >();
                }
                        
				innerList.add(sObj);
                
            }
            if(innerList != null && innerList.size() > 0 )
            {
                //System.debug('innerList=======');
                mainList.add(innerList);
            }    
            //System.debug('mainList====='+mainList);
        }
        mainList.remove(0);
        return mainList;
    } 
}