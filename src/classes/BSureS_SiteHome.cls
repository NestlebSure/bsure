/***********************************************************************************************
*   Controller Name : BSureS_SiteHome
*   Date            : 11/02/2017 
*   Author          : Dheeraj
*   Purpose         : This class provides the control for Buyer site home page.
*   Date                           Programmer              Reason
*   --------------------      --------------------    --------------------
*      11/02/2017                Dheeraj
**************************************************************************************************/
public class BSureS_SiteHome {
    public string contactid;
    public string contactname{set;get;}
    public string contactemail{set;get;}
 
   
    
    //Constructor
    public BSureS_SiteHome(){
        contactid = ApexPAges.currentPage().getParameters().get('cntctid');
        if(contactid != null){
        List<contact> contacts = [select Name,email from contact where id=:contactid];
          if(contacts.size()>0){
             contactname = contacts[0].name;
             contactemail = contacts[0].email;
           }
        }
    
    } 

    // Login Authentication check
    public PageReference LoginAuthCheck(){
        PageReference pgRef;
        Cookie userauth = ApexPages.currentPage().getCookies().get('loginAuth');
        
        if(userauth == null){
            pgRef = new PageReference('/apex/BSureS_SiteLogin');
        }else if(userauth != null){
            if(userauth.getValue() != contactid){
                pgRef = new PageReference('/apex/BSureS_SiteLogin');
            }
        }
        return pgRef;
    }
    
       public PageReference siteLogOut() {
        ApexPages.currentPage().setCookies(new System.Cookie[] { new System.Cookie('loginAuth',null, null, -1, false) });
        PageReference pgRef = new PageReference('/apex/BSureS_SiteLogin');
        return pgRef;
       } 
       
       public PageReference siteChangePassword(){
          PageReference pgRef = new PageReference('/apex/SiteChangePassword?cntctid='+contactid);
          pgRef.setRedirect(true);
          return pgRef;
       }
       
}