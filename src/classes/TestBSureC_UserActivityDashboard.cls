/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_UserActivityDashboard {

    static testMethod void myUnitTest()
   {
    
       list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureC_BuyerShare';
        cSettings4.Parameter_Key__c = 'BSureC_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Key__c = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureC_Analyst,BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Key__c = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureC_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
       
        
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(Contact_name__c='KingFisher');
        insert objCustomer;
        
        set<id> setUserIds = new set<Id>();
        setUserIds.add(objCustomer.id);
        setUserIds.add(userInfo.getUserId());
        
        system.assertEquals('KingFisher',objCustomer.Contact_name__c);
        
        list<BSureC_Invoice_Collection_Section__c> lstC = new list<BSureC_Invoice_Collection_Section__c>();
        list<BSureC_Unearned_Cash_Discount_Section__c> lstCR = new list<BSureC_Unearned_Cash_Discount_Section__c>();
        list<BSureC_Unauthorized_Deduction_Section__c>  lstUDS=new list<BSureC_Unauthorized_Deduction_Section__c>();
        list<BSureC_On_Going_Credit_Section__c>  lstOSC= new list<BSureC_On_Going_Credit_Section__c>();
        list<BSureC_Confidential_Documents_Section__c>  lstCDS= new list<BSureC_Confidential_Documents_Section__c>();
        list<BSureC_Credit_Increase_Section__c>  lstCIS=new list<BSureC_Credit_Increase_Section__c>();
       
        for(integer i=0;i<=3;i++){
            BSureC_Invoice_Collection_Section__c  cInfo = new BSureC_Invoice_Collection_Section__c();
            cInfo.Discussion_Type__c = 'Topic';
            cInfo.Customer_ID__c = objCustomer.Id;
            lstC.add(cInfo);
            BSureC_Invoice_Collection_Section__c  cInfoFile = new BSureC_Invoice_Collection_Section__c();
            cInfoFile.Discussion_Type__c = 'File';
            cInfoFile.Customer_ID__c = objCustomer.Id;
            lstC.add(cInfoFile);
            BSureC_Unearned_Cash_Discount_Section__c Cr = new BSureC_Unearned_Cash_Discount_Section__c();
            Cr.Discussion_Type__c = 'Topic';
            Cr.Customer_ID__c = objCustomer.Id;
            lstCR.add(CR);
            BSureC_Unearned_Cash_Discount_Section__c CrFile = new BSureC_Unearned_Cash_Discount_Section__c();
            CrFile.Discussion_Type__c = 'File';
            CrFile.Customer_ID__c = objCustomer.Id;
            lstCR.add(CRFile);
            BSureC_Unauthorized_Deduction_Section__c CrUDS = new BSureC_Unauthorized_Deduction_Section__c();
            CrUDS.Discussion_Type__c = 'Topic';
            CrUDS.Customer_ID__c = objCustomer.Id;
            lstUDS.add(CrUDS);
            BSureC_Unauthorized_Deduction_Section__c CrUDSFile = new BSureC_Unauthorized_Deduction_Section__c();
            CrUDSFile.Discussion_Type__c = 'File';
            CrUDSFile.Customer_ID__c = objCustomer.Id;
            lstUDS.add(CrUDSFile);
            BSureC_Confidential_Documents_Section__c  confi = new BSureC_Confidential_Documents_Section__c();
            confi.Discussion_Type__c = 'Topic';
            confi.Customer_ID__c = objCustomer.Id;
            lstCDS.add(confi);
            BSureC_Confidential_Documents_Section__c  confiFile = new BSureC_Confidential_Documents_Section__c();
            confiFile.Discussion_Type__c = 'File';
            confiFile.Customer_ID__c = objCustomer.Id;
            lstCDS.add(confiFile);
            BSureC_On_Going_Credit_Section__c  goingCredit = new BSureC_On_Going_Credit_Section__c();
            goingCredit.Discussion_Type__c = 'Topic';
            goingCredit.Customer_ID__c = objCustomer.Id;
            lstOSC.add(goingCredit);
            BSureC_On_Going_Credit_Section__c  goingCreditFile = new BSureC_On_Going_Credit_Section__c();
            goingCreditFile.Discussion_Type__c = 'File';
            goingCreditFile.Customer_ID__c = objCustomer.Id;
            lstOSC.add(goingCreditFile);
            BSureC_Credit_Increase_Section__c  creditIncr = new BSureC_Credit_Increase_Section__c();
            creditIncr.Discussion_Type__c = 'Topic';
            creditIncr.Customer_ID__c = objCustomer.Id;
            lstCIS.add(creditIncr);
            BSureC_Credit_Increase_Section__c  creditIncrFile = new BSureC_Credit_Increase_Section__c();
            creditIncrFile.Discussion_Type__c = 'File';
            creditIncrFile.Customer_ID__c = objCustomer.Id;
            lstCIS.add(creditIncrFile);
        }
        insert lstc;
        insert lstCR;
        insert lstUDS;
        insert lstOSC;
        insert lstCDS;
        insert lstCIS;
        system.assertEquals('KingFisher',objCustomer.Contact_Name__c);
      //  system.assertEquals(4,lstCR.size());
        
        BSureC_UserActivityDashboard UAD = new BSureC_UserActivityDashboard();
        UAD.userLoginCommentsCount(setUserIds);
        UAD.userLoginDocumentsCount(setUserIds);
        BSureC_UserActivityDashboard.userLHWrapper wrapper1=new BSureC_UserActivityDashboard.userLHWrapper();
        
        
   }
    }