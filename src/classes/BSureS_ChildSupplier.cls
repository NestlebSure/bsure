/************************************************************************************************       
Controller Name         : BSureS_ChildSupplier       
Date                    : 21/07/2014        
Author                  : satish.c      
Purpose                 : Child Suppliers       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          21/07/2014                  Initial Version
**************************************************************************************************/

global with sharing class BSureS_ChildSupplier {
	
	public list<Wrap_Cust_Infochange> lstSupplierInfo {get;set;}
	public set<String> setChildCustID {get;set;}
	public string strParmSupplierid{get;set;}
	public string strSuppName{get;set;}
	public string strCrid{get;set;}
	public boolean bool_selected_cust{get;set;}
	public boolean boolMultiCB{get;set;}
	public String strChildCustID {get;set;}
	
	public class Wrap_Cust_Infochange
    {
        public String str_Custnameinfo{get;set;}
        public string str_Custname{get;set;}
        /*public String str_Profile{get;set;}
        public String str_category{get;set;} 
        public string str_categoryname{get;set;}
        public string str_GlobeID{get;set;}*/
        public boolean bool_selected_cust{get;set;}
        public BSureS_Basic_Info__c objSupp{get;set;}
    }
    
	public BSureS_ChildSupplier()
	{	
		strParmSupplierid = ApexPages.CurrentPage().getParameters().get('Id');
		strCrid = ApexPages.CurrentPage().getParameters().get('crId');
		system.debug('strCrid@@@'+strCrid);
		lstSupplierInfo=new List<Wrap_Cust_Infochange>();
		setChildCustID = new set<string>();
		BSureS_Basic_Info__c supp = [SELECT Supplier_Name__c from BSureS_Basic_Info__c where id=: strParmSupplierid];
		strSuppName = supp.Supplier_Name__c;
		
		getChildSuppliers();
	}
    	
	public void getChildSuppliers()
	{
		//lstSupplierInfo = [SELECT id,Globe_ID__c,Supplier_Category_Name__c,Supplier_Category__c,Supplier_Name__c from BSureS_Basic_Info__c  where Parent_Supplier_ID__c =: strParmSupplierid];
		boolMultiCB = false;
		for(BSureS_Basic_Info__c changebasicinfo:[SELECT id,Supplier_Name__c,Review_Status__c,Rating_Type__c,Risk_Level_Type__c,Review_Complete_Date__c from BSureS_Basic_Info__c  where Parent_Supplier_ID__c =: strParmSupplierid order by Review_Status__c /*AND Review_Status__c != 'Started' AND Review_Status__c != 'Not Started' AND Review_Status__c != 'Pending Approval'*/])
		{
		    Wrap_Cust_Infochange rec_custinfochange=new Wrap_Cust_Infochange();      
		    rec_custinfochange.str_Custnameinfo=changebasicinfo.id;
		    rec_custinfochange.str_Custname=changebasicinfo.Supplier_Name__c;
		    rec_custinfochange.bool_selected_cust = false;
		    rec_custinfochange.objSupp = changebasicinfo;
		    lstSupplierInfo.add(rec_custinfochange);
		    if(rec_custinfochange.objSupp.Review_Status__c != 'Pending Approval'){
		    	boolMultiCB = true;
		    }
		}
		
	}
	
	/// <summary>
    /// FetchCustomerDetails method navigates to customer detail page
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference FetchCustomerDetails()
    {
        pageReference PageRef;
        string Customerinfo=apexpages.currentpage().getparameters().get('CustomerId');
        if(Customerinfo!=null)
        {
            PageRef=new pageReference('/'+Customerinfo);
            PageRef.setRedirect(true);
        }
        return PageRef;
    }
    public pagereference cancel() 
    {
        pagereference pageref = new pagereference('/apex/BSureS_ViewPerformCreditAnalysis?Id='+strCrid); 
        return pageref;
    }
    public pagereference CASave() 
    {
    	Integer i = 0;
    	strChildCustID = '';
    	for(Wrap_Cust_Infochange Customers:lstSupplierInfo)
        {
            if(Customers.bool_selected_cust==true)
            {
                i++;
                setChildCustID.add(Customers.str_Custnameinfo);
                strChildCustID += Customers.str_Custnameinfo+',';
            }
        }
        system.debug(strCrid+'setChildCustID$$$'+setChildCustID);
        if(strChildCustID !=null && strChildCustID.length()>0)
        	strChildCustID = strChildCustID.substring(0, strChildCustID.length()-1);
        
        pagereference pageref = new pagereference('/apex/BSureS_MirrorCreditReview?id='+strCrid+'&ChildCustID='+strChildCustID+'&parentSupp='+strSuppName); 
        return pageref;
    }
    
}