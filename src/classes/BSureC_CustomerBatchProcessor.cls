global class BSureC_CustomerBatchProcessor implements Database.batchable<SObject>,Database.Stateful {
    SObject[] source;
    global String STRCS;
    public list<string> csvfilelines {get;set;}
    public list<BSureC_Customer_Basic_Info_Stage__c> verlist {get;set;}
    global BSureC_CustomerBatchProcessor(SObject[] source , String StrCSV) {
        this.source = source;
        STRCS = StrCSV;
        System.Debug('It Came in Constructor');
       
    }


    global Iterable<SObject> start(Database.BatchableContext bc) {
        BSureC_BatchSObjectFeeder batchfeeder = new BSureC_BatchSObjectFeeder(source);
        System.Debug('It Came in Start' + BC.getJobId());
        return batchfeeder;
         
       
    }
    
    public void execute(Database.BatchableContext bc, SObject[] scope) {
        
       BSureC_insertdataFromCSV.Insertrecords(BC.getJobId(),STRCS );
    }
    
    global void finish(Database.BatchableContext bc) {
       
    }
}