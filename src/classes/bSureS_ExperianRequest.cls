global class bSureS_ExperianRequest {
    public string ExperianString;
    public string expRespString;
    public string parentId_lst;
    public string attachName_lst;
    public bSureS_ExperianRequest(){
    } 
    
    webservice static string generateExperianRpt(string SuppId, string BIN, string globeId)
    {
        string resString = '';
        BSure_Configuration_Settings__c custSetting = BSure_Configuration_Settings__c.getInstance('ExperianAuthenticationCheck');
        if(custSetting.Parameter_Value__c!=null && custSetting.Parameter_Value__c!='' && custSetting.Parameter_Value__c=='false'){
            resString = Label.BSureS_ExpTempDisable;
        }else{
            DateTime d = Datetime.now();
            string dateStr =  d.format('MMddyy') ;
            system.debug('::::::::::: ' + dateStr) ;
            string fName = dateStr+'_Experian_Rpt_'+globeId;
            Database.SaveResult SaveResult;
            try{
                bSureS_ExperianRequest objExpReq = new bSureS_ExperianRequest();
                resString = objExpReq.getExperianRequest(SuppId,fName,BIN); 
                list<BSureS_Credit_Review_Section__c> lstCreditReview = [Select id,name,Supplier_ID__c,TitleDescription__c,Document_Type__c  
                                                                    from BSureS_Credit_Review_Section__c 
                                                                    where Supplier_ID__c =: SuppId and TitleDescription__c =: fName];
                BSureS_Credit_Review_Section__c objCreditReview = new BSureS_Credit_Review_Section__c();
                if(lstCreditReview != null && lstCreditReview.size()>0){
                    objCreditReview = lstCreditReview[0];
                    Blob blobContent = Blob.valueof(objExpReq.expRespString);
                    Attachment myAttachment  = new Attachment();
                    myAttachment.Body = blobContent;
                    myAttachment.Name = fName;
                    myAttachment.parentId = objCreditReview.id;
                    SaveResult = Database.insert(myAttachment);
                    system.debug('SaveResult::'+SaveResult);
                    if(resString != '' && resString != null){
                        resString += ','+objCreditReview.id+','+fName;
                    }
                    list<BSureS_Basic_Info__c> UpAttLst = new list<BSureS_Basic_Info__c> ();
                    for(BSureS_Basic_Info__c echVal : [Select id ,AttachmentName__c ,Credit_Id__c 
                                                        from BSureS_Basic_Info__c
                                                        where id=:SuppId]){
                        echVal.AttachmentName__c=fname;
                        echVal.Credit_Id__c =objCreditReview.id;
                        UpAttLst.add(echVal);
                    }
                    if(UpAttLst!=null){
                        database.update(UpAttLst);
                    }
                }
            }catch(Exception ex){
                system.debug('Exception Occured '+ex.getMessage()+ex.getStackTraceString());
            }
            if(resString == '' || resString == null){
                resString = Label.BSureS_ExpException;
            }
            if(Test.isRunningTest()){
                resString = '';
            }
        }
        return resString;
    }
    public void previousCredit(string ParentId){
        string attchId='';
        integer cnt=0;
        parentId_lst='';
        attachName_lst='';
        for(BSureS_Credit_Review_Section__c lstVal:[Select id ,TitleDescription__c from BSureS_Credit_Review_Section__c
                                                         where Supplier_ID__c =: ParentId and TitleDescription__c LIKE '%Experian%' 
                                                        order by createddate DESC limit 2 ]){
            if(cnt==1){
                parentId_lst =lstVal.id;
                attachName_lst =lstVal.TitleDescription__c;
            }                       
            cnt++;                       
         }
    }
    public string getExperianRequest(string strSupplierId, string strfileName, string strBIN){
        string strRes = '';
        string strTimeOut = '';
        try{
            String strUsername;
            String strPassword;
            list<bSureS_ExpCred__c> lstCred = [SELECT Id,Name,Pwd__c,UN__c From bSureS_ExpCred__c where Credit_Agency__c ='Experian'];
            if(lstCred!=null && lstCred.size()>0){
                if(lstCred[0].UN__c!=null && lstCred[0].UN__c!=''){
                    strUsername = lstCred[0].UN__c;
                }
                if(lstCred[0].Pwd__c!=null && lstCred[0].Pwd__c!=''){
                    strPassword = lstCred[0].Pwd__c;
                }
            }else{
                strRes = Label.BSureS_ExpCred;
            }
            
            if(strUsername!=null && strUsername!='' && strPassword!=null && strPassword!=''){
                String strExperianEndpoint = BSureS_CommonUtil.getConfigurationValues('ExperianEndpoint').get(0);
                map<String,String> mapExpAlias = new map<string,string>();
                map<String,String> mapResultBook = new map<string,string>();
                for(BsureExperian__c objBsureExper:BsureExperian__c.getAll().values()){
                    if(objBsureExper!=null){
                        if(objBsureExper.NodeName__c!='' && objBsureExper.NodeName__c!='' && objBsureExper.AliasName__c!=null 
                                                                                          && objBsureExper.AliasName__c!=''){
                            mapExpAlias.put(objBsureExper.NodeName__c,objBsureExper.AliasName__c);
                        }
                    }
                 }
             
                getExperianEnquiryString(strBIN);
                system.debug('ExperianString===='+ExperianString);
                HttpRequest req = new HttpRequest();  
                req.setEndpoint(strExperianEndpoint);
                req.setHeader('content-type','application/x-www-form-urlencoded');
                req.setMethod('POST');
                Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                req.setHeader('Authorization', authorizationHeader);
                req.setBody(ExperianString); 
                Http http = new Http();
                XmlStreamReader reader;
                HttpResponse res = new HttpResponse();
                if(!Test.isRunningTest()){
                    try{
                        req.setTimeout(120000); // timeout in milliseconds - this is one minute
                        res = http.send(req);
                    }catch(Exception ex){
                        system.debug('Exception occured::: '+ex.getMessage()+ex.getStackTraceString());
                        strTimeOut = Label.BSureS_ExpTimeout+' '+ex.getMessage();
                    }
                    system.debug(res.getStatusCode()+'res===='+res.getBody());
                }else{
                    res.setHeader('Content-Type', 'application/xml');
                    res.setBody('{"foo":"bar"}');
                    res.setStatusCode(200);
                }
                expRespString = '';
                if(res!=null && res.getStatusCode() == 200){
                    reader = res.getXmlStreamReader();
                    expRespString = res.getBody();
                    expRespString = expRespString.replace('<?xml version="1.0" standalone="no"?>','');
                    //expRespString = expRespString.replace('&amp;','');
                    system.debug('expRespString==::'+expRespString);
                    system.debug('reader==::'+reader);
                    mapResultBook = parseBooks(reader,mapExpAlias);
                    system.debug('mapResultBook:::'+mapResultBook);
                    if(!mapResultBook.isEmpty()){
                        if(mapResultBook.containskey('ProcessingAction')){
                            system.debug('Error Code::'+mapResultBook.get('ProcessingAction'));
                            strRes = Label.BSureS_ExpErrorCode+mapResultBook.get('ProcessingAction');
                        }else if(mapResultBook.containskey('ErrorMessage')){
                            system.debug('Error Code::'+mapResultBook.get('ErrorMessage'));
                            strRes = Label.BSureS_ExpErrorCode+mapResultBook.get('ErrorMessage');
                        }else{
                            string recId = '';
                            try{
                                recId = insertCreditReviewSec(strSupplierId,strfileName,mapResultBook);
                                if(recId != null && recId != ''){
                                    string resVal = '';
                                    try{
                                        previousCredit(strSupplierId);
                                        resVal = InsertIntoExperain(mapResultBook,strSupplierId);
                                        system.debug('resVal==='+resVal);
                                        if(resVal != null && resVal != ''){
                                            strRes = Label.BSureS_ExpSuccess;
                                        }else{
                                            strRes = Label.BSureS_ExpFailData;
                                        }
                                    }catch(Exception ex){
                                        system.debug('Exception Handling for Experian Data '+ex.getMessage()+ex.getStackTraceString());
                                        if(resVal == null || resVal == ''){
                                            strRes = Label.BSureS_ExpFailData;
                                        }
                                    }
                                }else{
                                    strRes = Label.BSureS_ExpFailCR;
                                }
                            }catch(Exception ex){
                                system.debug('Exception Handling for Credit Review Data '+ex.getMessage()+ex.getStackTraceString());
                                if(recId == null || recId == ''){
                                    strRes = Label.BSureS_ExpFailCR;
                                }
                            }
                        }
                    }
                }else if(res!=null && res.getStatusCode() == 303){
                    strRes = Label.BSureS_ExpErrorCode+res.getStatusCode()+Label.BSureS_ExpSpecialChar+Label.BSureS_ExpAuthFail;
                    BSure_Configuration_Settings__c newObj = [SELECT Parameter_Key__c,Parameter_Value__c 
                                                                    From BSure_Configuration_Settings__c 
                                                                    Where Parameter_Key__c=:'ExperianAuthenticationCheck' 
                                                                    And Parameter_Value__c=:'true'];
                    if(newObj!=null){
                        newObj.Parameter_Value__c = 'false';
                        update newObj;
                    }
                }else{
                    if(strTimeOut != null && strTimeOut != ''){
                        strRes = strTimeOut;
                    }else{
                        strRes = Label.BSureS_ExpErrorCode+res.getStatusCode();
                    }
                }
            }else{
                strRes = Label.BSureS_ExpCred;
            }
        }catch(Exception ex){
            system.debug('Exception occured '+ex.getMessage()+ex.getStackTraceString());
        }
        system.debug('strRes=='+strRes);
        return strRes;
    }
  
    public void getExperianEnquiryString(string BINFileNumber){
        String strExperianDBHost = BSureS_CommonUtil.getConfigurationValues('ExperianDBHost').get(0);
        String strExperianEAI = BSureS_CommonUtil.getConfigurationValues('ExperianEAI').get(0);
        String strExperianSubCode = BSureS_CommonUtil.getConfigurationValues('ExperianSubCode').get(0);
        String strExperianVendorNumber = BSureS_CommonUtil.getConfigurationValues('ExperianVendorNumber').get(0);
        
        //Premier Profile with Business Identification Number (BIN)
        String PremierProfileString = '<?xml version="1.0" encoding="UTF-8"?>'+
            '<NetConnectRequest xmlns="http://www.experian.com/NetConnect" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.experian.com/NetConnect NetConnect.xsd">'+
                '<EAI>'+strExperianEAI+'</EAI>'+
                '<DBHost>'+strExperianDBHost+'</DBHost>'+
                '<ReferenceId>user1abc001</ReferenceId>'+
                '<Request xmlns="http://www.experian.com/WebDelivery" version="1.0">'+
                    '<Products>'+
                        '<PremierProfile>'+
                            '<Subscriber>'+
                                '<OpInitials>GG</OpInitials>'+
                                '<SubCode>'+strExperianSubCode+'</SubCode>'+
                            '</Subscriber>'+
                            '<BusinessApplicant>'+
                                '<BISFileNumber>'+BINFileNumber+'</BISFileNumber>'+
                            '</BusinessApplicant>'+
                            '<OutputType>'+
                                '<XML>'+
                                    '<Verbose>Y</Verbose>'+
                                '</XML>'+
                            '</OutputType>'+
                            '<Vendor>'+
                                '<VendorNumber>'+strExperianVendorNumber+'</VendorNumber>'+
                            '</Vendor>'+
                        '</PremierProfile>'+
                    '</Products>'+
                '</Request>'+
            '</NetConnectRequest>';
        ExperianString = 'NETCONNECT_TRANSACTION='+PremierProfileString;
    }  
    
    /* Reading the Xml Response*/
    public map<string,string> parseBooks(XmlStreamReader reader, map<string,string> mapAlias) 
    {
        map<String, String> mapXmlBook = new map<String, String>();
        String ErrorCode='';
        try{
            while(reader.hasNext() ) 
            {
                String strNewKey = '';
                String Value = '';
                if (reader.getEventType() == XmlTag.START_ELEMENT) 
                {
                    if('ProcessingAction' == reader.getLocalName())
                    {
                        ErrorCode = reader.getAttributeValue(null, 'code')+Label.BSureS_ExpSpecialChar+getValueFromTag(reader);
                        mapXmlBook.put('ProcessingAction',ErrorCode);
                        break;
                    }
                    if('ErrorMessage' == reader.getLocalName())
                    {
                        ErrorCode = getValueFromTag(reader);
                        mapXmlBook.put('ErrorMessage',ErrorCode);
                        break;
                    }
                    String CurrentTag = reader.getLocalName();
                    if(mapAlias.containskey(CurrentTag))
                    {
                        for(String strEq:mapAlias.keyset())
                        {
                            
                            if (strEq == reader.getLocalName())
                            {
                                if('LinkageRecordType' == strEq){
                                    string Score = '';
                                    Score = reader.getAttributeValue(null, 'code');
                                    //system.debug('Score==='+Score);
                                    if(Score!=null && Score!='' && Score == '1'){
                                        while(reader.hasNext()) 
                                        {
                                            reader.next();
                                            if(reader.getLocalName() == 'LinkageCompanyName'){
                                                Value = getValueFromTag(reader);
                                                mapXmlBook.put('LinkageCompanyName',Score);
                                            }
                                            break;
                                        }
                                    }
                                }else if('SICCodes' == strEq){
                                    reader.next();
                                    if(reader.getLocalName() == 'SIC'){
                                        string sicValue = '';
                                        string Code = reader.getAttributeValue(null, 'code');
                                        
                                        while(reader.hasNext()) 
                                        {
                                            if (reader.getEventType() == XmlTag.END_ELEMENT) 
                                            {
                                                break;
                                            } else if (reader.getEventType() == XmlTag.CHARACTERS) 
                                            {
                                                sicValue += reader.getText();
                                            }
                                        reader.next();
                                        }
                                        sicValue = sicValue +' - ' + Code;
                                        system.debug('sicValue==='+sicValue+Code);
                                        if(mapXmlBook.get('SICCodes')!=null && mapXmlBook.size()>0){
                                            string sicCode = mapXmlBook.get('SICCodes');
                                            system.debug('sicCode==='+sicCode);
                                            sicValue += '; '+sicCode;
                                            mapXmlBook.put('SICCodes',sicValue);
                                        }else{
                                            mapXmlBook.put('SICCodes',sicValue);
                                        }
                                    }
                                }else{
                                    strNewKey = mapAlias.get(strEq);
                                    if(strNewKey != null && strNewKey != '' && !mapXmlBook.containsKey(strNewKey) )
                                    {
                                        Value = getValueFromTag(reader);
                                        mapXmlBook.put(strNewKey,Value);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                reader.next();
            }
            return mapXmlBook;
        }catch(XmlException ex)
        {
            mapXmlBook.put('XmlParsingException','Exception during Xml parsing error : '+ex.getMessage()+ex.getStackTraceString());
            return mapXmlBook;
        }
    }
   
    /* Getting the Value from the Tag */
    public String getValueFromTag(XmlStreamReader reader) 
    {
        String returnvalue = '';
        reader.setCoalescing(true);
        while(reader.hasNext()) 
        {
            if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) 
            {
                returnvalue=reader.getText();
            }
        reader.next();
        }
        return returnvalue;
    }
    
    public string insertCreditReviewSec(string strRecId,string strFName,map<string,string> mapExpResult){
        Database.upsertResult Result;
        Database.saveResult expHistResult;//To save Old Experian values in history object
        try{
            list<BSureS_Credit_Review_Section__c> lstCreditReview = [Select id,name,Supplier_ID__c,TitleDescription__c,Document_Type__c from BSureS_Credit_Review_Section__c 
                                                            where Supplier_ID__c =: strRecId and TitleDescription__c =: strFName];
            BSureS_Credit_Review_Section__c objCreditReview = new BSureS_Credit_Review_Section__c();
            if(lstCreditReview != null && lstCreditReview.size()>0){
                objCreditReview = lstCreditReview[0];
                list<Attachment> attach = [select id,name from Attachment where ParentId=:objCreditReview.id];
                delete attach;
            }       
            system.debug('strFName==='+strFName+'****'+strRecId);
            objCreditReview.TitleDescription__c = strFName;
            objCreditReview.Supplier_ID__c = strRecId;
            objCreditReview.DiscussionType__c = 'File';
            
            String docType = BSureS_CommonUtil.getConfigurationValues('ExperianSupRecDocType').get(0);
            objCreditReview.Document_Type__c = docType;
            
            Result = Database.upsert(objCreditReview);
            decimal dTaxCount = 0;
            decimal dBankCount = 0;
            decimal dJudgmtCount = 0;
            bSureS_CreditAgency_History__c objExperianHistory = new bSureS_CreditAgency_History__c ();   
            objExperianHistory.Supplier_Basic_Info_Id__c = strRecId;
            objExperianHistory.Name='Experian';
            objExperianHistory.Credit_Review_Section__c =objCreditReview.id;
            if(mapExpResult.containskey('TaxLienFilingCount')){
                    dTaxCount = drop_leading_zeros(mapExpResult.get('TaxLienFilingCount'));
            }
            if(mapExpResult.containskey('BankruptcyFilingCount')){
                    dBankCount = drop_leading_zeros(mapExpResult.get('BankruptcyFilingCount'));
            }
            if(mapExpResult.containskey('JudgmentFilingCount')){
                    dJudgmtCount = drop_leading_zeros(mapExpResult.get('JudgmentFilingCount'));
            }
            objExperianHistory.Legal_Filings__c=string.valueof(dTaxCount+dBankCount+dJudgmtCount);
            if(mapExpResult.containskey('CurrentDBT')){
                    objExperianHistory.Company_DBT__c=string.valueof(drop_leading_zeros(mapExpResult.get('CurrentDBT')));
                }else{
                    objExperianHistory.Company_DBT__c='0';
                }
                
                if(mapExpResult.containskey('IndustryDBT')){
                    objExperianHistory.Industry_DBT__c=string.valueof(drop_leading_zeros(mapExpResult.get('IndustryDBT')));
                }else{
                    objExperianHistory.Industry_DBT__c='0';
                }
                
                if(mapExpResult.containskey('BankruptcyFilingCount')){
                    if(drop_leading_zeros(mapExpResult.get('BankruptcyFilingCount'))==0){
                        objExperianHistory.Bankruptcy__c = 'No';
                    }else{
                        objExperianHistory.Bankruptcy__c = 'Yes';
                    }
                }else{
                    objExperianHistory.Bankruptcy__c='';
                }
                
                if(mapExpResult.containskey('TaxLienFilingCount')){
                    objExperianHistory.Tax_Lien_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('TaxLienFilingCount')));
                }else{
                    objExperianHistory.Tax_Lien_filings__c='0';
                }
                
                if(mapExpResult.containskey('JudgmentFilingCount')){
                    objExperianHistory.Judgment_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('JudgmentFilingCount')));
                }else{
                    objExperianHistory.Judgment_filings__c='0';
                }
                
                if(mapExpResult.containskey('LegalBalance')){
                    objExperianHistory.Sum_legal_filings__c=Decimal.valueOf(mapExpResult.get('LegalBalance'));
                }else{
                    objExperianHistory.Sum_legal_filings__c=0;
                }
                
                if(mapExpResult.containskey('UCCFilings')){
                    objExperianHistory.UCC_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('UCCFilings')));
                }else{
                    objExperianHistory.UCC_filings__c='0';
                }
                
                if(mapExpResult.containskey('CommercialFraudRiskIndicatorCount')){
                    objExperianHistory.Fraud_Alerts__c=mapExpResult.get('CommercialFraudRiskIndicatorCount');
                }else{
                    objExperianHistory.Fraud_Alerts__c='0';
                }
                
                if(mapExpResult.containskey('EmployeeSize')){
                    objExperianHistory.Number_of_Employees__c = Decimal.valueof(mapExpResult.get('EmployeeSize'));
                }else{
                    objExperianHistory.Number_of_Employees__c=0;
                }
                
                if(mapExpResult.containskey('HighCreditAmountExtended')){
                    objExperianHistory.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('HighCreditAmountExtended'));
                }else if(mapExpResult.containskey('SingleHighCredit')){
                    objExperianHistory.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('SingleHighCredit'));
                }else{
                    objExperianHistory.Highest_amt_extended__c=0;
                }
                
                if(mapExpResult.containskey('LinkageCompanyName')){
                    objExperianHistory.Ultimate_Parent__c=mapExpResult.get('LinkageCompanyName');
                }else{
                    objExperianHistory.Ultimate_Parent__c='';
                }
                
                if(mapExpResult.containskey('DateOfIncorporation')){
                    string dt = mapExpResult.get('DateOfIncorporation');
                    integer intDate = integer.valueof(dt);
                    if(intDate > 1){
                        integer strYear = integer.valueof(dt.substring(0,4));
                        integer strMonth = integer.valueof(dt.substring(4,6));
                        integer strDate = integer.valueof(dt.substring(6,8));
                        Datetime myDateTime = Datetime.newInstance(stryear, strMonth, strDate);
                        objExperianHistory.Date_of_Incorporation__c = date.valueof(myDateTime);
                    }else{
                        objExperianHistory.Date_of_Incorporation__c=null;
                    }
                }else{
                    objExperianHistory.Date_of_Incorporation__c=null;
                }
                
                if(mapExpResult.containskey('SICCodes')){
                    objExperianHistory.SIC_Code__c=mapExpResult.get('SICCodes');
                }else{
                    objExperianHistory.SIC_Code__c='';
                }
                
                if(mapExpResult.containskey('ProfileDate')){
                    string dt = mapExpResult.get('ProfileDate');
                    integer strYear = integer.valueof(dt.substring(0,4));
                    integer strMonth = integer.valueof(dt.substring(4,6));
                    integer strDate = integer.valueof(dt.substring(6,8));
                    Datetime myDateTime = Datetime.newInstance(stryear, strMonth, strDate);
                    objExperianHistory.Report_Date__c = date.valueof(myDateTime);
                }else{
                    objExperianHistory.Report_Date__c=null;
                }
                system.debug('*****'+objExperianHistory);
                if(objExperianHistory!=null){
                database.upsert(objExperianHistory);
            } 
        }catch(Exception ex){
            system.debug('Exception occured while saving Experian report into Credit Review Section.'+ex.getMessage()+ex.getStackTraceString());
        }
        if(Result.isSuccess() == true){
            return Result.Id;
        }else{
            return '';
        }
    } 
    
    public static decimal drop_leading_zeros(String passedValue) {
        String return_string = null; //return string for passing back
        if (passedValue != null) { //if the passed value is not null
            return_string = passedValue.trim(); //trim the whitespace from the start and end of the value
            Pattern valid_characters = Pattern.compile('([0-9]+)'); //only numbers
            Matcher check_chars = valid_characters.matcher(return_string); //compare the string to the set of valid characters
            if (check_chars.matches()) { //if we have a somewhat valid number
                if (return_string.startsWith('0') && return_string.length() > 1) { //if the string begins with a 0 and the length is greater than 1
                    boolean keepChecking = true; //create a boolean variable
                    while (keepChecking) { //if boolean is true
                        if (return_string.startsWith('0') && return_string.length() > 1) { //if the string begins with 0 and there is more than 1 character
                            return_string = return_string.substring(1); //drop the first character
                        } else { //either the string doesn't begin with 0 or the length is less than or equal to 1
                            keepChecking = false; //stop the loop
                        }
                    }
                }
                if (return_string == '0') { //if the resulting string is now a single '0'
                    return_string = '0'; //set the string to null
                }
            } else { //otherwise the value passed was not valid
                return_string = '0'; //set the string to null
            }
        }
        return decimal.valueof(return_string); //pass back a value
    }
    
    public string InsertIntoExperain(map<string,string> mapExpResult, string strSuppId){
        Database.saveResult Result;//To save Experian values in supplier info object
       // Database.saveResult expHistResult;//To save Old Experian values in history object
        Savepoint sp = Database.setSavepoint(); 
        try{
            if(mapExpResult != null && mapExpResult.size() > 0){
                BSureS_Basic_Info__c objSupplierExpData = new BSureS_Basic_Info__c();
                objSupplierExpData = [Select Id, Name, Bankruptcy__c, Company_DBT__c, Date_of_Incorporation__c, Legal_Filings__c, Fraud_Alerts__c, 
                                            Highest_amt_extended__c, Industry_DBT__c, Judgment_filings__c, Number_of_Employees__c, Report_Date__c, 
                                            SIC_Code__c, Sum_legal_filings__c, Tax_Lien_filings__c, UCC_filings__c, Ultimate_Parent__c
                                            from BSureS_Basic_Info__c Where Id =: strSuppId];
                                
                decimal dTaxCount = 0;
                decimal dBankCount = 0;
                decimal dJudgmtCount = 0;
                
                if(mapExpResult.containskey('TaxLienFilingCount')){
                    dTaxCount = drop_leading_zeros(mapExpResult.get('TaxLienFilingCount'));
                }
                if(mapExpResult.containskey('BankruptcyFilingCount')){
                    dBankCount = drop_leading_zeros(mapExpResult.get('BankruptcyFilingCount'));
                }
                if(mapExpResult.containskey('JudgmentFilingCount')){
                    dJudgmtCount = drop_leading_zeros(mapExpResult.get('JudgmentFilingCount'));
                }
                objSupplierExpData.Legal_Filings__c=string.valueof(dTaxCount+dBankCount+dJudgmtCount);
                
                if(mapExpResult.containskey('CurrentDBT')){
                    objSupplierExpData.Company_DBT__c=string.valueof(drop_leading_zeros(mapExpResult.get('CurrentDBT')));
                }else{
                    objSupplierExpData.Company_DBT__c='0';
                }
                
                if(mapExpResult.containskey('IndustryDBT')){
                    objSupplierExpData.Industry_DBT__c=string.valueof(drop_leading_zeros(mapExpResult.get('IndustryDBT')));
                }else{
                    objSupplierExpData.Industry_DBT__c='0';
                }
                
                if(mapExpResult.containskey('BankruptcyFilingCount')){
                    if(drop_leading_zeros(mapExpResult.get('BankruptcyFilingCount'))==0){
                        objSupplierExpData.Bankruptcy__c = 'No';
                    }else{
                        objSupplierExpData.Bankruptcy__c = 'Yes';
                    }
                }else{
                    objSupplierExpData.Bankruptcy__c='';
                }
                
                if(mapExpResult.containskey('TaxLienFilingCount')){
                    objSupplierExpData.Tax_Lien_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('TaxLienFilingCount')));
                }else{
                    objSupplierExpData.Tax_Lien_filings__c='0';
                }
                
                if(mapExpResult.containskey('JudgmentFilingCount')){
                    objSupplierExpData.Judgment_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('JudgmentFilingCount')));
                }else{
                    objSupplierExpData.Judgment_filings__c='0';
                }
                
                if(mapExpResult.containskey('LegalBalance')){
                    objSupplierExpData.Sum_legal_filings__c=Decimal.valueOf(mapExpResult.get('LegalBalance'));
                }else{
                    objSupplierExpData.Sum_legal_filings__c=0;
                }
                
                if(mapExpResult.containskey('UCCFilings')){
                    objSupplierExpData.UCC_filings__c=string.valueof(drop_leading_zeros(mapExpResult.get('UCCFilings')));
                }else{
                    objSupplierExpData.UCC_filings__c='0';
                }
                
                if(mapExpResult.containskey('CommercialFraudRiskIndicatorCount')){
                    objSupplierExpData.Fraud_Alerts__c=mapExpResult.get('CommercialFraudRiskIndicatorCount');
                }else{
                    objSupplierExpData.Fraud_Alerts__c='0';
                }
                
                if(mapExpResult.containskey('EmployeeSize')){
                    objSupplierExpData.Number_of_Employees__c = Decimal.valueof(mapExpResult.get('EmployeeSize'));
                }else{
                    objSupplierExpData.Number_of_Employees__c=0;
                }
                
                if(mapExpResult.containskey('HighCreditAmountExtended')){
                    objSupplierExpData.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('HighCreditAmountExtended'));
                }else if(mapExpResult.containskey('SingleHighCredit')){
                    objSupplierExpData.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('SingleHighCredit'));
                }else{
                    objSupplierExpData.Highest_amt_extended__c=0;
                }
                
                if(mapExpResult.containskey('LinkageCompanyName')){
                    objSupplierExpData.Ultimate_Parent__c=mapExpResult.get('LinkageCompanyName');
                }else{
                    objSupplierExpData.Ultimate_Parent__c='';
                }
                
                if(mapExpResult.containskey('DateOfIncorporation')){
                    string dt = mapExpResult.get('DateOfIncorporation');
                    integer intDate = integer.valueof(dt);
                    if(intDate > 1){
                        integer strYear = integer.valueof(dt.substring(0,4));
                        integer strMonth = integer.valueof(dt.substring(4,6));
                        integer strDate = integer.valueof(dt.substring(6,8));
                        Datetime myDateTime = Datetime.newInstance(stryear, strMonth, strDate);
                        objSupplierExpData.Date_of_Incorporation__c = date.valueof(myDateTime);
                    }else{
                        objSupplierExpData.Date_of_Incorporation__c=null;
                    }
                }else{
                    objSupplierExpData.Date_of_Incorporation__c=null;
                }
                
                if(mapExpResult.containskey('SICCodes')){
                    objSupplierExpData.SIC_Code__c=mapExpResult.get('SICCodes');
                }else{
                    objSupplierExpData.SIC_Code__c='';
                }
                
                if(mapExpResult.containskey('ProfileDate')){
                    string dt = mapExpResult.get('ProfileDate');
                    integer strYear = integer.valueof(dt.substring(0,4));
                    integer strMonth = integer.valueof(dt.substring(4,6));
                    integer strDate = integer.valueof(dt.substring(6,8));
                    Datetime myDateTime = Datetime.newInstance(stryear, strMonth, strDate);
                    objSupplierExpData.Report_Date__c = date.valueof(myDateTime);
                }else{
                    objSupplierExpData.Report_Date__c=null;
                }
                Result = Database.update(objSupplierExpData);
            }
        }catch(Exception ex){
            system.debug('Exception occured while saving Experian Data.'+ex.getMessage()+ex.getStackTraceString());
        }
        if(Result.isSuccess() == true ){
            return Result.Id;
        }else{
            Database.rollback(sp);
            return '';
        }
    }
}