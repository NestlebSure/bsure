/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestbSureSCreditsafe {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.test.startTest();
      
      	BsureS_CreditSafeCodes__c custSettings1 = new BsureS_CreditSafeCodes__c();
      	custSettings1.Name='EndPointURL';
      	custSettings1.Message__c='https://testwebservices.creditsafe.com/GlobalData';
      	insert custSettings1;
      	
      	BsureS_CreditSafeCodes__c custSettings2 = new BsureS_CreditSafeCodes__c();
      	custSettings2.Name='SOAPAction';
      	custSettings2.Message__c='http://www.creditsafe.com/globaldata/operations/CompanyDataAccessService/RetrieveCompanyOnlineReport';
      	insert custSettings2;
      	
      	BsureS_CreditSafeCodes__c custSettings3 = new BsureS_CreditSafeCodes__c();
      	custSettings3.Name='20103';
      	custSettings3.Message__c='No results';
      	insert custSettings3;
      	
      	
        BSureS_Basic_Info__c objBasicInfo = new BSureS_Basic_Info__c();
        objBasicInfo.Supplier_Name__c = 'Demo Supplier';
        objBasicInfo.BISFileNumber__c = '7986548975';
        objBasicInfo.Globe_ID__c = '589647';
        objBasicInfo.Company_Registration_ID__c ='US023/X/US1234567';
        objBasicInfo.Planned_Review_Date__c = system.today();
        insert objBasicInfo;
      
        bSureS_ExpCred__c cred = new bSureS_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		cred.Credit_Agency__c='Creditsafe';
		insert cred;
		
        DateTime d = Date.Today() ;
        string dateStr =  d.format('ddMMyy') ;
      
        BSureS_Credit_Review_Section__c objCreditInfo = new BSureS_Credit_Review_Section__c();
        objCreditInfo.Supplier_ID__c = objBasicInfo.id;
        objCreditInfo.TitleDescription__c = dateStr+'_Creditsafe_Rpt_'+objBasicInfo.Globe_ID__c;
        insert objCreditInfo;
      	bSureS_CreditSafe echIns = new bSureS_CreditSafe();
      	bSureS_CreditSafe.FetchCompanyOnlineReport(objBasicInfo.Company_Registration_ID__c,objBasicInfo.Id,objBasicInfo.Globe_ID__c);
      	string assign='1234';
      	system.assertEquals('1234',assign);
      	system.test.stopTest();
    }
}