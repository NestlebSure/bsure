/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
public with sharing class TestBSureS_Spend_History_Stage_update {

  static testMethod void myUnitTest()
  {
     list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureS_AnalystRole2';
        cSettings.Parameter_Key__c = 'BSureS_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureS_ZoneManager1';
        cSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureS_SubZoneManager1';
        cSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureS_CountryManager1';
        cSettings3.Parameter_Key__c = 'BSureS_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureS_BuyerShare1';
        cSettings4.Parameter_Key__c = 'BSureS_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureS_MassTransferAllowedProfiles1';
        cSettings5.Parameter_Key__c = 'BSureS_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureS_Analyst,BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureS_MassTransferNonCreditProfiles1';
        cSettings6.Parameter_Key__c = 'BSureS_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureS_MassTransferCreditProfiles1';
        cSettings7.Parameter_Key__c = 'BSureS_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureS_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
         BSureS_Basic_Info__c BCRef=new BSureS_Basic_Info__c (Supplier_name__c='Test Supplier');
            insert BCRef;

    BSureS_Spend_History_Publish__c ObjSpendpublish=new BSureS_Spend_History_Publish__c();
    ObjSpendpublish.Globe_ID__c='1234';
    ObjSpendpublish.date__c=system.today();
    ObjSpendpublish.Supplier_ID__c=BCRef.id;
    insert ObjSpendpublish;
    BSureS_Spend_History_Stage__c ObjSpendStage=new BSureS_Spend_History_Stage__c();
    ObjSpendStage.Date__c=system.today();
    ObjSpendStage.Globe_ID__c='1234';
    ObjSpendStage.Spend_Amount__c=21312;
    ObjSpendStage.Spend_Period__c=system.today();
    ObjSpendStage.Supplier_ID__c=BCRef.id;
     insert ObjSpendStage;
     ObjSpendStage.Spend_Period__c=system.today()+1;
     update ObjSpendStage;
     system.Assertequals(BCRef.id,ObjSpendStage.Supplier_ID__c);
  }
  
  
  
  static testMethod void myUnitTest2()
  {
     list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureS_AnalystRole3';
        cSettings.Parameter_Key__c = 'BSureS_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureS_ZoneManager3';
        cSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureS_SubZoneManager3';
        cSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureS_CountryManager3';
        cSettings3.Parameter_Key__c = 'BSureS_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureS_BuyerShare3';
        cSettings4.Parameter_Key__c = 'BSureS_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureS_MassTransferAllowedProfiles3';
        cSettings5.Parameter_Key__c = 'BSureS_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureS_Analyst,BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureS_MassTransferNonCreditProfiles3';
        cSettings6.Parameter_Key__c = 'BSureS_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureS_MassTransferCreditProfiles3';
        cSettings7.Parameter_Key__c = 'BSureS_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureS_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
         BSureS_Basic_Info__c BCRef=new BSureS_Basic_Info__c (Supplier_name__c='Test Supplier');
            insert BCRef;

    BSureS_Spend_History_Publish__c ObjSpendpublish=new BSureS_Spend_History_Publish__c();
    ObjSpendpublish.Globe_ID__c='1234';
    ObjSpendpublish.date__c=system.today();
    ObjSpendpublish.Supplier_ID__c=BCRef.id;
    insert ObjSpendpublish;
    
    BSureS_Spend_History_Stage__c ObjSpendStage=new BSureS_Spend_History_Stage__c();
    ObjSpendStage.Date__c=null;
    ObjSpendStage.Globe_ID__c='';
    ObjSpendStage.Spend_Amount__c=null;
    ObjSpendStage.Spend_Period__c=null;
    ObjSpendStage.Supplier_ID__c=BCRef.id;
    insert ObjSpendStage;
    ObjSpendStage.Spend_Period__c=system.today()+1;
    update ObjSpendStage;
    system.Assertequals(BCRef.id,ObjSpendStage.Supplier_ID__c);
  }
  
}