/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_BackUpAnalystsTrigger {

    static testMethod void myUnitTest() 
    {
      String CustomerName = 'TestName';
      string strAnalystrole='';
      string strCountryManagerRole='';
      string strSubZoneManagerRole='';
      string strZoneManagerrole=''; 
      string strNames='';   
      list<string> lstMultipleBuyerRoles = new list<String>();
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Test America',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='Test AOA',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='Test North America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Test Latin America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='Test United States',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Test Cuba',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef; 
        
        strAnalystrole=BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole').get(0);
	    strZoneManagerrole=BSureC_CommonUtil.getConfigurationValues('BSureC_ZoneManager').get(0);
	    strSubZoneManagerRole=BSureC_CommonUtil.getConfigurationValues('BSureC_SubZoneManager').get(0);
	    strCountryManagerRole=BSureC_CommonUtil.getConfigurationValues('BSureC_CountryManager').get(0);
	    lstMultipleBuyerRoles.add(strAnalystrole);
        lstMultipleBuyerRoles.add(strZoneManagerrole);
        lstMultipleBuyerRoles.add(strSubZoneManagerRole);
        lstMultipleBuyerRoles.add(strCountryManagerRole);
       
        
       map<id,User> mapObj = new map<id,User>([select id,Name from User where UserRole.Name IN:lstMultipleBuyerRoles]);
       
       
        
        BSureC_Customer_Basic_Info__c objCustomer=new BSureC_Customer_Basic_Info__c();
        objCustomer.Customer_Name__c=CustomerName;       
        objCustomer.Zone__c=ZRef.Id;
        objCustomer.Sub_Zone__c=SubZoneRef.Id;
        objCustomer.Country__c=ConRef.Id;
        objCustomer.Backup_Analysts__c = UserInfo.getUserId();        
        insert objCustomer;
        
        strNames = objCustomer.Backup_Analyst_Names__c;        
       
       
  
        // Verify that the initial state is as expected.
        //objCustomer=[Select id,Name,Customer_Name__c From BSureC_Customer_Basic_Info__c  where id=:objCustomer.id];
        set<Id> struserids = new set<Id>();
        struserids.add(objCustomer.id);
        string strSubject='Update trigger';
        string strEmailBody='Tgigger has been updated';
        BSureC_CommonUtil.SendEmailToQueue(struserids, strSubject,strEmailBody,'High',system.today(),true);
        System.assertEquals(CustomerName, objCustomer.Customer_Name__c);
    }
}