/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_AddEditAdditionalAttributes {

    static testMethod void myUnitTest() {
        
    Profile p = [select Id,Name from Profile where Id != null limit 1];

    User u = new User(
        alias = 'stUser', 
        email='veereandranath.j@vertexcs.com',
        emailencodingkey='UTF-8',
        firstname='USER',
        lastname='TEST',
        languagelocalekey='en_US',
        localesidkey='en_US', 
        profileid = p.Id,
        timezonesidkey='America/Los_Angeles',
        username='BsureS@user.com'
    );
    
    insert u;
    
    system.assertEquals(p.Id,U.ProfileId);
    
    system.Test.startTest();  
    BSureS_AddEditAdditionalAttributes AddEdit = new  BSureS_AddEditAdditionalAttributes();

        // TO DO: implement unit test
        AddEdit.getlstProfiles();
        AddEdit.getUsersLst();
        AddEdit.pagenation();
        AddEdit.FirstbtnClick();
        AddEdit.LastbtnClick();
        AddEdit.nextBtnClick(); 
        AddEdit.previousBtnClick();
        AddEdit.userViewPage();
        AddEdit.getPreviousButtonEnabled();
        AddEdit.getNextButtonDisabled();
        AddEdit.getPageSize();
        AddEdit.getPageNumber();
        AddEdit.getTotalPageNumber();
        AddEdit.strSelectedProfile  = p.Id;
        AddEdit.getUsersLst();
        
        system.Test.stopTest();
    }
         
}