/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class TestBSureS_CreditTeamAssignment {
    static testMethod void myUnitTest() 
    {
    	
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com');
        insert standard;
         User standard2 = new User(
                alias = 'standt2',
                email='standarduser2@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing2', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest2@testorg.com');
        insert standard2;
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_CEORole21';
        objCSettings1.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings1.Parameter_Value__c = 'CEO';
        insert objCSettings1;
        system.assertEquals('CEO', objCSettings1.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureS_ZoneManagerrole21';
        objCSettings2.Parameter_Key__c = 'BSureS_ZoneManagerrole';
        objCSettings2.Parameter_Value__c = 'Zone Manager';
        insert objCSettings2;
        system.assertEquals('Zone Manager', objCSettings2.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'BSureS_analystrole21';
        objCSettings5.Parameter_Key__c = 'BSureS_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        
        BSureS_User_Additional_Attributes__c uaddObj= new BSureS_User_Additional_Attributes__c();
        uaddObj.User__c =standard.id;
        uaddObj.Manager__c =standard.id;
        uaddObj.BSureS_Zone__c = szone.id;
        insert uaddObj;
        
        BSureS_Basic_Info__c supplierObj=new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = standard.id;
        supplierObj.Manager__c = standard.id;
        supplierObj.Backup_Analysts__c = 'standard.id,standard2.id';
        supplierObj.Backup_Analysts_Names__c ='Steve,george';
        supplierObj.Zone__c = szone.id;
        insert supplierObj;
        
        
        
        system.assertEquals('Steve',supplierObj.Contact_name__c);
        ApexPages.currentPage().getParameters().put('id',supplierObj.id); 
        system.Test.startTest();
        BSureS_CreditTeamAssignment objCA = new BSureS_CreditTeamAssignment();
        
        objCA.getAnalysts();
        objCA.getManagers();
        objCA.getManagersLst(standard.id);
        objCA.getBackupAnalysts();
        objCA.selectclick();
        objCA.unselectclick(); 
        objCA.AssignCreditTeam(); 
        objCA.Cancel();
        objCA.SendNotification();
        system.Test.stopTest();
    } 
    
}