/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_MassTransferComponent {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        /*BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureC_BuyerShare';
        cSettings4.Parameter_Key__c = 'BSureC_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Key__c = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureC_Analyst,BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Key__c = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureC_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;*/
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region1');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category1');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America1',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA1',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America1',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America1',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States1',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba1',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state1',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef;
        

        BSureC_Customer_Basic_Info__c BRef=new BSureC_Customer_Basic_Info__c();
        BRef.Customer_Name__c='test customer1';
        string StrReg=RegRef.id;
        BRef.Customer_Category__c=CatRef.id;
        BRef.Customer_Region__c=StrReg;
        BRef.Customer_Group_Id__c='Test group';
        BRef.Bill_to_Account__c= 'Test bill';
        BRef.Credit_Account__c='Test Credit Acc';
        BRef.Owner_Ship__c='Public';
        //BRef.Has_Parent__c=true;
        //BRef.Parent_Customer_ID__c=BPRef.id;
        BRef.DND_Financial_Information__c='yes';
        BRef.Last_Financial_Statement_Received__c=system.today();
        BRef.Sole_Sourced__c='yes';
        BRef.Fiscal_Year_End__c=system.today();
        BRef.Review_Status__c='Scheduled';
        BRef.Customers_Zone__c=ZoneRef.id+','+ZRef.id;
        BRef.Customers_Sub_Zone__c=SubZoneRef.id+','+SZoneRef.id;
        BRef.Customer_Validation__c='test';
        BRef.Zone__c=ZoneRef.id;
        BRef.Sub_Zone__c=SubZoneRef.id;
        BRef.Customer_Countries__c=ConRef.id;
        BRef.Country__c=ConRef.id;
        BRef.State__c=StateRef.id;
        BRef.Street_Name_Address_Line_1__c='Test Add1';
        BRef.Street_Name_Address_Line_1__c='';
        //BRef.State_Province__c='Test State';
        BRef.State__c=StateRef.id;
        BRef.City__c='Test City';
        //BRef.City__c='';
        BRef.Postal_Code__c='500081';
        BRef.Analyst__c=Userinfo.getUserId();
        BRef.Backup_Analysts__c=Userinfo.getUserId();
        //BRef.Manager__c=Userinfo.getUserId();
        insert BRef;
        BRef.City__c='Test City2';
        update BRef;
        system.Assertequals('test customer1',BRef.Customer_Name__c);
        Profile Prof=[select id, name from Profile where name='BSureC_Analyst'];

		BSureC_AdditionalAttributes__c  Objaddatribute=new BSureC_AdditionalAttributes__c();
		Objaddatribute.Manager__c=Userinfo.getUserID();
		Objaddatribute.User__c=Userinfo.getUserID();
		insert Objaddatribute;
        //BSureC_UserCategoryMapping__c UserCat=new BSureC_UserCategoryMapping__c(CategoryId__c=Cat.id,User__c=Userinfo.getuserid());
        //insert UserCat;
        BSureC_MassTransferComponent MTCRef=new BSureC_MassTransferComponent();
        String catid=CatRef.id;
        String custid=BRef.id;
        BSureC_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,UserInfo.getProfileID(),UserInfo.getUserID(),custid);
        BSureC_MassTransferComponent.findSObjects('testObj','Praveen S','testField','',UserInfo.getProfileID(),UserInfo.getUserID(),custid);        //('User','a', 'Username__C',catid,'BSureC_Analyst,System Administrator','','') 
        BSureC_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,UserInfo.getProfileID(),UserInfo.getUserID(),'custid,test');
        BSureC_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,'testprof1,testprof2',UserInfo.getUserID(),'custid,test');
        //BSureC_MassTransferComponent.findSObjects('User','a', 'Username__C', '','','','');
        //BSureC_MassTransferComponent.findSObjects('User','a', 'Username__C',catid,'BSureC_Analyst,System Administrator,BSureC_Country Manager','',''); 
        //BSureC_MassTransferComponent.findSObjects('User','a', 'Username__C',catid,'BSureC_Analyst,System Administrator','','');
        //BSureC_MassTransferComponent.findSObjects('','a','Username__C','','BSureC_Analyst','','');
    }
}