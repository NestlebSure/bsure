public with sharing class CopyrightPageCls {
    public PageReference doRedirect(){
        PageReference pg = new PageReference('/apex/TermsAndConditions');
        pg.setRedirect(true);
        return pg;
        
    }
}