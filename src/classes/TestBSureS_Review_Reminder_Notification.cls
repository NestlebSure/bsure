/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 //Kishorekumar.a
@isTest (SeeAllData=true)
private class TestBSureS_Review_Reminder_Notification {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile pfl = [select id from profile where name='BSureS_Analyst' limit 1];
        Profile pf2 = [select id from profile where name='BSureS_Manager' limit 1];
       
        User testUser1 = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg312122.com');
        insert testUser1; 
        
         User Manager = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pf2.Id,  country='United States', CommunityNickname = 'u341321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg34422.com');
        insert Manager; 
        
        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = testUser1.Id;
        supplierObj.Manager__c = Manager.Id;
        supplierObj.Planned_Review_Date__c = system.today().addDays(30);
        insert supplierObj;
        
        system.assertEquals('Steve',supplierObj.Contact_Name__c);
        
        BSureS_Basic_Info__c supplierObj2 = new BSureS_Basic_Info__c();
        supplierObj2.Contact_name__c = 'Steve';
        supplierObj2.Supplier_Name__c = 'george M';
        supplierObj2.Analyst__c = testUser1.Id;
        supplierObj2.Manager__c = Manager.Id;
        supplierObj2.Planned_Review_Date__c = system.today().addDays(15);
        insert supplierObj2;
        
        BSureS_Basic_Info__c supplierObj3 = new BSureS_Basic_Info__c();
        supplierObj3.Contact_name__c = 'Steve';
        supplierObj3.Supplier_Name__c = 'george M';
        supplierObj3.Analyst__c = testUser1.Id;
        supplierObj3.Manager__c = Manager.Id;
        supplierObj3.Planned_Review_Date__c = system.today().addDays(7);
        insert supplierObj3;
        
        BSureS_Basic_Info__c supplierObj4 = new BSureS_Basic_Info__c();
        supplierObj4.Contact_name__c = 'Steve';
        supplierObj4.Supplier_Name__c = 'george M';
        supplierObj4.Analyst__c = testUser1.Id;
        supplierObj4.Manager__c = Manager.Id;
        supplierObj4.Planned_Review_Date__c = system.today().addDays(1);
        insert supplierObj4;
        
        system.test.starttest();
        BSureS_Review_Reminder_Notification RNS = new BSureS_Review_Reminder_Notification();
		system.schedule('ScheduleTestRN','0 0 0 * * ?',RNS);
		system.test.stoptest();
    }
}