/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_EmailReminders {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.Test.startTest();
        Profile pfl = [select id from profile where name='BSureS_Analyst' limit 1];
        Profile pf2 = [select id from profile where name='BSureS_Manager' limit 1];

    	User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='u1@testorg32.com');
        insert testUser;
        
        system.assertEquals('u1@testorg32.com',testUser.username);
        
        User testUser1 = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg312122.com');
        insert testUser1; 
        
        system.assertEquals('u1@testorg312122.com',testUser1.username);
        
        User Manager = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pf2.Id,  country='United States', CommunityNickname = 'u341321',
            timezonesidkey='America/Los_Angeles', username='u1@testorg34422.com');
        insert Manager;   
        
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c
        					 (Contact_name__c='KingFisher',
        					  Next_Review_Date__c = system.today().addDays(1),
        					  Planned_Review_Date__c = system.today().addDays(15),Backup_Analysts__c = testUser.Id,
        					  Analyst__c = testUser1.id,Manager__c = Manager.Id );
	  	insert objSupplier;
	  	system.assertEquals('KingFisher',objSupplier.Contact_Name__c);
	  	objSupplier.Next_Review_Date__c = system.today().addDays(10);
	  	update objSupplier; 
	  	system.Test.stopTest();
   		
   		
    }
}