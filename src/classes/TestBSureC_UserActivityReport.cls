/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_UserActivityReport {

    static testMethod void myUnitTest() 
    {
    	string assert = 'CheckAssert';
        // TO DO: implement unit test
         list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
	        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
	        cSettings.Name = 'BSureC_Report_Page_Size';
	        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size';
	        cSettings.Parameter_Value__c = '10';
	        lstSettings.add(cSettings);
	        insert lstSettings;
	        
        BSureC_UserActivityReport BUAR = new BSureC_UserActivityReport();       
       BUAR.getSortDirection();
       BUAR.setSortDirection('Asc');
       BUAR.SortData();
       BUAR.ExportToExcel();
       BUAR.showBuyerDetails();
       BUAR.closepopup();       
       BUAR.searchBtn();
       BUAR.closepopupBtn();
       system.assertequals('CheckAssert',assert);
       BUAR.Search();
       BUAR.userViewPage();
       BUAR.nextBtnClick();
       BUAR.previousBtnClick();
       BUAR.getPageNumber();
       BUAR.getPageSize();
       BUAR.getPreviousButtonEnabled();
       BUAR.getNextButtonDisabled();
       BUAR.getTotalPageNumber();
       BUAR.cancel();
       BUAR.FirstbtnClick() ;
       BUAR.LastbtnClick();
    }
}