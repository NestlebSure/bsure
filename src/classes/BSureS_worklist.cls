/************************************************************************************************       
Controller Name         : BSureS_worklist       
Date                    : 12/20/2012        
Author                  : Neha Jaiswal       
Purpose                 : To generate the reports for upcoming review pending for supplier
Change History          : Date           Programmer                     Reason       
-----------------------------   -------------------   --------------------------------------------          
                          12/21/2012      Neha Jaiswal          Initial Version
                          01/25/2013      Vinu Prathap          Modified to avoid the SOQL injection
                          08/11/2013      Veereandrnath j       Code Review Changes
**************************************************************************************************/               
global with sharing class BSureS_worklist {
    public id userId{get;set;}//to hold userId
    public string userName{get;set;}//to holsd userName
    public Boolean AnalystOptionsBlock{get;set;}//to display dropdown of analyst
    public list<selectOption> lstSubordinateNames{get;set;}//to pass list of analyst 
    public string strSubordinateNames{get;set;}//to get id of selected analyst
    public string strMainQuery{get;set;}//to fetch the details on search criteria
    public list<BSureS_Basic_Info__c> listCustomerBasicInfo{get;set;}//list of customer basic inforamation
    public integer counter{get;set;}  //keeps track of the offset
    public integer list_size{get;set;} //sets the page size or number of rows
    public integer total_size{get;set;}//for pagenation
    public Boolean worklistBlock{get;set;}//to render searched record
    public list<CommonUserClass> objWorkList{get;set;}//instance of wrapper class
    public list<BSureS_Spend_History_Publish__c> listSpendHistory{get;set;}//customer information in publish object
    public string supId {get;set;}//to pass selected supplier id
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public integer totallistsize{get;set;}//used for pagenation
    public string  sortDirection = 'ASC';  
    public string  sortExp = 'Next_Review_Date__c';
    public string  loginUserProfile {get;set;}
    public Boolean isexport{get;set;}
    public Boolean displayPanel{get;set;}
    public Boolean exportTrue{get;set;}
    public Integer worklistReportSize{get;set;}
    public String strReviewStatusC{get;set;}
    
    
   /// <summary>
   /// constructor: to display the dropdown list and upcoming schedules.
   /// </summary>
   /// <returns> upcoming schedules
    public BSureS_worklist()
     {  
        counter=0;
        list_size = 2;
        exportTrue=true;
        worklistBlock=true;
        displayPanel=false;
        // checking list size // veera
        if(!BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').isEmpty())
        	worklistReportSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').get(0));
    	//Temp Aditya 
    	worklistReportSize = 2;
    	//Temp Aditya End
        listCustomerBasicInfo=new list<BSureS_Basic_Info__c>();
        userName=String.valueOf(UserInfo.getUserId());
        String usrName=UserInfo.getFirstName()+' '+UserInfo.getLastName();
        list<BSureS_Basic_Info__c> lstOptionsAnalyst=new list<BSureS_Basic_Info__c>();
        AnalystOptionsBlock=true;
        Profile objCurProfile;
        // Checking Query permession // Veera
        if(Schema.Sobjecttype.Profile.isQueryable())
        	objCurProfile = [SELECT name FROM Profile WHERE id=:Userinfo.getProfileId()];
        loginUserProfile = objCurProfile.name;
        lstSubordinateNames=new list<selectOption>();
         // Checking Query permession // Veera
        if(loginUserProfile.equalsIgnoreCase('System Administrator') && Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable()){
            lstOptionsAnalyst = [SELECT Id,Analyst__c,Analyst_Name__c FROM BSureS_Basic_Info__c WHERE Analyst_Name__c!= null and Analyst__r.isActive= true order by Analyst_Name__c ASC];
        }else if(Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable()){
        	lstOptionsAnalyst=[SELECT Id,Analyst__c,Analyst_Name__c FROM BSureS_Basic_Info__c  WHERE Manager__c =: String.valueOf(UserInfo.getUserId()) and Analyst__r.isActive= true  order by Analyst_Name__c ASC];
        }
            if(lstOptionsAnalyst!=null && lstOptionsAnalyst.size()>0)
            {
                lstSubordinateNames.add(new selectOption('ALL','ALL'));
                strSubordinateNames='ALL';
                AnalystOptionsBlock=true;
                map<Id,String> mapoptions=new map<Id,String>();
                    for(BSureS_Basic_Info__c optionAnalyst:lstOptionsAnalyst)
                    {
                        if(optionAnalyst.Analyst__c!=null  && optionAnalyst.Analyst_Name__c!=null && optionAnalyst.Analyst_Name__c!='' && mapoptions.get(optionAnalyst.Analyst__c)==null)
                        {
                           mapoptions.put(optionAnalyst.Analyst__c,optionAnalyst.Analyst_Name__c);
                           lstSubordinateNames.add(new selectOption(optionAnalyst.Analyst__c,optionAnalyst.Analyst_Name__c));
                        }
                    }
            }
                else
                  {
                        lstSubordinateNames.add(new selectoption(userName,usrName));
                        strSubordinateNames=userName;
                  }
        // displayrecords();
        
         objWorkList=new list<CommonUserClass>();
         totallistsize = listCustomerBasicInfo.size();
     }
     
      public pageReference ExportCSV()
        {
            isexport=true;
            pagenation();
            pageReference pRef = new pageReference('/apex/BSureS_WorklistExport');
            return pRef;
        }
         
        //For sorting grid in ascending and descending order
      public string sortExpression
        {
            get
            {
                return sortExp;
            }
            set
            {
                if(value == sortExp)
                    sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
                else
                    sortDirection = 'ASC';
                sortExp = value;
            }
        }
    
     //For sorting grid in ascending and descending order
     public String getSortDirection()
       {
            //if not column is selected 
            if (sortExpression == null || sortExpression == '')
                return 'ASC';
            else
                return sortDirection;
       }
    
    //For sorting grid in ascending and descending order
    public void setSortDirection(String value)
       {  
            sortDirection = value;
       }    
    
    //For sorting grid in ascending and descending order
    public Pagereference SortData()
       {
            displayrecords();
            return null;
       }
    
 
    
    
    /// <summary>
   /// displaydata(): to display the report for the scheduled worklist for analyst and manager.
   /// </summary>
   /// <returns> records for read only  
     public pageReference displayrecords()
    {   
    	strReviewStatusC ='Completed';
        objWorkList=new list<CommonUserClass>();
        worklistBlock=true;
        counter=0;
        list_size = worklistReportSize;
        strMainQuery=' SELECT Analyst_for_Next_Review__c,Analyst_Name__c,City__c,Contact_name__c,Credit_Analysis_ID__c,Fiscal_Year_End__c,'+
                         'Globe_ID__c,Group_Id__c,IsActive__c,IsPotential_Supplier__c,Last_Financial_Statement_Received__c,Manager__c,Manager_Name__c,'+
                        ' Next_Review_Analyst_Name__c,Next_Review_Date__c,Rating__c,Rating_Type__c,Reviewed_by__c,Review_Status__c,Risk_Level_Type__c,'+
                         'Risk_Level__c,State_Province__c,Spend__c,State_Name__c,Sub_Zone__c,Sub_Zone_Name__c,Supplier_Name__c,Zone__c,'+
                        ' Zone_Name__c,Supplier_ID__c,Supplier_Category__c,Supplier_Category_Name__c from BSureS_Basic_Info__c'+
                          '  where  Next_Review_Date__c >= today and  Next_Review_Date__c = NEXT_N_DAYS:60  and Review_Status__c!=: strReviewStatusC ';
        
         if(strSubordinateNames!=null && strSubordinateNames!='' && strSubordinateNames=='ALL' && !loginUserProfile.equalsIgnoreCase('System Administrator'))
            {
                strMainQuery+= '  And Manager__c =:userName';
                
        }else if(strSubordinateNames!=null && strSubordinateNames!='' && strSubordinateNames!='ALL')
            {
                strMainQuery+= 'And Analyst__c =:strSubordinateNames';
            }
        
        strMainQuery+= ' ORDER BY  '+ string.escapeSingleQuotes(sortExpression) + ' ' + string.escapeSingleQuotes(sortDirection) ;
        if(Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable())
        	listCustomerBasicInfo = Database.Query(strMainQuery);
        total_size=listCustomerBasicInfo.size();
        pagenation();
        return null;
    }
    
     //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    
    public pagereference pagenation()
    {
        displayPanel=true;
        string strQuerys;
        worklistBlock=true;
        PrevPageNumber=counter+1;
        NxtPageNumber=counter+listCustomerBasicInfo.size();
        objWorkList=new list<CommonUserClass>();
        if(isexport==true){
              strQuerys=strMainQuery;
        }
        else{
        	string strLimit = ' limit :list_size  offset :counter';
            //strQuerys = strMainQuery +'  limit :list_size  offset :counter';
            strQuerys = strMainQuery + string.escapeSingleQuotes(strLimit);
        }
        if(Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable())
        listCustomerBasicInfo = Database.query(strQuerys);
        set<string> userIds=new set<string>();
        set<string> managerIds=new set<string>();
       
        for(BSureS_Basic_Info__c basicCustomerInfo:listCustomerBasicInfo)
        {
            userIds.add(basicCustomerInfo.id);
            managerIds.add(basicCustomerInfo.Manager__c);
        }
        list<User> listUser;
        if(Schema.sobjecttype.User.isQueryable())
        	listUser = [SELECT Id,LastName,FirstName  FROM User where  Id in :managerIds];
        map<string,string> mapManagerName=new  map<string,string>();             
       
        for(User u:listUser)
        {
            mapManagerName.put(u.Id,u.FirstName+' ' +u.LastName);
            
        }
        if(Schema.Sobjecttype.BSureS_Spend_History_Publish__c.isQueryable())
        listSpendHistory=[SELECT Globe_ID__c,Spend_Amount__c,Spend_Period__c,Supplier_ID__c from BSureS_Spend_History_Publish__c WHERE Supplier_ID__c =:userIds];
        
        map<string,BSureS_Spend_History_Publish__c> mapObjPublish=new map<string,BSureS_Spend_History_Publish__c>();
      
        for(BSureS_Spend_History_Publish__c dataPublish:listSpendHistory)
        {
            mapObjPublish.put(dataPublish.Supplier_ID__c,dataPublish);
        }
        for(BSureS_Basic_Info__c customerInfo:listCustomerBasicInfo)
        {
            CommonUserClass objListCustomer=new CommonUserClass();
            userIds.add(customerInfo.id);
            objListCustomer.supId=customerInfo.id;
            objListCustomer.strSupplierrName=customerInfo.Supplier_Name__c; 
            objListCustomer.strGlobeId=customerInfo.Globe_ID__c;
            objListCustomer.strAnalyst=customerInfo.Analyst_Name__c;
            objListCustomer.strNextReviewDate=customerInfo.Next_Review_Date__c;
            objListCustomer.strRating=customerInfo.Rating__c;
            objListCustomer.strRiskLevelType=customerInfo.Risk_Level__c;
            objListCustomer.strState=customerInfo.State_Name__c;
            objListCustomer.strCity=customerInfo.City__c;
            objListCustomer.strSupplierCategory=customerInfo.Supplier_Category_Name__c;
            objListCustomer.strManager=mapManagerName.get(customerInfo.Manager__c);
             BSureS_Spend_History_Publish__c  ObjPublish=mapObjPublish.get(customerInfo.id);
             if(ObjPublish!=null) 
             {
                objListCustomer.strSpendAmount=ObjPublish.Spend_Amount__c;
                objListCustomer.strSpendPeriod=ObjPublish.Spend_Period__c;
             }
             objWorkList.add(objListCustomer);
             //system.debug('objWorkList****'+objWorkList);
             
         }
            
        if( objWorkList.size()== 0)
                     {
                         exportTrue=true;
                         ApexPages.Message myEMsg=new ApexPages.Message(ApexPages.severity.INFO,System.label.BSureC_No_Record_Found);
                         ApexPages.addMessage(myEMsg);
                       }else{
                 exportTrue=false;
                     }
                    
                     return null; 
    }
    
    public pageReference getSupplier()
        {
            String id=Apexpages.currentPage().getParameters().get('supId');
            pagereference pg=new pagereference('/'+id);
            return pg;
        }
        
    /// <summary>
   /// CommonUserClass: A wraper class to fetch the records from different objects.
   /// </summary>
   /// <returns> records for read only  
    public class CommonUserClass
    {
        public string strSupplierrName{get;set;}// to hold supplier name
        public string strGroupId{get;set;}// to hold groupId
        public string strGlobeId{get;set;}//to hold globeId
        public string strManager{get;set;}//to hold manager name
        public date strLastReviewDate{get;set;}//to hold last Review Date
        public date strNextReviewDate{get;set;}//to hold next Review Date
        public string strState{get;set;}//to hold State
        public string strRating{get;set;}//to hold Rating
        public string strRatingType{get;set;}//to hold Rating Type
        public string strRiskCategory{get;set;}//to hold risk category
        public string strReviewStatus{get;set;}//to hold Review Status
        public string strRiskLevel{get;set;}//to hold Risk level
        public string strRiskLevelType{get;set;}//to hold risk level type
        public string strAssignedUser{get;set;}//to hold assigned user
        public string strAnalyst{get;set;}//to hold analyst
        public string strSupplierCategory{get;set;}//to hold supplier category
        public double strSpendAmount{get;set;}//to hold spend amount
        public date strSpendPeriod{get;set;}//to hold spend period
        public string supId{get;set;}//to hold supId
        public string strCity{get;set;}
        
        
    }
    /// <summary>
   /// This method is for pagination, if atleast one record found then passing 1 to the pagination method
   /// <param name="PageSize"></param>
   /// <returns>Integer</returns>  
   /// </summary>   
      public PageReference FirstbtnClick() { //user clicked beginning
          counter = 0;
          pagenation();
              return null;
           }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
       public PageReference previousBtnClick() { //user clicked previous button
          counter -= list_size;
          pagenation();
          return null;
       }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
       public PageReference nextBtnClick() { //user clicked next button
          counter += list_size;
          pagenation();
          return null;
       }
       
      
   // <summary>
   // Below method fires when user clicks on previous button of pagination.
   // </summary>
   // <returns>pagereference</returns>
       public PageReference LastbtnClick() { //user clicked end
          counter = total_size - math.mod(total_size, list_size);
          pagenation();
          return null;
       }
       
       
   /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
       public Boolean getPreviousButtonEnabled() {
          //this will disable the previous and beginning buttons
          if (counter>0) return false; else return true;
       }
       
       
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.  
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
       public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
          if (counter + list_size < total_size) return false; else return true;
       }
     
    
     
       public Integer getPageSize() {
          return total_size;
       }
     
     
     
       public Integer getPageNumber() {
          return counter/list_size + 1;                     
       }
             
       
  /// <summary>
  /// Below method gets the total no.of pages.
  /// <param name="TotalPageNumber"></param>
  /// <returns>Integer</returns>    
  /// </summary>
       public Integer getTotalPageNumber() {
          if (math.mod(total_size, list_size) > 0) {
             return total_size/list_size + 1;
          } else {
             return (total_size/list_size);
          }
      
       } 

  		public pageReference  cancel()
       {
         pageReference pRef = new pageReference('/apex/BSureS_Reports');
         return pRef;
       }
       
}