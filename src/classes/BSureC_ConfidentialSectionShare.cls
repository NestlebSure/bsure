/***********************************************************************************************
*       Controller Name : BSureC_ConfidentialSectionShare
*       Date            : 11/07/2012 
*       Author          : J.Veereandranath
*       Purpose         : This class is used to share Confidential records to Users. 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       23/11/2012                J.Veereandranath        Initial Version  
        22/01/2012                J.Veereandranath        Implemeneted Email Functionality                
**************************************************************************************************/
global with sharing class BSureC_ConfidentialSectionShare {
    public string strGroupName {get;set;} // to store Group Name
    public string strGroupSearch {get;set;} // to store search string
    public string strConShareId {get;set;}// to store share record id
    public list<selectOption> lstGroups{get;set;} // to store list of Group Names
    public list<string> lstAvailUser{get;set;} // to store list Available Users
    public list<string> lstShareUser{get;set;} // to store list of Shared Users
    public list<string> lstSelectedUser{get;set;} // to store Selected users
    public list<BSureC_Confidential_Documents_Section__Share> lstConShare {get;set;} // to store list of Share Records
    public Set<string> setLeftValues = new Set<string>(); // to store left Values profiles or Users
    public Set<string> setRightValues = new Set<string>(); // to store right Values profiles or Users
    public map<string,string> mapParams{get;set;} // to store Url Parameters
    public map<Id,User> mapUserName {get;set;} // to store User Id and User Record
    public list<string> ProfilesNotToInclude  = new list<string>{'%Collector%','%Sales%','%Customer Service%','%visitor%'};
    //public string[] RolesDoNot = new string[]{'%Customer Service%','%Collector%','On Going Collector','Sales','Un-authorized Collector','Un-earned Collector','Buyer','%Procurement Manager%'};
    public list<string> RolesDoNot{get;set;}
    public string supplierId;
    public string strSearch;  
    //Constructor
    public BSureC_ConfidentialSectionShare()
    {
        
        getRolesFromCustomSettings();
        strSearch =     '%'+'BSureC'+'%';
        lstConShare = new list<BSureC_Confidential_Documents_Section__Share>();
        mapParams = new map<string,string>();
        mapParams = apexpages.currentPage().getParameters();
        lstSelectedUser = new list<string>();
        lstGroups = new list<SelectOption>();
        lstAvailUser = new list<string>();
        lstShareUser = new list<string>();
        lstGroups.add(new SelectOption('Roles','Roles'));
        lstGroups.add(new SelectOption('Users','Users'));
        ConfidentialQuery();
         if(apexpages.currentpage().getParameters().get('cusId') != null)
            supplierId = apexpages.currentpage().getParameters().get('cusId') ;
        //NAGA Check if the user has read access on the Users field
      	if(Schema.sObjectType.User.isAccessible())
        mapUserName = new map<id,User>([select Id,Name from User limit 10000]);
        getRoles();
    }
    
    
    public void getRolesFromCustomSettings(){
        RolesDoNot = new list<string>();
        map<String, List<String>> mapConfigSettings = new map<String, List<String>>();
        mapConfigSettings = BSureC_CommonUtil.configSettingsMap;
        
        if(mapConfigSettings.get('bsurec_customerservice') != null
            && mapConfigSettings.get('bsurec_customerservice').get(0) != null)
        {
            RolesDoNot.add('%'+mapConfigSettings.get('bsurec_customerservice').get(0)+'%');
        }
        if(mapConfigSettings.get('bsurec_collector') != null
            && mapConfigSettings.get('bsurec_collector').get(0) != null)
        {
            RolesDoNot.add('%'+mapConfigSettings.get('bsurec_collector').get(0)+'%');
        }
        if(mapConfigSettings.get('bsurec_sales') != null
            && mapConfigSettings.get('bsurec_sales').get(0) != null)
        {
            RolesDoNot.add('%'+mapConfigSettings.get('bsurec_sales').get(0)+'%');
        }
        if(mapConfigSettings.get('bsurec_buyerrole') != null
            && mapConfigSettings.get('bsurec_buyerrole').get(0) != null)
        {
            RolesDoNot.add('%'+mapConfigSettings.get('bsurec_buyerrole').get(0)+'%');
        }
        if(mapConfigSettings.get('bsures_procurementmanagerrole') != null
            && mapConfigSettings.get('bsures_procurementmanagerrole').get(0) != null)
        {
            RolesDoNot.add('%'+mapConfigSettings.get('bsures_procurementmanagerrole').get(0)+'%');
        }
        
    }
     // <Summary> 
    //    Method Name : getProfiles
    // </Summary>
    public void getRoles(){   
       
       //NAGA Check if the user has read access on the UserRole object
      if(Schema.sObjectType.UserRole.isAccessible())
	  { 
	  		list<UserRole> lstRoles = [select Id,Name from UserRole Where (Not Name Like:RolesDoNot) Order by Name];
        	for(UserRole r:lstRoles){
            	setLeftValues.add(r.Id);
        	}
	  }
    }
     // <Summary> 
    //    Method Name : getGroups
    // </Summary>
    //    Description : querying profiles and Users and Preparig left Values
    public void getGroups(){
        setLeftValues.clear();
        setRightvalues.clear();
        if(strGroupName == 'Roles')
        {           
            getRoles();
        }
        else if(strGroupName == 'Users')
        {   
            //NAGA Check if the user has read access on the User object
      		if(Schema.sObjectType.User.isAccessible())
	  		{
            		list<User> lstUser= [select Id,Name from User where Profile.Name Like:strSearch and IsActive = true and (Not Profile.Name Like:ProfilesNotToInclude)  and Id !=:UserInfo.getUserId() ];
            		for(User objUser:lstUser){
                	setLeftValues.add(objUser.Id);
            		}
	  		}
        }
    }
     // <Summary> 
    //    Method Name : DeleteShare
    // </Summary>
    //    Description : Deleting Sharing records
    public void DeleteShare()
    {
        BSureC_Confidential_Documents_Section__Share share;
        setRightValues.clear();
        setLeftValues.clear();
        getRoles();
        share = new BSureC_Confidential_Documents_Section__Share(Id=strConShareId);
        if(BSureC_Confidential_Documents_Section__Share.sObjectType.getDescribe().isDeletable()){
           Delete share;   
          }
        ConfidentialQuery();
    }
     // <Summary> 
    //    Method Name : unselectclick
    // </Summary>
    //    Description : selected values removing from the list
    public void selectclick(){
        lstShareUser.Clear();
        for(String s: lstAvailUser){
            setLeftValues.remove(s);
            setRightValues.add(s);
        }       
    }
    
    // <Summary> 
    //    Method Name : unselectclick
    // </Summary>
    //    Description : selected values removing from the list
    public void unselectclick(){
        lstAvailUser.clear();
        for(String s : lstShareUser){
            setRightValues.remove(s);
            setLeftValues.add(s);
        }
    }
    // <Summary> 
    //    Method Name : getunSelectedValues
    // </Summary>
    //    Description : Preparing selectoptions
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> lstOptions = new List<SelectOption>();
        List<string> lstSelectedUser = new List<String>();
        lstSelectedUser.addAll(setLeftValues);
        lstSelectedUser.sort();
       if(strGroupName == 'Roles' || strGroupName == null  )
        {
            for(UserRole r : QuerySelectedRoles(lstSelectedUser)){
                lstOptions.add(new SelectOption(r.Id,r.Name));              
            }
            return lstOptions;
        }else {
            for(User u: QuerySelectedUsers(lstSelectedUser)){
                if(u != null)
                lstOptions.add(new SelectOption(u.Id,u.Name));
            }
            return lstOptions;
        }
        
    }
    
    // <Summary> 
    //    Method Name : getSelectedValues
    // </Summary>
    //    Description : Preparing selectoptions
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> lstOptions = new List<SelectOption>();
        lstSelectedUser = new List<String>();
        lstSelectedUser.addAll(setRightValues);
        lstSelectedUser.sort();
        
       if(strGroupName == 'Roles'  || strGroupName == null)
        {
            for(UserRole r: QuerySelectedRoles(lstSelectedUser)){
                lstOptions.add(new SelectOption(r.Id,r.Name));              
            }
            return lstOptions;
        }else {
            for(User u: QuerySelectedUsers(lstSelectedUser)){
                if(u != null)
                lstOptions.add(new SelectOption(u.Id,u.Name));
            }
            return lstOptions;
        }
    }
   // <Summary> 
     //   Method Name : Save
     // </Summary>
     //   Description : Saving the  Confidential share Records to User   
    public pagereference Save(){
        list<BSureC_Confidential_Documents_Section__Share>   lstConfShare = new list<BSureC_Confidential_Documents_Section__Share>();
        list<User> lstUser = new list<User>();
        string strEmailBody = '';
        string strFileLink = '';
        //NAGA Check if the user has read access on the BSureC_Confidential_Documents_Section__c object
      	if(Schema.sObjectType.BSureC_Confidential_Documents_Section__c.isAccessible())
	  	{
	        BSureC_Confidential_Documents_Section__c CInfo = [select Id,Title__c, TitleDescription__c,Customer_ID__r.Customer_Name__c,Customer_ID__r.Notification_Flag__c,CreatedById from BSureC_Confidential_Documents_Section__c where Id=:mapParams.get('parentId')];
		  	BSureC_Email_Queue__c objEmailQueue;
	        BSureC_Confidential_Documents_Section__Share  objConfShare;        
	      
	        if(lstSelectedUser != null && strGroupName == 'Roles')
	        {
	            list<User> allUsers = new list<User>();
	            //NAGA Check if the user has read access on the UserRole object
				if(Schema.sObjectType.UserRole.isAccessible() )
				{
	            	list<UserRole> selectedRoles = [select Id,Name,(select Id,Name,Profile.Name,Email, Receive_Email_Notifications__c, Receive_Daily_Digests__c from Users WHERE IsActive=true) from UserRole where Id In:lstSelectedUser ] ;
	            	for(UserRole u:selectedRoles){
	                    	allUsers.addALL(u.Users);
	            	}
				}
	            for(User u:allUsers){
	                if(u.Profile.Name.startsWith('BSureC') && CInfo.CreatedById != u.Id )
	                    lstUser.add(u);
	            }
	        }else if(strGroupName == 'Users')
	        	{
	            	//NAGA Check if the user has read access on the User object
					if(Schema.sObjectType.User.isAccessible() )
					{
	            		lstUser = [SELECT   Id,Name,Email, Receive_Email_Notifications__c, Receive_Daily_Digests__c
	                                FROM User 
	                                WHERE Id in:lstSelectedUser and Id !=:CInfo.CreatedById];
	        
					}	        		
	        	} 
        
		        list<BSureC_Email_Queue__c> EmailQueuelist = new list<BSureC_Email_Queue__c>();
		        strFileLink = CInfo.Title__c;
		        string strCustNameTemp = CInfo.Customer_ID__r.Customer_Name__c;
		        //system.debug('strFileLink+++++++++' + strFileLink);
		        if(strFileLink.contains('href="'))
		        {
		            strFileLink = CInfo.Title__c.replace('href="', 'href="' + URL.getSalesforceBaseUrl().toExternalForm());
		        }
		        //system.debug('strFileLink+++++++++' + strFileLink);
		        strEmailBody  = '<table cellspacing="2" cellpadding="0" border="1">';
		        //strEmailBody += '<tr><td height="30" colspan="2"><centre>Nestlé Customer Forum</centre></td></tr>';
		        strEmailBody += '<tr><td height="21px" width="22%">Customer: </td><td height="21px"> ' + strCustNameTemp +'</td></tr>';
		        strEmailBody += '<tr><td height="21px" width="22%">Section: </td><td height="21px"> Confidential Document Shared with you.</td></tr>';
		        //strEmailBody += '<tr><td height="21px" width="22%">Type </td><td height="21px">: ' + strParamReplace +'</td></tr>';
		        strEmailBody += '<tr><td height="21px" width="22%">Title: </td><td height="21px"> ' + CInfo.TitleDescription__c +'</td></tr>';
		        strEmailBody += '<tr><td height="21px" width="22%">Posted By: </td><td height="21px"> ' + UserInfo.getName() +'</td></tr>';
		        strEmailBody += '<tr><td height="21px" width="22%">Posted Date: </td><td height="21px"> '+ System.dateTime.Now().format('EEEE, MMM d, yyyy HH:mm:ss aaa z') +'</td></tr>';
		        strEmailBody += '<tr><td height="21px" colspan="2">To post a reply or view the document, please click here: ' + strFileLink + '</td></tr>';
		        strEmailBody += '<tr><td height="20" colspan="2"></td></tr>';
		        strEmailBody += '</table>';
		        
			  	
		        
		        for(User u:lstUser)
		        {
		            //string  PrepareHtml;
		            objEmailQueue = new BSureC_Email_Queue__c();
		            objConfShare = new BSureC_Confidential_Documents_Section__Share();
		            objConfShare.AccessLevel = 'Read';
		            //objConfShare.RowCause = 'Manual'; 
		            objConfShare.UserOrGroupId = u.Id;
		            objConfShare.ParentId = mapParams.get('parentId');
		            lstConfShare.add(objConfShare);
		            /* 
		            PrepareHtml = '<table cellpadding=\'2\'cellspacing=\'0\' >';
		            //PrepareHtml += '<tr> <td> Dear  '+ u.Name +'</td></tr>';
		            PrepareHtml += '<tr> <td> The following document was shared with you, by '+ userInfo.getName()+' </td></tr>';
		            PrepareHtml += '<tr><td><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+CInfo.Id+'" >'+CInfo.Title__c +'</a> </td></tr>';
		            //PrepareHtml += '<tr> <td>'+ system.label.BSureC_Thanks +'</td></tr>';
		            //PrepareHtml += '<tr> <td>'+ system.label.BSureC_Admin +'</td></tr>';
		            PrepareHtml += '</table>';
		            */
		            
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Body__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Body__c.isCreateable()){                        
		                    objEmailQueue.Email_Body__c = strEmailBody;
		            }
		
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Priority__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Priority__c.isCreateable()){                        
		                    objEmailQueue.Email_Priority__c = 'High';
		            }
		            
		            if(CInfo.Customer_ID__r.Notification_Flag__c)
		            {
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Priority__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Priority__c.isCreateable()){                                    
		                    objEmailQueue.Email_Status__c = 'NEW';
		            }
		                if(u.Receive_Email_Notifications__c)
		                {
		                    objEmailQueue.Email_Status__c = 'NEW';
		                }
		                else
		                {
		                    objEmailQueue.Email_Status__c = 'SENT';
		                }
		            }
		            else
		            {
		                objEmailQueue.Email_Status__c = 'SENT';
		            }
		            
		            if(u.Receive_Daily_Digests__c)
		            {
		                if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Is_Daily_Digest__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Is_Daily_Digest__c.isCreateable()){                                                
		                    objEmailQueue.Is_Daily_Digest__c = 'YES';
		                }
		            }
		            else
		            {
		                if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Is_Daily_Digest__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Is_Daily_Digest__c.isCreateable()){                                                            
		                    objEmailQueue.Is_Daily_Digest__c = 'NO';
		                }
		            }
		            
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Subject__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Subject__c.isCreateable()){                                                
		                    objEmailQueue.Email_Subject__c = 'Updates for ' + strCustNameTemp + ' : Manual Sharing';
		            }
		            
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Recipient_Address__c .isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Recipient_Address__c .isCreateable()){                                                            
		                    objEmailQueue.Recipient_Address__c = u.Email;
		            }
		            
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Send_Immediate__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Send_Immediate__c.isCreateable()){                                                            
		                    objEmailQueue.Send_Immediate__c = true;
		            }
		            
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Recipient_Name__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Recipient_Name__c.isCreateable()){                                                                    
		                    objEmailQueue.Recipient_Name__c = U.Name; 
		            }        
		
		            if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Send_On_Date__c.isUpdateable()||
		                    Schema.sObjectType.BSureC_Email_Queue__c.fields.Send_On_Date__c.isCreateable()){                                                            
		                    objEmailQueue.Send_On_Date__c = system.today();
		            }
		            
		            EmailQueuelist.add(objEmailQueue);
		        }
		        
		        if(!EmailQueuelist.isEmpty()){
		        	if(BSureC_Email_Queue__c.sObjectType.getDescribe().isCreateable())
		            Database.insert(EmailQueuelist,false);
		        } 
		             
		        if(!lstConfShare.isEmpty()){
		        	if(BSureC_Confidential_Documents_Section__Share.sObjectType.getDescribe().isCreateable())
		            insert lstConfShare;
		        }
		        ConfidentialQuery(); // Query confidential Share Records 
        
	  	}
        return cancel();
    }
     // <Summary> 
     //   Method Name : Find
     // </Summary>
     //   Description : Searching Profiles and Users with search string  
    public void Find(){
        setLeftValues.clear();
        setRightvalues.clear();
        String strgroupsearchsi = '';
        if(strGroupSearch.length() > 0)
        {
        	strgroupsearchsi = '%'+strGroupSearch+'%';
        }
        if(strGroupName == 'Roles' && strGroupSearch.length() > 0)
        {           
            //NAGA Check if the user has read access on the UserRole object
			if(Schema.sObjectType.UserRole.isAccessible() )
			{
				
	            list<UserRole> lstroles = [select Id,Name from UserRole where Name like:strgroupsearchsi and (Not Name Like:RolesDoNot) ];
	            for(UserRole role:lstroles){
	                setLeftValues.add(role.Id);
	            }
			}
        }
        else if(strGroupName == 'Users' && strGroupSearch.length() > 0)
        {           
            //NAGA Check if the user has read access on the User object
			if(Schema.sObjectType.User.isAccessible() )
			{
	            list<User> lstUser= [select Id,Name from User where Name Like:strgroupsearchsi  and Profile.Name like:strSearch and (Not Profile.Name Like:ProfilesNotToInclude)];
	            for(User objUser:lstUser){
	                setLeftValues.add(objUser.Id);
	            }
			}
        }
    }
     // <Summary> 
     //   Method Name : Cancel
     // </Summary>
     //   Description : Its Navigates to Record
    public Pagereference Cancel()
    {
        Pagereference Pageref = page.BSureC_ConfidentialSectionDetail;
        Pageref.getParameters().put('parentId',mapParams.get('parentId'));
        Pageref.getParameters().put('Name',mapParams.get('Name'));
        Pageref.getParameters().put('cusId',mapParams.get('cusId'));
        Pageref.setRedirect(true); 
        return pageref;
    }
     // <Summary> 
     //   Method Name : Cancel
     // </Summary>
     //   Description : Its Navigates to Record
    public Pagereference Cancel1()
    { 
        Pagereference Pageref = new pagereference('/'+mapparams.get('cusId'));  
        Pageref.setRedirect(true);  
        return pageref;
    }
     // <Summary>  
     //   Method Name : ConfidentialQuery
     // </Summary>
     //   Description : querying Confidential Share Records
    public void ConfidentialQuery()
    {
        //NAGA Check if the user has read access on the BSureC_Confidential_Documents_Section__Share object
		if(Schema.sObjectType.BSureC_Confidential_Documents_Section__Share.isAccessible() )
		{
        	lstConShare = [Select UserOrGroupId, ParentId,Id,AccessLevel,RowCause 
                                FROM BSureC_Confidential_Documents_Section__Share 
                                WHERE ParentId=:mapParams.get('parentId')];
		}
    }
    // <Summary> 
     //   Method Name : QuerySelectedProfiles
     // </Summary>
     //   Description : querying Selected Profile Records
    public list<UserRole> QuerySelectedRoles(list<string> roleIds){
        //NAGA Check if the user has read access on the UserRole object
		if(Schema.sObjectType.UserRole.isAccessible() )
		{
        	list<UserRole> lstURoles = [select id,name from UserRole where Id in:roleIds and (Not Name Like:RolesDoNot) order by Name];
        	return lstURoles;
		}
		else
		{
			return null;
		}
    }
    // <Summary> 
     //   Method Name : QuerySelectedUsers
     // </Summary>
     //   Description : querying selected User  Records
    public list<User> QuerySelectedUsers(list<string> userIds){
        //NAGA Check if the user has read access on the User object
		if(Schema.sObjectType.User.isAccessible() )
		{
        	list<User> lstUsers = [select id,name from User where Id in:userIds and (Not profile.Name Like:ProfilesNotToInclude)];
        	return lstUsers;
		}
		else
		{
			return null;
			
		}
    }
    // <Summary> 
     //   Method Name : Add
     // </Summary>
     //   Description : this Method navigates to Sharing Page
    public pagereference Add(){
        Pagereference Pageref = page.BSureC_ConfidentialShare;
        Pageref.getParameters().put('parentId',mapParams.get('parentId'));
        Pageref.getParameters().put('Name',mapParams.get('Name'));
        Pageref.getParameters().put('cusId',mapParams.get('cusId'));
        Pageref.setRedirect(true);
        return Pageref;
    }
}