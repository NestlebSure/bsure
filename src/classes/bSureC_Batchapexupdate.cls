/***********************************************************************************************
* Controller Name : bSureC_Batchapexupdate
* Date : 14th May,2014
* Author : Vidya Sagar H
* Purpose : To update bulk records more than 10,000 records.
* Date                  Programmer              Reason
* -------------------- ------------------- ------------------------- 
*05/14/2014(mm/dd/yy)   Vidya Sagar H         Initial version
* -------------------- ------------------- -------------------------
*/
global class bSureC_Batchapexupdate implements Database.batchable<SObject>,Database.Stateful{
	
	public list<SObject> updatelist {get;set;}// list of sobject to update
	//constructor
	global bSureC_Batchapexupdate(list<SObject> updatelist)
	{
	 this.updatelist = updatelist;
	}
	global Iterable<SObject> start(Database.BatchableContext bc) {
		return updatelist;
	}
	public void execute(Database.BatchableContext bc, list<SObject> scope) {
        update scope;
	}
	 global void finish(Database.BatchableContext bc) {
       system.debug('-----'+updatelist.size());
    }
}