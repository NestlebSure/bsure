/************************************************************************************************       
Controller Name         : BSureC_paymenthistorybean       
Date                    : 01/03/2012        
Author                  : Santhosh Palla       
Purpose                 : This is Payment History Bean 
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          01/03/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_paymenthistorybean 
{
        public String divisionname{get;set;}
        public String groupid{get;set;}
        public Decimal higcredit{get;set;}
        public Decimal totaldue{get;set;}
        public Decimal pastdue{get;set;}
        public Decimal percentcurrent{get;set;}
}