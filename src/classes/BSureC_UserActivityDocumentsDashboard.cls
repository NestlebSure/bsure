/***********************************************************************************************
* Controller Name   : BSureC_UserActivityDocumentsDashboard 
* Date              : 
* Author            : Neha Jaiswal  
* Purpose           : Class for customer User Activity Documents Dashboard
* Change History    : 07/01/2013
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 03/01/2012                                            Initial Version
**************************************************************************************************/
public with sharing class BSureC_UserActivityDocumentsDashboard {
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public date dtFromDate{get;set;} // to store from date
    public date dtToDate{get;set;} // to strore to Date
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
     public Boolean idsflag = false;//for commonnutil class
     
    public BSureC_Customer_Basic_Info__c objCustomer{get;set;} // to display from and to dates with standard datepicker
    public list<BSureC_AdditionalAttributes__c> lstUserAdditionatt{get;set;}//list of BSureC_AdditionalAttributes__c obj records
    public list<BSureC_Invoice_Collection_Section__c> lstInvoiceSection{get;set;}//list of BSureC_Invoice_Collection_Section__c obj records
    public list<BSureC_Unauthorized_Deduction_Section__c> lstUnauthorizedSection{get;set;}//list of BSureC_Unauthorized_Deduction_Section__c obj records
    public list<BSureC_Unearned_Cash_Discount_Section__c> lstUnearnedSection{get;set;}//list of BSureC_Unearned_Cash_Discount_Section__c obj records
    public list<BSureC_Confidential_Documents_Section__c> lstConfidentialSection{get;set;}//list of BSureC_Confidential_Documents_Section__c obj records
    public list<BSureC_Credit_Increase_Section__c> lstcreditanalysissection{get;set;}
    public list<BSureC_On_Going_Credit_Section__c> lstGoingSection{get;set;}//list of BSureC_On_Going_Credit_Section__c obj records
    public set<Id> setUserIds{get;set;}
    Public String strFile{get;set;}
    public map<string,list<BSureC_On_Going_Credit_Section__c>> mapGoingObj{get;set;}//to map record with list in BSureC_On_Going_Credit_Section__c
    public map<string,list<BSureC_Credit_Increase_Section__c>> mapCreditIncreaseSection{get;set;}
    public map<string,list<BSureC_Invoice_Collection_Section__c>> mapInvoiceObj{get;set;}//to map record with list in BSureC_Invoice_Collection_Section__c
    public map<string,list<BSureC_Unauthorized_Deduction_Section__c>> mapUnauthorizedObj{get;set;}//to map record with list in BSureC_Unauthorized_Deduction_Section__c
    public map<string,list<BSureC_Unearned_Cash_Discount_Section__c>> mapUnearnedObj{get;set;}//to map record with list in BSureC_Unearned_Cash_Discount_Section__c
    public map<string,list<BSureC_Confidential_Documents_Section__c>> mapConfidentialInfoObj{get;set;}//to map record with list in BSureC_Confidential_Documents_Section__c
    public map<string,Integer> mapFinal{get;set;}
    public list<UserDocumentCountWrapper> objDocumentsList {get;set;}
    
    public class UserDocumentCountWrapper//common wrapper class
    { 
        public integer uDocumentCount;
        public string  userloginmonth;
    } 
    public BSureC_UserActivityDocumentsDashboard()
    {
        objCustomer = new BSureC_Customer_Basic_Info__c();
        getZones();
        getSubZones();
        getCountries();
       getlogedinUser();
    }
    
     public void getlogedinUser()
    {
        string strUserId= userInfo.getUserId();
        system.debug('strUserId&&&'+strUserId);
        list<BSureC_AdditionalAttributes__c>  objUserDetail=[SELECT User__r.id,User__r.Name,Zone_Name__c,BSureC_Zone__c,
                                                             Sub_Zone_Name__c,BSureC_Sub_Zone__c,Country_Name__c,BSureC_Country__c 
                                                             from BSureC_AdditionalAttributes__c  WHERE User__r.Id =: strUserId];
                                                             
        if(objUserDetail !=null  && objUserDetail.size()>0)
            {
              strZoneId=     objUserDetail[0].BSureC_Zone__c;
              strSubZoneId=  objUserDetail[0].BSureC_Sub_Zone__c;
              strCountryId=  objUserDetail[0].BSureC_Country__c;
              system.debug('strZoneId&&&'+strZoneId);
              system.debug('strSubZoneId&&&'+strSubZoneId);
              system.debug('strCountryId&&&'+strCountryId);
            }
    }
    /// <summary>
    /// getZOnesOptions(): to display the dropdown list for zones from BSureC_commonutil.
    /// </summary>
   /// <returns> zones option list
    public pageReference getZones()
        {
            zoneOptions=new list<selectOption>();
            BSureC_CommonUtil.blnflag=true;
            zoneOptions=BSureC_CommonUtil.getZones();
            getSubZones();
            //getCustomers();
	        getCountries();
            return null;
       }
    
     /// <summary>
    /// getSubzonesoptions(): to display the dropdown list for subzones from BSureC_commonutil.
    /// </summary>
   /// <returns> subzones option list
    public pageReference getSubZones()
        {
            if(idsflag==false){
                
                strSubzoneId='ALL';
                strCountryId='ALL';
            }
            subZoneOptions=new list<selectoption>();
            BSureC_CommonUtil.blnflag=true;
            subZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
            getCountries();  
            //strSubzoneId='ALL';
           // getCustomers();
            return null;  
        }
    
    
     /// <summary>
    /// getCountriesOptions(): to display the dropdown list for countries from BSureC_commonutil.
    /// </summary>
   /// <returns> country option list
     public pageReference getCountries()
         {
            if(idsflag==false){
                
                
                strCountryId='ALL';
            }
         
             coutryOptions=new list<selectOption>();
             coutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
            // getCustomers();
             //strCountryId='ALL';  
             return null;
          }
          
          
    public void Search()
    {
        userLoginDocumentsCount();
    }
    
    /// <summary>
    /// userLoginDocumentsCount(): to count the documents activity by a user on customer objects
    /// </summary>
   /// <returns> subzones option list
    public void userLoginDocumentsCount()
    {
        string month='';
        string year='';
        string Month_Year;
        setUserIds = new set<Id>();
        mapInvoiceObj = new map<string,list<BSureC_Invoice_Collection_Section__c>>();
        mapUnauthorizedObj = new map<string,list<BSureC_Unauthorized_Deduction_Section__c>>();
        mapConfidentialInfoObj = new map<string,list<BSureC_Confidential_Documents_Section__c>>();
        mapUnearnedObj= new map<string,list<BSureC_Unearned_Cash_Discount_Section__c>>();
        mapCreditIncreaseSection=new map<string,list<BSureC_Credit_Increase_Section__c>>();
        mapGoingObj=new map<string,list<BSureC_On_Going_Credit_Section__c>>();
   
        objDocumentsList = new list<UserDocumentCountWrapper>();
        mapFinal = new map<string,Integer>();
        dtFromDate = objCustomer.Fiscal_Year_End__c;
        dtToDate = objCustomer.Next_Review_Date__c;
        strFile = 'File';
        
		if(Schema.sObjectType.BSureC_AdditionalAttributes__c.isAccessible())
		{ 
	        string strQuery = 'select id,User__c,User__r.Name,Zone_Name__c,BSureC_Zone__c,'+
	                                'Sub_Zone_Name__c,BSureC_Sub_Zone__c,Country_Name__c,BSureC_Country__c '+ 
	                                'from BSureC_AdditionalAttributes__c where User__r.isActive = true ';
	        if(strZoneId != null && strZoneId != 'ALL')
	        {
	            strQuery +=' AND BSureC_Zone__c =: strZoneId ';
	        }
	        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
	        {
	            strQuery +=' AND BSureC_Sub_Zone__c =: strSubZoneId ';
	        }
	        if(strCountryId != null && strCountryId != 'ALL'  )
	        {
	            strQuery +=' AND BSureC_Country__c =: strCountryId '; 
	        }   
	        lstUserAdditionatt= Database.Query(strQuery);
	        
	        if(lstUserAdditionatt != null && lstUserAdditionatt.size()>0)
	        {
	            for(BSureC_AdditionalAttributes__c objUAatt :lstUserAdditionatt )
	            {
	                setUserIds.add(objUAatt.User__c);
	            }
	        }
		}
//-------------------------------------------------------------------------------------------------------------------------------------------
		
		if(Schema.sObjectType.BSureC_Credit_Increase_Section__c.isAccessible())
		{ 
			string strQueryIncreaseStatus = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_Credit_Increase_Section__c'+
	                                        ' where Discussion_Type__c =: strFile AND CreatedById IN :setUserIds'; //Discussion_Type__c=\'File\'
	         if(dtFromDate != null)
	        {
	            strQueryIncreaseStatus += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryIncreaseStatus += ' AND CreatedDate <=:dtToDate';
	        }
	        lstcreditanalysissection = Database.query(strQueryIncreaseStatus);
	        list<BSureC_Credit_Increase_Section__c> lstIncreaseStatus = new list<BSureC_Credit_Increase_Section__c>();
	        if(lstcreditanalysissection != null && lstcreditanalysissection.size() > 0)
	        {
	            for(BSureC_Credit_Increase_Section__c objCreditStatus:lstcreditanalysissection) 
	            {
	                month = String.valueOf(objCreditStatus.CreatedDate.month());
	                year = String.valueOf(objCreditStatus.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapCreditIncreaseSection.containskey(Month_Year))
	                {
	                    lstIncreaseStatus = mapCreditIncreaseSection.get(Month_Year);
	                    lstIncreaseStatus.add(objCreditStatus);
	                    mapCreditIncreaseSection.put(Month_Year,lstIncreaseStatus);
	                }
	                else
	                {
	                    lstIncreaseStatus = new list<BSureC_Credit_Increase_Section__c>();
	                    lstIncreaseStatus.add(objCreditStatus);
	                    mapCreditIncreaseSection.put(Month_Year,lstIncreaseStatus);
	                }
	            }
	        }  
		}
//...........................................................................................................................................
        
        if(Schema.sObjectType.BSureC_On_Going_Credit_Section__c.isAccessible())
		{ 
	        string strQueryGoingReview = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_On_Going_Credit_Section__c'+
	                                        ' where Discussion_Type__c =: strFile AND CreatedById IN : setUserIds';
	        if(dtFromDate != null)
	        {
	            strQueryGoingReview += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryGoingReview += ' AND CreatedDate <=:dtToDate';
	        }
	         lstGoingSection = Database.query(strQueryGoingReview);
	        list<BSureC_On_Going_Credit_Section__c> lstCreditsReview = new list<BSureC_On_Going_Credit_Section__c>();
	        if(lstGoingSection != null && lstGoingSection.size() > 0)
	        {
	            for(BSureC_On_Going_Credit_Section__c objCreditReview: lstGoingSection)
	            {
	                month = String.valueOf(objCreditReview.CreatedDate.month());
	                year = String.valueOf(objCreditReview.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapGoingObj.containskey(Month_Year))
	                {
	                    lstCreditsReview = mapGoingObj.get(Month_Year);
	                    lstCreditsReview.add(objCreditReview);
	                    mapGoingObj.put(Month_Year,lstCreditsReview);
	                }
	                else
	                {
	                    lstCreditsReview = new list<BSureC_On_Going_Credit_Section__c>();
	                    lstCreditsReview.add(objCreditReview);
	                    mapGoingObj.put(Month_Year,lstCreditsReview);
	                }
	                
	            }
	            system.debug('-------lstCreditsReview---lstCreditsReview-'+lstCreditsReview.size());
	        } 
		}
      
//-----------------------------------------------------------------------------------------------------------------------------------------------       
        
        if(Schema.sObjectType.BSureC_Invoice_Collection_Section__c.isAccessible())
		{
	        string strQueryCreditReview = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_Invoice_Collection_Section__c'+
	                                        ' where Discussion_Type__c =: strFile AND CreatedById IN : setUserIds';
	        if(dtFromDate != null)
	        {
	            strQueryCreditReview += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryCreditReview += ' AND CreatedDate <=:dtToDate';
	        }
	        lstInvoiceSection = Database.query(strQueryCreditReview);
	        list<BSureC_Invoice_Collection_Section__c> lstCreditReview = new list<BSureC_Invoice_Collection_Section__c>();
	        if(lstInvoiceSection != null && lstInvoiceSection.size() > 0)
	        {
	            for(BSureC_Invoice_Collection_Section__c objCreditReview: lstInvoiceSection)
	            {
	                month = String.valueOf(objCreditReview.CreatedDate.month());
	                year = String.valueOf(objCreditReview.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapInvoiceObj.containskey(Month_Year))
	                {
	                    lstCreditReview = mapInvoiceObj.get(Month_Year);
	                    lstCreditReview.add(objCreditReview);
	                    mapInvoiceObj.put(Month_Year,lstCreditReview);
	                }
	                else
	                {
	                    lstCreditReview = new list<BSureC_Invoice_Collection_Section__c>();
	                    lstCreditReview.add(objCreditReview);
	                    mapInvoiceObj.put(Month_Year,lstCreditReview);
	                }
	                
	            }
	        }   
		}
//------------------------------------------------------------------------------------------------------------------------------------------- 
        
        if(Schema.sObjectType.BSureC_Unauthorized_Deduction_Section__c.isAccessible())
		{
	        string strQueryCreditStatus = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_Unauthorized_Deduction_Section__c'+
	                                        ' where Discussion_Type__c =:strFile AND CreatedById IN :'+'setUserIds';
	        if(dtFromDate != null)
	        {
	            strQueryCreditStatus += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryCreditStatus += ' AND CreatedDate <=:dtToDate';
	        }
	        lstUnauthorizedSection = Database.query(strQueryCreditStatus);
	        list<BSureC_Unauthorized_Deduction_Section__c> lstCreditStatus = new list<BSureC_Unauthorized_Deduction_Section__c>();
	        if(lstUnauthorizedSection != null && lstUnauthorizedSection.size() > 0)
	        {
	            for(BSureC_Unauthorized_Deduction_Section__c objCreditStatus:lstUnauthorizedSection) 
	            {
	                month = String.valueOf(objCreditStatus.CreatedDate.month());
	                year = String.valueOf(objCreditStatus.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapUnauthorizedObj.containskey(Month_Year))
	                {
	                    lstCreditStatus = mapUnauthorizedObj.get(Month_Year);
	                    lstCreditStatus.add(objCreditStatus);
	                    mapUnauthorizedObj.put(Month_Year,lstCreditStatus);
	                }
	                else
	                {
	                    lstCreditStatus = new list<BSureC_Unauthorized_Deduction_Section__c>();
	                    lstCreditStatus.add(objCreditStatus);
	                    mapUnauthorizedObj.put(Month_Year,lstCreditStatus);
	                }
	            }
	        }  
		}
//--------------------------------------------------------------------------------------------------------------------------------------------
        
        if(Schema.sObjectType.BSureC_Unearned_Cash_Discount_Section__c.isAccessible())
		{
	        string strQueryUnearnedStatus = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_Unearned_Cash_Discount_Section__c'+
	                                        ' where Discussion_Type__c =: strFile AND CreatedById IN :'+'setUserIds';
	        if(dtFromDate != null)
	        {
	            strQueryUnearnedStatus += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryUnearnedStatus += ' AND CreatedDate <=:dtToDate';
	        }
	        lstUnearnedSection = Database.query(strQueryUnearnedStatus);
	        list<BSureC_Unearned_Cash_Discount_Section__c> lstCredtStatus = new list<BSureC_Unearned_Cash_Discount_Section__c>();
	        if(lstUnearnedSection != null && lstUnearnedSection.size() > 0)
	        {
	            for(BSureC_Unearned_Cash_Discount_Section__c objCreditStatus:lstUnearnedSection) 
	            {
	                month = String.valueOf(objCreditStatus.CreatedDate.month());
	                year = String.valueOf(objCreditStatus.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapUnearnedObj.containskey(Month_Year))
	                {
	                    lstCredtStatus = mapUnearnedObj.get(Month_Year);
	                    lstCredtStatus.add(objCreditStatus);
	                    mapUnearnedObj.put(Month_Year,lstCredtStatus);
	                }
	                else
	                {
	                    lstCredtStatus = new list<BSureC_Unearned_Cash_Discount_Section__c>();
	                    lstCredtStatus.add(objCreditStatus);
	                    mapUnearnedObj.put(Month_Year,lstCredtStatus);
	                }
	            }
	        }  
		}       
//--------------------------------------------------------------------------------------------------------------------------------------------   
        
        if(Schema.sObjectType.BSureC_Confidential_Documents_Section__c.isAccessible())
		{
	        string strQueryConfidentialinfo = 'select id,CreatedById,CreatedDate,Discussion_Type__c from BSureC_Confidential_Documents_Section__c'+
	                                            ' where Discussion_Type__c =: strFile AND CreatedById IN :'+'setUserIds';
	        if(dtFromDate != null)
	        {
	            strQueryConfidentialinfo += ' AND CreatedDate >=: dtFromDate';
	        }
	        if(dtToDate != null)
	        {
	            strQueryConfidentialinfo += ' AND CreatedDate <=:dtToDate';
	        }
	        lstConfidentialSection = Database.query(strQueryConfidentialinfo);
	        list<BSureC_Confidential_Documents_Section__c> lstConfidentialInfo = new list<BSureC_Confidential_Documents_Section__c>();
	        if(lstConfidentialSection != null && lstConfidentialSection.size() >0 )
	        {
	            for(BSureC_Confidential_Documents_Section__c objConfidentialInfo : lstConfidentialSection)
	            {
	                month = String.valueOf(objConfidentialInfo.CreatedDate.month());
	                year = String.valueOf(objConfidentialInfo.CreatedDate.year());
	                Month_Year = year+'-'+month;
	                if(mapConfidentialInfoObj.containskey(Month_Year))
	                {
	                    lstConfidentialInfo = mapConfidentialInfoObj.get(Month_Year);
	                    lstConfidentialInfo.add(objConfidentialInfo);
	                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
	                }
	                else
	                {
	                    lstConfidentialInfo = new list<BSureC_Confidential_Documents_Section__c>();
	                    lstConfidentialInfo.add(objConfidentialInfo);
	                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
	                }
	            }
	        } 
		}
//--------------------------------------------------------------------------------------------------------------------------------------------      
        for(string strTime : mapInvoiceObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapInvoiceObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapInvoiceObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
//------------------------------------------------------------
        for(string strTime : mapUnauthorizedObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapUnauthorizedObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapUnauthorizedObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
//---------------------------------------------------------------
        for(string strTime : mapUnearnedObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapUnearnedObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapUnearnedObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
        
        for(string strTime : mapConfidentialInfoObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapConfidentialInfoObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapConfidentialInfoObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
  		 for(string strTime : mapCreditIncreaseSection.keySet())
         {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapCreditIncreaseSection.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapCreditIncreaseSection.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
         }
         for(string strTime : mapGoingObj.keySet())
         {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapGoingObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapGoingObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
         }
             
        for(string strTimeFinal : mapFinal.keySet())
        {
            UserDocumentCountWrapper objNew = new UserDocumentCountWrapper();
            objNew.uDocumentCount = mapFinal.get(strTimeFinal);
            objNew.userloginmonth = strTimeFinal;
            objDocumentsList.add(objNew);
        }
            UserDocumentCountWrapper objNew = new UserDocumentCountWrapper();
            objNew.uDocumentCount = 0;
            objNew.userloginmonth ='';
            objDocumentsList.add(objNew);                                     
    } 
    
}