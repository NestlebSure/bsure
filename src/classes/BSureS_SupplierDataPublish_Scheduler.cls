/************************************************************************************************       
Controller Name 		: BSureS_SupplierDataPublish_Scheduler       
Date                    :  
Author                  :        
Purpose         		: To Inser the Supplier bulk upload data File into a supplier main Object       
Change History 			: 
Date          				 Programmer                     Reason       
--------------------      -------------------    -------------------------       
 09/04/2013                 kishorekuamr.a			        assign analyst as a owner 
**************************************************************************************************/
global with sharing class BSureS_SupplierDataPublish_Scheduler implements Database.Batchable<SObject> 
{
	global String str_SupplierInfo_stage = null;
	public list<BSureS_Basic_Info_Stage__c> lst_supplierstage = new list<BSureS_Basic_Info_Stage__c>();
	public List<BSureS_Basic_Info__c> lst_Supplier = new List<BSureS_Basic_Info__c>();
	
	/// <summary>
	/// constructor
	/// </summary>
	public BSureS_SupplierDataPublish_Scheduler()
	{
		//below qureying the spend history stage records
		str_SupplierInfo_stage= ' SELECT id, Supplier_ID__c, Supplier_Name__c,ISPOTENTIAL_SUPPLIER__C,GLOBE_ID__C,OWNER_SHIP__C,SOLE_SOURCED__C,CONTACT_NAME__C, '
								+ ' F_S__C,NOTIFICATION_FLAG__C,PLANNED_REVIEW_DATE__C,FINANCIAL_INFORMATION__C,FISCAL_YEAR_END__C,SPEND__C,LAST_FINANCIAL_STATEMENT_RECEIVED__C,'
								+ ' SUPPLIER_CONTACT__C,EMAIL_ADDRESS__C,PHONE_NUMBER__C,FAX__C,WEBSITE_LINK__C,STREET_NAME_ADDRESS_LINE_1__C,ADDRESS_LINE_2__C,CITY__C, '
								+ ' POSTAL_CODE__C,Supplier_Category__c,Zone__c,Sub_Zone__c,BSureS_Country__c,State_Province__c,Supplier_Zones__c,Supplier_SubZones__c, '
								+ ' Supplier_Countries__c,Manager__c,Analyst__c, Bakup_Analysts__c, Bakup_Analysts_Names__c, BulkUpload_BuyerID__c '+
		 						+ ' FROM BSureS_Basic_Info_Stage__c WHERE Exception__c=NULL and Publish_Flag__c=false order by Supplier_Name__c';
	}
	
	/// <summary>
	/// start method fires when class get executes
	/// </summary>
	///<returns>query string of object</returns>
	global Iterable<sObject> start (Database.BatchableContext ctx)
	{         
	   return Database.query(str_SupplierInfo_stage);
	}	
	 
	/// <summary>
	/// execute method executes after start method to insert records in batch into publish object
	/// </summary>
	/// <param name="BC"></param>
	/// <param name="scope"></param>
     global void execute(Database.BatchableContext BC, list<BSureS_Basic_Info_Stage__c> scope)
     {
     	list<user> lstUserID=[select Id  from User where IsActive=True and Profile.Name like 'System Administrator' limit 1];
     	system.debug('lstUserID.get(0).Id####'+lstUserID.get(0).Id); 
     	lst_supplierstage=new list<BSureS_Basic_Info_Stage__c>();
 		for(BSureS_Basic_Info_Stage__c  obj : scope)
		{
			BSureS_Basic_Info__c rec_supplier_publish = new BSureS_Basic_Info__c();
			//rec_supplier_publish.Supplier_ID__c=obj.Supplier_ID__c;
			//rec_supplier_publish.Review_Status__c = 'Scheduled'; //Aditya V Hardcoded as it is from Bulk Upload and is Mandatory.
	        rec_supplier_publish.SUPPLIER_NAME__C= obj.SUPPLIER_NAME__C;
            rec_supplier_publish.ISPOTENTIAL_SUPPLIER__C = obj.ISPOTENTIAL_SUPPLIER__C;       
            rec_supplier_publish.GLOBE_ID__C = obj.GLOBE_ID__C;
            rec_supplier_publish.OWNER_SHIP__C = obj.OWNER_SHIP__C;
            rec_supplier_publish.SOLE_SOURCED__C = obj.SOLE_SOURCED__C;
            rec_supplier_publish.CONTACT_NAME__C = obj.CONTACT_NAME__C;
            rec_supplier_publish.F_S__C = obj.F_S__C;
            rec_supplier_publish.NOTIFICATION_FLAG__C = obj.NOTIFICATION_FLAG__C;
            rec_supplier_publish.PLANNED_REVIEW_DATE__C=obj.PLANNED_REVIEW_DATE__C;
            rec_supplier_publish.Next_Review_Date__c = obj.PLANNED_REVIEW_DATE__C;
            rec_supplier_publish.FINANCIAL_INFORMATION__C= obj.FINANCIAL_INFORMATION__C;
            rec_supplier_publish.FISCAL_YEAR_END__C = obj.FISCAL_YEAR_END__C;       
            rec_supplier_publish.SPEND__C = obj.SPEND__C;
            rec_supplier_publish.LAST_FINANCIAL_STATEMENT_RECEIVED__C = obj.LAST_FINANCIAL_STATEMENT_RECEIVED__C;
            rec_supplier_publish.SUPPLIER_CONTACT__C = obj.SUPPLIER_CONTACT__C;
            rec_supplier_publish.EMAIL_ADDRESS__C=obj.EMAIL_ADDRESS__C;
            rec_supplier_publish.PHONE_NUMBER__C = obj.PHONE_NUMBER__C;
            rec_supplier_publish.FAX__C = obj.FAX__C;            
            rec_supplier_publish.WEBSITE_LINK__C = obj.WEBSITE_LINK__C;
            rec_supplier_publish.STREET_NAME_ADDRESS_LINE_1__C = obj.STREET_NAME_ADDRESS_LINE_1__C;
            rec_supplier_publish.ADDRESS_LINE_2__C=obj.ADDRESS_LINE_2__C;            
            rec_supplier_publish.CITY__C= obj.CITY__C;
            rec_supplier_publish.POSTAL_CODE__C = obj.POSTAL_CODE__C;       
            rec_supplier_publish.Supplier_Category__c = obj.Supplier_Category__c;
            rec_supplier_publish.Zone__c = obj.Zone__c;
            rec_supplier_publish.Sub_Zone__c = obj.Sub_Zone__c;
            rec_supplier_publish.BSureS_Country__c=obj.BSureS_Country__c;
            rec_supplier_publish.State_Province__c = obj.State_Province__c;            
            //State_Province__c,Supplier_Zones__c,Supplier_SubZones__c,Supplier_Countries__c,Manager__c,Analyst__c '+
            rec_supplier_publish.Supplier_Zones__c = obj.Supplier_Zones__c; 
            rec_supplier_publish.Supplier_SubZones__c = obj.Supplier_SubZones__c; 
            rec_supplier_publish.Supplier_Countries__c = obj.Supplier_Countries__c; 
            rec_supplier_publish.Manager__c = obj.Manager__c; 
            rec_supplier_publish.Analyst__c = obj.Analyst__c;
            if(obj.Analyst__c!=null)
            	rec_supplier_publish.OwnerId = obj.Analyst__c;
            else
            	rec_supplier_publish.OwnerId = lstUserID.get(0).Id;
            rec_supplier_publish.Bakup_Analysts__c = obj.Bakup_Analysts__c;
            rec_supplier_publish.Bakup_Analysts_Names__c = obj.Bakup_Analysts_Names__c; 
            rec_supplier_publish.BulkUpload_BuyerID__c = obj.BulkUpload_BuyerID__c;        
            
			obj.Publish_Flag__c = true;//for inserted records making flag as true to update in stgae
			obj.Status__c = 'Published';
			obj.Publish_Date__c = Date.valueof(system.now());			
			//obj.Status_Resource_Value__c='/resource/1242640894000/Green';
			
			lst_Supplier.add(rec_supplier_publish);
			lst_supplierstage.add(obj);
		}
		Database.insert(lst_Supplier);//inserting records in publish
		BSureS_SupplierInfoIntermediate.isSupplierStageUpdate = true;
		Database.update(lst_supplierstage);//updating the stage records
		BSureS_SupplierInfoIntermediate.isSupplierStageUpdate = false;

     }
     
    /// <summary>
	/// finish method executes after completing all batches execution
	/// </summary>
	/// <param name="BC"></param>
     global void finish(Database.BatchableContext BC)
     {
     } 
}