/************************************************************************************************       
Controller Name 		: BSureC_PaymentHistoryItemstoApprove       
Date                    : 12/11/2012        
Author                  : Santhosh Palla       
Purpose         		: To Handle the Payment History Credit limit increase Approval Process
Change History 			: Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
						  12/11/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_PaymentHistoryItemstoApprove 
{
	public List<BSureC_Credit_Increase__c> lst_credit_increase {get;set;}//Holds the all credit increase data related to Login User
	public List<BSureC_Customer_Basic_Info__c> lst_customer_basic_info {get;set;}// Holds the all Customers info related to login User
	
	public Map<String,Id> map_customers_info{get;set;}//Holds the Customer's info related to login User
	
	public String str_loginprofile{get;set;}//Holds login User Profile
	public String str_sfdcBaseURL{get;set;}//Holds the page URL which is navigated When u click on Approve/Rejected
	
	
	/// <summary>  
 	/// Constructor  
 	/// </summary>
 	public BSureC_PaymentHistoryItemstoApprove()
 	{
 		lst_credit_increase = new List<BSureC_Credit_Increase__c>();
 		map_customers_info = new Map<String,Id>();
 		str_sfdcBaseURL = '/apex/BSureC_PaymentHistoryApprove?crId=';
 		String strReview = 'Review';
 		List<Profile> lst_profile = [SELECT Id,Name FROM Profile where Id!=null];
 		Map<Id,String> map_profiles = new Map<Id,String>();
 		
 		for(Profile rec_profile : lst_profile)
 		{
 			map_profiles.put(rec_profile.Id,rec_profile.Name);
 		}
 		
 		if(map_profiles != NULL && map_profiles.get(Userinfo.getProfileId()) != NULL) 
 		{
 			str_loginprofile = map_profiles.get(Userinfo.getProfileId());
 		}
 		 
 		if(str_loginprofile != null && str_loginprofile != '' && str_loginprofile == 'BSureC_Analyst')
 		{
 			lst_customer_basic_info = [SELECT Id 
 									   FROM BSureC_Customer_Basic_Info__c 
 									   WHERE Analyst__c =: String.valueOf(Userinfo.getUserId())];
 									   
 			for(BSureC_Customer_Basic_Info__c rec_customer_basic_info : lst_customer_basic_info)
 			{
 				if(map_customers_info.get(String.valueOf(rec_customer_basic_info.Id)) == null)
 				{
 					map_customers_info.put(String.valueOf(rec_customer_basic_info.Id),rec_customer_basic_info.Id);
 				}
 			}						   
 			
 			lst_credit_increase = [SELECT Id,New_Credit_Limit__c,New_Risk_Category__c,Credit_Limit__c,Risk_Category__c,Customer_Name__c
 								   FROM  BSureC_Credit_Increase__c 
 								   WHERE Status__c =: strReview
 								   AND New_Credit_Limit__c >=: Double.valueOf('1') 
 								   AND New_Credit_Limit__c <=: Double.valueOf('250000')
 								   AND Customer_Basic_Info_Id__c IN: map_customers_info.keySet()];
 			
 			if(lst_credit_increase == null || lst_credit_increase.size() == 0 )
 			{
 				ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'No Records Found');
                ApexPages.addMessage(errormsg);
 			}
	 		/*if(str_loginprofile != null && str_loginprofile != '' && str_loginprofile == 'BSureC_Analyst')
	 		{
	 			
	 			lst_credit_increase = [SELECT Id,New_Credit_Limit__c,New_Risk_Category__c,Credit_Limit__c,Risk_Category__c,Customer_Name__c
	 								   FROM  BSureC_Credit_Increase__c WHERE Status = 'Review' AND New_Credit_Limit__c >: Double.valueOf('250000') AND New_Credit_Limit__c <=: Double.valueOf('1000000')];
	 		}*/
 		}
 		else if(str_loginprofile != null && str_loginprofile != '' && str_loginprofile == 'BSureC_Zone_Manager')
 		{
 			
 			lst_credit_increase = [SELECT Id,New_Credit_Limit__c,New_Risk_Category__c,Credit_Limit__c,Risk_Category__c,Customer_Name__c
 								   FROM  BSureC_Credit_Increase__c 
 								   WHERE Status__c =: strReview  
 								   AND New_Credit_Limit__c >=: Double.valueOf('1000000')];
 		} 		
 		else
	 	{
 			ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'No Records Found');
            ApexPages.addMessage(errormsg);
	 	}
 	}
 	
}