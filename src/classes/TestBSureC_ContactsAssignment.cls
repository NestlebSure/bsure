/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_ContactsAssignment {

    static testMethod void myUnitTest() {
    	
    	Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
        	profiles.put(p.name, p.id);
        }
        
    	User standard = new User(
	        alias = 'standts',
	        email='standarduser@testorg.com',
	        emailencodingkey='UTF-8',
	        lastname='Testing', 
	        languagelocalekey='en_US',
	        localesidkey='en_US',
	        profileid = profiles.get('Standard User'),
	        timezonesidkey='America/Los_Angeles',
	        username='standardusertest@testorg.com');
        insert standard;
        
    	BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Americas34',IsActive__c=true);
        insert ZoneRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America34',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United State123',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;        
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state456456',Customer_Country__c=ConRef.id,IsActive__c=true);
        insert StateRef;       
       
        System.Assertequals('Test state456456',StateRef.name);
        Id currentUserId; 
        currentUserId = UserInfo.getUserId();
        BSureC_Customer_Basic_Info__c b=new BSureC_Customer_Basic_Info__c();
        b.Contact_Name__c = 'Steves';
        b.Customer_Name__c = standard.Name;
        b.Analyst__c = UserInfo.getUserId();
        b.Manager__c = UserInfo.getUserId();
        b.Backup_Analysts__c = UserInfo.getUserId();
        b.Zone__c = ZoneRef.id; 
        b.Sub_Zone__c = SubZoneRef.id;
        b.Country__c = ConRef.id;
        b.State__c = StateRef.id;       
        insert b;
        
        
        BSureC_Assigned_Contacts__c bAC = new BSureC_Assigned_Contacts__c();
        bac.Customer_Id__c=b.Id;
        bac.Contact_Name__c='test';
        bac.Comments__c='test comments';
        bac.Email_address__c='satish.c@vertexcs.com';
        bac.Phone_Number__c='988-2232-221';
        bac.Role__c='other';
        insert bAC;
        
        Apexpages.currentPage().getParameters().put('id',bAC.id);
        ApexPages.StandardController suplcontroller = new ApexPages.StandardController(bAC);
        BSureC_ContactsAssignment obj = new BSureC_ContactsAssignment(suplcontroller);
        
        obj.SaveBSureCustomerContact();
        obj.Cancel();
    }
}