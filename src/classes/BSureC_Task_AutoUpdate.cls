/************************************************************************************************       
Controller Name         : BSureC_Task_AutoUpdate       
Date                    : 06/24/2013        
Author                  : kishorekumar A       
Purpose                 : Scheduler       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          06/24/2013     Kishorekumar A           Initial Version
**************************************************************************************************/
global with sharing class BSureC_Task_AutoUpdate implements schedulable
{
	public list<Task> lstTask{get;set;}
	public list<BSureC_Credit_Increase__c> lstCreditAnalysis{get;set;}
	public list<BSureC_Customer_Basic_Info__c> lstCust{get;set;}
	public list<string> lstString{get;set;}
	public list<string> lstStringCAIds{get;set;}
	public string strCustStatus{get;set;}
	
	
	public BSureC_Task_AutoUpdate()
	{
		
	}
	public void execute(schedulablecontext sc)
	{
		strCustStatus = 'Overdue';
		lstString = new list<string>();
		lstStringCAIds = new list<string>();
		lstTask = new list<Task>([select id,OwnerId,whatId from Task where Status != 'Completed' and OwnerId != null]);
		if(!lstTask.isEmpty())
		{
			for(Task tObj : lstTask)
			{
				if(tObj.whatId != null)
				{ 
					lstString.add(tObj.whatId);
				}	
			}
		}
		if(!lstString.isEmpty())
		{ 
			lstCreditAnalysis = new list<BSureC_Credit_Increase__c>([select id,Status__c from BSureC_Credit_Increase__c
								where Id IN : lstString and Status__c = 'Completed']);
			
			lstCust = new list<BSureC_Customer_Basic_Info__c>([select id,Review_Status__c from BSureC_Customer_Basic_Info__c
								where Id IN : lstString and Review_Status__c !=: strCustStatus]);
		}
		if(!lstCreditAnalysis.isEmpty())
		{
			for(BSureC_Credit_Increase__c caObj : lstCreditAnalysis)
			{
				lstStringCAIds.add(caObj.Id);
			}
		}
		if(!lstCust.isEmpty())
		{
			for(BSureC_Customer_Basic_Info__c custObj : lstCust)
			{
				lstStringCAIds.add(custObj.Id);
			}
		}
		if(!lstStringCAIds.isEmpty())
		{
			if(Task.sObjectType.getDescribe().isDeletable())
			{
				delete[select id,OwnerId,whatId from Task where whatId IN : lstStringCAIds];
			}	
		}
	}  
}