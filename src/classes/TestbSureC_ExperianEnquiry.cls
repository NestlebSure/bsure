@isTest
private class TestbSureC_ExperianEnquiry {
    static testMethod void myUnitTest() {
    	System.test.startTest();
    	
    	BSureC_Customer_Basic_Info__c objBasicInfo = new BSureC_Customer_Basic_Info__c();
    	objBasicInfo.Customer_Name__c = 'Demo Customer';
    	objBasicInfo.BISFileNumber__c = '7986548975';
    	objBasicInfo.Credit_Account__c = '589647';
    	objBasicInfo.Planned_Review_Date__c = system.today();
    	insert objBasicInfo;
    	
    	DateTime d = Date.Today() ;
		string dateStr =  d.format('ddMMyy') ;
		string fName = dateStr+'_Experian_Rpt_'+objBasicInfo.Credit_Account__c;
		
    	BSureC_On_Going_Credit_Section__c objCreditInfo = new BSureC_On_Going_Credit_Section__c();
    	objCreditInfo.Customer_ID__c = objBasicInfo.id;
    	objCreditInfo.TitleDescription__c = fName;
    	insert objCreditInfo;
    	
    	Attachment objAttach = new Attachment();
    	objAttach.Name = dateStr+'_Experian_Rpt_'+objBasicInfo.Credit_Account__c;
    	objAttach.parentId = objCreditInfo.Id;
    	objAttach.Body = blob.valueof('This is to insert file for testing');
    	insert objAttach;
    	ApexPages.currentPage().getParameters().put('parentId',objCreditInfo.Id);
    	ApexPages.currentPage().getParameters().put('attachName',fName);
    	bSureC_ExperianEnquiry objExpEnq = new bSureC_ExperianEnquiry();
    	
    	string assign='1234';
        system.assertEquals('1234',assign);
        System.test.stopTest();
    }
}