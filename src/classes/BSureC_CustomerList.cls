/***********************************************************************************************
* Controller Name   : BSureC_CustomerList
* Date              : 02/14/2013
* Author            : ADITYA SRINIVAS V
* Purpose           : Class for Customer List based on Logged in Userlimit
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 02/14/2013            ADITYA SRINIVAS V   Initial Version
* 04/08/2014(mm/dd/yy)  Vidya Sagar H 		Added search functionality for Customer in BSureC_ViewCustomerDetails.page
**************************************************************************************************/

global with sharing class BSureC_CustomerList 
{
	// Standard controller for BSureC_ViewCustomerDetails.page
	// Added by Vidya Sagar
	 public BSureC_CustomerList(ApexPages.StandardController controller) 
    {
    
    }
    public ApexPages.StandardSetController objCustomerController{get; set;}
    
    public list<BSureC_Customer_Basic_Info__c> objCustomerList
    {
        get  
        {  
            if(objCustomerController != null)  
                return (List<BSureC_Customer_Basic_Info__c>)objCustomerController.getRecords();  
            else  
                return null ;  
        }
        set;
    }
    public list<UserRole> objCurrentUser {get; set; }
    public list<BSureC_AdditionalAttributes__c> objUserAttr {get; set; }
    
    public map<String, List<String>> mapConfigSettings = new map<String, List<String>>();
    
    //Added as on 07-06-13 - Search By Letter
    public List<string> searchChar{get;set;}  
    private string strQuery;    
    public string strCustomerName {get; set; }
    public string strCustomerSearch {get; set; }
    public string strAdvCustName {get; set; }
    public string strLoggedInUser {get; set; }
    public string strBackupAnalyst {get; set; }
    public Decimal dblSpendFrom  {get; set; }
    public Decimal dblSpendTo {get; set; }
    public Date dtNRFromDate  {get; set; }
    public Date dtNRToDate  {get; set; }
    public Date dtLRFromDate {get; set; }
    public Date dtLRToDate  {get; set; }
    //Role Based Search
    public string strUserRole {get; set; }
    public string strRoleName {get; set; }
    public string strRoleBase {get; set; }
    public string strUserQuery {get; set; }
    public string strQueryParam {get; set; }
    public string strDelCustID {get; set; }
    public string strAnalystChoice {get; set; }
    
    public Id strUserId {get; set; }
    public boolean showNewBtn {get; set; }
    public boolean showCustList {get; set; }
    public boolean showAnalystChoice {get; set; }
    public boolean showToVisitor {get; set; }
    
    public list<SelectOption> lstAnalystChoice {get; set; }
    
    //Sort Variables
    public string sortDirection = 'ASC';            //Initial Sort Direction
    public string sortExp = 'Customer_Name__c';     //Initial Sort Expression
    
    //Pagination Variables
    //public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer prevPageNumber {get;set;}//used for pagination
    public Integer nextPageNumber {get;set;}//used for pagination
    public Integer intListSize {get;set;}//used for pagination
    public integer totalListSize{get;set;}//total size of list for pagination 
    
    //Advanced Search Variables
    public string strAdvCustomerName {get; set; }
    public string strCustomerCatg {get; set; }
    public string strCustomerZone {get; set; }      //To hold Customer Zone
    public string strCustomerSubZone{get;set;}      //To hold Customer Sub Zone
    public string strCustomerCountry{get;set;}      //To hold Customer Country
    public string strCustomerState{get;set;}        //To hold Customer State
    public string strTodayDate{get;set;}            //To hold Today's Date
    public string strNextRevFromDate {get; set; }   //To hold Next Review Date Range
    public string strNextRevToDate {get; set; }     //To hold Next Review Date Range
    public string strLastRevFromDate {get; set; }   //To hold Next Review Date Range
    public string strLastRevToDate {get; set; }     //To hold Next Review Date Range
    public string dSpendFrom {get; set; }
    public string dSpendTo {get; set; }
    public string strRiskLevel {get; set; }
    public string strReviewStatus {get; set; }
    public string strCreditAcc {get; set; }
    
    public list<SelectOption> lstCCatg {get;set;}
    public list<SelectOption> lstCZones{get;set;}       //To hold Customer Zone list
    public list<SelectOption> lstCSubZones{get;set;}    //To hold Customer Sub Zone list
    public list<SelectOption> lstCCountries{get;set;}   //To hold Customer Country list
    public list<SelectOption> lstCStates{get;set;}      //To hold Customer State list
    
    public list<string> lstRiskLevels {get; set; }
    public list<string> lstStatus {get; set; }
    public set<string> setRiskLevels {get; set;}
    public set<string> setStatus {get; set;}
    
    //Properties Added by Veera on june 13th 2013
    public string strZoneMgrRole {get;set;}
    public string strSubZoneRole {get;set;}
    public string strCountryRole {get;set;}
    public string strCollectorRole   {get;set;}
    public string strUnAuthCollectorRole {get;set;}
    public string strUnEarnCollectorRole {get;set;}
    public string strCustServiceRole {get;set;}
    public string strCustSalesRole {get;set;}
    public set<string> setCusIds {get;set;}
    
    // Added by Vidya Sagar
    public boolean displayPopup {get; set;}  // used to show popup 
    public string strSearchCust {get;set;}
    public string strPopMsg {get;set;}
    
    public BSureC_CustomerList()
    {
        strRoleName = '';
        showNewBtn = false;
        showCustList = true;
        showAnalystChoice = false;
        showToVisitor = true;
        strAnalystChoice = 'AllCustomer';
        strTodayDate = Date.today().format();
        
        lstCCatg = new list<SelectOption>();
        lstCCatg = getCustomerCagetory();
        
        lstAnalystChoice = new list<SelectOption>();
        lstAnalystChoice.add(new SelectOption('AllCustomer', 'All Customers'));
        lstAnalystChoice.add(new SelectOption('MyCustomer', 'My Customers'));        
        
        strUserRole = Userinfo.getUserRoleId();
        strUserId = Userinfo.getUserId();
        objCurrentUser = [SELECT Id, Name 
                          FROM UserRole
                          WHERE Id =: strUserRole];
        if(objCurrentUser != null && objCurrentUser.size() > 0)
        {
            //system.debug('User Role Name :' + objCurrentUser.get(0).Name);
            strRoleName = objCurrentUser.get(0).Name;
        }
        objUserAttr = [SELECT User__r.Id, Name, BSureC_Zone__c, BSureC_Sub_Zone__c, BSureC_Country__c, BSureC_Region__c
                       FROM BSureC_AdditionalAttributes__c 
                       WHERE User__r.Id =: strUserId];
                       
        mapConfigSettings = BSureC_CommonUtil.configSettingsMap;
        
        if(strRoleName != '' && strRoleName.equalsIgnoreCase('CEO'))
        {
            showNewBtn = true;
        }
        else if(strRoleName != '' && strRoleName.equalsIgnoreCase('analyst'))
        {
            showAnalystChoice = true;
        }
        else if(strRoleName != '' 
                && (strRoleName.equalsIgnoreCase('Customer Service')
                    || strRoleName.equalsIgnoreCase('Sales')))
        {
            //system.debug('strRoleName+++' + strRoleName);
            showToVisitor = false;
        }

        SetQueryParams();
        //system.debug('User Role Name : ' + objUserAttr.get[0].User__r.Role);
        
        getCZones();
        getDefaultCustomerList();        
        //getCustomCustomerList();
        
        //Added as on 07-06-13
        searchChar=new string[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'All' };                    
    }
    
    public void SetQueryParams()
    {
        strZoneMgrRole = '';
        strSubZoneRole = '';
        strCountryRole = '';
        string strRegionRole = '';
        string strAnalystRole = '';
        string strBuyerRole = '';
        strUserQuery = '';
        strCollectorRole = '';
        strUnAuthCollectorRole = '';
        strUnEarnCollectorRole = '';
        strCustServiceRole = '';
        strCustSalesRole = '';
        strQueryParam = '';
        
        if(mapConfigSettings.get('bsurec_zonemanager') != null
            && mapConfigSettings.get('bsurec_zonemanager').get(0) != null)
        {
            strZoneMgrRole = mapConfigSettings.get('bsurec_zonemanager').get(0);
        }
        if(mapConfigSettings.get('bsurec_subzonemanager') != null
            && mapConfigSettings.get('bsurec_subzonemanager').get(0) != null)
        {
            strSubZoneRole = mapConfigSettings.get('bsurec_subzonemanager').get(0);
        }
        if(mapConfigSettings.get('bsurec_countrymanager') != null
            && mapConfigSettings.get('bsurec_countrymanager').get(0) != null)
        {
            strCountryRole = mapConfigSettings.get('bsurec_countrymanager').get(0);
        }
        if(mapConfigSettings.get('bsurec_regionmanagerrole') != null
            && mapConfigSettings.get('bsurec_regionmanagerrole').get(0) != null)
        {
            strRegionRole = mapConfigSettings.get('bsurec_regionmanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsurec_analystrole') != null
            && mapConfigSettings.get('bsurec_analystrole').get(0) != null)
        {
            strAnalystRole = mapConfigSettings.get('bsurec_analystrole').get(0);
        }
        if(mapConfigSettings.get('bsurec_ongoingcollector') != null
            && mapConfigSettings.get('bsurec_ongoingcollector').get(0) != null)
        {
            strCollectorRole = mapConfigSettings.get('bsurec_ongoingcollector').get(0);
        }
        if(mapConfigSettings.get('bsurec_un-authorizedcollector') != null
            && mapConfigSettings.get('bsurec_un-authorizedcollector').get(0) != null)
        {
            strUnAuthCollectorRole = mapConfigSettings.get('bsurec_un-authorizedcollector').get(0);
        }
        if(mapConfigSettings.get('bsurec_un-earnedcollector') != null
            && mapConfigSettings.get('bsurec_un-earnedcollector').get(0) != null)
        {
            strUnEarnCollectorRole = mapConfigSettings.get('bsurec_un-earnedcollector').get(0);
        }
        if(mapConfigSettings.get('bsurec_customerservice') != null
            && mapConfigSettings.get('bsurec_customerservice').get(0) != null)
        {
            strCustServiceRole = mapConfigSettings.get('bsurec_customerservice').get(0);
        }
        if(mapConfigSettings.get('bsurec_sales') != null
            && mapConfigSettings.get('bsurec_sales').get(0) != null)
        {
            strCustSalesRole = mapConfigSettings.get('bsurec_sales').get(0);
        }
        
        if(strRoleName != '' && strZoneMgrRole != '' && strRoleName.equalsIgnoreCase(strZoneMgrRole))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureC_Zone__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                    if(strAnalystChoice != null && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
                        strUserQuery = ' AND Zone__c =: strRoleBase';
                    strQueryParam = 'Zone__c##' + strRoleBase;
                }
                else
                {
                    showCustList = false;
                }
            }
            else
            {
                showCustList = false;
            }
        }
        else if(strRoleName != '' && strRoleName.equalsIgnoreCase(strSubZoneRole))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureC_Sub_Zone__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                    if(strAnalystChoice != null && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
                        strUserQuery = ' AND Sub_Zone__c =: strRoleBase';
                    strQueryParam = 'Sub_Zone__c##' + strRoleBase;
                }
                else
                {
                    showCustList = false;
                }
            }
            else
            {
                showCustList = false;
            }
        }
        else if((strRoleName != '' && strRoleName.equalsIgnoreCase(strCountryRole))
                || (strRoleName != '' && strRoleName.equalsIgnoreCase(strRegionRole)))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureC_Country__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                    if(strAnalystChoice != null && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
                        strUserQuery = ' AND Country__c =: strRoleBase';
                    strQueryParam = 'Country__c##' + strRoleBase;
                }
                else
                {
                    showCustList = false;
                }
            }
            else
            {
                showCustList = false;
            }
        }
    }
    public void getDefaultCustomerList()
    {
        string strSearchQuery = AdvSearchQueryString();
        objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strSearchQuery));  
        objCustomerController.setPageSize(25);  
        
        //totalListSize = objCustomerController.getResultSize();
        totalListSize = [SELECT COUNT() FROM BSureC_Customer_Basic_Info__c];
        pageSize = 25;// default page size will be 5
    }
    public void getCustomCustomerList()
    {
        string strSearchQuery = AdvSearchQueryString();
        objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strSearchQuery));  
        objCustomerController.setPageSize(25);  
        
        totalListSize = objCustomerController.getResultSize();
        pageSize = 25;// default page size will be 5
    }
    
    public Pagereference SearchByLetter()
    {       
        //string strCustomerSearchByLetter;     
        if(apexpages.currentpage().getparameters().get('alpha') == 'All') {
            strCustomerName = 'ALL';
        } else {
            strCustomerName = apexpages.currentpage().getparameters().get('alpha');
        }
               
        string strSearchQuery = AdvSearchQueryString();     
        objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strSearchQuery));  
        objCustomerController.setPageSize(25); 
        
        if(apexpages.currentpage().getparameters().get('alpha') == 'All') {
        totalListSize=[SELECT Count() from BSureC_Customer_Basic_Info__c];
        }else{
            totalListSize = objCustomerController.getResultSize();
        }
        pageSize = 25;// default page size will be 5    
        return null;        
    }
    
    /*
    public string SearchQueryString()
    {
        string strShowCustomers = BSureC_CommonUtil.getConfigurationValues('ShowAllCustomersForManagers').get(0);
        //system.debug('objCustomerList++++++' + objCustomerList.size());
        string strQuery = '';
        strQuery += 'SELECT Id, Customer_Name__c, Customer_Category_Name__c, City__c, Country__c, ';
        strQuery += 'State_Province__c, Credit_Limit__c, Country_Name__c, Review_Complete_Date__c, Credit_Account__c, ';
        strQuery += 'Analyst_Name__c, Sub_Zone__c, Zone__c, Risk_Category__c, Risk_Category_Type__c, '; 
        strQuery += 'Customer_Category__c, Next_Review_Date__c, Review_Status__c, State__c '; 
        strQuery += 'FROM BSureC_Customer_Basic_Info__c ';
        strQuery += 'WHERE Id != null ';
        
        if(strCustomerName != null && strCustomerName != '' 
            && !strCustomerName.equalsIgnoreCase('Start typing your Customer name'))
        {
            strCustomerSearch = '%' + strCustomerName + '%';
            strQuery += 'AND Customer_Name__c like : strCustomerSearch ';
        }
        
        if(showAnalystChoice && strAnalystChoice != null 
            && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
        {
            strLoggedInUser = UserInfo.getUserId();
            strBackupAnalyst = '%' + strLoggedInUser + '%';
            strQuery += 'AND ( Analyst__c =: strLoggedInUser ';
            strQuery += ' OR Backup_Analysts__c like: strBackupAnalyst ) ';
        }       
        if(strShowCustomers != null && !strShowCustomers.equalsIgnoreCase('TRUE') && strUserQuery != '')
        {
            strQuery += strUserQuery;
        }       
        if(sortExpression != null && sortExpression != ''
            && sortDirection != null && sortDirection != '')
        {
            strQuery += ' order by ' + sortExpression + ' ' + sortDirection;
        }
        if(!showCustList)
        {
            strQuery += ' LIMIT 0';
        } 
        else
        {
            strQuery += ' LIMIT 9000';
        }
        //system.debug('strQuery+++++111+++' + strQuery);
        return strQuery;
    }
    */
    
    public Pagereference CreateCustomer()
    {
        Pagereference pageRef = new Pagereference('/apex/BSureC_CustomerBasicInformation?retURL=/apex/BSureC_CustomerList');
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public Pagereference SortData()
    {
        getCustomCustomerList();
        return null;
    }
    
    public Pagereference getSearchResults()
    {
        getCustomCustomerList();
        
        if(!objCustomerList.isEmpty() && objCustomerList.size() == 1 && objCustomerList[0].Customer_Name__c == strCustomerName){
            Pagereference pref = new Pagereference('/'+objCustomerList[0].Id);
            return pref;     
        }
        
        return null;
    }
    /* @description : This method is used to Search Customer 
     * @Page : This method is used in BSureC_ViewCustomerDetails.page
     * @return : PageReference if customer exists, if not returns null
     */// Added by Vidya Sagar
    public Pagereference getSearchCustomer()
    {
    	
    	if(strSearchCust !='' && strSearchCust !=null){
        list<BSureC_Customer_Basic_Info__c> obj = [select id,Name from BSureC_Customer_Basic_Info__c where Customer_Name__c = :strSearchCust limit 1];
        if(obj != null && obj.size()>0){
            Pagereference pref = new Pagereference('/apex/BSureC_ViewCustomerDetails?id='+obj[0].Id);
            pref.setRedirect(true);
            return pref;     
        }        
         if(strSearchCust !='Start typing your Customer Name')
         strPopMsg = strSearchCust+ ' Customer does not exists';
         else
         strPopMsg = 'Please enter Customer name';
        /*ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Customer does not exists');
         ApexPages.addMessage(myMsg); */         
    	}
    	else
    	   strPopMsg = 'Please enter Customer name';
    	   displayPopup = true;
        return null;
    }
    
       
    /* @description : This method is used to close popup  
     * @Page : This method is used in BSureC_ViewCustomerDetails.page
     */// Added by Vidya Sagar
    public void closePopup() {        
        displayPopup = false; 
    }
    
    public Pagereference DeleteCustomerInfo()
    {
        //system.debug('strDelCustID++++' + strDelCustID);
        if(strDelCustID != null && strDelCustID != '')
        {
            list<BSureC_Customer_Basic_Info__c> objCust = [SELECT Id FROM BSureC_Customer_Basic_Info__c
                                                   WHERE Id =: strDelCustID];
            if(objCust != null && objCust.size() > 0)
            {
                delete objCust;
            }
            getCustomCustomerList();
        }
        return null;
    }
    
    //Adv Search Methods BEGIN
    
    /// <summary>
    /// Method to get Advanced Search Result for Customer search 
    /// </summary>
    public Pagereference getAdvSearchResult()
    {
        strCustomerName = '';
        string strAdvQuery = AdvSearchQueryString();
        objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strAdvQuery));  
        objCustomerController.setPageSize(25);  
        
        totalListSize = objCustomerController.getResultSize();  // this size for pagination
        pageSize = 25;  
        
        if(!objCustomerList.isEmpty() && objCustomerList.size() == 1 
            && objCustomerList[0].Customer_Name__c == strAdvCustomerName)
        {
            Pagereference pref = new Pagereference('/'+objCustomerList[0].Id);
            return pref;     
        }
        return null;
    }
    
    public string AdvSearchQueryString()
    {
        lstRiskLevels = new list<string>(); 
        lstStatus = new list<string>(); 
        setRiskLevels = new set<string>();
        setStatus = new set<string>();
        
        if(strRiskLevel != null && strRiskLevel != '')
        {
            //strRiskLevel = strRiskLevel.substring(0, strRiskLevel.length() -1);
            lstRiskLevels = strRiskLevel.split(',');
            setRiskLevels.addAll(lstRiskLevels);                    
        }
        if(strReviewStatus != null && strReviewStatus != '')
        {
            //strReviewStatus = strReviewStatus.substring(0, strReviewStatus.length() -1);
            lstStatus = strReviewStatus.split(',');
            setStatus.addAll(lstStatus);
        }
        
        string strShowCustomers = BSureC_CommonUtil.getConfigurationValues('ShowAllCustomersForManagers').get(0);
        //string strsearchletter = BSureC_CommonUtil.getConfigurationValues('ShowAllCustomersForManagers').get(0);
        
        string strQuery = 'SELECT Id, Customer_Name__c, Customer_Category_Name__c, City__c, Country__c, ';
        strQuery += 'State_Province__c, Credit_Limit__c, Country_Name__c, Review_Complete_Date__c, Credit_Account__c, ';
        strQuery += 'Analyst_Name__c, Sub_Zone__c, Zone__c, Risk_Category__c, Risk_Category_Type__c, '; 
        strQuery += 'Customer_Category__c, Next_Review_Date__c, Review_Status__c, State__c '; 
        strQuery += 'FROM BSureC_Customer_Basic_Info__c ';
        strQuery += 'WHERE Id != null ';
        
        if(strCustomerName != null && strCustomerName != '' 
            && !strCustomerName.equalsIgnoreCase('Start typing your Customer name')
            && !strCustomerName.equalsIgnoreCase('ALL'))
        {
            strCustomerSearch = strCustomerName + '%';           
            strQuery += 'AND Customer_Name__c like : strCustomerSearch ';            
        }
        else if(strCustomerName != null && strCustomerName != '' 
            && strCustomerName.equalsIgnoreCase('ALL'))
        {
            strCustomerName = '';
            strAdvCustomerName = '';
            strCustomerSubZone = '';
            strCustomerCountry = '';
            strCustomerState = '';
            strCustomerCatg = '';
            strNextRevFromDate = '';
            strNextRevToDate = '';
            strLastRevFromDate = '';
            strLastRevToDate = '';
            dSpendFrom = '';
            dSpendTo = '';
            strCreditAcc = '';    
        }
        else
        {
            if(strAdvCustomerName != null && strAdvCustomerName != '')
            {
                strAdvCustName = '%' + strAdvCustomerName + '%';  
                strQuery += 'AND Customer_Name__c  like : strAdvCustName ';
            }
            if(strCustomerCatg != null && strCustomerCatg != ''
                && !strCustomerCatg.equalsIgnoreCase('ALL'))
            {
                strQuery += 'AND Customer_Category__c =: strCustomerCatg ';
            }
            if(strCreditAcc != null && strCreditAcc != '')
            {
                strQuery += 'AND Credit_Account__c =: strCreditAcc ';
            }
            if(strCustomerZone != null && strCustomerZone != '' 
                && !strCustomerZone.equalsIgnoreCase('ALL'))
            {
                strQuery += 'AND Zone__c =: strCustomerZone ';
                if(strCustomerSubZone != null && strCustomerSubZone != '' 
                    && !strCustomerSubZone.equalsIgnoreCase('ALL'))
                {
                    strQuery += 'AND Sub_Zone__c =: strCustomerSubZone ';
                    if(strCustomerCountry != null && strCustomerCountry != '' 
                        && !strCustomerCountry.equalsIgnoreCase('ALL'))
                    {
                        strQuery += 'AND Country__c =: strCustomerCountry ';
                        if(strCustomerState != null && strCustomerState != '' 
                            && !strCustomerState.equalsIgnoreCase('ALL'))
                        {
                            strQuery += 'AND State__c =: strCustomerState ';
                        }
                    }
                }
            }
            if(dSpendFrom != null && dSpendFrom != '')
            {
                if(dSpendFrom.contains(','))
                {
                    dSpendFrom = dSpendFrom.replace(',', '');
                }
                dblSpendFrom = Decimal.valueOf(dSpendFrom);
                strQuery += 'AND Credit_Limit__c >=: dblSpendFrom ';
            }
            if(dSpendTo != null && dSpendTo != '')
            {
                if(dSpendTo.contains(','))
                {
                    dSpendTo = dSpendTo.replace(',', '');
                }
                dblSpendTo = Decimal.valueOf(dSpendTo);
                strQuery += 'AND Credit_Limit__c <=: dblSpendTo ';
            }
            if(strNextRevFromDate != null && strNextRevFromDate != '')
            {
                dtNRFromDate = Date.parse(strNextRevFromDate);
                strQuery += 'AND Next_Review_Date__c >=: dtNRFromDate ';
            }
            if(strNextRevToDate != null && strNextRevToDate != '')
            {
                dtNRToDate = Date.parse(strNextRevToDate);
                strQuery += 'AND Next_Review_Date__c <=: dtNRToDate ';
            }
            if(strLastRevFromDate != null && strLastRevFromDate != '')
            {
                dtLRFromDate = Date.parse(strLastRevFromDate);
                strQuery += 'AND Review_Complete_Date__c >=: dtLRFromDate ';
            }
            if(strLastRevToDate != null && strLastRevToDate != '')
            {
                dtLRToDate = Date.parse(strLastRevToDate);
                strQuery += 'AND Review_Complete_Date__c <=: dtLRToDate ';
            }
            if(strRiskLevel != null && strRiskLevel != '')
            {
                strQuery += 'AND Risk_Category__c IN: setRiskLevels ';
            }
            if(strReviewStatus != null && strReviewStatus != '')
            {
                strQuery += 'AND Review_Status__c IN: setStatus ';
            }
        }
        if(showAnalystChoice && strAnalystChoice != null 
            && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
        {
            strLoggedInUser = UserInfo.getUserId();
            strBackupAnalyst = '%' + strLoggedInUser + '%';
            strQuery += 'AND Analyst__c =: strLoggedInUser ';
            //strQuery += 'AND ( Analyst__c =: strLoggedInUser ';
            //strQuery += ' OR Backup_Analysts__c like: strBackupAnalyst ) ';
        } 
        if(strAnalystChoice != null && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mycustomer'))
        {
            if(strRoleName != null && strRoleName.equalsIgnoreCase(strZoneMgrRole)){
                strQuery += 'AND Zone__c =:strRoleBase';
            }else if(strRoleName != null && strRoleName.equalsIgnoreCase(strSubZoneRole)){
                strQuery += 'AND Sub_Zone__c =:strRoleBase';
            }else if(strRoleName != null && strRoleName.equalsIgnoreCase(strCountryRole)){
                strQuery += 'AND Country__c =:strRoleBase';
            }else if(strRoleName != null && (strRoleName.equalsIgnoreCase(strCollectorRole)
                                                || strRoleName.equalsIgnoreCase(strUnAuthCollectorRole)
                                                || strRoleName.equalsIgnoreCase(strUnEarnCollectorRole)
                                                || strRoleName.equalsIgnoreCase(strCustServiceRole)
                                                || strRoleName.equalsIgnoreCase(strCustSalesRole))){
                getCollectorSuppliers();
                strQuery += 'AND Id In:setCusIds';
            }        
        }       
        if(strShowCustomers != null && !strShowCustomers.equalsIgnoreCase('TRUE') && strUserQuery != '')
        {
            strQuery += strUserQuery;
        }       
        if(sortExpression != null && sortExpression != ''
            && sortDirection != null && sortDirection != '')
        {
            strQuery += ' order by ' + sortExpression + ' ' + sortDirection;
        }
        if(!showCustList)
        {
            strQuery += ' LIMIT 0';
        } 
        else
        {
            strQuery += ' LIMIT 10000';
        }
        
        system.debug('strQuery++++' + strQuery);
        return strQuery;
    }     
    
    
        /// <summary>
    /// to get Suppliers which are already assinged Buyers  
    /// </summary>
    
    public void getCollectorSuppliers(){
        setCusIds = new set<string>();
        list<BSureC_Assigned_Buyers__c> lstBuyers = new list<BSureC_Assigned_Buyers__c>();
        lstBuyers = [SELECT Id,User_Id__c,Customer_Id__c 
                     FROM BSureC_Assigned_Buyers__c 
                     WHERE User_Id__c =:UserInfo.getUserId() and User_Id__c != null ];
        for(BSureC_Assigned_Buyers__c assB:lstBuyers){
            setCusIds.add(assB.Customer_Id__c);
        }
    }
    
    /// <summary>
    /// Method to get Zone list for Customer search 
    /// </summary>
    public void getCZones() 
    {
        BSureC_CommonUtil.blnflag = true;
        lstCZones = new list<SelectOption>();
        lstCZones = BSureC_CommonUtil.getZones();
        lstCSubZones = new list<SelectOption>();
        lstCCountries = new list<SelectOption>();
        lstCStates = new List<SelectOption>();
    }
    
    /// <summary>
    /// Method to get Subzone list for Customer search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getSubZoneList()
    {
        BSureC_CommonUtil.blnflag = true;
        
        lstCSubZones = new list<SelectOption>();
        //system.debug('strCustomerZone+++++ ' + strCustomerZone);
        if(strCustomerZone != null && strCustomerZone != ''
            && !strCustomerZone.equalsIgnoreCase('all'))
        {
            lstCSubZones = BSureC_CommonUtil.getSubZones(strCustomerZone);
        }
        else
        {
            strCustomerSubZone = 'ALL';         
        }
        return lstCSubZones;     
    }
    
    /// <summary>
    /// Method to get Country list for Customer search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getCountriesList()
    {
        BSureC_CommonUtil.blnflag = true;
        lstCCountries = new list<SelectOption>();
        //system.debug('strCustomerSubZone+++++ ' + strCustomerSubZone);
        if(strCustomerSubZone != null && strCustomerSubZone != ''
            && !strCustomerSubZone.equalsIgnoreCase('all'))
        {
            lstCCountries = BSureC_CommonUtil.getCountries(strCustomerSubZone);
        }
        else
        {
            strCustomerCountry = 'ALL';
        }
        return lstCCountries;     
    }
    
    /// <summary> 
    /// Method to get Country list for Customer Contact Info
    /// </summary>  
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getStateList()
    {
        BSureC_CommonUtil.blnflag = true;
        //system.debug('strCustomerCountry++++++' + strCustomerCountry);
        
        lstCStates = new List<SelectOption>();
        if(strCustomerCountry != null && strCustomerCountry != ''
            && !strCustomerCountry.equalsIgnoreCase('all'))
        {
            lstCStates = BSureC_CommonUtil.getStates(strCustomerCountry);
        }
        return lstCStates;     
    }
    
    public list<SelectOption> getCustomerCagetory()
    {
        list<SelectOption> lstCustCategory = new list<SelectOption>();
        lstCustCategory.add(new selectOption('ALL','ALL'));
        list<BSureC_Category__c> lstSCatTypes = [SELECT id, Name, Category_ID__c
                                                      FROM BSureC_Category__c
                                                      where Id != null
                                                      order BY Name ];
        for(BSureC_Category__c objs : lstSCatTypes)
        {
            lstCustCategory.add(new SelectOption(objs.id,objs.Name));
        }
        return lstCustCategory;
    }
    
    public Pagereference ClearFields()
    {
        strAdvCustomerName = '';
        strCustomerSubZone = '';
        strCustomerCountry = '';
        strCustomerState = '';
        strCreditAcc = '';
        return null;
    }
    
    //Adv Search Methods END
    
    //SORT Methods BEGIN
    public string sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            if(value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
     
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }    
    //SORT Methods END
    
    //Pagination Methods BEGIN
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (totalListSize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totalListSize);
    }
    
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totalListSize!=0)
        {
            //system.debug(totalPageNumber+'-totalListSize--'+totalListSize);
            totalPageNumber = totalListSize / pageSize;
            Integer mod = totalListSize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
    public Boolean hasNext  
    {
        get  
        {
            return objCustomerController.getHasNext();  
        }  
        set;  
    }  
   
    public Boolean hasPrevious  
    {
        get  
        {  
            return objCustomerController.getHasPrevious();  
        }  
        set;  
    }  
   
    //Page number of the current displaying records  
    public Integer pageNumber  
    {  
        get  
        {  
            return objCustomerController.getPageNumber();  
        }  
        set;  
    }  
  
    //Returns the previous page of records  
    public void previous()  
    {  
        objCustomerController.previous();  
    }  
   
    //Returns the next page of records  
    public void next()  
    {  
        objCustomerController.next();  
    }  
    
    public void first()
    {
        objCustomerController.first();
    }
    
    public void last()
    {
        objCustomerController.last();
    }
    //Pagination Methods END
    
}