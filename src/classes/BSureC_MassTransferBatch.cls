global with sharing class BSureC_MassTransferBatch implements Database.Batchable<SObject>{
	
	global String strQuery = null;
	global string strParmUserid = '';
    global string str_AssignUser = '';
    public list<BSureC_Customer_Basic_Info_Stage__c> LstUpdateCustomers = new list<BSureC_Customer_Basic_Info_Stage__c>();
	
	/*public BSureC_MassTransferBatch()
    {
    	string strParmUserid = '';
    	string str_AssignUser = '';
    	String strUserID = '%'+strParmUserid+'%';
    	strQuery = 'SELECT id,Customer_Name__c,Backup_Analysts__c from BSureC_Customer_Basic_Info__c where Backup_Analysts__c Like: strUserID' ;
    }*/
    public BSureC_MassTransferBatch(String strParmUserid, String str_AssignUser)
    {
    	this.strParmUserid = strParmUserid;
    	this.str_AssignUser= str_AssignUser;
    	String strUserID = '%'+strParmUserid+'%';
    	strQuery = 'SELECT id,Customer_Name__c,Backup_Analysts__c from BSureC_Customer_Basic_Info__c where Backup_Analysts__c Like: strUserID' ;
    }
	global Database.QueryLocator start (Database.BatchableContext ctx)
    {         
       return Database.getQueryLocator(strQuery);
    }
	
	global void execute(Database.BatchableContext BC, list<BSureC_Customer_Basic_Info_Stage__c> scope)
    {
    	LstUpdateCustomers = new list<BSureC_Customer_Basic_Info_Stage__c>();
        for(BSureC_Customer_Basic_Info_Stage__c  obj : scope)
        {
        	obj.Backup_Analysts__c=obj.Backup_Analysts__c.replace(strParmUserid,str_AssignUser);
            LstUpdateCustomers.add(obj);
        }
        Database.SaveResult[] CreditCustomerUpdate = Database.Update(LstUpdateCustomers);
    }
    
    global void finish(Database.BatchableContext BC)
    { 
    	
    } 

}