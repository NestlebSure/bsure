/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_CommonUtil {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher Supplier');
        insert objSupplier;
        BSureS_Document_Type__c objDocType = new BSureS_Document_Type__c(Document_Type_Name__c = 'Presentations4234');
        insert objDocType;
        
        System.assertEquals('KingFisher Supplier',objSupplier.Contact_name__c);
        
        BSureS_Credit_Status_Section__c objCss = new BSureS_Credit_Status_Section__c();
        objCss.Supplier_ID__c = objSupplier.Id;
        insert objCss;
        Attachment att = new Attachment ();
        att.name = 'Test ';
        att.Body = blob.valueof('my file');
        att.ParentId = objCss.Id;
        insert att;
        
        system.Test.startTest();
        BSureS_CommonUtil.getDocTypesBSureS();
        BSureS_CommonUtil.getBSureSupplierInfo(objSupplier.Id);
        BSureS_CommonUtil.getCSSData(objCss.Id);        
        BSureS_CommonUtil.getAttachmentData(objCss.Id);
        BSureS_CommonUtil.UploadFile(objCss.Id, blob.valueof('test file'), 'Att Name');
        BSureS_CommonUtil.changeDateFormat('11/19/2012');
        system.Test.stopTest();
    }
}