public with sharing class bSureC_ExperianCred {
    public boolean visible{get;set;}
    public bSureC_ExpCred__c objCred{get;set;}
    public list<sobject> lstSObj;
    public boolean ShowAgency {get;set;}
    public string SelectedAgency{get;set;}
    
    public bSureC_ExperianCred(){
        ShowAgency = false;
        SelectedAgency='None';  
        visible = true;
        objCred = new bSureC_ExpCred__c();
    }
     public void processRequests(){
        objCred = new bSureC_ExpCred__c();
        visible = true;
        ShowAgency = false;
        if(SelectedAgency =='Experian' || SelectedAgency=='Creditsafe' ){
            list<bSureC_ExpCred__c> lstCred = [SELECT Id,Name,Pwd__c,UN__c 
                                                From bSureC_ExpCred__c 
                                                where Credit_Agency__c=:SelectedAgency];
            if(lstCred != null && lstCred.size()>0){
                objCred = lstCred[0];
                visible = false;
            }
            ShowAgency = true;
        }
    }
        
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','None'));
        options.add(new SelectOption('Experian','Experian'));
        options.add(new SelectOption('Creditsafe','Creditsafe'));
        return options;
    }
     
    public PageReference CustomSave(){
        if(objCred!=null){
            insert objCred;
            string strExpCred = BSureC_CommonUtil.getConfigurationValues('ExperianCredentialsByPass').get(0);
            if(strExpCred != null && strExpCred == 'true'){
                lstSObj = new list<sobject>();
                string strQuery = 'Select Id,Name,Vertex_BSureS__Pwd__c,Vertex_BSureS__UN__c From Vertex_BSureS__bSureS_ExpCred__c where Credit_Agency__c=:SelectedAgency';
                try{
                    lstSObj = Database.query(strQuery);
                    if(lstSObj != null && lstSObj.size() > 0)
                    {
                        for(Sobject sobj: lstSObj)
                        {
                            sobj.put('Vertex_BSureS__Pwd__c', objCred.Pwd__c);
                            sobj.put('Vertex_BSureS__UN__c',objCred.UN__c); 
                        }
                        Database.SaveResult[] srList = Database.update(lstSObj, false);
                    }else{
                        lstSObj = new list<sobject>();
                        sobject obj = schema.getGlobalDescribe().get('Vertex_BSureS__bSureS_ExpCred__c').newSObject();
                        obj.put('Vertex_BSureS__Pwd__c', objCred.Pwd__c);
                        obj.put('Vertex_BSureS__UN__c',objCred.UN__c); 
                        lstSObj.add(obj);
                        Database.SaveResult[] srList = Database.insert(lstSObj, false);
                    }
                }catch(Exception e){
                    system.debug('Exception Occured while saving Experian Credentials.');
                }
            }
        }
        PageReference pgRef = new PageReference('/apex/bSureC_ExperianCred');
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    public PageReference CustomUpdate(){
        if(objCred!=null){
            update objCred;
            string strExpCred = BSureC_CommonUtil.getConfigurationValues('ExperianCredentialsByPass').get(0);
            if(strExpCred != null && strExpCred == 'true'){
                lstSObj = new list<sobject>();
                string strQuery = 'Select Id,Name,Vertex_BSureS__Pwd__c,Vertex_BSureS__UN__c From Vertex_BSureS__bSureS_ExpCred__c where Credit_Agency__c=:SelectedAgency';
                try{
                    lstSObj = Database.query(strQuery);
                    if(lstSObj != null && lstSObj.size() > 0)
                    {
                        for(Sobject sobj: lstSObj)
                        {
                            sobj.put('Vertex_BSureS__Pwd__c', objCred.Pwd__c);
                            sobj.put('Vertex_BSureS__UN__c',objCred.UN__c); 
                        }
                        Database.SaveResult[] srList = Database.update(lstSObj, false);
                    }else{
                        lstSObj = new list<sobject>();
                        sobject obj = schema.getGlobalDescribe().get('Vertex_BSureS__bSureS_ExpCred__c').newSObject();
                        obj.put('Vertex_BSureS__Pwd__c', objCred.Pwd__c);
                        obj.put('Vertex_BSureS__UN__c',objCred.UN__c); 
                        lstSObj.add(obj);
                        Database.SaveResult[] srList = Database.insert(lstSObj, false);
                    }
                    string strParamKey = 'ExperianAuthenticationCheck';
                    //Update the custom setting for supplier side
                    sobject SObj1 = schema.getGlobalDescribe().get('Vertex_BSureS__BSure_Configuration_Settings__c').newSObject();
                    string strQuery1 = 'Select Id,Vertex_BSureS__Parameter_Value__c From Vertex_BSureS__BSure_Configuration_Settings__c Where Vertex_BSureS__Parameter_Key__c=:strParamKey limit 1';
                    SObj1 = Database.query(strQuery1);
                    if(SObj1 != null){
                        SObj1.put('Vertex_BSureS__Parameter_Value__c','true'); 
                        Database.SaveResult srList1 = Database.update(SObj1, false);
                    }
                    
                    //Update the custom setting for customer side
                    sobject SObj2 = schema.getGlobalDescribe().get('Vertex_BSureC__BSure_Configuration_Settings__c').newSObject();
                    string strQuery2 = 'Select Id,Vertex_BSureC__Parameter_Value__c From Vertex_BSureC__BSure_Configuration_Settings__c Where Vertex_BSureC__Parameter_Key__c=:strParamKey limit 1';
                    SObj2 = Database.query(strQuery2);
                    if(SObj2 != null){
                        SObj2.put('Vertex_BSureC__Parameter_Value__c','true'); 
                        Database.SaveResult srList2 = Database.update(SObj2, false);
                    }

                }catch(Exception e){
                    system.debug('Exception Occured while Updating Experian Credentials.');
                }
            }
        }
        PageReference pgRef = new PageReference('/apex/bSureC_ExperianCred');
        pgRef.setRedirect(true);
        return pgRef;
    }
}