/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTaskOwnerCheck {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Profile p = [select id,Name from profile where name='System Administrator' limit 1];
        system.assertEquals('System Administrator',p.Name);
        User objUser;
        if(p != null  ){
            objUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, country='United States',
            timezonesidkey='America/Los_Angeles', username='u1@32644.com');
            insert objUser;
        }
        
        BSureS_Basic_Info__c supplier = new BSureS_Basic_Info__c(
                    Supplier_Name__c = 'Test Supplier');
        insert supplier;        
        system.assertEquals('Test Supplier',supplier.Supplier_Name__c);
        
        Task t = new Task(
            whatId    = supplier.Id,
            OwnerId   = UserInfo.getUserId(),
            Subject   = 'Task Subject');
        insert t;
        
        system.assertEquals(supplier.Id,t.whatId);
        
        try{
            t.OwnerId = objUser.Id; 
        update t;
        }Catch(Exception e){
            //system.debug('exception...'+e);
        }
        
    }
   
}