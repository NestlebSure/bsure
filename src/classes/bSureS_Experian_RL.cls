public Class bSureS_Experian_RL{
    public list<bSureS_CreditAgency_History__c> Histrylist{get;set;}
    
    public bSureS_Experian_RL(ApexPages.StandardController controller) {
       //String currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
       String currentRecordId  ='a0gd000000AeymaAAB';
       system.debug('*****currentRecordId  ******'+currentRecordId  );
       Histrylist = new  list<bSureS_CreditAgency_History__c>();
       Histrylist  =[Select id ,Creditsafe_Credit_Limit__c,Creditsafe_Rating__c,CreatedDate,CreditSafe_Provider_Description__c,
                       Company_DBT__c,Report_Date__c,Highest_amt_extended__c
                       from bSureS_CreditAgency_History__c where Supplier_Basic_Info_Id__c=:currentRecordId 
                         and RecordTypeId ='012d0000001lAaVAAU' order by createddate deSC];
         system.debug('*****Histrylist  **'+Histrylist  );

    }
}