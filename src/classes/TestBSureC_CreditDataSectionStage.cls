/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CreditDataSectionStage {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
       
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
        objbasicinfo.Customer_Group_Id__c = '2345';
        objbasicinfo.Customer_Name__c = 'SAM';
        insert objbasicinfo;
        system.assertEquals('SAM', objbasicinfo.Customer_Name__c);
        BSureC_Credit_Data_Stage__c reccrdtdtpblsh = new BSureC_Credit_Data_Stage__c();
			reccrdtdtpblsh.CE__c = '2314';
			reccrdtdtpblsh.Average_Days_Slow__c = Integer.valueOf('4');
			reccrdtdtpblsh.Bill_to_Account__c = '';
			reccrdtdtpblsh.Credit_Limit__c = Integer.valueOf('12345');
			reccrdtdtpblsh.Discount__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Exposure__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Group_Id__c = '2345';
			reccrdtdtpblsh.Last_Review__c = Date.parse('11/19/2012');
			reccrdtdtpblsh.of_Limit__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Prompt__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Risk_Category__c = '';
			reccrdtdtpblsh.Slow__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Exception__c = NULL;
			reccrdtdtpblsh.Customer_Basic_Info_Id__c = String.valueOf(objbasicinfo.Id);
			insert reccrdtdtpblsh;
			// Test.startTest();
			BSureC_CreditDataSectionStage.loadbutton();
			// Test.stopTest();
       
    }
    
}