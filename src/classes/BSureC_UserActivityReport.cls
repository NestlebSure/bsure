/***********************************************************************************************
* Controller Name :  BSureC_UserActivityReport
* Date : 27 Dec 2012
* Author : Neha Jaiswal
* Purpose : This class written to generate report for which customers has been done for review  
* Change History :
* Date Programmer Reason
* -------------------- ------------------- -------------------------
* 12/27/2011 Neha Jaiswal Initial Version
* 06/03/2013 worked for Reviews done Count not populat currectly.
* 08/07/2013 Kishorekumar A   All Customer users are not populate on User list , in that case put code for to get all customer users
**************************************************************************************************/
global with sharing class BSureC_UserActivityReport { 
    public string   strZoneId{get;set;} // to store Zone Id
    public string   strSubZoneId{get;set;} // to store Sub Zone Id
    public string   strCountryId{get;set;}  // to store Country Id
    public string   strAnalystId{get;set;}  // to store Country Id
    public Integer  pageNumber;//used for pagination
    public Integer  pageSize;//used for pagination
    public Integer  totalPageNumber;//used for pagination
    public Integer  PrevPageNumber {get;set;}//used for pagination 
    public Integer  NxtPageNumber {get;set;}//used for pagination
    public Integer  NlistSize {get;set;}//used for pagination
    public Integer  Endlst {get;set;}//used for pagination
    public integer  totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public string   strFromDate{get;set;} // to hold from date
    public string   strToDate{get;set;} // to hold to Date
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records  
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public list<SelectOption> AnalystOptions{get;set;} // selectOptins for list of User(Analyst) Records
    public list<User> lstUsers{get;set;}
    public list<WrapperClass> lstWrap{get;set;}
    public list<WrapperClass> lstwrapPagination{get;set;}
    public BSureC_Customer_Basic_Info__c objCustomer{get;set;} // to display from and to dates with standard datepicker   
    public Boolean idsflag = false;
    public string  strAnalystName {get;set;}
    public boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public string strAnalystSearch{get;set;}
    public String[] strAnalystProfiles=new String[] {'System Administrator','BSureC_Analyst','BSureC_CreditAdmin','BSureC_Manager'};//profiles to be allowed
    public list<User> lstAnalystUser{get;set;} 
    public string  strTodayDate{get;set;}
    public string loginUserProfile {get;set;}
    public string userView{get;set;}//to assign from param
    public string  sortDirection = 'ASC';  
    public string  isDisplayExpBtn {get;set;}
    public string  sortExp = 'Name';
    public Boolean viewEdit{get;set;}
    public Boolean IsExportVisible{get;set;}
    public String strProfilecontain{get;set;}
    //For sorting grid in ascending and descending order
    public string sortExpression
    {
      get
        {
            return sortExp;
        }
      set
        {
            if(value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
    
     //For sorting grid in ascending and descending order
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    //For sorting grid in ascending and descending order
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }    
    
    //For sorting grid in ascending and descending order
    public Pagereference SortData()
    {
         Search();
         return null;
    }
    
    public BSureC_UserActivityReport()
     {   
        list<BSureC_AdditionalAttributes__c> AAttributes = new list<BSureC_AdditionalAttributes__c>();
        
        if(Profile.SObjectType.getDescribe().isQueryable()){
        Profile objCurProfile = [SELECT name FROM Profile WHERE id=:Userinfo.getProfileId()];
        loginUserProfile = objCurProfile.name;
        strProfilecontain = 'BSureC'+'%';
        if(loginUserProfile.equalsIgnoreCase('System Administrator')){
            strZoneId='ALL';
            strSubZoneId='ALL';
            strCountryId='ALL';
         }
        else{
             AAttributes = BSureC_CommonUtil.DefaultUserAttributes(UserInfo.getUserId()); 
	            if(AAttributes != null && AAttributes.size() > 0){
	            strZoneId       = AAttributes[0].BSureC_Zone__c;
	            strSubZoneId    = AAttributes[0].BSureC_Sub_Zone__c;
	            strCountryId    = AAttributes[0].BSureC_Country__c;
	            idsflag=true;
	            }
      		 }
        }

      		 
        strTodayDate = Date.today().format();
        objCustomer = new BSureC_Customer_Basic_Info__c();
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
        lstWrap = new list<WrapperClass>();
        lstwrapPagination = new list<WrapperClass>(); 
        //Date dtYear = Date.Today().addDays(-365);           
        // Date dtToday = Date.Today();        
        // strFromDate = dtYear.format();
        //strToDate =  dtToday.format();
        IsExportVisible=true;    
        getZones();
        getSubZones();
        getCountries();
    } 
	   // <Summary> 
	   // Method Name : getZones
	   // </Summary>
	   // Description : query all the zones 
    public void getZones()
    {  
         if(idsflag==false){
            strZoneId='ALL';
            }
            zoneOptions=new list<selectOption>();
            BSureC_CommonUtil.blnflag=true;
            zoneOptions=BSureC_CommonUtil.getZones();
            getSubZones();
            getCountries();
      }
        
     // <Summary> 
    // Method Name : getSubZones
    // </Summary>
    // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones
    public void getSubZones()
    {   
            if(idsflag==false || strZoneId=='ALL'){
                strSubZoneId='ALL';
                strCountryId='ALL';
            }
            //  strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
            subZoneOptions=new list<selectoption>();
            BSureC_CommonUtil.blnflag=true;
            subZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
            getCountries();  
        }
      
     // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries()
    {       
             {
            if(idsflag==false || strSubZoneId=='ALL'){
              strCountryId='ALL';
            }
            //strCountryId = (strZoneId == 'ALL' || strSubZoneId == 'ALL'?'ALL':strCountryId );
             coutryOptions=new list<selectOption>();
             coutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
             getAnalysts();
          }
    }
    
    public void getAnalysts()
    {   
        AnalystOptions = new list<selectOption>();
        if(strCountryId == 'ALL')
            strAnalystId = 'ALL';
        AnalystOptions  = BSureC_CommonUtil.getAnalysts(strZoneId, strSubZoneId, strCountryId);
     }
     
     // <Summary> 
    //Method Name : ExportToExcel
    // </Summary>
    //Description : To export all records into csv file
    public Pagereference ExportToExcel(){
        Pagereference pageref = new pagereference('/apex/BSureC_UserActivityReportExport');
        return pageref; 
    }
    
    public void showBuyerDetails()
    {
        showeditrecs=true; 
        
        if(User.SObjectType.getDescribe().isQueryable()){       
        lstAnalystUser=[Select Id, Name from User where Profile.Name like : strProfilecontain  and IsActive=true order By Name Asc]; //IN:strAnalystProfiles *******  Profile.Name like 'BSureC%'
        } 
    }
  
     public void closepopup() 
    {
        string strSelectedAnalyst = apexpages.currentpage().getparameters().get('Analystid');
        strAnalystName=strSelectedAnalyst;        
        showeditrecs=false; 
    }
  
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        string strAnalSrch = '%'+strAnalystSearch+'%';
        if(strAnalystSearch != null && strAnalystSearch != '') 
        {
        	if(User.SObjectType.getDescribe().isQueryable()){  
            	lstAnalystUser = [Select Id, Name from User where Name like: strAnalSrch AND Profile.Name like : strProfilecontain AND IsActive=true order By Name Asc]; //IN:strAnalystProfiles
        	}
        }
        else
        {
            if(User.SObjectType.getDescribe().isQueryable()){
            	lstAnalystUser = [Select Id, Name from User where IsActive=true AND Profile.Name like : strProfilecontain order By Name Asc]; //IN:strAnalystProfiles
            }           
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
     // <Summary> 
    //Method Name : Search
    // </Summary>
    //Description : its Search the records each user how many topics,documents and reviews has been created
    public void Search(){
        try
        { 
            
            map<Id,list<LoginHistory>>                                   mapLoginHistory     = new map<Id,list<LoginHistory>>();
            map<Id,list<BSureC_Invoice_Collection_Section__c>>          maptopicOnInvoice    = new map<Id,list<BSureC_Invoice_Collection_Section__c>>();
            map<Id,list<BSureC_On_Going_Credit_Section__c>>         maptopicONGoingCredit    = new map<Id,list<BSureC_On_Going_Credit_Section__c>>();
            map<Id,list<BSureC_Unauthorized_Deduction_Section__c >>   maptopicOnDeduction    = new map<Id,list<BSureC_Unauthorized_Deduction_Section__c >>();
            map<Id,list<BSureC_Unearned_Cash_Discount_Section__c    >> maptopicOnDiscount    = new map<Id,list<BSureC_Unearned_Cash_Discount_Section__c     >>();
            map<Id,list<BSureC_Confidential_Documents_Section__c    >>   maptopicOnConf      = new map<Id,list<BSureC_Confidential_Documents_Section__c >>();
            map<Id,list<BSureC_Invoice_Collection_Section__c>>          mapFileOnInvoice     = new map<Id,list<BSureC_Invoice_Collection_Section__c>>();
            map<Id,list<BSureC_Unauthorized_Deduction_Section__c   >> mapFileOnDeduction     = new map<Id,list<BSureC_Unauthorized_Deduction_Section__c >>();
            map<Id,list<BSureC_Confidential_Documents_Section__c    >>   mapFileOnConf       = new map<Id,list<BSureC_Confidential_Documents_Section__c >>();
            map<Id,list<BSureC_On_Going_Credit_Section__c>>       mapFileNGoingCredit        = new map<Id,list<BSureC_On_Going_Credit_Section__c>>();
            map<Id,list<BSureC_Unearned_Cash_Discount_Section__c        >> mapFileOnDiscount = new map<Id,list<BSureC_Unearned_Cash_Discount_Section__c     >>();
            list<BSureC_AdditionalAttributes__c>          lstAddAttributes                   = new list<BSureC_AdditionalAttributes__c> ();
            map<Id,list<BSureC_Credit_Increase_Section__c>>     mapTopicOnCA                 = new map<Id,list<BSureC_Credit_Increase_Section__c>>();
            map<Id,list<BSureC_Credit_Increase_Section__c>>     mapFileOnCA                  = new map<Id,list<BSureC_Credit_Increase_Section__c>>();
            map<Id,list<BSureC_Credit_Increase__c>>             mapReviewsDone               = new map<Id,list<BSureC_Credit_Increase__c>>();
            list<BSureC_Credit_Increase__c>                     lstCreditAnalysis            = new list<BSureC_Credit_Increase__c>();
            Date dtFrom;
            Date dtTo;
            dtFrom  = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
            dtTo    = ((strToDate != null && strToDate != '')?date.parse(strToDate):null);  
            set<Id> setUserIds = new set<Id>();
            lstUsers = new list<User>();
            lstUsers.Clear();
            lstWrap.clear(); 
               
            if(BSureC_AdditionalAttributes__c.SObjectType.getDescribe().isQueryable()){
	            string strQuery = 'select   id,User__c '+   
	                                        'FROM BSureC_AdditionalAttributes__c  '+ 
	                                        'WHERE Id != null ';
	            if(strZoneId != null && strZoneId != 'ALL')
	                strQuery+=' AND BSureC_Zone__c =:strZoneId  ';
	            if(strSubZoneId != null && strSubZoneId != 'ALL')
	                strQuery+=' AND BSureC_Sub_Zone__c =:strSubZoneId  ';
	            if(strCountryId != null && strCountryId != 'ALL')
	                strQuery+=' AND BSureC_Country__c =:strCountryId  '; 
	            if(strAnalystId != null && strAnalystId != 'ALL')
	                strQuery+=' AND User__c =:strAnalystId  ';
	            lstAddAttributes = database.query(strQuery);            
            }
            
	            for(BSureC_AdditionalAttributes__c BSureA:lstAddAttributes){
	                setUserIds.add(BSureA.User__c);
	            }  
	            
	        if(User.SObjectType.getDescribe().isQueryable()){             
	            string userQuery = ' SELECT Id,Name '+ 
	                               ' FROM User Where Id != null AND IsActive = true ' ;
	            if(strAnalystName != null && strAnalystName != '')
	            {
	                //strAnalystName = '%'+strAnalystName+'%';
	                //system.debug('strAnalystName*&*&***&*&'+strAnalystName);
	                userQuery += ' AND Name like:strAnalystName AND Id In:setUserIds order by name ' + sortDirection ;
	            }else {
	                userQuery+=  ' AND  Id in:setUserIds  order by name ' + sortDirection ;
	            }  	                  
	            lstUsers = database.query(userQuery);
	        }
            dtTo = dtTo.addDays(1);
            
            maptopicOnInvoice      =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Invoice_Collection_Section__c',dtFrom,dtTo,'Topic');
            maptopicONGoingCredit  =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_On_Going_Credit_Section__c',dtFrom,dtTo,'Topic');
            //maptopicONGoingCredit  =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_On_Going_Credit_Section__c',dtFrom,dtTo,'Topic');
            maptopicOnDeduction    =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Unauthorized_Deduction_Section__c',dtFrom,dtTo,'Topic');
            maptopicOnDiscount     =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Unearned_Cash_Discount_Section__c',dtFrom,dtTo,'Topic');
            maptopicOnConf         =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Confidential_Documents_Section__c',dtFrom,dtTo,'Topic');
            mapTopicOnCA           =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Credit_Increase_Section__c',dtFrom,dtTo,'Topic');
            mapFileOnInvoice       =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Invoice_Collection_Section__c',dtFrom,dtTo,'File');
            mapFileOnDeduction     =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Unauthorized_Deduction_Section__c',dtFrom,dtTo,'File');
            mapFileOnConf          =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Confidential_Documents_Section__c',dtFrom,dtTo,'File');
            mapFileOnDiscount      =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Unearned_Cash_Discount_Section__c',dtFrom,dtTo,'File');
            mapFileNGoingCredit    =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_On_Going_Credit_Section__c',dtFrom,dtTo,'File');
           // mapFileNGoingCredit  =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_On_Going_Credit_Section__c',dtFrom,dtTo,'File');                                                       
            mapFileOnCA            =   BSureC_CommonUtil.getUserMapwithlist(setUserIds,'BSureC_Credit_Increase_Section__c',dtFrom,dtTo,'File'); 
           
            String strCompletedstatus = 'Completed';
            string strCAQuery = ' SELECT    id,createdById,LastModifiedById  '+
                                ' FROm  BSureC_Credit_Increase__c '+
                                ' WHERE LastModifiedDate >=:dtFrom and LastModifiedDate <=:dtTo ';
			strCAQuery   += ' AND Status__c =: strCompletedstatus ';                                
            //strCAQuery   += ' AND Status__c =\'Completed\' ';
                
            lstCreditAnalysis = database.query(strCAQuery);
              
            for(BSureC_Credit_Increase__c objCA:lstCreditAnalysis) 
            {
                list<BSureC_Credit_Increase__c> lstCA = new list<BSureC_Credit_Increase__c>();
                lstCA = mapReviewsDone.get(objCA.LastModifiedById);
                if(lstCA == null )
                {
                    lstCA = new list<BSureC_Credit_Increase__c>();
                    lstCA.add(objCA);
                    mapReviewsDone.put(objCA.LastModifiedById,lstCA);
                }else 
                {
                    lstCA.add(objCA);
                }   
              }
             
            if(LoginHistory.SObjectType.getDescribe().isQueryable()){  
            	list<LoginHistory> userHistory = [SELECT    Id,
                                                        UserId,
                                                        LoginTime 
                                                        FROM LoginHistory 
                                                        WHERE UserId in:setUserIds 
                                                        AND LoginTime >=:dtFrom  
                                                        AND LoginTime <=:dtTo ];
            
	            for(LoginHistory LH:userHistory)
	            {
	                list<LoginHistory> History = new list<LoginHistory>();  
	                History = mapLoginHistory.get(LH.UserId);
	                if(History  == null ){
	                    History = new list<LoginHistory>(); 
	                    History.add(LH);
	                    mapLoginHistory.put(LH.UserId,History);
	                }else{  
	                    History.add(LH);
	                }   
	            } 
            }
            for(User u:lstUsers){
            	
                WrapperClass wc = new WrapperClass();
                wc.u                =   u;
                wc.nLogis           =   (mapLoginHistory.get(u.Id) != null? mapLoginHistory.get(u.Id).size():0);
                wc.Commentsmade     =   (mapTopicOnCA.get(u.Id) != null? mapTopicOnCA.get(u.Id).size():0) +(maptopicONGoingCredit.get(u.Id) != null? maptopicONGoingCredit.get(u.Id).size():0) + (maptopicOnInvoice.get(u.Id) != null? maptopicOnInvoice.get(u.Id).size():0) + (maptopicOnDeduction.get(u.Id) != null? maptopicOnDeduction.get(u.Id).Size():0)+ (maptopicOnDiscount.get(u.Id) != null? maptopicOnDiscount.get(u.Id).Size():0)+ (maptopicOnConf.get(u.Id) != null? maptopicOnConf.get(u.Id).Size():0);
                wc.DocumentsPosted  =   (mapFileOnCA.get(u.Id) != null? mapFileOnCA.get(u.Id).size():0) +(mapFileNGoingCredit.get(u.Id) != null? mapFileNGoingCredit.get(u.Id).size():0) +(mapFileOnInvoice.get(u.Id) != null? mapFileOnInvoice.get(u.Id).size():0) + (mapFileOnDeduction.get(u.Id) != null? mapFileOnDeduction.get(u.Id).Size():0)+ (mapFileOnDiscount.get(u.Id) != null? mapFileOnDiscount.get(u.Id).Size():0)+ (mapFileOnConf.get(u.Id) != null? mapFileOnConf.get(u.Id).Size():0);
                wc.ReviewsDone      =   (mapReviewsDone.get(u.Id)  != null ? mapReviewsDone.get(u.Id).size():0);
                wc.strUserId        =   u.id;
                lstWrap.add(wc);    
            }           
          }Catch(Exception ex){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage());
                ApexPages.addMessage(errormsg);
          }
               totallistsize= lstWrap.size();// this size for pagination
               pageSize = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));// default page size will be 10
         
           if(totallistsize==0)
            {
                IsExportVisible=true;
                BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
                NxtPageNumber= lstwrapPagination.size();
                PrevPageNumber=0;
            }
            else
            {
                IsExportVisible=false;
                BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
                NxtPageNumber= lstwrapPagination.size();
                PrevPageNumber=1;
            }  
    }
    
     public pageReference userViewPage()
       {
         pagereference pageView = new pagereference('/apex/BSureC_AdditionalAttribute?Id='+userView +'&viewEdit=false'); 
        return pageView;         
       }
      // <Summary> 
   // Class Name : WrapperClass
   // </Summary>
   // Description : instance of common used class.
   /// <summary>
    /// Search(): to display the fields value from different objects.
    /// </summary>
   /// <returns> instance of common class
    
    public class WrapperClass {
        public User         u               {get;set;}
        public integer      nLogis          {get;set;}
        public integer      Commentsmade    {get;set;}
        public integer      DocumentsPosted {get;set;}
        public integer      ReviewsDone     {get;set;}
        public string       strUserId       {get; set;}
    }
    
      // <Summary> 
     // Class Name : BindData
     // </Summary>
     // Description : pagenation.
     /// <summary>
     /// Search(): to display the fields value from different objects in pages.
     /// </summary>
     /// <returns> pagenation
      public void BindData(Integer newPageIndex)
    {
        try
        {
            lstwrapPagination = new list<WrapperClass>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            if(lstWrap != NULL)
            {
                for(WrapperClass b : lstWrap)  
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstwrapPagination.add(b);// here adding files list
                    }
                }
            }
            pageNumber = newPageIndex;
            NlistSize = lstwrapPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
     // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }


    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }


    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
   
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {   
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
   
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;
        } 
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstwrapPagination=new list<WrapperClass>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(WrapperClass a : lstWrap)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstwrapPagination.add(a);// here adding the folders list      
                }
            } 
            pageNumber = newPageIndex;
            NlistSize=lstwrapPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    public pageReference cancel()
    {
        Pagereference pageref = new pagereference('/apex/BSureC_Reports');
        return pageref; 
    
    }

    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        LastpageData(totalpagenumber);
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
         
}