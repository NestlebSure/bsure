/************************************************************************************************       
Controller Name         : BSureC_CreditDataSectionView       
Date                    : 11/16/2012        
Author                  : Santhosh Palla       
Purpose                 : To Insert the Credit Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/05/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_CreditDataSectionView 
{
    public List<BSureC_Credit_Data_Publish__c> lstcrdtdatapublsh{get;set;}
    private final BSureC_Customer_Basic_Info__c reccustbsicinfo;//to read the Customer Related values from Standard Controller
    public List<SelectOption> lstinfodate{get;set;}//For Information date Picklist Values
    public String plstdateid{get;set;}//Selected date from picklist
    public String str_customer_info_id {get;set;}
    public Map<Date,Date> maplstoptions {get;set;}
    public static String str_credit_cust_info_id{get;set;}
    //For pagenation

    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    
    public List<BSureC_Credit_Data_Publish__c> lstonchangereqList{get;set;}
    public List<BSureC_Credit_Data_Publish__c> Pagenationlist {get;set;}
    
    
    public BSureC_CreditDataSectionView(Apexpages.Standardcontroller controller)
    {
        //CE__c,Group_Id__c,Bill_to_Account__c,Credit_Limit__c,of_Limit__c,Slow__c,
        //Risk_Category__c,Prompt__c,Last_Review__c,Exposure__c,Discount__c,Average_Days_Slow__c
        
        //For pagenation
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = 5;
        
        //for Displaying the Records in Pageblock
        //String.valueOf(pDate)
        
        str_customer_info_id = Apexpages.currentPage().getParameters().get('cust');
        
        //system.debug('------str_customer_info_id--'+str_customer_info_id);
        if(str_customer_info_id == null || str_customer_info_id == '')
        {
            lstcrdtdatapublsh = new List<BSureC_Credit_Data_Publish__c>();
            reccustbsicinfo = (BSureC_Customer_Basic_Info__c)controller.getRecord();
            //system.debug('-----'+(BSureC_Customer_Basic_Info__c)controller.getRecord());
            str_customer_info_id = String.valueOf(reccustbsicinfo.Id);
        }
        
        loaddata();
        
        totallistsize = lstonchangereqList.size();  
        //system.debug('----totallistsize---'+totallistsize);
        if(totallistsize==0)
        {
              BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
              NxtPageNumber=Pagenationlist.size();
              PrevPageNumber=0;
              ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Records Found');
              ApexPages.addMessage(errormsg);
        }
        else
        {
              BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
              NxtPageNumber=Pagenationlist.size();
              PrevPageNumber=1;
              
        } 
        //String strGroupId = ;
        
        
        
    }
    public BSureC_CreditDataSectionView(String str)
    {
        str_customer_info_id = str_credit_cust_info_id;
    }
    public void loaddata()
    {
        lstinfodate = new List<SelectOption>();
        maplstoptions = new Map<Date,Date>();
        //NAGA Check if the user has read access on the BSureC_Credit_Data_Publish__c object
		if(Schema.sObjectType.BSureC_Credit_Data_Publish__c.isAccessible() )
		{
	        lstcrdtdatapublsh = [SELECT CE__c,Group_Id__c,Bill_to_Account__c,Credit_Limit__c,of_Limit__c,Slow__c,
	                        Risk_Category__c,Prompt__c,Last_Review__c,Exposure__c,Discount__c,Average_Days_Slow__c 
	                        FROM BSureC_Credit_Data_Publish__c WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id order by Name DESC];
	        
	        
	        lstonchangereqList =  new List<BSureC_Credit_Data_Publish__c>();    
	        lstonchangereqList =  lstcrdtdatapublsh;
		}
        
    }
    public List<BSureC_Credit_Data_Publish__c> creditdataview()
    {
        List<BSureC_Credit_Data_Publish__c> rec_credit_data_view = new List<BSureC_Credit_Data_Publish__c>();
        loaddata();
        
        rec_credit_data_view = lstonchangereqList;
        
        return rec_credit_data_view;
    }
    
    
     //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    
    public Integer getPageNumber()
    {
        return pageNumber; 
    }

  /// <summary>
  /// Below method is for getting pagesize how many records per page .
  /// <param name="PageSize"></param>
  /// <returns>Integer</returns>  
  /// </summary>    
    public Integer getPageSize()
    {
        return pageSize;
    }

  /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

  /// <summary>
  /// Below method is for enabling and disabling the nextbutton of pagination.
  /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }

  /// <summary>
  /// Below method gets the total no.of pages.
  /// <param name="TotalPageNumber"></param>
  /// <returns>Integer</returns>    
  /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber=0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }

        return totalPageNumber;
    }
    
  /// <summary>
  /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
  /// </summary>    
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************88pavajflajf');
        try
        {
            Pagenationlist = new List<BSureC_Credit_Data_Publish__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize =5;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
           
          if(lstonchangereqList!=NULL)
          {
                  for(BSureC_Credit_Data_Publish__c a : lstonchangereqList)
                  {
                    
                      counter++;
                       if (counter > min && counter <= max) 
                    {
                           Pagenationlist.add(a);// here adding the folders list       
                    }
                          
                      
                  }
            }

            
            pageNumber = newPageIndex;
        NlistSize=Pagenationlist.size();
        }

        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }

    }



   public void pageData(Integer newPageIndex)
    {
         
         
         
         if (newPageIndex > pageNumber)
            {
                                                            
                PrevPageNumber=PrevPageNumber+pagesize;   
                NxtPageNumber=NxtPageNumber+NlistSize; 
                     
            }
            
         if (newPageIndex < pageNumber)               
              
            {                                 
                PrevPageNumber=PrevPageNumber-pagesize;               
                NxtPageNumber=NxtPageNumber-NlistSize;      
                     
            }
          
            //system.debug('NlistSize============='+NlistSize);
            
    }
    
    public void LastpageData(Integer newPageIndex)
    {
        
      try
        {
            Pagenationlist = new List<BSureC_Credit_Data_Publish__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;      
            //system.debug('NlistSizeLastpage1============='+NlistSize);
              min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            if(lstonchangereqList!=NULL)
              {
                      for(BSureC_Credit_Data_Publish__c a : lstonchangereqList)
                      {
                          counter++;
                          
                          if (counter > min && counter <= max) 
                          {
                                 Pagenationlist.add(a);// here adding the folders list      
                          }
                      }
                }
            
            pageNumber = newPageIndex;
            //system.debug('min======'+min);  
          //system.debug('max======'+max); 
           NlistSize=Pagenationlist.size();
           //system.debug('NlistSizeLastpage2============='+NlistSize);
           
        
        
             //system.debug('inside lastpage==============='+newPageIndex);
             
             PrevPageNumber=totallistsize - NlistSize +1;   
                NxtPageNumber=totallistsize;
           
           
        }
          
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
        
    }
    
    // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
     // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    }
}