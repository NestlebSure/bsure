/************************************************************************************************       
Controller Name         : BSureC_CreditDataSection       
Date                    : 11/05/2012        
Author                  : Santhosh Palla       
Purpose                 : To Insert the Credit Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/05/2012      Santhosh.Palla            Initial Version
                          01/22/2013      VINU PRATHAP              Modified to show up message from Label
                          01/23/2013      VINU PRATHAP              Modified Queries with no where or limit class 
						  05/07/2013      B.Anupreethi              Modified condition to delete the data while uploading new file: as per tom/nitin confirmation  
**************************************************************************************************/
global with sharing class BSureC_CreditDataSection {


public String filename{get;set;}//For Uploaded file from page

public boolean AlertUser{get;set;}

public Transient blob bfilecontent{get;set;} // Holds the Uploaded file content from page
/// <summary>  
 /// Constructor  
 /// </summary>
 public BSureC_CreditDataSection()
 {
    //NAGA Check if the user has read access on the BSureC_Credit_Data_Stage__c object
	if(Schema.sObjectType.BSureC_Credit_Data_Stage__c.isAccessible() )
	{
    	List<BSureC_Credit_Data_Stage__c> existRec = [SELECT Id from BSureC_Credit_Data_Stage__c where id!= null];
   		if(existRec.size()>0)
    	{
    		AlertUser = true;
 		}
	    else
	    {
	    	AlertUser=false;
	  	}
	}
 }
 
    //Modified by VINU PRATHAP
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>    
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
     
 public pagereference SaveDetailsFile()  
 {
    /*//system.debug('----bfilecontent-----'+bfilecontent);
    //system.debug('-----filename----'+filename);
    List<BSureC_Credit_Data_Section_Attachement__c> lrecinsert = new List<BSureC_Credit_Data_Section_Attachement__c>();
    BSureC_Credit_Data_Section_Attachement__c recinsert = new BSureC_Credit_Data_Section_Attachement__c();
    recinsert.File_Name__c = filename;
    recinsert.IsActive__c = true;
    //objinsert.
    lrecinsert.add(recinsert);
    Database.Saveresult[] myresult = Database.insert(lrecinsert,false);
    
    //system.debug('----myresult----'+myresult);
     Id insertrecordId;
     for (Integer i = 0; i < myresult.size(); i++) 
     {
        insertrecordId = myresult[i].getId();
     }
     
    //  List<Attachment> attachment = new List<Attachment>();
      Attachment attachment = new Attachment();
      attachment.Name = filename;
      attachment.Body = bfilecontent;
      attachment.ParentId = insertrecordId;
     // attachment.add(recattachment);
      try
      {
      //    insert attachment;
      }
      catch(exception e)
      {
        //system.debug('---Exception----'+e);
      }
     // BsureC_CreditDataSectionBatch.parentrecid = insertrecordId;
    //  BsureC_CreditDataSectionScheduler obj = new BsureC_CreditDataSectionScheduler();    
    //  obj.execute(info, scope);  
    //BsureC_CreditDataSectionScheduler.parentrecid = insertrecordId;*/
    //system.debug('------bfilecontent---'+filename);
    if(bfilecontent!=NULL && filename.contains('.csv'))
    {
         pagereference pageref=new pagereference('/apex/BSureC_CreditDataStageListView');
        
        //Modified to limit the query by VINU PRATHAP
        //Delete[SELECT Id from BSureC_Credit_Data_Stage__c];
        //Delete[SELECT Id from BSureC_Credit_Data_Stage__c where id!= null AND createddate!=TODAY];
        //NAGA Check if the user has read access on the BSureC_Credit_Data_Stage__c object
		if(Schema.sObjectType.BSureC_Credit_Data_Stage__c.isAccessible() )
		{
        	List<BSureC_Credit_Data_Stage__c> lst_todelete= new List<BSureC_Credit_Data_Stage__c>([SELECT Id from BSureC_Credit_Data_Stage__c where id!= null]);
	        if(BSureC_Credit_Data_Stage__c.sObjectType.getDescribe().isDeletable())
	        {
	        	Database.delete(lst_todelete);
	        }
		}	
	       // Delete[SELECT Id from BSureC_Credit_Data_Stage__c where id!= null];//Anu:7yhMay13:modified as per tom/nitin confirmation 
	        BsureC_CreditDataSectionScheduler.createBatchesFromCSVFile(bfilecontent,'BSureC_Credit_Data_Stage__c','');
	        return pageref;
       // return null;
    }
    else
    {
        //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Upload a csv File');
        //ApexPages.addMessage(errormsg);
        showErrorMessage(system.Label.BSureC_Please_upload_a_csv_file);
        
        return null;
    }
    
 }
}