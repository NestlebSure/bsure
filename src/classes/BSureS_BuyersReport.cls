/***********************************************************************************************
* Controller Name   : BSureS_BuyersReport 
* Date              : 
* Author            : Kishorekumar A 
* Purpose           : Class for Supplier Buyers Report
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 17/12/2012                                    Initial Version
* 17/07/2013			Aditya V				Fixed Viewstate and Heapsize issues in Prod
* 12/08/2013			B.Anupreethi			Code review changes
**************************************************************************************************/
global with sharing class BSureS_BuyersReport 
{
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public list<BSureS_Assigned_Buyers__c> listSupplier {get;set;}
    public list<BSureS_User_Additional_Attributes__c> lstbuyerInfoPagination{get;set;}//to hold list supplier info
    public list<BSureS_Assigned_Buyers__c> lstBuyersGroup{get;set;}
    
    public transient list<buyerWrapperlist> buyerNames{get;set;}
    public transient list<BSureS_Assigned_Buyers__c> listAssignedNonCredit {get;set;}
    public transient list<BSureS_User_Additional_Attributes__c> lstUserAdditionatt{get;set;}   
    public transient list<buyerWrapperlist> lstbuyerPagination{get;set;}
    public transient list<supplierWrapperlist> lstSupplierWrapper{get;set;}
    public transient list<list<supplierWrapperlist >> wrapperCollection  {get; private set;}
    
    //public List<BSureS_Assigned_Buyers__c> listbuyersforSupp {get;set;}
    public map<id,string> mapBuyers{get;set;}
    public map<id,list<BSureS_Basic_Info__c>> mapBuyerSupplierList{get;set;}
    public map<string, list<BSureS_Assigned_Buyers__c>> mapBuyerSuppliers {get;set;}
    public map<id,list<BSureS_Assigned_Buyers__c>> map_SupplierInfo{get;set;}
    
    public set<string> buyerWList{get;set;}
    public set<string> setBuyersQuery {get;set;}
    public set<string> setBuyerIDs{get;set;}
    public list<string> lstBuyerIds{get;set;}
    //public list<string> supplId {get;set;}
    
    //Export
    list<BSureS_Basic_Info__c> lst_supplier_info_Export{get;set;}
    
    //pagenation
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public Integer ReportPageSize{get;set;}
    
    //test  Export
    public list<BSureS_User_Additional_Attributes__c> lstUAExport{get;set;}
    public list<String> lstuabuyerIds{get;set;}
    public list<BSureS_Assigned_Buyers__c> lstAssngBuyerExport;
    
    
    public boolean bFromLastBtnClick{get;set;}
    
    public class buyerWrapperlist
    { 
        public string buyerName{get;set;}
        public string buyerEmail{get;set;}
        public Id SupplId {get;set;} 
        public Id buyerId {get;set;} 
        public string supplierInfo {get;set;}
    }
    
    public class supplierWrapperlist
    {
        public string supplier_name{get;set;}
        public string category_name{get;set;}
        public string globe_id{get;set;}
        public string analyst_name{get;set;}
        public string buyer_name{get;set;}
        public string rating{get;set;}
        public string spend{get;set;}
        public string ratingtext{get;set;}
        public string reviewStatus{get;set;}
        public String ExpReviewEndDate{get;set;}// Kishore
        public String lastReviwCompleteDate{get;set;}// kishore
    } 
    
    public BSureS_BuyersReport()
    {
    	bFromLastBtnClick = false;
        string currentUserId = UserInfo.getUserId();
        ReportPageSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerReport_PageSize').get(0));
        //Anu Check if the user has read access on the BSureS_User_Additional_Attributes__c object
    	if (Schema.sObjectType.BSureS_User_Additional_Attributes__c.isAccessible())
    	{
        	lstbuyerInfoPagination = new list<BSureS_User_Additional_Attributes__c>([select BSureS_Zone__c,BSureS_Sub_Zone__c,BSureS_Country__c 
                                                    from BSureS_User_Additional_Attributes__c where User__c =:currentUserId]);
    	}
        if(lstbuyerInfoPagination != null && lstbuyerInfoPagination.size() > 0)
         {
            strZoneId = lstbuyerInfoPagination.get(0).BSureS_Zone__c;
            strSubZoneId = lstbuyerInfoPagination.get(0).BSureS_Sub_Zone__c;
            strCountryId = lstbuyerInfoPagination.get(0).BSureS_Country__c;
         }                  
        getZones();
        getSubZones();
        getCountries();  
        getBuyerslist();
    } 
    
    public void getZones()
    {  
      BSureS_CommonUtil.isReport = true;
      zoneOptions = new list<SelectOption>();
      zoneOptions = BSureS_CommonUtil.getZones();
    }
    
    public void getSubZones()
    {    
            
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
        //strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        
    }
   	
   	public void getCountries()
    {               
        if(strZoneId == 'ALL' || strSubZoneId == 'ALL' )
        strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();       
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
    }
    
    public void Search()
    {
        //system.debug('strZoneId==========='+strZoneId );
        //system.debug('strSubZoneId========='+strSubZoneId );
        //system.debug('strCountryId=========='+strCountryId );
        getBuyerslist();
    }
    
    public pagereference Cancel(){
        return page.BSureS_Reports;
    }
    
    public void getBuyerslist()
    {
        setBuyersQuery = new set<string>();
        //Anu Check if the user has read access on the BSureS_Assigned_Buyers__c object
    	if (Schema.sObjectType.BSureS_Assigned_Buyers__c.isAccessible())
    	{ 
	        String  strquery = '';
	        strquery += 'SELECT Buyer_Id__c FROM BSureS_Assigned_Buyers__c ';
	        strquery += 'WHERE Supplier_ID__c != null and Buyer_Id__r.IsActive= true ORDER BY Buyer_ID__c ';
	        lstBuyersGroup = Database.Query(strquery);
    	}
        
        //system.debug('lstBuyersGroup===size=='+lstBuyersGroup.size());
        for(BSureS_Assigned_Buyers__c aobj: lstBuyersGroup)
        {
        	setBuyersQuery.add(string.valueOf(aobj.Buyer_Id__c));
        }
        lstBuyersGroup.clear();
        GetBuyerUserAttributes();
        
        //pagination
        totallistsize = lstBuyerIds.size();// this size for pagination
        pageSize = ReportPageSize;
        
        if(totallistsize == 0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=setBuyerIDs.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=NlistSize;
            PrevPageNumber=1;
        }
    }
    
    public void GetBuyerUserAttributes()
    {
    	set<string> setBuyIdTemp = new set<string>();
    	lstBuyerIds = new  list<string>();
    	//Anu Check if the user has read access on the BSureS_User_Additional_Attributes__c object
    	if (Schema.sObjectType.BSureS_User_Additional_Attributes__c.isAccessible())
    	{
	    	string strQuery = 'select id,User__c,User__r.Name,User__r.Email,Zone_Name__c,BSureS_Zone__c,'+
	                                'Sub_Zone_Name__c,BSureS_Sub_Zone__c,Country_Name__c,BSureS_Country__c '+ 
	                                'from BSureS_User_Additional_Attributes__c where User__c IN :setBuyersQuery ';
	        //system.debug('strZoneId=====*****======='+strZoneId);
	        //system.debug('strSubZoneId======*****====='+strSubZoneId);
	        //system.debug('strQuery=======******========'+strQuery);
	        if(strZoneId != null && strZoneId != 'ALL')
	        {
	            strQuery +=' AND BSureS_Zone__c =: strZoneId ';
	        }
	        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
	        {
	            strQuery +=' AND BSureS_Sub_Zone__c =: strSubZoneId ';
	        }
	        if(strCountryId != null && strCountryId != 'ALL'  )
	        {
	            strQuery +=' AND BSureS_Country__c =: strCountryId '; 
	        }
	        lstUserAdditionatt = Database.Query(strQuery);
    	}
        
        if(lstUserAdditionatt != null && lstUserAdditionatt.size() > 0)
        {
        	for(BSureS_User_Additional_Attributes__c objEachUserAddAttrib : lstUserAdditionatt)
        	{
        		setBuyIdTemp.add(objEachUserAddAttrib.User__c);        		
        	}	
        	lstBuyerIds.addAll(setBuyersQuery);
        	lstBuyerIds.sort();        
        }
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************IN BindData');
        try
        {
            lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>();
            lstbuyerPagination = new list<buyerWrapperlist>();
            buyerWList = new set<string>();
            setBuyerIDs = new set<string>();
            mapBuyers = new map<id,string>();
	        mapBuyerSuppliers = new  map<string, list<BSureS_Assigned_Buyers__c>>();
            
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex);
            //system.debug('*************totalPageNumber =='+totalPageNumber);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null || pageSize > 5) {pageSize = 3;}
            if(bFromLastBtnClick)
            {
	            if (newPageIndex > pageNumber)
	            {
	                min = pageNumber * pageSize;
	                max = newPageIndex * pageSize;
	            }
	            else
	            {
	                max = newPageIndex * pageSize;
	                min = max - pageSize;
	            }
            }
            else
            {
            	min = (newPageIndex-1) * pageSize;
            	max = newPageIndex * pageSize;
            }
            
            if(lstBuyerIds != NULL)
            {
                for(string b : lstBuyerIds)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        setBuyerIDs.add(b);// here adding files list
                    }
                }
            }
            //system.debug('SetBuyerIDS+++++' + setBuyerIDs);
            //Anu Check if the user has read access on the BSureS_User_Additional_Attributes__c object
	    	if (Schema.sObjectType.BSureS_Assigned_Buyers__c.isAccessible())
	    	{   
	        	listAssignedNonCredit = [select id,Buyer_Id__c ,Buyer_Name__c,Buyer_Id__r.Name, Buyer_Id__r.Email,
		        						 Supplier_ID__c, Supplier_ID__r.Supplier_Name__c, Supplier_ID__r.Analyst_Name__c,
		        						 Supplier_ID__r.Globe_ID__c, Supplier_ID__r.Supplier_Category__c,
		        						 Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__r.City__c,Supplier_ID__r.Review_Status__c, 
		        						 Supplier_ID__r.Spend__c,Supplier_ID__r.Rating_Type__c,Supplier_ID__r.Rating__c,
		        						 Supplier_ID__r.State_Name__c,Supplier_ID__r.Review_Complete_Date__c from BSureS_Assigned_Buyers__c 
		        						 where Supplier_ID__c != null and Buyer_Id__r.IsActive= true 
		        						 and Buyer_Id__c IN : setBuyerIDs LIMIT 10000 ];
	    	}
	        
	        if(listAssignedNonCredit != null && listAssignedNonCredit.size() > 0)
	        {						 
		        for(BSureS_Assigned_Buyers__c buyerObj:listAssignedNonCredit)
		        { 
		            buyerWList.add(buyerObj.Buyer_Id__c);
		            
		            if(mapBuyerSuppliers.containsKey(buyerObj.Buyer_Id__c))
		            {
		                list<BSureS_Assigned_Buyers__c> objTempIDs = mapBuyerSuppliers.get(buyerObj.Buyer_Id__c);
		                objTempIDs.add(buyerObj);
		                mapBuyerSuppliers.put(buyerObj.Buyer_Id__c, objTempIDs);
		            }
		            else
		            {
		                list<BSureS_Assigned_Buyers__c> objTempIDs = new list<BSureS_Assigned_Buyers__c>();
		                objTempIDs.add(buyerObj);
		                mapBuyerSuppliers.put(buyerObj.Buyer_Id__c, objTempIDs);
		            }
		            if(mapBuyers.containsKey(buyerObj.Supplier_ID__c))
		            {
		                string strBuyer = mapBuyers.get(buyerObj.Supplier_ID__c);
		                mapBuyers.remove(buyerObj.Supplier_ID__c);
		                strBuyer += buyerObj.Buyer_Name__c + ', ';
		                mapBuyers.put(buyerObj.Supplier_ID__c, strBuyer);
		            }
		            else
		            {
		                mapBuyers.put(buyerObj.Supplier_ID__c, buyerObj.Buyer_Name__c + ', ');
		            }
		        }
		        GetBuyersData();
	        }
			
			if(buyerNames != null && buyerNames.size() > 0)
			{
				for(buyerWrapperlist b : buyerNames)
                {
                	lstbuyerPagination.add(b);// here adding files list
                }	
			}
			pageNumber = newPageIndex;
            NlistSize=lstbuyerPagination.size();
            
            if(bFromLastBtnClick)
            {
            	PrevPageNumber=totallistsize - NlistSize +1;   
            	NxtPageNumber=totallistsize; 
            }
            bFromLastBtnClick = false;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    
    public  void GetBuyersData()
    {
    	buyerNames = new list<buyerWrapperlist>();
        for(string buyerObj : mapBuyerSuppliers.keySet())
        {
            buyerWrapperlist wObj = new buyerWrapperlist(); 
            BSureS_Assigned_Buyers__c objBuyerAssg = mapBuyerSuppliers.get(buyerObj).get(0);
            wObj.buyerId = objBuyerAssg.Buyer_Id__c;
            wObj.buyerName = objBuyerAssg.Buyer_Id__r.Name;
            wObj.buyerEmail = objBuyerAssg.Buyer_Id__r.Email;
            if(objBuyerAssg.Buyer_Id__c != null )
            {
                wObj.supplierInfo = getSupplierDetails(objBuyerAssg.Buyer_Id__c);
            }
            buyerNames.add(wObj);
        }
    }
    
    public string getSupplierDetails(string strBuyerID)
    {
        string strSupplier = '';
        map_SupplierInfo = new map<id,list<BSureS_Assigned_Buyers__c>>();
        listSupplier = new list<BSureS_Assigned_Buyers__c>();
        ////system.debug('buyerString=========888888888888========='+buyerString);
        if(mapBuyerSuppliers.get(strBuyerID) != null)
        {
            listSupplier = mapBuyerSuppliers.get(strBuyerID);
            map_SupplierInfo.put(strBuyerID,listSupplier);
        }
        /*              
        if(listSupplier != null && listSupplier.size() > 0)
        {
            //strSupplier += 'Supplier Name\\tSupplier Category Name \\tSupplier City \\tSupplier State\\n';
            for(BSureS_Basic_Info__c objSuppInfo : listSupplier)
            {
                list<string> strSupplier = new list<string>();
                strSupplier.add(objSuppInfo.Supplier_Name__c);
                strSupplier.add(objSuppInfo.Supplier_Category_Name__c);
                strSupplier.add(objSuppInfo.City__c);
                strSupplier.add(objSuppInfo.State_Name__c);
                strSuppList.add(strSupplier);
            }
        }
        */
        map<Id,BSureS_Credit_Analysis__c> creditStatusRecs = new map<Id,BSureS_Credit_Analysis__c>();
        if(!listSupplier.isEmpty()){
        	creditStatusRecs = QueryCreditStatus();
        }
        if(listSupplier != null && listSupplier.size() > 0)
        {
            strSupplier += '<table width=\'100%\' border=\'0\'><tr height=\'20\'>';
            strSupplier += '<td><b>Supplier Name</b></td>';
            strSupplier += '<td><b>Category Name</b></td>';
            strSupplier += '<td><b>Review Status </b></td>';
            strSupplier += '<td><b>Expected Review End Date</b></td>';
            strSupplier += '<td><b>Rating</b></td>';
            strSupplier += '<td><b>Review Completed Date</b></td>';
            strSupplier += '<td><b>Spend</b></td>';
            strSupplier += '<td><b>Buyers</b></td>'; 
            //strSupplier += '<td><b>Globe ID</b></td>';
            //strSupplier += '<td><b>Analyst Name</b></td>';
            //strSupplier += '<td><b>Supplier City Name</b></td>';
            //strSupplier += '<td><b>Supplier State Name</b></td></tr>';
            for(BSureS_Assigned_Buyers__c objSuppInfo : listSupplier)
            {
                string strBuyerlist = mapBuyers.get(objSuppInfo.Supplier_ID__c);
                if(strBuyerlist != null && strBuyerlist != '')
                {
                    strBuyerlist = strBuyerlist.subString(0,strBuyerlist.length() - 2);
                }
                string strCHK ='';
                //if(objSuppInfo.Supplier_ID__r.Rating_Type__c != null)
                //{
                //	strCHK = objSuppInfo.Supplier_ID__r.Rating_Type__c.replace('"','\'');
                //}
                Date expreviewdate = null;
                String strexpected = '';
                String strCompletedDate = '';
                if(!creditStatusRecs.isEmpty()){
	            	String supplId=objSuppInfo.Supplier_ID__c;
	            	supplId =  supplId.subString(0,15) ;
	            	System.debug('supplId=============='+supplId);
	            	if(creditStatusRecs.containsKey(supplId)){
	            		expreviewdate  = creditStatusRecs.get(supplId).Expected_Review_End_Date__c != null ? creditStatusRecs.get(supplId).Expected_Review_End_Date__c : null;
	            		strexpected = expreviewdate != null ? expreviewdate.month() +'/'+ expreviewdate.day() +'/'+expreviewdate.year() : '' ;
	            		strCHK = creditStatusRecs.get(supplId).Rating__c;
	            	
	            	}
	            }
	            if(objSuppInfo.Supplier_ID__r.Review_Complete_Date__c != null){
	            	Date d = objSuppInfo.Supplier_ID__r.Review_Complete_Date__c;
	            	strCompletedDate = d.month() + '/'+d.day() +'/'+d.year();
	            }
                strSupplier += '<tr height=\'20\'>';
                strSupplier += '<td width=\'15%\'><a href=\'/apex/Bsures_viewSupplierDetails?id=' + objSuppInfo.Supplier_ID__c + '\' target=\'_blank\' >' + objSuppInfo.Supplier_ID__r.Supplier_Name__c + '</a></td>';
                strSupplier += '<td width=\'10%\'>' + objSuppInfo.Supplier_ID__r.Supplier_Category_Name__c + '</td>';
                strSupplier += '<td width=\'8%\'>' + objSuppInfo.Supplier_ID__r.Review_Status__c + '</td>';
                strSupplier += '<td width=\'12%\'>' +  strexpected + '</td>';
                strSupplier += '<td width=\'6%\'>' + strCHK  + '</td>';
                strSupplier += '<td width=\'10%\'>' +  strCompletedDate + '</td>';
                strSupplier += '<td width=\'10%\'>' + '$'+String.valueOf(objSuppInfo.Supplier_ID__r.Spend__c) + '</td>';
                strSupplier += '<td width=\'19%\'>' + strBuyerlist + '</td>'; //mapBuyers.get(objSuppInfo.id) 
                //strSupplier += '<td width=\'10%\'>' + objSuppInfo.Supplier_ID__r.Globe_ID__c + '</td>';
                //strSupplier += '<td width=\'13%\'>' + objSuppInfo.Supplier_ID__r.Analyst_Name__c + '</td>';
                //strSupplier += '<td width=\'25%\'>' + objSuppInfo.State_Name__c + '</td>';
                strSupplier += '</tr>';
            }
            strSupplier += '</table>';   
        }
         /*//system.debug('buyerNames==========='+buyerNames);
         listbuyersforSupp = [select Supplier_ID__c from BSureS_Assigned_Buyers__c where Buyer_Id__c=:buyerId];
         //system.debug('listbuyersforSupp==========='+listbuyersforSupp);
         for(BSureS_Assigned_Buyers__c buyerObjSupp: listbuyersforSupp)
         {
            supplId.add(buyerObjSupp.Supplier_ID__c);
         } 
         //system.debug('supplId=====88888========='+supplId);
         listSupplier = [select id,Supplier_Name__c,Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c
                         from BSureS_Basic_Info__c limit 5];
         */
         strSupplier = strSupplier.replace('null','');
         return strSupplier;
    }
    
    public pageReference ExportExcel() 
    {
        getExport();
        pageReference pRef = new pageReference('/apex/BSureS_BuyersReportExport');
        return pRef;
    }
    
    public void getExport()
    {
    	//system.debug('BuyerIds===================='+lstBuyerIds.size());
    	lstUAExport = new list<BSureS_User_Additional_Attributes__c>();
    	lstuabuyerIds = new list<string>();
    	String strAddQuery = '';
    	
    	if(lstBuyerIds != null && lstBuyerIds.size() > 0)
    	{
    		strAddQuery +='select id,User__c,BSureS_Zone__c,BSureS_Sub_Zone__c,BSureS_Country__c ';
    		strAddQuery +=' from BSureS_User_Additional_Attributes__c where User__c IN :lstBuyerIds  ';
    		if(strZoneId != null && strZoneId != 'ALL')
	        {
	            strAddQuery +=' AND BSureS_Zone__c =: strZoneId ';
	        }
	        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
	        {
	            strAddQuery +=' AND BSureS_Sub_Zone__c =: strSubZoneId ';
	        }
	        if(strCountryId != null && strCountryId != 'ALL'  )
	        {
	            strAddQuery +=' AND BSureS_Country__c =: strCountryId '; 
	        }
    	}
    	if(strAddQuery != null && strAddQuery != '')
    	{
    		lstUAExport = Database.Query(strAddQuery);
    	}
    	if(lstUAExport != null && lstUAExport.size() > 0)
    	{
    		for(BSureS_User_Additional_Attributes__c addObj : lstUAExport)
    		{
    			lstuabuyerIds.add(addObj.User__c);
    		}
    	}
    	if(lstuabuyerIds != null && lstuabuyerIds.size() > 0)
    	{
    		lstAssngBuyerExport = new list<BSureS_Assigned_Buyers__c>([select id,Buyer_Id__c ,Buyer_Name__c, Supplier_ID__c,
	        						 Supplier_ID__r.Supplier_Name__c, Supplier_ID__r.Analyst_Name__c,
	        						 Supplier_ID__r.Globe_ID__c, Supplier_ID__r.Supplier_Category__c,
	        						 Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__r.City__c,Supplier_ID__r.Review_Status__c, 
	        						 Supplier_ID__r.Spend__c,Supplier_ID__r.Rating_Type__c,Supplier_ID__r.Rating__c,
	        						 Supplier_ID__r.State_Name__c,Supplier_ID__r.Review_Complete_Date__c from BSureS_Assigned_Buyers__c 
	        						 where Supplier_ID__c != null and Buyer_Id__r.IsActive= true 
	        						 and Buyer_Id__c IN : lstuabuyerIds LIMIT 10000]); 
    	}
    	//system.debug('lstAssngBuyerExport======'+lstAssngBuyerExport.size());
    	if(lstAssngBuyerExport != null && lstAssngBuyerExport.size() > 0)
    	{
    		wrapperCollection=collectionOfCollections(lstAssngBuyerExport);
    	}
    	//System.debug('wrapperCollection.....'+wrapperCollection.size());
    }
    
    public List<List<supplierWrapperlist >> collectionOfCollections(List<BSureS_Assigned_Buyers__c> coll){ 
        List<List<supplierWrapperlist >> mainList = new List<List<supplierWrapperlist >>();
        List<supplierWrapperlist > innerList;
        Integer idx = 0;
        //System.debug('coll============'+coll.size());
        if(coll!=null)
        {
        	//system.debug('coll======'+coll.size());
        	innerList = new List<supplierWrapperlist >();
        	innerList.clear();
        	 map<Id,BSureS_Credit_Analysis__c> creditStatusRecs = new map<Id,BSureS_Credit_Analysis__c>();
		     creditStatusRecs = QueryCreditStatus();
            for(BSureS_Assigned_Buyers__c  sObj:coll)
            {
                if (Math.mod(idx++, 1000) == 0 ) 
                {
                	mainList.add(innerList); 
		            innerList = new List<supplierWrapperlist >();
                }
                supplierWrapperlist objsuppWrap = new supplierWrapperlist();
	            objsuppWrap.supplier_name = sObj.Supplier_ID__r.Supplier_Name__c;
	            objsuppWrap.category_name = sObj.Supplier_ID__r.Supplier_Category_Name__c;
	            //objsuppWrap.globe_id = objSuppW.Supplier_ID__r.Globe_ID__c;
	            objsuppWrap.rating = String.valueOf(sObj.Supplier_ID__r.Rating_Type__c);
	            objsuppWrap.reviewStatus = String.valueOf(sObj.Supplier_ID__r.Review_Status__c);
	            //objsuppWrap.ratingtext = String.valueOf(sObj.Supplier_ID__r.Rating__c);
	            objsuppWrap.spend = String.valueOf(sObj.Supplier_ID__r.Spend__c);
	            objsuppWrap.analyst_name = sObj.Supplier_ID__r.Analyst_Name__c;
	            objsuppWrap.buyer_name = sObj.Buyer_Name__c;
	            Date expreviewdate = null;
	            String strexpected = '';
	            String strCompletedDate = '';
	            if(!creditStatusRecs.isEmpty()){
	            	String supplId=sObj.Supplier_ID__c;
	            	supplId =  supplId.subString(0,15) ;
	            	System.debug('supplId=============='+supplId);
	            	if(creditStatusRecs.containsKey(supplId)){
	            		expreviewdate  = creditStatusRecs.get(supplId).Expected_Review_End_Date__c != null ? creditStatusRecs.get(supplId).Expected_Review_End_Date__c : null;
	            		strexpected = expreviewdate != null ? expreviewdate.month() +'/'+ expreviewdate.day() +'/'+expreviewdate.year() : '' ;
	            		objsuppWrap.ratingtext = creditStatusRecs.get(supplId).Rating__c;
	            	}
	            }
	            objsuppWrap.ExpReviewEndDate =  strexpected;
	            if(sObj.Supplier_ID__r.Review_Complete_Date__c != null){
	            	Date d = sObj.Supplier_ID__r.Review_Complete_Date__c ;
	            	strCompletedDate = d.month() + '/'+d.day() +'/'+d.year();
	            }
	            objsuppWrap.lastReviwCompleteDate = strCompletedDate ;
	            innerList.add(objsuppWrap);
            }
            if(innerList != null && innerList.size() > 0 )
            {
            	//System.debug('innerList=======');
            	mainList.add(innerList);
            }	 
            //System.debug('mainList====='+mainList);
        }
        mainList.remove(0);
        return mainList;
    } 
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        //system.debug('pageNumber========previousBtnClick======='+pageNumber);
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            //system.debug('PrevPageNumber===111=========='+PrevPageNumber);
            //system.debug('pagesize========111=========='+pagesize);
            //system.debug('NxtPageNumber======111====='+NxtPageNumber);
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {     
            //system.debug('PrevPageNumber===222=========='+PrevPageNumber);     
            //system.debug('pagesize========222=========='+pagesize);       
            //system.debug('NxtPageNumber======222====='+NxtPageNumber);                
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
        	/*
        	lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>();
            lstbuyerPagination = new list<buyerWrapperlist>();
            buyerWList = new set<string>();
            mapBuyers = new map<id,string>();
	        mapBuyerSuppliers = new  map<string, list<BSureS_Assigned_Buyers__c>>();
            setBuyerIDs = new set<string>();
            //system.debug('lstBuyerIds========='+lstBuyerIds);
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            min = (newPageIndex-1) * pageSize;
            max = newPageIndex * pageSize;
            
            if(lstBuyerIds != NULL)
            {
                for(string b : lstBuyerIds)
                {
                    counter++;
                
                    if (counter > min && counter <= max) 
                    {
                        setBuyerIDs.add(b);// here adding files list
                    }
                }
            }
            //system.debug('setBuyerIDs========='+setBuyerIDs);
	        listAssignedNonCredit = [select id,Buyer_Id__c ,Buyer_Name__c, Supplier_ID__c,
	        						 Supplier_ID__r.Supplier_Name__c, Supplier_ID__r.Analyst_Name__c,
	        						 Supplier_ID__r.Globe_ID__c, Supplier_ID__r.Supplier_Category__c,
	        						 Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__r.City__c,Supplier_ID__r.Review_Status__c, 
	        						 Supplier_ID__r.Spend__c,Supplier_ID__r.Rating_Type__c,Supplier_ID__r.Rating__c,
	        						 Supplier_ID__r.State_Name__c from BSureS_Assigned_Buyers__c 
	        						 where Supplier_ID__c != null and Buyer_Id__r.IsActive= true 
	        						 and Buyer_Id__c IN : setBuyerIDs LIMIT 10000 ];
	        
	        if(listAssignedNonCredit != null && listAssignedNonCredit.size() > 0)
	        {						 
		        for(BSureS_Assigned_Buyers__c buyerObj:listAssignedNonCredit)
		        { 
		            buyerWList.add(buyerObj.Buyer_Id__c);
		            
		            if(mapBuyerSuppliers.containsKey(buyerObj.Buyer_Id__c))
		            {
		                list<BSureS_Assigned_Buyers__c> objTempIDs = mapBuyerSuppliers.get(buyerObj.Buyer_Id__c);
		                objTempIDs.add(buyerObj);
		                mapBuyerSuppliers.put(buyerObj.Buyer_Id__c, objTempIDs);
		            }
		            else
		            {
		                list<BSureS_Assigned_Buyers__c> objTempIDs = new list<BSureS_Assigned_Buyers__c>();
		                objTempIDs.add(buyerObj);
		                mapBuyerSuppliers.put(buyerObj.Buyer_Id__c, objTempIDs);
		            }
		            if(mapBuyers.containsKey(buyerObj.Supplier_ID__c))
		            {
		                string strBuyer = mapBuyers.get(buyerObj.Supplier_ID__c);
		                mapBuyers.remove(buyerObj.Supplier_ID__c);
		                strBuyer += buyerObj.Buyer_Name__c + ', ';
		                mapBuyers.put(buyerObj.Supplier_ID__c, strBuyer);
		            }
		            else
		            {
		                mapBuyers.put(buyerObj.Supplier_ID__c, buyerObj.Buyer_Name__c + ', ');
		            }
		        }
		        GetBuyersData();
	        }
			
			if(buyerNames != null && buyerNames.size() > 0)
			{
				for(buyerWrapperlist b : buyerNames)
                {
                	lstbuyerPagination.add(b);// here adding files list
                }	
			}
			pageNumber = newPageIndex;
            NlistSize=lstbuyerPagination.size();
              
      		PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize; 
            */
        
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber);
        bFromLastBtnClick = true;
        //LastpageData(totalpagenumber);
        
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
    
    public map<Id,BSureS_Credit_Analysis__c> QueryCreditStatus()
    {
        map<Id,BSureS_Credit_Analysis__c> CreditStatuses = new map<Id,BSureS_Credit_Analysis__c>(); 
	        for(BSureS_Credit_Analysis__c credSta : [Select id,name,Supplier_ID__c,Expected_Review_End_Date__c,Review_Status__c, Rating__c  from BSureS_Credit_Analysis__c where Review_Status__c != 'Completed' order by Createddate]){
	        	String supplierId = credSta.Supplier_ID__c;
	        	if(supplierId!=null){
	        		supplierId=supplierId.length()<15 ? supplierId.subString(0,15) : supplierId;
	        		//inserting into map
	        		CreditStatuses.put(supplierId,credSta);
	        	}
        }
        return CreditStatuses;
    }
}