global class bSureC_ScheduleExperianReport implements Schedulable {
   global void execute(SchedulableContext sc) {
   	  integer batchSize = integer.valueOf(BSureC_CommonUtil.getConfigurationValues('ExperianBatchSize').get(0));
      bSureC_GenerateExperianReport objExpRep = new bSureC_GenerateExperianReport();
      database.executebatch(objExpRep,batchSize);
   } 
}