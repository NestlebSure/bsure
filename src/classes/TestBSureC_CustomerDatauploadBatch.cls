/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomerDatauploadBatch {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        BSureC_PaymentHistorySectionStage.loadcutomerdata();
        BSureC_PaymentHistorySectionStage.loadbutton();
        
        BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        
         BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Key__c = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Value__c = 'Region Manager';
        insert objCSettings4;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'bsurec_buyerrole';
        objCSettings6.Parameter_Key__c = 'bsurec_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_ShowCustomers';
        objCSettings7.Parameter_Key__c = 'ShowAllCustomersForManagers';
        objCSettings7.Parameter_Value__c = 'TRUE';
        insert objCSettings7;
        
        BSureC_Customer_Basic_Info_Stage__c rec_cust_info_stage = new BSureC_Customer_Basic_Info_Stage__c();
        rec_cust_info_stage.Address_Line_2__c	 = '';
		rec_cust_info_stage.Analyst__c		 = 	Userinfo.getUserId();
		rec_cust_info_stage.Bill_to_Account__c	= '123456';
		rec_cust_info_stage.City__c	= 'AR';
		rec_cust_info_stage.Contact_Name__c	= 'Santa';
		rec_cust_info_stage.Country__c	= obj_country.Id;
		rec_cust_info_stage.Credit_Account__c	= '000912345';
		rec_cust_info_stage.Credit_Limit__c	= 12345.00;
		rec_cust_info_stage.Customer_Contact__c=	'Santa';
		rec_cust_info_stage.Customer_Category__c=	obj_category.Id;
		rec_cust_info_stage.Customer_Desc__c	= 'DESC';
		rec_cust_info_stage.Customer_Name__c= 'Santa-cc';
		rec_cust_info_stage.Customer_Region__c= obj_region.Id	;
		rec_cust_info_stage.DND_Financial_Information__c=	'Yes';
		rec_cust_info_stage.Fax__c	= '1234567';
		rec_cust_info_stage.GMC__c= 100.00;
		rec_cust_info_stage.Customer_Group_Id__c=	'00001234';
		rec_cust_info_stage.Last_Financial_Statement_Received__c = system.today();
		rec_cust_info_stage.Manager__c	= Userinfo.getUserId();
		rec_cust_info_stage.Next_Review_Date__c	= system.today();
		rec_cust_info_stage.Notification_Flag__c	= true;
		rec_cust_info_stage.Planned_Review_Date__c= system.today();
		rec_cust_info_stage.Review_Complete_Date__c=	system.today();
		if(rec_cust_info_stage.Planned_Review_Date__c != null)
		{
			rec_cust_info_stage.Review_Status__c='Scheduled';
		}
		rec_cust_info_stage.Risk_Category__c=	'003';
		rec_cust_info_stage.State__c	= obj_state.Id;
		rec_cust_info_stage.Street_Name_Address_Line_1__c=	'143';
		rec_cust_info_stage.Sub_Zone__c	= obj_Sub_zone.Id;
		rec_cust_info_stage.Zone__c= obj_zone.Id;
		rec_cust_info_stage.Exception__c = NULL;
		rec_cust_info_stage.Publish_Flag__c = false;
		insert rec_cust_info_stage;
		//BSureC_CreditDataSectionBatch b = new BSureC_CreditDataSectionBatch (); 
      //		Database.executebatch(b);
      		
      	BSureC_CustomerDatauploadBatch b = new BSureC_CustomerDatauploadBatch();
      	Database.executebatch(b);	
      		
    }
}