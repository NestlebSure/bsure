/************************************************************************************************       
Controller Name         : BsureC_PaymentHistorySection 
Date                    : 11/09/2012        
Author                  : Santhosh Palla       
Purpose                 : To Insert the Credit Data Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/09/2012      Santhosh.Palla            Initial Version
                          01/22/2013      VINU PRATHAP          Modified to show up the error message from Label
                          
**************************************************************************************************/
global with sharing class BsureC_PaymentHistorySection {


public String filename{get;set;}//For Uploaded file from page
public List<Selectoption> lst_division_names {get;set;}// Holds the Division Names to display in Visual force page 
public String str_division_name{get;set;}//holds the selectd division name
public boolean AlertUser{get;set;}

public Transient blob bfilecontent{get;set;} // Holds the Uploaded file content from page
/// <summary>  
 /// Constructor  
 /// </summary>
 public BsureC_PaymentHistorySection()
 {
    List<BSureC_Payment_History_Stage__c> existRec = [SELECT Id from BSureC_Payment_History_Stage__c where id!= null];
    if(existRec.size()>0)
    {
        AlertUser = true;
    }
    else
    {
        AlertUser=false;
    }
    lst_division_names = new List<Selectoption>();
    List<BSureC_Divisions__c> lst_division_info = new List<BSureC_Divisions__c>();//List for Divison names
    lst_division_info = [SELECT Id,Division_Name__c FROM BSureC_Divisions__c WHERE Is_Active__c =: true order by Division_Name__c ASC];
    
    for(BSureC_Divisions__c rec_division_name : lst_division_info)
    {
        lst_division_names.add(new SelectOption(rec_division_name.Id,rec_division_name.Division_Name__c));
    }
    
 }
 
    //Modified by VINU PRATHAP
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>    
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    } 
 
 
 /// <summary>  
 /// Pagereference 
 /// to save the Csv file details to Custom object 
 /// </summary>
 
 public pagereference SaveDetailsFile()  
 {
    //system.debug('----bfilecontent-----'+bfilecontent);
    //system.debug('-----filename----'+filename);
 /* List<BSureC_Credit_Data_Section_Attachement__c> lrecinsert = new List<BSureC_Credit_Data_Section_Attachement__c>();
    BSureC_Credit_Data_Section_Attachement__c recinsert = new BSureC_Credit_Data_Section_Attachement__c();
    recinsert.File_Name__c = filename;
    recinsert.IsActive__c = true;
    //objinsert.
    lrecinsert.add(recinsert);
    Database.Saveresult[] myresult = Database.insert(lrecinsert,false);
    
    //system.debug('----myresult----'+myresult);
     Id insertrecordId;
     for (Integer i = 0; i < myresult.size(); i++) 
     {
        insertrecordId = myresult[i].getId();
     }
     
    //  List<Attachment> attachment = new List<Attachment>();
      Attachment attachment = new Attachment();
      attachment.Name = filename;
      attachment.Body = bfilecontent;
      attachment.ParentId = insertrecordId;
      //attachment.add(recattachment);
      try
      {
      //    insert attachment;
      }
      catch(exception e)
      {
        //system.debug('---Exception----'+e);
      }
     // BsureC_CreditDataSectionBatch.parentrecid = insertrecordId;
    //  BsureC_CreditDataSectionScheduler obj = new BsureC_CreditDataSectionScheduler();    
    //  obj.execute(info, scope);  
    //BsureC_CreditDataSectionScheduler.parentrecid = insertrecordId;
    */
    //system.debug('------str_division_name--'+str_division_name);
    if(str_division_name == NULL || str_division_name == '' || str_division_name == 'Select')
    {
        //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Select Division Name');
        //ApexPages.addMessage(errormsg);
        showErrorMessage(system.label.BSureC_Please_select_division_name);
        return null;
    }
    else
    {
        if(bfilecontent!=NULL && filename.contains('.csv'))
        {
            pagereference pageref=new pagereference('/apex/BSureC_PaymentHistoryStageListView');
            //List<BSureC_Payment_History_Stage__c> lst_todelete= new List<BSureC_Payment_History_Stage__c>([SELECT Id from BSureC_Payment_History_Stage__c WHERE Customer_Division_Id__c =: str_division_name]);
            List<BSureC_Payment_History_Stage__c> lst_todelete= new List<BSureC_Payment_History_Stage__c>([SELECT Id from BSureC_Payment_History_Stage__c WHERE Id!= null]);
            if(BSureC_Payment_History_Stage__c.sObjectType.getDescribe().isDeletable())
            {
                Database.delete(lst_todelete);
            }    
            BsureC_CreditDataSectionScheduler.createBatchesFromCSVFile(bfilecontent,'BSureC_Payment_History_Stage__c',str_division_name);
            return pageref;
            //return null;
        }
        else
        {
            //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Upload a csv File');
            //ApexPages.addMessage(errormsg);
            showErrorMessage(system.label.BSureC_Please_upload_a_csv_file);
            return null;
        }
    }
  
 }
}