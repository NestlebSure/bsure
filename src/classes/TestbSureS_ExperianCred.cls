@isTest(seeAllData=true)
private class TestbSureS_ExperianCred {
    static testMethod void myUnitTest() {
        System.test.startTest();
        BSure_Configuration_Settings__c  config = new BSure_Configuration_Settings__c ();
        config.Name='test';
        config.Parameter_Key__c='ExperianCredentialsByPass';
        config.Parameter_Value__c='true';
        insert config;
        
        bSureS_ExpCred__c cred = new bSureS_ExpCred__c();
        cred.Pwd__c = 'Password';
        cred.UN__c = 'UserName';
        cred.Credit_Agency__c='Experian';
        insert cred;
        
        bSureS_ExperianCred obj = new bSureS_ExperianCred();
        obj.SelectedAgency='Experian';
        obj.processRequests();
        obj.objCred = cred;
        obj.CustomUpdate();
        
        System.test.stopTest();
    }
    
    static testMethod void myUnitTest1() {
        System.test.startTest();
        BSure_Configuration_Settings__c  config = new BSure_Configuration_Settings__c ();
        config.Name='test';
        config.Parameter_Key__c='ExperianCredentialsByPass';
        config.Parameter_Value__c='true';
        insert config;
        
        bSureS_ExpCred__c cred = new bSureS_ExpCred__c();
        cred.Pwd__c = 'Password';
        cred.UN__c = 'UserName';
        cred.Credit_Agency__c='Experian';
        bSureS_ExperianCred obj = new bSureS_ExperianCred();
        obj.SelectedAgency='Experian';
        //obj.processRequests();
        obj.objCred = cred;
        obj.CustomSave();
        
        System.test.stopTest();
    }
}