public with sharing class BSureS_ManagerApproveReviewList {
	
	public list<BSureS_Credit_Analysis__c> CreditAnalysisInfo{get;set;}
	public boolean vsbPopup{get;set;}
	public string strSupplierId{get;set;}
	public String StrSuppliername{get;set;}
	public String Strlocation{get;set;}
	public String selectrating{get;set;}
	public String selectrisklevel{get;set;}
	public String strSpend{get;set;}
	DateTime dateObj;
	public string InputHiddenIds{get;set;}
	public string vrating{get;set;}
	
	public string testcomments{get;set;}
	//public string strReviewStatus{get;set;}
	public BSureS_Credit_Analysis__c cObj{get;set;}
	map<Id,creditanalysiswrapper> mapwrpeer = new map<Id,creditanalysiswrapper>();
	public class creditanalysiswrapper
	{
		public Id caid{get;set;}
		public string supplierId{get;set;}
		public string suppliername{get;set;}
		public string city{get;set;}
		public string state{get;set;}
		public string reviewname{get;set;}
		public string risklevel{get;set;}
		public string risklavelImg{get;set;}
		public decimal spend{get;set;}
		public string rating{get;set;}
		public string ratingImg{get;set;}
		public String nextreviewdate{get;set;}
		public date spendstartdate{get;set;}
		public date spendenddate{get;set;}
		public string approvalcomments{get;set;}
	}
	public list<creditanalysiswrapper> lstWrapper{get;set;}
	public BSureS_ManagerApproveReviewList()
	{
		vsbPopup = false;
		GetReviewData();
	}	
	
	public void GetReviewData()
	{
		cObj = new BSureS_Credit_Analysis__c();
		CreditAnalysisInfo = new list<BSureS_Credit_Analysis__c>([select Id,Supplier_ID__c,Supplier_ID__r.Supplier_Name__c,Risk_Level__c,Spend__c,Rating__c,Next_Review_Date__c,Risk_Level_Type__c,Supplier_ID__r.State_Name__c,
							Spend_Start_Date__c,Spend_End_Date__c,Approve_Comments__c,Review_Name__c,Rating_Type__c,Supplier_ID__r.City__c,Review_Status__c from  BSureS_Credit_Analysis__c
							where Supplier_ID__c != null and Review_Status__c ='Pending Approval']);
		lstWrapper = new list<creditanalysiswrapper>();
		//strReviewStatus='Pending Approval';
		/*string strQuery = '';
		strQuery += 'Select Id,Supplier_ID__c,Supplier_ID__r.Supplier_Name__c,Risk_Level__c,Spend__c,Rating__c,Next_Review_Date__c,Risk_Level_Type__c,Supplier_ID__r.State_Name__c,  ';
		strQuery += 'Spend_Start_Date__c,Spend_End_Date__c,Approve_Comments__c,Review_Name__c,Rating_Type__c,Supplier_ID__r.City__c,Review_Status__c from  BSureS_Credit_Analysis__c ';
		strQuery += 'where Supplier_ID__c != null and Rating_Type__c != null and Review_Status__c = Pending Approval ';
		String strpcaquery = strQuery;
		*/
		//String strpcaquery = CreditAnalysisQuery(strReviewStatus);
		//NAGA Check if the user has read access on the BSureS_Credit_Analysis__c object
      	if(Schema.sObjectType.BSureS_Credit_Analysis__c.isAccessible())
	  	{
			//CreditAnalysisInfo = Database.query(strpcaquery);
			system.debug('CreditAnalysisInfo===='+CreditAnalysisInfo.size());
			if(CreditAnalysisInfo != null && CreditAnalysisInfo.size() > 0)
			{
				for(BSureS_Credit_Analysis__c caObj : CreditAnalysisInfo)
				{
					dateObj = caObj.Next_Review_Date__c;
					creditanalysiswrapper cawrp = new creditanalysiswrapper();
					cawrp.caid = caObj.Id;
					cawrp.supplierId = caObj.Supplier_ID__c;
					cawrp.suppliername = caObj.Supplier_ID__r.Supplier_Name__c;
					cawrp.city = caObj.Supplier_ID__r.City__c;
					cawrp.state = caObj.Supplier_ID__r.State_Name__c;
					cawrp.reviewname = caObj.Review_Name__c;
					cawrp.risklevel = caObj.Risk_Level__c;
					cawrp.risklavelImg = caObj.Risk_Level_Type__c;
					cawrp.spend = caObj.Spend__c;
					cawrp.rating = caObj.Rating__c;
					if(caObj.Rating_Type__c != null )
					{
						cawrp.ratingImg = caObj.Rating_Type__c;
					}	
					cawrp.nextreviewdate = dateObj.format('MM/dd/yyyy'); //caObj.Next_Review_Date__c;
					cawrp.spendstartdate = caObj.Spend_Start_Date__c;
					cawrp.spendenddate = caObj.Spend_End_Date__c;
					cawrp.approvalcomments = caObj.Approve_Comments__c;
					lstWrapper.add(cawrp);
					mapwrpeer.put(caObj.Id,cawrp);
				}
			}	
	  	}
	}
	public String CreditAnalysisQuery()
	{
		String strQuery = ''; 
		
		strQuery += 'Select Id,Supplier_ID__c,Supplier_ID__r.Supplier_Name__c,Risk_Level__c,Spend__c,Rating__c,Next_Review_Date__c,Risk_Level_Type__c,Supplier_ID__r.State_Name__c,  ';
		strQuery += 'Spend_Start_Date__c,Spend_End_Date__c,Approve_Comments__c,Review_Name__c,Rating_Type__c,Supplier_ID__r.City__c from  BSureS_Credit_Analysis__c ';
		strQuery += 'where Supplier_ID__c != null AND Review_Status__c = strtestreview ';
		
		system.debug('strQuery=========='+strQuery);
		return strQuery;
	}
	public PageReference Supplierlink()
	{
		string strSupplierId = apexpages.currentpage().getparameters().get('Supplierid');
		PageReference pageRef;
		if(strSupplierId != null && strSupplierId != '')
		{
			pageRef = new PageReference('/apex/Bsures_viewSupplierDetails?Id='+strSupplierId);
		}
		return pageRef;
	}
	public void Editreviewinfo()
	{
		vsbPopup = true;
		string strSupplierId2 = apexpages.currentpage().getparameters().get('Supplierid');
		String strcaid = apexpages.currentpage().getparameters().get('ReviewId');
		system.debug('strSupplierId2========'+strSupplierId2);
		/*if(strSupplierId != null && strSupplierId != '')
		{
			strSupplierId = strSupplierId2;
		}*/	
		if(strcaid != null && strcaid != '')
		{
			cObj = new  BSureS_Credit_Analysis__c();
			list<BSureS_Credit_Analysis__c> lstcredit = new list<BSureS_Credit_Analysis__c>([select Id,Risk_Level__c,Spend__c,Rating__c,Next_Review_Date__c,Risk_Level_Type__c, 
														Spend_Start_Date__c,Spend_End_Date__c,Approve_Comments__c,Review_Name__c,Rating_Type__c 
														from BSureS_Credit_Analysis__c where Id =: strcaid limit 1]);
			if(mapwrpeer.get(strcaid) != null)
			{
				StrSuppliername = mapwrpeer.get(strcaid).suppliername;
				Strlocation = mapwrpeer.get(strcaid).city + ', '+ mapwrpeer.get(strcaid).state;
			}
			if(lstcredit != null && lstcredit.size() > 0)
			{
				for(BSureS_Credit_Analysis__c c: lstcredit)
				{
					cObj = c;
				}
			}
		}
		
		//StrSuppliername
	}
	//popup Edit
	public pageReference saveSingleEdit()
	{
		PageReference p;
		if(cObj != null)
		{
			//update cObj;
			cObj.Review_Status__c = 'Completed';
			Database.Saveresult updaterec= Database.update(cObj);
			if(updaterec.isSuccess())
			{
				p = new PageReference('/apex/BSureS_ManagerApproveReviewList');
			}
			vsbPopup = false;
		}
		GetReviewData();
		return p;
	}
	public void cancel()
	{
		vsbPopup = false;
	}
	public pageReference approve()
	{
		system.debug('InputHiddenIds=-========'+InputHiddenIds);
		system.debug('lstWrapper==========='+lstWrapper);
		String strIds = InputHiddenIds.removeEnd(',');
		list<String> lstids;
		if(strIds != null && strIds != '')
		{
			lstids = new list<String>();
			lstids = strIds.split(',');
		}
		system.debug('strIds========='+strIds);
		map<id,string> mapcomment = new map<id,string>();
		if(lstWrapper != null && lstWrapper.size() > 0)
		{
			for(creditanalysiswrapper cw : lstWrapper)
			{
				if(cw.caid != null)
				{
					mapcomment.put(cw.caid, cw.approvalcomments);
				}	
			}
		}	
		if(mapcomment != null && mapcomment.size() > 0)
		{
			if(lstids != null && lstids.size() > 0 )
			{
				list<BSureS_Credit_Analysis__c> lstca = new list<BSureS_Credit_Analysis__c>([select Id,Review_Status__c from BSureS_Credit_Analysis__c where Id IN: lstids]);
				for(BSureS_Credit_Analysis__c cObj : lstca)
				{
					cObj.Review_Status__c = 'Completed';
					cObj.Approve_Comments__c = mapcomment.get(cObj.Id);
				}
				update lstca;
			}
		}
		GetReviewData();
		/*
		if(lstids != null && lstids.size() > 0 )
		{
			list<BSureS_Credit_Analysis__c> lstca = new list<BSureS_Credit_Analysis__c>([select Id,Review_Status__c from BSureS_Credit_Analysis__c where Id IN: lstids]);
			if(lstca != null && lstca.size() > 0)
			{
				for(BSureS_Credit_Analysis__c cObj : lstca)
				{
					cObj.Review_Status__c = 'Completed';
				}
				update lstca;
			}
		}	*/ 
		return null;							
	}
	public void showdate()
	{
		system.debug('vrating=-========'+vrating);
	}
	/*
	public List<SelectOption> getratinglist()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('A','A'));
        options.add(new SelectOption('B','B'));
        options.add(new SelectOption('C','C'));
        options.add(new SelectOption('D','D'));
        options.add(new SelectOption('F','F'));
        options.add(new SelectOption('TBD','TBD'));
        options.add(new SelectOption('Inactive','Inactive'));
		return options;
	}*/
	
}