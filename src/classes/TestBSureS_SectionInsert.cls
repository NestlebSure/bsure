/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_SectionInsert {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Supplier_name__c='KingFisher Supplier');
   		insert objSupplier;
   		
   		system.assertEquals('KingFisher Supplier',objSupplier.Supplier_name__c);
              
      	BSureS_Credit_Review_Section__c objCRs = new BSureS_Credit_Review_Section__c();
   		objCRs.Supplier_ID__c = objSupplier.Id;
   		insert objCRs;
   		 system.assertEquals(objSupplier.Id, objCRs.Supplier_ID__c);
   		 
   		BSureS_Credit_Review_Discussion__c objCRsDis = new BSureS_Credit_Review_Discussion__c();
    	objCRsDis.BSureS_Credit_Review_Section__c=objCRs.Id;
    	objCRsDis.Title__c='Dummy Title';
    	insert objCRsDis;
    	system.assertEquals('Dummy Title', objCRsDis.Title__c);
   		
   		BSureS_Credit_Status_Section__c objCss = new BSureS_Credit_Status_Section__c();
   		objCss.Supplier_ID__c = objSupplier.Id;
   		insert objCss;
   		system.assertEquals(objSupplier.Id, objCss.Supplier_ID__c);
   		
   		BSureS_Credit_Status_Discussion__c objCssDis = new BSureS_Credit_Status_Discussion__c();
    	objCssDis.BSureS_Credit_Status_Section__c=objCss.Id;
    	objCssDis.Title__c='Dummy Title';
    	insert objCssDis;
    	system.assertEquals('Dummy Title', objCssDis.Title__c);
   		
   		//BSureS_Confidential_Info_Section__c
   		BSureS_Confidential_Info_Section__c objCI = new BSureS_Confidential_Info_Section__c();
   		objCI.Supplier_ID__c = objSupplier.Id;
   		insert objCI;
   		system.assertEquals(objSupplier.Id, objCI.Supplier_ID__c);
   		
   		BSureS_Confidential_Discussion__c objCIDis = new BSureS_Confidential_Discussion__c();
    	objCIDis.BSureS_Confidential_Info_Section__c=objCI.Id;
    	objCIDis.Title__c='Dummy Title';
    	insert objCIDis;
    	system.assertEquals('Dummy Title', objCIDis.Title__c);
   		
   		
   		BSureS_Credit_Analysis__c objCA = new BSureS_Credit_Analysis__c();
   		objCA.Supplier_ID__c = objSupplier.Id;
   		insert objCA;
   		system.assertEquals(objSupplier.Id, objCRs.Supplier_ID__c);
   		
   		
   		BSureS_Credit_Analysis_Section__c objCAS = new BSureS_Credit_Analysis_Section__c();
   		objCAS.Supplier_ID__c = objSupplier.Id;
   		objCAS.BSureS_Credit_Analysis__c=objCA.Id;
   		insert objCAS;
   		
   		system.assertEquals(objSupplier.Id, objCRs.Supplier_ID__c);	
   		
   		BSureS_Credit_Analysis_Discussion__c objCASDis = new BSureS_Credit_Analysis_Discussion__c();
    	objCASDis.BSureS_Credit_Analysis_Section__c=objCAS.Id;
    	objCASDis.Title__c='Dummy Title';
    	insert objCASDis;
    	system.assertEquals('Dummy Title', objCASDis.Title__c);
   		
   		
   		
    }
}