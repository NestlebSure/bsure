global class bSureS_ScheduleExperianReport implements Schedulable {
   global void execute(SchedulableContext sc) {
   	  integer batchSize = integer.valueOf(BSureS_CommonUtil.getConfigurationValues('ExperianBatchSize').get(0));
      bSureS_GenerateExperianReport objExpRep = new bSureS_GenerateExperianReport();
      database.executebatch(objExpRep,batchSize);
   } 
}