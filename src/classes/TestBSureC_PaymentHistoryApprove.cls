/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_PaymentHistoryApprove {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Zone Manager'];
    	Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Manager'];
    	
    	User objUser = new User(Alias = 'bsurean', Email='cust_Zonemanager@bsurenestle.com', 
			        EmailEncodingKey='UTF-8', LastName='Zonemanager_CustUser', LanguageLocaleKey='en_US', 
			        LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
			        TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
        system.runAs(objUser)
        {
        	 
        	 BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
	         objbasicinfo.Customer_Group_Id__c = '2345';
	         objbasicinfo.Customer_Name__c = 'SAM';
	         objbasicinfo.Analyst__c = Userinfo.getUserId();
	         objbasicinfo.Manager__c = Userinfo.getUserId();
	         insert objbasicinfo;
	         String str_cust_id = String.valueOf(objbasicinfo.Id);
	         
	         system.assertEquals('2345', objbasicinfo.Customer_Group_Id__c);
	         
	         BSureC_Credit_Increase__c rec_credit_increase = new BSureC_Credit_Increase__c();
		     rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
		     rec_credit_increase.Review_Name__c = 'Sam1';
		     rec_credit_increase.Credit_Limit__c = 250001.00;
		     rec_credit_increase.Reapproval__c = false;
		     rec_credit_increase.Risk_Category__c = '002';
		     rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
		     rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
		     rec_credit_increase.New_Credit_Limit__c = 250001.00;
		     rec_credit_increase.New_Risk_Category__c = '003';
		     rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
		     rec_credit_increase.Status__c = 'Pending Approval';
		     rec_credit_increase.Next_Review_Date__c = system.today();
		     insert rec_credit_increase;
		     
		     system.assertEquals('Sam1', rec_credit_increase.Review_Name__c);
		     
	         
	         
	         BSureC_Credit_Increase__c basicList = [SELECT Id FROM BSureC_Credit_Increase__c Where Id =: rec_credit_increase.Id];
	         ApexPages.currentPage().getParameters().put('crId',rec_credit_increase.Id);
	         ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
	         BSureC_PaymentHistoryApprove obj = new BSureC_PaymentHistoryApprove(ssc);
	         
	         obj.getrisk_category_Options();
	         obj.edit12();
	         obj.Save();
	         obj.approve();
	         obj.bl_view_flag = true;
	         obj.cancel();
	         obj.bl_view_flag = false;
	         obj.cancel();
        }
         
    }
     static testMethod void myAnalystTest1()
     {
     	UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
    	Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Analyst'];
    	
    	User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
			        EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
			        LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
			        TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
        system.runAs(objUser)
        {
        	 BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
	         objbasicinfo.Customer_Group_Id__c = '2345';
	         objbasicinfo.Customer_Name__c = 'SAM';
	         objbasicinfo.Analyst__c = Userinfo.getUserId();
	         objbasicinfo.Manager__c = Userinfo.getUserId();
	         insert objbasicinfo;
	         String str_cust_id = String.valueOf(objbasicinfo.Id);
	         
	         system.assertEquals('2345', objbasicinfo.Customer_Group_Id__c);
	         
	         BSureC_Credit_Increase__c rec_credit_increase = new BSureC_Credit_Increase__c();
		     rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
		     rec_credit_increase.Review_Name__c = 'Sam1';
		     rec_credit_increase.Credit_Limit__c = 250001.00;
		     rec_credit_increase.Reapproval__c = false;
		     rec_credit_increase.Risk_Category__c = '002';
		     rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
		     rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
		     rec_credit_increase.New_Credit_Limit__c = 250001.00;
		     rec_credit_increase.New_Risk_Category__c = '003';
		     rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
		     rec_credit_increase.Status__c = 'Pending Approval';
		     rec_credit_increase.Next_Review_Date__c = system.today();
		     insert rec_credit_increase;
		     
		     system.assertEquals('Sam1', rec_credit_increase.Review_Name__c);
		     
	         ApexPages.currentPage().getParameters().put('crId',rec_credit_increase.Id);
	         
	         BSureC_Credit_Increase__c basicList = [SELECT Id FROM BSureC_Credit_Increase__c Where Id =: rec_credit_increase.Id];
	         ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
	         BSureC_PaymentHistoryApprove obj = new BSureC_PaymentHistoryApprove(ssc);
        }			        
     }
}