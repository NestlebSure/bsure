/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureS_Masstransfer {

    static testMethod void myUnitTest() {

        // TO DO: implement unit test
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureS_AnalystRole1';
        cSettings.Parameter_Key__c = 'BSureS_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureS_ZoneManager1';
        cSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureS_SubZoneManager1';
        cSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureS_CountryManager1';
        cSettings3.Parameter_Key__c = 'BSureS_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureS_BuyerShare1';
        cSettings4.Parameter_Key__c = 'BSureS_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureS_MassTransferAllowedProfiles1';
        cSettings5.Parameter_Key__c = 'BSureS_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureS_Analyst,BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureS_MassTransferNonCreditProfiles1';
        cSettings6.Parameter_Key__c = 'BSureS_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureS_MassTransferCreditProfiles1';
        cSettings7.Parameter_Key__c = 'BSureS_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureS_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        system.Test.startTest();
        BSureS_Masstransfer ownership=new BSureS_Masstransfer();
        BSureS_Masstransfer.Wrap_Cust_Infoupdate updatewrap=new BSureS_Masstransfer.Wrap_Cust_Infoupdate();
        BSureS_Masstransfer.Wrap_User_Info updatewrapinfo=new BSureS_Masstransfer.Wrap_User_Info();
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
            ownership.SelectUser();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.SelectUserDetails();
            ownership.submitForUserRole();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            BSureS_Basic_Info__c BCRef=new BSureS_Basic_Info__c (Analyst__c=userinfo.getuserid());
            insert BCRef;
            BSureS_Basic_Info__c BCbRef=new BSureS_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
            insert BCbRef;
            BSureS_Assigned_Buyers__c BCaRef=new BSureS_Assigned_Buyers__c (Buyer_ID__c=userinfo.getuserid(),Supplier_ID__c=BCbRef.id);
            insert BCaRef;
            System.Assertequals(userinfo.getuserid(),BCRef.Analyst__c);
            ownership.submit();
            ownership.Cancel();
            ownership.hidePopup();
            ownership.CreditCustomers();
            ownership.NonCreditCustomers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
            ownership.CustomerData();
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
            ownership.CustomerData();
            //ownership.SearchCatUsers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
            ownership.CustomerNonCreditData();
            string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
            ownership.FetchCustomerDetails();
            ownership.str_CustType='Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Back Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Non Credit';
            ownership.ChangeInfo();
            ownership.ChangeOwnership();
            ownership.AssignUser();
            ownership.hideUser();
            ownership.str_CustType='Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Back Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Non Credit';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.nextBtnClick();
            ownership.previousBtnClick();
            ownership.getPageNumber();
            ownership.getPageSize();
            ownership.getPreviousButtonEnabled();
            ownership.getNextButtonDisabled();
            ownership.totalPageNumber=0;
            ownership.totallistsize=1;
            ownership.getTotalPageNumber();
            ownership.BindData(1);
            ownership.pageData(1);
            ownership.pageL=1;
            ownership.LastpageData(1);
            ownership.LastbtnClick();
            ownership.FirstbtnClick();
            system.Test.stopTest();
    }
    
    static testMethod void myUnitTest2() {

        // TO DO: implement unit test
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureS_AnalystRole11';
        cSettings.Parameter_Key__c = 'BSureS_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureS_ZoneManager11';
        cSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureS_SubZoneManager11';
        cSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureS_CountryManager11';
        cSettings3.Parameter_Key__c = 'BSureS_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureS_BuyerShare11';
        cSettings4.Parameter_Key__c = 'BSureS_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureS_MassTransferAllowedProfiles11';
        cSettings5.Parameter_Key__c = 'BSureS_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureS_Analyst,BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureS_MassTransferNonCreditProfiles11';
        cSettings6.Parameter_Key__c = 'BSureS_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureS_MassTransferCreditProfiles11';
        cSettings7.Parameter_Key__c = 'BSureS_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureS_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
        system.Test.startTest();
        BSureS_Masstransfer ownership=new BSureS_Masstransfer();
        BSureS_Masstransfer.Wrap_Cust_Infoupdate updatewrap=new BSureS_Masstransfer.Wrap_Cust_Infoupdate();
        BSureS_Masstransfer.Wrap_User_Info updatewrapinfo=new BSureS_Masstransfer.Wrap_User_Info();
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
            ownership.SelectUser();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.SelectUserDetails();
            ownership.submitForUserRole();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            BSureS_Basic_Info__c BCRef=new BSureS_Basic_Info__c (Analyst__c=userinfo.getuserid());
            insert BCRef;
            BSureS_Basic_Info__c BCbRef=new BSureS_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
            insert BCbRef;
            BSureS_Assigned_Buyers__c BCaRef=new BSureS_Assigned_Buyers__c (Buyer_ID__c=userinfo.getuserid(),Supplier_ID__c=BCbRef.id);
            insert BCaRef;
            System.Assertequals(userinfo.getuserid(),BCRef.Analyst__c);
            ownership.submit();
            ownership.Cancel();
            ownership.hidePopup();
            ownership.CreditCustomers();
            ownership.NonCreditCustomers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
            ownership.CustomerData();
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
            ownership.CustomerData();
            //ownership.SearchCatUsers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
            ownership.CustomerNonCreditData();
            string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
            ownership.FetchCustomerDetails();
            ownership.str_CustType='Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Back Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Non Credit';
            ownership.ChangeInfo();
            ownership.ChangeOwnership();
            ownership.AssignUser();
            ownership.hideUser();
            ownership.str_CustType='Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Back Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Non Credit';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.nextBtnClick();
            ownership.previousBtnClick();
            ownership.getPageNumber();
            ownership.getPageSize();
            ownership.getPreviousButtonEnabled();
            ownership.getNextButtonDisabled();
            ownership.totalPageNumber=0;
            ownership.totallistsize=1;
            ownership.getTotalPageNumber();
            ownership.BindData(1);
            ownership.pageData(1);
            ownership.pageL=2;
            ownership.LastpageData(1);
            ownership.LastbtnClick();
            ownership.FirstbtnClick();
            system.Test.stopTest();
    }
    
     static testMethod void myUnitTest3() {

        // TO DO: implement unit test
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureS_AnalystRole2';
        cSettings.Parameter_Key__c = 'BSureS_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureS_ZoneManager2';
        cSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureS_SubZoneManager2';
        cSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureS_CountryManage2r';
        cSettings3.Parameter_Key__c = 'BSureS_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureS_BuyerShare2';
        cSettings4.Parameter_Key__c = 'BSureS_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureS_MassTransferAllowedProfiles2';
        cSettings5.Parameter_Key__c = 'BSureS_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureS_Analyst,BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureS_MassTransferNonCreditProfiles2';
        cSettings6.Parameter_Key__c = 'BSureS_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureS_Collector,BSureS_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureS_MassTransferCreditProfiles2';
        cSettings7.Parameter_Key__c = 'BSureS_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureS_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
        system.Test.startTest();
        BSureS_Masstransfer ownership=new BSureS_Masstransfer();
        BSureS_Masstransfer.Wrap_Cust_Infoupdate updatewrap=new BSureS_Masstransfer.Wrap_Cust_Infoupdate();
        BSureS_Masstransfer.Wrap_User_Info updatewrapinfo=new BSureS_Masstransfer.Wrap_User_Info();
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
            ownership.SelectUser();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.SelectUserDetails();
            ownership.submitForUserRole();
            ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            BSureS_Basic_Info__c BCRef=new BSureS_Basic_Info__c (Analyst__c=userinfo.getuserid());
            insert BCRef;
            BSureS_Basic_Info__c BCbRef=new BSureS_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
            insert BCbRef;
            BSureS_Assigned_Buyers__c BCaRef=new BSureS_Assigned_Buyers__c (Buyer_ID__c=userinfo.getuserid(),Supplier_ID__c=BCbRef.id);
            insert BCaRef;
            System.Assertequals(userinfo.getuserid(),BCRef.Analyst__c);
            ownership.submit();
            ownership.Cancel();
            ownership.hidePopup();
            ownership.CreditCustomers();
            ownership.NonCreditCustomers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
            ownership.CustomerData();
            ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
            ownership.CustomerData();
            //ownership.SearchCatUsers();
            ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
            ownership.CustomerNonCreditData();
            string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
            ownership.FetchCustomerDetails();
            ownership.str_CustType='Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Back Analyst';
            ownership.ChangeInfo();
            ownership.str_CustType='Non Credit';
            ownership.ChangeInfo();
            ownership.ChangeOwnership();
            ownership.AssignUser();
            ownership.hideUser();
            ownership.str_CustType='Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Back Analyst';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.str_CustType='Non Credit';
            ownership.str_AssignUser=userinfo.getuserid();
            ownership.ChangeCustomerOwnerShip();
            ownership.nextBtnClick();
            ownership.previousBtnClick();
            ownership.getPageNumber();
            ownership.getPageSize();
            ownership.getPreviousButtonEnabled();
            ownership.getNextButtonDisabled();
            ownership.totalPageNumber=0;
            ownership.totallistsize=1;
            ownership.getTotalPageNumber();
            ownership.BindData(1);
            ownership.pageData(1);
            ownership.pageL=3;
            ownership.LastpageData(1);
            ownership.LastbtnClick();
            ownership.FirstbtnClick();
	system.Test.stopTest();
    }
}