/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest (SeeAllData=false)
public with sharing class TestBSureS_SupplierStatusReport 
{
    static testMethod void myUnitTest()
    {
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'All';
        insert scategory;
        
        BSureS_Basic_Info__c SuppObj = new BSureS_Basic_Info__c();
        SuppObj.Contact_name__c = 'test name'; 
        SuppObj.Supplier_Name__c = 'test parent name';
        SuppObj.Supplier_Category__c = scategory.id;
        insert SuppObj;
        system.assertEquals('test name',SuppObj.Contact_name__c);
        
         BSureS_Credit_Analysis__c bsureCreditAnalysis = new BSureS_Credit_Analysis__c();
       // bsureCreditAnalysis.name = 'test namecr'; 
        bsureCreditAnalysis.Supplier_ID__c = SuppObj.id;
        bsureCreditAnalysis.Expected_Review_End_Date__c = system.today();
        bsureCreditAnalysis.Review_Status__c = 'test';
        bsureCreditAnalysis.Rating__c ='1';
        bsureCreditAnalysis.Spend__c =2;
        bsureCreditAnalysis.Risk_Level__c = '1';
        insert bsureCreditAnalysis;
        
        
        
        BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_Report_Page_Size1';
        objCSettingsB.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsB.Parameter_Value__c = '10';
        insert objCSettingsB;
        
        BSure_Configuration_Settings__c objCSettingsB1 = new BSure_Configuration_Settings__c();
        objCSettingsB1.Name = 'BSureS_BuyerShare';
        objCSettingsB1.Parameter_Key__c = 'BSureS_BuyerShare';
        objCSettingsB1.Parameter_Value__c = 'TRUE';
        insert objCSettingsB1;
        
        BSureS_Zone__c zone1 = new BSureS_Zone__c();
        zone1.name='First Zone';
        insert zone1;
        
        BSureS_SubZone__c subZone1 = new BSureS_SubZone__c();
        subZone1.name ='First subZone';
        subZone1.ZoneID__c = zone1.id;
        insert subZone1;
        
        BSureS_Country__c country1 = new BSureS_Country__c();
        country1.name ='First Zone';
        country1.Sub_Zone_ID__c =subZone1.id;
        insert country1;
        
        
        BSureS_Assigned_Buyers__c buyer= new BSureS_Assigned_Buyers__c();
        //buyer.name = 'BUY1';
        buyer.Buyer_Name__c = 'RAMA';
        buyer.Supplier_ID__c = SuppObj.Id;
        insert buyer;
        /*BSureS_User_Additional_Attributes__c userAddAtt=new BSureS_User_Additional_Attributes__c();
        userAddAtt.name='useraddatt';
        userAddAtt.BSureS_Zone__c = zone1.id;
        userAddAtt.BSureS_Sub_Zone__c = subZone1.id;
        userAddAtt.BSureS_Country__c = country1.id;
        userAddAtt.User__c = UserInfo.getUserId();
        insert userAddAtt;*/
        
        system.Test.startTest();
        BSureS_SupplierStatusReport sObj = new BSureS_SupplierStatusReport();
        BSureS_SupplierStatusReport.SupplierWrapper wrpObj = new BSureS_SupplierStatusReport.SupplierWrapper();
        sObj.getCategoryList();
        sObj.SupDetails();
        sObj.Buyerslist();
        sObj.Search();
        sObj.nextBtnClick() ;
        sObj.previousBtnClick();
        sObj.getPageNumber();
        sObj.getPageSize();
        sObj.getPreviousButtonEnabled();
        sObj.getNextButtonDisabled();
        sObj.getTotalPageNumber();
        sObj.LastbtnClick();
        sObj.FirstbtnClick();
        sObj.Cancel();
        sObj.showBuyerDetails();
        sObj.searchBtn();
        sObj.closepopupBtn();
        sObj.ExportCSV();
        sObj.closepopup();
        sObj.ViewData();
        sObj.SortData();
        sObj.getSortDirection();
        sObj.setSortDirection('ASC');
        sObj.fetchDetailsforExport();
        //BSureS_SupplierStatusReport.getBsureInfo(zone1.id,subZone1.id,country1.id,scategory.name,null);
        //BSureS_SupplierStatusReport.currency('2');
        system.Test.stopTest();
    }
    
    
}