/***********************************************************************************************
* Controller Name   : BSureS_SupplierList
* Date              : 11/02/2012
* Author            : ADITYA SRINIVAS V
* Purpose           : Class for Supplier List based on Logged in User
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 02/12/2013            ADITYA SRINIVAS V   Initial Version
* 04/14/2014(mm/dd/yy)  Vidya Sagar H       Added search functionality for Customer in BSureS_viewSupplierDetails.page
**************************************************************************************************/

global with sharing class BSureS_SupplierList 
{
    //public list<BSureS_Basic_Info__c> objSupplierList{get; set; }
    public ApexPages.StandardSetController objSupplierController{get; set;}
    
    public list<BSureS_Basic_Info__c> objSupplierList
        {
                get  
        {  
            if(objSupplierController != null)  
                return (List<BSureS_Basic_Info__c>)objSupplierController.getRecords();  
            else  
                return null ;  
        }
        set;
    }
    // Standard controller for BSureS_viewSupplierDetails.page
    // Added by Vidya Sagar
     public BSureS_SupplierList(ApexPages.StandardController controller) 
    {
    
    }
    public list<BSureS_Basic_Info__c> lstSupplierInfo{get; set; }
    public list<UserRole> objCurrentUser {get; set; }
    public list<BSureS_User_Additional_Attributes__c> objUserAttr {get; set; }
    
    public map<String, List<String>> mapConfigSettings = new map<String, List<String>>();
    
    //Added as on 07-06-13 - Search By Letter
    public List<string> searchChar{get;set;}  
    private string strQuery;    
    public string strSupplierName {get; set; }
    public string strSupplierSearch {get; set; }
    public string strAdvSuppName {get; set; }
    public string strLoggedInUser {get; set; }
    public string strBackupAnalyst {get; set; }
    public Decimal dblSpendFrom  {get; set; }
    public Decimal dblSpendTo {get; set; }
    public Date dtNRFromDate  {get; set; }
    public Date dtNRToDate  {get; set; }
    public Date dtLRFromDate {get; set; }
    public Date dtLRToDate  {get; set; }
    
    
    //Role Based Search
    public string strUserRole {get; set; }
    public string strRoleName {get; set; }
    public string strRoleBase {get; set; }
    public string strUserQuery {get; set; }
    public string strQueryParam {get; set; }
    public string strDelSupplID {get; set; }
    public string strAnalystChoice {get; set; }
    
    public Id strUserId {get; set; }
    public boolean showNewBtn {get; set; }
    public boolean showSupplList {get; set; }
    public boolean showAnalystChoice {get; set; }
    
    public list<SelectOption> lstAnalystChoice {get; set; }
    
    //Sort Variables
    public string sortDirection = 'ASC';            //Initial Sort Direction
    public string sortExp = 'Supplier_Name__c';     //Initial Sort Expression
    
    //Pagination Variables
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer prevPageNumber {get;set;}//used for pagination
    public Integer nextPageNumber {get;set;}//used for pagination
    public Integer intListSize {get;set;}//used for pagination
    public integer totalListSize{get;set;}//total size of list for pagination 
    
    //Advanced Search Variables
    public string strAdvSupplierName {get; set; }
    public string strSupplierCatg {get; set; }
    public string strSupplierZone {get; set; }      //To hold Supplier Zone
    public string strSupplierSubZone{get;set;}      //To hold Supplier Sub Zone
    public string strSupplierCountry{get;set;}      //To hold Supplier Country
    public string strSupplierState{get;set;}        //To hold Supplier State
    public string strTodayDate{get;set;}            //To hold Today's Date
    public string strNextRevFromDate {get; set; }   //To hold Next Review Date Range
    public string strNextRevToDate {get; set; }     //To hold Next Review Date Range
    public string strLastRevFromDate {get; set; }   //To hold Next Review Date Range
    public string strLastRevToDate {get; set; }     //To hold Next Review Date Range
    public string dSpendFrom {get; set; }
    public string dSpendTo {get; set; }
    public string strRating {get; set; }
    public string strReviewStatus {get; set; }
    
    public list<SelectOption> lstSCatg {get;set;}
    public list<SelectOption> lstSZones{get;set;}       //To hold Supplier Zone list
    public list<SelectOption> lstSSubZones{get;set;}    //To hold Supplier Sub Zone list
    public list<SelectOption> lstSCountries{get;set;}   //To hold Supplier Country list
    public list<SelectOption> lstSStates{get;set;}      //To hold Supplier State list
    public list<string> lstRatings {get; set; }
    public list<string> lstStatus {get; set; }
    
    public set<string> setRatings {get; set;}
    public set<string> setStatus {get; set;}
    public set<string> setSupIds {get;set;}
    
    public string strZoneMgrRole {get;set;}
    public string strSubZoneRole {get;set;}
    public string strCountryRole {get;set;}
    public string strBuyerRole  {get;set;}
    
    // Added by Vidya Sagar
    public boolean displayPopup {get; set;}  // used to show popup 
    public string strSearchSupp {get;set;}
    public string strPopMsg {get;set;}
    
     //More than 10000 records count
        public boolean Morethan10000;
        public integer RecCnt{get;set;}
        
    public BSureS_SupplierList()
    {
        strRoleName = '';
        showNewBtn = false;
        showSupplList = true;
        showAnalystChoice = false;
        strAnalystChoice = 'AllSupplier';
        strTodayDate = Date.today().format();
        
        lstSCatg = new list<SelectOption>();
        BSureS_CommonUtil.isReport = true;
        lstSCatg = BSureS_CommonUtil.getBSureSCategory();
        
        lstAnalystChoice = new list<SelectOption>();
        lstAnalystChoice.add(new SelectOption('AllSupplier', 'All Suppliers'));
        lstAnalystChoice.add(new SelectOption('MySupplier', 'My Suppliers'));
        
        
        strUserRole = Userinfo.getUserRoleId();
        strUserId = Userinfo.getUserId();
        objCurrentUser = [SELECT Id, Name 
                          FROM UserRole
                          WHERE Id =: strUserRole];
        if(objCurrentUser != null && objCurrentUser.size() > 0)
        {
            //system.debug('User Role Name :' + objCurrentUser.get(0).Name);
            strRoleName = objCurrentUser.get(0).Name;
        }
        objUserAttr = [SELECT User__r.Id, Name, BSureS_Zone__c, BSureS_Sub_Zone__c, BSureS_Country__c
                       FROM BSureS_User_Additional_Attributes__c 
                       WHERE User__r.Id =: strUserId];
                       
        mapConfigSettings = BSureS_CommonUtil.configSettingsMap;
        
        if(strRoleName != '' && strRoleName.equalsIgnoreCase('CEO'))
        {
            showNewBtn = true;
            //showAnalystChoice = true;
        }
        else if(strRoleName != '' && strRoleName.equalsIgnoreCase('analyst'))
        {
            showAnalystChoice = true;
        }
        
        SetQueryParams();
        //system.debug('User Role Name : ' + objUserAttr.get[0].User__r.Role);
        getSZones();
        
        getCustomSupplierList();  
        
        //Added as on 07-06-13
        searchChar=new string[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'All' };      
    }
    
    public void SetQueryParams()
    {
        strZoneMgrRole = '';
        strSubZoneRole = '';
        strCountryRole = '';
        string strRegionRole = '';
        string strAnalystRole = '';
        string strProcMgrRole = '';
        strBuyerRole = '';
        strUserQuery = '';
        strQueryParam = '';
        //system.debug('mapConfigSettings====='+mapConfigSettings.keyset());

        if(mapConfigSettings.get('bsures_zonemanagerrole') != null 
            && mapConfigSettings.get('bsures_zonemanagerrole').get(0) != null)
        {
            strZoneMgrRole = mapConfigSettings.get('bsures_zonemanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsures_subzonemanagerrole') != null
            && mapConfigSettings.get('bsures_subzonemanagerrole').get(0) != null)
        {
            strSubZoneRole = mapConfigSettings.get('bsures_subzonemanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsures_countrymanagerrole') != null
            && mapConfigSettings.get('bsures_countrymanagerrole').get(0) != null)
        {
            strCountryRole = mapConfigSettings.get('bsures_countrymanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsures_regionmanagerrole') != null
            && mapConfigSettings.get('bsures_regionmanagerrole').get(0) != null)
        {
            strRegionRole = mapConfigSettings.get('bsures_regionmanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsures_analystrole') != null
            && mapConfigSettings.get('bsures_analystrole').get(0) != null)
        {
            strAnalystRole = mapConfigSettings.get('bsures_analystrole').get(0);
        }
        if(mapConfigSettings.get('bsures_procurementmanagerrole') != null 
            && mapConfigSettings.get('bsures_procurementmanagerrole').get(0) != null)
        {
            strProcMgrRole = mapConfigSettings.get('bsures_procurementmanagerrole').get(0);
        }
        if(mapConfigSettings.get('bsures_buyerrole') != null 
            && mapConfigSettings.get('bsures_buyerrole').get(0) != null)
        {
            strBuyerRole = mapConfigSettings.get('bsures_buyerrole').get(0);
        }

        if(strRoleName != '' && strZoneMgrRole != '' && strRoleName.equalsIgnoreCase(strZoneMgrRole))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureS_Zone__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                        if(strAnalystChoice != null && strAnalystChoice.equalsIgnoreCase('mysupplier'))
                        strUserQuery = ' AND Zone__c =: strRoleBase';
                    strQueryParam = 'Zone__c##' + strRoleBase;
                }
                else
                {
                    showSupplList = false;
                }
            }
            else
            {
                showSupplList = false;
            }
        }
        
        else if(strRoleName != '' && strRoleName.equalsIgnoreCase(strSubZoneRole))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureS_Sub_Zone__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                        if(strAnalystChoice != null && strAnalystChoice.equalsIgnoreCase('mysupplier'))
                        strUserQuery = ' AND Sub_Zone__c =: strRoleBase';
                    strQueryParam = 'Sub_Zone__c##' + strRoleBase;
                }
                else
                {
                    showSupplList = false;
                }
            }
            else
            {
                showSupplList = false;
            }
        }
        else if((strRoleName != '' && strRoleName.equalsIgnoreCase(strCountryRole))
                || (strRoleName != '' && strRoleName.equalsIgnoreCase(strProcMgrRole)) 
                || (strRoleName != '' && strRoleName.equalsIgnoreCase(strRegionRole)))
        {
            if(objUserAttr != null && objUserAttr.size() > 0)
            {
                strRoleBase = objUserAttr.get(0).BSureS_Country__c;
                if(strRoleBase != null && strRoleBase != '')
                {
                        if(strAnalystChoice != null && strAnalystChoice.equalsIgnoreCase('mysupplier'))
                        strUserQuery = ' AND BSureS_Country__c =: strRoleBase';
                    strQueryParam = 'BSureS_Country__c##' + strRoleBase;
                }
                else
                {
                    showSupplList = false;
                }
            }
            else
            {
                showSupplList = false;
            }
        }
    }
    
    public void getCustomSupplierList()
    {
        string strSearchQuery = AdvSearchQueryString();
        objSupplierController = new ApexPages.StandardSetController(Database.getQueryLocator(strSearchQuery));  
        objSupplierController.setPageSize(25);  
        
        if(Morethan10000 == false){
                totalListSize = objSupplierController.getResultSize();
        }
        else{
                totalListSize=RecCnt;
        }
        pageSize = 25;// default page size will be 5
        
    }
    
    public Pagereference SearchByLetter()
    {           
        //string strCustomerSearchByLetter;     
        if(apexpages.currentpage().getparameters().get('alpha') == 'All') {
                strSupplierName = 'ALL';
        } else {
                strSupplierName = apexpages.currentpage().getparameters().get('alpha');
        }
                   
                string strSearchQuery = AdvSearchQueryString();     
        objSupplierController = new ApexPages.StandardSetController(Database.getQueryLocator(strSearchQuery));  
        objSupplierController.setPageSize(25); 
        if(Morethan10000 == false){
                totalListSize = objSupplierController.getResultSize();
        }
        else{
                totalListSize=RecCnt;
        }
        pageSize = 25;// default page size will be 5    
        return null;            
    }
    
    /*
    public string SearchQueryString()
    {
        string strShowSuppliers = BSureS_CommonUtil.getConfigurationValues('ShowAllSuppliersForManagers').get(0);
        string strQuery = '';
        
        strQuery += 'SELECT Id, Supplier_Name__c, Supplier_Category_Name__c, City__c, BSureS_Country__c, ';
        strQuery += 'State_Name__c, Spend__c, Country_Name__c, Review_Complete_Date__c, Sub_Zone__c, ';
        strQuery += 'Analyst_Name__c, Zone__c, Rating_Type__c, Rating__c, Analyst__c, Review_Status__c, ';
        strQuery += 'Supplier_Category__c, State_Province__c, Next_Review_Date__c ';
        strQuery += 'FROM BSureS_Basic_Info__c ';
        strQuery += 'WHERE Id != null ';
        if(strSupplierName != null && strSupplierName != '' && !strSupplierName.equalsIgnoreCase('Start typing your supplier name'))
        {
            strSupplierSearch = '%' + strSupplierName + '%';
            strQuery += 'AND Supplier_Name__c like : strSupplierSearch ';
        }
        if(showAnalystChoice && strAnalystChoice != null 
            && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mysupplier'))
        {
            strLoggedInUser = UserInfo.getUserId();
            strBackupAnalyst = '%' + strLoggedInUser + '%';
            strQuery += 'AND (Analyst__c =: strLoggedInUser ';
            strQuery += ' OR Bakup_Analysts__c like: strBackupAnalyst ) ';
        }
        if(strShowSuppliers != null && !strShowSuppliers.equalsIgnoreCase('TRUE') && strUserQuery != '')
        {
            strQuery += strUserQuery;
        }
        if(sortExpression != null && sortExpression != ''
            && sortDirection != null && sortDirection != '')
        {
            strQuery += ' order by ' + sortExpression + ' ' + sortDirection;
        }

        if(!showSupplList)
        {
            strQuery += ' LIMIT 0';
        }  
        else
        { 
            strQuery += ' LIMIT 5000';
        }
        system.debug('strQuery+++++111+++' + strQuery);
        
        return strQuery;
    }
    */
    public Pagereference CreateSupplier()
    {
        Pagereference pageRef = new Pagereference('/apex/BSureS_SupplierBasicInformation?retURL=/apex/BSureS_SupplierList');
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public Pagereference SortData()
    {
        getCustomSupplierList();
        return null;
    }
    
    public Pagereference getSearchResults()
    {
        getCustomSupplierList();
        
        if(!objSupplierList.isEmpty() && objSupplierList.size() == 1
            && objSupplierList[0].Supplier_Name__c == strSupplierName)
        {
            Pagereference pageRef = new Pagereference('/' + objSupplierList[0].Id);
            return pageRef;
        }
        return null;
    }
    
    public Pagereference DeleteSupplierInfo()
    {
        //system.debug('strDelSupplID++++' + strDelSupplID);
        if(strDelSupplID != null && strDelSupplID != '')
        {
            list<BSureS_Basic_Info__c> objSuppl = [SELECT Id FROM BSureS_Basic_Info__c
                                                   WHERE Id =: strDelSupplID];
            if(objSuppl != null && objSuppl.size() > 0)
            {
                delete objSuppl;
            }
            getCustomSupplierList();
        }
        return null;
    }
    
    //Adv Search Methods BEGIN
    
    public Pagereference getAdvSearchResult()
    {
        strSupplierName = '';
        string strAdvQuery = AdvSearchQueryString();
        objSupplierController = new ApexPages.StandardSetController(Database.getQueryLocator(strAdvQuery));  
        objSupplierController.setPageSize(25);  
        
        totalListSize = objSupplierController.getResultSize();  // this size for pagination
        pageSize = 25;          // default page size will be 5
        
        if(!objSupplierList.isEmpty() && objSupplierList.size() == 1
            && objSupplierList[0].Supplier_Name__c == strAdvSupplierName)
        {
            Pagereference pageRef = new Pagereference('/' + objSupplierList[0].Id);
            return pageRef;
        }
        return null;
    }
    
    public string AdvSearchQueryString()
    {
        lstRatings = new list<string>(); 
        lstStatus = new list<string>(); 
        setRatings = new set<string>();
        setStatus = new set<string>();
        
        if(strRating != null && strRating != '')
        {
            //strRating = strRating.substring(0, strRating.length() -1);
            lstRatings = strRating.split(',');
            setRatings.addAll(lstRatings);
            //system.debug('setRatings++++' + setRatings);        
        }
        
        if(strReviewStatus != null && strReviewStatus != '')
        {
            //strReviewStatus = strReviewStatus.substring(0, strReviewStatus.length() -1);
            lstStatus = strReviewStatus.split(',');
            setStatus.addAll(lstStatus);
        }
        
        string strShowSuppliers = BSureS_CommonUtil.getConfigurationValues('ShowAllSuppliersForManagers').get(0);
        string strQuery = '';
        strQuery += 'SELECT Id, Supplier_Name__c, Supplier_Category_Name__c, City__c, BSureS_Country__c, ';
        strQuery += 'State_Name__c, Spend__c, Country_Name__c, Review_Complete_Date__c, Sub_Zone__c, ';
        strQuery += 'Analyst_Name__c, Zone__c, Rating_Type__c, Rating__c, Analyst__c, Review_Status__c, ';
        strQuery += 'Supplier_Category__c, State_Province__c, Next_Review_Date__c ';
        strQuery += 'FROM BSureS_Basic_Info__c ';
        strQuery += 'WHERE Id != null ';
        if(strSupplierName != null && strSupplierName != '' 
                && !strSupplierName.equalsIgnoreCase('Start typing your supplier name')
                && !strSupplierName.equalsIgnoreCase('ALL'))
        {
            strSupplierSearch = strSupplierName + '%';
            strQuery += 'AND Supplier_Name__c like : strSupplierSearch ';
        }
        else if(strSupplierName != null && strSupplierName != '' 
            && strSupplierName.equalsIgnoreCase('ALL'))
        {
                strSupplierName = '';
                strAdvSupplierName = '';
                strSupplierSubZone = '';
                strSupplierCountry = '';
                strSupplierState = '';
                strSupplierCatg = '';
                strNextRevFromDate = '';
                    strNextRevToDate = '';
                    strLastRevFromDate = '';
                    strLastRevToDate = '';
                    dSpendFrom = '';
                    dSpendTo = '';    
        }
        else
        {
                if(strAdvSupplierName != null && strAdvSupplierName != '')
                {
                    strAdvSuppName = '%' + strAdvSupplierName + '%';  
                    //system.debug('strAdvSuppName ++++ ' + strAdvSuppName);
                    strQuery += 'AND Supplier_Name__c like : strAdvSuppName ';
                }
                if(strSupplierCatg != null && strSupplierCatg != ''
                    && !strSupplierCatg.equalsIgnoreCase('ALL'))
                {
                    strQuery += 'AND Supplier_Category__c =: strSupplierCatg ';
                }
                if(strSupplierZone != null && strSupplierZone != '' 
                    && !strSupplierZone.equalsIgnoreCase('ALL'))
                {
                    strQuery += 'AND Zone__c =: strSupplierZone ';
                    if(strSupplierSubZone != null && strSupplierSubZone != '' 
                        && !strSupplierSubZone.equalsIgnoreCase('ALL'))
                    {
                        strQuery += 'AND Sub_Zone__c =: strSupplierSubZone ';
                        if(strSupplierCountry != null && strSupplierCountry != '' 
                            && !strSupplierCountry.equalsIgnoreCase('ALL'))
                        {
                            strQuery += 'AND BSureS_Country__c =: strSupplierCountry ';
                            if(strSupplierState != null && strSupplierState != '' 
                                && !strSupplierState.equalsIgnoreCase('ALL'))
                            {
                                strQuery += 'AND State_Province__c =: strSupplierState ';
                            }
                        }
                    }
                }
                if(dSpendFrom != null && dSpendFrom != '')
                {
                    if(dSpendFrom.contains(','))
                    {
                        dSpendFrom = dSpendFrom.replace(',', '');
                    }
                    dblSpendFrom = Decimal.valueOf(dSpendFrom);
                    strQuery += 'AND Spend__c >=: dblSpendFrom ';
                }
                if(dSpendTo != null && dSpendTo != '')
                {
                    if(dSpendTo.contains(','))
                    {
                        dSpendTo = dSpendTo.replace(',', '');
                    }
                    dblSpendTo = Decimal.valueOf(dSpendTo);
                    strQuery += 'AND Spend__c <=: dblSpendTo ';
                }
                if(strNextRevFromDate != null && strNextRevFromDate != '')
                {
                    dtNRFromDate = Date.parse(strNextRevFromDate);
                    strQuery += 'AND Next_Review_Date__c >=: dtNRFromDate ';
                }
                if(strNextRevToDate != null && strNextRevToDate != '')
                {
                    dtNRToDate = Date.parse(strNextRevToDate);
                    strQuery += 'AND Next_Review_Date__c <=: dtNRToDate ';
                }
                if(strLastRevFromDate != null && strLastRevFromDate != '')
                {
                    dtLRFromDate = Date.parse(strLastRevFromDate);
                    strQuery += 'AND Review_Complete_Date__c >=: dtLRFromDate ';
                }
                if(strLastRevToDate != null && strLastRevToDate != '')
                {
                    dtLRToDate = Date.parse(strLastRevToDate);
                    strQuery += 'AND Review_Complete_Date__c <=: dtLRToDate ';
                }
                if(strRating != null && strRating != '')
                {
                    strQuery += 'AND Rating__c IN: setRatings ';
                }
                if(strReviewStatus != null && strReviewStatus != '')
                {
                    strQuery += 'AND Review_Status__c IN: setStatus ';
                }
        }
        if(showAnalystChoice && strAnalystChoice != null 
            && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mysupplier'))
        {
            strLoggedInUser = UserInfo.getUserId();
            strBackupAnalyst = '%' + strLoggedInUser + '%';
            strQuery += 'AND Analyst__c =: strLoggedInUser ';
            //strQuery += 'AND (Analyst__c =: strLoggedInUser ';
            //strQuery += ' OR Bakup_Analysts__c like: strBackupAnalyst ) ';
        }
        if(strAnalystChoice != null && strAnalystChoice != '' && strAnalystChoice.equalsIgnoreCase('mysupplier'))
        {    
                //system.debug('strRoleBase....'+strRoleBase);
            if(strRoleName != null && strRoleName.equalsIgnoreCase(strZoneMgrRole)){
                        strQuery += 'AND Zone__c =:strRoleBase';
            }else if(strRoleName != null && strRoleName.equalsIgnoreCase(strSubZoneRole)){
                        strQuery += 'AND Sub_Zone__c =:strRoleBase';
            }else if(strRoleName != null && strRoleName.equalsIgnoreCase(strCountryRole)){
                        strQuery += 'AND BSureS_Country__c =:strRoleBase';
            }else if(strRoleName != null && strRoleName.equalsIgnoreCase(strBuyerRole)){
                getBuyerSuppliers();
                        strQuery += 'AND Id In:setSupIds';
            }           
        }
        
        if(strShowSuppliers != null && !strShowSuppliers.equalsIgnoreCase('TRUE') && strUserQuery != '')
        {
            strQuery += strUserQuery;
        }
        if(sortExpression != null && sortExpression != ''
            && sortDirection != null && sortDirection != '')
        {
            strQuery += ' order by ' + sortExpression + ' ' + sortDirection;
        }
        Morethan10000 =false;
        list<BSureS_Basic_Info__c> basicLst = database.query(strQuery);
        if(basicLst!=null && basicLst.size() > 0){
                RecCnt = basicLst.size();
                if(RecCnt > 10000){
                        Morethan10000 =true;
                }
                else{
                        Morethan10000 =false;
                }
        }
        if(!showSupplList)
        {
            strQuery += ' LIMIT 0';
        }  
        else
        { 
            strQuery += ' LIMIT 10000';
        }
       
        //system.debug('strQuery@@@@@@@@@@@@@@@@' + strQuery);
        return strQuery;
    }
     /// <summary>
    /// to get Suppliers which are already assinged Buyers  
    /// </summary>
    
    public void getBuyerSuppliers(){
        setSupIds = new set<string>();
        list<BSureS_Assigned_Buyers__c> lstBuyers = new list<BSureS_Assigned_Buyers__c>();
        lstBuyers = [SELECT Id,Buyer_ID__c,Supplier_ID__c 
                                 FROM BSureS_Assigned_Buyers__c 
                                 WHERE Buyer_ID__c =:UserInfo.getUserId()];
                for(BSureS_Assigned_Buyers__c assB:lstBuyers){
                        setSupIds.add(assB.Supplier_ID__c);
                }
    }
    
    /// <summary>
    /// Method to get Zone list for Supplier search 
    /// </summary>
    public void getSZones()
    {
        BSureS_CommonUtil.isReport = true;
        lstSZones = new list<SelectOption>();
        lstSZones = BSureS_CommonUtil.getZones();
        lstSSubZones = new list<SelectOption>();
        lstSCountries = new list<SelectOption>();
        lstSStates = new List<SelectOption>();
    }
    
    /// <summary>
    /// Method to get Subzone list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getSubZoneList()
    {
        BSureS_CommonUtil.isReport = true;
        
        lstSSubZones = new list<SelectOption>();
        //system.debug('strSupplierZone+++++ ' + strSupplierZone);
        if(strSupplierZone != null && strSupplierZone != ''
            && !strSupplierZone.equalsIgnoreCase('all'))
        {
            //strSupplierSubZone = 'ALL';
            lstSSubZones = BSureS_CommonUtil.getSubZones(strSupplierZone);
        }
        else
        {
            strSupplierSubZone = 'ALL';
        }
        lstSCountries = new list<SelectOption>();
        lstSStates = new List<SelectOption>();
        return lstSSubZones;     
    }
    
    /// <summary>
    /// Method to get Country list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getCountriesList()
    {
        BSureS_CommonUtil.isReport = true;
        
        lstSCountries = new list<SelectOption>();
        //system.debug('strSupplierSubZone+++++ ' + strSupplierSubZone);
        if(strSupplierSubZone != null && strSupplierSubZone != ''
            && !strSupplierSubZone.equalsIgnoreCase('all'))
        {
            //strSupplierCountry = 'ALL';
            lstSCountries = BSureS_CommonUtil.getCountries(strSupplierSubZone);
        }
        else
        {
            strSupplierCountry = 'ALL';
        }
        lstSStates = new List<SelectOption>();
        
        return lstSCountries;     
    }
    
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    /// <returns>List<SelectOption></returns>
    public list<SelectOption> getStateList()
    {
        BSureS_CommonUtil.isReport = true;
        //system.debug('strSupplierCountry++++++' + strSupplierCountry);
        
        lstSStates = new List<SelectOption>();
        if(strSupplierCountry != null && strSupplierCountry != ''
            && !strSupplierCountry.equalsIgnoreCase('all'))
        {
            lstSStates = BSureS_CommonUtil.getStates(strSupplierCountry);
        }
        return lstSStates;     
    }
    
    public Pagereference ClearFields()
    {
        strAdvSupplierName = '';
        strSupplierSubZone = '';
        strSupplierCountry = '';
        strSupplierState = '';
        return null;
    }
    
    //Adv Search Methods END
    
    //SORT Methods BEGIN
    public string sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            if(value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
     
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }    
    //SORT Methods END
    
    //Pagination Methods BEGIN
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (totalListSize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totalListSize);
    }
    
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totalListSize!=0)
        {
            //system.debug(totalPageNumber+'-totalListSize--'+totalListSize);
            totalPageNumber = totalListSize / pageSize;
            Integer mod = totalListSize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
    public Boolean hasNext  
    {
        get  
        {
                return objSupplierController.getHasNext();  
        }  
        set;  
    }  
   
    public Boolean hasPrevious  
    {
        get  
        {  
            return objSupplierController.getHasPrevious();  
        }  
        set;  
    }  
   
    //Page number of the current displaying records  
    public Integer pageNumber  
    {  
        get  
        {  
            return objSupplierController.getPageNumber();  
        }  
        set;  
    }  
  
    //Returns the previous page of records  
    public void previous()  
    {  
        objSupplierController.previous();  
    }  
   
    //Returns the next page of records  
    public void next()  
    {  
        objSupplierController.next();  
    }  
    
    public void first()
    {
        objSupplierController.first();
    }
    
    public void last()
    {
        objSupplierController.last();
    }
    //Pagination Methods END
    
    /* @description : This method is used to Search Supplier 
     * @Page : This method is used in BSureS_viewSupplierDetails.page
     * @return : PageReference if Supplier exists, if not returns null
     */// Added by Vidya Sagar
    public Pagereference getSearchSupplier()
    {
        if(strSearchSupp !='' && strSearchSupp !=null){
        list<BSureS_Basic_Info__c> obj = [select id,Name from BSureS_Basic_Info__c where Supplier_Name__c like :strSearchSupp +'%' limit 1];
        if(obj != null && obj.size()>0){
            Pagereference pref = new Pagereference('/apex/BSureS_viewSupplierDetails?id='+obj[0].Id);
            pref.setRedirect(true);
            return pref;     
        }        
         if(strSearchSupp !='Start typing your Supplier Name')
         strPopMsg = strSearchSupp+ ' Supplier does not exists';
         else
         strPopMsg = 'Please enter Supplier name';        
        }
        else
           strPopMsg = 'Please enter Supplier name';
           displayPopup = true;
        return null;
    }
    
       
    /* @description : This method is used to close popup  
     * @Page : This method is used in BSureS_viewSupplierDetails.page
     */// Added by Vidya Sagar
    public void closePopup() {        
        displayPopup = false; 
    }
    
}