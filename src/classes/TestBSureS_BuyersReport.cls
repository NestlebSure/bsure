/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
public with sharing class TestBSureS_BuyersReport {

    static testMethod void myUnitTest()
    {
    	
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        //system.assertEquals('TESTAOA', szone.Name);
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true;
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        
        
        BSureS_Country__c sCountry = new BSureS_Country__c();
        sCountry.IsActive__c = true;
        sCountry.Name = 'test united States';
        sCountry.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountry; 
        
        
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8', 
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureS_User_Additional_Attributes__c objUseradd = new BSureS_User_Additional_Attributes__c();
        objUseradd.User__c = standard.id;
        objUseradd.BSureS_Zone__c = szone.id;
        objUseradd.BSureS_Sub_Zone__c = sSubZone.id;
        objUseradd.BSureS_Country__c = sCountry.id;
        insert objUseradd;
        
        BSureS_Basic_Info__c SupplObj=new BSureS_Basic_Info__c();
        SupplObj.Contact_name__c = 'Steve';
        SupplObj.Supplier_Name__c = 'george M';
        SupplObj.Has_Parent__c = true;
        SupplObj.IsPotential_Supplier__c = true;
        SupplObj.Zone__c = szone.id;
        SupplObj.Sub_Zone__c = sSubZone.id;
        SupplObj.BSureS_Country__c = sCountry.Id;
        SupplObj.Analyst__c = standard.id;
        SupplObj.Manager__c = standard.id;
        SupplObj.Backup_Analysts__c = standard.id;
        insert SupplObj; 
        system.assertEquals('Steve',SupplObj.Contact_name__c);
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'BSureS_BuyerShare1';
        objCSettings5.Parameter_Key__c = 'BSureS_BuyerShare';
        objCSettings5.Parameter_Value__c = 'TRUE';
        insert objCSettings5;
                
        BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_BuyerReport_PageSize1';
        objCSettingsB.Parameter_Key__c = 'BSureS_BuyerReport_PageSize';
        objCSettingsB.Parameter_Value__c = '5';
        insert objCSettingsB;
        
        BSureS_Assigned_Buyers__c bObj = new BSureS_Assigned_Buyers__c();
        bObj.Buyer_Id__c = standard.id;
        bObj.Supplier_ID__c = SupplObj.id;
        insert bObj;
        
        system.Test.startTest();   
        
        BSureS_BuyersReport buyerObj= new BSureS_BuyersReport();
        BSureS_BuyersReport.buyerWrapperlist objbean=new BSureS_BuyersReport.buyerWrapperlist();
        BSureS_BuyersReport.supplierWrapperlist objsuppbean = new BSureS_BuyersReport.supplierWrapperlist();
        buyerObj.getZones();
        buyerObj.getSubZones();
        buyerObj.getCountries();
        buyerObj.getExport();
        buyerObj.ExportExcel();
        buyerObj.Search();
        buyerObj.getBuyerslist();
        buyerObj.getSupplierDetails(standard.id);
        buyerObj.nextBtnClick();
        buyerObj.previousBtnClick();
        buyerObj.getPageNumber();
        buyerObj.getPageSize();
        buyerObj.getPreviousButtonEnabled();
        buyerObj.getNextButtonDisabled();
        buyerObj.getTotalPageNumber();
        buyerObj.LastbtnClick();
        buyerObj.FirstbtnClick();
        system.Test.stopTest();
    }
}