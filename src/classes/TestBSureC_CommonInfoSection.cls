/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureC_CommonInfoSection {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        /*
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Key__c = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        lstSettings.add(cSettings4);
         
        insert lstSettings;
        */
        
        BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        set<String> setCategory = new set<String>();
        setCategory.add(obj_category.Name);
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c();
        objCustomer.Customer_Name__c='KingFisher Customer';
        objCustomer.Country__c = obj_country.id;
        objCustomer.Customer_Category__c = obj_category.id;
        objCustomer.Customer_Region__c = obj_region.id;
        objCustomer.State__c = obj_state.id;
        objCustomer.Sub_Zone__c = obj_Sub_zone.id;
        objCustomer.Zone__c = obj_zone.id;
        insert objCustomer;
        
        system.assertEquals('KingFisher Customer', objCustomer.Customer_Name__c);
        
        BSureC_Document_Types__c objDocType = new BSureC_Document_Types__c(Document_Type_Name__c = 'test Presentations',Code__c = '12345');
        insert objDocType;
        
        BSureC_Unearned_Cash_Discount_Section__c objUCDS = new BSureC_Unearned_Cash_Discount_Section__c();
        objUCDS.Customer_ID__c = objCustomer.Id;
        objUCDS.Comment__c = 'Test&test';
        objUCDS.Discussion_Type__c = 'Link';
        insert objUCDS;
       
        apexpages.currentpage().getParameters().put('custId',objCustomer.Id);
        apexpages.currentpage().getParameters().put('Type','Topic');       
        BSureC_CommonInfoSection objCommonInfo = new BSureC_CommonInfoSection();
        objCommonInfo.strSelectedBtn = 'Topic';
        objCommonInfo.strObject = 'BSureC_Unearned_Cash_Discount_Section__c';
        objCommonInfo.fileName = 'TestFile';
        objCommonInfo.strComments = 'Topic<>Comments';
        objCommonInfo.strDoctypeid = objDocType.Id;
        objCommonInfo.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo.strTitle ='Presentations On Education';
        objCommonInfo.SaveDetails();
        objCommonInfo.GetIDsForCrIncrease(objCustomer);
        objCommonInfo.GetIDsForInvoiceColl(objCustomer);
        objCommonInfo.GetIDsForOnGoing(objCustomer);
        objCommonInfo.GetIDsForUnAuthDed(objCustomer);
        objCommonInfo.GetIDsForUnearnedCashDisc(objCustomer);
        
        pagereference pgref =objCommonInfo.showErrorMessage('test');
       
        apexpages.currentpage().getParameters().put('type','File');
        BSureC_CommonInfoSection objCommonInfo1 = new BSureC_CommonInfoSection(); 
        objCommonInfo1.strSelectedBtn = 'File';
        objCommonInfo1.strObject = 'BSureC_Unearned_Cash_Discount_Section__c';
        objCommonInfo1.fileName = 'TestFile';
        objCommonInfo1.strComments = 'Topic &Comments';
        objCommonInfo1.strDoctypeid = objDocType.Id;       
        objCommonInfo1.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo1.strTitle ='Presentations On Education';
        objCommonInfo1.fsize = 5242881;
        objCommonInfo1.SaveDetails();  
        pagereference pgref1 =objCommonInfo1.showErrorMessage('test');     
        
        apexpages.currentpage().getParameters().put('Id',objUCDS.Id);
        apexpages.currentpage().getParameters().put('type','Link');
        BSureC_CommonInfoSection objCommonInfo2 = new BSureC_CommonInfoSection(); 
        objCommonInfo2.strSelectedBtn = 'Link';
        objCommonInfo2.customerId = objCustomer.id;
        objCommonInfo2.strObject = 'BSureC_Unearned_Cash_Discount_Section__c';
        objCommonInfo2.fileName = 'TestFile'; 
        objCommonInfo2.strComments = 'Topic&<> Comments';
        objCommonInfo2.strDoctypeid = objDocType.Id;
        objCommonInfo2.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo2.strTitle ='Presentations On Education';
        objCommonInfo2.SaveDetails();
        objCommonInfo2.getCustomLabel();
        pagereference pgref2 =objCommonInfo2.showErrorMessage('test');
    }
    
     static testMethod void myUnitTest1() {
        /*
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
         
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Key__c = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        lstSettings.add(cSettings4); 
         
        insert lstSettings;
        */
        Blob fileBody = blob.valueof('Test Blob Attachment file');
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(Customer_Name__c='KingFisher Customer');
        insert objCustomer;
       
        BSureC_Document_Types__c objDocType = new BSureC_Document_Types__c(Document_Type_Name__c = 'test Presentations2',Code__c = '123456');
        insert objDocType;
        system.assertEquals('KingFisher Customer', objCustomer.Customer_Name__c);
        
        /***BSureC_Unearned_Cash_Discount_Section__c***/
        BSureC_Unearned_Cash_Discount_Section__c objUCDS = new BSureC_Unearned_Cash_Discount_Section__c();
        objUCDS.Customer_ID__c = objCustomer.Id;
        objUCDS.Discussion_Type__c = 'File';
        objUCDS.Document_Type__c = objDocType.id;
        insert objUCDS;
        
        Attachment objAttach1 = new Attachment();
        objAttach1.ParentId = objUCDS.Id;
        objAttach1.Body = fileBody;
        objAttach1.Name = 'Unearned_Cash_Discount_Section';
        insert objAttach1;
        
        /***BSureC_Unauthorized_Deduction_Section__c***/
        BSureC_Unauthorized_Deduction_Section__c objUDS = new BSureC_Unauthorized_Deduction_Section__c();
        objUDS.Customer_ID__c = objCustomer.Id;
        objUDS.Discussion_Type__c = 'File';
        objUDS.Document_Type__c = objDocType.id;
        insert objUDS;
        
        Attachment objAttach2 = new Attachment();
        objAttach2.ParentId = objUDS.Id;
        objAttach2.Body = fileBody;
        objAttach2.Name = 'Unauthorized_Deduction_Section';
        insert objAttach2;
        
        /***BSureC_On_Going_Credit_Section__c***/
        BSureC_On_Going_Credit_Section__c objOGCS = new BSureC_On_Going_Credit_Section__c();
        objOGCS.Customer_ID__c = objCustomer.Id;
        objOGCS.Discussion_Type__c = 'File';
        objOGCS.Document_Type__c = objDocType.id;
        insert objOGCS;
        
        Attachment objAttach3 = new Attachment();
        objAttach3.ParentId = objOGCS.Id;
        objAttach3.Body = fileBody;
        objAttach3.Name = 'On_Going_Credit_Section';
        insert objAttach3;
        
        /***BSureC_Invoice_Collection_Section__c***/
        BSureC_Invoice_Collection_Section__c objICS = new BSureC_Invoice_Collection_Section__c();
        objICS.Customer_ID__c = objCustomer.Id;
        objICS.Discussion_Type__c = 'File';
        objICS.Document_Type__c = objDocType.id;
        insert objICS;
        
        Attachment objAttach4 = new Attachment();
        objAttach4.ParentId = objICS.Id;
        objAttach4.Body = fileBody;
        objAttach4.Name = 'Invoice_Collection_Section';
        insert objAttach4;
        
        /***BSureC_Credit_Increase_Section__c***/
        BSureC_Credit_Increase_Section__c objCIS = new BSureC_Credit_Increase_Section__c();
        objCIS.Customer_ID__c = objCustomer.Id;
        objCIS.Discussion_Type__c = 'File';
        objCIS.Document_Type__c = objDocType.id;
        insert objCIS;
        
        Attachment objAttach5 = new Attachment();
        objAttach5.ParentId = objCIS.Id;
        objAttach5.Body = fileBody;
        objAttach5.Name = 'Credit_Increase_Section';
        insert objAttach5;
        
        system.assertEquals(objCustomer.Id, objUCDS.Customer_ID__c);
        
        apexpages.currentpage().getParameters().put('custId',objCustomer.Id);
        apexpages.currentpage().getParameters().put('type','Link');
        
        BSureC_CommonInfoSection objCommonInfo1 = new BSureC_CommonInfoSection(); 
        objCommonInfo1.m_fileBody = blob.valueof('test topic file body');
        objCommonInfo1.m_fileName = 'file';
        apexpages.currentpage().getParameters().put('Id',objUCDS.Id);
        objCommonInfo1.strObject = 'BSureC_Unearned_Cash_Discount_Section__c';
        objCommonInfo1.strCustomerName = 'KingFisher Customer';
        //objCommonInfo1.strRightDocTypes[0] = objDocType.id;
        objCommonInfo1.strSelectedDocs='A,B';
        objCommonInfo1.SearchResults();
        set<String> coverdoc = new set<String>();
        coverdoc.add(objOGCS.id);
        objCommonInfo1.setDocs = coverdoc;
        objCommonInfo1.getAttachments();
        objCommonInfo1.pageSize=10;
        pagereference pgrefnxtbtn =objCommonInfo1.nextBtnClick();
        pagereference pgrefprvbtn =objCommonInfo1.previousBtnClick();
        integer i=objCommonInfo1.getTotalPageNumber();
        integer j=1;
        objCommonInfo1.BindData(j);
        objCommonInfo1.pageData(j);
        objCommonInfo1.LastpageData(j);
        integer k=objCommonInfo1.getPageNumber();
        integer l=objCommonInfo1.getPageSize();
        boolean a=objCommonInfo1.getPreviousButtonEnabled();
        boolean b=objCommonInfo1.getNextButtonDisabled();
        pagereference pgreflastbtn =objCommonInfo1.LastbtnClick();
        pagereference pgreffrstbtn =objCommonInfo1.FirstbtnClick();
        pagereference pgref3 =objCommonInfo1.showErrorMessage('test');
      }
      static testmethod void myunittest2()
      {
      	 BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        set<String> setCategory = new set<String>();
        setCategory.add(obj_category.Name);
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c();
        objCustomer.Customer_Name__c='KingFisher Customer';
        objCustomer.Country__c = obj_country.id;
        objCustomer.Customer_Category__c = obj_category.id;
        objCustomer.Customer_Region__c = obj_region.id;
        objCustomer.State__c = obj_state.id;
        objCustomer.Sub_Zone__c = obj_Sub_zone.id;
        objCustomer.Zone__c = obj_zone.id;
        objCustomer.Email_address__c = 'test@gmail.com';
        insert objCustomer;
        
        BSureC_On_Going_Credit_Section__c objconfDoct = new BSureC_On_Going_Credit_Section__c();
        objconfDoct.Comment__c = 'Test Comment';
        objconfDoct.Customer_ID__c = objCustomer.id;
        objconfDoct.TitleDescription__c = 'Test title description';
        objconfDoct.Source__c = 'test source';
        objconfDoct.Discussion_Type__c = 'Topic';
        insert objconfDoct;
        Attachment att = new Attachment();
        att.ParentId = objconfDoct.id;
        att.Body = blob.valueof('test blob');
        att.Name = 'topic';
        insert att;
        ApexPages.currentPage().getParameters().put('id',objconfDoct.id);
        apexpages.currentpage().getParameters().put('custId',objCustomer.Id);
        apexpages.currentpage().getParameters().put('type','Topic');
        BSureC_CommonInfoSection testcls = new BSureC_CommonInfoSection();
        testcls.topic_filebody = blob.valueof('test topic file body');
        testcls.topic_fileName = 'topic';
        testcls.strSelectedBtn = 'Topic';
        testcls.strObject = 'BSureC_On_Going_Credit_Section__c';
        testcls.strrecordid = objconfDoct.id;
        testcls.getEditObjectInfo();
        testcls.SaveDetails();
        testcls.strObject = 'BSureC_Unauthorized_Deduction_Section__c';
        testcls.SaveDetails();
        testcls.strObject = 'BSureC_Invoice_Collection_Section__c';
        testcls.SaveDetails();
        testcls.strObject = 'BSureC_Credit_Increase_Section__c';
        testcls.GetIDsForCrIncrease(objCustomer);
        testcls.SaveDetails();
      }
      static testmethod void myunittest3()
      {
      	 BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        set<String> setCategory = new set<String>();
        setCategory.add(obj_category.Name);
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c();
        objCustomer.Customer_Name__c='KingFisher Customer';
        objCustomer.Country__c = obj_country.id;
        objCustomer.Customer_Category__c = obj_category.id;
        objCustomer.Customer_Region__c = obj_region.id;
        objCustomer.State__c = obj_state.id;
        objCustomer.Sub_Zone__c = obj_Sub_zone.id;
        objCustomer.Zone__c = obj_zone.id;
        objCustomer.Email_address__c = 'test@gmail.com';
        insert objCustomer;
        apexpages.currentpage().getParameters().put('custId',objCustomer.Id);
        apexpages.currentpage().getParameters().put('type','Topic');
        BSureC_CommonInfoSection testcls = new BSureC_CommonInfoSection();
        testcls.topic_filebody = blob.valueof('test topic file body');
        testcls.topic_fileName = 'topic';
        testcls.strSelectedBtn = 'Topic';
        testcls.strTitle = 'topic test';
        testcls.strComments = 'test comments < test > comment';
        testcls.strObject = 'BSureC_On_Going_Credit_Section__c';
        testcls.getEditObjectInfo();
        testcls.SaveDetails();
      }
      static testmethod void myunittest4()
      {
      	 BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        set<String> setCategory = new set<String>();
        setCategory.add(obj_category.Name);
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c();
        objCustomer.Customer_Name__c='KingFisher Customer';
        objCustomer.Country__c = obj_country.id;
        objCustomer.Customer_Category__c = obj_category.id;
        objCustomer.Customer_Region__c = obj_region.id;
        objCustomer.State__c = obj_state.id;
        objCustomer.Sub_Zone__c = obj_Sub_zone.id;
        objCustomer.Zone__c = obj_zone.id;
        objCustomer.Email_address__c = 'test@gmail.com';
        insert objCustomer;
        apexpages.currentpage().getParameters().put('custId',objCustomer.Id);
        apexpages.currentpage().getParameters().put('type','Topic');
        BSureC_CommonInfoSection testcls = new BSureC_CommonInfoSection();
        testcls.topic_filebody = blob.valueof('test topic file body');
        testcls.topic_fileName = 'topic';
        testcls.strSelectedBtn = 'Topic';
        testcls.strTitle = '';
        testcls.strComments = '';
        testcls.strObject = 'BSureC_On_Going_Credit_Section__c';
        testcls.getEditObjectInfo();
        testcls.SaveDetails();
        apexpages.currentpage().getParameters().put('type','File');
        testcls.strSelectedBtn = 'File';
        testcls.getEditObjectInfo();
        testcls.SaveDetails();
      }
    
}