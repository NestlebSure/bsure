/***********************************************************************************************
*       Controller Name : BSureS_SpendByCategory
*       Date            : 11/02/2012 
*       Author          : B.Anupreethi
*       Purpose         : Its the report class where user can see the user can see to total spen amount based on category
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       12/26/2012                B.Anupreethi              Initial Version
*       01/19/2013                VINU PRATHAP              Modified to show up messages from Labels
*       03/04/2013                B.Anupreethi              //Anu:changed the review dates Review_Start_Date__c to Actual_Review_Start_Date__c 
                                                             and Review_End_Date__c to Expected_Review_End_Date__c
*       03/11/2013                B.Anupreethi               //Anu:changed the status to Approved to Completed as DBA loaded as completed
 		08/12/2013                veereandranath.j           Code Review Changes
**************************************************************************************************/
global with sharing class BSureS_SpendByCategory 
{
    public string strSupplierName{get;set;}//To hold Supplier name
    public string strCategory{get;set;} //To hold the category name
    public Id LoggedInUserId{get;set;}//To hold logged in user id
    public string strSupplierZone{get;set;}//To Hold supplier Zone
    public string strSupplierSubZone{get;set;}//To Hold supplier Sub Zone
    public string strSupplierCountry{get;set;}//To hold the sup state
    public list<SelectOption> lstSZone{get;set;}//to hold Zone list
    public list<SelectOption> lstSSubZone{get;set;}//to hold Sub Zone list
    public list<SelectOption> lstSCountry{get;set;}//to hold country list   
    public list<SelectOption> lstSCategory{get;set;}//To hold list of categories
    public BSureS_Basic_Info__c objSupplier {get;set;}//To hold supplier obj data
    public list<resultwrapper> lstResults {get;set;}//To hold results list
    public string strFromDate{get;set;}//To Hold Search from data
    public string strToDate{get;set;}//To Hold Search To data
    public string strTodayDate{get;set;}
    public map<string, list<String>> mapSupDetails{get;set;}
    public list<BSureS_Credit_Analysis__c> lstSupDetails{get;set;}
    
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public list<BSureS_Credit_Analysis__c> lstSupDetailsPagination{get;set;}//to hold list supplier info
    public string strCompleted ='Completed'; 
    
    /// <summary>
    /// wrapper class 
    /// </summary>  
    public class resultwrapper
    {
        public string strSupId{get;set;}
        public String strCatName{get;set;}
        public Double strTotalSpend{get;set;}
        
    }
    
     //sort
    private string  sortExp = 'Supplier_Category_Name__c';
    private string  sortDirection = 'ASC';  
    
     public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() {  
        getResults();
    }
    
    public pagereference Cancel(){
        return page.BSureS_Reports;
    } 
    
    /// <summary>
    /// Constructor 
    /// </summary>
    public BSureS_SpendByCategory()
    {
        BSureS_CommonUtil.isReport=true;
        objSupplier = new BSureS_Basic_Info__c();   
            
        lstSCategory = BSureS_CommonUtil.getBSureSCategory();
        //lstSCategory.sort();
        //strCategory = '';
        getUserAttributes();
        getSZone(); 
        
        Date dtYear = Date.Today().addDays(-365);      
        Date dtToday = Date.Today();    
        strTodayDate = Date.today().format();
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
    }
    
    /// <summary>
    /// Method is used to Bind the default values of logged in users
    /// </summary>
    public void getUserAttributes()
    {
        LoggedInUserId=UserInfo.getUserId();
        list<BSureS_User_Additional_Attributes__c> lstAttributes;
        if(Schema.Sobjecttype.BSureS_User_Additional_Attributes__c.isQueryable())
        lstAttributes = [SELECT User__c, BSureS_Country__c, BSureS_Sub_Zone__c, BSureS_Zone__c 
                        FROM BSureS_User_Additional_Attributes__c 
                        WHERE User__r.IsActive=: true AND User__c=:LoggedInUserId];
        if(lstAttributes.size()>0)
        {
            strSupplierZone = lstAttributes[0].BSureS_Zone__c;
            strSupplierSubZone = lstAttributes[0].BSureS_Sub_Zone__c;
            strSupplierCountry = lstAttributes[0].BSureS_Country__c;
        }
    }
    
     /// <summary>
    /// Method to get Zone list for Supplier search 
    /// </summary>
    public void getSZone()
    {
        BSureS_CommonUtil.isReport=true;
        lstSZone=new list<SelectOption>();          
        lstSZone = BSureS_CommonUtil.getZones();
        getSSubZoneList();  
    }
    
    /// <summary>
    /// Method to get Subzone list for Supplier search 
    /// </summary>
    public void getSSubZoneList()
    {
        
        
        BSureS_CommonUtil.isReport=true;
        lstSSubZone=new list<SelectOption>();
        if(strSupplierZone=='ALL')
        {
            strSupplierSubZone='ALL';
            strSupplierCountry='ALL';
        }       
        lstSCountry = new list<selectOption>();
        lstSCountry.add(new selectOption('ALL','ALL'));
        lstSSubZone = BSureS_CommonUtil.getSubZones(strSupplierZone);    
        
        getSCountriesList();
    }
    
    /// <summary>
    /// Method to get Country list for Supplier search 
    /// </summary>
    public void getSCountriesList()
    {
        BSureS_CommonUtil.isReport=true;
        lstSCountry=new list<SelectOption>();
        lstSCountry = BSureS_CommonUtil.getCountries(strSupplierSubZone);
    }
    /// Updated by VINU PRATHAP to Show error message from Labels
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
    
    public Pagereference ExportToExcel(){
        Pagereference pageref = new Pagereference('/apex/BSureS_SpendByCategoryExport');    
        return pageref;
    }
    
    public void getResults()
    {   
        lstSupDetails = new list<BSureS_Credit_Analysis__c>();
        lstSupDetails.clear();
        lstSupDetailsPagination = new list<BSureS_Credit_Analysis__c>();
        lstSupDetailsPagination.clear();
        if(strFromDate!='' && strToDate!='')
        {
            lstResults = new list<resultwrapper>();
            list<BSureS_Basic_Info__c> lstSup=new list<BSureS_Basic_Info__c>();
            string strQuery =   'SELECT Id,Name,Supplier_Name__c,Spend__c, State_Province__c,Supplier_Category_Name__c,Globe_ID__c, '+
                                'Sub_Zone__c,Zone__c,BSureS_Country__c, '+
                                'Sub_Zone__r.Name,Zone__r.Name,BSureS_Country__r.Name '+                                 
                                'from BSureS_Basic_Info__c where Supplier_Category_Name__c != null ';  
            
            if(strSupplierName != null && strSupplierName!='')
                strQuery+=' AND Supplier_Name__c =: strSupplierName ';
            if(strCategory != null && strCategory != 'ALL')
                strQuery+=' AND Supplier_Category__c =: strCategory ';
            if(strSupplierZone != null && strSupplierZone != 'ALL')
                strQuery +=' AND Zone__c =: strSupplierZone ';
            if(strSupplierSubZone != null && strSupplierSubZone != 'ALL')
                strQuery +=' AND Sub_Zone__c =: strSupplierSubZone ';
            if(strSupplierCountry != null && strSupplierCountry != 'ALL')
                strQuery +=' AND BSureS_Country__c =: strSupplierCountry ';
            
            
            strQuery +='order BY Supplier_Category__c limit 30000';
            if(Schema.Sobjecttype.BSureS_Basic_Info__c.isQueryable())
            lstSup=database.query(strQuery);  
            
            
            set<id> setids=new set<id>();
            for(BSureS_Basic_Info__c sup:lstSup)  
            {
                setids.add(sup.Id);
            }
            mapSupDetails = new map<string, list<string>>();
            list<string> supobj;
            map<id, Double> mapSpends = new map<id,Double>();
            map<string, double> mapCatSpend = new map<string, double>();
            double dSpendAmt = 0.0;
            mapSpends= getTotalSpend(setids);
            if(mapSpends.size()>0)
            { 
                for(BSureS_Basic_Info__c sup:lstSup)  
                {
                    if(mapCatSpend.containsKey(sup.Supplier_Category_Name__c))
                    {
                      
                        dSpendAmt = mapCatSpend.get(sup.Supplier_Category_Name__c);
                        dSpendAmt += double.valueOf((mapSpends.get(sup.Id) != null?mapSpends.get(sup.Id):0));
                        if(dSpendAmt != double.valueOf(0.0))
                        {
                            mapCatSpend.put(sup.Supplier_Category_Name__c, dSpendAmt);
                        }   
                        
                    }
                    else
                    {
                        dSpendAmt = 0;
                        dSpendAmt += double.valueOf((mapSpends.get(sup.Id) != null?mapSpends.get(sup.Id):0));
                        if(dSpendAmt != double.valueOf(0.0))
                        {
                            mapCatSpend.put(sup.Supplier_Category_Name__c, dSpendAmt);
                        }   
                        
                    }
                    
                    if(mapSupDetails.containsKey(sup.Supplier_Category_Name__c))
                    {
                        supobj = mapSupDetails.get(sup.Supplier_Category_Name__c);
                        supobj.add(sup.Id); 
                        mapSupDetails.put(sup.Supplier_Category_Name__c,supobj);
                    }
                    else
                    {
                        supobj= new list<string>();                     
                        supobj.add(sup.Id); 
                        mapSupDetails.put(sup.Supplier_Category_Name__c,supobj);
                    }
                    
                    
                }
                list<string> setCatNames= new list<string>(mapCatSpend.keySet());
                setCatNames.sort();
                for(string strCatName : setCatNames)
                {
                    resultwrapper newobj=new resultwrapper();
                        
                    newobj.strTotalSpend =mapCatSpend.get(strCatName);      
                    newobj.strCatName=strCatName; 
                    lstResults.add(newobj);
                }
            }       
        }
        else
        {   
            // Changed by VINU PRATHAP to show up the message from Label Message
            showErrorMessage(system.Label.BSureS_Please_select_date_range);
        }
    }
    
    public map<id, Double> getTotalSpend(set<id> supIDs)
    {
        //dtFromDate = Date.valueOf(dtFromDate);
        //dtToDate = Date.valueOf(dtToDate);
        Date dtFrom = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        Date dtTo= ((strToDate != null && strToDate != '')?date.parse(strToDate):null);         
        
        //system.debug('dtFrom--'+dtFrom+'dtTo--'+dtTo);
        map<id,Double> mapTotalSpends=new map<id,Double>();
        Double dTotalSpendamt;
         list<BSureS_Credit_Analysis__c> lstSpends;
        //Anu:changed the review dates Review_Start_Date__c to Actual_Review_Start_Date__c and Review_End_Date__c to Expected_Review_End_Date__c
        if(Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())
        		lstSpends=[SELECT Id,Spend__c ,Supplier_ID__c, Review_Start_Date__c, Review_End_Date__c, Actual_Review_Start_Date__c, Expected_Review_End_Date__c
                                       FROM BSureS_Credit_Analysis__c
                                       WHERE Supplier_ID__c in: supIDs AND Actual_Review_Start_Date__c >=: dtFrom  AND Expected_Review_End_Date__c <=: dtTo
                                       AND Review_Status__c=:strCompleted];//Anu:changed the status to Approved to Completed as DBA loaded as completed
        if(lstSpends.size()>0)
        {
            for(BSureS_Credit_Analysis__c objspend: lstSpends)
            {
                if(mapTotalSpends.get(objspend.Supplier_ID__c) == null)
                {
                    mapTotalSpends.put(objspend.Supplier_ID__c,Integer.valueof(objspend.Spend__c));
                }
                else if(mapTotalSpends.get(objspend.Supplier_ID__c) != null)
                {
                    Double dub_spent_amount = Double.valueOf(mapTotalSpends.get(objspend.Supplier_ID__c));
                    dTotalSpendamt= dub_spent_amount + Double.valueof(objspend.Spend__c);
                    mapTotalSpends.put(objspend.Supplier_ID__c,dTotalSpendamt);
                }   
            }
        }
        return mapTotalSpends;
    }
    
    public void getSupDetails()
    {
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
        string strSelectedCat = apexpages.currentpage().getparameters().get('SupSpend');
        lstSupDetails = new list<BSureS_Credit_Analysis__c>();
        lstSupDetailsPagination = new list<BSureS_Credit_Analysis__c>();
        Date dtFrom = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        Date dtTo= ((strToDate != null && strToDate != '')?date.parse(strToDate):null); 
        if(strSelectedCat!='' && strSelectedCat!=null)
        {
            if(mapSupDetails!=null)
            {
                //Anu:changed the status to Approved to Completed as DBA loaded as completed
                if(Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())
                lstSupDetails = [SELECT Id, Spend__c ,Supplier_ID__c,  Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__r.Supplier_Name__c,
                                        Supplier_ID__r.Globe_ID__c, Review_Start_Date__c, Review_End_Date__c, Review_Status__c, 
                                        Risk_Level__c, Rating__c, Rating_Type__c
                                FROM BSureS_Credit_Analysis__c
                                WHERE Supplier_ID__c in: mapSupDetails.get(strSelectedCat)  AND Supplier_ID__r.Supplier_Category_Name__c=: strSelectedCat
                                        AND Actual_Review_Start_Date__c >=: dtFrom  AND Expected_Review_End_Date__c <=: dtTo AND Review_Status__c=:strCompleted
                                order by Supplier_ID__r.Supplier_Name__c];
                
            }
        }
        totallistsize = lstSupDetails.size();// this size for pagination
        pageSize = 10;// default page size will be 10
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstSupDetailsPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstSupDetailsPagination.size();
            PrevPageNumber=1;
        }
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
     /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        try
        {
            lstSupDetailsPagination=new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }

            if(lstSupDetails!=NULL)
            {
                for(BSureS_Credit_Analysis__c b : lstSupDetails)
                {
                    counter++;
                    
                        if (counter > min && counter <= max) 
                        {
                            lstSupDetailsPagination.add(b);// here adding files list
                        }
                }
            }
            
            pageNumber = newPageIndex;
            NlistSize=lstSupDetailsPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstSupDetailsPagination=new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            for(BSureS_Credit_Analysis__c a : lstSupDetails)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstSupDetailsPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstSupDetailsPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
}