/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomerList {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        system.assertequals('Country Manager',objCSettings3.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Key__c = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Value__c = 'Region Manager';
        insert objCSettings4;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'bsurec_buyerrole';
        objCSettings6.Parameter_Key__c = 'bsurec_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
        system.assertEquals('bsurec_buyerrole', objCSettings6.Name);
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_ShowCustomers';
        objCSettings7.Parameter_Key__c = 'ShowAllCustomersForManagers';
        objCSettings7.Parameter_Value__c = 'TRUE';
        insert objCSettings7;
        
        BSureC_CustomerList objCList = new BSureC_CustomerList();
        objCList.strAnalystChoice = 'mycustomer';
        objCList.strCustomerName = 'Test Customer';
        objCList.strRiskLevel = 'abc,def,';
        objCList.strReviewStatus = 'pqr,xyz,';
        objCList.strDelCustID = 'yyyyyyyyyyyy';
        objCList.strAdvCustomerName = 'Test Adv Customer';
        objCList.strCustomerCatg = 'cccccccccc';
        objCList.strCustomerZone = 'zzzzzzzzzzz';
        objCList.strCustomerSubZone = 'ssssssssssss';
        objCList.strCustomerCountry = 'cccccccccc';
        objCList.strCustomerState = 'sssssssss';
        objCList.dSpendFrom = '23,456';
        objCList.dSpendTo = '34,567';
        objCList.strNextRevFromDate = system.today().format();
        objCList.strNextRevToDate = system.today().format();
        objCList.strLastRevFromDate = system.today().format();
        objCList.strLastRevToDate = system.today().format();
        
        objCList.CreateCustomer();
        //objCList.mapConfigSettings = BSureC_CommonUtil.configSettingsMap;
        
        objCList.SetQueryParams();
        objCList.getCustomCustomerList();
        objCList.getSearchResults();
        objCList.getAdvSearchResult();
        objCList.getCZones();
        objCList.getSubZoneList();
        objCList.getCountriesList();
        objCList.getStateList();
        objCList.totalPageNumber = 1;
        objCList.getNextButtonDisabled();
        objCList.getPreviousButtonEnabled();
        objCList.setSortDirection('ASC');
        objCList.getSortDirection();
        objCList.getTotalPageNumber();
        objCList.DeleteCustomerInfo();
        objCList.SortData();
        string strTestAdv = objCList.AdvSearchQueryString();
        //string strTest = objCList.SearchQueryString(); 
        
        BSureC_Customer_Basic_Info__c objCust = new BSureC_Customer_Basic_Info__c(Customer_Name__c='Search Customer');
        insert objCust;
        objCList.strSearchCust = objCust.Customer_Name__c;
        objCList.getSearchCustomer();
        objCList.closePopup();
        objCList.strSearchCust = 'Test search';
        objCList.getSearchCustomer();
        objCList.closePopup();
        objCList.strSearchCust = '';
        objCList.getSearchCustomer();
        objCList.closePopup();
    }
    
    static testMethod void myAnalystTest()
    {
    	
        
    	UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
    	Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Analyst'];
    	
    	User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
			        EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
			        LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
			        TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
			        
		
		system.runAs(objUser)
		{
			BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
	        objCSettings7.Name = 'BSureC_ShowCustomers';
	        objCSettings7.Parameter_Key__c = 'ShowAllCustomersForManagers';
	        objCSettings7.Parameter_Value__c = 'FALSE';
	        insert objCSettings7;
	        system.assertEquals('BSureC_ShowCustomers', objCSettings7.Name);
	        
			BSureC_CustomerList objCList = new BSureC_CustomerList();
        	objCList.strAnalystChoice = 'mycustomer';
        	
        	objCList.SetQueryParams();
	        objCList.getCustomCustomerList();
	        objCList.getSearchResults();
        	objCList.DeleteCustomerInfo();
        	objCList.SortData();
		}
    }
}