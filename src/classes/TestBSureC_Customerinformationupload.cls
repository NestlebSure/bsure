/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_Customerinformationupload {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureC_Region__c obj_region = new BSureC_Region__c();
        obj_region.Name = 'Central-c';
        insert obj_region;
        system.assertEquals('Central-c', obj_region.Name);
        
        BSureC_Category__c obj_category = new BSureC_Category__c();
        obj_category.Category_Name__c = 'APDO';
        obj_category.Name = 'APDO-c';
        insert obj_category;
        
        BSureC_Zone__c obj_zone = new BSureC_Zone__c();
        obj_zone.Name = 'America-c';
        obj_zone.IsActive__c = true;
        insert obj_zone;
        
        BSureC_Sub_Zone__c obj_Sub_zone = new BSureC_Sub_Zone__c();
        obj_Sub_zone.Name = 'North America-c';
        obj_Sub_zone.ZoneID__c = obj_zone.Id;
        obj_Sub_zone.IsActive__c = true;
        insert obj_Sub_zone;
        
        BSureC_Country__c obj_country = new BSureC_Country__c();
        obj_country.Name = 'United States of America-c';
        obj_country.Sub_Zone_ID__c = obj_Sub_zone.Id;
        obj_country.IsActive__c = true;
        insert obj_country;
        
        BSureC_State__c obj_state = new BSureC_State__c();
        obj_state.Name = 'Ohio-c';
        obj_state.Customer_Country__c = obj_country.Id;
        obj_state.IsActive__c = true;
        insert obj_state;
        
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings.Parameter_Value__c = 'BSureC_Analyst';
        insert cSettings;
        
        BSureC_Customerinformationupload obj = new BSureC_Customerinformationupload();
        
        obj.strZoneId = obj_zone.Id;
        obj.getSubZones();
        obj.strSubZoneId = obj_Sub_zone.Id;
        obj.getCountries();
        obj.strCountryId = obj_country.Id;
        obj.getStates();
        obj.strCategoryId = obj_category.Id;
        obj.strRegionId = obj_region.Id;
        obj.getAnalysts();
        
        obj.bfilecontent = Blob.valueOf('283607,Customer1,19738,283607,,,Santhosh,12/20/2013,Yes,No,,,10,Yes, , , , , , , , , ,123,abc,'+'\n'+'283607,Customer1,19738,283607,,,Santhosh,12/20/2013,Yes,No,,,10,Yes, , , , , , , , , ,123,abc,'); 
	    
	    obj.filename = 'sam1.csv';	
	    obj.SaveDetailsFile();
	    					   
    }
}