/************************************************************************************************       
Controller Name         : BSureC_ReviewWorklistForMangerAndAnalyst       
Date                    : 12/07/2012        
Author                  : Neha Jaiswal       
Purpose                 : To generate the reports for upcoming reviews for analyst and manager       
Change History          : Date           Programmer                     Reason       
-----------------------------   -------------------   --------------------------------------------      
                          11/07/2012            Neha Jaiswal          Initial Version
                          01/25/2013            Vinu Prathap          Modified to avoid SOQL injection
                          11/04/2014(mm/dd/yy)  Vidya Sagar H         Added Collector filter for reports
                          19/12/2014			Satish.c			  Worked on pagination and viewstate errors
**************************************************************************************************/
global with sharing class BSureC_ReviewWorklistForMangerAndAnalyst {
     public Id userId{get;set;}//to hold userId
     public User UserDetail{get;set;}//to get user details
     public string userName {get;set;}//to get user first name and last name
     public list<BSureC_Customer_Basic_Info__c> lstResultsPage {get;set;}//To hold results list
     public transient list<BSureC_Customer_Basic_Info__c> lstCustomerBasicInfo{get;set;}   //list of user with basic info
     public list<CommonUserClass> objWorkList{get;set;}//list of wrapper class 
     public list<CommonUserClass> finalobjWorkList{get;set;}
     
     public list<selectOption> lstSubordinateNames{get;set;}//to pass the list of subordinates
     public string  strSubordinateNames{get;set;}//to get id of subordinate selected
     public list<BSureC_Credit_Data_Publish__c> listPublish{get;set;}//to get the details of customer from data publish object
     public list<BSureC_Payment_History_Publish__c> listPaymentHistory{get;set;}// to get the user details from BSureC_Payment_History_Publish__c object
     public Boolean AnalystOptionsBlock{get;set;}//to rerender the analyst options
     public Boolean worklistBlock{get;set;} //to display the list of upcoming schedules.
     public integer counter=0;  //keeps track of the offset
     public integer list_size{get;set;} //sets the page size or number of rows
     public integer total_size{get;set;} //used to show user the total size of the list
     public string strMainQuery {get;set;}
     public string  sortDirection = 'ASC';  
     public string  sortExp = 'Next_Review_Date__c';
     public Boolean isexport{get;set;}
     public Boolean displayPanel{get;set;}
     public Boolean exportTrue{get;set;}
     //for pagination
     public Integer pageNumber;//used for pagination
     public Integer pageSize;//used for pagination
     public Integer totalPageNumber;//used for pagination
     public Integer PrevPageNumber {get;set;}//used for pagination
     public Integer NxtPageNumber {get;set;}//used for pagination
     public Integer NlistSize {get;set;}//used for pagination
     public Integer Endlst {get;set;}//used for pagination
     public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
     public List<BSureC_Customer_Basic_Info__c> Pagenationlist {get;set;}
     public string loginUserProfile {get;set;}
     
     // Starting....... Vidya Sagar
     public string  strCollectorNames{get;set;}//to get id of  selected collector
     public String[] strCollectorProfiles=new String[] {'BSureC_Collector'};//Profiles to be allowed
     public String[] strCollectorRole=new String[] {'Collector'};//Roles to be allowed
     
     public list<selectOption> getlstCollectorNames()
     {
        list<selectOption> options = new list<selectOption>();
        options.add(new selectoption('--Select--','--Select--'));
        list<User> lstCollectorUser=[Select Id, Name from User where Profile.Name IN:strCollectorProfiles and IsActive=true and UserRole.Name IN:strCollectorRole order By Name Asc];
        if(lstCollectorUser !=null && lstCollectorUser.size()>0)
        {
            for(User u:lstCollectorUser){
                options.add(new selectoption(u.Name,u.Name));           
            }
        }
        return options;
        
     }
     // Ending...... Vidya Sagar
    /// <summary>
    /// constructor: to display the dropdown list and upcoming schedules.
    /// </summary>
    /// <returns> upcoming schedules
     public BSureC_ReviewWorklistForMangerAndAnalyst()
     {
            exportTrue=true;
            lstCustomerBasicInfo=new list<BSureC_Customer_Basic_Info__c>();
            lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
            displayPanel=false;
            list_size = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));
            userName = String.valueOf(UserInfo.getUserId());
            string usrName=UserInfo.getFirstName()+'  '+UserInfo.getLastName(); 
            List<BSureC_Customer_Basic_Info__c> lstOptionsAnalyst = new List<BSureC_Customer_Basic_Info__c>();
            lstSubordinateNames = new list<selectOption>();
            AnalystOptionsBlock=true;
            Profile objCurProfile = [SELECT name FROM Profile WHERE id=:Userinfo.getProfileId()];
            loginUserProfile = objCurProfile.name;
            /*if(loginUserProfile.equalsIgnoreCase('System Administrator')){
            //Modified By VINU PRATHAP  
            //lstOptionsAnalyst = [SELECT Id,Analyst__c,Analyst_Name__c FROM BSureC_Customer_Basic_Info__c  order by Analyst_Name__c ASC];
            lstOptionsAnalyst = [SELECT Id,Analyst__c,Analyst_Name__c FROM BSureC_Customer_Basic_Info__c WHERE Analyst_Name__c != null order by Analyst_Name__c ASC];
            }else{
             lstOptionsAnalyst = [SELECT Id,Analyst__c,Analyst_Name__c FROM BSureC_Customer_Basic_Info__c WHERE Manager__c =: String.valueOf(UserInfo.getUserId()) order by Analyst_Name__c ASC];
            }*/
            
            lstOptionsAnalyst = [SELECT Id,Analyst__c,Analyst_Name__c FROM BSureC_Customer_Basic_Info__c WHERE Analyst_Name__c != null order by Analyst_Name__c ASC];
            
            if(lstOptionsAnalyst!=null && lstOptionsAnalyst.size()>0)
            {
                lstSubordinateNames.add(new selectoption('--Select--','--Select--'));
                //strSubordinateNames='ALL';
                AnalystOptionsBlock=true;
                map<Id,String> mapoptions = new map<Id,String>();
                      for(BSureC_Customer_Basic_Info__c recoption : lstOptionsAnalyst)  
                            {
                                if( recoption.Analyst_Name__c!=null && recoption.Analyst_Name__c!='' &&  mapoptions.get(recoption.Analyst__c) == null)
                                {
                                    mapoptions.put(recoption.Analyst__c,recoption.Analyst_Name__c);
                                    lstSubordinateNames.add(new selectOption(recoption.Analyst__c,recoption.Analyst_Name__c));
                                }
                            }
          	}
           	else
	        {
                String strAnalyst=BSureC_CommonUtil.getConfigurationValues('BSureC_MassTransferAllowedProfiles').get(0);
                if(objCurProfile.name == strAnalyst)
                {
                    lstSubordinateNames.add(new selectoption(userName,usrName));
                    strSubordinateNames=userName;
                }
	        }
         finalobjWorkList = new list<CommonUserClass>();
         listPublish=new list<BSureC_Credit_Data_Publish__c>();
        //displayrecords();
        //for pagination
         //totallistsize = lstCustomerBasicInfo.size();
        totallistsize=0;
        pageNumber = 0;
        totalPageNumber = 0;
       }
        public pageReference ExportCSV()
        {
            isexport=true;
          //blnExport = true;
           // if(strexportID!=null && strexportID!='' && strexportID!='--Select--'){
          //  string[] startNo= strexportID.split(' to ');
           // integer intstartNo=integer.ValueOf(startNo[0]);
          //  exportRecordsCount=intstartNo-1;
          //       }
            pagenation(0);
            pageReference pRef = new pageReference('/apex/BSureC_ReviWorkliForMangAndAnaltExport');
            return pRef;
        }
        
        //For sorting grid in ascending and descending order
       public string sortExpression
        {
            get
            {
                return sortExp;
            }
            set
            {
                if(value == sortExp)
                    sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
                else
                    sortDirection = 'ASC';
                sortExp = value;
            }
        }
    
     //For sorting grid in ascending and descending order
     public String getSortDirection()
       {
            //if not column is selected 
            if (sortExpression == null || sortExpression == '')
                return 'ASC';
            else
                return sortDirection;
       }
    
    //For sorting grid in ascending and descending order
    public void setSortDirection(String value)
       {  
            sortDirection = value;
       }    
    
    //For sorting grid in ascending and descending order
    public Pagereference SortData()
       {
            displayrecords();
            return null;
       }
        
     /// <summary>
    /// displaydata(): to display the report for the scheduled worklist for analyst and manager.
    /// </summary>
    /// <returns> records for read only  
     public pagereference displayrecords()
     {
        //counter=0;
        total_size=0;
       
        displayPanel=true;
        worklistBlock=true;
        finalobjWorkList = new list<CommonUserClass>();
        lstCustomerBasicInfo = new list<BSureC_Customer_Basic_Info__c>();
        lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
        strMainQuery= 'Select Risk_Category__c,Reviewed_by__c,Credit_Limit__c,State_Province__c,Rebate_Disc__c ,NAICS__c,GMC__c,'+
                         'Fiscal_Year_End__c,Facility_S__c, Facility_Expiration__c,Review_Status__c,Parent_Customer_Name__c,Parent_Customer_ID__c,Owner_Ship__c,OwnerId,Next_Review_Date__c,'+
                         'Next_Review_Analyst_Name__c,Name,Manager__c,Manager_Name__c,Last_Financial_Statement_Received__c,Id,'+
                         'Has_Parent__c,DND_Financial_Information__c,Customer_Name__c,Review_Date__c,Customer_ID__c,Customer_Group_Id__c,City__c,'+
                         'Customer_Category__c,Customer_Category_Name__c,Credit_Account__c,Contact_Name__c,Bill_to_Account__c,'+
                         'Analyst_for_Next_Review__c,Analyst__c,Analyst_Name__c,Collector_Name__c From BSureC_Customer_Basic_Info__c'+
                         '  where  Review_Status__c!=\'Completed\'';   //Next_Review_Date__c >= today and  Next_Review_Date__c = NEXT_N_DAYS:60  and                
        if(strSubordinateNames!=null && strSubordinateNames!='' && strSubordinateNames=='--Select--' && loginUserProfile!=null && loginUserProfile!='' && !loginUserProfile.equalsIgnoreCase('System Administrator'))
            {
                strMainQuery+= '  And Manager__c =:userName';
              
        }else if(strSubordinateNames!=null && strSubordinateNames!='' && strSubordinateNames!='--Select--')
            {
                strMainQuery+= 'And Analyst__c =:strSubordinateNames';
            }
        strMainQuery+= ' ORDER BY  '+ sortExpression+' '+sortDirection ;
        // Starting ....... Vidya Sagar
        if(strCollectorNames != null && strCollectorNames!='' && strCollectorNames!='--Select--')
        {
            //system.debug('++++++++++++++++++++++'+strCollectorNames);
            list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strMainQuery);
            //system.debug('++++++++++++++++++++++'+lstCrdInc.size());
             for(integer i=0;i<lstCrdInc.size();i++)
              {
                string Collectorname = lstCrdInc[i].Collector_Name__c;
                //system.debug('++++++++++++++++++++++'+Collectorname);
                if(Collectorname != null && Collectorname !=''){
                BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
                c = lstCrdInc[i];
                if(c.Collector_Name__c.contains(strCollectorNames)){
                    //system.debug('++++++++required++++++++++++++'+c.Collector_Name__c);
                    lstCustomerBasicInfo.add(c);
                }
                }
              }
            }
            else{
           lstCustomerBasicInfo = database.query(strMainQuery);
        
            }
         // Ending...... Vidya Sagar H
        //lstCustomerBasicInfo=Database.query(strMainQuery);
        /*if(lstCustomerBasicInfo!=null)
        total_size=lstCustomerBasicInfo.size();
        pagenation(0);*/
        
        totallistsize= lstCustomerBasicInfo.size();// this size for pagination
        //system.debug('totallistsize**'+totallistsize);
        pageSize = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));// default page size will be 10
        if(totallistsize==0)
        {
            //IsExportVisible=true;
            pagenation(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber= lstResultsPage.size();
            //system.debug('if NxtPageNumber**'+NxtPageNumber);
            PrevPageNumber=0;
        }
        else
        {
            //IsExportVisible=false;
            pagenation(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber= lstResultsPage.size();
            //system.debug('else NxtPageNumber**'+NxtPageNumber);
            PrevPageNumber=1;
        }
        
        return null;
     }
     
     //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
     public pagereference pagenation(Integer newPageIndex)
     {
     	//lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
        //PrevPageNumber=counter+1;
        Transient Integer counter = 0;
        Transient Integer min = 0;
        Transient Integer max = 0;
        //system.debug(counter+'Listsize&&&'+lstCustomerBasicInfo.size());
        //totallistsize=lstCustomerBasicInfo.size();
        
        if(pageSize == null)pageSize = 10;
        if (newPageIndex > pageNumber)
        {
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;
        }
        else
        {
            max = newPageIndex * pageSize;
            min = max - pageSize;
        }
        displayPanel=true;
        worklistBlock=true;
        string strQuery;
        objWorkList = new list<CommonUserClass>();
        
        lstCustomerBasicInfo = new list<BSureC_Customer_Basic_Info__c>();
        lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
        if(isexport==true){
            strQuery=strMainQuery;
        }else{
        strQuery=strMainQuery;//+'   limit '+ list_size + ' offset '+counter;
        }
        // string strQuery= strMainQuery+'  limit '+ list_size + ' offset '+counter;
        //lstCustomerBasicInfo=Database.query(strQuery);
        // Starting ......... Vidya Sagar
        if(strCollectorNames != null && strCollectorNames!='' && strCollectorNames!='--Select--')
        {
            //system.debug('++++++++++++++++++++++'+strCollectorNames);
            list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strMainQuery);
            //system.debug('++++++++++++++++++++++'+lstCrdInc.size());
             for(integer i=0;i<lstCrdInc.size();i++)
             {
                string Collectorname = lstCrdInc[i].Collector_Name__c;
                //system.debug('++++++++++++++++++++++'+Collectorname);
                if(Collectorname != null && Collectorname !=''){
	                BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
	                c = lstCrdInc[i];
	                if(c.Collector_Name__c.contains(strCollectorNames)){
	                    //system.debug('++++++++required++++++++++++++'+c.Collector_Name__c);
	                    lstCustomerBasicInfo.add(c);
	                }
                }
              }
        }
    	else{
       		lstCustomerBasicInfo = database.query(strMainQuery);        
        }
        // Ending ........ Vidya Sagar
        set<string> UserIds=new set<string>();
        Map<string,CommonUserClass> mapObjDetails =new Map<string,CommonUserClass>();
        map<id,id> mapID=new  map<id,id>(); 
   		
   		if(isexport == true){
   			
   			for(BSureC_Customer_Basic_Info__c BasicInfo1:lstCustomerBasicInfo)
	        {
	        	UserIds.add(BasicInfo1.Id);
	            lstResultsPage.add(BasicInfo1);// here adding files list
	        }
   		
   		}else{
			for(BSureC_Customer_Basic_Info__c BasicInfo1:lstCustomerBasicInfo)
	        {
	        	counter++;
	            if (counter > min && counter <= max) 
	            {
	            	UserIds.add(BasicInfo1.Id);
	                lstResultsPage.add(BasicInfo1);// here adding files list
	            }
	            
	        }
   		}             
    
		listPublish=[SELECT Discount__c,of_Limit__c,Last_Review__c,Prompt__c,Risk_Category__c,Slow__c,Average_Days_Slow__c,BSureC_Customer_Basic_Info__c,CE__c,Credit_Limit__c,
             Customer_Name__c,Exposure__c,Group_Id__c from BSureC_Credit_Data_Publish__c where BSureC_Customer_Basic_Info__c in :UserIds ] ;   
     
  
        listPaymentHistory=[SELECT High_Credit__c,BSureC_Customer_Basic_Info__c from BSureC_Payment_History_Publish__c where BSureC_Customer_Basic_Info__c in: UserIds];
        Map<string,BSureC_Credit_Data_Publish__c> mapObjPublish =new Map<string,BSureC_Credit_Data_Publish__c>();
        Map<string,BSureC_Payment_History_Publish__c> mapObjHistory =new Map<string,BSureC_Payment_History_Publish__c>();

		for(BSureC_Credit_Data_Publish__c DataPublish :listPublish)
        {
            mapObjPublish.put(DataPublish.BSureC_Customer_Basic_Info__c,DataPublish);
        }
    		
		for(BSureC_Payment_History_Publish__c creditHistory:listPaymentHistory) 
        {
            mapObjHistory.put(creditHistory.BSureC_Customer_Basic_Info__c,creditHistory);
        }
        
		for(BSureC_Customer_Basic_Info__c BasicInfo:lstResultsPage)
        {
	         CommonUserClass objWorkList1=new CommonUserClass(); 
	         UserIds.add(BasicInfo.Id);
	         objWorkList1.strCustomerName=BasicInfo.Customer_Name__c;
	         objWorkList1.custid=BasicInfo.Id;
	         objWorkList1.strCreditAccountNumber=BasicInfo.Credit_Account__c;
	         objWorkList1.strGroupNumber=BasicInfo.Customer_Group_Id__c;
	         objWorkList1.strCity=BasicInfo.City__c;
	         objWorkList1.strState=BasicInfo.State_Province__c;
	         objWorkList1.strAnalyst = BasicInfo.Analyst_Name__c;
	         objWorkList1.strCollector = BasicInfo.Collector_Name__c;
	         if(isexport==true && BasicInfo.Risk_Category__c!=null && BasicInfo.Risk_Category__c!='')
	         {
	         objWorkList1.strRiskCategory='\''+BasicInfo.Risk_Category__c;
	         }else{
	         objWorkList1.strRiskCategory=BasicInfo.Risk_Category__c;
	         }
	         objWorkList1.strGMC=BasicInfo.GMC__c;
	         objWorkList1.strNAICS=BasicInfo.NAICS__c;
	         objWorkList1.strRebateDist=BasicInfo.Rebate_Disc__c;
	         objWorkList1.strFYE=BasicInfo.Fiscal_Year_End__c;
	         objWorkList1.strFacilitySecured=BasicInfo.Facility_S__c;                
	         objWorkList1.strFacilityExpiration=BasicInfo.Facility_Expiration__c;
	         objWorkList1.strConfidential=BasicInfo.DND_Financial_Information__c;
	         objWorkList1.strReviewStatus=BasicInfo.Review_Status__c;
	         objWorkList1.strLatestFinancials= BasicInfo.Last_Financial_Statement_Received__c;  
	         objWorkList1.strNextReviewDate=BasicInfo.Next_Review_Date__c;
	         objWorkList1.strManager=BasicInfo.Manager_Name__c;
	         objWorkList1.strLastReviewDate=BasicInfo.Review_Date__c;
	         objWorkList1.strCreditLimit=BasicInfo.Credit_Limit__c;
	         BSureC_Credit_Data_Publish__c DataPublish1= mapObjPublish.get(BasicInfo.Id);
		     if(DataPublish1!=null)
		     {
	            objWorkList1.strPrompt = DataPublish1.Prompt__c;
	            objWorkList1.strDiscount=DataPublish1.Discount__c;
	            objWorkList1.strSlow=DataPublish1.Slow__c;
		     }
	                            
	          BSureC_Payment_History_Publish__c creditHistory1=mapObjHistory.get(BasicInfo.Id);
	          if(creditHistory1!=null)
	          {
	                 objWorkList1.strHighCredit=creditHistory1.High_Credit__c;
	          }
	          objWorkList.add(objWorkList1);
 
        }
		if(objWorkList.size()== 0)
		{
             exportTrue=true;
             ApexPages.Message myEMsg=new ApexPages.Message(ApexPages.severity.INFO,System.label.BSureC_No_Record_Found);
             ApexPages.addMessage(myEMsg);
        }else{
             exportTrue=false;
        }
        
        pageNumber = newPageIndex;
        //system.debug('pageNumber**'+pageNumber);
        NlistSize = objWorkList.size();
                    
         return null;     
     }
     
     /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
	        lstCustomerBasicInfo = new list<BSureC_Customer_Basic_Info__c>();
	        lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
	        objWorkList = new list<CommonUserClass>();
	        string strQuery;
	        
	        if(isexport==true){
	            strQuery=strMainQuery;
	        }else{
	        strQuery=strMainQuery;//+'   limit '+ list_size + ' offset '+counter;
	        }
	        // string strQuery= strMainQuery+'  limit '+ list_size + ' offset '+counter;
	        //lstCustomerBasicInfo=Database.query(strQuery);
	        // Starting ......... Vidya Sagar
	        if(strCollectorNames != null && strCollectorNames!='' && strCollectorNames!='--Select--')
	        {
	            //system.debug('++++++++++++++++++++++'+strCollectorNames);
	            list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strMainQuery);
	            //system.debug('++++++++++++++++++++++'+lstCrdInc.size());
	             for(integer i=0;i<lstCrdInc.size();i++)
	             {
	                string Collectorname = lstCrdInc[i].Collector_Name__c;
	                //system.debug('++++++++++++++++++++++'+Collectorname);
	                if(Collectorname != null && Collectorname !=''){
		                BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
		                c = lstCrdInc[i];
		                if(c.Collector_Name__c.contains(strCollectorNames)){
		                    //system.debug('++++++++required++++++++++++++'+c.Collector_Name__c);
		                    lstCustomerBasicInfo.add(c);
		                }
	                }
	              }
	        }
	    	else{
	       		lstCustomerBasicInfo = database.query(strMainQuery);        
	        }
	        
            
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            min = (newPageIndex-1) * pageSize;
            max = newPageIndex * pageSize;
            
            set<string> UserIds=new set<string>();
	        Map<string,CommonUserClass> mapObjDetails =new Map<string,CommonUserClass>();
	        map<id,id> mapID=new  map<id,id>(); 
	   		
	   		
			for(BSureC_Customer_Basic_Info__c BasicInfo1:lstCustomerBasicInfo)
	        {
	        	counter++;
	            if (counter > min && counter <= max) 
	            {
	            	UserIds.add(BasicInfo1.Id);
	                //strIds.add(b.id);
	                lstResultsPage.add(BasicInfo1);// here adding files list
	            }
	            
	        }             
			listPublish=[SELECT Discount__c,of_Limit__c,Last_Review__c,Prompt__c,Risk_Category__c,Slow__c,Average_Days_Slow__c,BSureC_Customer_Basic_Info__c,CE__c,Credit_Limit__c,
	             Customer_Name__c,Exposure__c,Group_Id__c from BSureC_Credit_Data_Publish__c where BSureC_Customer_Basic_Info__c in :UserIds ] ;   
	     
	  
	        listPaymentHistory=[SELECT High_Credit__c,BSureC_Customer_Basic_Info__c from BSureC_Payment_History_Publish__c where BSureC_Customer_Basic_Info__c in: UserIds];
	        Map<string,BSureC_Credit_Data_Publish__c> mapObjPublish =new Map<string,BSureC_Credit_Data_Publish__c>();
	        Map<string,BSureC_Payment_History_Publish__c> mapObjHistory =new Map<string,BSureC_Payment_History_Publish__c>();
	
			for(BSureC_Credit_Data_Publish__c DataPublish :listPublish)
	        {
	            mapObjPublish.put(DataPublish.BSureC_Customer_Basic_Info__c,DataPublish);
	        }
	    		
			for(BSureC_Payment_History_Publish__c creditHistory:listPaymentHistory) 
	        {
	            mapObjHistory.put(creditHistory.BSureC_Customer_Basic_Info__c,creditHistory);
	        }
	        
			for(BSureC_Customer_Basic_Info__c BasicInfo:lstResultsPage)
	        {
	         CommonUserClass objWorkList1=new CommonUserClass(); 
	         UserIds.add(BasicInfo.Id);
	         objWorkList1.strCustomerName=BasicInfo.Customer_Name__c;
	         objWorkList1.custid=BasicInfo.Id;
	         objWorkList1.strCreditAccountNumber=BasicInfo.Credit_Account__c;
	         objWorkList1.strGroupNumber=BasicInfo.Customer_Group_Id__c;
	         objWorkList1.strCity=BasicInfo.City__c;
	         objWorkList1.strState=BasicInfo.State_Province__c;
	         objWorkList1.strAnalyst = BasicInfo.Analyst_Name__c;
	         objWorkList1.strCollector = BasicInfo.Collector_Name__c;
	         if(isexport==true && BasicInfo.Risk_Category__c!=null && BasicInfo.Risk_Category__c!='')
	         {
	         objWorkList1.strRiskCategory='\''+BasicInfo.Risk_Category__c;
	         }else{
	         objWorkList1.strRiskCategory=BasicInfo.Risk_Category__c;
	         }
	         objWorkList1.strGMC=BasicInfo.GMC__c;
	         objWorkList1.strNAICS=BasicInfo.NAICS__c;
	         objWorkList1.strRebateDist=BasicInfo.Rebate_Disc__c;
	         objWorkList1.strFYE=BasicInfo.Fiscal_Year_End__c;
	         objWorkList1.strFacilitySecured=BasicInfo.Facility_S__c;                
	         objWorkList1.strFacilityExpiration=BasicInfo.Facility_Expiration__c;
	         objWorkList1.strConfidential=BasicInfo.DND_Financial_Information__c;
	         objWorkList1.strReviewStatus=BasicInfo.Review_Status__c;
	         objWorkList1.strLatestFinancials= BasicInfo.Last_Financial_Statement_Received__c;  
	         objWorkList1.strNextReviewDate=BasicInfo.Next_Review_Date__c;
	         objWorkList1.strManager=BasicInfo.Manager_Name__c;
	         objWorkList1.strLastReviewDate=BasicInfo.Review_Date__c;
	         objWorkList1.strCreditLimit=BasicInfo.Credit_Limit__c;
	         BSureC_Credit_Data_Publish__c DataPublish1= mapObjPublish.get(BasicInfo.Id);
		     if(DataPublish1!=null)
		     {
	            objWorkList1.strPrompt = DataPublish1.Prompt__c;
	            objWorkList1.strDiscount=DataPublish1.Discount__c;
	            objWorkList1.strSlow=DataPublish1.Slow__c;
		     }
	                            
	          BSureC_Payment_History_Publish__c creditHistory1=mapObjHistory.get(BasicInfo.Id);
	          if(creditHistory1!=null)
	          {
	                 objWorkList1.strHighCredit=creditHistory1.High_Credit__c;
	          }
	          objWorkList.add(objWorkList1);
 
        	}
            
            
            pageNumber = newPageIndex;
            NlistSize=lstResultsPage.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
     
      /// <summary>
     /// CommonUserClass: A wraper class to fetch the records from different objects.
     /// </summary>
     /// <returns> records for read only  
      public class CommonUserClass
     {
        public string strCustomerName {get; set;} //to hold Customer Name
        public string strCreditAccountNumber {get; set;}//to hold Credit Account Number
        public string strGroupNumber {get; set;}//to hold Group Number
        public string strCity {get; set;}//to hold city
        public string strState{get;set;}//to hold state
        public string strAnalyst{get;set;}//to hold Analyst Name
        public Date strLastSaleDate{get;set;}//to hold Last Sale Date
        public Date strLastReviewDate{get;set;} //to hold Last Review Date
        public double strPrompt {get; set;}//to hold prompt value
        public string strADS {get; set;}//to hold average date slow
        public string strRiskCategory {get; set;}//to hold risk Category
        public double strHighCredit{get;set;} //to hold high credit
        public double strCreditLimit{get;set;}//to hold credit limit
        public double strGMC{get;set;}// to hold gmc value
        public double strNAICS{get;set;}//to hold NAICS value
        public string strRebateDist{get;set;}//to hold rebate dist
        public Date strFYE {get; set;}//to hold financial year end date
        public String custid{get;set;}//to hold user id
        public Date strLatestFinancials  {get; set;}//to hold latest financials
        public Date strFacilityExpiration{get;set;}//to hols facility expiration
        public string strFacilitySecured{get;set;}//to hold facility secured/unsecured value
        public string strConfidential{get;set;}//to hold DND confidential 
        public string strCreditCard{get;set;}//to hold credit card
        public string strRisk{get;set;}//to hold risk 
        public double strDiscount{get;set;}//to hold discount value
        public double strSlow{get;set;}//to get slow value
        public string strReviewStatus{get;set;}//to get review status
        public Date strNextReviewDate{get;set;}//to get next review date
        public string strManager{get;set;}//to hold manager name
        // Added by Vidya Sagar
        public String strCollector{get;set;}// to hold Collectors name
   }
   
    public pagereference getCustomer()
    {
        String id=Apexpages.currentPage().getParameters().get('custid');
        system.debug('custid@@@'+id);
        pagereference pg=new pagereference('/'+id);
        return pg;
    }
   
   /// <summary>
   /// This method is for pagination, if atleast one record found then passing 1 to the pagination method
   /// <param name="PageSize"></param>
   /// <returns>Integer</returns>  
   /// </summary>   
    public PageReference FirstbtnClick() { //user clicked beginning
          //counter = 0;
          //pagenation(pageNumber + 1);
          //pageData(pageNumber + 1);
          pagenation(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
          NxtPageNumber=NlistSize;
          PrevPageNumber=1;
          return null;
    }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
     public PageReference previousBtnClick() { //user clicked previous button
          //counter -= list_size;
          pageData(pageNumber - 1);
          pagenation(pageNumber - 1);
          return null;
       }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
     public PageReference nextBtnClick() { //user clicked next button
          //counter += list_size;
          pagenation(pageNumber + 1);
          pageData(pageNumber + 1);
          return null;
       }
       
       
   // <summary>
   // Below method fires when user clicks on previous button of pagination.
   // </summary>
   // <returns>pagereference</returns>
      public PageReference LastbtnClick() { //user clicked end
          /*counter = total_size - math.mod(total_size, list_size);
          pagenation();*/
          //system.debug('totalpagenumber@@'+totalpagenumber);
          //pagenation(totalpagenumber - 1);
          //pageData(totalpagenumber);
          LastpageData(totalpagenumber);
          return null;
       }
       
       
   /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
       public Boolean getPreviousButtonEnabled() {
          //this will disable the previous and beginning buttons
          //if (counter>0) return false; else return true;
          return !(pageNumber > 1);
       }
       
       
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
    /// </summary>
   public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
      //if (counter + list_size < total_size) return false; else return true;
      if(totallistsize!=null)
        {
            if (totallistsize ==0) 
                return true;
            else
                return ((pageNumber * pageSize) >= totallistsize);
        }
    	return true;
   }
     
       public Integer getPageSize() {
          //return total_size;
          return pageSize;
       }
      
       public Integer getPageNumber() {
          return pageNumber; //counter/list_size + 1;
       }
       
  /// <summary>
  /// Below method gets the total no.of pages.
  /// <param name="TotalPageNumber"></param>
  /// <returns>Integer</returns>    
  /// </summary>
       public Integer getTotalPageNumber() {
          /*if (total_size!=null && list_size!=null && math.mod(total_size, list_size) > 0) {
             return total_size/list_size + 1;
          } else if(total_size!=null && list_size!=null ){
             return (total_size/list_size);
          }
          else
          {
            return null;
          }*/
          
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=null && totallistsize!=0  )
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
      
       }
       
       public pageReference  cancel()
       {
         pageReference pRef = new pageReference('/apex/BSureC_Reports');
         return pRef;
       }
       /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
             
        }
        //system.debug('NlistSize============='+NlistSize);
    }
       
 }