/***********************************************************************************************
* Controller Name   : BSureC_ContactsAssignment
* Date              : 12/07/2014 
* Author            : Satish.CH
* Purpose           : Class for Assign Contacts 
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- ------------------------- 
* 12/01/2014            Satish.c        	Initial Version

**************************************************************************************************/

public with sharing class BSureC_ContactsAssignment {

	public string currentCustomerId{get;set;}
	public string contactId{get;set;}
	public string strCurrentSupplierId{get;set;} // page returned id
	public boolean custinsertval{get;set;} // Holds boolean value for validating duplicate customer Contact
	public BSureC_Assigned_Contacts__c BSureContactInfo{get;set;}
	public string Role{get;set;}
	public Boolean Edit{get;set;}
	public Boolean isSave{get;set;}
	
	
	public BSureC_ContactsAssignment(ApexPages.StandardController stdController)
    {
    	Edit = false;
    	currentCustomerId=Apexpages.currentPage().getParameters().get('Custid');
    	BSureContactInfo= new BSureC_Assigned_Contacts__c();
    	
    	if(Apexpages.currentPage().getParameters().get('id') != null)
		{  
			contactId = apexpages.currentpage().getParameters().get('id'); 
			BSureContactInfo = (BSureC_Assigned_Contacts__c)stdController.getRecord();
			Edit = true;
            
            if(contactId != null)
            {
            	BSureContactInfo = [SELECT Id,Customer_Id__c,Customer_Name__c,Contact_Name__c,Comments__c,Email_address__c,Fax__c,Phone_Number__c,Role__c,Website_Link__c from BSureC_Assigned_Contacts__c where Id=: contactId Limit 1];
            	currentCustomerId = BSureContactInfo.Customer_Id__c;
            	isSave = false;
            }
            
		}
    }
    
    public pageReference SaveBSureCustomerContact()
    {
        pageReference CustRef;
        Integer CreditAccDupCheck = 0;
        Integer CreditbILLDupCheck = 0;
        string errmsg=null;
        custinsertval=false;
        string custvalidate='';
        //Database.SaveResult CustInfoInsert;
        
        //if(isSave==true)
        //{
	        system.debug('===BSureContactInfo.Contact_Name__c==='+BSureContactInfo.Contact_Name__c);
	        
	        if(BSureContactInfo.Contact_Name__c=='' || BSureContactInfo.Contact_Name__c==null)
	        {
	            custinsertval=true;
	            showErrorMessage('Please Enter Contact Name');
	        }
	        else if(BSureContactInfo.Role__c=='-None-')
	        {
	            custinsertval=true;
	            showErrorMessage('Please Select Role');
	        }
	        if(!custinsertval)
	        {
	        	BSureContactInfo.Customer_Id__c = currentCustomerId;
	            system.debug('===BSureCustInfo==='+currentCustomerId);
	            if(currentCustomerId !=null || currentCustomerId !='')
	            {
	                Database.UpsertResult resultDB= Database.upsert(BSureContactInfo);
	                //insert BSureContactInfo;
	                CustRef=new PageReference('/'+currentCustomerId);
	            }
	            return CustRef;
	        }
        //}
        return null;
     }
     public pageReference showErrorMessage(String msg)
     {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
     }
     public pagereference Cancel()  
     {
        pagereference pageref = new pagereference('/apex/BSureC_ViewCustomerDetails?Id='+currentCustomerId);
        return pageref;
     }

}