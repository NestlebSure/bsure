/* Change History    : 
*   Date           	Programmer         	Reason
*	13/08/2013		B.Anupreethi		Code revie changes
================================================================================================*/
global with sharing class BSureS_Dashboard {
    public list<Dashboard> lstDashboard{get;set;}
    public list<User> lstUser{get;set;}
    public Id DashboardID;
    
    public string strBuyerRole{get;set;}
    public string strCEO{get;set;}
    public string strAnalystRole{get;set;}
    public string strZoneManagerRole{get;set;}
    public string strSubZoneManagerRole{get;set;}
    public string strCountryManagerRole{get;set;}
    
    public BSureS_Dashboard()
    {
            getDashboardID();
    }
    public Id getDashboardID()
    {
    	String strTitle;
    	//String strUserRole = UserInfo.get
    	 //Anu Check if the user has read access on the Dashboard,User object
    	if(!Schema.sObjectType.Dashboard.isAccessible() || !Schema.sObjectType.User.isAccessible())    
    	{
    		return '';
    	}
    	strBuyerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerRole').get(0);
    	strCEO = BSureS_CommonUtil.getConfigurationValues('BSureS_CEORole').get(0);
    	strAnalystRole = BSureS_CommonUtil.getConfigurationValues('BSureS_AnalystRole').get(0);
    	strZoneManagerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_ZoneManagerRole').get(0);
    	strSubZoneManagerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_SubZoneManagerRole').get(0);
    	strCountryManagerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_CountryManagerRole').get(0);
    	lstUser = new list<User>([select id,userrole.name from User where id =: UserInfo.getUserId() ]);
    	//system.debug('lstUser=====id==='+lstUser.get(0).Id + 'User role===='+lstUser.get(0).Userrole);
    	if(lstUser != null && lstUser.size() > 0)
    	{
    		
        	lstDashboard = new list<Dashboard>();
        	//lstDashboard = [Select Id From Dashboard where title = 'Suppliers Status Dashboard'];
        	//Supplier Status Dashboard for Admin
        	if( (strCEO != null && strCEO !='') && (strCEO == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Status Dashboard for Admin';
        		lstDashboard = [Select Id From Dashboard where title =: strTitle];
        	}
        	else if((strAnalystRole != null && strAnalystRole != '') && (strAnalystRole == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Status Dashboard for Analyst';
        		lstDashboard = [Select Id From Dashboard where title =: strTitle ];
        	}	
        	else if((strBuyerRole != null && strBuyerRole != '') && (strBuyerRole == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Status Dasboard for Buyer';
        		lstDashboard = [Select Id From Dashboard where title =: strTitle ];
        	}
        	else if((strZoneManagerRole != null && strZoneManagerRole != '') && (strZoneManagerRole == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Dashboard For Manager';
        		lstDashboard = [Select Id From Dashboard where title =: strTitle ];
        	}
        	else if((strSubZoneManagerRole != null && strSubZoneManagerRole != '') && (strSubZoneManagerRole == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Dashboard For Manager';
        		lstDashboard = [Select Id From Dashboard where title =:strTitle ];
        	}
        	else if((strCountryManagerRole != null && strCountryManagerRole != '') && (strCountryManagerRole == lstUser.get(0).userrole.name))
        	{
        		strTitle='Supplier Dashboard For Manager';
        		lstDashboard = [Select Id From Dashboard where title =: strTitle];
        	}
    	}	
        if(lstDashboard != null && lstDashboard.size() > 0)
        {
            DashboardID = lstDashboard.get(0).Id;
        }
        //system.debug('dashboardRecordId========='+DashboardID);
        return DashboardID;
    }
}