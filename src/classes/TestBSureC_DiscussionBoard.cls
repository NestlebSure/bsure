/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureC_DiscussionBoard {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Apexpages.currentPage().getParameters().put('mode', 'CICS-000001');
        list<BSureC_Customer_Basic_Info__c> objListCust = new list<BSureC_Customer_Basic_Info__c>();
        
        /*
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'BSureC_VisitorProfile';
        objCSettings6.Parameter_Key__c = 'BSureC_VisitorProfile';
        objCSettings6.Parameter_Value__c = 'BSureC_Visitor';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Key__c = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        insert cSettings4;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_CreditAdminProfile';
        objCSettings7.Parameter_Key__c = 'BSureC_CreditAdminProfile';
        objCSettings7.Parameter_Value__c = 'BSureC_CreditAdmin';
        insert objCSettings7;
        */
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
        
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Analyst'];
        
        User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
                
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(
                                                    Customer_Name__c='KingFisher Customer',
                                                    Analyst__c = objUser.Id);
                                                    
        objListCust.add(objCustomer);
        
        Database.Saveresult objSaveCust =  Database.insert(objCustomer);
        
        Apexpages.currentPage().getParameters().put('custid', objSaveCust.Id);
        
        BSureC_Invoice_Collection_Section__c objSection = new BSureC_Invoice_Collection_Section__c();
        objSection.Comment__c = 'Test Comment &lt;&quot;\nTest&quot;&gt;';
        objSection.TitleDescription__c = 'Test Description';
        objSection.Discussion_Type__c = 'Link';
        objSection.Source__c = 'Test Source';
        system.assertEquals('Test Description',objSection.TitleDescription__c);
        objSection.DiscussionsCount__c = 1;
        objSection.Customer_ID__c = objSaveCust.Id;
        Database.Saveresult objSaveSec =  Database.insert(objSection);
        
        
        Apexpages.currentPage().getParameters().put('prtid', objSaveSec.Id);
        Apexpages.currentPage().getParameters().put('id', objSaveSec.Id);
        Apexpages.currentPage().getParameters().put('mode', 'CICS-000001');
        Apexpages.currentPage().getParameters().put('type', 'Link');
        Apexpages.currentPage().getParameters().put('type', 'Topic');
        Apexpages.currentPage().getParameters().put('type', 'File');
        Apexpages.currentPage().getParameters().put('mode', 'CUCDS-000001');
        Apexpages.currentPage().getParameters().put('mode', 'CUDS-000001');
        Apexpages.currentPage().getParameters().put('mode', 'CCDS-000001');
        Apexpages.currentPage().getParameters().put('mode', 'COGCS-000001');
        Apexpages.currentPage().getParameters().put('mode', 'CCIS-000001');
        
        BSureC_DiscussionBoard objBSureDisc = new BSureC_DiscussionBoard();
        list<BSureC_DiscussionBoard.DiscussBean> objListBean = new list<BSureC_DiscussionBoard.DiscussBean>();
         
        objBSureDisc.SetSObjectInfo('CUCDS');
        objBSureDisc.SetSObjectInfo('CUDS');
        objBSureDisc.SetSObjectInfo('CCDS');
        objBSureDisc.SetSObjectInfo('COGCS');
        objBSureDisc.SetSObjectInfo('');
        objBSureDisc.SetSObjectInfo('CCIS');
        objBSureDisc.SetSObjectInfo('CICS');
        
        objBSureDisc.strTimezoneSIDKey = 'America/Los_Angeles';
        system.assertEquals('America/Los_Angeles',objBSureDisc.strTimezoneSIDKey);
        objBSureDisc.ClosePostFields();
        objBSureDisc.DeletePost();
        objBSureDisc.PostNewReply();
        objBSureDisc.strDiscussionType = 'File';
        
        objBSureDisc.GetQueryParams();
        objBSureDisc.GetThreadSObjectData(objSaveSec.Id);
        objBSureDisc.SendNotifications();
        
        BSureC_Invoice_Collection_Discussion__c objDummy = new BSureC_Invoice_Collection_Discussion__c();
        objDummy.Comment__c = 'Test Comment';
        objDummy.Title__c = 'Test Title';
        objDummy.Link__c = 'Test Link';
        objDummy.Parent_ID__c = 'TestID';
        //objDummy.CreatedDate = system.today();
        objDummy.Invoice_Collection_ID__c = 'CICS-000001';
        system.assertEquals('Test Comment',objDummy.Comment__c);
        objDummy.BSureC_Invoice_Collection_Section__c = objSaveSec.Id;
        
        Database.Saveresult objSave =  Database.insert(objDummy);
        
        objBSureDisc.strFileName = 'FileName';
        objBSureDisc.UploadFile(objSave.Id);
        objBSureDisc.GetIDsForCrAnalysis(objListCust);
        objBSureDisc.GetIDsForInvoiceCol(objListCust);
        objBSureDisc.GetIDsForOnGoing(objListCust);
        objBSureDisc.GetIDsForUnAuthDed(objListCust);
        objBSureDisc.GetIDsForUnCashDisc(objListCust);
        
        BSureC_Invoice_Collection_Discussion__c objGetDummy = [SELECT Invoice_Collection_DiscussionID__c 
                                                                FROM BSureC_Invoice_Collection_Discussion__c
                                                                WHERE Id =: objSave.Id];
        objBSureDisc.GetParentSObjData(objGetDummy.Invoice_Collection_DiscussionID__c);
        
        //HardCoded for CICS - Begin
        objBSureDisc.GetDiscSObjectData('CICS-000001');
        
        BSureC_DiscussionBoard.DiscussBean objNewBean = new BSureC_DiscussionBoard.DiscussBean();
        
        objNewBean.strComment = 'Test Comment';
        objNewBean.strTitle = 'Test Title';
        objNewBean.strThreadID = 'CICS-000001';
        objNewBean.strCreatedBy = 'Test User';
        objNewBean.strParentID = 'Test';
        objNewBean.strFileName = 'TestFile.txt';
        system.assertEquals('Test Comment',objNewBean.strComment);
        
        objNewBean.strThreadSFDCID = objSaveSec.Id;
        objListBean.add(objNewBean);
        objBSureDisc.UpdateDiscussionSObject(objNewBean);
        objBSureDisc.GenerateHTML(objListBean);
        
        
        objBSureDisc.hdnParentID = 'CICS-000001';
        objBSureDisc.ReplyOnPost();
        objBSureDisc.showErrorMessage('All Is Well!!');
        objBSureDisc.Cancel();
        objBSureDisc.ClosePostFields();
        //HardCoded for CICS - END
    
    }
    
    static testMethod void myUnitTest2() {
        /*
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'BSureC_VisitorProfile';
        objCSettings6.Parameter_Key__c = 'BSureC_VisitorProfile';
        objCSettings6.Parameter_Value__c = 'BSureC_Visitor';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Key__c = 'CustomerFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        insert cSettings4;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_CreditAdminProfile';
        objCSettings7.Parameter_Key__c = 'BSureC_CreditAdminProfile';
        objCSettings7.Parameter_Value__c = 'BSureC_CreditAdmin';
        insert objCSettings7;
        */
        Apexpages.currentPage().getParameters().put('mode', 'CICS-000001');
        Apexpages.currentPage().getParameters().put('type', 'Link');
       
        BSureC_DiscussionBoard objBSureDisc = new BSureC_DiscussionBoard();
        list<BSureC_DiscussionBoard.DiscussBean> objListBean = new list<BSureC_DiscussionBoard.DiscussBean>();
        
        objBSureDisc.SetSObjectInfo('CICS');
        
        objBSureDisc.strTimezoneSIDKey = 'America/Los_Angeles';
        
        system.assertEquals('America/Los_Angeles',objBSureDisc.strTimezoneSIDKey);
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(Customer_Name__c='KingFisher Customer');
        Database.Saveresult objSaveCust =  Database.insert(objCustomer);
        
        objBSureDisc.GetQueryParams();
        
    }
}