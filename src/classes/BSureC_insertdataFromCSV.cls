public class BSureC_insertdataFromCSV{
    
    // constructor
    public BSureC_insertdataFromCSV(){    
         
    }
   
    public static void Insertrecords(string strBatchId,string strCSV)
    {
      
        // main code starts -----------------------
       list<BSureC_Category__c> lstSCatTypes = [SELECT id, Name, Category_ID__c FROM BSureC_Category__c WHERE id!=null order BY Name limit 50000];
       list<BSureC_Region__c> listregion = [select id,Name from BSureC_Region__c WHERE id!=null order by Name ASC limit 50000];
       list<BSureC_Zone__c> lstZones = [select Id,Name from BSureC_Zone__c  where IsActive__c=true order by Name ASC limit 50000];
       list<BSureC_Sub_Zone__c> lstSubZones = [select Id,Name,ZoneID__c from BSureC_Sub_Zone__c where IsActive__c=true order by Name ASC limit 50000];
       list<BSureC_Country__c> lstCountry  = [select Id,Name,Sub_Zone_ID__c  from BSureC_Country__c where IsActive__c=true order by Name ASC limit 50000];
       list<BSureC_State__c> lstStateInfo = [select id, Name from BSureC_State__c where IsActive__c = true  order by Name limit 50000];
       list<User> lstAnalysts = [SELECT Id, Name, UserRole.Name FROM User WHERE IsActive = true AND Profile.Name =: 'BSureC_Analyst' AND UserRole.Name =: 'Analyst' limit 50000];
       list<User> lstCollectors = [SELECT Id, Name, UserRole.Name FROM User WHERE IsActive = true AND Profile.Name =: 'BSureC_Collector' AND UserRole.Name =: 'Collector' limit 50000];
       list<BSureC_Customer_Basic_Info__c> lst_customer_info = [SELECT Id,Customer_Group_Id__c,Credit_Account__c 
                                                                 FROM BSureC_Customer_Basic_Info__c
                                                                 WHERE id!=null AND Credit_Account__c != null limit 50000];
       map<string,Id> mapcattypes = new map<String,Id>();
       map<string,Id> mapregions = new map<String,Id>();
       map<string,Id> mapzones = new map<String,Id>();
       map<string,Id> mapsubzones = new map<String,Id>();
       map<string,Id> mapcountries = new map<String,Id>();
       map<string,Id> mapstate = new map<String,Id>();
       map<string,Id> mapanalysts = new map<String,Id>();
       map<string,string> mapcollectors = new map<String,string>();
       list<string> lstcollnames = new list<string>();
       map<String,Id> map_customer_info = new map<String,Id>();
       if(lst_customer_info != null && lst_customer_info.size() > 0)
        {
        	for(BSureC_Customer_Basic_Info__c reccustinfo : lst_customer_info)
	        {
	            map_customer_info.put(reccustinfo.Credit_Account__c,reccustinfo.Id);
	        }
        } 
       if(lstSCatTypes.size()>0){
       	 for(integer i=0;i<lstSCatTypes.size();i++)
		    {
		    	mapcattypes.put(lstSCatTypes[i].Name,lstSCatTypes[i].id);		    
		    }
       }
       if(listregion.size()>0){
       	 for(integer i=0;i<listregion.size();i++)
		    {
		    	mapregions.put(listregion[i].Name,listregion[i].id);		    
		    }
       }
       if(lstZones.size()>0){
       	 for(integer i=0;i<lstZones.size();i++)
		    {
		    	mapzones.put(lstZones[i].Name,lstZones[i].id);		    
		    }
       }
       if(lstSubZones.size()>0){
       	 for(integer i=0;i<lstSubZones.size();i++)
		    {
		    	mapsubzones.put(lstSubZones[i].Name,lstSubZones[i].id);		    
		    }
       }
       if(lstCountry.size()>0){
       	 for(integer i=0;i<lstCountry.size();i++)
		    {
		    	mapcountries.put(lstCountry[i].Name,lstCountry[i].id);		    
		    }
       }
       if(lstStateInfo.size()>0){
       	 for(integer i=0;i<lstStateInfo.size();i++)
		    {
		    	mapstate.put(lstStateInfo[i].Name,lstStateInfo[i].id);		    
		    }
       }
       if(lstAnalysts.size()>0){
       	 for(integer i=0;i<lstAnalysts.size();i++)
		    {
		    	mapanalysts.put(lstAnalysts[i].Name,lstAnalysts[i].id);		    
		    }
       }
       if(lstCollectors.size()>0){
       	 for(integer i=0;i<lstCollectors.size();i++)
		    {
		    	system.debug('lstCollectors[i].Name'+lstCollectors[i].Name+'=====');
		    	mapcollectors.put(lstCollectors[i].Name,lstCollectors[i].Name);	
		    	lstcollnames.add(lstCollectors[i].Name);  
		    }
       }
        list<list<String>> parsedFields = parseCSV(strCSV,true);
        list<BSureC_Customer_Basic_Info_Stage__c> lststage = new list<BSureC_Customer_Basic_Info_Stage__c>();
       // list<string> inputvalues = new list<String>();
        // for(integer i=0;i<parsedFields.size();i++)
               for(List<String> inputvalues : parsedFields)
        {
         // list<string> inputvalues = new list<String>();
         // inputvalues = parsedFields[i];
          system.debug('-----------for inputvalues size--------------'+inputvalues);
         BSureC_Customer_Basic_Info_Stage__c rec_customer_info_stage = new BSureC_Customer_Basic_Info_Stage__c();
         rec_customer_info_stage.Status__c='Initialized';
         rec_customer_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
          if(inputvalues[0]!=NULL && inputvalues[0]!=' ' && inputvalues[0]!='')
                    {
                    	system.debug('------------------inputvalues[0]------'+inputvalues[0]);
		         if(map_customer_info.get(inputvalues[0]) != null )
		                        {
		                        	system.debug('test mappppppppppp');
		                        	rec_customer_info_stage.Exception__c='Credit Account is already exists';
			                        rec_customer_info_stage.Status__c='Exception';
			                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
		                        }
		         if( inputvalues[0].contains('\'') )
		                        {
		                            String Str_corrected_value = inputvalues[0].substring(1, inputvalues[0].length());
		                            rec_customer_info_stage.Credit_Account__c = Str_corrected_value;
		                        }
		                        else
		                        {
		                            rec_customer_info_stage.Credit_Account__c = inputvalues[0];
		                        }
		                           
		                   }                   
                   else
                   {
                        rec_customer_info_stage.Exception__c='Credit Account is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
              system.debug('rec_customer_info_stage========'+rec_customer_info_stage);
              system.debug('------------------inputvalues[1]------'+inputvalues[1]);
              if(inputvalues[1]!=NULL && inputvalues[1]!=' ' && inputvalues[1]!='')
                   {
                        //system.debug('inputvalues[1] !=NULL========================'+inputvalues[1]);
                        rec_customer_info_stage.Customer_Name__c = inputvalues[1];
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Customer Name is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                       else{
	                        rec_customer_info_stage.Exception__c += ', '+'Customer Name is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                      	  }
                   }
                   system.debug('------------------inputvalues[2]------'+inputvalues[2]);
                   if(inputvalues[2]!=NULL && inputvalues[2]!=' ' && inputvalues[2]!='')
                   {
                        //system.debug('inputvalues[2] !=NULL========================'+inputvalues[2]);
                        if( inputvalues[0].contains('\'') )
                        {
                            String Str_corrected_value = inputvalues[2].substring(1, inputvalues[2].length());
                            rec_customer_info_stage.Customer_Group_Id__c = Str_corrected_value;
                        }
                        else
                        {
                            rec_customer_info_stage.Customer_Group_Id__c = inputvalues[2];
                        }
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Group Id is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                           else{
	                        rec_customer_info_stage.Exception__c += ', '+'Group Id is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                           }
                   }
                   system.debug('------------------inputvalues[3]------'+inputvalues[3]);
                   if(inputvalues[3]!=NULL && inputvalues[3]!=' ' && inputvalues[3]!='')
                   {
                        //system.debug('inputvalues[3] !=NULL========================'+inputvalues[3]);
                        if( inputvalues[0].contains('\'') )
                        {
                            String Str_corrected_value = inputvalues[3].substring(1, inputvalues[3].length());
                            rec_customer_info_stage.Bill_to_Account__c = Str_corrected_value;
                        }
                        else
                        {
                            rec_customer_info_stage.Bill_to_Account__c = inputvalues[3];
                        }
                   }
                  /* else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Bill to Account is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
		                        rec_customer_info_stage.Exception__c += ', '+'Bill to Account is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }*/
                   system.debug('------------------inputvalues[4]------'+inputvalues[4]);
                   if(inputvalues[4]!=NULL && inputvalues[4]!=' ' && inputvalues[4]!='')
                   {
                          //system.debug('inputvalues[4] !=NULL========================'+inputvalues[4]);
                          rec_customer_info_stage.Owner_Ship__c = inputvalues[4];
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[5]------'+inputvalues[5]);
                   if(inputvalues[5]!=NULL && inputvalues[5]!=' ' && inputvalues[5]!='')
                   {
                          //system.debug('inputvalues[5] !=NULL========================'+inputvalues[5]);
                          rec_customer_info_stage.Sole_Sourced__c = inputvalues[5];
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[6]------'+inputvalues[6]);
                   if(inputvalues[6]!=NULL && inputvalues[6]!=' ' && inputvalues[6]!='')
                   {
                          //system.debug('inputvalues[6] !=NULL========================'+inputvalues[6]);
                          rec_customer_info_stage.Contact_Name__c = inputvalues[6];
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[7]------'+inputvalues[7]);
                   if(inputvalues[7]!=NULL && inputvalues[7]!=' ' && inputvalues[7]!='')
                   {
                        //system.debug('inputvalues[7] !=NULL========================'+inputvalues[7]);
                        try
                        {
                            rec_customer_info_stage.Planned_Review_Date__c = Date.parse(inputvalues[7]); //assigning values
                        }
                        catch(Exception e)
                        {
                            if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c = String.valueof(e)+' in Date';
                                rec_customer_info_stage.Status__c='Exception';
                                rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                            else
                            {
                                rec_customer_info_stage.Exception__c += ','+String.valueof(e)+'in Date';
                                rec_customer_info_stage.Status__c='Exception';
                                rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }   
                        }
                   }
                   else
                   {
                       if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Planned Review Date is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                        else{
	                        rec_customer_info_stage.Exception__c +=', '+'Planned Review Date is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                   }
                   system.debug('------------------inputvalues[8]------'+inputvalues[8]);
                   if(inputvalues[8]!=NULL && inputvalues[8]!=' ' && inputvalues[8]!='')
                   {
                        //system.debug('inputvalues[8] !=NULL========================'+inputvalues[8]);
                        rec_customer_info_stage.Notification_Flag__c = Boolean.valueOf(inputvalues[8]);
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[9]------'+inputvalues[9]);
                   if(inputvalues[9]!=NULL && inputvalues[9]!=' ' && inputvalues[9]!='')
                   {
                        //system.debug('inputvalues[9] !=NULL========================'+inputvalues[9]);
                        rec_customer_info_stage.DND_Financial_Information__c = inputvalues[9];
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                               rec_customer_info_stage.Exception__c='DND Financial Information is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
		                        rec_customer_info_stage.Exception__c += ', '+'DND Financial Information is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }
                   system.debug('------------------inputvalues[10]------'+inputvalues[10]);
                   if(inputvalues[10]!=NULL && inputvalues[10]!=' ' && inputvalues[10]!='')
                   {
                        //system.debug('inputvalues[10] !=NULL========================'+inputvalues[10]);
                        //rec_customer_info_stage.Fiscal_Year_End__c = inputvalues[10];
                   }
                   else
                   {
                        
                   }
                   system.debug('------------------inputvalues[11]------'+inputvalues[11]);
                   if(inputvalues[11]!=NULL && inputvalues[11]!=' ' && inputvalues[11]!='')
                   {
                        //system.debug('inputvalues[11] !=NULL========================'+inputvalues[11]);
                    //  rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[11];
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[12]------'+inputvalues[12]);
                   if(inputvalues[12]!=NULL && inputvalues[12]!=' ' && inputvalues[12]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   system.debug('------------------inputvalues[13]------'+inputvalues[13]);
                   if(inputvalues[13]!=NULL && inputvalues[13]!=' ' && inputvalues[13]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[14]!=NULL && inputvalues[14]!=' ' && inputvalues[14]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[15]!=NULL && inputvalues[15]!=' ' && inputvalues[15]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[16]!=NULL && inputvalues[16]!=' ' && inputvalues[16]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[17]!=NULL && inputvalues[17]!=' ' && inputvalues[17]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[18]!=NULL && inputvalues[18]!=' ' && inputvalues[18]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[19]!=NULL && inputvalues[19]!=' ' && inputvalues[19]!='')
                   {
                        //system.debug('inputvalues[19] !=NULL========================'+inputvalues[19]);
                        //rec_customer_info_stage.Email_address__c = inputvalues[19];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[20]!=NULL && inputvalues[20]!=' ' && inputvalues[20]!='')
                   {
                        //system.debug('inputvalues[20] !=NULL========================'+inputvalues[20]);
                        //rec_customer_info_stage.Phone_Number__c = inputvalues[20];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[21]!=NULL && inputvalues[21]!=' ' && inputvalues[21]!='')
                   {
                        //system.debug('inputvalues[21] !=NULL========================'+inputvalues[21]);
                        //rec_customer_info_stage.City__c = inputvalues[21];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[22]!=NULL && inputvalues[22]!=' ' && inputvalues[22]!='')
                   {
                        //system.debug('inputvalues[22] !=NULL========================'+inputvalues[22]);
                        rec_customer_info_stage.City__c = inputvalues[22];
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                               rec_customer_info_stage.Exception__c='City is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
	                        rec_customer_info_stage.Exception__c +=', '+'City is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }
                   if(inputvalues[23]!=NULL && inputvalues[23]!=' ' && inputvalues[23]!='')
                   {
                        rec_customer_info_stage.Postal_Code__c = inputvalues[23];
                   }
                   else
                   {
                    
                   }
                    if(inputvalues[24]!=NULL && inputvalues[24]!=' ' && inputvalues[24]!='')
                   {
                        rec_customer_info_stage.Street_Name_Address_Line_1__c = inputvalues[24];
                   }
                   else
                   {
                       if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Street Name Address Line 1 is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                        else{
	                        rec_customer_info_stage.Exception__c += ', '+'Street Name Address Line 1 is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                   }
                   system.debug('------------------inputvalues[25]------'+inputvalues[25]);
                   if(inputvalues[25]!=NULL && inputvalues[25]!=' ' && inputvalues[25]!='')
                   {
                   if(mapcattypes.containsKey(inputvalues[25]))
			         {
			          rec_customer_info_stage.Customer_Category__c = mapcattypes.get(inputvalues[25]);
			          system.debug('------------------v2 name------'+inputvalues[25]);
			         
			         }
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c='Category is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                          else{
		                        rec_customer_info_stage.Exception__c +=', '+'Category is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                          }
                   }
                   system.debug('------------------inputvalues[26]------'+inputvalues[26]);
                   if(inputvalues[26]!=NULL && inputvalues[26]!=' ' && inputvalues[26]!='')
                   {
			        if(mapregions.containsKey(inputvalues[26]))
			         {
			          rec_customer_info_stage.Customer_Region__c = mapregions.get(inputvalues[26]);
			          system.debug('------------------v2 name------'+inputvalues[26]);
			         
			         }
                   }
                    else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                 rec_customer_info_stage.Exception__c='Region is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
		                        rec_customer_info_stage.Exception__c+=', '+'Region is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }
                   system.debug('------------------inputvalues[27]------'+inputvalues[27]);
                   if(inputvalues[27]!=NULL && inputvalues[27]!=' ' && inputvalues[27]!='')
                   {
			          if(mapzones.containsKey(inputvalues[27]))
			         {
			          rec_customer_info_stage.Zone__c = mapzones.get(inputvalues[27]);
			          system.debug('------------------v2 name------'+inputvalues[27]);
			         
			         }
                   }
                    else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                 rec_customer_info_stage.Exception__c='Zone is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                       else{
	                        rec_customer_info_stage.Exception__c+=', '+'Zone is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                       }
                   }
                   system.debug('------------------inputvalues[28]------'+inputvalues[28]);
                   if(inputvalues[28]!=NULL && inputvalues[28]!=' ' && inputvalues[28]!='')
                   {
			          if(mapsubzones.containsKey(inputvalues[28]))
			         {
			          rec_customer_info_stage.Sub_Zone__c = mapsubzones.get(inputvalues[28]);
			          system.debug('------------------v2 name------'+inputvalues[28]);
			         
			         }
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                             rec_customer_info_stage.Exception__c='Sub Zone is mandatory';
		                     rec_customer_info_stage.Status__c='Exception';
		                     rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
		                        rec_customer_info_stage.Exception__c+=', '+'Sub Zone is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }
                   system.debug('------------------inputvalues[29]------'+inputvalues[29]);
                   if(inputvalues[29]!=NULL && inputvalues[29]!=' ' && inputvalues[29]!='')
                   {
			          if(mapcountries.containsKey(inputvalues[29]))
			         {
			          rec_customer_info_stage.Country__c = mapcountries.get(inputvalues[29]);
			          system.debug('------------------v2 name------'+inputvalues[29]);
			         
			         }
                   }
                   else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                             	rec_customer_info_stage.Exception__c='Country is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                         else{
		                        rec_customer_info_stage.Exception__c+=', '+'Country is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                         }
                   }
                   system.debug('------------------inputvalues[30]------'+inputvalues[30]);
                   if(inputvalues[30]!=NULL && inputvalues[30]!=' ' && inputvalues[30]!='')
                   {
			          if(mapstate.containsKey(inputvalues[30]))
			         {
			          rec_customer_info_stage.State__c = mapstate.get(inputvalues[30]);
			          system.debug('------------------v2 name------'+inputvalues[30]);
			         
			         }
                   }
                   else
                   {
                       if(rec_customer_info_stage.Exception__c==NULL)
                            {
                             rec_customer_info_stage.Exception__c='State is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                        else{
	                        rec_customer_info_stage.Exception__c+=', '+'State is mandatory';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                   }
                   system.debug('------------------inputvalues[31]------'+inputvalues[31]);
                   if(inputvalues[31]!=NULL && inputvalues[31]!=' ' && inputvalues[31]!='')
                   {
			         string str = inputvalues[31].trim();
			          if(mapanalysts.containsKey(str))
			         {
			          rec_customer_info_stage.Analyst__c = mapanalysts.get(str);
			          system.debug('------------------v2 name------'+inputvalues[31]);			         
			         }
                   }
                   else
                   {
                       if(rec_customer_info_stage.Exception__c==NULL)
                            {
                            	rec_customer_info_stage.Exception__c='Analyst is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                        else{
		                        rec_customer_info_stage.Exception__c+=', '+'Analyst is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                   }
                    system.debug('------------------inputvalues[32]------'+inputvalues[32]+'-');
                    system.debug('------------------mapcollectors------'+mapcollectors);	
                   if(inputvalues[32]!=NULL && inputvalues[32]!=' ' && inputvalues[32]!='')
                   {
                   	system.debug('------ccccc-1-');
                   	string str = inputvalues[32].trim();
                   	system.debug('------str-1-'+str+'=====');
			         if(mapcollectors.containskey(str))
			         {
			         	system.debug('------ccccc-2-');
			          rec_customer_info_stage.Collectors__c = mapcollectors.get(str);
			          system.debug('------------------v2 name------'+inputvalues[32]);			         
			         }
                   }
			        else
                   {
                        if(rec_customer_info_stage.Exception__c==NULL)
                            {
                            	rec_customer_info_stage.Exception__c='Collector is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                        else{
		                        rec_customer_info_stage.Exception__c+=', '+'Collector is mandatory';
		                        rec_customer_info_stage.Status__c='Exception';
		                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                   } 
                   lststage.add(rec_customer_info_stage);
         
       // system.debug('-----------------------------------'+vstring[0]);
       // system.debug('-----------------------------------'+vstring[1]);
       // system.debug('-----------------------------------'+vstring[2]);
        }
        try{
           insert lststage;
           
         }catch(DMLException er) {
            system.debug('The following exception has occurred: ' + er.getMessage());               
               /* ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,er.getMessage());
                ApexPages.addMessage(myMsg1);*/
                
         }
     
      
    }
    
    	    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {    
			    system.debug('***** Showed Up Here clsAdddataFromCSV ' + skipHeaders + contents );
			     List<List<String>> allFields = new List<List<String>>();
    
				    transient List<String> lines = new List<String>();
				    try {
				        lines = contents.split('\n');
				    } catch (System.ListException e) {
				        //system.debug('***** Limits exceeded?' + e.getMessage());
				    }
				    //system.debug('Line num is'+lines.size());
				    Integer num = 0;
				    for(String line : lines) {
				        // check for blank CSV lines (only commas)
				        if (line.replaceAll(',','').trim().length() == 0) break;
				        
				        List<String> fields = line.split(',');
				        List<String> cleanFields = new List<String>();
				        String compositeField;
				        Boolean makeCompositeField = false;
				        for(String field : fields) {
				            if (field.startsWith('"') && field.endsWith('"')) {
				                cleanFields.add(field.replaceAll('DBLQT','"'));
				            } else if (field.startsWith('"')) {
				                makeCompositeField = true;
				                compositeField = field;
				            } else if (field.endsWith('"')) {
				                compositeField += ',' + field;
				                cleanFields.add(compositeField.replaceAll('DBLQT','"'));
				                makeCompositeField = false;
				            } else if (makeCompositeField) {
				                compositeField += ',' + field;
				            } else {
				                //cleanFields.add(field.replaceAll('DBLQT','"'));
				                cleanFields.add(field);
				            }
				        }
				        
				        allFields.add(cleanFields);
				    }
				    if (skipHeaders) allFields.remove(0);
				    return allFields;
   		}
    	
    
    }