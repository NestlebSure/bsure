/************************************************************************************************       
Controller Name         : BSureC_CustomerInformationReport       
Date                    : 12/20/2012        
Author                  : Neha Jaiswal       
Purpose                 : To generate the reports for customer information on different search criteria    
Change History          : Date           Programmer                     Reason       
-----------------------------   -------------------   --------------------------------------------      
                          12/20/2012     		 Neha Jaiswal         Initial Version
                          11/04/2014(mm/dd/yy)	 Vidya Sagar H		  Added Collector filter for reports
**************************************************************************************************/
global with sharing class  BSureC_CustomerInformationReport
{
    public string strZoneId{get;set;}//to hold zone Id
    public list<selectOption> lstZoneOptions{get;set;}//to pass list os options of zone
    public string strSubzoneId{get;set;}//to hold subzone id
    public list<selectOption> lstSubZoneOptions {get;set;}//to pass list of options of subzones
    public string strCountryId{get;set;}//to hold country id
    public list<selectoption> lstCoutryOptions{get;set;}//to pass list of options of country
    public string strCustomerId{get;set;}//to hold customer id
    public list<selectOption> lstCustomerOptions{get;set;}//to pass list of customers
    public string strAnalystId{get;set;}//to hold Analyst id
    public list<selectOption> lstAnalystOptions{get;set;}//to pass list of Analyst
    list<BSureC_Customer_Basic_Info__c>  lstCustomerSelected{get;set;}// list to display the searched records of customer
    public ApexPages.StandardSetController objCustomerController{get; set;}
    public list<BSureC_Customer_Basic_Info__c> objCustomerList
    {
        get  
        {  
            if(objCustomerController != null)  
                return (List<BSureC_Customer_Basic_Info__c>)objCustomerController.getRecords();  
            else  
                return null ;  
        }
        set;
    }
    public list<CommonUserclass> ObjWorkList{get;set;}//common wrapper class to display differnt  objects fields value
    public string strMainQuery{get;set;}//to fetch the details on search criteria
    public list<CommonUserclass> FinalWorkList{get;set;}//instance of wrapper class
    list<BSureC_Assigned_Buyers__c > listAssignedUser{get;set;}//to assign the buyers list for particular customer
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public integer totallistsize{get;set;}//used for pagenation
    public integer counter=0;  //keeps track of the offset
    public Integer NlistSize {get;set;}//used for pagination
    //public Integer pageNumber;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public list<BSureC_Customer_Basic_Info__c> lstCustPagination{get;set;}
    Public List<BSureC_Customer_Basic_Info__c> lstCustomer{get;set;}
    public integer pageSize{get;set;} //sets the page size or number of rows
    public integer total_size{get;set;}//for pagenation
    public string customerId{get;set;}//to pass customer id
    public Boolean renSearch{get;set;}
    public transient list<BSureC_Credit_Data_Publish__c> listPublish{get;set;}
    public Boolean blnExport = false;
    public string sortDirection = 'ASC';
    public string sortExp = 'Customer_Name__c';
    public string loginUserProfile{get;set;}
    public Boolean displayPanel{get;set;}
    transient public List<List<BSureC_Customer_Basic_Info__c >> wrapperCollection  {get; private set;}
    public string reportId {get;set;}
    public string sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            if(value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
     
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }    
    
    public Pagereference SortData()
    {
         fetchDetails();
         return null;
    }
    
    // Starting....... Vidya Sagar
     public string  strCollectorNames{get;set;}//to get id of  selected collector
     public String[] strCollectorProfiles=new String[] {'BSureC_Collector'};//Profiles to be allowed
     public String[] strCollectorRole=new String[] {'Collector'};//Roles to be allowed
     
     public list<selectOption> getlstCollectorNames()
     {
     	list<selectOption> options = new list<selectOption>();
     	options.add(new selectoption('ALL','ALL'));
     	list<User> lstCollectorUser=[Select Id, Name from User where Profile.Name IN:strCollectorProfiles and IsActive=true and UserRole.Name IN:strCollectorRole order By Name Asc];
     	if(lstCollectorUser !=null && lstCollectorUser.size()>0)
     	{
     		for(User u:lstCollectorUser){
     			options.add(new selectoption(u.Name,u.Name));     		
     		}
     	}
     	return options;
     	
     }
     // Ending...... Vidya Sagar
    /// <summary> 
   /// constructor: to display the default list of customers when a user logs in
   /// </summary>
   /// <returns> default list.
    public BSureC_CustomerInformationReport()
    {  
           displayPanel=false;
           renSearch=true;
           totallistsize = 0;
           total_size = 0;
           string userName = String.valueOf(UserInfo.getUserId());
           pageSize = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));
           List<BSureC_AdditionalAttributes__c> BSAobj =new List<BSureC_AdditionalAttributes__c>();
           BSAobj=[SELECT BSureC_Country__c,BSureC_Sub_Zone__c,BSureC_Zone__c,Country_Name__c,Sub_Zone_Name__c,User__c,Zone_Name__c,id,Name,BSureC_Region__c FROM BSureC_AdditionalAttributes__c where User__c=:userName limit 1];
           Profile objCurProfile=[SELECT name FROM Profile WHERE id =:userInfo.getProfileId()];
           loginUserProfile = objCurProfile.name;
           if(loginUserProfile.equalsIgnoreCase('System Administrator'))
            {
                strZoneId='ALL';
                strSubzoneId='ALL';
                strCountryId='ALL';
                
            }else{
           if(BSAobj != null && BSAobj.size()>0)
               {
                   strZoneId=BSAobj[0].BSureC_Zone__c;
                   strSubzoneId=BSAobj[0].BSureC_Sub_Zone__c;
                   strCountryId=BSAobj[0].BSureC_Country__c;
               }
            }
                   lstCustomerSelected=new list<BSureC_Customer_Basic_Info__c>();
                   listAssignedUser=new list<BSureC_Assigned_Buyers__c>();
                   listPublish=new list<BSureC_Credit_Data_Publish__c>();
                   BSureC_CommonUtil.blnflag=true;
                   lstZoneOptions=BSureC_CommonUtil.getZones();
                   lstSubZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
                   lstCoutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
                   //fetchDetails();
                   getAnalystOptions();
            totallistsize = lstCustomerSelected.size();
            reportId = BSureC_CommonUtil.getConfigurationValues('BSureC_AllCustomerReportId').get(0);
    }
        
     /// <summary>
    /// getZOnesOptions(): to display the dropdown list for zones from BSureC_commonutil.
    /// </summary>
   /// <returns> zones option list
    public pageReference getZOnesOptions()
        {  
            lstZoneOptions=new list<selectOption>();
            BSureC_CommonUtil.blnflag=true;
            lstZoneOptions=BSureC_CommonUtil.getZones();
            getSubzonesoptions();
            //getCountriesOptions();  
           
            return null;
       }
    
     /// <summary>
    /// getSubzonesoptions(): to display the dropdown list for subzones from BSureC_commonutil.
    /// </summary>
   /// <returns> subzones option list
    public pageReference getSubzonesoptions()
        {
             
            strSubzoneId='ALL';
            strCountryId='ALL';
            strAnalystId='ALL';
            lstSubZoneOptions=new list<selectoption>();
            BSureC_CommonUtil.blnflag=true;
            lstSubZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
            //system.debug('-----lstSubZoneOptions---'+lstSubZoneOptions);
            getCountriesOptions(); 
            getAnalystOptions();
           
            return null;  
        }
    
    
     /// <summary>
    /// getCountriesOptions(): to display the dropdown list for countries from BSureC_commonutil.
    /// </summary>
   /// <returns> country option list
     public pageReference getCountriesOptions()
         {
             strCountryId='ALL';  
             strAnalystId='ALL';
             lstCoutryOptions=new list<selectOption>();
             lstCoutryOptions=BSureC_CommonUtil.getCountries(strZoneId,strSubzoneId);
              getAnalystOptions();
            // //system.debug('-----lstCoutryOptions---'+lstCoutryOptions);
             return null;
          }
          
      
      public pageReference getAnalystOptions()
         {
             strAnalystId='ALL';
             lstAnalystOptions=new list<selectOption>();
             lstAnalystOptions=BSureC_CommonUtil.getAnalystlistsSearch(strZoneId,strSubzoneId,strCountryId);
             return null;
          }
          
    public pageReference ExportCSV()
    {
        blnExport = true;
        
        //fetchDetailsforExport();
        //pageReference pRef = new pageReference('/apex/BSureC_CustomerInformationReportExport');
        
        pageReference pRef = new pageReference('/'+reportId);
        return pRef;
    }
    
    /// <summary>
    /// fetchDetailsforExport(): to export the records of cutomer based on search criteria
    /// </summary>
    /// <returns> searched record for customers     
    public pageReference  fetchDetailsforExport()
    {
        displayPanel=true;
        blnExport=false;
        lstCustomer =new list<BSureC_Customer_Basic_Info__c>();
        strMainQuery= 'SELECT Customer_Name__c,Customer_Group_Id__c,City__c,State_Province__c,State__c,Analyst_Name__c,Collector_Name__c,'+
                      'Credit_Account__c,Zone__c,Customer_Category_Name__C,Sub_Zone__c,Customer_Region_Name__c,Customer_Region__c,Country__c,Zone_Name__c,Sub_Zone_Name__c,Bill_to_Account__c,Risk_Category__c,Customer_Category__c,Credit_Limit__c from BSureC_Customer_Basic_Info__c  where Id != null ';   
               
        if(strZoneId!=null && strZoneId!='ALL')
        {
            strMainQuery+=' AND Zone__c =:strZoneId';
        }
        if(strSubZoneId!=null && strSubZoneId!='ALL')
        {
            strMainQuery+= ' and Sub_Zone__c=:strSubZoneId';
        }
        if(strCountryId!=null && strCountryId!='ALL')
        {
            strMainQuery+=' and Country__c=:strCountryId';
        }
        if(strCustomerId!=null && strCustomerId != '' && strCustomerId!='ALL' )
        {
            strMainQuery+=' and Customer_Name__c=:strCustomerId';
        }
        if(strAnalystId!= null && strAnalystId!='' && strAnalystId!='ALL' )
        {
            strMainQuery+=' and Analyst__c=:strAnalystId';
        }
        strMainQuery+= ' ORDER BY  ' + sortExpression + ' ' + sortDirection + ' LIMIT 8000';
         
         //lstCustomer=database.query(strMainQuery); 
         // Starting ......... Vidya Sagar 
         if(strCollectorNames != null && strCollectorNames!='' && strCollectorNames!='ALL')
        {
        	//system.debug('++++++++++++++++++++++'+strCollectorNames);
        	list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strMainQuery);
        	//system.debug('++++++++++++++++++++++'+lstCrdInc.size());
        	 for(integer i=0;i<lstCrdInc.size();i++)
        	  {
        	  	string Collectorname = lstCrdInc[i].Collector_Name__c;
        	  	//system.debug('++++++++++++++++++++++'+Collectorname);
        	  	if(Collectorname != null && Collectorname !=''){
        	    BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
        	    c = lstCrdInc[i];
        	    if(c.Collector_Name__c.contains(strCollectorNames)){
        	    	//system.debug('++++++++required++++++++++++++'+c.Collector_Name__c);
        	    	lstCustomer.add(c);
        	    }
        	  	}
        	  }
        	}
        	else{
           lstCustomer = database.query(strMainQuery);
        
        	} 
          // Ending ........... Vidya Sagar   	
         wrapperCollection=collectionOfCollections(lstCustomer);        
               
        return null;
    }    
    
    /// <summary>
    /// To Collection Of Collection
    /// Converts a collection of Object to collection of collection with parent collection holding upto 1000 items
    /// </summary>
     public List<List<BSureC_Customer_Basic_Info__c >> collectionOfCollections(List<BSureC_Customer_Basic_Info__c > coll){ 
        List<List<BSureC_Customer_Basic_Info__c >> mainList = new List<List<BSureC_Customer_Basic_Info__c >>();
        List<BSureC_Customer_Basic_Info__c > innerList = null;
        Integer idx = 0;
        
        if(coll!=null)
        {
        	//system.debug('coll.........'+coll.size());
            for(BSureC_Customer_Basic_Info__c  obj:coll){
            	
                if (Math.mod(idx++, 1000) == 0 ) 
                {
                    innerList = new List<BSureC_Customer_Basic_Info__c >();
                    mainList.add(innerList);
                    //system.debug('mainList.........'+mainList);
                }
                
                innerList.add(obj);           
            }
        }
               
        return mainList;
    }    
    /// <summary>
    /// fetchDetails(): to display the records of cutomer based on dearch criteria
    /// </summary>
    /// <returns> serched record for customers     
    public pageReference  fetchDetails()
    {
        displayPanel=true;
        blnExport=false;
        ObjWorkList=new list<CommonUserclass>();
        strMainQuery= 'SELECT Customer_Name__c,Customer_Group_Id__c,City__c,State_Province__c,State__c,Analyst_Name__c,Collector_Name__c,'+
                      'Credit_Account__c,Zone__c,Customer_Category_Name__C,Sub_Zone__c,Customer_Region_Name__c,Customer_Region__c,Country__c,Zone_Name__c,Sub_Zone_Name__c,Bill_to_Account__c,Risk_Category__c,Customer_Category__c,Credit_Limit__c from BSureC_Customer_Basic_Info__c  where Id != null ';   
               
        if(strZoneId!=null && strZoneId!='ALL')
        {
            strMainQuery+=' AND Zone__c =:strZoneId';
        }
        if(strSubZoneId!=null && strSubZoneId!='ALL')
        {
            strMainQuery+= ' and Sub_Zone__c=:strSubZoneId';
        }
        if(strCountryId!=null && strCountryId!='ALL')
        {
            strMainQuery+=' and Country__c=:strCountryId';
        }
        if(strCustomerId!=null && strCustomerId != '' && strCustomerId!='ALL' )
        {
            strMainQuery+=' and Customer_Name__c=:strCustomerId';
        }
        if(strAnalystId!= null && strAnalystId!='' && strAnalystId!='ALL' )
        {
            strMainQuery+=' and Analyst__c=:strAnalystId';
        }
       
        strMainQuery+= ' ORDER BY  ' + sortExpression + ' ' + sortDirection + ' LIMIT 8000';
        // Starting ....... Vidya Sagar
         if(strCollectorNames != null && strCollectorNames!='' && strCollectorNames!='ALL')
        {
        	//system.debug('++++++++++++++++++++++'+strCollectorNames);
        	list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strMainQuery);
        	list<BSureC_Customer_Basic_Info__c> lstCustInfo = new list<BSureC_Customer_Basic_Info__c>();
        	//system.debug('++++++++++++++++++++++'+lstCrdInc.size());
        	 for(integer i=0;i<lstCrdInc.size();i++)
        	  {
        	  	string Collectorname = lstCrdInc[i].Collector_Name__c;
        	  	//system.debug('++++++++++++++++++++++'+Collectorname);
        	  	if(Collectorname != null && Collectorname !=''){
        	    BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
        	    c = lstCrdInc[i];
        	    if(c.Collector_Name__c.contains(strCollectorNames)){
        	    	//system.debug('++++++++required++++++++++++++'+c.Collector_Name__c);
        	    	lstCustInfo.add(c);
        	    }
        	  	}
        	  }
        	  objCustomerController = new ApexPages.StandardSetController(lstCustInfo);
        	}
        	else{
           objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strMainQuery));
        
        	} 
          // Ending ........ Vidya Sagar       
        //objCustomerController = new ApexPages.StandardSetController(Database.getQueryLocator(strMainQuery));  
        pageSize = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));
        objCustomerController.setPageSize(pageSize); 
        totallistsize = objCustomerController.getResultSize();
        pagenation();
        if(totallistsize > 0)
        {
            renSearch=false;
        }
        
        return null;
    }
           
              //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    
    public pagereference pagenation() 
    {   
        displayPanel=true;
        string strQuery;
        set<id> userId=new set<id>();
        if(blnExport==true)
        {
            for(BSureC_Customer_Basic_Info__c CustomerInfo11:objCustomerList)
            {
                userId.add(CustomerInfo11.id);
            }
        }else
        {
            for(BSureC_Customer_Basic_Info__c CustomerInfo1:objCustomerList)
            {
                userId.add(CustomerInfo1.id);
            }
        }
                
        listPublish=[SELECT Discount__c,of_Limit__c,Last_Review__c,Prompt__c,Risk_Category__c,Slow__c,Average_Days_Slow__c,BSureC_Customer_Basic_Info__c,CE__c,Credit_Limit__c,
                     Customer_Name__c,Exposure__c,Group_Id__c, Last_Sale_Date__c from BSureC_Credit_Data_Publish__c where BSureC_Customer_Basic_Info__c in :userId ] ;   
        listAssignedUser=[Select Buyer_Name__c,Customer_Id__c,Customer_Name__c,User_Id__c,User_Name__c from BSureC_Assigned_Buyers__c where Customer_Id__c =:userId];
        
        Map<string,BSureC_Credit_Data_Publish__c> mapObjPublish =new Map<string,BSureC_Credit_Data_Publish__c>();
        map<string,string> MapObjAssignedUser=new map<string,string>();
        set<id> strid=new set<id>();
       
        for(BSureC_Assigned_Buyers__c struseridlist: listAssignedUser)
        {
            strid.add(struseridlist.User_Id__c);  
        }
     
        for(BSureC_Credit_Data_Publish__c DataPublish :listPublish)
        {
            mapObjPublish.put(DataPublish.BSureC_Customer_Basic_Info__c,DataPublish);
        }
        list<user> lstUser=[select name from user where id=:strid ];
        map<string,string> MapObjUserName=new map<string,string>();
     
        for(user u:lstUser){
            MapObjUserName.put(u.id,u.name);
        }    
     
        for(BSureC_Assigned_Buyers__c DataAssignedUser: listAssignedUser)
        {   
            if(MapObjAssignedUser.get(DataAssignedUser.Customer_Id__c) !=null ){
                string strBuyer=MapObjAssignedUser.get(DataAssignedUser.Customer_Id__c);
                if(strBuyer!=null && strBuyer!=''){
                    
                strBuyer+='; '+MapObjUserName.get(DataAssignedUser.User_Id__c);
                }else{
                    strBuyer=MapObjUserName.get(DataAssignedUser.User_Id__c);
                }
                MapObjAssignedUser.put(DataAssignedUser.Customer_Id__c,strBuyer);
            }else{
                
                MapObjAssignedUser.put(DataAssignedUser.Customer_Id__c,MapObjUserName.get(DataAssignedUser.User_Id__c));
            }
        }
        ObjWorkList=new list<CommonUserclass>();
        for(BSureC_Customer_Basic_Info__c CustomerInfo : objCustomerList)
        {
            CommonUserclass objWorkListUser=new CommonUserclass();
            objWorkListUser.customerId=CustomerInfo.Id;
            objWorkListUser.Billtos=CustomerInfo.Bill_to_Account__c;
            objWorkListUser.CreditAccount=CustomerInfo.Credit_Account__c;
            objWorkListUser.GroupNumber=CustomerInfo.Customer_Group_Id__c;
            objWorkListUser.Name=CustomerInfo.Customer_Name__c;
            objWorkListUser.strCollector=CustomerInfo.Collector_Name__c;// added by Vidya Sagar
            if(blnExport==true && CustomerInfo.Risk_Category__c!=null && CustomerInfo.Risk_Category__c!='')
            {
                objWorkListUser.RiskCategory='\''+CustomerInfo.Risk_Category__c;
            }else{
                objWorkListUser.RiskCategory=CustomerInfo.Risk_Category__c;
            }
            // objWorkListUser.RiskCategory=CustomerInfo.Risk_Category__c;
            objWorkListUser.strcityLocation=CustomerInfo.City__c;
            objWorkListUser.strstate=CustomerInfo.State_Province__c;
            objWorkListUser.Region=CustomerInfo.Customer_Region_Name__c;
            objWorkListUser.Lineofbusiness=CustomerInfo.Customer_Category_Name__C;
            objWorkListUser.CreditLimit=CustomerInfo.Credit_Limit__c;
            objWorkListUser.Usersassignedtothecustomer=MapObjAssignedUser.get(CustomerInfo.id);
            BSureC_Credit_Data_Publish__c DataPublish1= mapObjPublish.get(CustomerInfo.Id);
            if(DataPublish1!=null)
            {
              //  objWorkListUser.CreditLimit=DataPublish1.Credit_Limit__c;
                objWorkListUser.LastSale=DataPublish1.Last_Sale_Date__c;
            }
            ObjWorkList.add(objWorkListUser); 
             //system.debug('ObjWorkListObjWorkList'+ObjWorkList.size()); 
        }
        return null;
           
    }
        public pageReference getCustomer()
        {
            String id=Apexpages.currentPage().getParameters().get('customerId');
            pagereference pg=new pagereference('/'+id);
            return pg;
        }
        
        
     public class CommonUserclass
       {
         public string Name {get;set;}
         public string GroupNumber{get;set;}
         public string strstate{get;set;}
         public string strcityLocation{get;set;}
         public string CreditAccount{get;set;}
         public string Billtos{get;set;}
         public string RiskCategory{get;set;}
         public double CreditLimit{get;set;}
         public Date LastSale{get;set;}
         public string Lineofbusiness{get;set;}
         public string Region{get;set;}
         public string Usersassignedtothecustomer{get;set;}
         public string customerId{get;set;}
         public string strCollector{get;set;}
     }    
       /// <summary>
   /// This method is for pagination, if atleast one record found then passing 1 to the pagination method
   /// <param name="PageSize"></param>
   /// <returns>Integer</returns>  
   /// </summary>   
      public PageReference FirstbtnClick() { //user clicked beginning
         
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        pagenation();
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
           }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
       public PageReference previousBtnClick() { //user clicked previous button
          pageData(pageNumber - 1);
          BindData(pageNumber - 1);
          pagenation();
          return null;
       }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
       public PageReference nextBtnClick() { //user clicked next button
         //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pagenation();
        pageData(pageNumber + 1);
        
        return null;
       }
       
      
   // <summary>
   // Below method fires when user clicks on previous button of pagination.
   // </summary>
   // <returns>pagereference</returns>
       public PageReference LastbtnClick() { //user clicked end
          BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        pagenation();
        return null;
       }
       
       
   /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
       public Boolean getPreviousButtonEnabled() {
          //this will disable the previous and beginning buttons
          return !(pageNumber > 1);
       }
       
       
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.  
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
       public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
       if (totallistsize ==0) 
            return true;
        else
         //system.debug('totallistsize**************'+totallistsize);   
        
            return ((pageNumber * pageSize) >= totallistsize);      
          }
     
    
     
       public Integer getPageSize() {
         //system.debug('pageSize********pageSize******'+pageSize);
          return pageSize;
       }
     
     
     
       public Integer getPageNumber() {
        //system.debug('pageNumber********pageNumber******'+pageNumber);
            return pageNumber; 
       }
       
      public pagereference cancel()
      {
        pageReference pRef = new pageReference('/apex/BSureC_Reports');
        return pRef;
      } 
    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>    
    /// </summary>
    public Integer getTotalPageNumber() 
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
            //system.debug('totalPageNumber********pageNumber******'+totalPageNumber);
        }
        return totalPageNumber;
    } 
    
    public void BindData(Integer newPageIndex)
    {
       
        try
        {
             lstCustPagination = new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            if(lstCustomerSelected != NULL)
            {
                for(BSureC_Customer_Basic_Info__c b : lstCustomerSelected)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                         lstCustPagination.add(b);// here adding files list
                    }
                }
            }
            pageNumber = newPageIndex;
            NlistSize = lstCustPagination.size();
           // pagenation();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
        
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
     /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
             
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstCustPagination = new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(BSureC_Customer_Basic_Info__c a : lstCustomerSelected)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstCustPagination.add(a);// here adding the folders list 
                            
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstCustPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }

    public Boolean hasNext  
    {
        get  
        {
            return objCustomerController.getHasNext();  
        }  
        set;  
    }  
   
    public Boolean hasPrevious  
    {
        get  
        {  
            return objCustomerController.getHasPrevious();  
        }  
        set;  
    }  
   
    //Page number of the current displaying records  
    public Integer pageNumber  
    {  
        get  
        {  
            return objCustomerController.getPageNumber();  
        }  
        set;  
    }  
  
    //Returns the previous page of records  
    public void previous()  
    {  
        objCustomerController.previous();  
        pagenation();
    }  
   
    //Returns the next page of records  
    public void next()  
    {  
        objCustomerController.next();
        pagenation();  
    }  
    
    public void first()
    {
        objCustomerController.first();
        pagenation();
    }
    
    public void last()
    {
        objCustomerController.last();
        pagenation();
    }
}