/*************************************************************************************************
* Controller Name   : BSureS_ViewPerformCreditAnalysis
* Date              : 18/02/2013
* Author            : Kishorekumar  
* Purpose           : Class for Perform Credit Analysis Detail Page
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 18/02/2013            KishoreKumar                    Initial Version
* 11/08/2013            Veereandranaath.j               Code Review 
**************************************************************************************************/
global with sharing class BSureS_ViewPerformCreditAnalysis 
{
    public string strCAId{get;set;} //Variable for get Credit Analysis ID
    public Id currentUserId{get;set;} // get current user id
    public boolean visibilityNewTopic{get;set;}
    public boolean visibilityUploadFile{get;set;}
    public boolean visibilityExistsDoc{get;set;}
    public boolean visibilityEdit{get;set;}
    public boolean visibilityMirrorReview{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
    public list<BSureS_Assigned_Buyers__c> lstAssignBuyers{get;set;}
    public string strBuyerRole{get;set;}
    
    public  BSureS_ViewPerformCreditAnalysis(ApexPages.StandardController controller)
    {
        
        strCAId = Apexpages.currentPage().getParameters().get('Id'); //get the current Credit Analysis id 
        visibilityNewTopic = true;  
        visibilityUploadFile = true;
        visibilityExistsDoc = true; 
        visibilityEdit = true;
        visibilityMirrorReview =false;
        currentUserId = UserInfo.getUserId();
        if(strCAId != null && strCAId != '' && Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())
        {
            lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>([select id,Supplier_ID__c,Review_Status__c from BSureS_Credit_Analysis__c where id =:strCAId]);
        }   
        if(!BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerRole').isEmpty() )
        	strBuyerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerRole').get(0);
    	list<user> lstUser;
    	if(Schema.Sobjecttype.User.isQueryable())
        	lstUser = new list<user>([select id from User where userrole.Name =:strBuyerRole and Id=:currentUserId]);
        
        if((lstCreditAnalysis != null && lstCreditAnalysis.size() > 0) 
                && (lstUser != null && lstUser.size() > 0) && Schema.Sobjecttype.BSureS_Assigned_Buyers__c.isQueryable() )
        {
            if(lstCreditAnalysis.get(0).Supplier_ID__c != null)
            {
                lstAssignBuyers = new list<BSureS_Assigned_Buyers__c>([select id,Buyer_Id__c,Supplier_ID__c 
                                            from BSureS_Assigned_Buyers__c 
                                            where Supplier_ID__c =: lstCreditAnalysis.get(0).Supplier_ID__c 
                                            and Buyer_Id__c =: lstUser.get(0).Id]);
            }   
        } 
        
        if(lstUser != null && lstUser.size() > 0)
        {
            if(lstAssignBuyers != null && lstAssignBuyers.size() >0)
            {
                visibilityNewTopic = true;  
                visibilityUploadFile = true;
                visibilityExistsDoc = true;
            }   
            else
            {
                visibilityNewTopic = false;
                visibilityUploadFile = false;
                visibilityExistsDoc = false; 
            }
        }
        else
        {
	        if(lstCreditAnalysis.get(0).Review_Status__c != null && lstCreditAnalysis.get(0).Review_Status__c== 'Completed')
	        {
	            visibilityEdit = false;
	            visibilityMirrorReview = true;
	        } 
        }
    }
    public BSureS_Credit_Analysis__c getCreditAnalysisDetails()
    {
        BSureS_Credit_Analysis__c caObj;
        if(strCAId != null && strCAId !='' && Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable() )
        {
            caObj = [select id from BSureS_Credit_Analysis__c where id =:strCAId limit 1];
        }
        return caObj;
    }
}