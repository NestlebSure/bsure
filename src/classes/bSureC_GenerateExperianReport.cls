global class bSureC_GenerateExperianReport implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts{
	public String query;
   	global Database.QueryLocator start(Database.BatchableContext BC){
		query = 'Select id,Name,Customer_Name__c,BISFileNumber__c,Credit_Account__c,Planned_Review_Date__c From BSureC_Customer_Basic_Info__c '+
				'where BISFileNumber__c!=\'\' and BISFileNumber__c!=null '+
				'and Credit_Account__c!=\'\' and Credit_Account__c!=null and Risk_Category__c!=\'005\'';
      	return Database.getQueryLocator(query);
   	} 
	map<string,string> lstUnSuccessCustomer = new map<string,string>();
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
		for(Sobject s : scope){
			date dNow = system.today();
			date planRewDate = Date.valueOf(s.get('Planned_Review_Date__c'));
			if(planRewDate!=null && planRewDate > dNow && dNow.daysBetween(planRewDate) == 7){
				system.debug('***'+dNow.daysBetween(planRewDate));
				string custId =  String.valueOf(s.get('Id'));
				string BIN =  String.valueOf(s.get('BISFileNumber__c'));
				string credAcct =  String.valueOf(s.get('Credit_Account__c'));
      			string strExpRes = bSureC_ExperianRequest.generateExperianRpt(custId,BIN,credAcct);
      			system.debug('strExpRes===='+strExpRes);
      			if(strExpRes==null || strExpRes=='' || !strExpRes.contains(Label.BSureC_ExpSuccess)){
      				string custName = string.valueOf(s.get('Customer_Name__c'));
      				string strId = string.valueOf(s.get('Id'));
      				string[] arrMsg = new String[] {};  
      				if(strExpRes.contains(',')){
      					arrMsg = strExpRes.split(',');
      				}else{
      					arrMsg.add(strExpRes);
      				}
      				lstUnSuccessCustomer.put(strId,custName+' --> '+arrMsg[0]+';');
      			}
			}
      	}      
	}
	
   	global void finish(Database.BatchableContext BC){  
   		string batchMail = '';
   		string rptFailMail = '';
   		if(BSureC_CommonUtil.getConfigurationValues('ExperianBatchFailMail')!=null & BSureC_CommonUtil.getConfigurationValues('ExperianBatchFailMail').size()>0){
   			batchMail = BSureC_CommonUtil.getConfigurationValues('ExperianBatchFailMail').get(0);
   		}
   		if(BSureC_CommonUtil.getConfigurationValues('ExperianFailMail')!=null & BSureC_CommonUtil.getConfigurationValues('ExperianFailMail').size()>0){
   			rptFailMail = BSureC_CommonUtil.getConfigurationValues('ExperianFailMail').get(0);
   		}

 		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus  
  							From AsyncApexJob Where Id = :BC.getJobId()];  
   		if(batchMail != '' && batchMail != null && a.NumberOfErrors !=null && a.NumberOfErrors != 0){
	 		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
	 		String[] toAddresses = new String[] {};  
	 		if(batchMail.contains(',')){
	 			toAddresses = batchMail.split(',');
	 		}else{
	 			toAddresses.add(batchMail);
	 		}
	 		mail.setToAddresses(toAddresses);  
	 		mail.setSubject('BatchJobXYZ Status: ' + a.Status);  
	 		mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +  
	  		' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);  
	    
	 		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
   		}
 		if(lstUnSuccessCustomer!=null && lstUnSuccessCustomer.size()>0 && rptFailMail!='' && rptFailMail!=null){
 			Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();  
	 		String[] toAddresses1 = new String[] {};  
	 		if(rptFailMail.contains(',')){
	 			toAddresses1 = rptFailMail.split(',');
	 		}else{
	 			toAddresses1.add(rptFailMail);
	 		}
	 		mail1.setToAddresses(toAddresses1);  
	 		mail1.setSubject(Label.BSureC_ExpFail);  
	 		String strEmailBody =''; 
	        strEmailBody += '<table>';
	        strEmailBody += Label.BSureC_ExpFailCust;
	        strEmailBody += '<tr></tr>';
	        strEmailBody += '</table>';
	        strEmailBody += '<table cellspacing="2" cellpadding="0" border="1" bordercolor="#9AFEFF" >';
	        strEmailBody += '<tr><td height="30px"><font name="Verdana" ><center> <b>Customer Name</b></center></font></td>';
	        strEmailBody += '<td height="30px"><font name="Verdana"><center><b>Error Code</b> </center></font></td>';
	        strEmailBody += '<td height="30px"><font name="Verdana"><center><b>Reason</b></center></font></td></tr>';
	        for(string str : lstUnSuccessCustomer.keyset()){
	        	string[] strCustName = lstUnSuccessCustomer.get(str).split('-->');
	        	strEmailBody += '<tr><td height="21px"><font name="Verdana" ><center>'+ strCustName[0] +'</center></font></td>';
	        	if(strCustName[1].contains(Label.BSureC_ExpSpecialChar)){
	        		string[] strCodeReason = strCustName[1].split(Label.BSureC_ExpSpecialChar);
	        		if(strCodeReason[0].contains(':')){
	        			string[] strCode = strCodeReason[0].split(':');
	        			strEmailBody += '<td height="21px">'+strCode[1]+'</td>';
	        		}
	        		if(strCodeReason[1].contains(';')){
	        			strEmailBody += '<td height="21px">'+strCodeReason[1].replace(';','')+'</td>';
	        		}else{
	        			strEmailBody += '<td height="21px">'+strCodeReason[1]+'</td>';
	        		}
	        	}else{
	        		strEmailBody += '<td height="21px">'+ +'</td>';
		        	if(strCustName[1].contains(';') && strCustName[1].contains(Label.BSureC_ExpErrorCode)){
	        			string strReason = strCustName[1].replace(';','');
	        			strReason = strReason.replace(Label.BSureC_ExpErrorCode,'');
	        			strEmailBody += '<td height="21px">'+strReason+'</td>';
	        		}else if(strCustName[1].contains(';')){
	        			strEmailBody += '<td height="21px">'+strCustName[1].replace(';','')+'</td>';
	        		}else{
	        			strEmailBody += '<td height="21px">'+strCustName[1]+'</td>';
	        		}
	        	}
	        }
	        strEmailBody += '</tr></table>';        
	                
	 		mail1.setHtmlBody(strEmailBody);  
	 		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });  
 		}
	}  
}