/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_MassTransferOfOwnership {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureC_BuyerShare';
        cSettings4.Parameter_Key__c = 'BSureC_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Key__c = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureC_Analyst,BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Key__c = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureC_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
        BSureC_MassTransferOfOwnership ownership=new BSureC_MassTransferOfOwnership();
        BSureC_MassTransferOfOwnership.Wrap_User_Info ownershipwrap=new BSureC_MassTransferOfOwnership.Wrap_User_Info();
        BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate ownershipwrapupdate=new BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate();
        
        //Wrap_User_Info UserWrap=new Wrap_User_Info();
     /*ownershipwrap.boolseluser=false;
     ownershipwrap.str_UserProfile=UserInfo.getProfileID();
     ownershipwrap.str_Username=UserInfo.getUserName();
     ownershipwrap.str_UserFirstname=UserInfo.getFirstName();
     ownershipwrap.str_UserLastname=UserInfo.getLastName ();
     ownershipwrap.str_UserId=UserInfo.getUserID();
     ownershipwrap.str_RoleName='BSureC_Analyst';
     ownership.lstUsrsWithSelectedProfile.add(ownershipwrap);*/
 
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
         BSureC_Customer_Basic_Info__c BCRef=new BSureC_Customer_Basic_Info__c (Analyst__c=userinfo.getuserid());
         insert BCRef;
         BSureC_Customer_Basic_Info__c BCbRef=new BSureC_Customer_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
         insert BCbRef;
         BSureC_Assigned_Buyers__c BCaRef=new BSureC_Assigned_Buyers__c (User_Id__c=userinfo.getuserid(),Customer_Id__c=BCbRef.id);
         insert BCaRef;
         system.Assertequals(Userinfo.getuserID(),BCRef.Analyst__c);
         //ownership.strAllowedProfiles='BSureC_Analyst,BSureC_Collector';
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
        ownership.SelectUser();
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.SelectUserDetails();
        ownership.submitForUserRole();
        ownership.submit();
        ownership.Cancel();
        ownership.hidePopup();
        ownership.CreditCustomers();
        ownership.NonCreditCustomers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
        ownership.CustomerData();
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
        ownership.CustomerData();
        ownership.SearchCatUsers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
        ownership.CustomerNonCreditData();
        string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
        ownership.FetchCustomerDetails();
        ownership.str_CustType='Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Back Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Non Credit';
        ownership.ChangeInfo();
        ownership.ChangeOwnership();
        ownership.AssignUser();
        ownership.hideUser();
        ownership.str_CustType='Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Back Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Non Credit';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.nextBtnClick();
        ownership.previousBtnClick();
        ownership.getPageNumber();
        ownership.getPageSize();
        ownership.getPreviousButtonEnabled();
        ownership.getNextButtonDisabled();
        ownership.totalPageNumber=0;
        ownership.totallistsize=1;
        ownership.getTotalPageNumber();
        ownership.BindData(1);
        ownership.pageData(1);
        ownership.pageL=1;
        ownership.LastpageData(1);
        ownership.LastbtnClick();
        ownership.FirstbtnClick();
    }
    
        static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureC_BuyerShare';
        cSettings4.Parameter_Key__c = 'BSureC_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Key__c = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureC_Analyst,BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Key__c = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureC_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
        BSureC_MassTransferOfOwnership ownership=new BSureC_MassTransferOfOwnership();
        BSureC_MassTransferOfOwnership.Wrap_User_Info ownershipwrap=new BSureC_MassTransferOfOwnership.Wrap_User_Info();
        BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate ownershipwrapupdate=new BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate();
        
        
         BSureC_Customer_Basic_Info__c BCRef=new BSureC_Customer_Basic_Info__c (Analyst__c=userinfo.getuserid());
         insert BCRef;
         BSureC_Customer_Basic_Info__c BCbRef=new BSureC_Customer_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
         insert BCbRef;
         BSureC_Assigned_Buyers__c BCaRef=new BSureC_Assigned_Buyers__c (User_Id__c=userinfo.getuserid(),Customer_Id__c=BCbRef.id);
         insert BCaRef;
         system.Assertequals(Userinfo.getuserID(),BCRef.Analyst__c);
         //ownership.strAllowedProfiles=ownership.strAllowedProfilescust;
         ownership.strParmProfilename='Backup Analyst';
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
        ownership.SelectUser();
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.SelectUserDetails();
        ownership.submitForUserRole();
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
         ownership.conflag=false;
         //selectedProfileId
        ownership.submit();
        ownership.Cancel();
        ownership.hidePopup();
        ownership.CreditCustomers();
        ownership.NonCreditCustomers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
        ownership.CustomerData();
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
        ownership.CustomerData();
        ownership.SearchCatUsers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
        ownership.CustomerNonCreditData();
        string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
        ownership.FetchCustomerDetails();
        ownership.str_CustType='Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Back Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Non Credit';
        ownership.ChangeInfo();
        ownership.ChangeOwnership();
        ownership.AssignUser();
        ownership.hideUser();
        ownership.str_CustType='Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Back Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Non Credit';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.nextBtnClick();
        ownership.previousBtnClick();
        ownership.getPageNumber();
        ownership.getPageSize();
        ownership.getPreviousButtonEnabled();
        ownership.getNextButtonDisabled();
        ownership.totalPageNumber=0;
        ownership.totallistsize=1;
        ownership.getTotalPageNumber();
        ownership.BindData(1);
        ownership.pageData(1);
        ownership.pageL=2;
        ownership.LastpageData(1);
        ownership.LastbtnClick();
        ownership.FirstbtnClick();
    }
    
    Static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        BSure_Configuration_Settings__c cSettings4 =new BSure_Configuration_Settings__c();
        cSettings4.Name = 'BSureC_BuyerShare';
        cSettings4.Parameter_Key__c = 'BSureC_BuyerShare';
        cSettings4.Parameter_Value__c = 'TRUE';
        lstSettings.add(cSettings4);

        BSure_Configuration_Settings__c cSettings5 =new BSure_Configuration_Settings__c();
        cSettings5.Name = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Key__c = 'BSureC_MassTransferAllowedProfiles';
        cSettings5.Parameter_Value__c = 'BSureC_Analyst,BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings5 );
        
        BSure_Configuration_Settings__c cSettings6 =new BSure_Configuration_Settings__c();
        cSettings6.Name = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Key__c = 'BSureC_MassTransferNonCreditProfiles';
        cSettings6.Parameter_Value__c = 'BSureC_Collector,BSureC_Visitor';
        lstSettings.add(cSettings6 );
        
        BSure_Configuration_Settings__c cSettings7 =new BSure_Configuration_Settings__c();
        cSettings7.Name = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Key__c = 'BSureC_MassTransferCreditProfiles';
        cSettings7.Parameter_Value__c = 'BSureC_Analyst';
        lstSettings.add(cSettings7 );
        
        insert lstSettings;
        
        BSureC_MassTransferOfOwnership ownership=new BSureC_MassTransferOfOwnership();
        BSureC_MassTransferOfOwnership.Wrap_User_Info ownershipwrap=new BSureC_MassTransferOfOwnership.Wrap_User_Info();
        BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate ownershipwrapupdate=new BSureC_MassTransferOfOwnership.Wrap_Cust_Infoupdate();
        ownership.strParmUserid=UserInfo.getUserID();
        ownership.strParmProfilename='Backup Analyst';
        //ownership.strAllowedProfiles=ownership.strAllowedProfilescust;
        ownership.getUserProfiles();
        ownership.struserName='e';
        ownership.getUserWithSelectedProfile();
        ownership.SelectUser();
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.SelectUserDetails();
        ownership.submitForUserRole();
        ownership.strParmSelectedUserid=apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
         BSureC_Customer_Basic_Info__c BCRef=new BSureC_Customer_Basic_Info__c (Analyst__c=userinfo.getuserid());
         insert BCRef;
         BSureC_Customer_Basic_Info__c BCbRef=new BSureC_Customer_Basic_Info__c (Backup_Analysts__c=userinfo.getuserid());
         insert BCbRef;
         BSureC_Assigned_Buyers__c BCaRef=new BSureC_Assigned_Buyers__c (User_Id__c=userinfo.getuserid(),Customer_Id__c=BCbRef.id);
         insert BCaRef;
         system.Assertequals(Userinfo.getuserID(),BCRef.Analyst__c);
         ownership.conflag=false;
        ownership.submit();
        ownership.Cancel();
        ownership.hidePopup();
        ownership.CreditCustomers();
        ownership.NonCreditCustomers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('uid',userinfo.getuserid());
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Analyst');
        ownership.CustomerData();
        ownership.strParmProfilename = apexpages.currentpage().getparameters().put('pval','Back Analyst');
        ownership.CustomerData();
        ownership.SearchCatUsers();
        ownership.strParmUserid = apexpages.currentpage().getparameters().put('prmUserID',userinfo.getuserid());
        ownership.CustomerNonCreditData();
        string Customerinfo=apexpages.currentpage().getparameters().put('CustomerId',BCRef.id);
        ownership.FetchCustomerDetails();
        ownership.str_CustType='Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Back Analyst';
        ownership.ChangeInfo();
        ownership.str_CustType='Non Credit';
        ownership.ChangeInfo();
        ownership.ChangeOwnership();
        ownership.AssignUser();
        ownership.hideUser();
        ownership.str_CustType='Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Back Analyst';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.str_CustType='Non Credit';
        ownership.str_AssignUser=userinfo.getuserid();
        ownership.ChangeCustomerOwnerShip();
        ownership.BindData(1);
        ownership.nextBtnClick();
        ownership.previousBtnClick();
        ownership.getPageNumber();
        ownership.getPageSize();
        ownership.getPreviousButtonEnabled();
        ownership.getNextButtonDisabled();
        ownership.totalPageNumber=0;
        ownership.totallistsize=1;
        ownership.getTotalPageNumber();
        
        ownership.pageData(1);
        ownership.pageL=3;
        ownership.LastpageData(1);
        ownership.LastbtnClick();
        ownership.FirstbtnClick();
    }
}