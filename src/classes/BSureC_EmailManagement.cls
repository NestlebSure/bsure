global with sharing class BSureC_EmailManagement implements Database.Batchable<sObject>,
															Database.stateful, schedulable{
    string strEmailQueueQuery;
    string strNSubject = '';
    string strNBody = '';
    string strDDSubject = '';
    string strDDBody = '';    
    string strForumName = '';
    public String strEmailStatus{get;set;}
    public String strIsdailydigestYes{get;set;}
    public String strIsdailydigestNo{get;set;}
    public Date strSendOndate{get;set;}
    public boolean strSendImmediatefalse{get;set;}
    public boolean strSendImmediatetrue{get;set;}
    
    map<string, list<BSureC_Email_Queue__c>> mapEmailQueue = new map<string, list<BSureC_Email_Queue__c>>();
    list<EmailTemplate> lstEmailTemplates;
    set<string> setTemplateNames = new set<string>{'BSureC_Notifications', 'BSureC_DailyDigest'};
    
    global BSureC_EmailManagement()
    {  
    	//system.debug('Current Time : ' + System.now().time());
    	strEmailStatus = 'NEW';
    	strSendOndate = System.today();
    	strIsdailydigestNo = 'NO';
    	strIsdailydigestYes = 'YES';
    	strSendImmediatefalse = false;
    	strSendImmediatetrue = true;
    	
    	Time timeToStart = Time.newInstance(18, 20, 0, 0);
    	Time timeToStop = timeToStart.addMinutes(35);
        strEmailQueueQuery  = 'SELECT Id, Recipient_Address__c, Is_Daily_Digest__c, Email_Subject__c, ';
        strEmailQueueQuery += 'Email_Status__c, Email_Priority__c, Email_Body__c, Recipient_Name__c, Send_Immediate__c ';
        strEmailQueueQuery += 'FROM BSureC_Email_Queue__c ';
        strEmailQueueQuery += 'WHERE Email_Status__c =: strEmailStatus  ';
        strEmailQueueQuery += 'AND Send_On_Date__c <=: strSendOndate ';
    	strEmailQueueQuery += 'AND ((Is_Daily_Digest__c =: strIsdailydigestNo ';
    	strEmailQueueQuery += 'AND Send_Immediate__c =: strSendImmediatefalse ) ';
    	strEmailQueueQuery += 'OR (Is_Daily_Digest__c =: strIsdailydigestYes ';
    	strEmailQueueQuery += 'AND Send_Immediate__c =: strSendImmediatetrue) ';
    	strEmailQueueQuery += 'OR (Is_Daily_Digest__c =: strIsdailydigestNo ';
    	strEmailQueueQuery += 'AND Send_Immediate__c =: strSendImmediatetrue)) ';
        strEmailQueueQuery += 'ORDER BY CreatedDate limit 1000';
        system.debug('strEmailQueueQuery=========='+strEmailQueueQuery);
        /*
        strEmailQueueQuery += 'WHERE Email_Status__c = \'NEW\' ';
        strEmailQueueQuery += 'AND Send_On_Date__c <= TODAY ';
    	strEmailQueueQuery += 'AND ((Is_Daily_Digest__c = \'NO\' ';
    	strEmailQueueQuery += 'AND Send_Immediate__c = false) ';
    	strEmailQueueQuery += 'OR (Is_Daily_Digest__c = \'YES\' ';
    	strEmailQueueQuery += 'AND Send_Immediate__c = true) ';
    	strEmailQueueQuery += 'OR (Is_Daily_Digest__c = \'NO\' ';
    	strEmailQueueQuery += 'AND Send_Immediate__c = true)) ';
        strEmailQueueQuery += 'ORDER BY CreatedDate limit 1000';
        */
        //system.debug('strEmailQueueQuery+++' + strEmailQueueQuery);
    }
    
    global Database.QueryLocator start(Database.BatchableContext Bc)
    {
        return Database.getQueryLocator(strEmailQueueQuery);
    }
    
    global void Execute(Database.BatchableContext Bc, list<BSureC_Email_Queue__c> lstEmailMsgs)    
    {  
    	string strUniqueKey = '';
    	strForumName = BSureC_CommonUtil.getConfigurationValues('BSureC_ForumName').get(0);
        list<string> lstIndEmailAdd = new list<string>();
        list<Messaging.SingleEmailMessage> objMailsList = new list<Messaging.SingleEmailMessage>(); 
        
        GetEmailTemplates();
        
        for(BSureC_Email_Queue__c objEachMail : lstEmailMsgs)
        {
        	strUniqueKey = objEachMail.Recipient_Address__c.trim() + '~' +
                	 (objEachMail.Is_Daily_Digest__c != null && objEachMail.Is_Daily_Digest__c != '' 
                	 	? objEachMail.Is_Daily_Digest__c.trim() + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase() 
                	 	: 'YES' + string.valueOf(objEachMail.Send_Immediate__c).toUpperCase());
            
            if(mapEmailQueue.containsKey(strUniqueKey))
            {
            	list<BSureC_Email_Queue__c> objListEQ = mapEmailQueue.get(strUniqueKey);
            	objListEQ.add(objEachMail);
            	mapEmailQueue.put(strUniqueKey, objListEQ);
            }
            else
            {
            	list<BSureC_Email_Queue__c> objListEQ = new list<BSureC_Email_Queue__c>();
            	objListEQ.add(objEachMail);
            	mapEmailQueue.put(strUniqueKey, objListEQ);
            }
        }
        //system.debug('mapEmailQueue+++++++++' + mapEmailQueue.size());
        if(mapEmailQueue != null && mapEmailQueue.size() > 0)
        {
        	for(string toAddressKey : mapEmailQueue.keySet())
        	{
        		if(toAddressKey.contains('YESFALSE'))
        		{
        			objMailsList.add(GenerateConsolidatedMail(mapEmailQueue.get(toAddressKey)));
        		}
        		else
        		{
        			for(BSureC_Email_Queue__c objEachMsg : mapEmailQueue.get(toAddressKey))
        			{
	        			string strSubject = strNSubject.replace('%%SUBJECT%%', objEachMsg.Email_Subject__c);
	        			string strEmailBody = strNBody.replace('%%USER%%', objEachMsg.Recipient_Name__c);
	        			strEmailBody = strEmailBody.replace('%%EMAILBODY%%', objEachMsg.Email_Body__c);
	        			strEmailBody = strEmailBody.replace('\r\n', '</br>');
	        			Messaging.SingleEmailMessage objIndividualMail = new Messaging.SingleEmailMessage();
	        			lstIndEmailAdd.clear();
	        			lstIndEmailAdd.add(objEachMsg.Recipient_Address__c);
	        			
						objIndividualMail.setToAddresses(lstIndEmailAdd);
						objIndividualMail.setUseSignature(false);
						objIndividualMail.setSaveAsActivity(false);
						objIndividualMail.setSubject(strSubject); 
						objIndividualMail.setSenderDisplayName(strForumName);
						objIndividualMail.setHtmlBody(strEmailBody);
						
						objMailsList.add(objIndividualMail);
        			}
        		}
        	}
        }
        Messaging.sendEmail(objMailsList);
        UpdateEmailQueue(lstEmailMsgs);
    }
    
    public Messaging.SingleEmailMessage GenerateConsolidatedMail(list<BSureC_Email_Queue__c> objListEQ)
    {
    	string strEmailAdds = '';
    	string strRecipientName = '';
    	list<string> lstEmailAdd = new list<string>();
    	string strSubject = strDDSubject.replace('%%DATE%%', System.today().format()); 
    	string strEmailBody = '<table width="100%" border="0" cellpadding="10" cellspacing="10">';
    	
    	Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
    	
    	for(BSureC_Email_Queue__c objEachMsg : objListEQ)
    	{
    		strEmailAdds = objEachMsg.Recipient_Address__c;
    		strRecipientName = objEachMsg.Recipient_Name__c;
    		strEmailBody += '<tr><td width="100%" style="border-bottom: 1px solid;">';
    		strEmailBody += '<b>' + objEachMsg.Email_Subject__c + '</b></br>';
    		strEmailBody += '______________________________________________________</br></br>';
    		strEmailBody += objEachMsg.Email_Body__c;
    		strEmailBody += '</br></td></tr>';
    	}
    	strEmailBody += '</table>';
    	string strFinalBody = strDDBody.replace('%%USER%%', strRecipientName != null && strRecipientName != '' ? strRecipientName : '');
    	strFinalBody = strFinalBody.replace('%%EMAILBODY%%', strEmailBody);
    	strFinalBody = strFinalBody.replace('\r\n', '</br></br>');
    	lstEmailAdd.add(strEmailAdds);
    	
    	objMail.saveAsActivity = false;
        objMail.setToAddresses(lstEmailAdd);
        objMail.setUseSignature(false);
        objMail.setSaveAsActivity(false);
        objMail.setSubject(strSubject); 
        objMail.setSenderDisplayName(strForumName);
        objMail.setHtmlBody(strFinalBody);
    	return objMail;
    }
    
    public void UpdateEmailQueue(list<BSureC_Email_Queue__c> lstEmailMsgs)
    {
    	try
    	{
    		for(BSureC_Email_Queue__c objEachMail : lstEmailMsgs)
	    	{
	    		if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Status__c.isUpdateable()
	    			||Schema.sObjectType.BSureC_Email_Queue__c.fields.Email_Status__c.isCreateable()){
	    		objEachMail.Email_Status__c = 'SENT';		//Anil:added the if condition
	    		}
	    		if (Schema.sObjectType.BSureC_Email_Queue__c.fields.Delivered_Date__c.isUpdateable()
	    			||Schema.sObjectType.BSureC_Email_Queue__c.fields.Delivered_Date__c.isCreateable()){
	    		objEachMail.Delivered_Date__c = Datetime.now();		//Anil:added the if condition
	    		}
	    	}
	    	if(BSureC_Email_Queue__c.sObjectType.getDescribe().isUpdateable())	//Anil:added the if condition
	    	Database.update(lstEmailMsgs);
    	}
    	catch(Exception ex)
    	{
    		//system.debug('Exception in UpdateEmailQueue ' + ex);
    	}
    }
    
    public void GetEmailTemplates()
    {
    	lstEmailTemplates = new list<EmailTemplate>();
    	lstEmailTemplates = [SELECT Subject, Name, IsActive, Body
    						 FROM EmailTemplate
    						 WHERE Name IN: setTemplateNames
    						 AND IsActive =: true];
    	
    	if(lstEmailTemplates != null && lstEmailTemplates.size() > 0)
    	{
    		for(EmailTemplate objEach : lstEmailTemplates)
    		{
    			if(objEach.Name.contains('DailyDigest'))
    			{
    				strDDBody = objEach.Body;
    				strDDSubject = objEach.Subject;
    			}
    			else if(objEach.Name.contains('Notifications'))
    			{
    				strNBody = objEach.Body;
    				strNSubject = objEach.Subject;
    			}
    		}
    		//system.debug('strDDBody++++++++ ' + strDDBody);
    		//system.debug('strNBody++++++++ ' + strNBody);
    	}
    }
    
    global void execute(SchedulableContext SC)
    {
       	BSureC_EmailManagement  objEmailMngmt =  new BSureC_EmailManagement();
       	ID batchprocessid = Database.executeBatch(objEmailMngmt);  
    }
    
    global void finish(Database.BatchableContext Bc){       
                 
    }
}