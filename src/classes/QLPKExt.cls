public with sharing class QLPKExt {

	public QLPKExt(){
	
	}
	
	public QLPKExt(Apexpages.Standardcontroller controller){
	}

    @RemoteAction
    public static List<BSureS_Basic_Info__c> queryChunk(String firstId, String lastId, Boolean isLast) {
		
		//last Id range uses <=, all others use <
		String lastClause = 'AND Id < \''+ lastId +'\'  ';
		if (isLast) {
			lastClause = 'AND Id <= \''+ lastId +'\'  ';	
		}		
		/*
		String SOQL =  	'SELECT Id, Supplier_Name__c,Review_Status__c ' +
						'FROM BSureS_Basic_Info__c ' +
						'WHERE  Id >= \'' + firstId + '\' ' +
						lastClause;
		*/
		String SOQL =  	'Select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c '+ 
                        //' ,Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c '+
                        //' ,Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
                        //', Owner_Ship__c ,Contact_Name__c '+
                        ' FROM BSureS_Basic_Info__c ' +
                        'WHERE  Id >= \'' + firstId + '\' ' +
						lastClause;
						
		return database.query(SOQL);
    }    



    
}