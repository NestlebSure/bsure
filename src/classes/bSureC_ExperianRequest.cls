global class bSureC_ExperianRequest {
    public string ExperianString;
    public string expRespString;
    public string parentId_lst;
    public string attachName_lst;
    public bSureC_ExperianRequest(){
    }  
    
    webservice static string generateExperianRpt(string CustId, string BIN, string crdtAcct)
    {
        string resString = '';
        BSure_Configuration_Settings__c custSetting = BSure_Configuration_Settings__c.getInstance('ExperianAuthenticationCheck');
        
        if(custSetting.Parameter_Value__c!=null && custSetting.Parameter_Value__c!='' &&  custSetting.Parameter_Value__c=='false'){
            resString = Label.BSureC_ExpTempDisable;
        }else{
            DateTime d = Datetime.now();
            string dateStr =  d.format('MMddyy') ;
            system.debug('::::::::::: ' + dateStr) ;
            string fName = dateStr+'_Experian_Rpt_'+crdtAcct;
            Database.SaveResult SaveResult;
            try{
                bSureC_ExperianRequest objExpReq = new bSureC_ExperianRequest();
                resString = objExpReq.getExperianRequest(CustId,fName,BIN); 
                list<BSureC_On_Going_Credit_Section__c> lstOGCS = [Select id,name,Customer_ID__c,TitleDescription__c,Document_Type__c  
                                                                    from BSureC_On_Going_Credit_Section__c 
                                                                    where Customer_ID__c =: CustId and TitleDescription__c =: fName];
                BSureC_On_Going_Credit_Section__c objOGCS = new BSureC_On_Going_Credit_Section__c();
                if(lstOGCS != null && lstOGCS.size()>0){
                    objOGCS = lstOGCS[0];
                    Blob blobContent = Blob.valueof(objExpReq.expRespString);
                    Attachment myAttachment  = new Attachment();
                    myAttachment.Body = blobContent;
                    myAttachment.Name = fName;
                    myAttachment.parentId = objOGCS.id;
                    SaveResult = Database.insert(myAttachment);
                    system.debug('SaveResult::'+SaveResult);
                    if(resString != '' && resString != null){
                        resString += ','+objOGCS.id+','+fName;
                    }
                }
            }catch(Exception ex){
                system.debug('Exception Occured '+ex.getMessage()+ex.getStackTraceString());
            }
            if(resString == '' || resString == null){
                resString = Label.BSureC_ExpException;
            }
            if(Test.isRunningTest()){
                resString = '';
            }
        }
        return resString;
    }
    
    public string getExperianRequest(string strCustomerId, string strfileName, string strBIN){
        string strRes = '';
        string strTimeOut = '';
        try{
            String strUsername;
            String strPassword;
            
            list<bSureC_ExpCred__c> lstCred = [SELECT Id,Name,Pwd__c,UN__c From bSureC_ExpCred__c where Credit_Agency__c ='Experian'];
            if(lstCred!=null && lstCred.size()>0){
                if(lstCred[0].UN__c!=null && lstCred[0].UN__c!=''){
                    strUsername = lstCred[0].UN__c;
                }
                if(lstCred[0].Pwd__c!=null && lstCred[0].Pwd__c!=''){
                    strPassword = lstCred[0].Pwd__c;
                }
            }else{
                strRes = Label.BSureC_ExpCred;
            }
            
            if(strUsername!=null && strUsername!='' && strPassword!=null && strPassword!=''){
                String strExperianEndpoint = BSureC_CommonUtil.getConfigurationValues('ExperianEndpoint').get(0);
                map<String,String> mapExpAlias = new map<string,string>();
                map<String,String> mapResultBook = new map<string,string>();
                for(BsureExperian__c objBsureExper:BsureExperian__c.getAll().values()){
                    if(objBsureExper!=null){
                        if(objBsureExper.NodeName__c!='' && objBsureExper.NodeName__c!='' && objBsureExper.AliasName__c!=null 
                                                                                          && objBsureExper.AliasName__c!=''){
                            mapExpAlias.put(objBsureExper.NodeName__c,objBsureExper.AliasName__c);
                        }
                    }
                 }
             
                getExperianEnquiryString(strBIN);
                HttpRequest req = new HttpRequest();  
                req.setEndpoint(strExperianEndpoint);
                req.setHeader('content-type','application/x-www-form-urlencoded');
                req.setMethod('POST');
                Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                req.setHeader('Authorization', authorizationHeader);
                req.setBody(ExperianString); 
                Http http = new Http();
                XmlStreamReader reader;
                HttpResponse res = new HttpResponse();
                if(!Test.isRunningTest()){
                    try{
                        req.setTimeout(120000); // timeout in milliseconds - this is one minute
                        res = http.send(req);
                    }catch(Exception ex){
                        system.debug('Exception occured::: '+ex.getMessage()+ex.getStackTraceString());
                        strTimeOut = Label.BSureC_ExpTimeout+' '+ex.getMessage();
                    }
                    system.debug(res.getStatusCode()+'res===='+res.getBody());
                }else{
                    res.setHeader('Content-Type', 'application/xml');
                    res.setBody('{"foo":"bar"}');
                    res.setStatusCode(200);
                }
                expRespString = '';
                if(res!=null && res.getStatusCode() == 200){
                    reader = res.getXmlStreamReader();
                    expRespString = res.getBody();
                    expRespString = expRespString.replace('<?xml version="1.0" standalone="no"?>','');
                    //expRespString = expRespString.replace('&amp;','');
                    //expRespString = expRespString.replace('%','');
                    //system.debug('expRespString===::'+expRespString);
                    mapResultBook = parseBooks(reader,mapExpAlias);
                    system.debug('mapResultBook:::'+mapResultBook);
                    if(!mapResultBook.isEmpty()){
                        if(mapResultBook.containskey('ProcessingAction')){
                            system.debug('Error Code::'+mapResultBook.get('ProcessingAction'));
                            strRes = Label.BSureC_ExpErrorCode+mapResultBook.get('ProcessingAction');
                        }else if(mapResultBook.containskey('ErrorMessage')){
                            system.debug('Error Code::'+mapResultBook.get('ErrorMessage'));
                            strRes = Label.BSureC_ExpErrorCode+mapResultBook.get('ErrorMessage');
                        }else{
                            string recId = '';
                            try{
                            	previousCredit(strCustomerId);
                                recId = insertONGS(strCustomerId,strfileName,mapResultBook);
                                if(recId != null && recId != ''){
                                    string resVal = '';
                                    try{
                                        resVal = InsertIntoExperain(mapResultBook,strCustomerId);
                                        if(resVal != null && resVal != ''){
                                            strRes = Label.BSureC_ExpSuccess;
                                        }else{
                                            strRes = Label.BSureC_ExpFailData;
                                        }
                                    }catch(Exception ex){
                                        system.debug('Exception Handling for Experian Data '+ex.getMessage()+ex.getStackTraceString());
                                        if(resVal == null || resVal == ''){
                                            strRes = Label.BSureC_ExpFailData;
                                        }
                                    }
                                }else{
                                    strRes = Label.BSureC_ExpFailONGS;
                                }
                            }catch(Exception ex){
                                system.debug('Exception Handling for ONGS Data '+ex.getMessage()+ex.getStackTraceString());
                                if(recId == null || recId == ''){
                                    strRes = Label.BSureC_ExpFailONGS;
                                }
                            }
                        }
                    }
                }else if(res!=null && res.getStatusCode() == 303){
                    strRes = Label.BSureC_ExpErrorCode+res.getStatusCode()+Label.BSureC_ExpSpecialChar+Label.BSureC_ExpAuthFail;
                    BSure_Configuration_Settings__c newObj = [SELECT Parameter_Key__c,Parameter_Value__c 
                                                                    From BSure_Configuration_Settings__c 
                                                                    Where Parameter_Key__c=:'ExperianAuthenticationCheck' 
                                                                    And Parameter_Value__c=:'true'];
                    if(newObj!=null){
                        newObj.Parameter_Value__c = 'false';
                        update newObj;
                    }
                }else{
                    if(strTimeOut != null && strTimeOut != ''){
                        strRes = strTimeOut;
                    }else{
                        strRes = Label.BSureC_ExpErrorCode+res.getStatusCode();
                    }
                }
            }else{
                strRes = Label.BSureC_ExpCred;
            }
        }catch(Exception ex){
            system.debug('Exception occured '+ex.getMessage()+ex.getStackTraceString());
        }
        system.debug('strRes=='+strRes);
        return strRes;
    }
  	public void previousCredit(string ParentId){
  		string attchId='';
    	integer cnt=0;
    	parentId_lst='';
    	attachName_lst='';
    	for(BSureC_On_Going_Credit_Section__c lstVal:[Select id ,TitleDescription__c from BSureC_On_Going_Credit_Section__c
    													 where Customer_ID__c =: ParentId and TitleDescription__c LIKE '%Experian%' 
    													order by createddate DESC limit 2 ]){
    		if(cnt==1){
    			parentId_lst =lstVal.id;
    			attachName_lst =lstVal.TitleDescription__c;
    		}						
    		cnt++;						 
		 }
  	}
    public void getExperianEnquiryString(string BINFileNumber){
        String strExperianDBHost = BSureC_CommonUtil.getConfigurationValues('ExperianDBHost').get(0);
        String strExperianEAI = BSureC_CommonUtil.getConfigurationValues('ExperianEAI').get(0);
        String strExperianSubCode = BSureC_CommonUtil.getConfigurationValues('ExperianSubCode').get(0);
        String strExperianVendorNumber = BSureC_CommonUtil.getConfigurationValues('ExperianVendorNumber').get(0);
        
        //Premier Profile with Business Identification Number (BIN)
        String PremierProfileString = '<?xml version="1.0" encoding="UTF-8"?>'+
            '<NetConnectRequest xmlns="http://www.experian.com/NetConnect" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.experian.com/NetConnect NetConnect.xsd">'+
                '<EAI>'+strExperianEAI+'</EAI>'+
                '<DBHost>'+strExperianDBHost+'</DBHost>'+
                '<ReferenceId>user1abc001</ReferenceId>'+
                '<Request xmlns="http://www.experian.com/WebDelivery" version="1.0">'+
                    '<Products>'+
                        '<PremierProfile>'+
                            '<Subscriber>'+
                                '<OpInitials>GG</OpInitials>'+
                                '<SubCode>'+strExperianSubCode+'</SubCode>'+
                            '</Subscriber>'+
                            '<BusinessApplicant>'+
                                '<BISFileNumber>'+BINFileNumber+'</BISFileNumber>'+
                            '</BusinessApplicant>'+
                            '<OutputType>'+
                                '<XML>'+
                                    '<Verbose>Y</Verbose>'+
                                '</XML>'+
                            '</OutputType>'+
                            '<Vendor>'+
                                '<VendorNumber>'+strExperianVendorNumber+'</VendorNumber>'+
                            '</Vendor>'+
                        '</PremierProfile>'+
                    '</Products>'+
                '</Request>'+
            '</NetConnectRequest>';
        ExperianString = 'NETCONNECT_TRANSACTION='+PremierProfileString;
    }  
    
    /* Reading the Xml Response*/
    public map<string,string> parseBooks(XmlStreamReader reader, map<string,string> mapAlias) 
    {
        map<String, String> mapXmlBook = new map<String, String>();
        String ErrorCode='';
        try{
            while(reader.hasNext() ) 
            {
                String strNewKey = '';
                String Value = '';
                if (reader.getEventType() == XmlTag.START_ELEMENT) 
                {
                    if('ProcessingAction' == reader.getLocalName())
                    {
                        ErrorCode = reader.getAttributeValue(null, 'code')+Label.BSureC_ExpSpecialChar+getValueFromTag(reader);
                        mapXmlBook.put('ProcessingAction',ErrorCode);
                        break;
                    }
                    if('ErrorMessage' == reader.getLocalName())
                    {
                        ErrorCode = getValueFromTag(reader);
                        mapXmlBook.put('ErrorMessage',ErrorCode);
                        break;
                    }
                    String CurrentTag = reader.getLocalName();
                    if(mapAlias.containskey(CurrentTag))
                    {
                        for(String strEq:mapAlias.keyset())
                        {
                            if (strEq == reader.getLocalName())
                            {
                                if('IntelliscoreScoreInformation' == strEq){
                                    string Score = '';
                                    string Title = '';
                                    system.debug(reader.nextTag() +'Entered==='+reader.getLocalName());
                                    while(reader.hasNext()) 
                                    {
                                        reader.next();
                                        if(Score=='' && reader.getLocalName() == 'Score'){
                                            Score = getValueFromTag(reader);
                                        }else if(Score!='' && reader.getLocalName() == 'ModelTitle'){
                                            Title = getValueFromTag(reader);
                                            if(Title!=null && Title!='' && Title == 'FINANCIAL STABILITY RISK'){
                                                Title = 'FINANCIALSTABILITYRISK';
                                            }else if(Title!=null && Title!='' && Title == 'INTELLISCORE PLUS V2'){
                                                Title = 'INTELLISCOREPLUSV2';
                                            }
                                            break;
                                        }
                                    }
                                    mapXmlBook.put(Title,Score);
                                    //system.debug('==='+mapXmlBook);
                                }else{
                                    strNewKey = mapAlias.get(strEq);
                                    if(strNewKey != null && strNewKey != '' && !mapXmlBook.containsKey(strNewKey) )
                                    {
                                        Value = getValueFromTag(reader);
                                        mapXmlBook.put(strNewKey,Value);
                                    }
                                    break;
                                }
                            } 
                        }
                    }
                }
                reader.next();
            }
            return mapXmlBook;
        }catch(XmlException ex)
        {
            mapXmlBook.put('XmlParsingException','Exception during Xml parsing error : '+ex.getMessage()+ex.getStackTraceString());
            return mapXmlBook;
        }
    }
   
    /* Getting the Value from the Tag */
    public String getValueFromTag(XmlStreamReader reader) 
    {
        String returnvalue = '';
        reader.setCoalescing(true);
        while(reader.hasNext()) 
        {
            if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) 
            {
                returnvalue=reader.getText();
            }
        reader.next();
        }
        return returnvalue;
    }
    
    public string insertONGS(string strRecId,string strFName,map<string,string> mapExpResult){
        Database.upsertResult Result;
        try{
            list<BSureC_On_Going_Credit_Section__c> lstOGCS = [Select id,name,Customer_ID__c,TitleDescription__c,Document_Type__c from BSureC_On_Going_Credit_Section__c 
                                                            where Customer_ID__c =: strRecId and TitleDescription__c =: strFName];
            BSureC_On_Going_Credit_Section__c objOGCS = new BSureC_On_Going_Credit_Section__c();
            if(lstOGCS != null && lstOGCS.size()>0){
                objOGCS = lstOGCS[0];
                list<Attachment> attach = [select id,name from Attachment where ParentId=:objOGCS.id];
                delete attach;
            }       
            system.debug('strFName==='+strFName+'****'+strRecId);
            objOGCS.TitleDescription__c = strFName;
            objOGCS.Customer_ID__c = strRecId;
            objOGCS.Discussion_Type__c = 'File';
            
            String docType = BSureC_CommonUtil.getConfigurationValues('ExperianCustRecDocType').get(0); 
            objOGCS.Document_Type__c = docType;
            
            Result = Database.upsert(objOGCS);
        }catch(Exception ex){
            system.debug('Exception occured while saving Experian report into OnGoingCreditSection.'+ex.getMessage()+ex.getStackTraceString());
        }
        if(Result.isSuccess() == true){
            return Result.Id;
        }else{
            return '';
        }
    } 
    
        public static decimal drop_leading_zeros(String passedValue) {
        String return_string = null; //return string for passing back
        if (passedValue != null) { //if the passed value is not null
            return_string = passedValue.trim(); //trim the whitespace from the start and end of the value
            Pattern valid_characters = Pattern.compile('([0-9]+)'); //only numbers
            Matcher check_chars = valid_characters.matcher(return_string); //compare the string to the set of valid characters
            if (check_chars.matches()) { //if we have a somewhat valid number
                if (return_string.startsWith('0') && return_string.length() > 1) { //if the string begins with a 0 and the length is greater than 1
                    boolean keepChecking = true; //create a boolean variable
                    while (keepChecking) { //if boolean is true
                        if (return_string.startsWith('0') && return_string.length() > 1) { //if the string begins with 0 and there is more than 1 character
                            return_string = return_string.substring(1); //drop the first character
                        } else { //either the string doesn't begin with 0 or the length is less than or equal to 1
                            keepChecking = false; //stop the loop
                        }
                    }
                }
                if (return_string == '0') { //if the resulting string is now a single '0'
                    return_string = '0'; //set the string to null
                }
            } else { //otherwise the value passed was not valid
                return_string = '0'; //set the string to null
            }
        }
        return decimal.valueof(return_string); //pass back a value
    }
    
    public string InsertIntoExperain(map<string,string> mapExpResult, string strCustId){
        Database.saveResult Result;//To save Experian values in customer info object
        Database.saveResult expHistResult;//To save Old Experian values in history object
        Savepoint sp = Database.setSavepoint(); 
        try{
            if(mapExpResult != null && mapExpResult.size() > 0){
                BSureC_Customer_Basic_Info__c objCustomerExpData = new BSureC_Customer_Basic_Info__c();
                objCustomerExpData = [Select id, Name, Company_DBT__c, Credit_Recommendation__c, Legal_Filings__c, Financial_Risk__c, 
                                                Fraud_Alerts__c, Highest_amt_extended__c, Intelliscore_Plus__c, Report_Date__c
                                            From BSureC_Customer_Basic_Info__c Where id =: strCustId];
                /*                         
                bSureC_CreditAgency_History__c objExperianHistory = new bSureC_CreditAgency_History__c ();   
                objExperianHistory.Customer_Info_Id__c = objCustomerExpData.Id;
                objExperianHistory.Name='Experian';
                objExperianHistory.RecordTypeId='012d0000001lAYFAA2'; 
                objExperianHistory.AttachmentId__c=attachName_lst;
                objExperianHistory.Parent_Id__c= parentId_lst;
                system.debug('AttachmentId__c==='+attachName_lst +'&&'+parentId_lst);
                if(string.isNotBlank(objCustomerExpData.Company_DBT__c)){            
                    objExperianHistory.Company_DBT__c = objCustomerExpData.Company_DBT__c;  
                }
                if(objCustomerExpData.Credit_Recommendation__c != null){   
                    objExperianHistory.Credit_Recommendation__c = objCustomerExpData.Credit_Recommendation__c;
                }
                if(string.isNotBlank(objCustomerExpData.Legal_Filings__c)){    
                    objExperianHistory.Legal_Filings__c = objCustomerExpData.Legal_Filings__c;
                }
                if(string.isNotBlank(objCustomerExpData.Financial_Risk__c)){   
                    objExperianHistory.Financial_Risk__c = objCustomerExpData.Financial_Risk__c;
                }
                if(string.isNotBlank(objCustomerExpData.Fraud_Alerts__c)){ 
                    objExperianHistory.Fraud_Alerts__c = objCustomerExpData.Fraud_Alerts__c;
                }
                if(objCustomerExpData.Highest_amt_extended__c != null){    
                    objExperianHistory.Highest_amt_extended__c = objCustomerExpData.Highest_amt_extended__c;
                }
                if(string.isNotBlank(objCustomerExpData.Intelliscore_Plus__c)){    
                    objExperianHistory.Intelliscore_Plus__c = objCustomerExpData.Intelliscore_Plus__c;
                }
                if(objCustomerExpData.Report_Date__c != null){ 
                    objExperianHistory.Report_Date__c = objCustomerExpData.Report_Date__c;
                }
                 
                expHistResult = Database.insert(objExperianHistory);
                system.debug('expHistResult==='+expHistResult);
                system.debug('objExperianHistory==='+objExperianHistory);
                */
                string riskClass = '';
                if(mapExpResult.containskey('RiskClass')){
                    if(mapExpResult.get('RiskClass') == '1'){
                        riskClass = 'LOW RISK';
                    }else if(mapExpResult.get('RiskClass') == '2'){
                        riskClass = 'LOW-MEDIUM RISK    ';
                    }else if(mapExpResult.get('RiskClass') == '3'){
                        riskClass = 'MEDIUM RISK';
                    }else if(mapExpResult.get('RiskClass') == '4'){
                        riskClass = 'MEDIUM-HIGH RISK';
                    }else if(mapExpResult.get('RiskClass') == '5'){
                        riskClass = 'HIGH RISK';
                    }else{
                        riskClass = 'TEST LOW-MEDIUM RISK';
                    }
                }
                
                if(mapExpResult.containskey('FINANCIALSTABILITYRISK')){
                     string riskValue = mapExpResult.get('FINANCIALSTABILITYRISK');
                     if(riskValue != null && riskValue != ''){
                        riskValue = riskValue.substring(riskValue.length()-4,riskValue.length());
                        if(riskValue.length() == 4){
                            riskValue = riskValue.substring(0,riskValue.length()-2);
                        }else if(riskValue.length() == 5){
                            riskValue = riskValue.substring(0,riskValue.length()-3);
                        }
                        if(riskClass!=null && riskClass!=''){
                            objCustomerExpData.Financial_Risk__c = riskValue+'  '+riskClass;
                        }else{
                            objCustomerExpData.Financial_Risk__c = riskValue;
                        }
                     }
                }else{
                    objCustomerExpData.Financial_Risk__c = '0';
                }
                
                if(mapExpResult.containskey('CommercialFraudRiskIndicatorCount')){
                    objCustomerExpData.Fraud_Alerts__c=mapExpResult.get('CommercialFraudRiskIndicatorCount');
                }else{
                    objCustomerExpData.Fraud_Alerts__c='0';
                }
                
                if(mapExpResult.containskey('INTELLISCOREPLUSV2')){
                    string scoreValue = mapExpResult.get('INTELLISCOREPLUSV2');
                    if(scoreValue != null && scoreValue != ''){
                        scoreValue = scoreValue.substring(scoreValue.length()-4,scoreValue.length());
                        if(scoreValue.length() == 4){
                            scoreValue = scoreValue.substring(0,scoreValue.length()-2);
                        }else if(scoreValue.length() == 5){
                            scoreValue = scoreValue.substring(0,scoreValue.length()-3);
                        }
                        objCustomerExpData.Intelliscore_Plus__c = scoreValue;
                    }
                }else{
                    objCustomerExpData.Intelliscore_Plus__c = '0';
                }
                
                if(mapExpResult.containskey('HighCreditAmountExtended')){
                    objCustomerExpData.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('HighCreditAmountExtended'));
                }else if(mapExpResult.containskey('SingleHighCredit')){
                    objCustomerExpData.Highest_amt_extended__c=Decimal.valueof(mapExpResult.get('SingleHighCredit'));
                }else{
                    objCustomerExpData.Highest_amt_extended__c=0;
                }
                
                if(mapExpResult.containskey('CreditLimitAmount')){
                    objCustomerExpData.Credit_Recommendation__c=Decimal.valueof(mapExpResult.get('CreditLimitAmount'));
                }else{
                    objCustomerExpData.Credit_Recommendation__c=0;
                }
                
                decimal dTaxCount = 0;
                decimal dBankCount = 0;
                decimal dJudgmtCount = 0;
                
                if(mapExpResult.containskey('TaxLienFilingCount')){
                    dTaxCount = drop_leading_zeros(mapExpResult.get('TaxLienFilingCount'));
                }
                if(mapExpResult.containskey('BankruptcyFilingCount')){
                    dBankCount = drop_leading_zeros(mapExpResult.get('BankruptcyFilingCount'));
                }
                if(mapExpResult.containskey('JudgmentFilingCount')){
                    dJudgmtCount = drop_leading_zeros(mapExpResult.get('JudgmentFilingCount'));
                }
                objCustomerExpData.Legal_Filings__c=string.valueof(dTaxCount+dBankCount+dJudgmtCount);
                
                if(mapExpResult.containskey('CurrentDBT')){
                    objCustomerExpData.Company_DBT__c=string.valueof(drop_leading_zeros(mapExpResult.get('CurrentDBT')));
                }else{
                    objCustomerExpData.Company_DBT__c = '0';
                }
                
                if(mapExpResult.containskey('ProfileDate')){
                    string dt = mapExpResult.get('ProfileDate');
                    integer strYear = integer.valueof(dt.substring(0,4));
                    integer strMonth = integer.valueof(dt.substring(4,6));
                    integer strDate = integer.valueof(dt.substring(6,8));
                    Datetime myDateTime = Datetime.newInstance(stryear, strMonth, strDate);
                    objCustomerExpData.Report_Date__c = date.valueof(myDateTime);
                }else{
                    objCustomerExpData.Report_Date__c = null;
                }
                
                Result = Database.update(objCustomerExpData);
            }
        }catch(Exception ex){
            system.debug('Exception occured while saving Experian Data.'+ex.getMessage()+ex.getStackTraceString());
        }
        if(Result.isSuccess() == true && expHistResult.isSuccess() == true){
            return Result.Id;
        }else{
            Database.rollback(sp);
            return '';
        }
    }
}