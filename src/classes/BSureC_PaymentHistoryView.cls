/************************************************************************************************       
Controller Name         : BSureC_PaymentHistoryView       
Date                    : 11/15/2012        
Author                  : Santhosh Palla       
Purpose                 : To View the Payment History in Customer Basic Info Pagelayout      
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/05/2012      Santhosh.Palla            Initial Version
                          01/22/2013      VINU PRATHAP          Modified to show up the error message from Label
**************************************************************************************************/
global with sharing class BSureC_PaymentHistoryView 
{
    
    public List<BSureC_Payment_History_Publish__c> lst_payment_history_publish{get;set;}
    private final BSureC_Customer_Basic_Info__c rec_customer_info;//to read the Customer Related values from Standard Controller
    public List<BSureC_Divisions__c> lst_division_names{get;set;}
    public List<SelectOption> lst_selectoptions_info{get;set;}//For Information date Picklist Values lst_selectoptions_info
    public String str_selected_date{get;set;}//Selected date from picklist str_selected_date
    public String str_customer_info_id {get;set;}
    public static String str_cust_id{get;set;}
    public String str_credit_increase_id{get;set;}
    public Map<Date,Date> map_infromationdate {get;set;}
    public Map<Id,BSureC_Payment_History_Publish__c>map_payment_history_publish{get;set;}
    public List<BSureC_paymenthistorybean> lst_payment_history_publish_view{get;set;}
    
    //For Grand Total
    public BSureC_Payment_History_Publish__c rec_credit_totals{get;set;} 
    public List<Sobject> totalRow{get;set;}
    public Decimal total_percent{get;set;}
    /// <summary>  
    /// Constructor  
    /// </summary>
    public BSureC_PaymentHistoryView(Apexpages.Standardcontroller controller)
    {
        
        //for Displaying the Records in Pageblock
        //String.valueOf(pDate)
        totalRow = new List<Sobject>();
       	//system.debug('------Apexpages.currentPage().getParameters()----'+Apexpages.currentPage());
        str_customer_info_id = Apexpages.currentPage().getParameters().get('cust');
      	//str_credit_increase_id = Apexpages.currentPage().getParameters().get('id');
        
       
        //system.debug('------str_customer_info_id--'+str_customer_info_id);
        if(str_customer_info_id == null || str_customer_info_id == '')
        {
            if((BSureC_Customer_Basic_Info__c)controller.getRecord() != null)
            {
                lst_payment_history_publish = new List<BSureC_Payment_History_Publish__c>();
                rec_customer_info = (BSureC_Customer_Basic_Info__c)controller.getRecord();
                //system.debug('-----'+(BSureC_Customer_Basic_Info__c)controller.getRecord());
                str_customer_info_id = String.valueOf(rec_customer_info.Id);
            }
        }
        
        //to load the payment history data
        
        loaddata();
        
    }
    
    public BSureC_PaymentHistoryView(String str)
    {
        str_customer_info_id = str_cust_id;
    }
    
    /// <summary>  
    /// Pagereferance for Information Date Picklist
    /// returns the records depends upon the selected Date from Picklist 
    /// </summary>
    public void bindtable()
    {
        lst_payment_history_publish = new List<BSureC_Payment_History_Publish__c>();
        lst_payment_history_publish_view = new List<BSureC_paymenthistorybean>();
        map_payment_history_publish = new Map<Id,BSureC_Payment_History_Publish__c>();
        totalRow = new List<Sobject>();
        set<String> str_active_divisions = new set<String>();
        //Total Row details Aditya 06/06/2013
        Decimal recCreditHigh = 0;
        Decimal recCreditTotalDue = 0;
        Decimal recCreditPastDue = 0;
        //Total Row details Aditya 06/06/2013
        
        if(str_selected_date != null && str_selected_date != '' && str_selected_date != 'All')
        {
            //system.debug('-------str_selected_date--'+str_selected_date);
            //system.debug('-------date.parse(str_selected_date)--'+date.valueOf(str_selected_date));
            
            lst_payment_history_publish = [SELECT Percent_Current__c,Past_Due__c,Name,Total_Due__c,Information_Date__c,High_Credit__c,
                                                  Group_Id__c,Division_Name__c,
                                                  BSureC_Divisions__c 
                                           FROM BSureC_Payment_History_Publish__c 
                                           WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id
                                           AND Information_Date__c =: date.valueOf(str_selected_date)];
          
          	for(BSureC_Payment_History_Publish__c rec_payment_publish_info : lst_payment_history_publish)
            {
                if(map_payment_history_publish.get(Id.valueOf(rec_payment_publish_info.BSureC_Divisions__c)) == null)
                {
                    map_payment_history_publish.put( Id.valueOf(rec_payment_publish_info.BSureC_Divisions__c), rec_payment_publish_info);
                }
            }
            if(lst_payment_history_publish !=null && lst_payment_history_publish.size()>0)
            {
                
                lst_division_names = [SELECT Id,Name,Division_Name__c 
                                  FROM BSureC_Divisions__c 
                                  WHERE Is_Active__c =: true order by Division_Name__c ASC];
             //   set<String> str_active_divisions = new set<String>();
                
                for(BSureC_Divisions__c objdivisions : lst_division_names)
                {
                    str_active_divisions.add(String.valueOf(objdivisions.id));
                }   
            }
                                               
            for(BSureC_Divisions__c rec_division_names :  lst_division_names)
            {
                BSureC_paymenthistorybean rec_payment_info = new BSureC_paymenthistorybean();
                if(map_payment_history_publish.get(rec_division_names.Id) != null)
                {
                    BSureC_Payment_History_Publish__c rec_paymenthistory_info = map_payment_history_publish.get(rec_division_names.Id);
                    
                    rec_payment_info.divisionname = rec_paymenthistory_info.Division_Name__c;
                    rec_payment_info.groupid = rec_paymenthistory_info.Group_Id__c;
                    rec_payment_info.higcredit = rec_paymenthistory_info.High_Credit__c;
                    rec_payment_info.pastdue = rec_paymenthistory_info.Past_Due__c;
                    rec_payment_info.totaldue = rec_paymenthistory_info.Total_Due__c;
                    /*
                    if(rec_paymenthistory_info.Percent_Current__c != null)
                    rec_payment_info.percentcurrent =Decimal.valueOf(Double.valueOf(rec_paymenthistory_info.Percent_Current__c/100));
                    else
                    rec_payment_info.percentcurrent = Decimal.valueOf( Double.valueOf('0') );
                    */
                    //Total Row details Aditya 06/06/2013
                    if(rec_payment_info.totaldue != null && rec_payment_info.pastdue != null
                    	&& Decimal.valueOf(Double.valueOf(rec_payment_info.totaldue)) > 0)
                    {
                    	rec_payment_info.percentcurrent = Decimal.valueOf(Double.valueOf(((rec_payment_info.totaldue - rec_payment_info.pastdue)/rec_payment_info.totaldue))*100).stripTrailingZeros().setScale(1);
                    }
                    else
                    {
                    	rec_payment_info.percentcurrent = Decimal.valueOf( Double.valueOf('0') );
                    }
                }
                else
                {
                    rec_payment_info.divisionname = rec_division_names.Division_Name__c;
                    rec_payment_info.higcredit = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.totaldue = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.pastdue = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.percentcurrent = Decimal.valueOf( Double.valueOf('0') );
                }
                
                lst_payment_history_publish_view.add(rec_payment_info);
                
                //Total Row details Aditya 06/06/2013
                
                recCreditHigh += rec_payment_info.higcredit;
                recCreditTotalDue += rec_payment_info.totaldue;
                recCreditPastDue += rec_payment_info.pastdue;
                
                if(recCreditTotalDue > 0)
                {
                    total_percent = (((recCreditTotalDue - recCreditPastDue)/recCreditTotalDue)*100).stripTrailingZeros().setScale(1) ;
                }
                else
                {
                    total_percent = 0.00;
                }
                
            } 
            //Total Rows Aditya 06/06/2013
            rec_credit_totals = new BSureC_Payment_History_Publish__c();
            rec_credit_totals.High_Credit__c = recCreditHigh;
            rec_credit_totals.Total_Due__c = recCreditTotalDue;
            rec_credit_totals.Past_Due__c = recCreditPastDue;
            
            /*  
            //system.debug('---totalRow--'+totalRow);  
            totalRow = [SELECT SUM(BSureC_Payment_History_Publish__c.High_Credit__c) SHighcredit,
                                   SUM(BSureC_Payment_History_Publish__c.Total_Due__c) STotaldue,
                                   SUM(BSureC_Payment_History_Publish__c.Past_Due__c) SPastdue
                                   FROM BSureC_Payment_History_Publish__c
                                   WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id
                                   AND Information_Date__c =: date.valueOf(str_selected_date)
                                   AND BSureC_Divisions__c IN: str_active_divisions
                                   GROUP by ROLLUP(Information_Date__c)
                                   ];
            rec_credit_totals = new BSureC_Payment_History_Publish__c();
            //system.debug('---totalRow--'+totalRow);  
            for( Sobject r : totalRow)  
            {
                rec_credit_totals.High_Credit__c = (decimal)(r.get('SHighcredit'));
                rec_credit_totals.Total_Due__c = (decimal)(r.get('STotaldue'));
                rec_credit_totals.Past_Due__c = (decimal)(r.get('SPastdue'));
                if(rec_credit_totals.Total_Due__c != 0.00)
                {
                    total_percent = ((rec_credit_totals.Total_Due__c - rec_credit_totals.Past_Due__c)/rec_credit_totals.Total_Due__c) ;
                }
                else
                {
                    total_percent = 0.00;
                }
                
            }    
          	*/                           
        }
        else
        {
            //system.debug('-------str_selected_date--'+str_selected_date);
            ////system.debug('-------date.parse(str_selected_date)--'+date.valueOf(str_selected_date));
            
            lst_payment_history_publish = [SELECT Percent_Current__c,Past_Due__c,Name,Total_Due__c,Information_Date__c,
                                                  High_Credit__c,Group_Id__c 
                                           FROM BSureC_Payment_History_Publish__c 
                                           WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id];
        }
    }
    
    
    //Modified by VINU PRATHAP
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>    
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,msg);
        ApexPages.addMessage(errormsg);
        return null;
    } 
    
    /// <summary>  
    /// Void methode for retriving the data on page load
    /// </summary>
    public void loaddata()
    {
        lst_selectoptions_info = new List<SelectOption>();
        lst_payment_history_publish_view = new List<BSureC_paymenthistorybean>();
        
        map_payment_history_publish = new Map<Id,BSureC_Payment_History_Publish__c>();
        map_infromationdate = new Map<Date,Date>();
        //Total Row details Aditya 06/06/2013
        Decimal recCreditHigh = 0;
        Decimal recCreditTotalDue = 0;
        Decimal recCreditPastDue = 0;
        //Total Row details Aditya 06/06/2013
        
        lst_payment_history_publish = [SELECT Name,Information_Date__c,Group_Id__c,Division_Name__c 
                                       FROM BSureC_Payment_History_Publish__c 
                                       WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id order by Information_Date__c DESC limit 50000];
        //system.debug('-----lst_payment_history_publish--'+lst_payment_history_publish);
        for(BSureC_Payment_History_Publish__c lstobj : lst_payment_history_publish)
        {
            if(map_infromationdate.get(lstobj.Information_Date__c) == null)
            {
                //system.debug('-----Information_Date__c---'+lstobj.Information_Date__c.format());
                lst_selectoptions_info.add(new SelectOption(String.valueOf(lstobj.Information_Date__c),String.valueOf(lstobj.Information_Date__c.format())));
                map_infromationdate.put(lstobj.Information_Date__c,lstobj.Information_Date__c); 
            }
        } 
        if(lst_payment_history_publish.size() > 0)
        {
            lst_division_names = [SELECT Id,Name,Division_Name__c 
                                  FROM BSureC_Divisions__c 
                                  WHERE Is_Active__c =: true order by Division_Name__c ASC];
            set<String> str_active_divisions = new set<String>();
            
            for(BSureC_Divisions__c objdivisions : lst_division_names)
            {
                str_active_divisions.add(String.valueOf(objdivisions.id));
            }                     
                                  
            lst_payment_history_publish = [SELECT Percent_Current__c,Past_Due__c,Name,Total_Due__c,Information_Date__c,High_Credit__c,
                                                  Group_Id__c,Division_Name__c,
                                                  BSureC_Divisions__c
                                           FROM BSureC_Payment_History_Publish__c 
                                           WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id
                                           AND Information_Date__c =: date.valueOf(lst_selectoptions_info[0].getValue())
                                           //GROUP by ROLLUP(Information_Date__c)
                                           ];
           // //system.debug
            for(BSureC_Payment_History_Publish__c rec_payment_publish_info : lst_payment_history_publish)
            {
                if(rec_payment_publish_info.BSureC_Divisions__c != null && map_payment_history_publish.get(Id.valueOf(rec_payment_publish_info.BSureC_Divisions__c)) == null)
                {
                    map_payment_history_publish.put( Id.valueOf(rec_payment_publish_info.BSureC_Divisions__c), rec_payment_publish_info);
                }
            }                                  
            for(BSureC_Divisions__c rec_division_names :  lst_division_names)
            {
                BSureC_paymenthistorybean rec_payment_info = new BSureC_paymenthistorybean();
                if(map_payment_history_publish.get(rec_division_names.Id) != null)
                {
                    BSureC_Payment_History_Publish__c rec_paymenthistory_info = map_payment_history_publish.get(rec_division_names.Id);
                    
                    rec_payment_info.divisionname = rec_paymenthistory_info.Division_Name__c;
                    rec_payment_info.groupid = rec_paymenthistory_info.Group_Id__c;
                    rec_payment_info.higcredit = rec_paymenthistory_info.High_Credit__c;
                    rec_payment_info.pastdue = rec_paymenthistory_info.Past_Due__c;
                    rec_payment_info.totaldue = rec_paymenthistory_info.Total_Due__c;
                    /*
                    if(rec_paymenthistory_info.Percent_Current__c != null)
                    rec_payment_info.percentcurrent = Decimal.valueOf(Double.valueOf(rec_paymenthistory_info.Percent_Current__c/100));
                    */
                    //Total Row details Aditya 06/06/2013
                    if(rec_payment_info.totaldue != null && rec_payment_info.pastdue != null
                    	&& Decimal.valueOf(Double.valueOf(rec_payment_info.totaldue)) > 0)
                    {
                    	rec_payment_info.percentcurrent = Decimal.valueOf(Double.valueOf(((rec_payment_info.totaldue - rec_payment_info.pastdue)/rec_payment_info.totaldue))*100).stripTrailingZeros().setScale(1);
                    }
                    else
                    {
                    	rec_payment_info.percentcurrent = Decimal.valueOf( Double.valueOf('0') );
                    }
                    
                }
                else
                {
                    rec_payment_info.divisionname = rec_division_names.Division_Name__c;
                    rec_payment_info.higcredit = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.totaldue = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.pastdue = Decimal.valueOf( Double.valueOf('0') );
                    rec_payment_info.percentcurrent = Decimal.valueOf( Double.valueOf('0') );
                }
                
                lst_payment_history_publish_view.add(rec_payment_info);
                //Total Row details Aditya 06/06/2013
                
                recCreditHigh += rec_payment_info.higcredit;
                recCreditTotalDue += rec_payment_info.totaldue;
                recCreditPastDue += rec_payment_info.pastdue;
                
                if(recCreditTotalDue > 0)
                {
                    total_percent = (((recCreditTotalDue - recCreditPastDue)/recCreditTotalDue)*100).stripTrailingZeros().setScale(1);
                }
                else
                {
                    total_percent = 0.00;
                }
            }
            //Total Rows Aditya 06/06/2013
            rec_credit_totals = new BSureC_Payment_History_Publish__c();
            rec_credit_totals.High_Credit__c = recCreditHigh;
            rec_credit_totals.Total_Due__c = recCreditTotalDue;
            rec_credit_totals.Past_Due__c = recCreditPastDue;
            
            /*
            //system.debug('---str_customer_info_id--'+str_customer_info_id); 
            //system.debug('---str_active_divisions--'+str_active_divisions);  
            totalRow = [SELECT SUM(BSureC_Payment_History_Publish__c.High_Credit__c) SHighcredit,
                                   SUM(BSureC_Payment_History_Publish__c.Total_Due__c) STotaldue,
                                   SUM(BSureC_Payment_History_Publish__c.Past_Due__c) SPastdue
                                   FROM BSureC_Payment_History_Publish__c
                                   WHERE BSureC_Customer_Basic_Info__c =: str_customer_info_id
                                   AND Information_Date__c =: date.valueOf(lst_selectoptions_info[0].getValue())
                                   AND BSureC_Divisions__c IN: str_active_divisions
                                   GROUP by ROLLUP(Information_Date__c)
                                   ];
            rec_credit_totals = new BSureC_Payment_History_Publish__c();
            //system.debug('---totalRow--'+totalRow);  
            for( Sobject r : totalRow)  
            {
                rec_credit_totals.High_Credit__c = (decimal)(r.get('SHighcredit'));
                rec_credit_totals.Total_Due__c = (decimal)(r.get('STotaldue'));
                rec_credit_totals.Past_Due__c = (decimal)(r.get('SPastdue'));
                
                if(rec_credit_totals.Total_Due__c != 0.00 && rec_credit_totals.Total_Due__c != null)
                {
                    total_percent = ((rec_credit_totals.Total_Due__c - rec_credit_totals.Past_Due__c)/rec_credit_totals.Total_Due__c) ;
                }
                else
                {
                    total_percent = 0.00;
                }
            } 
            */     
        }
        else
        {
            //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Records Found');
            //ApexPages.addMessage(errormsg);
            showErrorMessage('No Records Found in Payment History');
        }
        //system.debug('------lst_payment_history_publish_view----'+lst_payment_history_publish_view);
    }
    
    public List<BSureC_paymenthistorybean> paymenthistoryview()
    {
        List<BSureC_paymenthistorybean> rec_payment_view = new List<BSureC_paymenthistorybean>();
        loaddata();
        
        rec_payment_view = lst_payment_history_publish_view;
        BSureC_CreditIncreaseSection.grandtotal_eachRow = rec_credit_totals;
        BSureC_CreditIncreaseSection.total_percent = total_percent;
        BSureC_CreditIncreaseSection.lst_selectoptions_infodate = lst_selectoptions_info;
        
        return rec_payment_view;
    }
    
    public List<BSureC_paymenthistorybean> viewonchange(String str_changed_date)
    {
        List<BSureC_paymenthistorybean> rec_payment_view = new List<BSureC_paymenthistorybean>();
        str_selected_date = str_changed_date;
        bindtable();
        
        rec_payment_view = lst_payment_history_publish_view;
        BSureC_CreditIncreaseSection.grandtotal_eachRow = rec_credit_totals;
        BSureC_CreditIncreaseSection.total_percent = total_percent;
        BSureC_CreditIncreaseSection.lst_selectoptions_infodate = lst_selectoptions_info;
        
        return rec_payment_view;
    }
    
}