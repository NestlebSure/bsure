/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CommonUtil {
 
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        // TO DO: implement unit test
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef;        
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);        
        insert lstSettings;
      
      
      //list<BSureC_AdditionalAttributes__c> lstAddAttributes = new list<BSureC_AdditionalAttributes__c>();

      //list<User> userid = new list<User>();
      string userid= apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
      string gettemplate='Default';  
      BSureC_Customer_Basic_Info__c objCustomer=new BSureC_Customer_Basic_Info__c(Customer_Name__c='Nestle BSure Customer');
      insert objCustomer;
      
      
      BSureC_Unauthorized_Deduction_Section__c objUDS=new BSureC_Unauthorized_Deduction_Section__c();
      objUDS.Customer_ID__c=objCustomer.Id;
      insert objUDS;
      
      BSureC_Document_Types__c objDoc=new BSureC_Document_Types__c(Document_Type_Name__c='Reference',Code__c='RR');
      insert objDoc;
      
      Attachment objAttach=new Attachment();
      objAttach.Name='Test';
      objAttach.Body = blob.valueof('my file');
      objAttach.ParentId=objUDS.id;
      insert objAttach;
      
      BSureC_CommonUtil.getDocTypesBSureC();
      //BSureC_CommonUtil.getUCDSCustomerInfo(objCustomer.Id);
      //BSureC_CommonUtil.getUCDSData(objUDS.Id);
      BSureC_CommonUtil.getAttachmentData(objUDS.Id);
      BSureC_CommonUtil.UploadFile(objUDS.Id, blob.valueof('my file'), 'TestName');
      BSureC_CommonUtil.getManagers();
      BSureC_CommonUtil.getZones();
      BSureC_CommonUtil.getSubZones(SubZoneRef.id);
      BSureC_CommonUtil.getCountries(ConRef.id);
      
      BSureC_CommonUtil.getStates(StateRef.id);
      BSureC_CommonUtil.getAnalysts(ZRef.id,SubZoneRef.id,ConRef.id);
      BSureC_CommonUtil.getAnalystlistsSearch(ZRef.id,SubZoneRef.id,ConRef.id);
      BSureC_CommonUtil.getConfigurationValues(cSettings.Name);
      BSureC_CommonUtil.DefaultUserAttributes(userid);
      set<string> struserids = new set<string>();
      struserids.add(userid);
      BSureC_CommonUtil.InsertFollowers(objAttach.id, struserids);
      BSureC_CommonUtil.GetEmailTemplate(gettemplate);
      BSureC_CommonUtil.changeDateFormat('12/04/2012');
      set<id> EmailId = new set<id>();
      EmailId.add(userid);
      Date SendDate;
      SendDate = system.today();
      string ParameterName='BSureC_AnalystRole';
      String RecipientAddress = 'test@123.com';
      
      Profile aeProfileObj=[select Id,Name from Profile where Name='BSureC_Manager'];
      UserRole aeRole = [select Id, Name from UserRole where Name='CEO'];
 
      
      
      User ReceiveUser = new User();
      ReceiveUser.Title = 'Test User';
      ReceiveUser.Username = 'Testse@email.com';
      ReceiveUser.Alias ='test';
      ReceiveUser.Email= 'Test@email.com';
      ReceiveUser.CommunityNickname = 'Test Community';
      //ReceiveUser.UserRole = 'Salesforce';
      //ReceiveUser.Profile ='Test Profile';
      ReceiveUser.Receive_Email_Notifications__c = true;
      ReceiveUser.EmailEncodingKey='UTF-8';
      ReceiveUser.LanguageLocaleKey='en_US';
      ReceiveUser.LocaleSidKey='en_US';
      ReceiveUser.TimeZoneSidKey='America/Los_Angeles';
      ReceiveUser.LastName='TestName';      
      ReceiveUser.ProfileId  = aeProfileObj.id;
      ReceiveUser.UserRoleId = aeRole.id;
      //insert ReceiveUser; 
      
      BSureC_Email_Queue__c objque=new BSureC_Email_Queue__c();
      string strEmailBody = 'Test Mail';
      objque.Email_Body__c=strEmailBody;
      objque.Email_Subject__c ='Test Subject';
      objque.Email_Status__c ='SENT';
      //objque.r
      objque.Is_Daily_Digest__c ='NO';
      objque.Recipient_Address__c=RecipientAddress;
      objque.Send_Immediate__c = true;
      objque.Send_On_Date__c = SendDate;
      objque.Recipient_Name__c = 'Test Name';
      
      insert objque; 
      
      BSureC_CommonUtil.SendEmailToQueue(new set<Id>{UserInfo.getUserId()} ,'Subject', 'Status Update', 'High Priority', SendDate, true);
      BSureC_CommonUtil.getConfigurationValues(ParameterName);
     //getConfigurationValues
      //configSettingsMap
      BSureC_CommonUtil.getBSureCustomerInfo(objCustomer.Id);
      
      
      BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
      insert Bzone;  
      system.Assertequals('Test Supplier Zone',Bzone.Name);  
      Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
      BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
      BSzone.Name = 'Test Supplier Sub Zone';
      BSzone.ZoneID__c = Bzone.Id;
      insert BSzone;
      BSzone= [select id,Name from BSureC_Sub_Zone__c where id=:BSzone.Id];
      BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
      insert BsCountry;
      BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
     
      BSureC_CommonUtil.getSubZones(Bzone.Id);
      BSureC_CommonUtil.getCountries(BsCountry.Id);
      //list<string>lststring =new BSure_Configuration_Settings__c();
      
      Map<String, List<String>> configSettingsMap = new Map<String, List<String>>();
      BSureC_CommonUtil.getCountries(Bzone.Id,BSzone.Id);
      BSureC_CommonUtil.getUserMapwithlist(new set<Id>{UserInfo.getUserId()},'BSureC_Invoice_Collection_Section__c' , system.today().addDays(-30), system.today(), 'Topic');
      
      
    }
}