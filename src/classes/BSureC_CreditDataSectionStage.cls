/************************************************************************************************       
Controller Name 		: BSureC_CreditDataSectionStage       
Date                    : 11/08/2012        
Author                  : Santhosh Palla       
Purpose         		: To Inser the Credit Data Section File into a Custom Object       
Change History 			: Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
						  11/08/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_CreditDataSectionStage 
{
	
	
	webService static void loadbutton()
	{
		/*
		List<BSureC_Credit_Data_Stage__c> ltcrdtdtstg = [SELECT of_Limit__c,Slow__c,Risk_Category__c,Record_Date__c,Prompt__c,
														Last_Review__c,Group_Id__c,Exposure__c,Discount__c,Average_Days_Slow__c,
														Credit_Limit__c,CE__c,Bill_to_Account__c FROM BSureC_Credit_Data_Stage__c];
														
		List<BSureC_Credit_Data_Publish__c>	ltcrdtdtpblsh = new List<BSureC_Credit_Data_Publish__c>();
													
		for(BSureC_Credit_Data_Stage__c  obj : ltcrdtdtstg)
		{
			BSureC_Credit_Data_Publish__c reccrdtdtpblsh = new BSureC_Credit_Data_Publish__c();
			reccrdtdtpblsh.CE__c = obj.CE__c;
			reccrdtdtpblsh.Average_Days_Slow__c = obj.Average_Days_Slow__c;
			reccrdtdtpblsh.Bill_to_Account__c = obj.Bill_to_Account__c;
			reccrdtdtpblsh.Credit_Limit__c = obj.Credit_Limit__c;
			reccrdtdtpblsh.Discount__c = obj.Discount__c;
			reccrdtdtpblsh.Exposure__c = obj.Exposure__c;
			reccrdtdtpblsh.Group_Id__c = obj.Group_Id__c;
			reccrdtdtpblsh.Last_Review__c = obj.Last_Review__c;
			reccrdtdtpblsh.of_Limit__c = obj.of_Limit__c;
			reccrdtdtpblsh.Prompt__c = obj.Prompt__c;
			reccrdtdtpblsh.Record_Date__c = obj.Record_Date__c;
			reccrdtdtpblsh.Risk_Category__c = obj.Risk_Category__c;
			reccrdtdtpblsh.Slow__c = obj.Slow__c;
			
			ltcrdtdtpblsh.add(reccrdtdtpblsh);
		}
		Database.insert(ltcrdtdtpblsh);*/
		
		 BSureC_CreditDataSectionBatch b = new BSureC_CreditDataSectionBatch (); 
      	 database.executebatch(b,100);
	}
}