/***********************************************************************************************
* Controller Name : BSureC_AddEditAdditionalAttributes
* Date : 4th Feb,2013
* Author : Neha Jaiswal
* Purpose : To provide ADD/EditAdditional Attributes for User on Customer Application Admin Tools of Nestle.
* -------------------- ------------------- -------------------------
*/// testing application
global with sharing class BSureC_AddEditAdditionalAttributes {
    public string strSelectedProfile{get;set;}//to hold the selected profile
    public list<selectOption> lstProfiles{get;set;}//select option list for all the customer profile
    public List<user> Users{get;set;}//list of user of selected profile
    public string userView{get;set;}//to assign from param
    public list<Profile> lstProfileAll{get;set;}//list of all profile
    public set<string> strProfile{get;set;}//set of profile to hold
    public integer counter=0;  //keeps track of the offset
    public integer list_size=20; //sets the page size or number of rows
    public integer total_size{get;set;} //used to show user the total size of the list
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public string strMainQuery {get;set;}
    public string bSureString;

  //constructor
    public BSureC_AddEditAdditionalAttributes()
    {
        strSelectedProfile='ALL';
        getlstProfiles();
        getUsersLst();
    }
    
    /// <summary>
    /// getlstProfiles
    /// </summary>
    /// <returns>all profile for BSureCustomer</returns>
    public pageReference getlstProfiles()
    {
    strProfile=new set<string>();
    bSureString = 'BSureC%';
    lstProfiles=new list<selectOption>();
    lstProfiles.add(new SelectOption('ALL','ALL'));
    //NAGA Check if the user has read access on the Users field
    if (Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Profile.isAccessible()){ 
    	lstProfileAll= [Select Id,Name,(select Id,Name from Users)  from Profile  where Name Like : bSureString order By Name Asc];
    }
    map<Id,list<User>> mapUsers = new map<Id,list<User>>();
        for(Profile p:lstProfileAll){   
            mapUsers.put(p.Id,p.Users);
        }
        
    if( lstProfileAll!=null && lstProfileAll.size() >0)
        {  
        for(Profile p:lstProfileAll)
            {
                if(mapUsers.get(p.Id) != null && mapUsers.get(p.Id).size() > 0 ){
                string strpName = p.Name.replace('BSureC_','');  
                lstProfiles.add(new SelectOption(p.Name,strpName));
                strProfile.add(p.Name);
            }
        }
    }
    return null;
  }
 
    public pagereference Cancel(){
        return page.BSureC_Reports;
    }
    /// <summary>
    /// getlstProfiles
    /// </summary>
    /// <returns>the users of the selected profile</returns>
    public PageReference getUsersLst()
    {
     //counter = 0;
     //NxtPageNumber=0;
     //PrevPageNumber=0;
     //NAGA Check if the user has read access on the Users field
      if(!Schema.sObjectType.User.isAccessible())
	  { 
	  	return null;
	  }
     strMainQuery='SELECT Id,Name,LastName,FirstName,phone,UserName,Email,Alias,UserRoleId,'+
                            ' UserRole.Name,Profile.Name,IsActive,EmailEncodingKey,TimeZoneSidKey,'+
                            ' LocaleSidKey,LanguageLocaleKey,CompanyName,Department,City,Country,State'+
                            ' FROM User';
     if(strSelectedProfile!='ALL' && strSelectedProfile!='')
         {
         strMainQuery= strMainQuery + '  WHERE Profile.Name =:strSelectedProfile AND IsActive=true  order by FirstName';
         }
     else 
         {
         strMainQuery= strMainQuery + '  WHERE Profile.Name IN :strProfile  AND IsActive=true  order by FirstName';
         }
    users=Database.query(strMainQuery);
    total_size = users.size();
    if( users.size()>0 ){
        pagenation();
    }else   
        {
          ApexPages.Message myEMsg=new ApexPages.Message(ApexPages.severity.INFO,System.label.BSureC_No_Record_Found);
          ApexPages.addMessage(myEMsg);
         }
    return null;
 }
 
              //Pagenation
    //**********************below methods for pagination******************************************************////
    //based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public pagereference pagenation( ){
        //counter = 0;
        PrevPageNumber=counter+1;
        string strQuery;
        //NAGA querying list size and counter dynamically
        strQuery=strMainQuery+'  limit :list_size  offset :counter';
        users=Database.query(strQuery);
        NxtPageNumber=counter+users.size();
        
        return null;
    }
    
    /// <summary>
    /// getlstProfiles
    /// </summary>
    /// <returns>to navigate the user for the additional attribute page</returns>
   public pageReference userViewPage()
       {
        pagereference pageView = new pagereference('/apex/BSureC_AdditionalAttribute?Id='+userView+'&viewflag=true'); 
        return pageView; 
       }
       
  public PageReference FirstbtnClick() { //user clicked beginning
      counter = 0;
      pagenation();
      return null;
           }
    
    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
  public PageReference previousBtnClick() { //user clicked previous button
      counter -= list_size;
      pagenation();
      return null;
      }
       
    // // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
   public PageReference nextBtnClick() { //user clicked next button
      counter += list_size;
      pagenation();
      return null;
       }
       
      
   // <summary>
   // Below method fires when user clicks on previous button of pagination.
   // </summary>
   // <returns>pagereference</returns>
   public PageReference LastbtnClick() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
     
      pagenation();
      //NxtPageNumber=total_size;
      return null;
       }
       
       
   /// <summary>
  /// Below method is for enabling and disabling the previous button of pagination.
  /// <param name="PreviousButtonEnabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getPreviousButtonEnabled() {
          //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
       }
       
       
   /// <summary>
   /// Below method is for enabling and disabling the nextbutton of pagination.
   /// <param name="NextButtonDisabled"></param>
  /// <returns>boolean</returns>    
  /// </summary>
   public Boolean getNextButtonDisabled() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
      }
 

 
   public Integer getPageSize() {
      return total_size;
      }
 
 
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
      }
       
       
  /// <summary>
  /// Below method gets the total no.of pages.
  /// <param name="TotalPageNumber"></param>
  /// <returns>Integer</returns>    
  /// </summary>
   public Integer getTotalPageNumber() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
  
   } 
 
}