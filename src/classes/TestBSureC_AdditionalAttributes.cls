@isTest(SeeAllData=true)
private class TestBSureC_AdditionalAttributes {

        
     static testMethod void myAnalystTest1()
     {              
        
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Analyst'];
        
        User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
        User objUser2 = new User(Alias = 'bsurean2', Email='cust_analyst@bsurenestle2.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser2', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com2');
         insert objUser2;
        system.runAs(objUser)
        {
            
        apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
        apexpages.currentpage().getParameters().get('viewflag');
       
        
        BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;     
        system.Assertequals('Test Supplier Zone',Bzone.Name);            
        
        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;

        BSureC_Category__c scategory = new BSureC_Category__c();
        scategory.Name = 'Nestle Account';
        system.Assertequals('Nestle Account',scategory.Name);
        insert scategory;                
    
        BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
        
        BSureC_Region__c BsRegion=new BSureC_Region__c(Name='South1');
        system.Assertequals('South1',BsRegion.Name);
        insert BsRegion;            
        
        BSureC_Zone_Manager__c objManager = new BSureC_Zone_Manager__c();
        objManager.Zone__c=Bzone.Id;
        insert objManager;
        
        
        BSureC_AdditionalAttributes__c bSureUserAttributes1 =new BSureC_AdditionalAttributes__c();
        bSureUserAttributes1.BSureC_Zone__c=Bzone.Id;
        bSureUserAttributes1.BSureC_Sub_Zone__c= BSzone.Id;
        bSureUserAttributes1.BSureC_Country__c = BsCountry.Id;
        bSureUserAttributes1.User__c = UserInfo.getUserId();         
        bSureUserAttributes1.Manager__c = objManager.Zone_Manager__c;
        bSureUserAttributes1.BSureC_Region__c=BsRegion.Id;
        bSureUserAttributes1.Region__c=BsRegion.Id;
        insert bSureUserAttributes1;        
    

        BSureC_UserCategoryMapping__c objUCategory = new BSureC_UserCategoryMapping__c();        
        objUCategory.CategoryID__c=scategory.Id;
        insert objUCategory;
		
		BSureC_Customer_Basic_Info__c custobj = new BSureC_Customer_Basic_Info__c();
         custobj.Analyst__c= objUser.id;
         custobj.Customer_Name__c = 'test cust';
         insert custobj;
        list<User> userid = new list<User>();
        
        BSureC_AdditionalAttributes objBsureAA=new BSureC_AdditionalAttributes();
        
        objBsureAA.strZoneId = Bzone.id;
        objBsureAA.strSubZoneId = BSzone.id;
        objBsureAA.strCountryId = BsCountry.Id;
        objBsureAA.strManagerId = objUser2.id;
        objBsureAA.strRegionId = BsRegion.id;
        objBsureAA.view_flag=true;
        objBsureAA.view_Edit_flag=true;
        objBsureAA.visibilityEdit=false;
        objBsureAA.visibilitySingle=true;
        objBsureAA.displaydata();
        objBsureAA.getZones();
        objBsureAA.getSubZones();
        objBsureAA.getCountries();
        objBsureAA.getmanagersonchangeregion();
        objBsureAA.getManagers();        
        objBsureAA.getUserIds(userid);
        objBsureAA.getRoleUsers(new set<string>{UserInfo.getUserName()});
        objBsureAA.selectclick();
        objBsureAA.unselectclick();
        objBsureAA.getunSelectedValues();
        objBsureAA.getSelectedValues();
        objBsureAA.getRegions();
        objBsureAA.strManagerId=bSureUserAttributes1.Manager__c;
        objBsureAA.updatemanager();
        objBsureAA.Save();
        objBsureAA.Cancel();
        objBsureAA.Edit();
     
      }
    }
    static testmethod void Managertest()
    {
    	UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Collector'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureC_Collector'];
        User objUser = new User(Alias = 'bsurean', Email='cust_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com');
        User objUser2 = new User(Alias = 'bsurean2', Email='cust_analyst@bsurenestle2.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_CustUser2', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='cust_analyst@bsurenestle.com2');
         insert objUser2;
        system.runAs(objUser)
        {
             BSureC_Customer_Basic_Info__c custobj = new BSureC_Customer_Basic_Info__c();
         custobj.Collector__c = objUser.id;
         custobj.Customer_Name__c = 'test cust';
         custobj.Collector_Manager__c = objUser2.id;
         custobj.Collector_Manager_Name__c = objUser2.Name;
         insert custobj;
         BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;     
        system.Assertequals('Test Supplier Zone',Bzone.Name);            
        
        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;

        BSureC_Category__c scategory = new BSureC_Category__c();
        scategory.Name = 'Nestle Account';
        system.Assertequals('Nestle Account',scategory.Name);
        insert scategory;                
    
        BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
        
        BSureC_Region__c BsRegion=new BSureC_Region__c(Name='South1');
        system.Assertequals('South1',BsRegion.Name);
        insert BsRegion;            
        
        BSureC_Zone_Manager__c objManager = new BSureC_Zone_Manager__c();
        objManager.Zone__c=Bzone.Id;
        insert objManager;
        
        
        BSureC_AdditionalAttributes__c bSureUserAttributes1 =new BSureC_AdditionalAttributes__c();
        bSureUserAttributes1.BSureC_Zone__c=Bzone.Id;
        bSureUserAttributes1.BSureC_Sub_Zone__c= BSzone.Id;
        bSureUserAttributes1.BSureC_Country__c = BsCountry.Id;
        bSureUserAttributes1.User__c = UserInfo.getUserId();         
        bSureUserAttributes1.Manager__c = objManager.Zone_Manager__c;
        bSureUserAttributes1.BSureC_Region__c=BsRegion.Id;
        bSureUserAttributes1.Region__c=BsRegion.Id;
        insert bSureUserAttributes1; 
        apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
        apexpages.currentpage().getParameters().get('viewflag');
        BSureC_AdditionalAttributes obj = new BSureC_AdditionalAttributes();
        obj.strZoneId = Bzone.id;
        obj.strSubZoneId = BSzone.id;
        obj.strCountryId = BsCountry.Id;
        obj.strManagerId = objUser2.id;
        obj.strRegionId = BsRegion.id;
        obj.save();
        }
    
    } 
 }