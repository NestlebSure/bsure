/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethodz
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_ReviewWorklstMangerAndAnalyst {

    static testMethod void myUnitTest() {
        
        //user creation - kishore
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_Report_Page_Size_tt';
        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size_tt';
        cSettings.Parameter_Value__c = '10';
        lstSettings.add(cSettings);
        insert lstSettings;
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com');
        insert standard;
   
        
        // TO DO: implement unit test
        
        list<BSureC_Customer_Basic_Info__c> lstCustomer=new list<BSureC_Customer_Basic_Info__c>();
        BSureC_Customer_Basic_Info__c ObjCustomer=new BSureC_Customer_Basic_Info__c();
         ObjCustomer.Next_Review_Date__c=system.today().addDays(+30);
         ObjCustomer.Review_Status__c='Scheduled';
        insert ObjCustomer;
        lstCustomer.add(ObjCustomer);
        set<string> usersId=new set<string>();
        usersId.add(ObjCustomer.id);
        
        //create customer : kishore
        BSureC_Customer_Basic_Info__c testObj = new BSureC_Customer_Basic_Info__c();
        testObj.City__c = 'test city';
        testObj.Next_Review_Date__c = system.today();
        testObj.Planned_Review_Date__c = system.today();
        testObj.Customer_Name__c = 'test customer';
        testObj.Analyst__c = standard.Id;
        testObj.Manager__c = standard.Id;
        testObj.Collector_Name__c = 'Cust collec';
        insert testObj;
        system.assertequals('test customer',testObj.Customer_Name__c);
        
        BSureC_Credit_Data_Publish__c objPublish=new BSureC_Credit_Data_Publish__c();
        objPublish.BSureC_Customer_Basic_Info__c=ObjCustomer.Id;
        insert objPublish;
        
       
        BSureC_ReviewWorklistForMangerAndAnalyst.CommonUserClass commonObj=new BSureC_ReviewWorklistForMangerAndAnalyst.CommonUserClass();
        
        BSureC_Payment_History_Publish__c creditHistory=new BSureC_Payment_History_Publish__c();
        creditHistory.BSureC_Customer_Basic_Info__c=ObjCustomer.Id;
        creditHistory.High_Credit__c=12233;
        insert creditHistory;
        system.assertequals(12233,creditHistory.High_Credit__c);
      
      
        BSureC_ReviewWorklistForMangerAndAnalyst objReview=new BSureC_ReviewWorklistForMangerAndAnalyst();        
        objReview.getlstCollectorNames();
        objReview.strSubordinatenames='';
        objReview.displayrecords();
        
        objReview.getTotalPageNumber();
        objReview.getNextButtonDisabled();
        objReview.nextBtnClick();
        objReview.previousBtnClick();
        objReview.LastbtnClick();
        objReview.FirstbtnClick();
        //objReview.LastpageData(2);
        //objReview.pageData(1);
        //objReview.BindData(0);
        objReview.getCustomer();
        objReview.getPreviousButtonEnabled();
        objReview.getPageSize();
        objReview.getPageNumber();
        objReview.ExportCSV();
        objReview.getSortDirection();
        objReview.cancel();
        objReview.SortData();
    }
}