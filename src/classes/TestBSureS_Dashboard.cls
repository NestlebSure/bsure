/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_Dashboard {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_ZoneManagerRole';
        objCSettings1.Parameter_Key__c = 'BSureS_ZoneManagerRole';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        system.assertEquals('BSureS_ZoneManagerRole', objCSettings1.Name);
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureS_SubZoneManagerRole';
        objCSettings2.Parameter_Key__c = 'BSureS_SubZoneManagerRole';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'BSureS_CountryManagerRole';
        objCSettings3.Parameter_Key__c = 'BSureS_CountryManagerRole';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'BSureS_BuyerRole';
        objCSettings4.Parameter_Key__c = 'BSureS_BuyerRole';
        objCSettings4.Parameter_Value__c = 'Buyer';
        insert objCSettings4;
        
        system.assertEquals('BSureS_BuyerRole', objCSettings4.Name);
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'BSureS_analystrole';
        objCSettings5.Parameter_Key__c = 'BSureS_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'BSureS_CEORole';
        objCSettings6.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings6.Parameter_Value__c = 'CEO';
        insert objCSettings6;
        
        system.assertEquals('BSureS_CEORole', objCSettings6.Name);
        
        string strDummy = 'testVariable';
        system.assertEquals('testVariable',strDummy);
        system.Test.startTest();
        BSureS_Dashboard dashObj = new BSureS_Dashboard();
        dashObj.getDashboardID();
        system.Test.stopTest();
    }
}