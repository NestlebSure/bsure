public class BSureS_FindCompanies {

	public string CountryName{get;set;}	
	public string CompanyName{get;set;}
	public string CompanyStatus{get;set;}
	public string CompanyType{get;set;}
	public string OfficeType{get;set;}
	public String SafeNumber {get;set;}
	public String RegNumber {get;set;}
	public String StreetName {get;set;}
	public String City {get;set;}
	public String PostalCode {get;set;}
	public String SearchMode{get;set;}
	public static Map<String, BsureS_CreditSafeCodes__c> mcs;

	public BSureS_FindCompanies(){
	        mcs = new Map<String, BsureS_CreditSafeCodes__c>();
	    }
	 public PageReference SearchCompanies() {
		string strExperianEndpoint ='';
        string strSoapAction ='';
        bSureS_CreditSafe obj = new bSureS_CreditSafe();
	    mcs = BsureS_CreditSafeCodes__c.getAll();
        if(mcs!=null){
            if(mcs.get('EndPointURL')!=null){
                BsureS_CreditSafeCodes__c echVar = mcs.get('EndPointURL');
                strExperianEndpoint = echVar.Message__c ; 
            }
            if(mcs.get('SOAPAction')!=null){
                BsureS_CreditSafeCodes__c echVar = mcs.get('SOAPAction');
                strSoapAction = echVar.Message__c   ;
            }
        }
		string strUsername ='';
		string strPassword ='';
        list<bSureS_ExpCred__c> lstCreSafe =[Select id,Name,Pwd__c,UN__c 
                                                From bSureS_ExpCred__c  
                                                where Credit_Agency__c='Creditsafe' limit 1];
        if(lstCreSafe!=null){
            strUsername =lstCreSafe[0].UN__c;
            strPassword =lstCreSafe[0].Pwd__c;
        } 
		string	StrtXML='';
		string	CondXML='';
		string	EndXML='';
		String XMLSTR='';
		StrtXML ='<?xml version="1.0" encoding="utf-8"?>'+
					'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oper="http://www.creditsafe.com/globaldata/operations"  xmlns:dat="http://www.creditsafe.com/globaldata/datatypes" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">'+
 					'<soapenv:Header/>'+
          			  '<soapenv:Body>'+
          			  	'<oper:FindCompanies>'+
          			  	 '<oper:countries1>'+CountryName +'</oper:countries1>'+
          			  	 	 '<oper:searchCriteria>';
       	if(SafeNumber!=null && SafeNumber!=''){
       		CondXML ='<dat:SafeNumber>'+ SafeNumber+'</dat:SafeNumber>';
       	}
       /*
       	else{
       		CondXML+='<dat:SafeNumber/>';
       	}
       	if(CompanyName!=null && CompanyName!=''){
       		CondXML ='<dat:Name MatchType='+SearchMode+'>'+ CompanyName+'</dat:Name>';
       	}
       	else{
       		CondXML+='<dat:Name/>';
       	} 
       	if(CompanyType!=null && CompanyType!=''){
       		CondXML+='<dat:Type>'+ CompanyType+'</dat:Type>';
       	}
       	else{
       		CondXML+='<dat:Type/>';
       	}
       	if(OfficeType!=null && OfficeType!=''){
       		CondXML+='<dat:OfficeType>'+ OfficeType+'</dat:OfficeType>';
       	}
       	else{
       		CondXML+='<dat:OfficeType/>';
       	}
       	if(CompanyStatus!=null && CompanyStatus!=''){
       		CondXML+='<dat:Status>'+ CompanyStatus+'</dat:Status>';
       	}
       	else{
       		CondXML+='<dat:Status/>';
       	}*/
       			  			  	 	 
  		EndXML = '</oper:searchCriteria>'+
          			  	'</oper:FindCompanies>'+
            		  '</soapenv:Body>'+
 				'</soapenv:Envelope>';  
 				
 		XMLSTR= StrtXML +CondXML+EndXML;
 		system.debug('Final String=='+XMLSTR);
 		HttpRequest req = new HttpRequest();  
        req.setEndpoint(strExperianEndpoint);
        req.setHeader('content-type','text/xml; charset=UTF-8');
        //req.setHeader('SOAPAction',strSoapAction);
        req.setHeader('SOAPAction','http://www.creditsafe.com/globaldata/operations/CompanyDataAccessService/FindCompanies');
        req.setMethod('POST');
        Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setBody(XMLSTR); 
        system.debug('Request-->'+req);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        XmlStreamReader reader;	
        try{
            req.setTimeout(120000); // timeout in milliseconds - this is one minute
            res = http.send(req);
        }
        catch(Exception ex){
           	 system.debug('Exception occured::: '+ex.getMessage()+ex.getStackTraceString());
        }	
        system.debug(res.getStatusCode()+'res===='+res.getBody()); 				
    return null;
    }
	public PageReference Cancel() {
	        return null;
	    }
	public List<SelectOption> getCountryLst() {
	        List<SelectOption> options = new List<SelectOption>();
	        options.add(new SelectOption('',''));
	        options.add(new SelectOption('AF','AF'));
			options.add(new SelectOption('AL','AL'));
			options.add(new SelectOption('AM','AM'));
			options.add(new SelectOption('AS','AS'));
			options.add(new SelectOption('AT','AT'));
			options.add(new SelectOption('AU','AU'));
			options.add(new SelectOption('AZ','AZ'));
			options.add(new SelectOption('BA','BA'));
			options.add(new SelectOption('BD','BD'));
			options.add(new SelectOption('BG','BG'));
			options.add(new SelectOption('BR','BR'));
			options.add(new SelectOption('BY','BY'));
			options.add(new SelectOption('CH','CH'));
			options.add(new SelectOption('CN','CN'));
			options.add(new SelectOption('CZ','CZ'));
			options.add(new SelectOption('DE','DE'));
			options.add(new SelectOption('DK','DK'));
			options.add(new SelectOption('EE','EE'));
			options.add(new SelectOption('CA','CA'));
			options.add(new SelectOption('ES','ES'));
			options.add(new SelectOption('FI','FI'));
			options.add(new SelectOption('FM','FM'));
			options.add(new SelectOption('FR','FR'));
			options.add(new SelectOption('GB','GB'));
			options.add(new SelectOption('GE','GE'));
			options.add(new SelectOption('GU','GU'));
			options.add(new SelectOption('HR','HR'));
			options.add(new SelectOption('HU','HU'));
			options.add(new SelectOption('IE','IE'));
			options.add(new SelectOption('IN','IN'));
			options.add(new SelectOption('IS','IS'));
			options.add(new SelectOption('IT','IT'));
			options.add(new SelectOption('JP','JP'));
			options.add(new SelectOption('KG','KG'));
			options.add(new SelectOption('KH','KH'));
			options.add(new SelectOption('KM','KM'));
			options.add(new SelectOption('KR','KR'));
			options.add(new SelectOption('KZ','KZ'));
			options.add(new SelectOption('LA','LA'));
			options.add(new SelectOption('LI','LI'));
			options.add(new SelectOption('LK','LK'));
			options.add(new SelectOption('LT','LT'));
			options.add(new SelectOption('LU','LU'));
			options.add(new SelectOption('LV','LV'));
			options.add(new SelectOption('ME','ME'));
			options.add(new SelectOption('MH','MH'));
			options.add(new SelectOption('MK','MK'));
			options.add(new SelectOption('MM','MM'));
			options.add(new SelectOption('MP','MP'));
			options.add(new SelectOption('MT','MT'));
			options.add(new SelectOption('MX','MX'));
			options.add(new SelectOption('MY','MY'));
			options.add(new SelectOption('NL','NL'));
			options.add(new SelectOption('NO','NO'));
			options.add(new SelectOption('NP','NP'));
			options.add(new SelectOption('PK','PK'));
			options.add(new SelectOption('PL','PL'));
			options.add(new SelectOption('PLC','PLC'));
			options.add(new SelectOption('PR','PR'));
			options.add(new SelectOption('PT','PT'));
			options.add(new SelectOption('PW','PW'));
			options.add(new SelectOption('RO','RO'));
			options.add(new SelectOption('RS','RS'));
			options.add(new SelectOption('RU','RU'));
			options.add(new SelectOption('SE','SE'));
			options.add(new SelectOption('SI','SI'));
			options.add(new SelectOption('SK','SK'));
			options.add(new SelectOption('TH','TH'));
			options.add(new SelectOption('TJ','TJ'));
			options.add(new SelectOption('TM','TM'));
			options.add(new SelectOption('TW','TW'));
			options.add(new SelectOption('UA','UA'));
			options.add(new SelectOption('US','US'));
			options.add(new SelectOption('UK','UK'));
			options.add(new SelectOption('UZ','UZ'));
			options.add(new SelectOption('VI','VI'));
			options.add(new SelectOption('VN','VN'));
			
	        return options;
	    }
	
}