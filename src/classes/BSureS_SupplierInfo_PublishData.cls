/************************************************************************************************       
Controller Name 		: BSureS_SpendHistory_PublishData       
Date                    : 11/12/2012        
Author                  : B.Anupreethi       
Purpose         		: To Inser the Supplier bulk upload data File into a supplier main Object       
Change History 			: 
Date          				 Programmer                     Reason       
--------------------      -------------------    -------------------------       
 22/03/2012      			B.Anupreethi            	Initial Version
**************************************************************************************************/
global with sharing class BSureS_SupplierInfo_PublishData 
{
	/// <summary>
	/// loadbutton method fires when user clicks on publish button 
	/// in spend history stage list view to execute records and insert as batch 
	/// </summary>
	webService static void loadbutton()
	{
		 BSureS_SupplierDataPublish_Scheduler b = new BSureS_SupplierDataPublish_Scheduler (); 
      	 database.executebatch(b,20);//executing batch class
      	 
	}

}