/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_PaymentHistoryItemstoApprove {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        BSureC_PaymentHistoryItemstoApprove objhist = new BSureC_PaymentHistoryItemstoApprove();    		   
        objhist.str_loginprofile='BsureC_Analyst';
        BSureC_Customer_Basic_Info__c objinfo = new BSureC_Customer_Basic_Info__c();
        objinfo.Customer_Name__c='TestPayment';
        objinfo.Analyst__c=Userinfo.getUserId();
        insert objinfo;
        system.assertEquals('TestPayment', objinfo.Customer_Name__c);
        
        BSureC_Credit_Increase__c objIncrese = new BSureC_Credit_Increase__c();
        objIncrese.Status__c = 'Review' ;
        objIncrese.New_Credit_Limit__c=250000;
        insert objIncrese;
        
    }
}