/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_BSureC_CustomerDocumentTypeReport
 {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
         list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_AnalystRole';
        cSettings.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings);
        BSure_Configuration_Settings__c cSettings1 = new BSure_Configuration_Settings__c();
        cSettings1.Name = 'BSureC_ZoneManager';
        cSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        cSettings1.Parameter_Value__c = 'Zone Manager';
        lstSettings.add(cSettings1);
        BSure_Configuration_Settings__c cSettings2 = new BSure_Configuration_Settings__c();
        cSettings2.Name = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        cSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        lstSettings.add(cSettings2);
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_CountryManager';
        cSettings3.Parameter_Key__c = 'BSureC_CountryManager';
        cSettings3.Parameter_Value__c = 'Country Manager';
        lstSettings.add(cSettings3);
        
        insert lstSettings;
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef;
        
        
        BSureC_Customer_Basic_Info__c BRef=new BSureC_Customer_Basic_Info__c();
                BRef.Customer_Name__c='test customer';
                string StrReg=RegRef.id;
                BRef.Customer_Category__c=CatRef.id;
                BRef.Customer_Region__c=StrReg;
                BRef.Customer_Group_Id__c='Test group';
                BRef.Bill_to_Account__c= 'Test bill';
                BRef.Credit_Account__c='Test Credit Acc';
                BRef.Owner_Ship__c='Public';
                //BRef.Has_Parent__c=true;
                //BRef.Parent_Customer_ID__c=BPRef.id;
                BRef.DND_Financial_Information__c='yes';
                BRef.Last_Financial_Statement_Received__c=system.today();
                BRef.Sole_Sourced__c='yes';
                BRef.Fiscal_Year_End__c=system.today();
                BRef.Review_Status__c='Scheduled';
                BRef.Customers_Zone__c=ZoneRef.id+','+ZRef.id;
                BRef.Customers_Sub_Zone__c=SubZoneRef.id+','+SZoneRef.id;
                BRef.Customer_Validation__c='test';
                BRef.Zone__c=ZoneRef.id;
                BRef.Sub_Zone__c=SubZoneRef.id;
                BRef.Customer_Countries__c=ConRef.id;
                BRef.Country__c=ConRef.id;
                BRef.State__c=StateRef.id;
                BRef.Street_Name_Address_Line_1__c='Test Add1';
                BRef.Street_Name_Address_Line_1__c='';
                //BRef.State_Province__c='Test State';
                BRef.State__c=StateRef.id;
                BRef.City__c='Test City';
                //BRef.City__c='';
                BRef.Postal_Code__c='500081';
                //BRef.Analyst__c=Userinfo.getUserId();
                //BRef.Backup_Analysts__c=Userinfo.getUserId();
                //BRef.Manager__c=Userinfo.getUserId();
                insert BRef;
                 BRef.City__c='Test City2';
                 update BRef;
            system.Assertequals('test customer',BRef.Customer_Name__c);    
        list<BSureC_Document_Types__c> Objdoctypelst=new List<BSureC_Document_Types__c>();
        
        //BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(Customer_Name__c='KingFisher Customer1');
        //insert objCustomer;
        
        BSureC_Document_Types__c Objdoctype=new BSureC_Document_Types__c();
        Objdoctype.Code__c='123';
        Objdoctype.Document_Type_Name__c='TestDoc';
        insert Objdoctype;
        Objdoctypelst.add(Objdoctype);
        
          list<BSureC_Credit_Increase_Section__c> Objcreditinclst=new List<BSureC_Credit_Increase_Section__c>();
          list<BSureC_Unearned_Cash_Discount_Section__c> Objunearnedcashlst=new List<BSureC_Unearned_Cash_Discount_Section__c>();
          list<BSureC_On_Going_Credit_Section__c> Objongoinglst=new List<BSureC_On_Going_Credit_Section__c>();
          list<BSureC_Unauthorized_Deduction_Section__c> Objunauthlst=new List<BSureC_Unauthorized_Deduction_Section__c>();
          list<BSureC_Confidential_Documents_Section__c> ObjConfidoclst=new List<BSureC_Confidential_Documents_Section__c>();
          list<BSureC_Invoice_Collection_Section__c> Objinvoicecollst=new List<BSureC_Invoice_Collection_Section__c>();    
  
     BSureC_Credit_Increase_Section__c Objcreditinc=new BSureC_Credit_Increase_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
     insert Objcreditinc;
     Objcreditinclst.add(Objcreditinc);
     BSureC_Unearned_Cash_Discount_Section__c Objunearnedcash=new BSureC_Unearned_Cash_Discount_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
      insert Objunearnedcash;
      Objunearnedcashlst.add(Objunearnedcash);
     BSureC_On_Going_Credit_Section__c Objongoing=new BSureC_On_Going_Credit_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
      insert Objongoing;
      Objongoinglst.add(Objongoing);
     BSureC_Unauthorized_Deduction_Section__c Objunauth=new BSureC_Unauthorized_Deduction_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
      insert Objunauth;
      Objunauthlst.add(Objunauth);
     BSureC_Confidential_Documents_Section__c ObjConfidoc=new BSureC_Confidential_Documents_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
      insert ObjConfidoc;
      ObjConfidoclst.add(ObjConfidoc);
     BSureC_Invoice_Collection_Section__c Objinvoicecol=new BSureC_Invoice_Collection_Section__c(Customer_ID__c=BRef.id,Document_Type__c=Objdoctype.id);
      insert Objinvoicecol;
      Objinvoicecollst.add(Objinvoicecol);
      
     BSureC_CustomerDocumentTypeReport custdoc=new BSureC_CustomerDocumentTypeReport();
     custdoc.lstDocsObject =Objdoctypelst;
     custdoc.lstcreditanalysissection=Objcreditinclst;
     custdoc.lstUnearnedCash=Objunearnedcashlst;
     custdoc.lstOngoingCredit=Objongoinglst;
     custdoc.lstunauthorizedded=Objunauthlst;
     custdoc.lstconfdocsection=ObjConfidoclst;
     custdoc.lstinvoicecolsection=Objinvoicecollst;
     
     BSureC_CustomerDocumentTypeReport.DocumentWrapper custdocwrap=new BSureC_CustomerDocumentTypeReport.DocumentWrapper();
     BSureC_CustomerDocumentTypeReport.DocumentWrapperdoc custdocwrapdoc=new BSureC_CustomerDocumentTypeReport.DocumentWrapperdoc();
     custdoc.Search();
     custdoc.ExportExcel();
     custdoc.selectclick();
     custdoc.unselectclick();
     custdoc.getunSelectedValues();
     custdoc.getSelectedValues();
     custdoc.CustomerDetails();
     custdoc.getPageNumber();
     custdoc.getPageSize();
     custdoc.getPreviousButtonEnabled();
     custdoc.getNextButtonDisabled();
     custdoc.getTotalPageNumber();
     custdoc.pageData(1);
     custdoc.LastpageData(1);
     custdoc.nextBtnClick();
     custdoc.previousBtnClick();
     custdoc.LastbtnClick();
     custdoc.FirstbtnClick();   
    }
}