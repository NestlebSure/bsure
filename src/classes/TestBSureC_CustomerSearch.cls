/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomerSearch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
        insert RegRef;
        BSureC_Category__c CatRef=new BSureC_Category__c(name='Test Category');
        insert CatRef;
        BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America',IsActive__c=true);
        insert ZoneRef;
        BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA',IsActive__c=true);
        insert ZRef;
        BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SubZoneRef;
        BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America',ZoneID__c=ZoneRef.id,IsActive__c=true);
        insert SZoneRef;
        BSureC_Country__c ConRef=new BSureC_Country__c(name='United States',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert ConRef;
        BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
        insert CRef;
        BSureC_State__c StateRef=new BSureC_State__c(name='Test state',Customer_Country__c=CRef.id,IsActive__c=true);
        insert StateRef;
        
        system.assertEquals('Test state',StateRef.Name);
        
        BSureC_Customer_Basic_Info__c BRef=new BSureC_Customer_Basic_Info__c();
        BRef.Customer_Name__c='test customer';
        string StrReg=RegRef.id;
        BRef.Customer_Category__c=CatRef.id;
        BRef.Customer_Region__c=StrReg;
        BRef.Customer_Group_Id__c='Test group';
        BRef.Bill_to_Account__c= 'Test bill';
        BRef.Credit_Account__c='Test Credit Acc';
        BRef.Owner_Ship__c='Public';
        //BRef.Has_Parent__c=true;
        //BRef.Parent_Customer_ID__c=BPRef.id;
        BRef.DND_Financial_Information__c='yes';
        BRef.Last_Financial_Statement_Received__c=system.today();
        BRef.Sole_Sourced__c='yes';
        BRef.Fiscal_Year_End__c=system.today();
        BRef.Review_Status__c='Scheduled';
        BRef.Customers_Zone__c=ZoneRef.id+','+ZRef.id;
        BRef.Customers_Sub_Zone__c=SubZoneRef.id+','+SZoneRef.id;
        BRef.Customer_Validation__c='test';
        BRef.Zone__c=ZoneRef.id;
        BRef.Sub_Zone__c=SubZoneRef.id;
        BRef.Customer_Countries__c=ConRef.id;
        BRef.Country__c=ConRef.id;
        BRef.State__c=StateRef.id;
        BRef.Street_Name_Address_Line_1__c='Test Add1';
        BRef.Street_Name_Address_Line_1__c='';
        BRef.State__c=StateRef.id;
        BRef.City__c='Test City';   
        BRef.Postal_Code__c='500081';   
        insert BRef;       
         
        system.assertEquals('500081',BRef.Postal_Code__c);

        BSureC_CustomerSearch objcustsrch=new BSureC_CustomerSearch();
        objcustsrch.strCustomerName='Dummy';
        objcustsrch.pageSize=10;
        String assign='1234';
        system.assertequals('1234',assign);
        objcustsrch.Search();
        pagereference pgrefnxtbtn =objcustsrch.nextBtnClick();
        pagereference pgrefprvbtn =objcustsrch.previousBtnClick();
        integer i=objcustsrch.getTotalPageNumber();
        integer j=1;
        objcustsrch.BindData(j);
        objcustsrch.pageData(j);
        objcustsrch.LastpageData(j);
        pagereference pgreflastbtn =objcustsrch.LastbtnClick();
        pagereference pgreffrstbtn =objcustsrch.FirstbtnClick();
    }
}