global with sharing class BSureS_MassTransferComponent {


    @RemoteAction 
    global static SObject[] findSObjects(string obj, string qry, string addFields, string profilename,String ProfileIDs,String uid,String Customerids) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
            List<sObject> L;
                ////system.debug('in if==========================='+ProfileIDs);
       if(profilename==NULL || profilename=='')
       {
        ////system.debug('in if===========================');
            List<String> fieldList=new List<String>(); 
        if (addFields != '')  
        fieldList = addFields.split(',');
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        if(!System.Test.isRunningTest())
        {
            Schema.SObjectType sot = gd.get(obj);
            if (sot == null) 
            {
                return null;
            }
        }
        ////system.debug('qry---'+qry+'--addFields='+addFields);
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';

        List<String> Profilelistcust=BSureS_CommonUtil.getConfigurationValues('BSureS_MassTransferAllowedRoles');
        set<String> Profilelist=new set<String>();
       Profilelist.addAll(Profilelistcust);
       // set<String> Profilelist=new set<String>{'BSureS_Analyst','BSureS_Buyer'};
        ////system.debug('Profilelist=========='+Profilelist);
        String soql='select Name,User.Profile.Name,isActive From User Where Name '+Filter;
        soql+=' AND isActive=true';
        soql+=' AND User.Profile.Name  IN:  Profilelist';
        soql+=' order by Name';
        
        //system.debug('Qry: '+soql);
        
         L = new List<sObject>();
        try 
        {
            //NAGA Check if the user has read access on the User object
	      	if(Schema.sObjectType.User.isAccessible())
		  	{
	            L = Database.query(soql);
		  	}
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
        
      }
      
     else
       {
        ////system.debug('in if===============cat============'+profilename);
        set<String> lstusers=new set<String>();
        set<String> lstfinalusers=new Set<String>();
        String str_catuids='';
        //Creating the filter text 
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //NAGA Check if the user has read access on the BSureS_UserCategoryMapping__c object
	     if(Schema.sObjectType.BSureS_UserCategoryMapping__c.isAccessible())
		{
	        for(BSureS_UserCategoryMapping__c UserCat:[Select UserID__c from BSureS_UserCategoryMapping__c where CategoryID__c=:profilename])
	        {
	            lstusers.add(UserCat.UserID__c);
	            str_catuids+=','+UserCat.UserID__c;
	        }
		}
        ////system.debug('in if===============cat=======lstusers====='+lstusers);
        List<string> prof=new List<String>();
        List<String> customerid=new List<String>();
        if(ProfileIDs.contains(','))
        {
        prof=ProfileIDs.split(',');
        }
        else
        {
        prof.add(ProfileIDs);
        }
        
        set<String> set_managerids=new set<String>();
        set<String> set_adduserids=new set<String>();
        //NAGA Check if the user has read access on the BSureS_User_Additional_Attributes__c object
	     if(Schema.sObjectType.BSureS_User_Additional_Attributes__c.isAccessible())
		{
	        for(BSureS_User_Additional_Attributes__c lst_Objadditional:[SELECT Manager__c,User__c FROM BSureS_User_Additional_Attributes__c WHERE User__c=:uid])
	        {
	            set_managerids.add(lst_Objadditional.Manager__c);
	        }
		}
        //NAGA Check if the user has read access on the BSureS_User_Additional_Attributes__c object
	     if(Schema.sObjectType.BSureS_User_Additional_Attributes__c.isAccessible())
		{
	        for(BSureS_User_Additional_Attributes__c lst_Objadditional:[SELECT Manager__c,User__c FROM BSureS_User_Additional_Attributes__c WHERE Manager__c in : set_managerids])
	        {
	            set_adduserids.add(lst_Objadditional.User__c);
	        }
		}
        
        set<String> set_finaluids=new set<String>();
        for(String str:set_adduserids)
        {
            ////system.debug('inside for=========='+str);
            ////system.debug('str_catuids====insideif====='+str_catuids);
            if(str_catuids.contains(str))
            {
                ////system.debug('=====inside');
                set_finaluids.add(str);
            }
        }
        
        if(Customerids!=NULL)
        {
            if(Customerids.contains(','))
            {
            customerid=Customerids.split(','); 
            }
            else
            {
            customerid.add(Customerids);
            }
        }
         //NAGA Check if the user has read access on the BSureS_Basic_Info__c object
	     if(Schema.sObjectType.BSureS_Basic_Info__c.isAccessible())
		{   ////system.debug('customerid=========='+customerid);
	        for(BSureS_Basic_Info__c ObjSup:[SELECT id,Analyst__c,Supplier_Name__c,Backup_Analysts__c from BSureS_Basic_Info__c where id in :customerid])
	        {
	        if(ObjSup.Analyst__c!=NULL)
	        {
	            if(set_finaluids.contains(ObjSup.Analyst__c))
	            {
	                ////system.debug('in if=====analyst');
	                set_finaluids.remove(ObjSup.Analyst__c);
	            }
	        }
	
	            
	        }
		}
		//NAGA Check if the user has read access on the BSureS_Basic_Info__c object
	     if(Schema.sObjectType.BSureS_Basic_Info__c.isAccessible())
		{
	        for(BSureS_Basic_Info__c ObjSup:[SELECT id,Analyst__c,Supplier_Name__c,Backup_Analysts__c from BSureS_Basic_Info__c where id in :customerid])
	        {
	        if(ObjSup.Backup_Analysts__c!=NULL)
	        {
	            String[] str=ObjSup.Backup_Analysts__c.split(',');
	            for(String Objstr:str)
	            {
	                if(set_finaluids.contains(Objstr))
	                {
	                    set_finaluids.remove(Objstr);
	                }
	            }
	        }
	        }
		}
        //NAGA Check if the user has read access on the User object
	     if(Schema.sObjectType.User.isAccessible())
		{
	        for(User u:[select id,name from user where id in: set_finaluids and ProfileID IN:prof])
	        {
	            lstfinalusers.add(u.id);
	        }
		}
        ////system.debug('in if===============cat=======lstfinalusers====='+lstfinalusers);
        String soql='select id,Name,User.Profile.Name,isActive From User Where Name '+Filter;
        soql+=' AND isActive=true AND id  IN:  lstfinalusers';
        ////system.debug('uid==========='+uid);
        if(uid!=null && uid!='')
        {
            ////system.debug('');
            soql+=' AND id!=:uid'; 
        }
        soql+=' order by Name';
        
        ////system.debug('Qry: '+soql);
        
         L = new List<sObject>();
        try 
        {
        	 //NAGA Check if the user has read access on the User object
	     	if(Schema.sObjectType.User.isAccessible())
			{
	            	L = Database.query(soql);
			}
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
        
      }
      ////system.debug('L========================'+L);
        return L;
   }
}