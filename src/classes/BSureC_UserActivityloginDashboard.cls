/***********************************************************************************************
* Controller Name   : BSureC_UserActivityloginDashboard 
* Date              : 
* Author            : Neha Jaiswal  
* Purpose           : Class for Customer  User Activity Login Dashboard
* Change History    : 07/01/2013
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
*                                           Initial Version
**************************************************************************************************/
public with sharing class BSureC_UserActivityloginDashboard {
     
    public list<user> lstUser{get;set;}  
    public list<BSureC_AdditionalAttributes__c> lstUserAdditionatt{get;set;} 
    public map<string,list<LoginHistory>> mapuserloginHistory{get;set;}
    public BSureC_Customer_Basic_Info__c objCustomer{get;set;} // to display from and to dates with standard datepicker 
    public list<LoginHistory> userloginHistory{get;set;}
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public set<string> setuserId{get;set;} 
    public list<userLHWrapper> lstWrapper {get;set;}
    public datetime dtFromDate{get;set;} // to store from date
    public datetime dtToDate{get;set;} // to strore to Date
    public Boolean idsflag = false;//for commonnutil class
    
    public class userLHWrapper
    {
        public integer uloginCount;
        public string userloginmonth;
    } 
      
    public BSureC_UserActivityloginDashboard() 
    {
        objCustomer = new BSureC_Customer_Basic_Info__c();
        getZones();
        getSubZones();
        getCountries(); 
        getlogedinUser();
        //mapuser = new  map<id,User>([select id,name from User where isActive = true]);
    }
    
    public void getlogedinUser()
    {
        string strUserId= userInfo.getUserId();
        system.debug('strUserId&&&'+strUserId);
        list<BSureC_AdditionalAttributes__c>  objUserDetail=[SELECT User__r.id,User__r.Name,Zone_Name__c,BSureC_Zone__c,
                                                             Sub_Zone_Name__c,BSureC_Sub_Zone__c,Country_Name__c,BSureC_Country__c 
                                                             from BSureC_AdditionalAttributes__c  WHERE User__r.Id =: strUserId];
                                                             
        if(objUserDetail !=null  && objUserDetail.size()>0)
        {
          strZoneId=     objUserDetail[0].BSureC_Zone__c;
          strSubZoneId=  objUserDetail[0].BSureC_Sub_Zone__c;
          strCountryId=  objUserDetail[0].BSureC_Country__c;
          system.debug('strZoneId&&&'+strZoneId);
          system.debug('strSubZoneId&&&'+strSubZoneId);
          system.debug('strCountryId&&&'+strCountryId);
        }
    } 
     /// <summary>
    /// getZOnesOptions(): to display the dropdown list for zones from BSureC_commonutil.
    /// </summary>
   /// <returns> zones option list
    public pageReference getZones()
    {
        zoneOptions=new list<selectOption>();
        BSureC_CommonUtil.blnflag=true;
        zoneOptions=BSureC_CommonUtil.getZones();
        getSubZones();
        //getCustomers();
        getCountries();
        return null;
    }
    
     /// <summary>
    /// getSubzonesoptions(): to display the dropdown list for subzones from BSureC_commonutil.
    /// </summary>
   /// <returns> subzones option list
    public pageReference getSubZones()
    {
        if(idsflag==false){
            
            strSubzoneId='ALL';
            strCountryId='ALL';
        }
        subZoneOptions=new list<selectoption>();
        BSureC_CommonUtil.blnflag=true;
        subZoneOptions=BSureC_CommonUtil.getSubZones(strZoneId);
        getCountries();  
        //strSubzoneId='ALL';
       // getCustomers();
        return null;  
    }
    
    
     /// <summary>
    /// getCountriesOptions(): to display the dropdown list for countries from BSureC_commonutil.
    /// </summary>
   /// <returns> country option list
     public pageReference getCountries()
     {
        if(idsflag==false){
            strCountryId='ALL';
        }
     
         coutryOptions=new list<selectOption>();
         coutryOptions=BSureC_CommonUtil.getCountries(strZoneId, strSubzoneId);
         return null;
      }
       
    public void Search()
    {
        if(objCustomer.Fiscal_Year_End__c != null)
        {
            dtFromDate  = Datetime.newInstance(objCustomer.Fiscal_Year_End__c,Time.newInstance(12, 0, 0, 0));
        }
        if(objCustomer.Next_Review_Date__c != null)
        {
            dtToDate    = Datetime.newInstance(objCustomer.Next_Review_Date__c,Time.newInstance(12, 0, 0, 0));
        }
        getUserLogin();
    }
    public List<User> getUserLogin()
    {
        setuserId= new set<string>();
        lstWrapper=new list<userLHWrapper> ();
        if(Schema.sObjectType.BSureC_AdditionalAttributes__c.isAccessible())
		{ 
	        string strQuery = 'select id,User__c,User__r.Name,Zone_Name__c,BSureC_Zone__c,'+
	                                'Sub_Zone_Name__c,BSureC_Sub_Zone__c,Country_Name__c,BSureC_Country__c '+ 
	                                'from BSureC_AdditionalAttributes__c where User__r.isActive = true ';
	        if(strZoneId != null && strZoneId != 'ALL')
	        {
	            strQuery +=' AND BSureC_Zone__c =: strZoneId ';
	        }
	        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
	        {
	            strQuery +=' AND BSureC_Sub_Zone__c =: strSubZoneId ';
	        }
	        if(strCountryId != null && strCountryId != 'ALL'  )
	        {
	            strQuery +=' AND BSureC_Country__c =: strCountryId '; 
	        }
	        
	        lstUserAdditionatt= Database.Query(strQuery);
		}
        
        list<LoginHistory> lstlHistory = new list<LoginHistory>();
        mapuserloginHistory = new map<string,list<LoginHistory>>();
        
        if(lstUserAdditionatt != null && lstUserAdditionatt.size()>0)
        {
	        for(BSureC_AdditionalAttributes__c objU: lstUserAdditionatt)
	        {
	            setuserId.add(objU.User__c);
	        }
        }
        
        if(Schema.sObjectType.LoginHistory.isAccessible())
		{ 
	         string strLHQuery = 'select UserId,LoginTime FROM LoginHistory where UserId IN : setuserId ';
	         if(dtFromDate != null)
	         {
	            strLHQuery += 'AND LoginTime >=: dtFromDate ';
	         }  
	         if(dtToDate != null)
	         {
	            strLHQuery += 'AND LoginTime <=: dtToDate ';
	         }                      
	         userloginHistory = Database.query(strLHQuery);
		}
		
		if(userloginHistory != null && userloginHistory.size()>0)
		{
	         for(LoginHistory objLoginHistory: userloginHistory)     
	         {
	             String monthH = String.valueOf(objLoginHistory.LoginTime.month());
	             string yearH = string.valueOf(objLoginHistory.LoginTime.year());
	             string strHistory = yearH +'-'+ monthH;
	             
	             if(mapuserloginHistory.containsKey(strHistory))
	             {
	                lstlHistory = mapuserloginHistory.get(strHistory);
	                lstlHistory.add(objLoginHistory);
	                mapuserloginHistory.put(strHistory,lstlHistory);
	             }
	             else
	             {
	                lstlHistory = new list<LoginHistory>();
	                lstlHistory.add(objLoginHistory);
	                mapuserloginHistory.put(strHistory,lstlHistory);
	             }
	         }
        }
        set<string> setmapdata = mapuserloginHistory.keyset();
        list<string> lstlogindata = new list<string>();
        lstlogindata.addall(setmapdata);
        lstlogindata.sort();
        boolean isDummy = false;
        for(string u : lstlogindata)
        { 
            userLHWrapper wObj= new userLHWrapper();
            wObj.uloginCount = mapuserloginHistory.get(u).size();
            wObj.userloginmonth = u;
            lstWrapper.add(wObj);
        } 
        userLHWrapper wObj= new userLHWrapper();
        wObj.uloginCount = 0;
        wObj.userloginmonth = '';
        lstWrapper.add(wObj);
                                                         
        return null;   
    }
}