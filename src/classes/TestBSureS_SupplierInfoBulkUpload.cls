/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_SupplierInfoBulkUpload {

    static testMethod void myUnitTest() 
    {
        //        apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
        
        BSureS_Category__c Category1 = new BSureS_Category__c(Name = 'Category1');
        BSureS_Category__c Category2 = new BSureS_Category__c(Name = 'Category2');
        list<BSureS_Category__c> lstCategores = new  list<BSureS_Category__c>{Category1,Category2};
        insert lstCategores; 
        
        
        BSureS_User_Additional_Attributes__c BUser = new BSureS_User_Additional_Attributes__c(); 
        BUser.User__c = userInfo.getUserId();
        insert BUser;
        
        BSureS_UserCategoryMapping__c UserCategory1 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category1.Id
            );
         BSureS_UserCategoryMapping__c UserCategory2 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category2.Id
            );
        list<BSureS_UserCategoryMapping__c> lstUserC = new list<BSureS_UserCategoryMapping__c>{UserCategory1,UserCategory2};
        insert lstUserC;
        
        system.assertEquals(2,lstUserC.size());
      
        
        BSureS_Zone__c Bzone = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureS_Zone__c where id=:Bzone.Id];
        BSureS_SubZone__c BSzone = new BSureS_SubZone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        BSzone= [select id,Name from BSureS_SubZone__c where id=:BSzone.Id];
        
        BSureS_Country__c BsCountry = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureS_Country__c  where id=:BsCountry.Id];
        
        system.assertEquals('Test Supplier Country',BsCountry.Name);
        
        // TO DO: implement unit test
        system.Test.startTest();
         blob strval=blob.valueof('str1,str2,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,str14,str15,str16,str17,str18,str19,str20,str21,str22\nstr1,str2,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,anu@co.in,anu@co.in,str16,str17,str18,str19,str20,str21,str22');
        BSureS_SupplierInformationBulkUpload objSupplierInformationBulkUpload=new BSureS_SupplierInformationBulkUpload();
        objSupplierInformationBulkUpload.bfilecontent=strval;
        objSupplierInformationBulkUpload.strSCIZoneId = Bzone.Id; 
        objSupplierInformationBulkUpload.strSCISubZoneId = BSzone.Id;
        objSupplierInformationBulkUpload.strSCICountryId = BsCountry.Id;
        objSupplierInformationBulkUpload.mapZones.put(Bzone.Id,Bzone.Name);
        objSupplierInformationBulkUpload.mapSubZones.put(BSzone.Id,BSzone.Name);
        objSupplierInformationBulkUpload.mapCountries.put(BsCountry.Id,BsCountry.Name);
        pagereference pgref = objSupplierInformationBulkUpload.SaveUploadData();
        
        objSupplierInformationBulkUpload.getSZoneList();
        objSupplierInformationBulkUpload.getSSubZoneList();
        objSupplierInformationBulkUpload.getSCountryList(); 
        objSupplierInformationBulkUpload.getSAnalystList();
        objSupplierInformationBulkUpload.getSAnalystManagerList();
        system.Test.stopTest();
    }
    
     static testMethod void myUnitTest1() 
    {
        //        apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
        
        BSureS_Category__c Category11 = new BSureS_Category__c(Name = 'Category1');
        BSureS_Category__c Category21 = new BSureS_Category__c(Name = 'Category2');
        list<BSureS_Category__c> lstCategores1 = new  list<BSureS_Category__c>{Category11,Category21};
        insert lstCategores1; 
        
        
        BSureS_User_Additional_Attributes__c BUser1 = new BSureS_User_Additional_Attributes__c(); 
        BUser1.User__c = userInfo.getUserId();
        insert BUser1;
        
        BSureS_UserCategoryMapping__c UserCategory11 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category11.Id
            );
         BSureS_UserCategoryMapping__c UserCategory21 = new BSureS_UserCategoryMapping__c (
            UserID__c =UserInfo.getUserId(),
            CategoryID__c = Category21.Id
            );
        list<BSureS_UserCategoryMapping__c> lstUserC1 = new list<BSureS_UserCategoryMapping__c>{UserCategory11,UserCategory21};
        insert lstUserC1;
        
        system.assertEquals(2,lstUserC1.size());
      
        
        BSureS_Zone__c Bzone1 = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone1;  
        
        Bzone1 = [select id,Name from BSureS_Zone__c where id=:Bzone1.Id];
        BSureS_SubZone__c BSzone1 = new BSureS_SubZone__c();
        BSzone1.Name = 'Test Supplier Sub Zone';
        BSzone1.ZoneID__c = Bzone1.Id;
        insert BSzone1;
        
        BSzone1= [select id,Name from BSureS_SubZone__c where id=:BSzone1.Id];
        
        BSureS_Country__c BsCountry1 = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone1.Id);
        insert BsCountry1;
        BsCountry1 = [select Id,Name from BSureS_Country__c  where id=:BsCountry1.Id];
        
        system.assertEquals('Test Supplier Country',BsCountry1.Name);
        
        system.Test.startTest();
        // TO DO: implement unit test
         blob strval1=blob.valueof('str1,str2,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,str14,str15,str16,str17,str18,str19,str20,str21,str22 \n NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,anu@co.in,anu@co.in,NULL,NULL,NULL,NULL,NULL,NULL,NULL');
        BSureS_SupplierInformationBulkUpload objSupplierInformationBulkUpload=new BSureS_SupplierInformationBulkUpload();
        objSupplierInformationBulkUpload.bfilecontent=strval1;
        objSupplierInformationBulkUpload.strSCIZoneId = Bzone1.Id; 
        objSupplierInformationBulkUpload.strSCISubZoneId = BSzone1.Id;
        objSupplierInformationBulkUpload.strSCICountryId = BsCountry1.Id;
        objSupplierInformationBulkUpload.mapZones.put(Bzone1.Id,Bzone1.Name);
        objSupplierInformationBulkUpload.mapSubZones.put(BSzone1.Id,BSzone1.Name);
        objSupplierInformationBulkUpload.mapCountries.put(BsCountry1.Id,BsCountry1.Name);
        pagereference pgref = objSupplierInformationBulkUpload.SaveUploadData();
        
        objSupplierInformationBulkUpload.getSZoneList();
        objSupplierInformationBulkUpload.getSSubZoneList();
        objSupplierInformationBulkUpload.getSCountryList(); 
        objSupplierInformationBulkUpload.getSAnalystList();
        objSupplierInformationBulkUpload.getSAnalystManagerList();
        system.Test.stopTest();
    }
}