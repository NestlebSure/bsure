public with sharing class Base62PKext {
    
    public Base62PKext(){
	
	} 
	
	public Base62PKext(Apexpages.Standardcontroller controller){
	}    
    
    @RemoteAction
    public static Id getFirstId() {
    	Id firstId;
    	List<BSureS_Basic_Info__c> loList = [SELECT Id FROM BSureS_Basic_Info__c ORDER BY Id ASC LIMIT 1];
		if (!loList.isEmpty()) {
			firstId = loList[0].Id;
		}
		return firstId;
    }        
    
    @RemoteAction
    public static Id getLastId() {
    	Id lastId;
    	List<BSureS_Basic_Info__c> loList = [SELECT Id FROM BSureS_Basic_Info__c ORDER BY Id DESC LIMIT 1];
		if (!loList.isEmpty()) {
			lastId = loList[0].Id;
		}
		return lastId;
    }            
    
    
	@RemoteAction
	public static List<Map<String, String>> doBase62Chunk(Id firstId, Id lastId, Integer chunkSize) {    
		List<Map<String, String>> rangeListChunked = new List<Map<String, String>>();

			
		if ((firstId != null) && (lastId != null)) {
			//chunk our start and end Ids into batches of specified size
			List<Map<String, String>> chunkList = ChunkService.chunkIdRange(firstId, lastId, chunkSize);
				
			for (Map<String, String> chunk: chunkList) {
				rangeListChunked.add(
					New Map<String, String> {
						'first' => chunk.get('first'),
						'last' => chunk.get('last')
						}
				);	
			}						
		}

		return rangeListChunked;
	}    
	
	
    @RemoteAction
    public static List<BSureS_Basic_Info__c> queryChunk(String firstId, String lastId) {
    	system.debug('firstId==='+firstId+'==lastId=='+lastId);
		String SOQL =  	'SELECT Id, Supplier_Name__c,Review_Status__c ' +
						'FROM BSureS_Basic_Info__c ' +
						'WHERE Id >= \'' + firstId + '\' ' +
						'AND Id <= \''+ lastId +'\'  ';
		system.debug('SOQL==='+SOQL);
		List<BSureS_Basic_Info__c> lstBBInfo = database.query(SOQL);
		system.debug('lstBBInfo==='+lstBBInfo.size());
		return lstBBInfo;
    }    	
    
    
    
}