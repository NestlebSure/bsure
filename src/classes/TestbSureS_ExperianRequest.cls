@isTest
private class TestbSureS_ExperianRequest {
    static testMethod void myUnitTest() {
        System.test.startTest();
      
        BSureS_Basic_Info__c objBasicInfo = new BSureS_Basic_Info__c();
        objBasicInfo.Supplier_Name__c = 'Demo Supplier';
        objBasicInfo.BISFileNumber__c = '7986548975';
        objBasicInfo.Globe_ID__c = '589647';
        objBasicInfo.Planned_Review_Date__c = system.today();
        insert objBasicInfo;
      
        bSureS_ExpCred__c cred = new bSureS_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		cred.Credit_Agency__c='Experian';
		insert cred;
		
        DateTime d = Date.Today() ;
        string dateStr =  d.format('ddMMyy') ;
      
        BSureS_Credit_Review_Section__c objCreditInfo = new BSureS_Credit_Review_Section__c();
        objCreditInfo.Supplier_ID__c = objBasicInfo.id;
        objCreditInfo.TitleDescription__c = dateStr+'_Experian_Rpt_'+objBasicInfo.Globe_ID__c;
        insert objCreditInfo;
      
        bSureS_ExperianRequest objExpReq = new bSureS_ExperianRequest();
        bSureS_ExperianRequest.generateExperianRpt(objBasicInfo.Id,objBasicInfo.BISFileNumber__c,objBasicInfo.Globe_ID__c);
        decimal de = bSureS_ExperianRequest.drop_leading_zeros('001200');
    	map<String,String> mapExpAlias = new map<string,string>();
        map<String,String> mapResultBook = new map<string,string>();
        for(BsureExperian__c objBsureExper:BsureExperian__c.getAll().values()){
            if(objBsureExper!=null){
                if(objBsureExper.NodeName__c!='' && objBsureExper.NodeName__c!='' && objBsureExper.AliasName__c!=null 
                																  && objBsureExper.AliasName__c!=''){
                    mapExpAlias.put(objBsureExper.NodeName__c,objBsureExper.AliasName__c);
                }
            }
         }
         HttpResponse res = new HttpResponse();
         res.setHeader('Content-Type', 'application/xml');
		 res.setBody('<NetConnectResponse xmlns="http://www.experian.com/NetConnectResponse">'+
						'<CompletionCode>0000</CompletionCode>'+
						'<ReferenceId>PPR INQF Experian</ReferenceId>'+
						'<Products xmlns="http://www.experian.com/ARFResponse">'+
							'<PremierProfile>'+
								'<ExpandedCreditSummary>'+
									'<BankruptcyFilingCount>0000</BankruptcyFilingCount>'+
									'<TaxLienFilingCount>0000</TaxLienFilingCount>'+
									'<JudgmentFilingCount>0004</JudgmentFilingCount>'+
									'<LegalBalance>00000006100</LegalBalance>'+
									'<UCCFilings>0018</UCCFilings>'+
									'<CurrentDBT>003</CurrentDBT>'+
									'<CommercialFraudRiskIndicatorCount>0000</CommercialFraudRiskIndicatorCount>'+
								'</ExpandedCreditSummary>'+
								'<ExpandedBusinessNameAndAddress>'+
									'<ProfileDate>20120730</ProfileDate>'+
								'</ExpandedBusinessNameAndAddress>'+
								'<ExecutiveSummary>'+
									'<IndustryDBT>014</IndustryDBT>'+
									'<HighCreditAmountExtended>00000124400</HighCreditAmountExtended>'+
								'</ExecutiveSummary>'+
								'<CorporateLinkage>'+
									'<LinkageRecordType code="1">Ultimate Parent</LinkageRecordType>'+
									'<LinkageRecordBIN>924502359</LinkageRecordBIN>'+
									'<LinkageCompanyName>EXPERIAN PLC</LinkageCompanyName>'+
								'</CorporateLinkage>'+
								'<BusinessFacts>'+
									'<EmployeeSize>000001500</EmployeeSize>'+
								'</BusinessFacts>'+
								'<SICCodes>'+
									'<SIC code="2741">MISCELLANEOUS PUBLISHING</SIC>'+
								'</SICCodes>'+
								'<SICCodes>'+
									'<SIC code="2759">COMMERCIAL PRINTING, NEC</SIC>'+
								'</SICCodes>'+
								'<SICCodes>'+
									'<SIC code="7359">EQUIPMENT RENTAL &amp; LEASING, NEC</SIC>'+
								'</SICCodes>'+
								'<IntelliscoreScoreInformation>'+
									'<PubliclyHeldCompany code="N"/>'+
									'<LimitedProfile code="N"/>'+
									'<Filler>3</Filler>'+
									'<ScoreInfo>'+
										'<Score>0000002900</Score>'+
									'</ScoreInfo>'+
									'<ProfileNumber>I309192566</ProfileNumber>'+
									'<ModelInformation>'+
										'<ModelCode>000223</ModelCode>'+
										'<ModelTitle>FINANCIAL STABILITY RISK</ModelTitle>'+
									'</ModelInformation>'+
									'<PercentileRanking>028</PercentileRanking>'+
									'<Action>TEST LOW-MEDIUM RISK</Action>'+
									'<RiskClass>3</RiskClass>'+
								'</IntelliscoreScoreInformation>'+
								'<IntelliscoreScoreInformation>'+
									'<PubliclyHeldCompany code="N"/>'+
									'<LimitedProfile code="N"/>'+
									'<Filler>01</Filler>'+
									'<ScoreInfo>'+
										'<Score>0000004400</Score>'+
									'</ScoreInfo>'+
									'<ModelInformation>'+
										'<ModelCode>000215</ModelCode>'+
										'<ModelTitle>INTELLISCORE PLUS V2</ModelTitle>'+
										'<CustomModelCode>01</CustomModelCode>'+
									'</ModelInformation>'+
									'<PercentileRanking>043</PercentileRanking>'+
									'<Action>MEDIUM RISK</Action>'+
								'</IntelliscoreScoreInformation>'+
								'<ScoreTrendsCreditLimit>'+
									'<CreditLimitAmount>00000175000</CreditLimitAmount>'+
								'</ScoreTrendsCreditLimit>'+
							'</PremierProfile>'+
						'</Products>'+
					'</NetConnectResponse>');
         res.setStatusCode(200);
         XmlStreamReader reader = res.getXmlStreamReader();
         mapResultBook = objExpReq.parseBooks(reader,mapExpAlias);
    	 string result =  objExpReq.InsertIntoExperain(mapResultBook,objBasicInfo.Id);
         string assign='1234';
         system.assertEquals('1234',assign);
        
         System.test.stopTest();
	}
}