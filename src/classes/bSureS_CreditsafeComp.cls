public with sharing class bSureS_CreditsafeComp {
	public string expResString{get;set;}
	
	public bSureS_CreditsafeComp(){
		Attachment objAttach = new Attachment();
		objAttach = [Select id,Name,parentId,Body From Attachment Where id='00Pd000000gr8mMEAQ'];
		 if(objAttach!=null){
		 		Blob fileBody = objAttach.body;
            expResString = fileBody.toString() ;
		 }
		 
	}
    
}