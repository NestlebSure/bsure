/***********************************************************************************************
*   Controller Name : BSureC_CustomerBasicInformation
*   Date            : 11/2/2012 11:50 PM
*   Author          : A.Manjula(manjula.a@vertexcs.com)
*   Purpose         : Navigates to the New Customer creation page
*   Change History  :
*   Date                       Programmer             Reason
*   --------------------       -------------------    -------------------------
*   11/2/2012 11:50 PM         A.Manjula              Initial Version
*   11/6/2012 4:00PM           A.Manjula              Implementation
*   11/7/2012 11:40PM          A.Manjula              Implementatin
*   11/7/2012 12:05PM          A.Manjula              Issue checking with zone and subzone
*   11/7/2012 1:05PM           A.Manjula              Zone and subzone assignment to the contact info
*   12/11/2012 4:15 PM         A.Manjula              Checking Has parent related issue
*   04/01/2013 4.49 PM         K.Vinu Prathap         Denying the customer to use existing Credit Account Value
*   07/08/2013                 Kishorekumar A         The Credit Account field should be editable in Customer Information,  give provision to copy(ctrl c ) and Paste( ctrl v) .
*   27/08/2013                 Kishorekumar A         Review status update if planned review date is future date (overdue - Scheduled) 
**************************************************************************************************/

//public class BSureC_CustomerBasicInformation
global with sharing class BSureC_CustomerBasicInformation
{
    public string CustomerId{get;set;} // Holds page returned id
    public  BSureC_Customer_Basic_Info__c BSureC_Cust{get;set;} // Holds BSureC_Basic_Info__c object
    public  BSureC_Customer_Basic_Info__c BSureCustInfo{get;set;} // Holds BSureC_Basic_Info__c object
    Public List<string> lstLeft_BSureC_Zone{get;set;} // Holds list of left Zone  values  
    public List<string> lstRight_BSureC_Zone{get;set;} // Holds list of right Zone selected values  
    public List<string> lstLeft_BSureC_SubZone{get;set;} // Holds list of left subZone  values  
    public List<string> lstRight_BSureC_SubZone{get;set;} // Holds list of right subZone selected values 
    public List<string> lstLeft_BSureC_Country {get; set; }   //Country Available
    public List<string> lstRight_BSureC_Country {get; set; }   //Country Assigned 
    public List<SelectOption> BSureCAvailableCountryList = new List<SelectOption>(); 
    public List<SelectOption> BSureCAssignedCountryList = new List<SelectOption>(); 
    public Set<string> setLeftCountryValues = new Set<string>();
    public Set<string> setRightCountryValues = new Set<string>();
    public set<string>  setassignSubZone = new set<string>(); // variable for store selected assigned Subzone vales useful for Edit the record
    public list<string> lstEditCountries = new list<string>();
     
    Set<string> leftZonevalues = new Set<string>(); // Holds set of left zone values
    Set<string> rightZonevalues = new Set<string>(); // Holds set of right zone values
    Set<string> leftSubZonevalues = new Set<string>(); // Holds set of left subzone values
    Set<string> rightSubZonevalues = new Set<string>(); // Holds set of right subzone values
    public List<BSureC_Zone__c> lstBSureZoneRef=new List<BSureC_Zone__c>(); //Holds list of leftzone values
    public List<BSureC_Sub_Zone__c> lstBSureSubZoneRef=new List<BSureC_Sub_Zone__c>(); //Holds list of leftsubzone values
    public List<selectoption> lstavailCountry{get;set;} //Holds list of countries
    public List<selectoption> lstavailState{get;set;} //Holds list of States
    public List<selectoption> lstavailContactZone{get;set;} //Holds list of zones
    public List<selectoption> lstavailContactSubZone{get;set;} //Holds list of subzones
    public string strZonevalue{get;set;} // Holds selected zone value of type string
    public string strSubZonevalue{get;set;} // Holds selected subzone value of type string
    public string strCountryvalue{get;set;} // Holds selected country value of type string
    public string strStatevalue{get;set;} // Holds selected country value of type string
    public boolean custinsertval{get;set;} // Holds boolean value for validating duplicate customers
    public string strParentCustomername{get;set;} // Holds parent customer name
    public string strParentCustid{get;set;} // Holds parent customer id
      
    public List<string> lstSelectedZones = new List<String>(); // Holds selcted zone values
    public boolean leftsubzonerender=true; // Holds boolean value for fetching zone, subzone values
    public boolean parentCustRender{get;set;} // Renders parent customer inputfield
    //public boolean hideplannedreviewdate{get;set;}//TO HIDE PLANNED REVIEW DATE IN EDIT VIEW
    public map<string,string> mapSubZone{get;set;} // Holds subzone values
    
    public Id currentUserId{get;set;} // get current user id
    public string strAnalystID{get;set;} //Variable for get Customer Alalyst ID
    public string strManagerID{get;set;} //Variable for get Customer Manager ID
    public string strBackUpAnalystsID{get;set;} //Variable for get Customer Backup Alalysts ID
    
    public list<BSureC_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureC_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureC_Country_Manager__c> lstCountryManager{get;set;}
    public set<id> setuserIds= new set<id>();
    public list<id> lstbackupAnalystids{get;set;}     
    public string strCEO{get;set;}     
    public boolean strRegionReadOnly{get;set;} 
    public boolean blncustomerEdit{get;set;}
    public list<BSureC_Credit_Increase__c> lstCreditIncrease{get;set;}   
    public string CustomerdirectId{get;set;}
    public boolean blncredit{get;set;}
    /// <summary>
    ///  BSureC_CustomerBasicInformation contructur is invoked when the page is loaded
    /// </summary>
    /// <param name="controller"></param>
    public BSureC_CustomerBasicInformation(ApexPages.StandardController controller)
    {
        //hideplannedreviewdate=false;
        CustomerId=Apexpages.currentPage().getParameters().get('id');
        //system.debug('===customerid==='+CustomerId);
        mapSubZone=new map<string,string>();
        lstLeft_BSureC_Zone = new List<string>();
        lstRight_BSureC_Zone = new List<string>();
        lstLeft_BSureC_SubZone = new List<string>();
        lstRight_BSureC_SubZone = new List<string>();
        leftZonevalues=new Set<string>();
        RightZonevalues=new Set<string>();
        leftSubZonevalues=new Set<string>();
        RightSubZonevalues=new Set<string>();
        blncredit = true;
        //BSureCAvailableSubZoneList=new List<SelectOption>();
        BSureCustInfo=new   BSureC_Customer_Basic_Info__c();
        lstCreditIncrease=new list<BSureC_Credit_Increase__c>();
        strParentCustomername='';
        strParentCustid='';
        if(CustomerId!=null)
        {
            //system.debug('===custid if==='+CustomerId);
            
              try
              {
              for(  BSureC_Customer_Basic_Info__c BSureCust:[SELECT id,Bill_to_Account__c,Contact_name__c,Credit_Account__c,Customer_Group_Id__c,Country_Name__c, 
                                                          country__c, Customer_ID__c,Customer_Name__c,Customer_Region__c,
                                                          DND_Financial_Information__c,City__c,Postal_Code__c,BISFileNumber__c,
                                                          Customer_Contact__c,Email_address__c,Phone_Number__c,Fax__c,Website_Link__c,Fiscal_Year_End__c,
                                                          Has_Parent__c,Last_Financial_Statement_Received__c,Owner_Ship__c, Parent_Customer_ID__c,
                                                          Parent_Customer_Name__c,Review_Status__c,Sole_Sourced__c,Customer_Category__c,State__C,
                                                          Customers_Sub_Zone__c,Customers_Zone__c,Customer_Region_Name__c,Notification_Flag__c,
                                                          Zone__c, Sub_Zone__c,Street_Name_Address_Line_1__c , Address_Line_2__c,
                                                          GMC__c, NAICS__c, Rebate_Disc__c, Facility_Expiration__c, Facility_S__c, Credit_Card__c,
                                                          Analyst__c, Backup_Analysts__c, Manager__c, Customer_Countries__c,Planned_Review_Date__c
                                                          ,Company_Registration_ID__c,Credit_Account_NWNA__c,Credit_Account_NPPC__c
                                                          FROM  BSureC_Customer_Basic_Info__c
                                                          WHERE id=:CustomerId])
              {
                    BSureCustInfo=BSureCust;
              }
                  
                  //If the Review Status = Started the Next Review Date is not editable within the Last Review section.
                  lstCreditIncrease=[select id,Status__c from BSureC_Credit_Increase__c where Id =:CustomerId and Status__c = 'Started'];
                  //system.debug('lstCreditIncrease======size====='+lstCreditIncrease.size());
                  if(lstCreditIncrease != null && lstCreditIncrease.size() > 0)
                  {
                        //system.debug('testEditNDate==test=========='+blncustomerEdit);
                        blncustomerEdit = false;
                  }
                    //system.debug('testEditNDate======123======'+blncustomerEdit);             
              }
              catch(Exception ex)
              {
               //system.debug('===ex==='+ex);
               }
        }
       /* else
        {
            //parentCustRender=false;  
            BSureCustInfo=new BSureC_Customer_Basic_Info__c();
            //hideplannedreviewdate=true; 
            ////system.debug('===hideplannedreviewdate==='+hideplannedreviewdate);               
        }*/
         
        List<string> LftZone=new List<string>();
        List<string> LftSubZone=new List<string>();
        if(BSureCustInfo.Parent_Customer_ID__c!=null)
        {
            strParentCustomername=BSureCustInfo.Parent_Customer_Name__c;
            strParentCustid=BSureCustInfo.Parent_Customer_ID__c;
        }
        

       string strStatevalue1;
       if(BSureCustInfo.id!=null)
        {
            //system.debug('===inside if==='+BSureCustInfo);
            if(BSureCustInfo.Zone__c!=null || BSureCustInfo.Zone__c!='')
            {
              strZonevalue=BSureCustInfo.Zone__c;
              //system.debug('==in=strZonevalue==='+strZonevalue);
            }
            
            if(BSureCustInfo.Sub_Zone__c!=null || BSureCustInfo.Sub_Zone__c!='')
            {
              strSubZonevalue=BSureCustInfo.Sub_Zone__c;
              //system.debug('strSubZonevalue**'+strSubZonevalue);
            }
            if(BSureCustInfo.Country__c!=null || BSureCustInfo.Country__c!='')
            {           
               strCountryvalue=BSureCustInfo.Country__c;
               //system.debug('strCountryvalue**'+strCountryvalue);
            }
            if(BSureCustInfo.State__C!=null || BSureCustInfo.State__C!='')
            {
              strStatevalue=BSureCustInfo.State__c;
              strStatevalue1=strStatevalue;
            }
        }
 //*******************************************************************************************       
        
        lstavailContactZone=BSureC_CommonUtil.getZones();
        lstavailContactSubZone=BSureC_CommonUtil.getSubZones(strZonevalue);
        lstavailCountry=BSureC_CommonUtil.getCountries(strSubZonevalue);
        lstState();
        if(BSureCustInfo.id!=null){
        strStatevalue=strStatevalue1;
        }
    }
        
    /// <summary>
    /// getBSureCustInfo returns customer information to the page
    /// </summary>
    /// <returns>BSureC_Customer_Basic_Info__c</returns> 
      public    BSureC_Customer_Basic_Info__c getBSureCustInfo()
      {
            return BSureCustInfo;
      }
      
    /// <summary>
    /// BSureC_ParentCustomer renders Parent customer record
    /// </summary>
    /*public void BSureC_ParentCustomer()
    {
        if(BSureCustInfo.Has_Parent__c==true)
        {
            parentCustRender=true;             
        }
        else
        {       
            parentCustRender=false;             
        }
    }*/
 
    public void GetZones()
    {
        lstContactZone();
    }
    public void GetSubZones()
    {
        lstContactSubZone();
    }
    public void GetCountries()
    {
        lstCountry();
    }
    public void GetStates()
    {
        lstState();
    }    
       /// <summary>
    /// getZones returns Contacts Zone
    /// </summary>
    public pageReference lstContactZone()
    {   
        
        strSubZonevalue='Select';
        strCountryvalue='Select';
        strStatevalue='Select';
        lstavailContactZone = new List<SelectOption>();
        lstavailContactZone.add(new selectoption('Select','Select'));
        
            
        for(BSureC_Zone__c BSCZoneRef:[SELECT id, name, IsActive__c from BSureC_Zone__c where IsActive__c=:true order by Name])
        {
               
               lstavailContactZone.add(new selectoption(BSCZoneRef.id,BSCZoneRef.name));         
               
        }
        lstContactSubZone();
        return null;
       
        
    }
    
    /// <summary>
    /// getSubZones returns Contacts Sub-Zone
    /// </summary>
    public pageReference lstContactSubZone()
    {
        
        lstavailContactSubZone = new List<SelectOption>();
        strSubZonevalue='Select';
        strCountryvalue='Select';
        strStatevalue='Select';
        lstavailContactSubZone.add(new selectoption('Select','Select'));
        if( strZonevalue!= null && strZonevalue!='' && strZonevalue!= 'Select')
        {
        for(BSureC_Sub_Zone__c SZoneRef:[SELECT id, name, ZoneID__c, Zone_Name__c, IsActive__c from BSureC_Sub_Zone__c where IsActive__c=:true and ZoneID__c=:strZonevalue order by Name])
            {
                
                lstavailContactSubZone.add(new selectoption(SZoneRef.id,SZoneRef.name));
                
            }  
        }           
        lstCountry();
        return  null;
        
        
    }
    

        /// <summary>
    /// getCountries returns Contacts Country
    /// </summary>
    public pageReference lstCountry()
    {
        lstavailCountry = new List<SelectOption>();
        strCountryvalue='Select';
        strStatevalue='Select';
        lstavailCountry.add(new selectoption('Select','Select'));
        if( strSubZonevalue!= null && strSubZonevalue!='' && strSubZonevalue!= 'Select')
        {
        for(BSureC_Country__c BSureCCountry:[SELECT id, name, Sub_Zone_ID__c from BSureC_Country__c where IsActive__c=:true and Sub_Zone_ID__c=:strSubZonevalue order by Name])
        {
            
            lstavailCountry.add(new selectoption(BSureCCountry.id,BSureCCountry.name));
            
        }   
        }  
        lstState(); 
        return  null;
        
    }
    
    /// <summary>
    /// getStates returns Contacts States
    /// </summary>
    public pageReference lstState()
    {
        lstavailState = new List<SelectOption>();
        strStatevalue='Select';
        lstavailState.add(new selectoption('Select','Select'));
        if(strCountryvalue!=null&&strCountryvalue!=''&&strCountryvalue!='Select')
        for(BSureC_State__c BSureCState:[SELECT id, name, Customer_Country__c from BSureC_State__c where IsActive__c=:true and Customer_Country__c=:strCountryvalue order by Name])
        {
            
            lstavailState.add(new selectoption(BSureCState.id,BSureCState.name));
        }
        return  null;
        
    }
    
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
    
    /// <summary>
    /// SaveBSureCustomer method creates the customer information
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference SaveBSureCustomer()
    {
        pageReference CustRef;
        Integer CreditAccDupCheck = 0;
        Integer CreditbILLDupCheck = 0;
        string errmsg=null;
        custinsertval=false;
        string custvalidate='';
        Database.SaveResult CustInfoInsert;
        String strCreditamt = BSureCustInfo.Credit_Account__c;
        if(strCreditamt != null && strCreditamt != '')
        {
            blncredit = strCreditamt.isNumeric();
        }   
        //system.debug('===blncredit==='+blncredit);
        //system.debug('===strParentCustomername==='+strParentCustomername);
        //system.debug('===BSureCustInfo.CustomerName==='+BSureCustInfo.Customer_Name__c);
        //system.debug('===BSureCustInfo.CreditAccount==='+BSureCustInfo.Credit_Account__c);
        //system.debug('===BSureCustInfo.Customer_Category__c==='+BSureCustInfo.Customer_Category__c);
         if(BSureCustInfo.Customer_Name__c=='' || BSureCustInfo.Customer_Name__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Customer_Name);
        }
        else if(BSureCustInfo.Customer_Category__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_CategoryAlert);
        }
        else if(BSureCustInfo.Customer_Region__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_RegionAlert);
        }
        else if(BSureCustInfo.Customer_Group_Id__c=='' || BSureCustInfo.Customer_Group_Id__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Customer_Group_Id);            
        }
        
        else if(BSureCustInfo.Credit_Account__c==''  || BSureCustInfo.Credit_Account__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Credit_account);
        }   

        else if(BSureCustInfo.Credit_Account__c != null && blncredit == false)
        {
            custinsertval=true;
            showErrorMessage('Invalid Credit Account');
        }
        else if(BSureCustInfo.Planned_Review_Date__c==null )
        {           
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Please_select_the_Planned_Review_Date);
        }
        else if(BSureCustInfo.Review_Status__c != 'Overdue' && BSureCustInfo.Planned_Review_Date__c!=null && BSureCustInfo.Planned_Review_Date__c < system.today())
        {           
            custinsertval=true;
            showErrorMessage('Planned Review Date should not be Past Date.');
        }
        else if(BSureCustInfo.DND_Financial_Information__c==''  || BSureCustInfo.DND_Financial_Information__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_DND_Financial_Information);
        }
        
         
        else if(BSureCustInfo.Has_Parent__c==true && strParentCustid=='')
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Has_Parent);
        }
        else if(strZonevalue=='Select')
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_ZoneAlert);
        }
        else if(strSubZonevalue=='Select')
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_SubZoneAlert);
        }
        
        else if(BSureCustInfo.Street_Name_Address_Line_1__c==''  || BSureCustInfo.Street_Name_Address_Line_1__c==null )
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Address_Line_1);
        }   
        else if(strCountryvalue=='Select')
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_CountryAlert);
        }
        else if(strStateValue=='Select')
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_StateAlert);
        }
            
        else if(BSureCustInfo.City__c ==''  || BSureCustInfo.City__c==null)
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_CityAlert);            
        }
        else if(BSureCustInfo.Credit_Card__c != null  && string.valueof(BSureCustInfo.Credit_Card__c).length() != 16 )
        {
            custinsertval=true;
            showErrorMessage(system.label.BSureC_Credit_Card_validation);                    
        }

        
        else if(BSureCustInfo.NAICS__c != null && (string.valueof(BSureCustInfo.NAICS__c).length() < 2 || string.valueof(BSureCustInfo.NAICS__c).length() > 6))
        {
            custinsertval=true;
            showErrorMessage(system.label.BsureC_Naics_Validation);
        }       
        

        
         if(BSureCustInfo.Credit_Account__c !=''  && BSureCustInfo.Credit_Account__c!=null)
        {      
            CreditAccDupCheck = [SELECT count() from BSureC_Customer_Basic_Info__c where Credit_Account__c =:BSureCustInfo.Credit_Account__c and id!=:CustomerId];
               //system.debug('testvalue'+CreditAccDupCheck);
               //system.debug('testvalue1'+BSureCustInfo.Credit_Account__c);
               //system.debug('testvalue2'+CustomerId);
               if(CreditAccDupCheck>0)
               {
                    custinsertval=true;
                    showErrorMessage(system.label.BSureC_Credit_Account_Exists); 
               }
        } 
          if(BSureCustInfo.Bill_to_Account__c !=''  &&  BSureCustInfo.Bill_to_Account__c!=null)
        {      
            CreditbILLDupCheck = [SELECT count() from BSureC_Customer_Basic_Info__c where Bill_to_Account__c =:BSureCustInfo.Bill_to_Account__c and id!=:CustomerId];
               if(CreditbILLDupCheck>0)
               {
                    custinsertval=true;  
                    showErrorMessage(system.label.BSureC_BillToAccountExits); 
               }
        } 
        //system.debug('SaveBSureCustomer strCountryvalue***'+strCountryvalue);
        if(BSureCustInfo.Customer_Name__c!=null && strStatevalue!=null && BSureCustInfo.City__c!=null)
        {
        custvalidate=BSureCustInfo.Customer_Name__c+'||'+strStatevalue+'||'+BSureCustInfo.City__c;
        Integer CustomerInf=[select count() from BSureC_Customer_Basic_Info__c where Customer_Validation__c=:custvalidate and id!=:CustomerId];
        if(CustomerInf>0)
        {
            custinsertval=true;
            showErrorMessage(system.label.CustomerNameValidation);
        }
        }
        
        BSureCustInfo.Customer_Validation__c=custvalidate;
        //system.debug('===strParentCustid==='+strParentCustid);
        if(BSureCustInfo.Has_Parent__c==true && strParentCustid!=null && strParentCustid!='')
        {
            BSureCustInfo.Parent_Customer_ID__c=strParentCustid;
            //system.debug('====strParentCustid===='+strParentCustid);
            //system.debug('====BSureCustInfo.Parent_Customer_ID__c===='+BSureCustInfo.Parent_Customer_ID__c);
        }
        if(BSureCustInfo.Has_Parent__c==false)
        {
            BSureCustInfo.Parent_Customer_ID__c=null;
        }
        
        //system.debug('strZonevalue====='+strZonevalue);
        //system.debug('strSubZonevalue====='+strSubZonevalue);
        //system.debug('strCountryvalue====='+strCountryvalue);
        //system.debug('strStatevalue====='+strStatevalue);
        if(strZonevalue!=null && strZonevalue!='Select')
        {
        BSureCustInfo.Zone__c=strZonevalue;
        }
        if(strSubZonevalue!=null && strSubZonevalue!='Select')
        {
         BSureCustInfo.Sub_Zone__c=strSubZonevalue;
        }
        if(strCountryvalue!=null && strCountryvalue!='Select')
        {
          BSureCustInfo.Country__c=strCountryvalue;
        }
        if(strStatevalue!=null && strStatevalue!='Select')
        {
        BSureCustInfo.State__c=strStatevalue;
        }        
        ////system.debug('ReviewStatusbefore.......'+BSureCustInfo.Review_Status__c);
        //kishroe added below code for update the review status like scheduled 
        if(BSureCustInfo.Review_Status__c != null && BSureCustInfo.Review_Status__c == 'Overdue')
        {
            if(BSureCustInfo.Planned_Review_Date__c >= system.today())
            {
                BSureCustInfo.Review_Status__c = 'Scheduled';
            }
        }
                        
        if(!custinsertval)
        {
            ////system.debug('===BSureCustInfo==='+BSureCustInfo);
            if(CustomerId==null || CustomerId=='')
            {
                //CustInfoInsert= Database.Insert(BSureCustInfo,false);
                insert BSureCustInfo;
                CustRef=new PageReference('/'+BSureCustInfo.id);
                
                ////system.debug('=Insert==CustInfoInsert==='+CustInfoInsert);
            }
            else
            {
                CustInfoInsert= Database.Update(BSureCustInfo,false);
                
                if(CustInfoInsert.isSuccess())
                {
                    CustomerdirectId = CustInfoInsert.getId();
                    CustRef=new PageReference('/'+CustomerdirectId);
                }
                ////system.debug('===Update====CustInfoInsert==='+CustInfoInsert);
                //CustRef=new PageReference('/'+CustInfoInsert.id);
            }
            
            return CustRef;
        }
         return null;
        
                
   }  

   
    /// <summary>
    /// redirect method redirect to customer information page based on the user information in customer
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference redirect()
    {
       /* Commented by Santhosh P on 31 Jan 2012
       currentUserId = UserInfo.getUserId();
        List<user> AdminProfile = [select id from user where Profile.Name='System Administrator' and id =:currentUserId];
        //system.debug('AdminProfile====='+AdminProfile); 
        //system.debug('currentUserId=========='+currentUserId);
        if(BSureCustInfo.Analyst__c != null )
        {
            strAnalystID = BSureCustInfo.Analyst__c;
        }
        if(BSureCustInfo.Manager__c != null)
        {
            strManagerID = BSureCustInfo.Manager__c;
        }
        if(BSureCustInfo.Backup_Analysts__c != null)
        {
            strBackUpAnalystsID = BSureCustInfo.Backup_Analysts__c;
        } 
        //system.debug('currentUserId====1======'+currentUserId);
        //system.debug('strAnalystID=====1======'+strAnalystID);*/
        
        lstZoneManager = new list<BSureC_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureC_Sub_Zone_Manager__c>();
        lstCountryManager = new list<BSureC_Country_Manager__c>();
        currentUserId = UserInfo.getUserId();
        //system.debug('BSureCustInfo.Zone__c=============='+BSureCustInfo.Zone__c);
        //system.debug('BSureCustInfo.Sub_Zone__c=============='+BSureCustInfo.Sub_Zone__c);
        //system.debug('BSureCustInfo.Country__c========='+BSureCustInfo.Country__c);
        if(BSureCustInfo.Zone__c != null)
        {
            lstZoneManager = [select Zone_Manager__c from BSureC_Zone_Manager__c where Zone__c =: BSureCustInfo.Zone__c];
        }
        if(lstZoneManager != null && lstZoneManager.size() > 0)
        {
            for(BSureC_Zone_Manager__c objZM: lstZoneManager)
            {
                setuserIds.add(objZM.Zone_Manager__c);
            }
        }
        if(BSureCustInfo.Sub_Zone__c != null)
        {
            lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureC_Sub_Zone_Manager__c where Sub_Zone__c =: BSureCustInfo.Sub_Zone__c];
        }
        if(lstSubZoneManager != null && lstSubZoneManager.size() > 0)
        {
            for(BSureC_Sub_Zone_Manager__c objSZM:lstSubZoneManager)
            {
                setuserIds.add(objSZM.Sub_Zone_Manager__c);
            }
        }
        if(BSureCustInfo.Country__c != null)
        {
            lstCountryManager = [select Country_Manager__c from BSureC_Country_Manager__c where Country__c =: BSureCustInfo.Country__c];
        }
        if(lstCountryManager != null && lstCountryManager.size() > 0)
        {
            for(BSureC_Country_Manager__c objCM:lstCountryManager)
            {
                setuserIds.add(objCM.Country_Manager__c);
            }
        }
        /*Edit link 
        currentUserId = UserInfo.getUserId();
        List<user> AdminProfile = [select id from user where Profile.Name='System Administrator' and id =:currentUserId];
        if(AdminProfile != null && AdminProfile.size() > 0)
        {
            setuserIds.add(AdminProfile.get(0).id);
        }*/
        
        currentUserId = UserInfo.getUserId();
        strCEO = BSureC_CommonUtil.getConfigurationValues('BSureC_CEORole').get(0);
        
        //system.debug('AdminRole........'+strCEO);
        list<User> AdminRole = [select id from User where Userrole.Name =:strCEO and Id =:currentUserId];
        if((AdminRole != null && AdminRole.size() > 0 ) && AdminRole.get(0).id == currentUserId)
        {
            strRegionReadOnly = true;           
            setuserIds.add(AdminRole.get(0).id);
        }       
        //Added by satish on May 28th 2014
        List<Profile> lst_profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
        
        if(lst_profile !=null && lst_profile.size() >0 && lst_profile[0].Name == 'BSureC_Analyst'){
            
            setuserIds.add(UserInfo.getUserId());
            strAnalystID = BSureCustInfo.Analyst__c;
        }        
        /*if(BSureCustInfo.Analyst__c != null )
        {
            setuserIds.add(BSureCustInfo.Analyst__c);
            strAnalystID = BSureCustInfo.Analyst__c;
        }*/
        if(BSureCustInfo.Manager__c != null)
        {
            
            setuserIds.add(BSureCustInfo.Manager__c);
            strManagerID = BSureCustInfo.Manager__c;
        }
        /*if(BSureCustInfo.Backup_Analysts__c != null)
        {
            if(BSureCustInfo.Backup_Analysts__c.contains(','))
            {
                lstbackupAnalystids = BSureCustInfo.Backup_Analysts__c.split(',');
                setuserIds.addall(lstbackupAnalystids);
            }
            else
            {
                setuserIds.add(BSureCustInfo.Backup_Analysts__c);
            }
            
        }*/ 
      //   if(currentUserId == strAnalystID || currentUserId == strManagerID ||(AdminProfile.size() > 0 && currentUserId == AdminProfile.get(0).id)) 
      
        if(setuserIds.contains(currentUserId))
        {   
            return null;         
             
        }else 
        {   
            return redirectDetailPage();
        }
        return null; 
    }
    
    /// <summary>
    /// redirectDetailPage method redirect to customer information detail page
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference redirectDetailPage()
    {
        PageReference pageRefDetail;
        if(CustomerId != null)
        {
            if(CustomerdirectId != null && CustomerdirectId != '')
            {
                pageRefDetail = new PageReference('/apex/BSureC_ViewCustomerDetails?id='+CustomerdirectId);
            }
            else
            {
                pageRefDetail = new PageReference('/apex/BSureC_ViewCustomerDetails?id='+CustomerId);
            }
            //pageRefDetail = new PageReference('/apex/BSureC_View ?id='+CustomerId);
            pageRefDetail.setRedirect(true); 
        }
        return pageRefDetail;
    }    
}