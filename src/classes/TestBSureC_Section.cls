/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_Section {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureC_Customer_Basic_Info__c objCustomer = new BSureC_Customer_Basic_Info__c(Customer_Name__c='KingFisher Customer');
        insert objCustomer;
       
        system.assertEquals('KingFisher Customer', objCustomer.Customer_Name__c);
        
        BSureC_Unearned_Cash_Discount_Section__c objUCDS = new BSureC_Unearned_Cash_Discount_Section__c();
        objUCDS.Customer_ID__c = objCustomer.Id;
        insert objUCDS;       
        system.assertEquals(objCustomer.Id, objUCDS.Customer_ID__c);
        
        BSureC_Unearned_Cash_Discount_Discussion__c objUCDSDis = new BSureC_Unearned_Cash_Discount_Discussion__c();
        objUCDSDis.BSureC_Unearned_Cash_Discount_Section__c=objUCDS.Id;
        objUCDSDis.Title__c='Dummy Title';
        insert objUCDSDis;
        system.assertEquals('Dummy Title', objUCDSDis.Title__c);
        
        BSureC_Invoice_Collection_Section__c objICS = new BSureC_Invoice_Collection_Section__c();
        objICS.Customer_ID__c = objCustomer.Id;
        insert objICS;       
        system.assertEquals(objCustomer.Id, objICS.Customer_ID__c);
        
        BSureC_Invoice_Collection_Discussion__c objICSDis = new BSureC_Invoice_Collection_Discussion__c();
        objICSDis.BSureC_Invoice_Collection_Section__c=objICS.Id;
        objICSDis.Title__c='Dummy Title';
        insert objICSDis;
        system.assertEquals('Dummy Title', objICSDis.Title__c);
                    
        //BSureC_On_Going_Credit_Section__c
        BSureC_On_Going_Credit_Section__c objOnG = new BSureC_On_Going_Credit_Section__c();
        objOnG.Customer_ID__c = objCustomer.Id;
        insert objOnG;
        system.assertEquals(objCustomer.Id, objOnG.Customer_ID__c);
        
        BSureC_On_Going_Credit_Discussion__c objOnGDis = new BSureC_On_Going_Credit_Discussion__c();
        objOnGDis.BSureC_On_Going_Credit_Section__c=objOnG.Id;
        objOnGDis.Title__c='Dummy Title';
        insert objOnGDis;
        system.assertEquals('Dummy Title', objOnGDis.Title__c);
        
        //BSureC_Unauthorized_Deduction_Section__c
        BSureC_Unauthorized_Deduction_Section__c objUNA = new BSureC_Unauthorized_Deduction_Section__c();
        objUNA.Customer_ID__c = objCustomer.Id;
        insert objUNA;
        system.assertEquals(objCustomer.Id, objUNA.Customer_ID__c);
        
        BSureC_Unauthorized_Deduction_Discussion__c objUNADis = new BSureC_Unauthorized_Deduction_Discussion__c();
        objUNADis.BSureC_Unauthorized_Deduction_Section__c=objUNA.Id;
        objUNADis.Title__c='Dummy Title';
        insert objUNADis;
        system.assertEquals('Dummy Title', objUNADis.Title__c);
        
        //BSureC_Confidential_Documents_Section__c
        BSureC_Confidential_Documents_Section__c objConf = new BSureC_Confidential_Documents_Section__c();
        objConf.Customer_ID__c = objCustomer.Id;        
        insert objConf;
        system.assertEquals(objCustomer.Id, objConf.Customer_ID__c);
        
        BSureC_Confidential_Document_Discussion__c objConfDis = new BSureC_Confidential_Document_Discussion__c();
        objConfDis.BSureC_Confidential_Documents_Section__c=objConf.Id;
        objConfDis.Title__c='Dummy Title';
        insert objConfDis;
        system.assertEquals('Dummy Title', objConfDis.Title__c);
        
        //BSureC_Credit_Increase_Section__c
            //BSureC_Credit_Increase__c
        BSureC_Credit_Increase__c objCPerf = new BSureC_Credit_Increase__c();
        objCPerf.Customer_Basic_Info_Id__c = objCustomer.Id;
        insert objCPerf;
        
        system.assertEquals(objCustomer.Id, objCPerf.Customer_Basic_Info_Id__c);        
            
        
        BSureC_Credit_Increase_Section__c objCPerfSec = new BSureC_Credit_Increase_Section__c();
        objCPerfSec.Customer_ID__c = objCustomer.Id;
        objCPerfSec.BSureC_Credit_Increase__c = objCPerf.Id;
        insert objCPerfSec;
        
        system.assertEquals(objCustomer.Id, objCPerfSec.Customer_ID__c);
        
        BSureC_Credit_Increase_Discussion__c objCPerfSecDis = new BSureC_Credit_Increase_Discussion__c();
        objCPerfSecDis.BSureC_Credit_Increase_Section__c=objCPerfSec.Id;
        objCPerfSecDis.Title__c='Dummy Title';
        insert objCPerfSecDis;
        system.assertEquals('Dummy Title', objCPerfSecDis.Title__c);    
        
            
    }
}