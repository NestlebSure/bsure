/************************************************************************************************       
Controller Name         : BSureS_Task_AutoUpdate       
Date                    : 06/24/2013        
Author                  : kishorekumar A       
Purpose                 : Scheduler       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          06/24/2013     Kishorekumar A           Initial Version
                          08/11/2013     Veereandranath J         Code Review
**************************************************************************************************/
global with sharing class BSureS_Task_AutoUpdate implements schedulable
{
	public list<Task> lstTask{get;set;}
	public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
	public list<BSureS_Basic_Info__c> lstSupp{get;set;}
	public list<string> lstString{get;set;}
	public list<string> lstStringCAIds{get;set;}
	public string strstatus{get;set;} // Added for Security Review
	public string strSupstatus{get;set;}
	
	
	global BSureS_Task_AutoUpdate()
	{
		
	}
	public void execute(schedulablecontext sc)
	{
		strSupstatus = 'Overdue';
		strstatus = 'Completed';
		lstString = new list<string>();
		lstStringCAIds = new list<string>();
		if(Schema.Sobjecttype.Task.isQueryable())
			lstTask = new list<Task>([select id,OwnerId,whatId from Task where Status !=:strstatus and OwnerId != null]);
		if(!lstTask.isEmpty())
		{
			for(Task tObj : lstTask)
			{
				if(tObj.whatId != null)
				{ 
					lstString.add(tObj.whatId);
				}	
			}
		}
		if(!lstString.isEmpty() && Schema.Sobjecttype.BSureS_Credit_Analysis__c.isQueryable())
		
		{ 
			lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>([select id from BSureS_Credit_Analysis__c
								where Id IN : lstString and Review_Status__c =:strstatus]);
								
			lstSupp = new list<BSureS_Basic_Info__c>([select id from BSureS_Basic_Info__c
								where Id IN : lstString and Review_Status__c !=: strSupstatus]);
								
		}
		if(!lstCreditAnalysis.isEmpty())
		{
			for(BSureS_Credit_Analysis__c caObj : lstCreditAnalysis)
			{
				lstStringCAIds.add(caObj.Id);
			}
		}
		if(!lstSupp.isEmpty())
		{
			for(BSureS_Basic_Info__c supObj : lstSupp)
			{
				lstStringCAIds.add(supObj.Id);
			}
		}
		if(!lstStringCAIds.isEmpty())
		{
			// Added for Security Reivew
			//if(Schema.sObjectType.Task.isDeletable())
			if(Task.sObjectType.getDescribe().isDeletable())
			{
				delete[select id,OwnerId,whatId from Task where whatId IN : lstStringCAIds];
			}
		}
	}
}