global with sharing class BSureC_MasstransferAnalystBatch implements Database.Batchable<SObject> {
    
    global String strQuery = null;
    global String strCustType = '';
    global string strParmUserid = '';
    global string str_AssignUser = '';
    global map<string,string> mapUserMngr = new map<string,string>();
    global map<string,string> mapUserName = new map<string,string>();
    global map<string,string> mapMngr =  new map<string,string>();
    global set<string> setCustomers = new set<string>();
    public list<BSureC_Customer_Basic_Info__c> LstUpdateCustomers = new list<BSureC_Customer_Basic_Info__c>();
    public string strException;
    
    public BSureC_MasstransferAnalystBatch(String strCustType, String strParmUserid, String str_AssignUser, map<string,string> mapUserMngr, set<string> setCustomers)
    { 
    
        
        this.strCustType = strCustType;
        this.strParmUserid = strParmUserid;
        this.str_AssignUser= str_AssignUser;
        this.mapUserMngr = mapUserMngr;
        this.setCustomers = setCustomers;
        String strUserID = '%'+strParmUserid+'%';
        
        system.debug('strCustType$$$$$$$'+strCustType);
        if(strCustType == 'Analyst'){
            strQuery = 'SELECT id,Customer_Name__c,Analyst__c,Manager__c,Backup_Analysts__c,ownerID from BSureC_Customer_Basic_Info__c where id in :setCustomers' ;
        }
        else if(strCustType == 'Collector'){
            strQuery = 'SELECT id,Customer_Name__c,Collector__c,Collector_Name__c,Collector_Manager__c,Collector_Manager_Name__c from BSureC_Customer_Basic_Info__c where id in :setCustomers' ;
        }
            
        system.debug('In constructor ' +strCustType+'strParmUserid@@@@'+strParmUserid+'setCustomers$$'+str_AssignUser+' '+setcustomers.size());
   
    }
    
    global Database.QueryLocator start (Database.BatchableContext ctx)
    { 
        system.debug('strQuery'+strQuery);        
        return Database.getQueryLocator(strQuery);
        
         
        
    }
    
    global void execute(Database.BatchableContext BC, list<BSureC_Customer_Basic_Info__c> scope)
    { 
          system.debug('scope====>'+scope.size());
             
        try{
        LstUpdateCustomers = new list<BSureC_Customer_Basic_Info__c>();
        
        if(strCustType == 'Analyst'){
            
            for(BSureC_Customer_Basic_Info__c  changebasicinfo : scope)
            {
                String strExtAnalyst = changebasicinfo.Analyst__c;
                changebasicinfo.Analyst__c = Id.valueOf(str_AssignUser) ;
                changebasicinfo.ownerID = Id.valueOf(str_AssignUser);
                if(mapUserMngr != null && mapUserMngr.size()>0 && mapUserMngr.get(str_AssignUser) != null)
                changebasicinfo.Manager__c = Id.valueOf(mapUserMngr.get(str_AssignUser));
                
                LstUpdateCustomers.add(changebasicinfo);
            }
        }else if(strCustType == 'Collector'){
            
            getUserName();
            getUserMngr();
            
            for(BSureC_Customer_Basic_Info__c changebasicinfo: scope)
            {
                String strMngrID;
                String strOldMngrID;
                String strCollectorIds='';
                String strCollectorNames='';
                String strCollectorMngrIDs='';
                String strCollectorMngrNames='';
                
                list<string> lstCollector = new list<string>();
                list<string> lstCollectorName = new list<string>();
                list<string> lstCollectorMngrIDs = new list<string>();
                list<string> lstCollectorMngrNames = new list<string>();
                
                //CollectorIDS
                if(changebasicinfo.Collector__c.contains(',')){
                    lstCollector = changebasicinfo.Collector__c.split(',');
                    for(string p:lstCollector){
                        system.debug(str_AssignUser+'p$$$$$$'+p+' '+strParmUserid +' '+str_AssignUser);
                        if(p == strParmUserid && p != str_AssignUser){
                            p=str_AssignUser;
                        }
                        strCollectorIds += p +',';
                    }
                    strCollectorIds =  strCollectorIds.left(strCollectorIds.length()-1);
                    //system.debug('strCollectorIds$$$$$$'+strCollectorIds);
                    if(strCollectorIds !=null && strCollectorIds !='')
                        changebasicinfo.Collector__c = strCollectorIds;
                    
                }else{
                    changebasicinfo.Collector__c = changebasicinfo.Collector__c.replace(strParmUserid,str_AssignUser);
                    System.Debug('in replace single collector vj'+ changebasicinfo.Collector__c+' '+changebasicinfo.Customer_Name__c);
                }
                 System.Debug('collector change vj'+changebasicinfo.Collector__c+' '+changebasicinfo.Customer_Name__c);
                //CollectorNames
                
                string colOldName = mapUserName.get(strParmUserid);
                System.Debug('mapUserNam e97=====> '+mapUserName);
                System.Debug('mapUserName.value()98=====> '+mapUserName.get(strParmUserid));
                System.Debug('strParmUserid 99=====> '+strParmUserid);
                string colName = mapUserName.get(str_AssignUser);
                
                System.Debug('cololdname '+colOldName +' '+colName);
                
                if(changebasicinfo.Collector_Name__c.contains(';')){
                    lstCollectorName = changebasicinfo.Collector_Name__c.split(';');
                    
                    for(string q:lstCollectorName){
                        //system.debug('q$$$$$$'+q);
                        if(q == colOldName && q != colName){
                            q=colName;
                        }
                        strCollectorNames += q +';';
                    }
                    strCollectorNames =  strCollectorNames.left(strCollectorNames.length()-1);
                    //system.debug('strCollectorIds$$$$$$'+strCollectorIds);
                    if(strCollectorNames !=null && strCollectorNames !='')
                        changebasicinfo.Collector_Name__c = strCollectorNames;
                        
                }else{
                    changebasicinfo.Collector_Name__c = changebasicinfo.Collector_Name__c.replace(colOldName,colName);
                    System.Debug('in replace single collector name vj '+ changebasicinfo.Collector__c+' '+changebasicinfo.Customer_Name__c);
                }
                
                System.Debug('collector name change vj'+changebasicinfo.Collector_Name__c+' '+changebasicinfo.Customer_Name__c);
                
                //ManagerIDs
                
                if(mapUserMngr != null && mapUserMngr.size()>0){
                    strMngrID = mapUserMngr.get(str_AssignUser);
                }
                if(mapMngr !=null && mapMngr.size()>0){
                    strOldMngrID = mapMngr.get(strParmUserid);
                }
                
                system.debug(strOldMngrID+'###MngrIDDD$$$'+strMngrID);
                if(changebasicinfo.Collector_Manager__c.contains(',')){
                    lstCollectorMngrIDs = changebasicinfo.Collector_Manager__c.split(',');
                    for(string r:lstCollectorMngrIDs){
                        //system.debug('r$$$$$$'+r);
                        if(r == strOldMngrID && r != strMngrID){
                            r=strMngrID;
                        }
                        strCollectorMngrIDs += r +',';
                    }
                    strCollectorMngrIDs =  strCollectorMngrIDs.left(strCollectorMngrIDs.length()-1);
                    //system.debug('strCollectorMngrIDs$$$$$$'+strCollectorMngrIDs);
                    if(strCollectorMngrIDs !=null && strCollectorMngrIDs !='')
                        changebasicinfo.Collector_Manager__c = strCollectorMngrIDs;
                    
                }else{
                    changebasicinfo.Collector_Manager__c = changebasicinfo.Collector_Manager__c.replace(strOldMngrID, strMngrID);
                }
                //ManagerNames
                string colOldMngrName = mapUserName.get(strOldMngrID);
                string colMngrName = mapUserName.get(strMngrID);
                system.debug(colOldMngrName+'###Mngr$$$'+colMngrName);
                if(changebasicinfo.Collector_Manager_Name__c.contains(';')){
                    lstCollectorMngrNames = changebasicinfo.Collector_Manager_Name__c.split(';');
                    for(string s:lstCollectorMngrNames){
                        //system.debug('s$$$$$$'+s);
                        if(s == colOldMngrName && s != colMngrName){
                            s=colMngrName;
                        }
                        strCollectorMngrNames += s +';';
                    }
                    strCollectorMngrNames =  strCollectorMngrNames.left(strCollectorMngrNames.length()-1);
                    //system.debug('strCollectorMngrNames$$$$$$'+strCollectorMngrNames);
                    if(strCollectorMngrNames !=null && strCollectorMngrNames !='')
                        changebasicinfo.Collector_Manager_Name__c = strCollectorMngrNames;
                    
                }else{
                    changebasicinfo.Collector_Manager_Name__c = changebasicinfo.Collector_Manager_Name__c.replace(colOldMngrName, colMngrName);
                }
                
                
                LstUpdateCustomers.add(changebasicinfo);
            }
            
        }
        system.debug('LstUpdateCustomers@@@@@'+LstUpdateCustomers);
        
        Database.SaveResult[] CreditCustomerUpdate = Database.Update(LstUpdateCustomers);
       
       for(Database.SaveResult sr : CreditCustomerUpdate)
        {
            if(!sr.isSuccess())
            {
                //SendJobStatus(null);
                System.Debug('error update record '+sr.getErrors());
            }
            
        }
        
        }
        catch(Exception ex){
            
            strException= string.valueOf(ex);
            System.Debug('Exception occured '+ex.getMessage()+' '+ex.getStackTraceString());
            //SendJobStatus(ex);
        }
        
    }
    
    public void SendJobStatus(){
        
        String sendMTOJobStatus = BSureC_CommonUtil.getConfigurationValues('BSureC_SendMassTransferJobMail').get(0);
        String MTOJobMailID = BSureC_CommonUtil.getConfigurationValues('BSureC_SendMassTransferJobMailID').get(0);
        system.debug(strException+'sendMTOJobStatus@@@@'+sendMTOJobStatus);
        
        if(sendMTOJobStatus.equalsIgnoreCase('TRUE')){
            
            List<String> lst_toaddress = new List<String>();
            lst_toaddress.add(MTOJobMailID);
            String str_subject = 'MassTransferOfOwnership job status';
            String str_Email_body = '';
            str_Email_body += '<table cellspacing="2" cellpadding="0" border="0" bordercolor="#D2EAF1" >';
            
            if(strException != null && strException !=''){
                str_Email_body += '<tr><td height="50px" width="80%" align="left">'+ system.label.BSureC_Masstransfer_Of_Ownership_Mail_Error +'</td></tr>';
                str_Email_body += '<br/><tr><td height="50px" width="80%" align="left">'+ strException + '</td></tr>';  
            }else{
            str_Email_body += '<tr><td height="50px" width="80%" align="left">'+ system.label.BSureC_Masstransfer_Of_Ownership_Mail_Content +'</td></tr>';
            }
            str_Email_body += '<br/><tr><td height="50px"/></tr><tr><td height="50px" width="80%" align="left">powered  by <b> bSure.</b> </td></tr>';
            str_Email_body += '</table>';                 
           
            if(lst_toaddress != null && lst_toaddress.size() > 0)
            {
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                //mail1.saveAsActivity = false; 
                mail1.setToAddresses(lst_toaddress); 
                mail1.setUseSignature(false);
                //mail1.setSaveAsActivity(false);
                mail1.setSubject(str_subject);
                mail1.setHtmlBody(str_Email_body); 
                //mail1.setSenderDisplayName(strForumName);
                List<Messaging.SendEmailResult> results  = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});
                
            }
        }
    }
    public void getUserName()
    {
        list<User> uname = [select id,name from User where id != null AND name != null];
        
        if (uname != null && uname.size()>0){
            for(User u:uname){
                mapUserName.put(u.id,u.name);
            }
        }       
    }
    public void getUserMngr(){
        
        list<BSureC_AdditionalAttributes__c> lstuserMngr = [SELECT id, User__c, Manager__c FROM BSureC_AdditionalAttributes__c WHERE Id != null AND User__r.IsActive = true];
        if(lstuserMngr!=null && lstuserMngr.size()>0){
            
            for(BSureC_AdditionalAttributes__c um:lstuserMngr){
                mapMngr.put(um.User__c,um.Manager__c);
            }
        }
        
    }
    global void finish(Database.BatchableContext BC)
    { 
         SendJobStatus();       
    }

}