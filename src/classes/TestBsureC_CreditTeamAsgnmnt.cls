/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true) 
private class TestBsureC_CreditTeamAsgnmnt {
 
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        UserRole uRole = [select Id,Name from UserRole where Name=:'Analyst' limit 1];
        
        profile p= [select id, name from Profile  limit 1];
                User umanager = new User(
                    alias = 'stUser', 
                    email='suresh.v@vertexcs.com',
                    emailencodingkey='UTF-8',
                    firstname='USER',
                    lastname='TEST',
                    languagelocalekey='en_US',
                    localesidkey='en_US', 
                    profileid = p.Id,
                    timezonesidkey='America/Los_Angeles',
                    username='site_test234@trmp.com'
                );
               insert umanager ;
               
               User UAnalyst = new User(
                    alias = 'stUser32', 
                    email='prathap.k@vertexcs.com',
                    emailencodingkey='UTF-8',
                    firstname='USER33',
                    lastname='TEST33',
                    languagelocalekey='en_US',
                    localesidkey='en_US', 
                    profileid = p.Id,
                    timezonesidkey='America/Los_Angeles',
                    username='site_test123@trmp.com',
                    UserRoleId = uRole.Id 
                );
               
              insert UAnalyst;          

            system.runas(UAnalyst){
            
                BSureC_Region__c RegRef=new BSureC_Region__c(name='Test Region');
                insert RegRef;
                
                BSureC_Category__c CatRef =new BSureC_Category__c(name='TestCategory1512',Category_Name__c='Testcat23');
                insert CatRef;
                
                
                BSureC_userCategoryMapping__c uCategory = new BSureC_userCategoryMapping__c(
                User__c = UAnalyst.Id,CategoryId__c = CatRef.Id);
                insert uCategory;
            
            //BSureC_Zone__c ZoneRefE=new BSureC_Zone__c(name='---Select---',IsActive__c=true);
            //insert ZoneRefE;
            
            
                BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='America1',IsActive__c=true);
                insert ZoneRef;
                BSureC_Zone__c ZRef=new BSureC_Zone__c(name='AOA1',IsActive__c=true);
                insert ZRef;
                BSureC_Sub_Zone__c SubZoneRef=new BSureC_Sub_Zone__c(name='North America1',ZoneID__c=ZoneRef.id,IsActive__c=true);
                insert SubZoneRef;
                BSureC_Sub_Zone__c SZoneRef=new BSureC_Sub_Zone__c(name='Latin America1',ZoneID__c=ZoneRef.id,IsActive__c=true);
                insert SZoneRef;
                BSureC_Country__c ConRef=new BSureC_Country__c(name='United States1',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
                insert ConRef;
                BSureC_Country__c CRef=new BSureC_Country__c(name='Cuba1',Sub_Zone_ID__c=SubZoneRef.id,IsActive__c=true);
                insert CRef;             
            
            //BSureC_State__c StateRef=new BSureC_State__c(name='Test state',Customer_Country__c=CRef.id,IsActive__c=true);
            //insert StateRef;            
                
                List<BSureC_AdditionalAttributes__c> lstAditionalAttrib=new List<BSureC_AdditionalAttributes__c>();
                   BSureC_AdditionalAttributes__c objAdditionalAttributes=new BSureC_AdditionalAttributes__c();
                   objAdditionalAttributes.BSureC_Zone__c=ZoneRef.id;
                   objAdditionalAttributes.BSureC_Sub_Zone__c=SubZoneRef.id;
                   objAdditionalAttributes.BSureC_Region__c=RegRef.id;
                   objAdditionalAttributes.BSureC_Country__c=ConRef.id;
                   objAdditionalAttributes.User__c = UAnalyst.Id;
                   insert objAdditionalAttributes;
                    lstAditionalAttrib.add(objAdditionalAttributes); 
             
             
             
                  BSureC_Customer_Basic_Info__c tstBSureCustomerBasicInfo=new BSureC_Customer_Basic_Info__c();
                     tstBSureCustomerBasicInfo.Customer_Name__c='Delhaize America Inc (Food Lion, Hannaford, J H Harvey)';
                     tstBSureCustomerBasicInfo.Customer_Category__c=CatRef.id;
                     tstBSureCustomerBasicInfo.Country__c=ConRef.id;//Country a06G000000KBcWi
                     string StrReg=RegRef.id;
                     tstBSureCustomerBasicInfo.Customer_Region__c=StrReg;//Region a0JG000000DI3tg
                     tstBSureCustomerBasicInfo.Zone__c=ZoneRef.id;//Zone  a0PG0000005GGVl
                     tstBSureCustomerBasicInfo.Sub_Zone__c=SubZoneRef.id;//Sub Zone  a0KG000000FIhnI             
                     tstBSureCustomerBasicInfo.IsActive__c=true;
                  insert tstBSureCustomerBasicInfo;
                  
                  
                  
                  system.assertEquals('Delhaize America Inc (Food Lion, Hannaford, J H Harvey)', tstBSureCustomerBasicInfo.Customer_Name__c);
                 
                  
                     string custid=ApexPages.CurrentPage().getParameters().put('id',tstBSureCustomerBasicInfo.id); 
                     ApexPages.StandardController sc = new ApexPages.standardController(tstBSureCustomerBasicInfo);  
                 
                 BsureC_CreditTeamAsgnmnt objCreditTeamAsgnmntCls=new BsureC_CreditTeamAsgnmnt();
                 
                 objCreditTeamAsgnmntCls.strCustomerId=custid;
                 //objCreditTeamAsgnmntCls.lstAnalysts=[SELECT Id, Name,Profile.Id,Profile.Name FROM User Where Profile.Name='BSureC_Analyst'];
                  BSureC_Customer_Basic_Info__c tstBSureCustomerBasicInfo1=new BSureC_Customer_Basic_Info__c();
                     tstBSureCustomerBasicInfo1.Customer_Name__c='Delhaize America Inc (Food Lion, Hannaford, J H Harvey)';
                     tstBSureCustomerBasicInfo1.Customer_Category__c=CatRef.id;
                     tstBSureCustomerBasicInfo1.Country__c=ConRef.id;//Country a06G000000KBcWi
                     string StrReg1=RegRef.id;
                     tstBSureCustomerBasicInfo1.Customer_Region__c=StrReg1;//Region a0JG000000DI3tg
                     tstBSureCustomerBasicInfo1.Zone__c=ZoneRef.id;//Zone  a0PG0000005GGVl
                     tstBSureCustomerBasicInfo1.Sub_Zone__c=SubZoneRef.id;//Sub Zone  a0KG000000FIhnI 
                     tstBSureCustomerBasicInfo1.Analyst__c=UAnalyst.Id;
                     tstBSureCustomerBasicInfo1.Manager__c=umanager.Id;            
                     tstBSureCustomerBasicInfo1.IsActive__c=true;
                  insert tstBSureCustomerBasicInfo1;   
                  system.assertEquals('Delhaize America Inc (Food Lion, Hannaford, J H Harvey)', tstBSureCustomerBasicInfo1.Customer_Name__c);
             
            
                 objCreditTeamAsgnmntCls.IsTest=true;
                 //objCreditTeamAsgnmntCls.paramMap.put('Id',tstBSureCustomerBasicInfo.Id);
                 objCreditTeamAsgnmntCls.Init();
                 
                 List<User> lstUser = New List<User>();
                 User UAnalyst1 = new User(
                        alias = 'yiy343', 
                        email='prathap.k@vertexcs.com',
                        emailencodingkey='UTF-8',
                        firstname='USER3343',
                        lastname='TEST3343',
                        languagelocalekey='en_US',
                        localesidkey='en_US', 
                        profileid = p.Id,
                        timezonesidkey='America/Los_Angeles',
                        username='site_test12345@trmp.com'
                    );
                   
                  insert UAnalyst1;  
                 lstUser.add(UAnalyst1);
                 
                  List<BSureC_AdditionalAttributes__c> lstAditionalAttrib1=new List<BSureC_AdditionalAttributes__c>();
                   BSureC_AdditionalAttributes__c objAdditionalAttributes1=new BSureC_AdditionalAttributes__c();
                   objAdditionalAttributes1.BSureC_Zone__c=ZoneRef.id;
                   objAdditionalAttributes1.BSureC_Sub_Zone__c=SubZoneRef.id;
                   objAdditionalAttributes1.BSureC_Region__c=RegRef.id;
                   objAdditionalAttributes1.BSureC_Country__c=ConRef.id;
                   objAdditionalAttributes1.User__c=umanager.Id;
                   insert objAdditionalAttributes1;
                lstAditionalAttrib1.add(objAdditionalAttributes1); 
                 
                 objCreditTeamAsgnmntCls.getAnalysts();
                 //objCreditTeamAsgnmntCls.selectedAnalystId=lstuser[0].id;
                 objCreditTeamAsgnmntCls.selectedAnalystId=UAnalyst.Id; 
                 objCreditTeamAsgnmntCls.getManagers();
                 //objCreditTeamAsgnmntCls.selectedManagerId=umanager.Id;
                 BSureC_Customer_Basic_Info__c tstBSureCustomerBasicInfo2=new BSureC_Customer_Basic_Info__c();
                 tstBSureCustomerBasicInfo2.Customer_Name__c='Delhaize America Inc (Food Lion, Hannaford, J H Harvey)';
                 tstBSureCustomerBasicInfo2.Customer_Category__c=CatRef.id;
                 tstBSureCustomerBasicInfo2.Country__c=ConRef.id;//Country a06G000000KBcWi
                 string StrReg2=RegRef.id;
                 tstBSureCustomerBasicInfo2.Customer_Region__c=StrReg1;//Region a0JG000000DI3tg
                 tstBSureCustomerBasicInfo2.Zone__c=ZoneRef.id;//Zone  a0PG0000005GGVl
                 tstBSureCustomerBasicInfo2.Sub_Zone__c=SubZoneRef.id;//Sub Zone  a0KG000000FIhnI 
                 tstBSureCustomerBasicInfo2.Analyst__c=UAnalyst.Id;
                 tstBSureCustomerBasicInfo2.Manager__c=umanager.Id;            
                 tstBSureCustomerBasicInfo2.IsActive__c=true;
                  insert tstBSureCustomerBasicInfo2;   
                  system.assertEquals('Delhaize America Inc (Food Lion, Hannaford, J H Harvey)', tstBSureCustomerBasicInfo2.Customer_Name__c);
                  objCreditTeamAsgnmntCls.selectedManagerId = umanager.Id;
                 objCreditTeamAsgnmntCls.getBackupAnalysts();
                 objCreditTeamAsgnmntCls.selectclick();
                 objCreditTeamAsgnmntCls.unselectclick();
                 objCreditTeamAsgnmntCls.getunSelectedValues();
                 objCreditTeamAsgnmntCls.getSelectedValues();             
                 //system.debug('selectedAnalystId***');
                 //objCreditTeamAsgnmntCls.selectedAnalystId=UAnalyst.Id;  
                 //objCreditTeamAsgnmntCls.selectedManagerId=umanager.Id;
                 
                 objCreditTeamAsgnmntCls.strSelectedBackUpAnalystNames= UAnalyst.Name;
                 
                  //system.assertEquals(null, tstBSureCustomerBasicInfo1.id);                                               
                 objCreditTeamAsgnmntCls.AssignCreditTeam();
                 objCreditTeamAsgnmntCls.SendNotification();
                 objCreditTeamAsgnmntCls.CancleAssignCreditTeam();
                
                 //objCreditTeamAsgnmntCls.getBackupAnalysts();
                 
                 
                 
            }
            //system.ar
    }
}