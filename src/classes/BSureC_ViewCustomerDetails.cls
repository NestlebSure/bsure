/***********************************************************************************************
* Controller Name   : BSureC_ViewCustomerDetails
* Date              : 10/12/2012
* Author            : Manjula.a(manjula.a@vertexcs.com)  
* Purpose           : Customer View page creation
* Change History    : 
* Date           Programmer         Reason
* -------------------- ------------------- -------------------------
* 10/12/2012          Manjula.a      Initial Version
**************************************************************************************************/
//public class BSureC_ViewCustomerDetails {
global with sharing class BSureC_ViewCustomerDetails {
	public string strCustomerId{get;set;} //Variable for get customer ID
  	public string strAnalystName{get;set;} //Variable for get customer Alalyst ID
  	public string strManagerName{get;set;} //Variable for get customer Manager ID
  	public string strBackUpAnalystsNames{get;set;} //Variable for get customer Backup Alalysts ID
  	public boolean visibilityEdit{get;set;}
  	public boolean visibilityCredit{get;set;}
  	public Id currentUserId{get;set;}
  
    public boolean visibilityNewCreditAnalysisBtn{get;set;}
    public boolean visibilityNonCreditBtn{get;set;}//New Buyer button Visibility
    public list<BSureC_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureC_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureC_Country_Manager__c> lstCountryManager{get;set;}
    public set<id> setuserIds= new set<id>();
    public string strCEO{get;set;}
    public string strZoneManager{get;set;}
    public string strSubZoneManager{get;set;}
    public string strCountryManager{get;set;}
    public string strAnalystManager{get;set;}
    public list<string> lstMultipleManagerRoles{get;set;}
    public List<UserRole> lstManagerRoles{get;set;}// Profile object
    public string strCollectorRole{get;set;}
    
    public BSureC_ViewCustomerDetails(ApexPages.StandardController controller) 
    {
        lstMultipleManagerRoles=new list<string>();
        lstManagerRoles=new List<UserRole>();
      	strCustomerId=Apexpages.currentPage().getParameters().get('id'); //get the  Customer id
        visibilityEdit = false;
        visibilityCredit = false; 
        visibilityNonCreditBtn = false;
        visibilityNewCreditAnalysisBtn = false;
            
        /*   BSureC_Customer_Basic_Info__c CustDetailsView;
        if(strCustomerId != null)
        {
       		CustDetailsView = [select id,Analyst__c,Analyst_Name__c,Manager__c,Backup_Analysts__c,Customer_ID__c 
                          from BSureC_Customer_Basic_Info__c where id =: strCustomerId];
        }
        if(CustDetailsView != null)
        {
	        if(CustDetailsView.Analyst__c != null ) 
	        {
	          strAnalystName = CustDetailsView.Analyst__c;
	        }
	        if(CustDetailsView.Manager__c!= null )
	        {
	          strManagerName = CustDetailsView.Manager__c;
	        }
	        if(CustDetailsView.Backup_Analysts__c != null)
	        {
	          strBackUpAnalystsNames = CustDetailsView.Backup_Analysts__c;
	        }
      	}  
         currentUserId = UserInfo.getUserId();
         Id systemAdminProfile = UserInfo.getProfileId();
         // Id AdminProfile = [select id from Profile where Name='System Administrator'].get(0).Id;
         Profile AdminProfile = [select id,Name FROM Profile where Id=:systemAdminProfile];
         // if(systemAdminProfile == AdminProfile)
         if(AdminProfile.Name == 'System Administrator' || AdminProfile.Name == 'BSureC_Sub Zone Manager' || 
           AdminProfile.Name == 'BSureC_Sub Zone Manager' || AdminProfile.Name == 'BSureS_Country Manager' )
          {
            visibilityEdit = true;
            visibilityCredit=true;
          }
          if(strBackUpAnalystsNames != null && strBackUpAnalystsNames.contains(','))
          {
            for(String backana:strBackUpAnalystsNames.split(','))
            {
              if(currentUserId == backana)
              {
                visibilityEdit = true;
                visibilityCredit=true;
                break;
              }
            }
          }
          if(currentUserId == strAnalystName  || currentUserId == strManagerName)
          {
            visibilityEdit = true;
            visibilityCredit=true;        
          }*/
        
        //by satish on april 23rd 2014
         strCollectorRole = BSureC_CommonUtil.getConfigurationValues('BsureC_Collector').get(0);
         List<Profile> lst_profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
         List<UserRole> lst_Role = [SELECT Name FROM UserRole WHERE Id =: Userinfo.getUserRoleId()];
         system.debug(lst_Role[0].Name+'lst_Role[0].Name@@@'+lst_profile[0].Name);
         if(lst_profile[0].Name == 'BSureC_Collector' && lst_Role[0].Name == strCollectorRole){
         	system.debug('Collector@@');
         	visibilityNewCreditAnalysisBtn = true;
         }
           
        lstZoneManager = new list<BSureC_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureC_Sub_Zone_Manager__c>();
        lstCountryManager = new list<BSureC_Country_Manager__c>();
        
        list<BSureC_Customer_Basic_Info__c> sDetailsView;
        if(strCustomerId != null)
        {
             sDetailsView = [SELECT id,Analyst__c,Analyst_Name__c,Manager__c,Backup_Analysts__c,Customer_ID__c,
                                                Zone__c,Sub_Zone__c,Country__c
                                                FROM BSureC_Customer_Basic_Info__c WHERE id =: strCustomerId];
        }
        if(sDetailsView != null && sDetailsView.size() >0 )
        {
            if(sDetailsView.get(0).Analyst__c != null )
            {
                strAnalystName = sDetailsView.get(0).Analyst__c;
            }
            if(sDetailsView.get(0).Manager__c!= null )
            {
                strManagerName = sDetailsView.get(0).Manager__c;
            }
            if(sDetailsView.get(0).Backup_Analysts__c != null)
            {
                strBackUpAnalystsNames = sDetailsView.get(0).Backup_Analysts__c;
            }
            
            if(sDetailsView.get(0).Zone__c != null)
            {
                lstZoneManager = [select Zone__c,Zone_Manager__c from BSureC_Zone_Manager__c where Zone__c =: sDetailsView.get(0).Zone__c];
            }
            if(sDetailsView.get(0).Sub_Zone__c != null)
            {
                lstSubZoneManager = [select Sub_Zone__c,Sub_Zone_Manager__c  from BSureC_Sub_Zone_Manager__c where Sub_Zone__c =: sDetailsView.get(0).Sub_Zone__c];
            }
            if(sDetailsView.get(0).Country__c != null)
            {
                lstCountryManager = [select Country__c,Country_Manager__c from BSureC_Country_Manager__c where Country__c =: sDetailsView.get(0).Country__c];
            }
        }   
        
        currentUserId = UserInfo.getUserId();
        
        strCEO = BSureC_CommonUtil.getConfigurationValues('BSureC_CEORole').get(0);
        //system.debug('AdminRole........'+strCEO);
        list<User> AdminRole = [select id from User where Userrole.Name =:strCEO and Id =:currentUserId];
        if(AdminRole != null && AdminRole.size() > 0)
        {
        	system.debug('AdminRole@@');
            setuserIds.add(AdminRole.get(0).id);
            visibilityEdit = true;
            visibilityCredit = true; 
            visibilityNonCreditBtn = true;
            visibilityNewCreditAnalysisBtn = true;
        }
       
       strZoneManager=BSureC_CommonUtil.getConfigurationValues('BSureC_ZoneManager').get(0);
       strSubZoneManager=BSureC_CommonUtil.getConfigurationValues('BSureC_SubZoneManager').get(0);
       strCountryManager=BSureC_CommonUtil.getConfigurationValues('BSureC_CountryManager').get(0);
       strAnalystManager=BSureC_CommonUtil.getConfigurationValues('BSureC_AnalystRole').get(0);
       lstMultipleManagerRoles.add(strZoneManager);
       lstMultipleManagerRoles.add(strSubZoneManager);
       lstMultipleManagerRoles.add(strCountryManager);
       lstMultipleManagerRoles.add(strAnalystManager);
       
        //system.debug('----strZoneManager---'+strZoneManager);
       //system.debug('----strSubZoneManager---'+strSubZoneManager);
       //system.debug('----strCountryManager---'+strCountryManager);
       //system.debug('----strAnalystManager---'+strAnalystManager);
       
        if(lstMultipleManagerRoles!=null && lstMultipleManagerRoles.size()>0)
        {
            lstManagerRoles= [select id from UserRole where userrole.name IN :lstMultipleManagerRoles];
        }
       //&& currentUserId == lstManagerRoles.get(0).id  List<user> ManagerProfile = [select id from user where Profile.Name='BSureC_Manager' and id =:currentUserId];
     /*   if(lstManagerRoles!=null && lstManagerRoles.size() > 0 )
        {
            visibilityEdit = true;
            visibilityCredit = true; 
            visibilityNonCreditBtn = true;
            visibilityNewCreditAnalysisBtn = true;
        }
        else if(lstManagerRoles==null && lstManagerRoles.size() == 0)
        {
            visibilityEdit = false;
            visibilityCredit = false; 
            visibilityNonCreditBtn = false;
            visibilityNewCreditAnalysisBtn = false;
        }
        */
        
        //List<Profile> lst_profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
        
        if(lst_profile[0].Name == 'BSureC_Analyst'){
        	system.debug('BSureC_Analyst@@');
        	visibilityEdit = true;
            visibilityNewCreditAnalysisBtn = true;
            visibilityNonCreditBtn = false;
            visibilityCredit = false;
        }
        
        /*if(strBackUpAnalystsNames != null )
        {
            if(strBackUpAnalystsNames.contains(','))
            {
                for(String ba:strBackUpAnalystsNames.split(','))
                {
                    if(currentUserId == ba)
                    {
                        visibilityEdit = true;
                        visibilityNewCreditAnalysisBtn = true;
                      //  visibilityNonCreditBtn = true;
                      //  visibilityCredit = true;
                        break;
                    }
                }
            }
            else
            {
                if(currentUserId == strBackUpAnalystsNames)
                {
                    visibilityEdit = true;
                    visibilityNewCreditAnalysisBtn = true;
                   // visibilityNonCreditBtn = true;
                  //  visibilityCredit = true;
                }
            }   
        }*///Commented by satish on 21st may 2014
        if(currentUserId == strAnalystName )
        {
            visibilityEdit = true;
            //visibilityNewCreditAnalysisBtn = true;
            //visibilityNonCreditBtn = false;
            //visibilityCredit = false;
        } 
        
        if(strManagerName != null && currentUserId == strManagerName)
        {
        	system.debug('Manager@@');
            visibilityEdit = true;
            visibilityCredit = true;
            visibilityNonCreditBtn = true;
            visibilityNewCreditAnalysisBtn = true;
        }
        
        //system.debug('----visibilityCredit-111--'+visibilityCredit);
        //system.debug('----visibilityNewCreditAnalysisBtn---'+visibilityNewCreditAnalysisBtn);
        //system.debug('----visibilityEdit---'+visibilityEdit);
        //system.debug('----visibilityNonCreditBtn---'+visibilityNonCreditBtn);
        
        //system.debug('----sDetailsView---'+sDetailsView);
        //system.debug('----currentUserId---'+currentUserId);
        //system.debug('----lstZoneManager---'+lstZoneManager);
        //system.debug('----lstSubZoneManager---'+lstSubZoneManager);
        //system.debug('----lstCountryManager---'+lstCountryManager);
        
        if(lstZoneManager != null && lstZoneManager.size() > 0)
        {
            for(BSureC_Zone_Manager__c ObjZM:lstZoneManager)
            {
                if(ObjZM.Zone_Manager__c == currentUserId)
                {
                	system.debug('lstZoneManager@@');
                    visibilityCredit = true; 
                    visibilityNonCreditBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                }
            }
        }
        if(lstSubZoneManager != null && lstSubZoneManager.size() >0)
        {
            for(BSureC_Sub_Zone_Manager__c ObjSZM : lstSubZoneManager)
            {
                if(ObjSZM.Sub_Zone_Manager__c == currentUserId)
                {
                	system.debug('SubZoneManager@@');
                    visibilityCredit = true; 
                    visibilityNonCreditBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                }
            }
        }
        if(lstCountryManager != null && lstCountryManager.size() >0)
        {
            for(BSureC_Country_Manager__c ObjCM : lstCountryManager)
            {
                if(ObjCM.Country_Manager__c == currentUserId)
                {
                	system.debug('countryManager@@');
                    visibilityCredit = true; 
                    visibilityNonCreditBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                }
            }
        }
       //system.debug('----visibilityCredit-222--'+visibilityCredit);
        //system.debug('----visibilityNewCreditAnalysisBtn---'+visibilityNewCreditAnalysisBtn);
        //system.debug('----visibilityEdit---'+visibilityEdit);
        //system.debug('----visibilityNonCreditBtn---'+visibilityNonCreditBtn);
    }    
    /// <summary>
  /// Method to view the customer Details
  /// </summary>
    public BSureC_Customer_Basic_Info__c getCustomerDetails() {
      BSureC_Customer_Basic_Info__c CustDetails;
      if(strCustomerId != null)
      {
        CustDetails = [select id,Customer_ID__c from BSureC_Customer_Basic_Info__c where id =: strCustomerId limit 1];  
      }
        return CustDetails;
    }
}