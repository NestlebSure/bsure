/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_UserActivityDashboard {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        //system.assertEquals('TESTAOA', szone.Name);
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true;
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        
        
        BSureS_Country__c sCountry = new BSureS_Country__c();
        sCountry.IsActive__c = true;
        sCountry.Name = 'test united States';
        sCountry.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountry; 
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8', 
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
        BSureS_User_Additional_Attributes__c objUseradd = new BSureS_User_Additional_Attributes__c();
        objUseradd.User__c = standard.id;
        objUseradd.BSureS_Zone__c = szone.id;
        objUseradd.BSureS_Sub_Zone__c = sSubZone.id;
        objUseradd.BSureS_Country__c = sCountry.id;
        insert objUseradd;
        list<BSureS_User_Additional_Attributes__c> lstAdd = new list<BSureS_User_Additional_Attributes__c>();
        lstAdd.add(objUseradd);
        
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
   		insert objSupplier;
   		
   		system.assertEquals('KingFisher',objSupplier.Contact_name__c);
   		
   		list<BSureS_Confidential_Info_Section__c> lstC = new list<BSureS_Confidential_Info_Section__c>();
   		list<BSureS_Credit_Review_Section__c> lstCR = new list<BSureS_Credit_Review_Section__c>();
   		for(integer i=0;i<=3;i++){
   			BSureS_Confidential_Info_Section__c  cInfo = new BSureS_Confidential_Info_Section__c();
	   		cInfo.DiscussionType__c = 'Topic';
	   		cInfo.Supplier_ID__c = objSupplier.Id;
	   		lstC.add(cInfo);
	   		BSureS_Credit_Review_Section__c Cr = new BSureS_Credit_Review_Section__c();
	   		Cr.DiscussionType__c = 'Topic';
	   		Cr.Supplier_ID__c = objSupplier.Id;
	   		lstCR.add(CR);
   		}
   		insert lstc;
   		insert lstCR;
   		
   		system.assertEquals(4,lstc.size());
   		system.assertEquals(4,lstCR.size());
   		
   		system.Test.startTest();
   		BSureS_UserActivityDashboard UAD = new BSureS_UserActivityDashboard();
   		//UAD.strUserId = standard.Id;
   		UAD.strZoneId = szone.Id;
   		UAD.strSubZoneId = sSubZone.Id;
   		UAD.strCountryId  = sCountry.Id;
   		BSureS_UserActivityDashboard.UserCommentCountWrapper cwrp1= new BSureS_UserActivityDashboard.UserCommentCountWrapper();
   		BSureS_UserActivityDashboard.UserDocumentCountWrapper cwrp2= new BSureS_UserActivityDashboard.UserDocumentCountWrapper();
   		BSureS_UserActivityDashboard.userLHWrapper cwrp3 = new  BSureS_UserActivityDashboard.userLHWrapper();
        UAD.SetLocation(); 
        system.Test.stopTest();
         
            		
    }
}