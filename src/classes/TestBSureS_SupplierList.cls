/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_SupplierList {

    static testMethod void myUnitTest() {

        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_ZoneManagerrole';
        objCSettings1.Parameter_Key__c = 'BSureS_ZoneManagerrole';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        system.assertEquals('Zone Manager', objCSettings1.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureS_SubZoneManagerRole';
        objCSettings2.Parameter_Key__c = 'BSureS_SubZoneManagerRole';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'BSureS_countrymanagerrole';
        objCSettings3.Parameter_Key__c = 'BSureS_countrymanagerrole';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'BSureS_regionmanagerrole';
        objCSettings4.Parameter_Key__c = 'BSureS_regionmanagerrole';
        objCSettings4.Parameter_Value__c = 'Region Manager';
        insert objCSettings4;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'BSureS_analystrole';
        objCSettings5.Parameter_Key__c = 'BSureS_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'BSureS_buyerrole';
        objCSettings6.Parameter_Key__c = 'BSureS_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureS_ShowSuppliers';
        objCSettings7.Parameter_Key__c = 'ShowAllSuppliersForManagers';
        objCSettings7.Parameter_Value__c = 'TRUE';
        insert objCSettings7;
        
        BSureS_Basic_Info__c obj = new BSureS_Basic_Info__c(Supplier_Name__c='Test Search');
        insert obj;
        BSureS_SupplierList clsSupp = new BSureS_SupplierList();
        clsSupp.strSearchSupp = obj.Supplier_Name__c;
        clsSupp.getSearchSupplier();
        clsSupp.closePopup();
        clsSupp.strSearchSupp = 'Test Supplier';
        clsSupp.getSearchSupplier();
        clsSupp.closePopup();
        clsSupp.strSearchSupp = '';
        clsSupp.getSearchSupplier();
        clsSupp.closePopup();
        
        system.Test.startTest();
        BSureS_SupplierList objSupplList = new BSureS_SupplierList();
        objSupplList.strSupplierName = 'Test Supplier';
        objSupplList.strRating = 'abc,def,';
        objSupplList.strReviewStatus = 'pqr,xyz,';
        objSupplList.strDelSupplID = 'xxxxxxxxxxx'; 
        objSupplList.strAdvSupplierName = 'Test Adv Supplier';
        objSupplList.strSupplierCatg = 'kkkkkkkkk';
        objSupplList.strSupplierZone = 'zzzzzzzzzzzzz';
        objSupplList.strSupplierSubZone = 'sssssssssssss';
        objSupplList.strSupplierCountry = 'cccccccccccc';
        objSupplList.strSupplierState = 'sssssssssss';
        objSupplList.dSpendFrom = '345,678';
        objSupplList.dSpendTo = '223,456';
        objSupplList.strNextRevFromDate = system.today().format();
        objSupplList.strNextRevToDate = system.today().format();
        objSupplList.strLastRevFromDate = system.today().format();
        objSupplList.strLastRevToDate = system.today().format();
        
        objSupplList.CreateSupplier();
        objSupplList.SortData();
        objSupplList.strAnalystChoice = 'mysupplier';
        objSupplList.getCustomSupplierList();
        objSupplList.getSearchResults();
        objSupplList.getAdvSearchResult();
        objSupplList.getSZones();
        objSupplList.getCountriesList();
        objSupplList.getSubZoneList();
        objSupplList.getStateList();
        objSupplList.DeleteSupplierInfo();
        objSupplList.getSortDirection();
        objSupplList.setSortDirection('ASC');
        objSupplList.getPreviousButtonEnabled();
        objSupplList.getNextButtonDisabled();
        objSupplList.getTotalPageNumber();
        string strTestAdv = objSupplList.AdvSearchQueryString();
        //string strTest = objSupplList.SearchQueryString();
        system.Test.stopTest();
    }
    static testMethod void myAnalystTest()
    {
        
        
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureS_Analyst'];
        
        User objUser = new User(Alias = 'bsurean', Email='suppl_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_SupplUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='suppl_analyst@bsurenestle.com');
                    
        
        system.runAs(objUser)
        {
            BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
            objCSettings7.Name = 'BSureS_ShowSuppliers';
            objCSettings7.Parameter_Key__c = 'ShowAllSuppliersForManagers';
            objCSettings7.Parameter_Value__c = 'TRUE';
            insert objCSettings7;
            
            system.assertEquals('ShowAllSuppliersForManagers', objCSettings7.Parameter_Key__c);
            
            system.Test.startTest();
            BSureS_SupplierList objSupplList = new BSureS_SupplierList();
            objSupplList.strAnalystChoice = 'mysupplier';            
            objSupplList.SetQueryParams();
            objSupplList.getCustomSupplierList();
            objSupplList.getSearchResults();
            objSupplList.DeleteSupplierInfo();
            objSupplList.SortData();
            
            system.Test.stopTest();
        }
    }
}