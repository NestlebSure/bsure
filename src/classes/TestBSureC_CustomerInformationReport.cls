/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomerInformationReport {

    static testMethod void myUnitTest() 
    {
        BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        BSzone= [select id,Name from BSureC_Sub_Zone__c where id=:BSzone.Id];
    
	    BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
	    insert BsCountry;
	    system.assertequals('Test Supplier Country',BsCountry.Name);
	    BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
	    
	    list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_Report_Page_Size_Test';
        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size_Test';
        cSettings.Parameter_Value__c = '10';
        lstSettings.add(cSettings);
        insert lstSettings;
	    
	    BSureC_AdditionalAttributes__c b=new BSureC_AdditionalAttributes__c();
	    b.BSureC_Zone__c = Bzone.id;
	    b.BSureC_Sub_Zone__c = BSzone.id;
	    b.BSureC_Country__c = BsCountry.id;
	    b.User__c=UserInfo.getUserId();
	    insert b;
    	
    	BSureC_Customer_Basic_Info__c Cust = new BSureC_Customer_Basic_Info__c();
    	Cust.Customer_Name__c = 'Test Cust';
    	Cust.Collector_Name__c='Test Collector';
    	Cust.Zone__c = Bzone.Id;
    	Cust.Sub_Zone__c = BSzone.Id;
    	Cust.Country__c = BsCountry.Id;
    	insert Cust;
	    //ApexPages.currentPage().getParameters().put('id',b.id); 
	    Apexpages.currentPage().getParameters().put('custId',b.id);
	    //ApexPages.StandardController controller = new ApexPages.StandardController(b); 
	    BSureC_CustomerInformationReport sviewObj= new BSureC_CustomerInformationReport(); 
	    sviewObj.getlstCollectorNames();  
	    sviewObj.strCollectorNames = 'Test Collector';
	    sviewObj.strCountryId = BsCountry.Id;
	    sviewObj.strSubzoneId = BSzone.Id;
	    sviewObj.strZoneId = Bzone.Id;
	    sviewObj.getSortDirection();
	    sviewObj.setSortDirection('Asc');
	    sviewObj.SortData();
	    sviewObj.getZOnesOptions();
	    sviewObj.getSubzonesoptions();
	    sviewObj.getCountriesOptions();
	    sviewObj.blnExport=true;
	    sviewObj.pagenation();
	     sviewObj.getCustomer();
	    sviewObj.ExportCSV();
	    sviewObj.fetchDetails();
	    sviewObj.fetchDetailsforExport();
	    //sviewObj.getCustomers(); 
	    sviewObj.getTotalPageNumber();
		sviewObj.getPageNumber();
		sviewObj.getPageSize();
		sviewObj.getNextButtonDisabled();
		sviewObj.getPreviousButtonEnabled();
		sviewObj.LastbtnClick();
		sviewObj.nextBtnClick();
		sviewObj.previousBtnClick();
		sviewObj.FirstbtnClick();
		sviewObj.cancel();
		BSureC_CustomerInformationReport.CommonUserclass commonClass=new BSureC_CustomerInformationReport.CommonUserclass();
		//sviewObj.getCustomer();
       
    }
}