/************************************************************************************************       
Controller Name         : BSureC_PaymentHistoryApprove       
Date                    : 12/11/2012        
Author                  : Santhosh Palla       
Purpose                 : To Approve the New Credit Limit of the Customer
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          12/11/2012      Santhosh.Palla            Initial Version
**************************************************************************************************/
global with sharing class BSureC_PaymentHistoryApprove 
{
    //Lists and Maps
    public List<BSureC_Credit_Increase__c> lst_credit_increase_details{get;set;}
    public BSureC_Credit_Increase__c rec_credit_increase_info{get;set;}
    private final BSureC_Credit_Increase__c rec_customer_basic_info_std;//to read the Customer Related values from Standard Controller
    public Map<Id,BSureC_Credit_Increase__c> map_credit_increase{get;set;}
    public UserRole rec_profile_info{get;set;}
    public BSureC_Customer_Basic_Info__c rec_cust_info{get;set;}
    
    //String Variables
    public String str_approval_comments{get;set;}
    public String str_credit_increase_Id{get;set;}
    public String str_credit_increase_querystring_id{get;set;}
    public String str_customer_info_id{get;set;}
    public String str_flag{get;set;}
    public String login_user_id{get;set;}
    public String login_user_role{get;set;}
    public String strAnalyst{get;set;}
    public String strManager{get;set;}
    public Id profile_id{get;set;}
    
    //Boolean Variables
    public Boolean bl_credit_limit_output_flag{get;set;}
    public Boolean bl_credit_limit_input_flag{get;set;}
    public Boolean bl_view_flag{get;set;}
    
    
    public BSureC_PaymentHistoryApprove(Apexpages.Standardcontroller controller)
    {
       
        profile_id = Userinfo.getUserRoleId();//getProfileId();
        login_user_id = Userinfo.getUserId();
        // login_user_role = 
        rec_profile_info = [SELECT Name FROM UserRole where Id =: profile_id];
        //rec_profile_info = [SELECT Profile.Name,UserRole.Name FROM User where Id =: Userinfo.getUserId()];
        
        bl_credit_limit_output_flag = false;
        bl_credit_limit_input_flag = false;
        bl_view_flag = false;
        
        map_credit_increase = new Map<Id,BSureC_Credit_Increase__c>();
        
        str_credit_increase_querystring_id = ApexPages.currentPage().getParameters().get('crId');
        
        if(str_credit_increase_querystring_id == null || str_credit_increase_querystring_id == '')
        {
             rec_customer_basic_info_std = (BSureC_Credit_Increase__c)controller.getRecord();
             //bl_credit_limit_output_flag = false;
            // bl_credit_limit_input_flag = true;
             str_credit_increase_querystring_id = String.valueOf(rec_customer_basic_info_std.Id);
        }
        
        lst_credit_increase_details = new List<BSureC_Credit_Increase__c>();
        
        lst_credit_increase_details = [SELECT Id,New_Credit_Limit__c,New_Risk_Category__c,Credit_Limit__c,Percent_of_Limit__c,Exposure__c,
                                              Next_Review_Date__c,Review_End_Date__c,Review_Start_Date__c,External_Link__c,Status__c,                 
                                              Risk_Category__c,Customer_Name__c,Base_Comments__c,Last_Review__c,Customer_Basic_Info_Id__c,
                                              Review_Complete_Date__c,Review_Name__c, Manager_Comments__c,Approval_Comments__c,
                                              Approved_Analyst__c,Analyst_Comments__c,Approved_Manager__c,Approved_Zone_Manager__c,CreatedById
                                       FROM  BSureC_Credit_Increase__c WHERE Id =: Id.valueOf(str_credit_increase_querystring_id)];
                                       
        rec_credit_increase_info =  lst_credit_increase_details[0]; 
        str_customer_info_id =  lst_credit_increase_details[0].Customer_Basic_Info_Id__c;
        system.debug('lst_credit_increase_details[0].Approved_Analyst__c@@'+lst_credit_increase_details[0].Approved_Analyst__c);
        if(lst_credit_increase_details[0].Approved_Analyst__c!=null)
        	strAnalyst = getName(lst_credit_increase_details[0].Approved_Analyst__c);
        else
        	strAnalyst = '';
        
        
        List<User> lst_Role = [SELECT UserRole.Name FROM User WHERE Id =: lst_credit_increase_details[0].CreatedById];
		system.debug('lst_Role@@'+lst_Role);       
        rec_cust_info = new BSureC_Customer_Basic_Info__c();
        
        //SOQL query for customer analyst and manager 
        rec_cust_info = [SELECT Id,Analyst__c,Manager__c,Next_Review_Date__c,Company_DBT__c,Credit_Recommendation__c,Legal_Filings__c,Financial_Risk__c,
								Fraud_Alerts__c,Highest_amt_extended__c,Intelliscore_Plus__c,Report_Date__c
                         FROM BSureC_Customer_Basic_Info__c 
                         WHERE Id =: Id.valueOf( lst_credit_increase_details[0].Customer_Basic_Info_Id__c)];
       
        //system.debug('----lst_credit_increase_details[0].Status__c---'+lst_credit_increase_details[0].Status__c);
        //system.debug('=======rec_profile_info.Name-----'+rec_profile_info.Name);
        //system.debug('=======login_user_id-----'+login_user_id);
        system.debug(rec_profile_info.Name+'=======rec_cust_info.Manager__c-----'+lst_credit_increase_details[0].Status__c);
        if(lst_credit_increase_details[0].Status__c != 'Pending Approval' && lst_credit_increase_details[0].Status__c != 'Pending for Analyst Approval' && lst_credit_increase_details[0].Status__c != 'Started')  
        {
            bl_credit_limit_output_flag = false;
            bl_credit_limit_input_flag = false;
            bl_view_flag = false;
        }
        else
        {
        	if(lst_Role !=null && lst_Role.size()>0 && lst_Role[0].UserRole.Name == 'Collector')
            {
	            bl_credit_limit_output_flag = true;
	            bl_credit_limit_input_flag = false;
	            bl_view_flag = false;
            }
            if(rec_profile_info.Name == 'Analyst')
            {	system.debug('ANALYST@@');
                if(rec_credit_increase_info.New_Credit_Limit__c > Double.valueOf('0') && 
                    rec_credit_increase_info.New_Credit_Limit__c <= Double.valueOf('250000'))
                     
                {
                    bl_credit_limit_output_flag = true;
                    bl_credit_limit_input_flag = false;
                    bl_view_flag = false;
                }
                else
                {
                    bl_credit_limit_output_flag = true;
                }
            }
            if(login_user_id == rec_cust_info.Manager__c && 
                     rec_credit_increase_info.New_Credit_Limit__c >= Double.valueOf('250000') &&
                     rec_credit_increase_info.New_Credit_Limit__c <= Double.valueOf('1000000'))
	         {
	         		bl_credit_limit_output_flag = true;
                    bl_credit_limit_input_flag = false;
                    bl_view_flag = false;
	         }
            if(rec_profile_info.Name == 'Zone Manager')
            {
                if((rec_credit_increase_info.New_Credit_Limit__c >= Double.valueOf('1000000')))
                {
                    bl_credit_limit_output_flag = true;
                    bl_credit_limit_input_flag = false;
                    bl_view_flag = false;
                }
                else
                {
                    bl_credit_limit_output_flag = true;
                }
            }
            if(login_user_id == rec_cust_info.Manager__c && 
               rec_credit_increase_info.New_Credit_Limit__c >= Double.valueOf('250000') &&
               rec_credit_increase_info.New_Credit_Limit__c < Double.valueOf('1000000'))
               {
                    bl_credit_limit_output_flag = true;
                    bl_credit_limit_input_flag = false;
                    bl_view_flag = false;
               }
               else
                {
                    bl_credit_limit_output_flag = true;
                }
            
        }
        
        for(BSureC_Credit_Increase__c rec_credit_increase : lst_credit_increase_details)
        {
            map_credit_increase.put(rec_credit_increase.Id,rec_credit_increase);
        }
        
        //system.debug('----bl_credit_limit_output_flag--'+bl_credit_limit_output_flag);
        //system.debug('----bl_credit_limit_input_flag--'+bl_credit_limit_input_flag);
        //system.debug('-----str_credit_increase_querystring_id---'+str_credit_increase_querystring_id);
    }
    public BSureC_PaymentHistoryApprove()
    {
        
    }
    /// <summary>  
    /// This for Retriving the picklist values of risk category  
    /// </summary>
     public List<SelectOption> getrisk_category_Options() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
      //  Schema.DescribeFieldResult fieldResult = BSureC_Credit_Data_Publish__c.Type.getDescribe();
      //  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      //  for(Schema.PicklistEntry p : ple)
     //   options.add(new SelectOption(p.getValue(), p.getValue()));   
          options.add(new SelectOption('000', '000')); 
          options.add(new SelectOption('001', '001')); 
          options.add(new SelectOption('002', '002')); 
          options.add(new SelectOption('003', '003')); 
          options.add(new SelectOption('004', '004')); 
          options.add(new SelectOption('005', '005')); 
          options.add(new SelectOption('007', '007')); 
        return options;
    }     
    
    public Pagereference cancel()
    {
         pagereference pageref;
         if(bl_view_flag)
         {
            pageref = new pagereference('/apex/BSureC_PaymentHistoryItemstoApprove');
         } 
         else
         {
            //('/apex/BSureC_ViewCustomerDetails?id='& Customer_Basic_Info_Id__r.Id
            pageref = new pagereference('/apex/BSureC_ViewCustomerDetails?id='+str_customer_info_id);
         }      
         return pageref;
    }
    public Pagereference edit12()
    {
        //system.debug('------str_credit_increase_Id--'+str_credit_increase_Id);
        //if(str_flag == 'Credit_Limit')
        //{
            
            //Apexpages.Standardcontroller obj = lst_credit_increase_details[0];
            /*bl_view_flag = true;
            bl_credit_limit_output_flag = false;
            bl_credit_limit_input_flag = true;
            if(lst_credit_increase_details[0].Status__c != 'Review'|| rec_profile_info.Name != 'BSureC_Analyst' || rec_profile_info.Name != 'BSureC_Global Manager')  
            {
                bl_credit_limit_output_flag = false;
                bl_credit_limit_input_flag = false;
                bl_view_flag = false;
                
            }  */
            pagereference pageref = new pagereference('/apex/BSureC_CreditIncreaseSection?pcaid='+str_credit_increase_Id);
            //system.debug('----pageref--'+pageref);
            return pageref;   
        //}
        //return null;
    }
    public Pagereference Save()
    {
        //system.debug('------str_flag--'+str_flag);
        //if(str_flag == 'Credit_Limit')
        //{
           rec_credit_increase_info.Status__c = 'Pending Approval';
            update rec_credit_increase_info;    
            bl_credit_limit_output_flag = true;
            bl_credit_limit_input_flag = false;
             pagereference pageref = new pagereference('/apex/BSureC_PaymentHistoryItemstoApprove');      
             return pageref; 
        //}
        return null;
    }
    public Pagereference approve()
    {
        pagereference pageref = new pagereference('/apex/BSureC_CreditIncreaseSection?pcaid='+str_credit_increase_Id+'&approveflag='+'true');
        return pageref;
    }
    public String getName(Id userID)
    {
    	system.debug('userID####'+userID);
    	list<User> lstUser = [SELECT Name FROM User where Id =: Id.valueOf(userID)];
    	if(lstUser!=null && lstUser.size()>0)
    	return lstUser[0].Name;
    	else
    	return '';
    }
}