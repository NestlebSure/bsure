public Class bSureC_Experian_RL{
    public list<bSureC_CreditAgency_History__c> Histrylist{get;set;}
    
    public bSureC_Experian_RL(ApexPages.StandardController controller) {
       String currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
       Histrylist = new  list<bSureC_CreditAgency_History__c>();
       Histrylist  =[Select id ,Creditsafe_Credit_Limit__c,Creditsafe_Rating__c,CreatedDate,CreditSafe_Provider_Description__c,
                   Credit_Recommendation__c,Company_DBT__c,Report_Date__c,Highest_amt_extended__c
                       from bSureC_CreditAgency_History__c where Customer_Info_Id__c =:currentRecordId   and RecordTypeId ='012d0000001lAYFAA2'];

    }
}