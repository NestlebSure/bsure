/**This is a sample controller with sample data of how to use the
    Paginate class*/
public with sharing class PaginationDemoController1 {
    public PaginateController paginater {get;set;}
    public List<CustomClass> genericList{get;set;}
    public List<List<CustomClass>> fullGenericList{get;set;}
	
	private list<String> lstIds = new list<String>(); 
	Integer counter = 0;//TO track the number of records parsed
    Integer limitSize = 10;//Number of records to be displayed
    Integer totalSize =0; //To Store the total number of records available
    
    public PaginationDemoController1(){
        //Get the data we need to paginate
        List<CustomClass> resultsList = populateData();
		
		system.debug('lstIds==='+lstIds.size());
        //Set the page size
        Integer pageSize = 10;
		totalSize = lstIds.size();
        //Create a new instance of Paginate passing in the overall size of
        //the list of data and the page size you want
        this.paginater = new PaginateController(resultsList.size(), pageSize);

        //These lists hold the data
        this.fullGenericList = new List<List<CustomClass>>();
        this.genericList = new List<CustomClass>();
		
		Set<String> tempIds = new Set<String>();
        //Break out the full list into a list of lists
        if(lstIds.size() > 0){
            for(Integer i = 0; i < pageSize; i++)
            {
            	tempIds.add(lstIds[i]);
            }
        }
        
        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
        if(tempIds!=null && tempIds.size()>0)
        {
        	
	        string strQuery1 = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c, '+ 
	                        'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
	                        'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
	                        ', Owner_Ship__c ,Contact_Name__c '+
	                        ' from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
			
			string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id IN: tempIds ';
	                        
			//AllContacts = Database.query(strQuery);
			Integer i = 1;
			for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
			{
				genericList.add(new CustomClass(i, sObj.Supplier_Name__c));
				i++;
			}
		
        }
        
		
    }
    
    public void beginning(){
   
        genericList.clear();
        counter=0;
        Set<String> tempIds = new Set<String>();
        if((counter + limitSize) <= totalSize){
	        //Break out the full list into a list of lists
	        if(lstIds.size() > 0){
	            for(Integer i=0;i<limitSize;i++)
	            {
	            	tempIds.add(lstIds[i]);
	            }
	        }
           
        } else{
       
            if(lstIds.size() > 0){
	            for(Integer i=0;i<totalSize;i++)
	            {
	            	tempIds.add(lstIds[i]);
	            }
	        }   
           
        }
        
        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
        if(tempIds!=null && tempIds.size()>0)
        {
        	
			string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id IN: tempIds ';
	                        
			//AllContacts = Database.query(strQuery);
			Integer i = counter+1;
			for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
			{
				genericList.add(new CustomClass(i, sObj.Supplier_Name__c));
				i++;
			}
		
        }
       
    }

    public void previousPage(){
    	
        counter=counter-limitSize;    
        system.debug('counter==='+counter);
        if(counter >= 0 && (counter+limitSize) <= totalSize){
           genericList.clear();
            
            Set<String> tempIds = new Set<String>();
	        //Break out the full list into a list of lists
	        if(lstIds.size() > 0){
	            for(Integer i=counter;i<(counter+limitSize);i++)
	            {
	            	tempIds.add(lstIds[i]);
	            }
	        }
	        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
	        if(tempIds!=null && tempIds.size()>0)
	        {
	        	
				string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id IN: tempIds ';
		                        
				//AllContacts = Database.query(strQuery);
				Integer i = counter+1;
				for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
				{
					genericList.add(new CustomClass(i, sObj.Supplier_Name__c));
					i++;
				}
			
	        }
	        
	        system.debug('genericList==='+genericList);
        }   
    }

    public void nextPage(){
        
        genericList.clear();
        counter=counter+limitSize;
       	system.debug('counter==='+counter);
        if((counter+limitSize) <= totalSize){
           
            
            Set<String> tempIds = new Set<String>();
	        //Break out the full list into a list of lists
	        if(lstIds.size() > 0){
	            for(Integer i=counter-1;i<(counter+limitSize -1);i++)
	            {
	            	tempIds.add(lstIds[i]);
	            }
	        }
	        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
	        if(tempIds!=null && tempIds.size()>0)
	        {
	        	
				string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id IN: tempIds ';
		                        
				//AllContacts = Database.query(strQuery);
				Integer i = counter+1;
				for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
				{
					genericList.add(new CustomClass(i, sObj.Supplier_Name__c));
					i++;
				}
			
	        }
	        
	        system.debug('genericList==='+genericList);
        }
    }
	
	public void last (){
   
        genericList.clear();
       
        if(math.mod(totalSize , limitSize) == 0){
            counter = limitSize * ((totalSize/limitSize)-1);
        } else if (math.mod(totalSize , limitSize) != 0){
            counter = limitSize * ((totalSize/limitSize));
        }
        
        Set<String> tempIds = new Set<String>();
        //Break out the full list into a list of lists
        if(lstIds.size() > 0){
            for(Integer i=counter-1;i<totalSize-1;i++)
            {
            	tempIds.add(lstIds[i]);
            }
        }
        
        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
        if(tempIds!=null && tempIds.size()>0)
        {
        	
			string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id IN: tempIds ';
	                        
			//AllContacts = Database.query(strQuery);
			Integer i = counter+1;
			for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
			{
				genericList.add(new CustomClass(i, sObj.Supplier_Name__c));
				i++;
			}
		
        }
       
    }
    
    public Boolean getDisableNext(){
   
        if((counter + limitSize) >= totalSize )
            return true ;
        else
            return false ;
    }
   
    public Boolean getDisablePrevious(){
   
        if(counter == 0)
            return true ;
        else
            return false ;
    } 
    
    public PageReference updatePage(){
        this.paginater.updateNumbers();
        return changeData();
    }

    public PageReference changeData(){
        this.genericList = this.fullGenericList.get(this.paginater.index);
        return null;
    }

    public List<CustomClass> populateData(){
        List<CustomClass> customClassList = new List<CustomClass>();
        /*
        for(Integer i = 1; i < 50; i++){
            customClassList.add(new CustomClass(i, 'Name:  ' + String.valueOf(i)));
        }
        */
        List<BSureS_Basic_Info__c> AllContacts = new list<BSureS_Basic_Info__c>();
        string strQuery1 = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,F_S__c, '+ 
                        'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
                        'Rating__c,Review_Complete_Date__c,Risk_Level__c,Spend__c,Supplier_Countries__c,Parent_Supplier_ID__r.Supplier_Name__c'+
                        ', Owner_Ship__c ,Contact_Name__c '+
                        ' from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
		
		string strQuery = 'select id,Supplier_Name__c from BSureS_Basic_Info__c where id != null LIMIT 50000 ';
                        
		AllContacts = Database.query(strQuery);
		
		/*
		for(Integer i = 1; i < AllContacts.size(); i++)
		{
			customClassList.add(new CustomClass(i, AllContacts[i].Supplier_Name__c));
			
		}
		*/
		Integer i = 1;
		for(BSureS_Basic_Info__c sObj : Database.query(strQuery))
		{
			//customClassList.add(new CustomClass(i, sObj.Supplier_Name__c));
			i++;
			lstIds.add(sObj.Id);
		}
        return customClassList;
    }

    public class CustomClass{
        public Integer num{get;set;}
        public String name{get;set;}

        public CustomClass(Integer num, String name){
            this.num = num;
            this.name = name;
        }
    }
}