/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_BSureS_SpendHistory_Dataupload {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        //test.start();
        blob strval=blob.valueof('str1,str2,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11');
        system.Test.startTest();
        BSureS_SpendHistory_Dataupload test_BSureS_SpendHistory_Dataupload=new BSureS_SpendHistory_Dataupload();
        test_BSureS_SpendHistory_Dataupload.bfilecontent=strval;        
        test_BSureS_SpendHistory_Dataupload.SaveDetailsFile();        
        String assign='1234';
        System.Assertequals('1234',assign);
        system.Test.stopTest();
        
        
    }
}