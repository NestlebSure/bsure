/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CreditDataSectionView {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
        objbasicinfo.Customer_Group_Id__c = '2345';
        objbasicinfo.Customer_Name__c = 'SAM';
        insert objbasicinfo;
         system.assertEquals('SAM', objbasicinfo.Customer_Name__c);     
        BSureC_Credit_Data_Publish__c reccrdtdtpblsh = new BSureC_Credit_Data_Publish__c();
			reccrdtdtpblsh.CE__c = '2314';
			reccrdtdtpblsh.Average_Days_Slow__c = Integer.valueOf('4');
			reccrdtdtpblsh.Bill_to_Account__c = '';
			reccrdtdtpblsh.Credit_Limit__c = Integer.valueOf('12345');
			reccrdtdtpblsh.Discount__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Exposure__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Group_Id__c = '2345';
			reccrdtdtpblsh.Last_Review__c = Date.parse('11/19/2012');
			reccrdtdtpblsh.of_Limit__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Prompt__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.Risk_Category__c = ''; 
			reccrdtdtpblsh.Slow__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh.BSureC_Customer_Basic_Info__c = String.valueOf(objbasicinfo.Id);
			
			insert reccrdtdtpblsh;
			
			BSureC_Credit_Data_Publish__c reccrdtdtpblsh1 = new BSureC_Credit_Data_Publish__c();
			reccrdtdtpblsh1.CE__c = '23145';
			reccrdtdtpblsh1.Average_Days_Slow__c = Integer.valueOf('4');
			reccrdtdtpblsh1.Bill_to_Account__c = '';
			reccrdtdtpblsh1.Credit_Limit__c = Integer.valueOf('12345');
			reccrdtdtpblsh1.Discount__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh1.Exposure__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh1.Group_Id__c = '2345';
			reccrdtdtpblsh1.Last_Review__c = Date.parse('11/19/2012');
			reccrdtdtpblsh1.of_Limit__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh1.Prompt__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh1.Risk_Category__c = '';
			reccrdtdtpblsh1.Slow__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh1.BSureC_Customer_Basic_Info__c = String.valueOf(objbasicinfo.Id);
			
			insert reccrdtdtpblsh1;
			
			BSureC_Credit_Data_Publish__c reccrdtdtpblsh2 = new BSureC_Credit_Data_Publish__c();
			reccrdtdtpblsh2.CE__c = '23145';
			reccrdtdtpblsh2.Average_Days_Slow__c = Integer.valueOf('4');
			reccrdtdtpblsh2.Bill_to_Account__c = '';
			reccrdtdtpblsh2.Credit_Limit__c = Integer.valueOf('12345');
			reccrdtdtpblsh2.Discount__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh2.Exposure__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh2.Group_Id__c = '2345';
			reccrdtdtpblsh2.Last_Review__c = Date.parse('11/19/2012');
			reccrdtdtpblsh2.of_Limit__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh2.Prompt__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh2.Risk_Category__c = '';
			reccrdtdtpblsh2.Slow__c = Decimal.valueOf( Double.valueOf('1234') );
			reccrdtdtpblsh2.BSureC_Customer_Basic_Info__c = String.valueOf(objbasicinfo.Id);
			
			insert reccrdtdtpblsh2;
			
			//system.debug('----obj---'+[SELECT ID,BSureC_Customer_Basic_Info__c FROM BSureC_Credit_Data_Publish__c]);
			BSureC_Customer_Basic_Info__c basicList = [SELECT Id FROM BSureC_Customer_Basic_Info__c Where Id =: objbasicinfo.Id];
			ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
			BSureC_CreditDataSectionView obj = new BSureC_CreditDataSectionView(ssc);
			
	        obj.getTotalPageNumber();
	        obj.getNextButtonDisabled();
	        obj.nextBtnClick();
	        obj.previousBtnClick();
	        obj.LastbtnClick();
	        obj.FirstbtnClick();
	        obj.LastpageData(2);
	        obj.pageData(1);
	        obj.BindData(0);
			
    }
   
}