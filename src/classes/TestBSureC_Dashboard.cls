/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_Dashboard {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>(); 
        BSure_Configuration_Settings__c cSettings3 = new BSure_Configuration_Settings__c();
        cSettings3.Name = 'BSureC_AnalystRole2';
        cSettings3.Parameter_Key__c = 'BSureC_AnalystRole';
        cSettings3.Parameter_Value__c = 'Analyst';
        lstSettings.add(cSettings3);         
        insert lstSettings;
         
        /* 
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_AnalystRole2';
        objCSettings1.Parameter_Key__c = 'BSureC_AnalystRole';
        objCSettings1.Parameter_Value__c = 'Analyst';
        insert objCSettings1;
        */
        BSureC_Dashboard dashObj = new BSureC_Dashboard();
        dashObj.strANALYST=cSettings3.Parameter_Key__c;
        dashObj.getDashboardID();
        dashObj.getDashboardIDAnal();
        string assign='1234';
        system.assertEquals('1234',assign);
    }
}