/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_ChildSupplier {
	
    static testMethod void myUnitTest() {
        
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        
        BSureS_Basic_Info__c parent =new BSureS_Basic_Info__c();
        parent.Contact_name__c = 'Stephen';
        parent.Supplier_Name__c = 'testparent';
        parent.Supplier_Category__c = scategory.id;
        //b.Group_Id__c = '12345'; 
        parent.Review_Status__c='completed';
        insert parent;
        
        BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
        b.Contact_name__c = 'Steve jobs';
        b.Supplier_Name__c = 'test';
        b.Supplier_Category__c = scategory.id;
        b.Parent_Supplier_ID__c = parent.id; 
        //b.Group_Id__c = '12345'; 
        /*b.Zone__c = szone.Id;
        b.Supplier_Zones__c = 'AOA,TEST2';
        b.Supplier_SubZones__c = 'AFRICA,EUROPE';
        b.Sub_Zone__c = sSubZone.Id;
        b.Supplier_Countries__c = 'TEST,TEST2';
        b.BSureS_Country__c = sCount.Id;*/
        b.Review_Status__c='completed';
        insert b;
        
        ApexPages.currentPage().getParameters().put('Id',b.id);
        ApexPages.currentPage().getParameters().put('CustomerId',b.id);
        system.Test.startTest();
        //ApexPages.StandardController controller = new ApexPages.StandardController(b); 
        BSureS_ChildSupplier obj = new BSureS_ChildSupplier();
        obj.strParmSupplierid = parent.id;
        obj.getChildSuppliers();
        obj.FetchCustomerDetails();
        obj.CASave();
        obj.cancel();
    }
}