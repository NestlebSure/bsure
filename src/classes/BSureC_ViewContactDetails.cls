/************************************************************************************************       
Controller Name         : BSureV_ViewContactDetails       
Date                    : 10/11/2014        
Author                  : satish.c      
Purpose                 : Customer Contact Details       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          10/11/2014                  Initial Version
**************************************************************************************************/

public with sharing class BSureC_ViewContactDetails {


	public string strContactId{get;set;}
	
	public BSureC_ViewContactDetails() 
    {
    	strContactId=Apexpages.currentPage().getParameters().get('Id');
    	
    }
	public BSureC_Assigned_Contacts__c getContactDetails() 
    {
        BSureC_Assigned_Contacts__c cDetails;
        cDetails = [select id,Customer_Id__c from BSureC_Assigned_Contacts__c where id =: strContactId limit 1];  
        return cDetails;
    }
}