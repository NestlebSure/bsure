/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_MirrorReviewBatch {

    static testMethod void myUnitTest() {
        
         BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
         objCSettings1.Name = 'BSureS_SendMirrorReviewMail1';
         objCSettings1.Parameter_Key__c = 'BSureS_SendMirrorReviewMail';
         objCSettings1.Parameter_Value__c = 'TRUE';
         insert objCSettings1;
         
         BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
         objCSettings2.Name = 'BSureS_SendMirrorReviewMailID1';
         objCSettings2.Parameter_Key__c = 'BSureS_SendMirrorReviewMailID';
         objCSettings2.Parameter_Value__c = 'satish.c@vertexcs.com';
         insert objCSettings2;
         
         BSureS_Document_Type__c objDocType = new BSureS_Document_Type__c(Document_Type_Name__c = 'Presdfdfd23232entations');
         insert objDocType;
        
         BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher Supplier');
         insert objSupplier;
         
         BSureS_Credit_Analysis_Section__c objbasicinfo = new BSureS_Credit_Analysis_Section__c();
         objbasicinfo.Supplier_ID__c = objSupplier.id;
         //objbasicinfo.strSelectedBtn = 'Link';
         //objCommonInfo2.strObject = 'BSureS_Credit_Status_Section__c';
         //objbasicinfo.fileName = 'TestFile'; 
         objbasicinfo.Comment__c = 'Topic Comments';
         objbasicinfo.Document_Type__c = objDocType.Id;
         //objCommonInfo2.m_fileBody = blob.valueof('Attachement File');
         //objCommonInfo2.strTitle ='Presentations On Education';
         //objCommonInfo2.SaveDetails();
         objbasicinfo.Mirror_Support__c = objbasicinfo.Id;
         //objbasicinfo.Analyst__c = Userinfo.getUserId();
         //objbasicinfo.Manager__c = Userinfo.getUserId();
         //objbasicinfo.Backup_Analysts__c= '2314';
         insert objbasicinfo;
         
         BSureS_Credit_Analysis_Section__c objbasicinfo1 = new BSureS_Credit_Analysis_Section__c();
         objbasicinfo1.Supplier_ID__c = objSupplier.id;
         objbasicinfo1.Comment__c = 'Topic Comments1';
         objbasicinfo1.Document_Type__c = objDocType.Id;
         objbasicinfo1.Mirror_Support__c = objbasicinfo.Id;
         insert objbasicinfo1;
         
         set<id> lstSource = new set<id>();
         list<BSureS_Credit_Analysis_Section__c> lstdest = new list<BSureS_Credit_Analysis_Section__c>();
         lstSource.add(objbasicinfo.id);
         lstdest.add(objbasicinfo1);
         
         Database.BatchableContext ctx = null;
         Database.BatchableContext BC =null;
         
         BSureS_MirrorReviewBatch b = new BSureS_MirrorReviewBatch(lstdest,'',null);
         b.start(ctx);
         b.execute(BC, lstdest);
         //Database.executebatch(b,10);
         b.finish(BC);
         b.SendJobStatus();
    }
}