/***********************************************************************************************
* Controller Name   : BSureC_ReviewsOverdueReport
* Date              : 28/12/2012 
* Author            : suresh.v
* Purpose           : Class for Review reports
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- ------------------------- 
* 28/12/2012            suresh.v            Initial Version 
*08/01/2013             suresh.v            Modified by suresh 
*10/04/2014(mm/dd/yy)	Vidya Sagar H		Added Collector filter for reports
**************************************************************************************************/ 
 
global with sharing class BSureC_ReviewsOverdueReport  
{  
    public list<BSureC_Customer_Basic_Info__c> lstCustomers{get;set;} 
    public Transient string strCustomerName{get;set;}//To hold Supplier name
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public string strAnalystId{get;set;}  // to store Country Id
    public string strReportstatus{get;set;}// to get Report Satus
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records  
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public list<SelectOption> AnalystOptions{get;set;} // selectOptins for list of User(Analyst) Records
    public List<User> lstAnalystUser{get;set;} 
    public date startDate{get;set;}
    public date endDate{get;set;}
    public string strTodayDate{get;set;}
    public string strFromDate{get;set;}//To Hold Search from data
    public string strToDate{get;set;}//To Hold Search To data 
    public string todaydate{get;set;} 
    public  string  strAnalystName {get;set;}
    public  string strAnalystSearch{get;set;}   
    public String[] strAnalystProfiles=new String[] {'BSureC_Analyst'};//Profiles to be allowed
    BSureC_Customer_Basic_Info__c objCustInfo;
     
    //for pagination    
    //public list<BSureC_Customer_Basic_Info__c> lstResults {get;set;}//To hold results list
    public list<BSureC_Customer_Basic_Info__c> lstResultsPage {get;set;}//To hold results list
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public  boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public Boolean IsExportVisible{get;set;}
    Public set<string>setanalyst;
    
     // Starting........Vidya Sagar 
     public string strCollectorName {get;set;}
     public boolean showCollector {get;set;}
     public list<User> lstCollectorUser {get;set;}
     public String[] strCollectorProfiles=new String[] {'BSureC_Collector'};//Profiles to be allowed
     public String[] strCollectorRole=new String[] {'Collector'};//Roles to be allowed
     public  string strCollectorSearch{get;set;} 
     public string strCollectorID {get;set;}
     public void showCollectorDetails()
    {
        showCollector=true;  
        
       lstCollectorUser=[Select Id, Name from User where Profile.Name IN:strCollectorProfiles and IsActive=true and UserRole.Name IN:strCollectorRole order By Name Asc];
        
    }
    
    public void searchCollector()
    {
        
        string strCollSrch = '%'+strCollectorSearch+'%';
        if(strCollectorSearch != null && strCollectorSearch != '') 
        {
            lstCollectorUser = [Select Id, Name from User where Name like: strCollSrch AND Profile.Name IN:strCollectorProfiles AND IsActive=true and UserRole.Name IN:strCollectorRole order By Name Asc];
        }
        else
        {
            lstCollectorUser = [Select Id, Name from User where IsActive=true AND Profile.Name IN:strCollectorProfiles and UserRole.Name IN:strCollectorRole order By Name Asc];           
        }
    }
    public void closepopupColl() 
    {
        string strSelectedCollector = apexpages.currentpage().getparameters().get('Collectorid');
        strCollectorID = apexpages.currentpage().getparameters().get('CollectorUserid');
        strCollectorName=strSelectedCollector;
        system.debug(strCollectorID+'strSelectedCollector@@@@@'+strSelectedCollector);
        showCollector=false; 
    }
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupCollector() 
    {
        showCollector=false; 
    }
     // Ending...........Vidya Sagar H
     //Sort Variables
    private string  sortDirection = 'ASC'; 
     private string  sortExp = 'Customer_Name__c';
        
     public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() 
    {  
        //system.debug('sort......');
        getCustomers();
    }   
         
    public BSureC_ReviewsOverdueReport()
    {
        lstCustomers  = new list<BSureC_Customer_Basic_Info__c>();
        //lstResults = new  list<BSureC_Customer_Basic_Info__c> ();
        //strAnalystId='All';
        objCustInfo=new BSureC_Customer_Basic_Info__c();
        lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
        
         
        List<BSureC_AdditionalAttributes__c>  AAttributes = new List<BSureC_AdditionalAttributes__c> ();
        
         
         AAttributes =[select id,User__c,
                                              BSureC_Zone__c,
                                              BSureC_Sub_Zone__c,
                                              BSureC_Country__c 
                                              FROM BSureC_AdditionalAttributes__c WHERE User__c =:UserInfo.getUserId() limit 1]; 
         
        
        if(AAttributes != null && AAttributes.size()> 0 ){
            strZoneId       = AAttributes[0].BSureC_Zone__c;
            strSubZoneId    = AAttributes[0].BSureC_Sub_Zone__c;
            strCountryId    = AAttributes[0].BSureC_Country__c;
        }
        
        
        Date dtYear = Date.Today().addDays(-365);           
        Date dtToday = Date.Today();        
        //strFromDate = dtYear.format();
        //strToDate =  dtToday.format();        
         strTodayDate = Date.today().format();
         IsExportVisible=true;
        getZones();
        getSubZones();
        getCountries();
        //getCustomers(); 
        getAnalysts();      
        
        totallistsize=0;
        pageNumber = 0;
        totalPageNumber = 0;
    }
    
    
    
    //public BSureC_Customer_Basic_Info__c getProxyObject() { return objCustInfo; }
    // <Summary> 
    //Method Name : getSuppliers
    // </Summary>
    //Description : query all the Suppliers with specific filters
    public void getCustomers()
    {   
        String strgetanalyst;
        Date dtFrom;
        Date dtTo;
        lstCustomers  = new list<BSureC_Customer_Basic_Info__c>();
        lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
        
         
        dtFrom  = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        dtTo    = ((strToDate != null && strToDate != '')?date.parse(strToDate):null);       
        
        string strQuery = 'SELECt Id,Analyst__r.Name,Country__r.Name,Customer_Name__c,Review_Status__c,Collector_Name__c  from BSureC_Customer_Basic_Info__c '+
                          ' where Analyst__c != null ';      
          
       
            
           if(strZoneId != null && strZoneId != 'ALL')
           {            
             strQuery +=' AND Zone__c =: strZoneId ';
           }            
            
           if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
           {            
             strQuery +=' AND Sub_Zone__c =: strSubZoneId ';
           }           
            
           if(strCountryId != null && strCountryId != 'ALL'  )
           {             
             strQuery +=' AND Country__c =: strCountryId ';
           }                          
           
          //system.debug('strAnalystName**'+strAnalystName);  
           if(strAnalystName != null && strAnalystName != '')
            {
                strgetanalyst= '%'+strAnalystName+'%';
                
                strQuery += ' AND Analyst__r.Name like:strgetanalyst';
            }   
          
            //system.debug('strCustomerName'+strCustomerName);
           if(strCustomerName != null && strCustomerName!='')
           {  
               string strcustname='%'+strCustomerName+'%';                
               strQuery +=' AND Customer_Name__c Like: strcustname ';
           }
            string sortFullExp = sortExpression  + ' ' + sortDirection; 
           //Below code is used for actual values, just commented to check other conditions ,should be uncommented      
         
         string strStatus='Overdue';
         strQuery +=' AND Review_Status__c =: strStatus ';
        
         strQuery += ' order by ' + sortFullExp +' limit 1000 ';     
             
          // starting......  Vidya Sagar
          if(strCollectorName != null && strCollectorName!='')
        	{
        		//system.debug('++++++++++++++++++'+strCollectorName);
        		list<BSureC_Customer_Basic_Info__c> lstCrdInc = database.query(strQuery);
        	//system.debug('++++++++++++++++++'+lstCrdInc.size()); 
        	 for(integer i=0;i<lstCrdInc.size();i++)
        	  {
        	  	string Collectorname = lstCrdInc[i].Collector_Name__c;
        	  	if(Collectorname != null && Collectorname !=''){
        	    BSureC_Customer_Basic_Info__c c = new BSureC_Customer_Basic_Info__c();
        	    c = lstCrdInc[i];
        	   // system.debug('==============='+lstCrdInc[i].Collector_Name__c);
        	    if(c.Collector_Name__c.contains(strCollectorName)){
        	    	//system.debug('----------record id -----'+c.Collector_Name__c);
        	    	lstCustomers.add(c);
        	    }
        	  	}
        	  }
        	}
        	else{
           lstCustomers = database.query(strQuery);
        
        	}  
        	// ending....... Vidya Sagar 
        //system.debug('StringQuery:::::::::'+strQuery);
        //lstCustomers = database.query(strQuery);                  
        totallistsize= lstCustomers.size();// this size for pagination
        //system.debug('totallistsize**'+totallistsize);
        pageSize = Integer.valueOf(BSureC_CommonUtil.getConfigurationValues('BSureC_Report_Page_Size').get(0));// default page size will be 10
        if(totallistsize==0)
        {
            IsExportVisible=true;
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber= lstResultsPage.size();
            //system.debug('if NxtPageNumber**'+NxtPageNumber);
            PrevPageNumber=0;
        }
        else
        {
            IsExportVisible=false;
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber= lstResultsPage.size();
            //system.debug('else NxtPageNumber**'+NxtPageNumber);
            PrevPageNumber=1;
        } 
           
    }
    
    public void getZones()
    {   
        //BSureC_CommonUtil.isReport = true;
        zoneOptions = new list<SelectOption>();
        BSureC_CommonUtil.blnflag = true;
        
        zoneOptions = BSureC_CommonUtil.getZones(); 
        
    }
     // <Summary> 
   // Method Name : getSubZones
   // </Summary>
   // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones
    public void getSubZones()
    {    
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
            
        getCountries();
        
        //strSubZoneId ='Select';
        //strCountryId = 'Select';      
        //BSureC_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();      
        BSureC_CommonUtil.blnflag = true;
        subZoneOptions  = BSureC_CommonUtil.getSubZones(strZoneId);
        //getCustomers();
    }
     // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries()
    {               
        if(strZoneId == 'ALL' || strSubZoneId == 'ALL' )
         strCountryId = 'ALL';
        //if((strZoneId == 'Select' && strSubZoneId == 'Select') || strSubZoneId != 'Select')
            //strCountryId = 'Select';
        //BSureC_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>(); 
        BSureC_CommonUtil.blnflag = true;           
        coutryOptions = BSureC_CommonUtil.getCountries(strSubZoneId);       
        //getCustomers();
        
    }    
    public void showBuyerDetails()
    {
        showeditrecs=true;        
        ////system.debug('category==ShowDetail===Supplier=Name=='+ strsupplierCategoryName.Name);  
       // profile getprofileId = [select id from profile where name = 'BSureS_Buyer'];
       //Selecting users with Profile BSureC_Analyst
       lstAnalystUser=[Select Id, Name from User where Profile.Name IN:strAnalystProfiles and IsActive=true order By Name Asc];
        
    }
    
    public void closepopup() 
    {
        string strSelectedAnalyst = apexpages.currentpage().getparameters().get('Analystid');
        strAnalystName=strSelectedAnalyst;        
        showeditrecs=false; 
    }
    
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        //system.debug('strSearchBuyer========'+strAnalystSearch);
        string strAnalSrch = '%'+strAnalystSearch+'%';
        if(strAnalystSearch != null && strAnalystSearch != '') 
        {
            lstAnalystUser = [Select Id, Name from User where Name like: strAnalSrch AND Profile.Name IN:strAnalystProfiles AND IsActive=true order By Name Asc];
        }
        else
        {
            lstAnalystUser = [Select Id, Name from User where IsActive=true AND Profile.Name IN:strAnalystProfiles order By Name Asc];           
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
    public void getAnalysts()
    {
        AnalystOptions = new list<selectOption>();
        setanalyst=new set<string>();
        //list<User> lstUser = [select Id,Name from User where Profile.Name=:'BSureC_Analyst'];
        list<User> lstUser = [select Id,Name from User where Profile.Name IN:strAnalystProfiles and IsActive=true Order By Name Asc];
        lstUser.sort();
        AnalystOptions.add(new selectOption('ALL','ALL'));
        for(User U:lstUser )
        {
            AnalystOptions.add(new selectOption(u.Id,U.Name));
            setanalyst.add(u.Id);   
        }
    }
    
    public pagereference cancel()
    {
       pageReference pRef = new pageReference('/apex/BSureC_Reports');
       return pRef;
    }
    //for pagination methods
    
    public void BindData(Integer newPageIndex)
    {
       
        try
        {
            
            lstResultsPage = new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            if(lstCustomers != NULL)
            {
                for(BSureC_Customer_Basic_Info__c b : lstCustomers)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstResultsPage.add(b);// here adding files list
                    }
                }
            }
            
            pageNumber = newPageIndex;
            //system.debug('pageNumber**'+pageNumber);
            NlistSize = lstResultsPage.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }
    
     // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if(totallistsize!=null)
        {
            if (totallistsize ==0) 
                return true;
            else
                return ((pageNumber * pageSize) >= totallistsize);
        }
        return true;
    }
    
    /// <summary>
    /// Below method gets the total no.of pages.   
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=null && totallistsize!=0  )
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
             
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
   /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstResultsPage=new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(BSureC_Customer_Basic_Info__c a : lstCustomers)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstResultsPage.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstResultsPage.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
    
    public pageReference  ExportCSV()
    {
        if(lstCustomers != NULL && lstCustomers.size()>0)
        {
           PageReference pageRef;
           pageRef= new PageReference('/apex/BSureC_ReviewStatusReportExport'); 
           return pageRef;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Rocords to Export'));
            return null;
        }
    }

}