/************************************************************************************************       
Controller Name         : BSureC_CustomerDocumentTypeReport 
Date                    : 1/18/2013        
Author                  : Praveen S       
Purpose                 : cutomer document type report      
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          1/18/2013      Praveen S            Initial Version
**************************************************************************************************/
global with sharing class BSureC_CustomerDocumentTypeReport 
{  
    public Boolean showErrorMsg{get;set;}
    public boolean renexportexcel{get;set;}
    public boolean rensearch{get;set;}
    public string strDocumentType{get;set;} 
    public string strFromDate{get;set;}
    public string strToDate{get;set;}
    public String strCustomer{get;set;}
    public integer level; //for folder levels
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public List<string> leftselected{get;set;} //  Category values 
    public List<string> rightselected{get;set;}//  Category values 
    public transient List<BSureC_Unearned_Cash_Discount_Section__c> lstUnearnedCash;
    public transient List<BSureC_On_Going_Credit_Section__c> lstOngoingCredit;
    public transient List<BSureC_Unauthorized_Deduction_Section__c> lstunauthorizedded;
    public transient List<BSureC_Confidential_Documents_Section__c> lstconfdocsection;
    public transient List<BSureC_Invoice_Collection_Section__c> lstinvoicecolsection; 
    public transient List<BSureC_Credit_Increase_Section__c> lstcreditanalysissection;   
    public List<DocumentWrapper> lstDocumentWrapper;
    public List<DocumentWrapper> lstDocumentWrapperexport{get;set;}
    public List<DocumentWrapper> lstDocumentWrapperpag{get;set;}
    public list<BSureC_Document_Types__c> lstDocsObject{get;set;}
    public Set<string> leftvalues = new Set<string>(); //list of leftvalues for unselected()
    public Set<string> rightvalues = new Set<string>();//list of rightvalues for selected ()
    public set<string> setcustomernames{get;set;}
    public Date dtFrom{get;set;}
    public Date dtTo{get;set;}
    public string todaydate{get;set;}   
    public string strCustIdParam{get;set;}		//XREF Changes Aditya V 08/13/2012
     
    public class DocumentWrapper
    {
        public string strCustomername{get;set;}
        public string strCreditAcc{get;set;}
        public string strCustomerid{get;set;}
        public string strCommonCustDocvalue{get;set;}
        public List<DocumentWrapperdoc> docwrap{get;set;} 
    }
    
    public class DocumentWrapperdoc
    {
        public string strdocname{get;set;}
        public integer doccount{get;set;}
    }

    public BSureC_CustomerDocumentTypeReport()
    {
        renexportexcel=true;
        rensearch=true;
        totallistsize=0;
        pageNumber = 0;
        totalPageNumber = 0;
        lstUnearnedCash=new List<BSureC_Unearned_Cash_Discount_Section__c>();
        lstOngoingCredit=new List<BSureC_On_Going_Credit_Section__c>();
        lstunauthorizedded=new List<BSureC_Unauthorized_Deduction_Section__c>();
        lstconfdocsection=new List<BSureC_Confidential_Documents_Section__c>();
        lstinvoicecolsection=new List<BSureC_Invoice_Collection_Section__c>(); 
        lstcreditanalysissection=new List<BSureC_Credit_Increase_Section__c>();
        lstDocumentWrapper=new List<DocumentWrapper>();

        lstDocumentWrapperpag=new List<DocumentWrapper>();
        lstDocumentWrapperexport=new List<DocumentWrapper>();
        lstDocsObject=new list<BSureC_Document_Types__c>();
        lstDocumentWrapper=new List<DocumentWrapper>(); 
        leftselected = new List<string>();
        rightselected = new List<string>();

        Date dtYear = Date.Today().addDays(-7);      
        Date dtToday = Date.Today();    
        strFromDate = dtYear.format();
        strToDate =  dtToday.format();      
        todaydate =  dtToday.format();
        showErrorMsg=false;
		//NAGA Check if the user has read access on the BSureC_Document_Types__c object
         if (Schema.sObjectType.BSureC_Document_Types__c.isAccessible())
         {
	         lstDocsObject = [select id,Code__c,Document_Type_Name__c from BSureC_Document_Types__c WHERE Document_Type_Name__c!=NULL];
	         
	             for(BSureC_Document_Types__c DocumentTemp:lstDocsObject)
	             {
	                leftvalues.add(DocumentTemp.Document_Type_Name__c);
	             }
         }
     }
                 
     public Map<String,List<String>> populateCustDetails(Map<String,List<String>> MapcustNamedocID
                                                                        ,String customerId,String docType)
     {
        if(MapcustNamedocID.containsKey(customerId))
        {
            List<String> newList1=MapcustNamedocID.get(customerId);
            newList1.add(docType);
            MapcustNamedocID.put(customerId,newList1);  
        }
        else
        {
            List<String> newList= new List<String>();
            newList.add(docType);
            MapcustNamedocID.put(customerId,newList); 
        }
        return MapcustNamedocID;
     }
     

  public PageReference Search()
  {   
        renexportexcel=true;
        showErrorMsg=false;
        //system.debug('strCustomer......'+strCustomer);
        //system.debug('rightvalues....'+rightvalues);
        Map<String,integer> MapCustdoctype=new Map<String,integer>();
        Map<String,String> MapDocnameID=new Map<String,String>();
        Map<String,List<String>> MapcustNamedocID=new Map<String,List<String>>();
        lstUnearnedCash=new List<BSureC_Unearned_Cash_Discount_Section__c>();
        lstOngoingCredit=new List<BSureC_On_Going_Credit_Section__c>();
        lstunauthorizedded=new List<BSureC_Unauthorized_Deduction_Section__c>();
        lstconfdocsection=new List<BSureC_Confidential_Documents_Section__c>();
        lstinvoicecolsection=new List<BSureC_Invoice_Collection_Section__c>(); 
        lstcreditanalysissection=new List<BSureC_Credit_Increase_Section__c>();
        lstDocumentWrapper=new List<DocumentWrapper>();
        //lstDocumentWrapper.clear();
         lstDocumentWrapperpag=new List<DocumentWrapper>();
        setcustomernames=new Set<String>();
        List<DocumentWrapperDoc> wapdoc= new list<DocumentWrapperDoc>();
        pageSize = 10;// default page size will be 10
        Integer g=1;
        
        dtFrom   = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        dtTo  = ((strToDate != null && strToDate != '')?date.parse(strToDate):null);  
        dtTo   = dtTo.addDays(2); 
          
      //system.debug('dtFrom=============='+dtFrom);
      //system.debug('dtTo=============='+dtTo);
      //system.debug('rightselected======================'+rightselected);
      //system.debug('rightvalues======================'+rightvalues);

//************************************below for CreditAnalysis object***************************************************************
  	  //NAGA Check if the user has read access on the BSureC_Credit_Increase_Section__c object
      if(!Schema.sObjectType.BSureC_Credit_Increase_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strcreditdatasec='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_Credit_Increase_Section__c'+
                         '   WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strcreditdatasec+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strcreditdatasec+= ' And Document_Type_Name__c in : rightvalues '; 
    }
    strcreditdatasec+=' order by Document_Type_Name__c';
    
    lstcreditanalysissection=database.query(strcreditdatasec);
    
    if(lstcreditanalysissection.size()>0)
    {        
        for(BSureC_Credit_Increase_Section__c creditanalysis:lstcreditanalysissection)
        {
          setcustomernames.add(creditanalysis.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,creditanalysis.Customer_ID__c,creditanalysis.Document_Type_Name__c);
          MapDocnameID.put(creditanalysis.Document_Type_Name__c ,creditanalysis.Document_Type__c);
              if(MapCustdoctype.containsKey(creditanalysis.Document_Type__c+'~'+creditanalysis.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(creditanalysis.Document_Type__c+'~'+creditanalysis.Customer_ID__c);
                MapCustdoctype.put(creditanalysis.Document_Type__c+'~'+creditanalysis.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(creditanalysis.Document_Type__c+'~'+creditanalysis.Customer_ID__c,g);
              }
        }
    }  
//************************************below for unearned object***************************************************************
  	//NAGA Check if the user has read access on the BSureC_Unearned_Cash_Discount_Section__c object
      if(!Schema.sObjectType.BSureC_Unearned_Cash_Discount_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strunearnedcash='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_Unearned_Cash_Discount_Section__c'+
                         '   WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strunearnedcash+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strunearnedcash+= ' And Document_Type_Name__c in : rightvalues '; 
    }
    strunearnedcash+=' order by Document_Type_Name__c';
    
    lstUnearnedCash=database.query(strunearnedcash);
    
    if(lstUnearnedCash.size()>0)
    {        
        for(BSureC_Unearned_Cash_Discount_Section__c Unearned:lstUnearnedCash)
        {
          setcustomernames.add(Unearned.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,Unearned.Customer_ID__c,Unearned.Document_Type_Name__c);
          MapDocnameID.put(Unearned.Document_Type_Name__c ,Unearned.Document_Type__c);
              if(MapCustdoctype.containsKey(Unearned.Document_Type__c+'~'+Unearned.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(Unearned.Document_Type__c+'~'+Unearned.Customer_ID__c);
                MapCustdoctype.put(Unearned.Document_Type__c+'~'+Unearned.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(Unearned.Document_Type__c+'~'+Unearned.Customer_ID__c,g);
              }
        }
    }
 //*************************************below for Ongoing object***************************************************************
  	//NAGA Check if the user has read access on the BSureC_On_Going_Credit_Section__c object
      if(!Schema.sObjectType.BSureC_On_Going_Credit_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strOngoing='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_On_Going_Credit_Section__c '+
                    '  WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strOngoing+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strOngoing+= ' And Document_Type_Name__c in : rightvalues'; 
    }
    strOngoing+=' order by Document_Type_Name__c';
    
    lstOngoingCredit=database.query(strOngoing);
    system.debug('lstOngoingCredit####'+lstOngoingCredit.size());
    if(lstOngoingCredit.size()>0)
    {        
        for(BSureC_On_Going_Credit_Section__c ongoing:lstOngoingCredit)
        {
          setcustomernames.add(ongoing.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,ongoing.Customer_ID__c,ongoing.Document_Type_Name__c);
          MapDocnameID.put(ongoing.Document_Type_Name__c ,ongoing.Document_Type__c);
              if(MapCustdoctype.containsKey(ongoing.Document_Type__c+'~'+ongoing.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(ongoing.Document_Type__c+'~'+ongoing.Customer_ID__c);
                MapCustdoctype.put(ongoing.Document_Type__c+'~'+ongoing.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(ongoing.Document_Type__c+'~'+ongoing.Customer_ID__c,g);
              }
        }
    }
 //*************************************below for Unauthorized object***************************************************************
  	//NAGA Check if the user has read access on the BSureC_Unauthorized_Deduction_Section__c object
      if(!Schema.sObjectType.BSureC_Unauthorized_Deduction_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strunauthorized='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_Unauthorized_Deduction_Section__c  '+
                        '  WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strunauthorized+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strunauthorized+= ' And Document_Type_Name__c in : rightvalues'; 
    }
    strunauthorized+=' order by Document_Type_Name__c';
    
    lstunauthorizedded=database.query(strunauthorized);
    
    if(lstunauthorizedded.size()>0)
    {        
        for(BSureC_Unauthorized_Deduction_Section__c unauthorized:lstunauthorizedded)
        {
          setcustomernames.add(unauthorized.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,unauthorized.Customer_ID__c,unauthorized.Document_Type_Name__c);
          MapDocnameID.put(unauthorized.Document_Type_Name__c ,unauthorized.Document_Type__c);
              if(MapCustdoctype.containsKey(unauthorized.Document_Type__c+'~'+unauthorized.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(unauthorized.Document_Type__c+'~'+unauthorized.Customer_ID__c);
                MapCustdoctype.put(unauthorized.Document_Type__c+'~'+unauthorized.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(unauthorized.Document_Type__c+'~'+unauthorized.Customer_ID__c,g);
              }
        }
    }
 //*************************************below for Confidential object***************************************************************
  	//NAGA Check if the user has read access on the BSureC_Confidential_Documents_Section__c object
      if(!Schema.sObjectType.BSureC_Confidential_Documents_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strconfidential='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_Confidential_Documents_Section__c '+
                        '  WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strconfidential+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strconfidential+= ' And Document_Type_Name__c in : rightvalues'; 
    }
    strconfidential+=' order by Document_Type_Name__c';
    
    lstconfdocsection=database.query(strconfidential);
    
    if(lstconfdocsection.size()>0)
    {        
        for(BSureC_Confidential_Documents_Section__c confidential:lstconfdocsection)
        {
          setcustomernames.add(confidential.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,confidential.Customer_ID__c,confidential.Document_Type_Name__c);
          MapDocnameID.put(confidential.Document_Type_Name__c ,confidential.Document_Type__c);
              if(MapCustdoctype.containsKey(confidential.Document_Type__c+'~'+confidential.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(confidential.Document_Type__c+'~'+confidential.Customer_ID__c);
                MapCustdoctype.put(confidential.Document_Type__c+'~'+confidential.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(confidential.Document_Type__c+'~'+confidential.Customer_ID__c,g);
              }
        }
    }
 //*************************************below for Invoice object***************************************************************
  	//NAGA Check if the user has read access on the BSureC_Invoice_Collection_Section__c object
      if(!Schema.sObjectType.BSureC_Invoice_Collection_Section__c.isAccessible())
	  { 
	  	return null;
	  }
  	string strinvoice='SELECT id,Customer_ID__c,Document_Type_Name__c,Document_Type__c from BSureC_Invoice_Collection_Section__c '+
                    '  WHERE  CreatedDate >=:dtFrom and CreatedDate <=: dtTo ';
    if(strCustomer != null && strCustomer!='') 
    {
      strinvoice+= ' And Customer_ID__r.Customer_Name__c =: strCustomer'; 
    }
    
    if(rightvalues!=null && rightvalues.size()>0)
    {
       strinvoice+= ' And Document_Type_Name__c in : rightvalues'; 
    }
    strinvoice+=' order by Document_Type_Name__c';
    
    lstinvoicecolsection=database.query(strinvoice);
    
    if(lstinvoicecolsection.size()>0)
    {        
        for(BSureC_Invoice_Collection_Section__c invoice:lstinvoicecolsection)
        {
          setcustomernames.add(invoice.Customer_ID__c);
          MapcustNamedocID =populateCustDetails(MapcustNamedocID,invoice.Customer_ID__c,invoice.Document_Type_Name__c);
          MapDocnameID.put(invoice.Document_Type_Name__c ,invoice.Document_Type__c);
              if(MapCustdoctype.containsKey(invoice.Document_Type__c+'~'+invoice.Customer_ID__c))
              {
                Integer a=MapCustdoctype.get(invoice.Document_Type__c+'~'+invoice.Customer_ID__c);
                MapCustdoctype.put(invoice.Document_Type__c+'~'+invoice.Customer_ID__c,a+1);
              }
              else
              {
                MapCustdoctype.put(invoice.Document_Type__c+'~'+invoice.Customer_ID__c,g);
              }
        }
    }
    //system.debug('setcustomernames================='+setcustomernames.size());
        if(setcustomernames.size()>0 && Schema.sObjectType.BSureC_Customer_Basic_Info__c.isAccessible())
        {
              for(BSureC_Customer_Basic_Info__c Customer:[SELECT id,Customer_Name__c,Credit_Account__c from BSureC_Customer_Basic_Info__c where id IN: setcustomernames order by Customer_Name__c])
              {
                DocumentWrapper Objwrap=new DocumentWrapper();
                Objwrap.strCustomername=Customer.Customer_Name__c;
                Objwrap.strCustomerid=Customer.id;
                Objwrap.strCreditAcc = Customer.Credit_Account__c;
                List<String> docTypeList=MapcustNamedocID.get(Customer.id);
                Set<String> docTypeSet=new Set<String>();
                docTypeSet.addAll(docTypeList);
                    if(docTypeSet != null && docTypeSet.size()>0)
                    {
                        for(String str:docTypeSet)
                        {
                            DocumentWrapperDoc Cust=new DocumentWrapperDoc();  
                            Cust.strdocname=str;
                            String custId=MapDocnameID.get(str);
                            Cust.doccount=MapCustdoctype.get(custId+'~'+Customer.id);
                            wapdoc.add(Cust);
                        }   
                                 
                        Objwrap.docwrap=wapdoc;
                    }
                    
                    wapdoc=new list<DocumentWrapperDoc>();
                    lstDocumentWrapper.add(Objwrap);
              }
            
        }
        lstDocumentWrapperexport=lstDocumentWrapper;
        totallistsize=lstDocumentWrapper.size();//for pagination  

        if(totallistsize==0)
        {
            //system.debug('inside=======00000000======'+totallistsize);
            showErrorMsg=true;
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstDocumentWrapperpag.size();
            PrevPageNumber=0;
        }
        else
        {
            //system.debug('inside=======else======'+totallistsize);
            renexportexcel=false;
            showErrorMsg=false;
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstDocumentWrapperpag.size();
            PrevPageNumber=1;
            
        }           
      //system.debug('showErrorMsg====aftercondition========'+showErrorMsg);
      //system.debug('lstDocumentWrapper============'+lstDocumentWrapper);
     return null;   
  }
  
  public pageReference ExportExcel() 
  {
    /*//getExport();
    lstDocumentWrapperexport=new List<DocumentWrapper>();
    lstDocumentWrapperexport=lstDocumentWrapper;
    pageReference pRef = new pageReference('/apex/BSureC_CustomerDocReportExportExcel');
    return pRef;*/
    //getExport();
    pageReference pRef;
    lstDocumentWrapperexport=new List<DocumentWrapper>();
    lstDocumentWrapperexport=lstDocumentWrapper;
    pRef = new pageReference('/apex/BSureC_CustomerDocReportExportExcel');
    /*if(lstDocumentWrapperexport.size()>=1000)
    {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Cannot export more than thousand records'));
    }
    else
    {
     pRef = new pageReference('/apex/BSureC_CustomerDocReportExportExcel');
    }*/
    return pRef;
  }

   //Method Name : selectclick
     // Description : selected values removing from the list
    public PageReference selectclick()
    {
        rightselected.clear();
        for(String strLeft: leftselected)
        {
            leftvalues.remove(strLeft);
            rightvalues.add(strLeft);
            //system.debug('rightvalues=====inselect===='+rightvalues.size());
        }
        if(rightvalues.size()==0)
        {
            rensearch=true;
        }
        else
        {
            rensearch=false;
        }
        return null;
    }
    
    //    Method Name : unselectclick
    //    Description : selected values removing from the list
    public void unselectclick()
    {
        leftselected.clear();
        for(String strRight : rightselected)
        {  
          rightvalues.remove(strRight);
          leftvalues.add(strRight);
        }
        //system.debug('rightvalues=====inunselect===='+rightvalues.size());
        if(rightvalues.size()==0)
        {
            rensearch=true;
        }
        else
        {
            rensearch=false;
        }
    }
    
   
    // Method Name : getunSelectedValues
    // Description : preparing the selectOptions for Unselcted values  
    public List<SelectOption> getunSelectedValues()
    {
      List<SelectOption> Documentoptions = new List<SelectOption>();
      List<string> documentUnselected=new List<String>();
      documentUnselected.addAll(leftvalues);
       for(string sltCategory : documentUnselected)
       {
            Documentoptions.add(new SelectOption(sltCategory,sltCategory));
            Documentoptions.sort();
       }    
        return Documentoptions;
    }
    
    
     //   Method Name : getunSelectedValues
     //   Description : preparing the selectOptions for selcted values    
    public List<SelectOption> getSelectedValues()
    {
        List<SelectOption> DocumentSelectoptions= new List<SelectOption>();
        List<string> documentSelected=new List<String>();
        documentSelected.addAll(rightvalues);
         for(string sltCategoryLst : documentSelected)
         {
             DocumentSelectoptions.add(new SelectOption(sltCategoryLst,sltCategoryLst));
             DocumentSelectoptions.sort();
         }
        return DocumentSelectoptions;
    }
    
    public pagereference CustomerDetails()
    {
        pagereference pageref;
        String custid = apexpages.currentPage().getParameters().get('customerid');
        strCustIdParam = custid;
        pageref=new pagereference('/apex/BSureC_ViewCustomerDetails?id='+strCustIdParam);
        pageref.setRedirect(true);
        return pageref;
    }
    
    //**********************below methods for pagination******************************************************////
//based upon the totallistsize size the calculations will be happen and the pagination will be working accordingly

    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }

    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <param name="PageSize"></param>
    /// <returns>Integer</returns>  
    /// </summary>    
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }

    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber=0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            totalPageNumber = totallistsize / pageSize;
            //system.debug('totalpagenumber***' + totalpagenumber);
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>    
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************88pavajflajf');
        try
        {
            lstDocumentWrapperpag=new List<DocumentWrapper>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize = 3;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
           
            if(lstDocumentWrapper!=NULL)
            {
                for(DocumentWrapper b : lstDocumentWrapper)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                        lstDocumentWrapperpag.add(b); 
                }
            }
            NlistSize=lstDocumentWrapperpag.size();
            pageNumber = newPageIndex;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }

    public void pageData(Integer newPageIndex)
    {
        if (newPageIndex > pageNumber)
        {                                          
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize;     
        }
            
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;           
        }
            //system.debug('NlistSize============='+NlistSize);
    }
    
    public void LastpageData(Integer newPageIndex)
    {      
        try
        {
            lstDocumentWrapperpag=new List<DocumentWrapper>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    

            if(lstDocumentWrapper!=NULL)
            {
                for(DocumentWrapper b : lstDocumentWrapper)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                        lstDocumentWrapperpag.add(b); 
                }
            }
            pageNumber = newPageIndex; 
            NlistSize=lstDocumentWrapperpag.size();
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        } 
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    

    
    // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
     // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    }
    
    public pagereference cancel()
      {
        pageReference pRef = new pageReference('/apex/BSureC_Reports');
        return pRef;
      } 
}