/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CreditIncreaseSection {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
         objbasicinfo.Customer_Group_Id__c = '2345';
         objbasicinfo.Customer_Name__c = 'SAM';
         objbasicinfo.Analyst__c = Userinfo.getUserId();
         objbasicinfo.Manager__c = Userinfo.getUserId();
         insert objbasicinfo;
        String str_cust_id = String.valueOf(objbasicinfo.Id);
        ApexPages.currentPage().getParameters().put('Id',str_cust_id);
        
         system.assertEquals('2345', objbasicinfo.Customer_Group_Id__c);
        
        BSureC_Credit_Increase__c objcreditincrease = new BSureC_Credit_Increase__c();
        objcreditincrease.Review_Name__c = '123sam';
        objcreditincrease.Review_Start_Date__c = date.parse('03/28/2013');
        objcreditincrease.Review_End_Date__c = date.parse('03/29/2013');
        objcreditincrease.Risk_Category__c = '002';
        objcreditincrease.Credit_Limit__c = 250000.00;
        objcreditincrease.Customer_Basic_Info_Id__c = String.valueOf(objbasicinfo.Id);
        insert objcreditincrease;  
          
        BSureC_Credit_Data_Publish__c obj_credit_data_publish = new BSureC_Credit_Data_Publish__c();
        obj_credit_data_publish.Credit_Limit__c = Decimal.valueOf( Double.valueOf('3500') ) ;
        obj_credit_data_publish.of_Limit__c = Decimal.valueOf( Double.valueOf('120') );
       // obj_credit_data_publishCustomer_Name__c
        //obj_credit_data_publish.Exposure__c = '';
        obj_credit_data_publish.Risk_Category__c = '002';
       // obj_credit_data_publish.Last_Review__c = 
        obj_credit_data_publish.BSureC_Customer_Basic_Info__c = str_cust_id;
        insert obj_credit_data_publish;
         
         //system.debug('---objbasicinfo.Id---'+objbasicinfo.Id);
         BSureC_Credit_Increase__c basicList = [SELECT Id FROM BSureC_Credit_Increase__c Where Id =: objcreditincrease.Id];
         ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
         BSureC_CreditIncreaseSection obj = new BSureC_CreditIncreaseSection(ssc);
        // obj.
       //  BSureC_CreditIncreaseSection obj= new BSureC_CreditIncreaseSection();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.str_new_credit_limit = '250';
         obj.str_risk_category = '001';
         obj.str_Comments = 'sam';
         
       //  obj.rec_credit_increase = new BSureC_Credit_Increase__c();
     //    obj.rec_credit_increase.Review_Name__c = 'Sam2';
      //   obj.rec_credit_increase.Credit_Limit__c = 1000001.00;
      //   obj.rec_credit_increase.Risk_Category__c = '002';
     //    obj.rec_credit_increase.Next_Review_Date__c = system.today();
     //    obj.saveinfo(); 
         obj.str_customer_info_id = str_cust_id;
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.SaveDates();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.SaveDates();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.SaveDates();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/28/2013');
         obj.SaveDates();
         
          //condition 1 for save dates
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.SaveDates();
         
         obj.rec_credit_increase.Review_Start_Date__c = date.today().addDays(2);
         
         obj.SaveDates();
         
         //condition 2 for savedates
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = '';
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.SaveDates();
         
         obj.rec_credit_increase.Review_Start_Date__c = date.today().addDays(2);
         
         obj.SaveDates();
         
         //condition 3 for save dates
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = '';
         //obj.rec_credit_increase.Customer_Basic_Info_Id__c = '';
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.SaveDates();
         
         obj.rec_credit_increase.Review_Start_Date__c = date.today().addDays(2);
         
         obj.SaveDates();
         
         
    }
    static testMethod void myUnitTest2()
    {
    	Profile objProf = [select id, name from Profile where name ='BSureC_Analyst'];
        UserRole objRole = [select id, name from UserRole where name ='Analyst'];
        User objUser = new User(Alias = 'PKnox', 
                                Email='satish@vertexcs.com', 
                                EmailEncodingKey='UTF-8', 
                                LastName='Analyst_CustUser', 
                                LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', 
                                ProfileId = objProf.Id, 
                                UserRoleId = objRole.Id, 
                                TimeZoneSidKey='America/Los_Angeles', 
                                UserName='cust_analyst@bsurenestle.com');
        
        insert objUser;
        
    	System.runAs(objUser){
        //kishore
        /*Map<string,ID> roleIds = new Map<string,ID>();
        Map<String,ID> profiles = new Map<String,ID>();
        //kishore
        //Kishore
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureC_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Zone Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureC_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com');
        insert standardZM;
        
        
        BSureC_Zone_Manager__c cmObj = new BSureC_Zone_Manager__c();
        system.runAs(standardZM)
        {
        	BSureC_Zone__c ZoneRef=new BSureC_Zone__c(name='Americas34',IsActive__c=true);
        	insert ZoneRef;
	        cmObj.Zone__c = ZoneRef.Id;
	        cmObj.Zone_Manager__c = standardZM.Id;
	        insert cmObj;
        } */    
	    // TO DO: implement unit test
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
         objbasicinfo.Customer_Group_Id__c = '2345';
         objbasicinfo.Customer_Name__c = 'SAM';
         objbasicinfo.Analyst__c = Userinfo.getUserId();
         objbasicinfo.Manager__c = Userinfo.getUserId();
         insert objbasicinfo;
        
        
        String str_cust_id = String.valueOf(objbasicinfo.Id);
        ApexPages.currentPage().getParameters().put('Id',str_cust_id);
        system.assertEquals('2345', objbasicinfo.Customer_Group_Id__c);
        
        BSureC_Credit_Increase__c objcreditincrease = new BSureC_Credit_Increase__c();
        objcreditincrease.Review_Name__c = '123sam';
        objcreditincrease.Review_Start_Date__c = date.parse('03/28/2013');
        objcreditincrease.Review_End_Date__c = date.parse('03/29/2013');
        objcreditincrease.Risk_Category__c = '002';
        objcreditincrease.New_Risk_Category__c = '002';
        objcreditincrease.New_Credit_Limit__c = 250000.00;
        objcreditincrease.Next_Review_Date__c = date.parse('03/29/2014');
        objcreditincrease.Credit_Limit__c = 250000.00;
        objcreditincrease.Customer_Basic_Info_Id__c = String.valueOf(objbasicinfo.Id);
        insert objcreditincrease;  
        
        
          
        BSureC_Credit_Data_Publish__c obj_credit_data_publish = new BSureC_Credit_Data_Publish__c();
        obj_credit_data_publish.Credit_Limit__c = Decimal.valueOf( Double.valueOf('3500') ) ;
        obj_credit_data_publish.of_Limit__c = Decimal.valueOf( Double.valueOf('120') );
       // obj_credit_data_publishCustomer_Name__c
        //obj_credit_data_publish.Exposure__c = '';
        obj_credit_data_publish.Risk_Category__c = '002';
       // obj_credit_data_publish.Last_Review__c = 
        obj_credit_data_publish.BSureC_Customer_Basic_Info__c = str_cust_id;
        insert obj_credit_data_publish;
         
         //system.debug('---objbasicinfo.Id---'+objbasicinfo.Id);
         BSureC_Credit_Increase__c basicList = [SELECT Id FROM BSureC_Credit_Increase__c Where Id =: objcreditincrease.Id];
         ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
         BSureC_CreditIncreaseSection obj = new BSureC_CreditIncreaseSection(ssc);
         
         obj.getrisk_category_Options();
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.readnextreviewdate();
         
         //Pagination
         obj.pageNumber = 2;
         obj.pageSize = 2;
         obj.totallistsize = 5;
         
         obj.pageData(1);
         obj.LastpageData(1);
         obj.nextBtnClick();
         obj.previousBtnClick();
         obj.LastbtnClick();
         obj.FirstbtnClick();
         obj.getTotalPageNumber();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.Reapproval__c = false;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.approve12();
         obj.str_credit_increase_id = String.valueOf(objcreditincrease.Id);
         
         obj.reject();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.Reapproval__c = false;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 1000001.00;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         //obj.approve12();
        
         //BSureC_paymenthistoryviewbean obj = new BSureC_paymenthistoryviewbean();
        }
        
    } 
    static testMethod void myUnitTest3()
    {
         // TO DO: implement unit test
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
         objbasicinfo.Customer_Group_Id__c = '2345';
         objbasicinfo.Customer_Name__c = 'SAM';
         objbasicinfo.Analyst__c = Userinfo.getUserId();
         objbasicinfo.Manager__c = Userinfo.getUserId();
         insert objbasicinfo;
        String str_cust_id = String.valueOf(objbasicinfo.Id);
        ApexPages.currentPage().getParameters().put('Id',str_cust_id);
        system.assertEquals('2345', objbasicinfo.Customer_Group_Id__c);
        
        BSureC_Credit_Increase__c objcreditincrease = new BSureC_Credit_Increase__c();
        objcreditincrease.Review_Name__c = '123sam';
        objcreditincrease.Review_Start_Date__c = date.parse('03/28/2013');
        objcreditincrease.Review_End_Date__c = date.parse('03/29/2013');
        objcreditincrease.Risk_Category__c = '002';
        objcreditincrease.Credit_Limit__c = 250000.00;
        objcreditincrease.Customer_Basic_Info_Id__c = String.valueOf(objbasicinfo.Id);
        insert objcreditincrease;  
          
        BSureC_Credit_Data_Publish__c obj_credit_data_publish = new BSureC_Credit_Data_Publish__c();
        obj_credit_data_publish.Credit_Limit__c = Decimal.valueOf( Double.valueOf('3500') ) ;
        obj_credit_data_publish.of_Limit__c = Decimal.valueOf( Double.valueOf('120') );
       // obj_credit_data_publishCustomer_Name__c
        //obj_credit_data_publish.Exposure__c = '';
        obj_credit_data_publish.Risk_Category__c = '002';
       // obj_credit_data_publish.Last_Review__c = 
        obj_credit_data_publish.BSureC_Customer_Basic_Info__c = str_cust_id;
        insert obj_credit_data_publish;
         
         //system.debug('---objbasicinfo.Id---'+objbasicinfo.Id);
         BSureC_Credit_Increase__c basicList = [SELECT Id FROM BSureC_Credit_Increase__c Where Id =: objcreditincrease.Id];
         ApexPages.Standardcontroller ssc = new ApexPages.Standardcontroller(basicList);
         BSureC_CreditIncreaseSection obj = new BSureC_CreditIncreaseSection(ssc);
        // obj.
       //  BSureC_CreditIncreaseSection obj= new BSureC_CreditIncreaseSection();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.str_new_credit_limit = '250';
         obj.str_risk_category = '001';
         obj.str_Comments = 'sam';
         
          //Condition for validations in saveinfo
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('-1'));
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
         obj.getAnalystApproval();
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
         obj.getAnalystApproval(); 
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
         obj.getAnalystApproval(); 
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_Start_Date__c =  system.today();
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
          
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c =date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
         
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.Next_Review_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.saveinfo();
         
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Review_Start_Date__c = system.today();
         obj.rec_credit_increase.Review_End_Date__c = date.today().addMonths(Integer.valueOf('1'));
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.saveinfo();
          
         
         //Condition 1 for save info
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Credit_Limit__c = 1000.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
          
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = String.valueOf(obj_credit_data_publish.Id);
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
          
         
         
         //Condition 2 for save info
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = '';
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Credit_Limit__c = 1000.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
          
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.str_customer_id = '';
         obj.rec_credit_increase.Customer_Basic_Info_Id__c = str_cust_id;
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
         
         
         
         //condition 3 for save info
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam';
         obj.rec_credit_increase.Credit_Limit__c = 1000.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 2500;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
          
         
         obj.rec_credit_increase = new BSureC_Credit_Increase__c();
         obj.rec_credit_increase.Review_Name__c = 'Sam1';
         obj.rec_credit_increase.Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.Risk_Category__c = '002';
         obj.rec_credit_increase.Review_Start_Date__c = date.parse('03/28/2013');
         obj.rec_credit_increase.Review_End_Date__c = date.parse('03/29/2013');
         obj.rec_credit_increase.New_Credit_Limit__c = 250001.00;
         obj.rec_credit_increase.New_Risk_Category__c = '003';
         obj.rec_credit_increase.Next_Review_Date__c = date.parse('03/28/2014');
         obj.rec_credit_increase.Status__c = 'Pending Approval';
         obj.rec_credit_increase.Next_Review_Date__c = system.today();
         obj.saveinfo();
          
    }
}