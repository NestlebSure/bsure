/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CreditDataSection {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
         
       
       BSureC_CreditDataSection obj = new BSureC_CreditDataSection();
      /* obj.bfilecontent = Blob.valueOf('Group Id,High Credit,Open Balance,Past Due,Percent Current,Division Name,Informatio Date,'+
							+'3885,1349,828,828,828,NPPC,11/19/2012,'+
							+'20317,4606,4491,491,491,NPPC,11/19/2012,'+
							+'8274,10191,10191,8809,8809,NPPC,11/19/2012,'+
							+'6900,536,368,184,184,NPPC,11/19/2012');
		*/	
		BSureC_Credit_Data_Stage__c obj1 = new BSureC_Credit_Data_Stage__c();
		obj1.CE__c = '1234';
		insert obj1;
		
		
		
	   obj.bfilecontent = Blob.valueOf('CE,Group ,Name,City,State,Reg,Last Sale,Last Int Rev, Disc, Prompt,Slow,ADS,Risk Cat,High Credit,Credit Limit,\n'+
						  +'69479,,Chokhani Enterprises,Kathmandu,,89,6-Jan-09,,0,0,0,0,5,0,85.78\n'+
						  +'\'175757,3885,Givaudan Suisse SA,Vernier,GE,89,25-Sep-12,25-Sep-12,0,32,68,38,4,129957.51,350000\n'+
						  +',20317,Morris National Inc,La Salle,QC,89,27-Sep-12,11/19/2012,t,6,1,3,3,1293768.92,11\n'+
						  +'281643,8274,Scheidegger Trading Co Inc,San Francisco,CA,89,8-Jan-09,,,0,0,0,5,0,890\n');	
		obj.filename = 'sam.csv';				  				
	   obj.SaveDetailsFile();		
	   
	   	obj.bfilecontent = null;
	   	obj.SaveDetailsFile();
	   	
   // }
    //   static testMethod void myUnitTest2()
    //   {
       		BSureC_Divisions__c objdiv = new BSureC_Divisions__c();
       		objdiv.Division_Name__c = 'Nest';
       		objdiv.Division_Short_Name__c = 'NE';
       		insert objdiv;
       		system.assertEquals('Nest', objdiv.Division_Name__c);
		   	BsureC_PaymentHistorySection objph = new BsureC_PaymentHistorySection();
		   	objph.bfilecontent = Blob.valueOf('Group Id,High Credit,Open Balance,Past Due,Percent Current,Division Name,Informatio Date,\n'+
								+'\'3885,1349,828,,828,NPPC,11/19/2012\n'+
								+',Null,1,1,00.00,, \n'+
								+'8274,123,asd,123,8809,NPPC,null\n'+
								+'6900,Null,,184,Null,NPPC,11/19/2012\n');
			objph.str_division_name = String.valueOf(objdiv.Id);					
			objph.filename = 'sam12.csv';					
		    objph.SaveDetailsFile();
		   	objph.bfilecontent = null;
		   	objph.SaveDetailsFile();
       }
}