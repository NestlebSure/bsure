//Controller used for the AutoComplete Enhancement
global with sharing class BSureC_AutoComplete {

    @RemoteAction
    global static SObject[] findSObjects(string obj, string qry, string addFields, string profilename) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        List<String> fieldList=new List<String>(); 
        if (addFields != '')  
        fieldList = addFields.split(',');
        
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();       
        Schema.SObjectType sot = gd.get(obj.toLowerCase());
 
        if (sot == null) 
        {
            return null;
        }
        system.debug('qry---'+qry+'--addFields='+addFields);
        /* Creating the filter text */
        String filter = ' like \'' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Id';
        
        /* If any additional field was passed, adding it to the soql */
        if (fieldList.size()>0) 
        {
            for (String s : fieldList) 
            {
                soql += ', ' + s;
            }
        }
        soql += ' from ' + obj + ' where '+ addFields + filter;
        if(profilename != null && profilename != '')
        {
            soql += ' and Profile.Name =\'' + profilename + '\'';
        }
        soql += ' order by '+addFields +' limit 20';
        
        //system.debug('Qry: '+soql);
        
        List<sObject> L = new List<sObject>();
        try 
        {
            if(sot.getDescribe().isAccessible()) {
                L = Database.query(soql);
            }
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        return L;
   }
   
   @RemoteAction 
    global static SObject[] findSObjectscoll(string obj, string qry, string addFields, string profilename, string userrolename) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        List<String> fieldList=new List<String>(); 
        if (addFields != '')  
        fieldList = addFields.split(',');
        
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();       
        Schema.SObjectType sot = gd.get(obj.toLowerCase());
 
        if (sot == null) 
        {
            return null;
        }
        system.debug('qry---'+qry+'--addFields='+addFields);
        /* Creating the filter text */
        String filter = ' like \'' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Id';
        
        /* If any additional field was passed, adding it to the soql */
        if (fieldList.size()>0) 
        {
            for (String s : fieldList) 
            {
                soql += ', ' + s;
            }
        }
        soql += ' from ' + obj + ' where '+ addFields + filter;
        if(profilename != null && profilename != '')
        {
            soql += ' and Profile.Name =\'' + profilename + '\'';
        }
        if(userrolename != null && userrolename != '')
        {
            soql += ' and UserRole.Name =\'' + userrolename + '\'';
        }
        soql += ' order by '+addFields +' limit 20';
        
        //system.debug('Qry: '+soql);
        
        List<sObject> L = new List<sObject>();
        try 
        {
            if(sot.getDescribe().isAccessible()) {
                L = Database.query(soql);
            }
        }
        catch (QueryException e) 
        {
            //system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        return L;
   }
}