public with sharing class BSureC_ImportCustomerUploadProcess {

	public String filename{get;set;}//For Uploaded file from page

    public Transient blob bfilecontent{get;set;} // Holds the Uploaded file content from page
    public static String strfiledata{get;set;}// Holds the CSV file Data ::Added Kishore
    public static String[] filelines = new String[]{};// Holds the each row of the CSV file  ::Added Kishore
	 public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
     public pageReference doCancel()
    {
      pageReference p = new pageReference('/apex/BSureC_AdminTools');
      return p;
    }
     public BSureC_ImportCustomerUploadProcess(ApexPages.StandardController controller)
    {
      
    }
     public pagereference SaveDetailsFile()  
     {
           try{
                if(bfilecontent!=NULL && filename.contains('.csv'))
                {
                	system.debug('------savedetailsfile button-----');
                	strfiledata = bfilecontent.toString(); //kishore
                	filelines = strfiledata.split('\n');
                	list<String> lstduplicatechk = new list<string>();
                	set<string> setduplicatechk = new set<string>();
                	for(Integer i=0; i < filelines.size(); i++)
                	{
                		String[] strinputvalues = new String[]{};
                		strinputvalues = filelines[i].split(',');
                		if(strinputvalues[0]!=NULL && strinputvalues[0]!=' ' && strinputvalues[0]!='')
                		{
                			lstduplicatechk.add(strinputvalues[0]);
                			setduplicatechk.add(strinputvalues[0]);
                		}
                	}
                	Integer custCount=[select count() from BSureC_Customer_Basic_Info__c where Credit_Account__c IN : setduplicatechk];
                	system.debug(custCount+'setduplicatechk####'+setduplicatechk);
                	if(lstduplicatechk.size() != setduplicatechk.size())
                	{
                		showErrorMessage('File is having duplicate Credit Accounts. ');
                		return null;
                	}
                	else if (custCount >0){
                		showErrorMessage('File is having Existing Credit Accounts. ');
                		return null;
                	}
                	else
                	{
	                	boolean rtnExport = false;
	                    pagereference pageref=new pagereference('/apex/BsureC_CustomerinfouploadStagelistview');
	                
	                    //Modified to limit the query by VINU PRATHAP
				        //Delete[SELECT Id from BSureC_Customer_Basic_Info_Stage__c where id!= null AND createddate!=TODAY];
				        if(BSureC_Customer_Basic_Info_Stage__c.sObjectType.getDescribe().isDeletable() )
				        {
				        	Delete[SELECT Id from BSureC_Customer_Basic_Info_Stage__c where id!= null];//Anu:29thApr13:modified as per tom/nitin confirmation
				        }
				       // try{
				        	system.debug('------try for batch-----');
				        	BSureC_Customer_Basic_Info_Stage__c[] sourcetemp = new BSureC_Customer_Basic_Info_Stage__c[]{};
            				BSureC_Customer_Basic_Info_Stage__c objs = new BSureC_Customer_Basic_Info_Stage__c();
            				sourcetemp.add(objs);
            				string csvstring = bfilecontent.tostring();
            				BSureC_CustomerBatchProcessor  objBatchProcess= new BSureC_CustomerBatchProcessor(sourcetemp,csvstring);
            				String strBatchId = Database.executeBatch(objBatchProcess, 200);
           					System.Debug('##### Batch ID is ' + strBatchId );
           					return pageref;
				       /* }
				        catch(Exception e)
				        {
				        	system.debug('-------Exception--------'+e);
	                    	return null;
				        }*/
				        	
	                   /* rtnExport = BSureC_CustomerinfouploadScheduler.createBatchesFromCSV(bfilecontent,'BSureC_Customer_Basic_Info_Stage__c',
	                    strCategoryId,strRegionId,strZoneId,strSubZoneId,strCountryId );
	                    if(rtnExport)
	                    {
	                    	
	                    }
	                    else
	                    {
	                    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The uploaded file contians more than 50 records. Please upload 50 records at a time.'));
	                    	return null;
	                    }*/
                	}   
                }
                else
                {
                    showErrorMessage(system.Label.BSureC_Please_upload_a_csv_file);
                    return null;
                }
           		//return null;
            }
            
     		catch(Exception e){
     			system.debug('-------Exception--------'+e);
            	
            }
        return null;
                }
    
}