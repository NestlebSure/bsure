/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_MirrorCreditReview {

    static testMethod void myUnitTest() {
        
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        
        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = UserInfo.getUserId();
        supplierObj.Manager__c = UserInfo.getUserId();
        supplierObj.Backup_Analysts__c = UserInfo.getUserId() + ',' + UserInfo.getUserId();
        supplierObj.Next_Review_Date__c = date.today();
        supplierObj.Planned_Review_Date__c = date.today();
        insert supplierObj;
        
        BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
        b.Contact_name__c = 'Steve jobs';
        b.Supplier_Name__c = 'test';
        b.Supplier_Category__c = scategory.id;
        b.Parent_Supplier_ID__c = supplierObj.id; 
        b.Review_Status__c='completed';
        insert b;
        
        BSureS_Basic_Info__c c=new BSureS_Basic_Info__c();
        c.Contact_name__c = 'Steve1 jobs';
        c.Supplier_Name__c = 'test1';
        c.Supplier_Category__c = scategory.id;
        c.Parent_Supplier_ID__c = supplierObj.id; 
        c.Review_Status__c='completed';
        insert c;
        
        list<string> setChild= new list<string>();
        setChild.add(b.id);
        setChild.add(c.id);
        
        BSureS_Credit_Analysis__c pca = new BSureS_Credit_Analysis__c();
        pca.Review_Name__c = 'Test Review123';
        pca.Comment__c = 'text Comments';
        pca.Risk_Level__c =  'High';
        pca.Rating__c = 'A';
        pca.Review_Status__c = 'Started';
        pca.Spend__c = double.valueOf('4543525');
        pca.Actual_Review_Start_Date__c = date.today();
        pca.Expected_Review_End_Date__c = date.today();
        pca.Supplier_ID__c = supplierObj.id;
        //pca.Review_Period__c = date.today();
        //pca.Review_Start_Date__c = date.today();
        pca.Spend_Start_Date__c = date.today();
        pca.Spend_End_Date__c = date.today();
        pca.Next_Review_Date__c = date.today();
        insert pca;
        
        Apexpages.currentPage().getParameters().put('id',pca.id);
        system.test.starttest();
        ApexPages.StandardController controller = new ApexPages.StandardController(pca); 
        BSureS_MirrorCreditReview pObj= new BSureS_MirrorCreditReview(controller);
        //pObj.strSupplierId  =  supplierObj.id;
        //pObj.strCurrentSupplierId = supplierObj.id;
        pObj.setChildCustID = setChild;
        pObj.creditanalysisId = pca.Id;
        pObj.visibilityNextRDate();
        pObj.submitApproval();
        pObj.EditObjectInfo();
        pObj.saveandsubmitApproval();
    }
}