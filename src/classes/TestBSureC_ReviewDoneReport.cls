/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_ReviewDoneReport {

    static testMethod void myUnitTest() 
    {
           // TO DO: implement unit test 
            BSureC_Customer_Basic_Info__c objcust = new BSureC_Customer_Basic_Info__c(Contact_name__c='KingFisher',Collector_Name__c='Cust Collec');    	
	   		insert objcust;
	   		BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
	        insert Bzone;
	        
	        Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
	        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
	        BSzone.Name = 'Test customer Sub Zone';
	        BSzone.ZoneID__c = Bzone.Id;
	        insert BSzone;
	        
	        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
	        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
	        cSettings.Name = 'BSureC_Report_Page_Size_CC';
	        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size_CC';
	        cSettings.Parameter_Value__c = '10';
	        lstSettings.add(cSettings);
	        insert lstSettings;
	        
	        system.assertEquals('Test customer Sub Zone',BSzone.Name);
        
             BSzone= [select id,Name from BSureC_Sub_Zone__c where id=:BSzone.Id];
   		       
           BSureC_ReviewDoneReport objreviewsdone=new BSureC_ReviewDoneReport();
	       Date dtYear = Date.Today().addDays(-365);           
	        Date dtToday = Date.Today();        
	        objreviewsdone.strFromDate = dtYear.format();
	        objreviewsdone.strToDate =  dtToday.format();
	       
	       objreviewsdone.getSortDirection();
		   objreviewsdone.setSortDirection('Asc');
		   objreviewsdone.Sort();
		   
		   objreviewsdone.showCollectorDetails();
		   objreviewsdone.searchCollector();
		   objreviewsdone.closepopupColl();
		   objreviewsdone.closepopupCollector();
		   
	       objreviewsdone.showBuyerDetails();
	       objreviewsdone.closepopup();
	       objreviewsdone.searchBtn();
	       objreviewsdone.closepopupBtn();
	       objreviewsdone.BindData(1);	       
	       objreviewsdone.PrevPageNumber=1;
		   objreviewsdone.NxtPageNumber=2;
	       objreviewsdone.nextBtnClick();	      
	       objreviewsdone.getPageNumber();
	       objreviewsdone.getPageSize();
	       objreviewsdone.getPreviousButtonEnabled();
	       objreviewsdone.getNextButtonDisabled();
	       objreviewsdone.getTotalPageNumber();
	       objreviewsdone.pageData(2);
	       objreviewsdone.LastpageData(2);
	       objreviewsdone.FirstbtnClick();
	       objreviewsdone.LastbtnClick();
	       objreviewsdone.ExportCSV();
	      
    }
}