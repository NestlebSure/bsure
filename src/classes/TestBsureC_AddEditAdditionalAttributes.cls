@istest
private class TestBsureC_AddEditAdditionalAttributes {

	static testMethod void myUnitTest() {
        apexpages.currentpage().getParameters().put('id',userInfo.getUserId());
        BSureC_AddEditAdditionalAttributes objBsureAA=new BSureC_AddEditAdditionalAttributes();
        
        BSureC_Zone__c Bzone = new BSureC_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;      
        system.Assertequals('Test Supplier Zone',Bzone.Name);    
        Bzone = [select id,Name from BSureC_Zone__c where id=:Bzone.Id];
        
        BSureC_Sub_Zone__c BSzone = new BSureC_Sub_Zone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;

        BSureC_Category__c scategory = new BSureC_Category__c();
        scategory.Name = 'Nestle Account';
        insert scategory;
        
                
        BSzone= [select id,Name from BSureC_Sub_Zone__c where id=:BSzone.Id];    
	    BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
	    insert BsCountry;
	    BsCountry = [select Id,Name from BSureC_Country__c  where id=:BsCountry.Id];
	    
	    BSureC_Region__c BsRegion=new BSureC_Region__c(Name='Souths');
	    insert BsRegion;
	    
        BSureC_AdditionalAttributes__c bSureUserAttributes1 =new BSureC_AdditionalAttributes__c();
        bSureUserAttributes1.BSureC_Zone__c=Bzone.Id;
        bSureUserAttributes1.BSureC_Sub_Zone__c= BSzone.Id;
        bSureUserAttributes1.BSureC_Country__c = BsCountry.Id;
        bSureUserAttributes1.User__c = UserInfo.getUserId();        
        bSureUserAttributes1.BSureC_Region__c=BsRegion.Id;
        bSureUserAttributes1.Region__c=BsRegion.Id;
        insert bSureUserAttributes1;	    
    

        BSureC_UserCategoryMapping__c objUCategory = new BSureC_UserCategoryMapping__c();        
        objUCategory.CategoryID__c=scategory.Id;
        insert objUCategory;

        list<User> userid = new list<User>();
        integer inte = 45;
        objBsureAA.Cancel();
        objBsureAA.FirstbtnClick();
        objBsureAA.getlstProfiles();
        objBsureAA.getUsersLst();
        objBsureAA.LastbtnClick();
        objBsureAA.nextBtnClick();
        objBsureAA.pagenation();
        objBsureAA.previousBtnClick();
        objBsureAA.userViewPage();

        
        		
	}


}