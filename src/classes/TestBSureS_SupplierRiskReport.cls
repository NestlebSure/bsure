/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class TestBSureS_SupplierRiskReport {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
   		insert objSupplier;
   		
   		BSureS_Zone__c Bzone = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureS_Zone__c where id=:Bzone.Id];
        BSureS_SubZone__c BSzone = new BSureS_SubZone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        system.assertEquals('Test Supplier Sub Zone',BSzone.Name);
        
        BSzone= [select id,Name from BSureS_SubZone__c where id=:BSzone.Id];
		
		BSureS_Country__c BsCountry = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
		insert BsCountry;
		system.assertEquals('Test Supplier Country',BsCountry.Name);
		
		BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_Report_Page_Size1';
        objCSettingsB.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsB.Parameter_Value__c = '10';
        insert objCSettingsB;
        
		BsCountry = [select Id,Name from BSureS_Country__c  where id=:BsCountry.Id];
		
		system.Test.startTest();
        BSureS_SupplierRiskReport objeRisk=new BSureS_SupplierRiskReport();
         Date dtYear = Date.Today().addDays(-365);           
        Date dtToday = Date.Today();        
        objeRisk.strFromDate = dtYear.format();
        objeRisk.strToDate =  dtToday.format();
        BSureS_SupplierRiskReport.resultwrapper obj= new BSureS_SupplierRiskReport.resultwrapper();
     	 BSureS_Basic_Info__c objSup=new BSureS_Basic_Info__c();
        objSup.Supplier_Name__c='Dummy Supplier';
        //objSup.Fiscal_Year_End__c=system.today();
        //objSup.Next_Review_Date__c=system.today();
        //objNSF.fromdate=objSup.Fiscal_Year_End__c;
        //objNSF.todate=objSup.Next_Review_Date__c;
        Database.SaveResult MySaveResult =Database.insert(objSup);
        id idsup=MySaveResult.id;
        system.assertEquals('Dummy Supplier',objSup.Supplier_Name__c);
        
       // objeRisk.fromdate= system.Today();
        //objeRisk.todate= system.Today();
        objeRisk.getSZone();
        objeRisk.getSSubZoneList();
        objeRisk.getSCountriesList();
        objeRisk.getResults();
        set<id> setids=new set<id>();
        setids.add(idsup);
        objeRisk.getRiskCount(setids);
         objeRisk.getSupDetails();
         objeRisk.showBuyerDetails();
        objeRisk.closepopup();
        objeRisk.searchBtn();
        objeRisk.closepopupBtn();
        objeRisk.pageSize=10;
      	pagereference pgrefnxtbtn = objeRisk.nextBtnClick();
        pagereference pgrefprvbtn = objeRisk.previousBtnClick();
        integer i=objeRisk.getTotalPageNumber();
        integer j=1;
        objeRisk.BindData(j);
        objeRisk.pageData(j);
        objeRisk.LastpageData(j);
        integer k=objeRisk.getPageNumber();
        integer l=objeRisk.getPageSize();
        boolean a=objeRisk.getPreviousButtonEnabled();
        boolean b=objeRisk.getNextButtonDisabled();
        pagereference pgreflastbtn =objeRisk.LastbtnClick();
        pagereference pgreffrstbtn =objeRisk.FirstbtnClick();
        objeRisk.getSortDirection();
        objeRisk.Cancel();
        objeRisk.showErrorMessage('msg');
        pagereference pgrefexp =objeRisk.ExportToExcel();
        system.Test.stopTest();
    }
}