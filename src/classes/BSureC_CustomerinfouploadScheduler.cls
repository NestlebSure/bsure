/************************************************************************************************       
Controller Name         : BSureC_CustomerinfouploadScheduler       
Date                    : 03/16/2013        
Author                  : Santhosh Palla       
Purpose                 : This Controller will handle the bulk Customer Upload       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/26/2012      Santhosh.Palla            Initial Version
                          06/26/2013        Aditya V        Added Collectors and Street Name
**************************************************************************************************/
global with sharing class BSureC_CustomerinfouploadScheduler implements Database.Stateful,Schedulable
{
    global static String strfiledata{get;set;}// Holds the CSV file Data
    global static String[] filelines = new String[]{};// Holds the each row of the CSV file
    global static List<BSureC_Customer_Basic_Info_Stage__c> lst_customer_info_stage = new List<BSureC_Customer_Basic_Info_Stage__c>();
    global static Id parentrecid{get;set;} 
    global static Map<String,Id> map_customer_info{get;set;}
   // BSureC_Customer_Basic_Info_Stage__c
    //public List<String> lstcsvrec = new  List<String>(); //Holds the each row of the CSV file
    //global CSVParser m_parser;
    
    global void execute(SchedulableContext sc) 
    {
        //createBatchesFromCSVFile(parentrecid);
    }
    
    global static void createBatchesFromCSVFile(blob bflcontent,String strObjectName,
         String strCategoryId,String strRegionId,String strZoneId,String strSubZoneId,String strCountryId)
    {
    
    }
    // @future
    global static boolean createBatchesFromCSV(blob bflcontent,String strObjectName,
         String strCategoryId,String strRegionId,String strZoneId,String strSubZoneId,String strCountryId)
    {
    	
    	String [] CustomerinfoUpdateFields = new String [] {'Status__c',
														'Status_Resource_Value__c',
														'Contact_Name__c',
														'Exception__c',
														'Credit_Account__c',
														'Customer_Group_Id__c',
														'Bill_to_Account__c',
														'Owner_Ship__c',
														'Sole_Sourced__c',
														'Planned_Review_Date__c',
														'Notification_Flag__c',
														'DND_Financial_Information__c',
														'City__c',
														'Postal_Code__c',
														'Street_Name_Address_Line_1__c',
														'Customer_Category__c',
														'Customer_Region__c',
														'Zone__c',
														'Sub_Zone__c',
														'Country__c'
	                                                     };
	                                                     
	    
	    Map<String,Schema.SObjectField> m = Schema.SObjectType.BSureC_Customer_Basic_Info_Stage__c.fields.getMap();
	    for (String fieldToCheck : CustomerinfoUpdateFields) {
			// Check if the user has create access on the each field
			if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
			  //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,fieldToCheck+'Insufficient access'));
			  return null;
			}
	    }		    
	    
	    
	                                                         	
    	map_customer_info = new Map<String,Id>();
    	List<BSureC_Customer_Basic_Info__c> lst_customer_info = new List<BSureC_Customer_Basic_Info__c>([SELECT Id,Customer_Group_Id__c,Credit_Account__c 
                                                                 FROM BSureC_Customer_Basic_Info__c
                                                                 WHERE id!=null AND Credit_Account__c != null ]);
        if(lst_customer_info != null && lst_customer_info.size() > 0)
        {
        	for(BSureC_Customer_Basic_Info__c reccustinfo : lst_customer_info)
	        {
	            map_customer_info.put(reccustinfo.Credit_Account__c,reccustinfo.Id);
	        }
        }   
                                                             
        strfiledata = bflcontent.toString();
        //system.debug('strfiledata@'+strfiledata);
        filelines = strfiledata.split('\n');
        //system.debug('filelines@'+filelines);
        if(strObjectName == 'BSureC_Customer_Basic_Info_Stage__c')
        {
            lst_customer_info_stage = new List<BSureC_Customer_Basic_Info_Stage__c>();
            set<string> ceNumber=new set<string>();
            if(filelines.size()<=51)
            {
                for(Integer i=1;i<filelines.size();i++)
                {
                    String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                    //system.debug('---inputvalues---'+inputvalues);
                    BSureC_Customer_Basic_Info_Stage__c rec_customer_info_stage = new BSureC_Customer_Basic_Info_Stage__c();
                    rec_customer_info_stage.Status__c='Initialized';
                    rec_customer_info_stage.Status_Resource_Value__c= '/resource/1242640894000/Yellow';
                   
                    if(inputvalues[0]!=NULL && inputvalues[0]!=' ' && inputvalues[0]!='')
                    {
                        system.debug('inputvalues[0] !=NULL========================'+inputvalues[0]);
                        system.debug('map_customer_info.get(inputvalues[0])========='+map_customer_info.get(inputvalues[0]));
                        if(map_customer_info.get(inputvalues[0]) != null )
                        {
                        	system.debug('test mappppppppppp');
                        	rec_customer_info_stage.Exception__c='Credit Account is already exists';
	                        rec_customer_info_stage.Status__c='Exception';
	                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                        }
                        if( inputvalues[0].contains('\'') )
                        {
                            String Str_corrected_value = inputvalues[0].substring(1, inputvalues[0].length());
                            rec_customer_info_stage.Credit_Account__c = Str_corrected_value;
                        }
                        else
                        {
                            rec_customer_info_stage.Credit_Account__c = inputvalues[0];
                        }
                           
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Credit Account is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   system.debug('rec_customer_info_stage========'+rec_customer_info_stage);
                   if(inputvalues[1]!=NULL && inputvalues[1]!=' ' && inputvalues[1]!='')
                   {
                        //system.debug('inputvalues[1] !=NULL========================'+inputvalues[1]);
                        rec_customer_info_stage.Customer_Name__c = inputvalues[1];
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Customer Name is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[2]!=NULL && inputvalues[2]!=' ' && inputvalues[2]!='')
                   {
                        //system.debug('inputvalues[2] !=NULL========================'+inputvalues[2]);
                        if( inputvalues[0].contains('\'') )
                        {
                            String Str_corrected_value = inputvalues[2].substring(1, inputvalues[2].length());
                            rec_customer_info_stage.Customer_Group_Id__c = Str_corrected_value;
                        }
                        else
                        {
                            rec_customer_info_stage.Customer_Group_Id__c = inputvalues[2];
                        }
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Group Id is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[3]!=NULL && inputvalues[3]!=' ' && inputvalues[3]!='')
                   {
                        //system.debug('inputvalues[3] !=NULL========================'+inputvalues[3]);
                        if( inputvalues[0].contains('\'') )
                        {
                            String Str_corrected_value = inputvalues[3].substring(1, inputvalues[3].length());
                            rec_customer_info_stage.Bill_to_Account__c = Str_corrected_value;
                        }
                        else
                        {
                            rec_customer_info_stage.Bill_to_Account__c = inputvalues[3];
                        }
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Bill to Account is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[4]!=NULL && inputvalues[4]!=' ' && inputvalues[4]!='')
                   {
                          //system.debug('inputvalues[4] !=NULL========================'+inputvalues[4]);
                          rec_customer_info_stage.Owner_Ship__c = inputvalues[4];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[5]!=NULL && inputvalues[5]!=' ' && inputvalues[5]!='')
                   {
                          //system.debug('inputvalues[5] !=NULL========================'+inputvalues[5]);
                          rec_customer_info_stage.Sole_Sourced__c = inputvalues[5];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[6]!=NULL && inputvalues[6]!=' ' && inputvalues[6]!='')
                   {
                          //system.debug('inputvalues[6] !=NULL========================'+inputvalues[6]);
                          rec_customer_info_stage.Contact_Name__c = inputvalues[6];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[7]!=NULL && inputvalues[7]!=' ' && inputvalues[7]!='')
                   {
                        //system.debug('inputvalues[7] !=NULL========================'+inputvalues[7]);
                        try
                        {
                            rec_customer_info_stage.Planned_Review_Date__c = Date.parse(inputvalues[7]); //assigning values
                        }
                        catch(Exception e)
                        {
                            if(rec_customer_info_stage.Exception__c==NULL)
                            {
                                rec_customer_info_stage.Exception__c = String.valueof(e)+' in Date';
                                rec_customer_info_stage.Status__c='Exception';
                                rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }
                            else
                            {
                                rec_customer_info_stage.Exception__c += ','+String.valueof(e)+'in Date';
                                rec_customer_info_stage.Status__c='Exception';
                                rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                            }   
                        }
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Planned Review Date is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[8]!=NULL && inputvalues[8]!=' ' && inputvalues[8]!='')
                   {
                        //system.debug('inputvalues[8] !=NULL========================'+inputvalues[8]);
                        rec_customer_info_stage.Notification_Flag__c = Boolean.valueOf(inputvalues[8]);
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[9]!=NULL && inputvalues[9]!=' ' && inputvalues[9]!='')
                   {
                        //system.debug('inputvalues[9] !=NULL========================'+inputvalues[9]);
                        rec_customer_info_stage.DND_Financial_Information__c = inputvalues[9];
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='DND Financial Information is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[10]!=NULL && inputvalues[10]!=' ' && inputvalues[10]!='')
                   {
                        //system.debug('inputvalues[10] !=NULL========================'+inputvalues[10]);
                        //rec_customer_info_stage.Fiscal_Year_End__c = inputvalues[10];
                   }
                   else
                   {
                        
                   }
                   if(inputvalues[11]!=NULL && inputvalues[11]!=' ' && inputvalues[11]!='')
                   {
                        //system.debug('inputvalues[11] !=NULL========================'+inputvalues[11]);
                    //  rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[11];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[12]!=NULL && inputvalues[12]!=' ' && inputvalues[12]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[13]!=NULL && inputvalues[13]!=' ' && inputvalues[13]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[14]!=NULL && inputvalues[14]!=' ' && inputvalues[14]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[15]!=NULL && inputvalues[15]!=' ' && inputvalues[15]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[16]!=NULL && inputvalues[16]!=' ' && inputvalues[16]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[17]!=NULL && inputvalues[17]!=' ' && inputvalues[17]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[18]!=NULL && inputvalues[18]!=' ' && inputvalues[18]!='')
                   {
                        //system.debug('inputvalues[12] !=NULL========================'+inputvalues[12]);
                        //rec_customer_info_stage.Last_Financial_Statement_Received__c = inputvalues[12];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[19]!=NULL && inputvalues[19]!=' ' && inputvalues[19]!='')
                   {
                        //system.debug('inputvalues[19] !=NULL========================'+inputvalues[19]);
                        //rec_customer_info_stage.Email_address__c = inputvalues[19];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[20]!=NULL && inputvalues[20]!=' ' && inputvalues[20]!='')
                   {
                        //system.debug('inputvalues[20] !=NULL========================'+inputvalues[20]);
                        //rec_customer_info_stage.Phone_Number__c = inputvalues[20];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[21]!=NULL && inputvalues[21]!=' ' && inputvalues[21]!='')
                   {
                        //system.debug('inputvalues[21] !=NULL========================'+inputvalues[21]);
                        //rec_customer_info_stage.City__c = inputvalues[21];
                   }
                   else
                   {
                    
                   }
                   if(inputvalues[22]!=NULL && inputvalues[22]!=' ' && inputvalues[22]!='')
                   {
                        //system.debug('inputvalues[22] !=NULL========================'+inputvalues[22]);
                        rec_customer_info_stage.City__c = inputvalues[22];
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='City is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   if(inputvalues[23]!=NULL && inputvalues[23]!=' ' && inputvalues[23]!='')
                   {
                        rec_customer_info_stage.Postal_Code__c = inputvalues[23];
                   }
                   else
                   {
                    
                   }
                    if(inputvalues[24]!=NULL && inputvalues[24]!=' ' && inputvalues[24]!='')
                   {
                        rec_customer_info_stage.Street_Name_Address_Line_1__c = inputvalues[24];
                   }
                   else
                   {
                        rec_customer_info_stage.Exception__c='Street Name Address Line 1 is mandatory';
                        rec_customer_info_stage.Status__c='Exception';
                        rec_customer_info_stage.Status_Resource_Value__c='/resource/1242640894000/Red';
                   }
                   /* if(inputvalues[25]!=NULL && inputvalues[25]!=' ' && inputvalues[25]!='')
                   {
                    
                   }
                   else
                   {
                    
                   }*/
                   rec_customer_info_stage.Customer_Category__c = strCategoryId;
                  // rec_customer_info_stage.
              	   rec_customer_info_stage.Customer_Region__c = strRegionId; 
                   rec_customer_info_stage.Zone__c = strZoneId;
                   rec_customer_info_stage.Sub_Zone__c = strSubZoneId;
                   rec_customer_info_stage.Country__c = strCountryId;
                   system.debug('rec_customer_info_stage=========='+rec_customer_info_stage);
                     lst_customer_info_stage.add(rec_customer_info_stage);
                }
                database.insert(lst_customer_info_stage);
                return true;
           }     
           else
           {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The uploaded file contians more than 50 records. Please upload 50 records at a time.'));
                 return false;
           }
        }
        return true;
    }
}