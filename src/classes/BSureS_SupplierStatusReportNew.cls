/***********************************************************************************************
* Controller Name   : BSureS_SupplierStatusReportNew       
* Date              : 
* Author            : Kishorekumar A 
* Purpose           : Class for Supplier Status Report
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 05/15/2015                                    Initial Version
*												Kishorekumar		
**************************************************************************************************/	
global with sharing class BSureS_SupplierStatusReportNew {
	
	public  transient List<BSureS_Basic_Info__c> lstsuplInfo;
	public  list<SupplierWrapper> wraperlist{get;set;}
	public map<id,string> mapBuyers;
	public  List<BSureS_Assigned_Buyers__c> lstbuyers;
	public List<String> lstCategoryNames ;
	public List<BSureS_Category__c> lstCategory ;
	public string strSelectedCategory ;
	
	//Region
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions;  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions; //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions; // selectOptins for list of Country Records
    
    
	public class SupplierWrapper
    {
        public Id supplId{get;set;}
        public string supplName{get;set;}
        public string supplGroupId{get;set;}
        public string supplGlobeId{get;set;}
        public string suppReviewStatus{get;set;}
        public string suppAnalystName{get;set;}
        public string supplCategoryname{get;set;}
        public string supplCity{get;set;}
        public string supplState{get;set;}
        public string strBuyerCSV {get;set;}
        public String  strExpReviewEndDate{get;set;}
        public string lastReviewRating{get;set;}
        public String lastReviewComplDate{get;set;}
        
    }
    
	public BSureS_SupplierStatusReportNew(){
		getSupDetails();
	}
	
	public void  getSupDetails(){
		wraperlist = new List<SupplierWrapper>();
		lstCategoryNames = new list<string>();
        lstCategory = [select Name from BSureS_Category__c WHERE Name!= null order by Name];
        for(BSureS_Category__c cObj : lstCategory)
        { 
            ////System.debug('cObj.Name ========'+cObj.Name);
            
            if(cObj.Name != null && cObj.Name !='')
            {
                lstCategoryNames.add(cObj.Name);
            }
        }
        
		getBuyerslist();
		string strQuery = 'select id,Supplier_Name__c,Review_Status__c,Analyst_Name__c,Group_Id__c,Globe_ID__c,'+ 
                            'Supplier_Category__c,Supplier_Category_Name__c,City__c,State_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,'+
                            'Rating__c,Review_Complete_Date__c'+
                            ' from BSureS_Basic_Info__c where id != null limit 2000 ';
        lstsuplInfo = Database.query(strQuery);
        map<Id,BSureS_Credit_Analysis__c> creditStatusRecs = new map<Id,BSureS_Credit_Analysis__c>();
        if(!lstsuplInfo.isEmpty()){
        	creditStatusRecs = QueryCreditStatus();
        }
        for(BSureS_Basic_Info__c sObj: lstsuplInfo)
        {
            string strBuyers = mapBuyers.get(sObj.Id);
            if(strBuyers != null && strBuyers != '')
            {
                strBuyers = strBuyers.substring(0, strBuyers.length() - 2);
            }
            SupplierWrapper swObj = new SupplierWrapper();
            swObj.supplId = sObj.id;
            swObj.supplName = sObj.Supplier_Name__c;
            swObj.supplGroupId = sObj.Group_Id__c;
            swObj.supplGlobeId = sObj.Globe_ID__c;
            swObj.suppReviewStatus = sObj.Review_Status__c;
            swObj.suppAnalystName = sObj.Analyst_Name__c;
            swObj.supplCategoryname = sObj.Supplier_Category_Name__c; 
            swObj.supplCity = sObj.City__c;
            swObj.supplState = sObj.State_Name__c;
            swObj.strBuyerCSV = strBuyers != null && strBuyers != '' ? strBuyers : '';
            
            // @added by Surya 05/08/15
            if(!creditStatusRecs.isEmpty()){
            	String supplId=sObj.id;
            	supplId = supplId.length()<15 ? supplId.subString(0,15) : supplId;
            	if(creditStatusRecs.containsKey(supplId)){
            		if(creditStatusRecs.get(supplId).Expected_Review_End_Date__c != null){
            			DateTime d = creditStatusRecs.get(supplId).Expected_Review_End_Date__c ;
            			String strDate = d.month() +'/'+d.day()+'/'+d.year();
            			swObj.strExpReviewEndDate = creditStatusRecs.get(supplId).Review_Status__c!='Completed' ? strDate : null;
            		}
            	}
            }
            swObj.lastReviewRating = sObj.Rating__c;
            if(sObj.Review_Complete_Date__c != null){
            	DateTime d = sObj.Review_Complete_Date__c ;
            	String strDate = d.month() +'/'+d.day()+'/'+d.year();
            	swObj.lastReviewComplDate = strDate;
            }
            
            ////System.debug('swObj.strBuyerCSV========'+swObj.strBuyerCSV);
            wraperlist.add(swObj);
            // m_CsvColumns = 'supplName,supplCategoryname,supplGlobeId,suppReviewStatus,suppAnalystName,strBuyerCSV';
        }
	}
	public void getBuyerslist()
    {
        ////System.debug('wraperlist====****======'+wraperlist);
        mapBuyers = new map<id,string>(); 
        if(strSelectedCategory == 'All')
        {
            lstbuyers = new List<BSureS_Assigned_Buyers__c>([select Buyer_Name__c, Supplier_ID__c from BSureS_Assigned_Buyers__c where Supplier_ID__r.Supplier_Category_Name__c IN : lstCategoryNames ]);
        }
        else
        {
            lstbuyers = new List<BSureS_Assigned_Buyers__c>([select Buyer_Name__c, Supplier_ID__c from BSureS_Assigned_Buyers__c where Supplier_ID__r.Supplier_Category_Name__c =: strSelectedCategory ]);
        }
        for(BSureS_Assigned_Buyers__c objEachBuyer:lstbuyers)
        {
            if(mapBuyers.containsKey(objEachBuyer.Supplier_ID__c))
            {
                string strBuyer = mapBuyers.get(objEachBuyer.Supplier_ID__c);
                mapBuyers.remove(objEachBuyer.Supplier_ID__c);
                strBuyer += objEachBuyer.Buyer_Name__c + ', ';
                mapBuyers.put(objEachBuyer.Supplier_ID__c, strBuyer);
            }
            else
            {
                mapBuyers.put(objEachBuyer.Supplier_ID__c, objEachBuyer.Buyer_Name__c + ', ');
            }
        }

        ////System.debug('mapBuyers========'+mapBuyers); 
        //return lstbuyers;
    }
    public void zonelistchange()
    {
    	System.debug('af------call----------');
    }
    public list<BSureS_Zone__c> getzonelist(){
    	list<BSureS_Zone__c> lstZones = new list<BSureS_Zone__c>();
    	lstZones = [select Id,Name from BSureS_Zone__c where IsActive__c=true and Name != null order by Name];
    	return lstZones;
    }
    public list<BSureS_SubZone__c> getsubzonelist(){
    	System.debug('strZoneId==============='+strZoneId);
    	list<BSureS_SubZone__c> lstSubZones = new list<BSureS_SubZone__c>();
    	lstSubZones = [select Id,Name,ZoneID__c from BSureS_SubZone__c where ZoneID__c =:strZoneId and IsActive__c=true order by Name];
    	return lstSubZones;
    }    
    public void getZones()
    {  
      BSureS_CommonUtil.isReport = true;
      zoneOptions = new list<SelectOption>(); 
      zoneOptions = BSureS_CommonUtil.getZones();
    }
    public void getSubZones()
    {    
            
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
        //strCountryId = 'ALL';
        getCountries();     
        BSureS_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        
    }
   public void getCountries()
    {               
        if(strZoneId == 'ALL' || strSubZoneId == 'ALL' )
        strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();       
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
    }
   
	public map<Id,BSureS_Credit_Analysis__c> QueryCreditStatus()
    {
        map<Id,BSureS_Credit_Analysis__c> CreditStatuses = new map<Id,BSureS_Credit_Analysis__c>(); 
        if(!lstsuplInfo.isEmpty()){
	        for(BSureS_Credit_Analysis__c credSta : [Select id,name,Supplier_ID__c,Expected_Review_End_Date__c,Review_Status__c  from BSureS_Credit_Analysis__c where Supplier_ID__c IN :lstsuplInfo order by Createddate]){
	        	String supplierId = credSta.Supplier_ID__c;
	        	if(supplierId!=null){
	        		supplierId=supplierId.length()<15 ? supplierId.subString(0,15) : supplierId;
	        		//inserting into map
	        		CreditStatuses.put(supplierId,credSta);
	        	}
	        }
        }
        return CreditStatuses;
    }
    
}