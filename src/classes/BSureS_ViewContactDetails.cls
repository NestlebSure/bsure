/************************************************************************************************       
Controller Name         : BSureS_ViewContactDetails       
Date                    : 21/07/2014        
Author                  : satish.c      
Purpose                 : Supplier Contact Details       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          10/11/2014                  Initial Version
**************************************************************************************************/

public with sharing class BSureS_ViewContactDetails {
	
	public string strContactId{get;set;}
	
	public BSureS_ViewContactDetails() 
    {
    	strContactId=Apexpages.currentPage().getParameters().get('Id');
    	
    }
	public BSureS_Assigned_Contacts__c getContactDetails() 
    {
        BSureS_Assigned_Contacts__c cDetails;
        cDetails = [select id,Supplier_ID__c from BSureS_Assigned_Contacts__c where id =: strContactId limit 1];  
        return cDetails;
    }

}