/**
 * An apex page controller that exposes the site login functionality
 */
public class BSureS_SiteLogin {
	public string uname{get;set;}
    public string passwrd{get;set;}
    public string valMsg{get;set;}
    public string contactid;
    public string resourceid;
    public string caseid;
    public string isEdit; 
    public string accept;
    public string reject;
    public case caseobj;
    public BSureS_SiteLogin(){
     uname = '';
     passwrd = '';
     valMsg = '';
    }
 
    
    public PageReference login(){
        pagereference pgRefr;
        	 valMsg='';
            if((uname!=null && uname!='') && (passwrd!=null && passwrd!='')){
            	    string strquery  = 'select id,name from Contact where Email!=null AND Email=:uname AND Password__c!=null AND Password__c=:passwrd';
                    List<contact> lstContact=database.query(strquery);
                    if(lstContact!=null && lstContact.size()>0){
                    	contactid = lstContact[0].id;
                        Cookie userauth = ApexPages.currentPage().getCookies().get('loginAuth');
                        if(userauth==null){
                                Cookie counter = new Cookie('loginAuth',contactid,null,-1,false);
                            ApexPages.currentPage().setCookies(new System.Cookie[] { new System.Cookie('loginAuth',contactid, null, -1, false) });
                        }else{
                                        ApexPages.currentPage().setCookies(new System.Cookie[] { new System.Cookie('loginAuth',contactid, null, -1, false) });
                        }
                        pgRefr = new pagereference('/apex/BSureS_SiteHome?cntctid='+contactid);
                        pgRefr.setRedirect(true);
                        
                    }else{
                        valMsg='Invalid Credentials';
                    } 
            }
            else if((uname==null || uname=='') && (passwrd==null || passwrd=='')){
            	
                valMsg='Please enter username and password';
            }   
            else{
            	if(uname==null || uname==''){
            		valMsg='Please enter username';
            	}
            	else{
            		valMsg='Please enter password';
            	}
            }
         return pgRefr;    
    }
    
    public PageReference forgotPassword(){
    	pagereference pgRefr = new pagereference('/apex/SiteForgotPassword');
    	pgRefr.setRedirect(true);
    	return pgRefr;
    }
}