/************************************************************************************************       
Controller Name         : BSureC_Customerinformationupload       
Date                    : 03/16/2013        
Author                  : Santhosh Palla       
Purpose                 : This Controller will handle the bulk Customer Upload       
Change History          : 
Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
11/26/2012      Santhosh.Palla            Initial Version
29/04/2013		B.Anupreethi			 Modified the query as per tom/nitin confirmation
**************************************************************************************************/
global with sharing class BSureC_Customerinformationupload 
{
    public String filename{get;set;}//For Uploaded file from page

    public Transient blob bfilecontent{get;set;} // Holds the Uploaded file content from page
    
    //For Search criteria
    
    public string strZoneId {get;set;}//to pass selected Zone from VFPage
    public list<selectOption> lstZoneOptions{get;set;}//Zone options available to select
    public string strSubZoneId {get;set;}//to pass selected subzone from VFpage
    public list<selectOption> lstSubZoneOptions{get;set;}//options for subzones available
    public string strCountryId {get;set;}//to pass country selected from VFpage
    public list<SelectOption> lstCoutryOptions{get;set;}//to get the list for country
    public string strManagerId{get;set;}//to pass manager selected from VFPage
    public list<SelectOption> lstManagers{get;set;}//options available for list of user records
    public string strRegionId{get;set;}//to pass the regionId from VFPage
    public list<selectOption> lstRegionOptions{get;set;}//to get the ist of regions from BSureC_Region__c object
    public string strStateId{get;set;}//to pass the State from VFPage
    public list<selectOption> lstStateOptions{get;set;}//to get the ist of State from State object
    public string strCategoryId{get;set;}//to pass the State from VFPage
    public list<selectOption> lstCategoryOptions{get;set;}//to get the ist of Analyst 
    public string strAnalystId{get;set;}//to pass the Analyst from VFPage
    public list<selectOption> lstAnalystOptions{get;set;}//to get the ist of Analyst 
    
    public string strprofile {get;set;}
    public boolean AlertUser{get;set;}
    
    public static String strfiledata{get;set;}// Holds the CSV file Data ::Added Kishore
    public static String[] filelines = new String[]{};// Holds the each row of the CSV file  ::Added Kishore
    
    /// <summary>  
    /// Constructor  
    /// </summary>
    public BSureC_Customerinformationupload()
    {
        getCategory();
        getRegions();
        getZones();
        //NAGA Check if the user has read access on the BSureC_Customer_Basic_Info_Stage__c object
        if (Schema.sObjectType.BSureC_Customer_Basic_Info_Stage__c.isAccessible())
        {
	    	List<BSureC_Customer_Basic_Info_Stage__c> existRec = [SELECT Id from BSureC_Customer_Basic_Info_Stage__c where id!= null];
	       	if(existRec.size()>0)
	        {
	        	AlertUser = true;
	    	}
	        else
	        {
	        	AlertUser=false;
	        }
        }
	}
    
     /// <summary>
    /// getRegions: list of regions from BSureC_Region__c
    /// </summary>
    /// <returns>get the list of regions</returns>
    public void getRegions()
    { 
        lstRegionOptions=new list<selectOption>();
        list<BSureC_Region__c> listregion=new list<BSureC_Region__c>();
    
        lstRegionOptions.add(new selectOption('Select','Select'));
        //NAGA Check if the user has read access on the BSureC_Region__c object
        if (Schema.sObjectType.BSureC_Region__c.isAccessible())
        {
        	listregion=[select id,Name from BSureC_Region__c WHERE id!=null order by Name ASC];
	         //system.debug('&*&*&$$$$$'+listregion);
	        if(listregion!=null)
	        {
	             for(BSureC_Region__c bs:listregion)
	            {
	               lstRegionOptions.add(new selectoption(bs.id,bs.Name));
	               //mapBSureRegion.put(bs.id,bs.Name);
	            }  
	        }
        }
    }
    /// <summary>
    /// getCategory: list of Categories from BSureC_Category__c
    /// </summary>
    /// <returns>get the list of regions</returns>
    public void getCategory()
    {
        lstCategoryOptions = new list<SelectOption>();
        
        lstCategoryOptions.add(new selectOption('Select','Select'));
        //NAGA Check if the user has read access on the BSureC_Category__c object
        if (Schema.sObjectType.BSureC_Category__c.isAccessible())
        {
	        list<BSureC_Category__c> lstSCatTypes = [SELECT id, Name, Category_ID__c
	                                                      FROM BSureC_Category__c WHERE id!=null 
	                                                      order BY Name ];
	        for(BSureC_Category__c objs : lstSCatTypes)
	        {
	            lstCategoryOptions.add(new SelectOption(objs.id,objs.Name));
	        }
        }
    }
    public void getZones()
    {
        list<BSureC_Zone__c> lstZones = new list<BSureC_Zone__c>();
        lstZoneOptions = new list<selectOption>();
        lstSubZoneOptions = new list<selectOption>();
        lstCoutryOptions = new list<selectOption>();
        
        lstZoneOptions.add(new selectOption('Select','Select'));
        lstSubZoneOptions.add(new selectOption('Select','Select'));
        lstCoutryOptions.add(new selectOption('Select','Select')); 
        //NAGA Check if the user has read access on the BSureC_Zone__c object
        if (Schema.sObjectType.BSureC_Zone__c.isAccessible())
        {
	        lstZones = [select Id,Name from BSureC_Zone__c  where IsActive__c=true order by Name ASC];
	              
	        for(BSureC_Zone__c z:lstZones)
	        {           
	            lstZoneOptions.add(new selectOption(z.id,z.Name));
	        }  
        }
         
    }
     public void getSubZones(){ 
        
        list<BSureC_Sub_Zone__c> lstSubZones = new list<BSureC_Sub_Zone__c>();
        lstSubZoneOptions = new list<selectOption>();
        
        lstSubZoneOptions.add(new selectOption('Select','Select')); 
        if(strZoneId  != null && Schema.sObjectType.BSureC_Sub_Zone__c.isAccessible())
        {
            lstSubZones = [select Id,Name,ZoneID__c from BSureC_Sub_Zone__c where ZoneID__c =:strZoneId and IsActive__c=true order by Name ASC];
        }
              
        for(BSureC_Sub_Zone__c BSZone:lstSubZones) 
        {               
            lstSubZoneOptions.add(new selectOption(BSZone.Id,BSZone.Name)); 
        }
    } 
     /// <summary>
   /// getCountries: egt the list of countries from BSureC_Country__c
   /// </summary>
   /// <returns>get the list of countries</returns>
    public void getCountries()
    {   
        list<BSureC_Country__c> lstCountry  = new list<BSureC_Country__c>(); 
        lstCoutryOptions = new list<selectOption>();
            lstCoutryOptions.add(new selectOption('Select','Select'));  
        
        if(strSubZoneId  != null && Schema.sObjectType.BSureC_Country__c.isAccessible())
        {   
            lstCountry = [select Id,Name,Sub_Zone_ID__c  from BSureC_Country__c where Sub_Zone_ID__c =:strSubZoneId  and IsActive__c=true order by Name ASC];
        }
        for(BSureC_Country__c c:lstCountry)
        {           
            lstCoutryOptions.add(new selectOption(c.Id,c.Name));
        }       
    }
     /// <summary>
    /// Anu This method returns the list of state for selected country Options
    /// </summary>      
    /// <returns> list<selectOption> </returns>
     ///Added by Anu 24/01/2013
    public void getStates()
    {   
        list<BSureC_State__c> lstState  = new list<BSureC_State__c>(); 
        lstStateOptions = new list<selectOption>();
        
        lstStateOptions.add(new selectOption('Select','Select'));
        
        if(strCountryId  != null && Schema.sObjectType.BSureC_State__c.isAccessible())
        {                
            lstState = [select Id,Name  from BSureC_State__c where Customer_Country__c =:strCountryId  and IsActive__c=true order by Name ];
        }
        //system.debug('lstState.......'+lstState);
        
        for(BSureC_State__c c:lstState)
        {           
            lstStateOptions.add(new selectOption(c.Id,c.Name));
        }       
    }  
    
    /// <summary>
    /// This Method returns the list of Analysts from the users
    /// </summary>
    /// <returns>list<SelectOption></returns>
     public void getAnalysts()
    {   
        lstAnalystOptions = new list<selectOption>();
        list<BSureC_AdditionalAttributes__c> lstAddAttributes = new list<BSureC_AdditionalAttributes__c>();
        set<Id> setUserIds = new set<Id>();
        list<User> lstUser = new list<User>();
        String customsetting_object = '';
        
        string strQuery = 'select   id,User__c '+   
                                    'FROM BSureC_AdditionalAttributes__c  '+ 
                                    'WHERE Id != null ';
                                    
            if(strZoneId != null && strZoneId != 'ALL')
                strQuery+=' AND BSureC_Zone__c =:strZoneId  ';
            if(strSubZoneId != null && strSubZoneId != 'ALL')
                strQuery+=' AND BSureC_Sub_Zone__c =:strSubZoneId  ';
            if(strCountryId != null && strCountryId != 'ALL')
                strQuery+=' AND BSureC_Country__c =:strCountryId  ';
            
                    
        lstAddAttributes = database.query(strQuery);
        
                                                                            
        for(BSureC_AdditionalAttributes__c BSureA:lstAddAttributes){
            setUserIds.add(BSureA.User__c);
        }
        customsetting_object = BSureC_CommonUtil.getConfigurationValues('BSureC_MassTransferCreditProfiles').get(0); //BSure_Configuration_Settings__c.getValues(BSureC_MassTransferCreditProfiles);
        
        if(!setUserIds.isEmpty() &&  Schema.sObjectType.User.isAccessible()) 
        {
            lstUser = [SELECT Id,Name,(select id from BSureC_AdditionalAttributes__r) 
                                        FROM User 
                                        WHERE  Id in:setUserIds
                                        AND Profile.Name =: customsetting_object
                                        ];
        }   
                                    
        lstAnalystOptions.add(new selectOption('Select','Select'));
        
        for(User U:lstUser )
        {
            lstAnalystOptions.add(new selectOption(u.Id,U.Name));  
        }
    } 
    /// <summary>
    /// This Method returns the list of Managers for the user
    /// </summary>
    /// <returns>list<SelectOption></returns>
    
    public void getManagers()
    {
        
    }
     //Modified by VINU PRATHAP
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>    
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
     public pagereference SaveDetailsFile()  
     {
        //system.debug('------bfilecontent---'+filename);
        //system.debug('------strCategoryId---'+strCategoryId);
        //system.debug('------strRegionId---'+strRegionId);
        //system.debug('------strZoneId---'+strZoneId);
        //system.debug('------strSubZoneId---'+strSubZoneId);
        //system.debug('------strCountryId---'+strCountryId);
        
            if(strCategoryId == '' || strCategoryId == NULL || strCategoryId == 'Select')
            {
                showErrorMessage('Please Select the Category');
                return null;
            }
            else if(strRegionId == ''|| strRegionId == NULL || strRegionId == 'Select')
            {
                showErrorMessage('Please Select the Region');
                return null;
            }
            else if(strZoneId == '' || strZoneId == NULL || strZoneId == 'Select')
            {
                showErrorMessage('Please Select the Zone');
                return null;
            }
            else if(strSubZoneId == '' || strSubZoneId == NULL || strSubZoneId == 'Select')
            {
                showErrorMessage('Please Select the SubZone');
                return null;
            }
            else if(strCountryId == '' || strCountryId == NULL || strCountryId == 'Select')
            {
                showErrorMessage('Please Select the Country');
                return null;
            }
            else
            {
            	
                if(bfilecontent!=NULL && filename.contains('.csv'))
                {
                	strfiledata = bfilecontent.toString(); //kishore
                	filelines = strfiledata.split('\n');
                	list<String> lstduplicatechk = new list<string>();
                	set<string> setduplicatechk = new set<string>();
                	for(Integer i=0; i < filelines.size(); i++)
                	{
                		String[] strinputvalues = new String[]{};
                		strinputvalues = filelines[i].split(',');
                		if(strinputvalues[0]!=NULL && strinputvalues[0]!=' ' && strinputvalues[0]!='')
                		{
                			lstduplicatechk.add(strinputvalues[0]);
                			setduplicatechk.add(strinputvalues[0]);
                		}
                	}
                	Integer custCount=[select count() from BSureC_Customer_Basic_Info__c where Credit_Account__c IN : setduplicatechk];
                	system.debug(custCount+'setduplicatechk####'+setduplicatechk);
                	if(lstduplicatechk.size() != setduplicatechk.size())
                	{
                		showErrorMessage('File is having duplicate Credit Accounts. ');
                		return null;
                	}
                	else if (custCount >0){
                		showErrorMessage('File is having Existing Credit Accounts. ');
                		return null;
                	}
                	else
                	{
	                	boolean rtnExport = false;
	                    pagereference pageref=new pagereference('/apex/BSureC_CustomerinfoIntermediate');
	                
	                    //Modified to limit the query by VINU PRATHAP
				        //Delete[SELECT Id from BSureC_Customer_Basic_Info_Stage__c where id!= null AND createddate!=TODAY];
				        if(BSureC_Customer_Basic_Info_Stage__c.sObjectType.getDescribe().isDeletable() )
				        {
				        	Delete[SELECT Id from BSureC_Customer_Basic_Info_Stage__c where id!= null];//Anu:29thApr13:modified as per tom/nitin confirmation
				        }	
	                    rtnExport = BSureC_CustomerinfouploadScheduler.createBatchesFromCSV(bfilecontent,'BSureC_Customer_Basic_Info_Stage__c',
	                    strCategoryId,strRegionId,strZoneId,strSubZoneId,strCountryId );
	                    if(rtnExport)
	                    {
	                    	return pageref;
	                    }
	                    else
	                    {
	                    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The uploaded file contians more than 50 records. Please upload 50 records at a time.'));
	                    	return null;
	                    }
                	}   
                }
                else
                {
                    showErrorMessage(system.Label.BSureC_Please_upload_a_csv_file);
                    return null;
                }
           
            }
        
        
     }
     
}