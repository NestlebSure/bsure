/***********************************************************************************************
*       Controller Name : BSureC_CustomerSearch
*       Date            : 12/03/2012 
*       Author          : B.Anupreethi
*       Purpose         : To Get the Customer search results
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       12/03/2012                B.Anupreethi                 Initial Version
*       12/10/2012                B.Anupreethi           //Modified based on the feedback from Nitin-added countries/state and sap id
**************************************************************************************************/

global with sharing class BSureC_CustomerSearch {
    
    //Anu:Link changes  
    public string strCustomerID{get;set;}//To hold Customer Id
    public string strCustomerName{get;set;}//To hold Customer Name
    public string strCustCity{get;set;}//To hold the Customer City
    public string strCustState{get;set;}//To hold the Customer state    
    public string strCustCountry{get;set;}//To hold the Customer state
    public string strCreditAcc{get;set;}//To Hold Customer Credit Account id
    public string strCustZone{get;set;}//To Hold supplier Zone
    public string strCustSubZone{get;set;}//To Hold supplier Sub Zone   
    public Id LoggedInUserId{get;set;}//To hold logged in user id
    
    public list<SelectOption> lstCZone{get;set;}//to hold Zone list
    public list<SelectOption> lstCSubZone{get;set;}//to hold Sub Zone list
    public list<SelectOption> lstCCountry{get;set;}//to hold country list
    public List<SelectOption> lstAvailableStates{get;set;}//To hold Avab states
    public Transient List<BSureC_Customer_Basic_Info__c> lstCustomerInfo{get;set;}//To hold list supplier info
    public Transient list<BSureC_Customer_Basic_Info__c> lstCustInfoPagination{get;set;}//to hold list supplier info
    
    //Anu for pagination
    public integer level{set;get;} //for folder levels
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    
    /// <summary>
    /// Contructor 
    /// </summary>
    public BSureC_CustomerSearch()
    {
        lstCustomerInfo = new List<BSureC_Customer_Basic_Info__c>();
        lstCustInfoPagination = new list<BSureC_Customer_Basic_Info__c>();
        getCZone();
        getUserAttributes();
        strCustomerName=Apexpages.currentPage().getParameters().get('CustName');    
        getCustomerInfo();//to fetch the supplier info  
    }
    
    /// <summary>
    /// Page method used for search criteria(search for auppliers) called from a page action function
    /// </summary>
    public void Search()
    {  
        getCustomerInfo();//to get supplier info based on search
    }
    
     /// <summary>
     /// Method is used to Bind the default values of logged in users
     /// </summary>
     public void getUserAttributes()
     {
         LoggedInUserId=UserInfo.getUserId();
         //system.debug('LoggedInUserId---'+LoggedInUserId);
         list<BSureC_AdditionalAttributes__c> lstAttributes = [SELECT User__c, BSureC_Country__c, BSureC_Sub_Zone__c, BSureC_Zone__c 
                                                                 FROM BSureC_AdditionalAttributes__c 
                                                                 WHERE User__r.IsActive=: true AND User__c=:LoggedInUserId];
         
         //system.debug('lstAttributes---'+lstAttributes);
         if(lstAttributes.size()>0)
         {
             strCustZone = lstAttributes[0].BSureC_Zone__c;
             strCustSubZone = lstAttributes[0].BSureC_Sub_Zone__c;
             strCustCountry = lstAttributes[0].BSureC_Country__c;
         }
     }
    
    /// <summary>
    /// Method to get Zone list for Supplier search 
    /// </summary>
    public void getCZone()
    {
         
        BSureC_CommonUtil.blnflag=false;
        lstCZone=new list<SelectOption>();   
        lstCZone=BSureC_CommonUtil.getZones();        
        //getCSubZoneList();
        //getCCountriesList();
        getSubZoneList(); 
        //system.debug('lstSZone--'+lstCZone);
    }
    
    /// <summary>
    /// Method to get Subzone list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public Pagereference getSubZoneList()
    {
        //system.debug('strCustZone....'+strCustZone);
        lstCSubZone = new list<SelectOption>();
        if(strCustZone != null)
        {
            strCustSubZone = 'Select';
        }
         
        //getCCountriesList();      
        BSureC_CommonUtil.blnflag = false;
        //lstCSubZone=new list<SelectOption>();
        lstCSubZone.clear();
        lstCSubZone = BSureC_CommonUtil.getSubZones(strCustZone);       
        getCountriesList();
        return null;  
    }
   
    /// <summary>
    /// Method to get Country list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public Pagereference getCountriesList()
    {
        lstCCountry = new list<SelectOption>();
        
        if(strCustSubZone != null)
        {
            strCustCountry = 'Select';
        }
        //system.debug('strCustSubZone.........'+strCustSubZone);
        
        BSureC_CommonUtil.blnflag = false;
        lstCCountry.clear();        
        lstCCountry = BSureC_CommonUtil.getCountries(strCustSubZone); 
        getStateList();
        //system.debug('...SCountryLst......'+lstCCountry);
        return null;  
    }
   
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary> 
    /// <returns>List<SelectOption></returns> 
    public Pagereference getStateList()
    {
        BSureC_CommonUtil.blnflag=false;
        lstAvailableStates = new List<SelectOption>();
        lstAvailableStates=BSureC_CommonUtil.getStates(strCustCountry);
        //system.debug('strSCICountryId=======******==='+strCustCountry);
        
        return null;   
    }
    
    /// <summary>
    /// Page method used for search criteria(search for auppliers)
    /// </summary>
    public void getCustomerInfo()
    {
        //lstCustomerInfo = new List<BSureC_Customer_Basic_Info__c>();
        string strQuery = 'SELECT id, Name, Country_Name__c,Customer_Name__c, Customer_Group_Id__c, State_Province__c, City__c, Street_Name_Address_Line_1__c,Credit_Account__c FROM BSureC_Customer_Basic_Info__c  WHERE id!=null';
        
        //system.debug(strCustomerName+'--'+strCreditAcc+'--'+strCustZone+'--'+strCustSubZone+'--');
        if(strCustomerName!='' && strCustomerName!=null)
        {
            string strcustname='%'+strCustomerName+'%';
            strQuery += ' AND Customer_Name__c LIKE: strcustname';
        }
        if(strCreditAcc!='' && strCreditAcc!=null)
        {
            strQuery += ' AND Credit_Account__c =: strCreditAcc ';
        }        
        if(strCustZone!=''  && strCustZone!=null && strCustZone!='Select')
        {
            strQuery += ' AND Zone__c =: strCustZone ';
        }
        if(strCustSubZone!='' && strCustSubZone!=null && strCustSubZone!='Select')
        {
            strQuery += ' AND Sub_Zone__c =: strCustSubZone ';
        }         
        //system.debug('strCustCountry---'+strCustCountry); 
        if(strCustCountry!='' && strCustCountry!=null && strCustCountry!='Select')
        {
            strQuery += ' AND Country__c =: strCustCountry ';
        }
        //system.debug('strCustState---'+strCustState);
        if(strCustState!='' && strCustState!=null  && strCustState!='Select')
        {
            //string strSupState = '%'+strSupplierState+'%';
            strQuery += ' AND State__c =: strCustState ';
        }       
        if(strCustCity!='' && strCustCity!=null)
        {
            string strCustCity = '%'+strCustCity+'%';
            strQuery += ' AND City__c LIKE: strCustCity';
        }
        
       	//strQuery +=' limit 1000';
        //system.debug('strQuery--'+strQuery);
        lstCustomerInfo = database.Query(strQuery);
        //system.debug('lstCustomerInfo---'+lstCustomerInfo);
        
        totallistsize = lstCustomerInfo.size();// this size for pagination
        pageSize = 10;// default page size will be 10
        //system.debug('totallistsize================='+totallistsize);
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstCustInfoPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstCustInfoPagination.size();
            PrevPageNumber=1;
        }
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
     /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************88pavajflajf');
        try
        {
            lstCustInfoPagination=new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex);
            //system.debug('*************totalPageNumber =='+totalPageNumber);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
                    
                        
	        //lstCustomerInfo=new List<BSureC_Customer_Basic_Info__c>();
	        string strQuery = 'SELECT id, Name, Country_Name__c,Customer_Name__c, Customer_Group_Id__c, State_Province__c, City__c, Street_Name_Address_Line_1__c,Credit_Account__c FROM BSureC_Customer_Basic_Info__c  WHERE id!=null';
	        
	        //system.debug(strCustomerName+'--'+strCreditAcc+'--'+strCustZone+'--'+strCustSubZone+'--');
	        if(strCustomerName!='' && strCustomerName!=null)
	        {
	            string strcustname='%'+strCustomerName+'%';
	            strQuery += ' AND Customer_Name__c LIKE: strcustname';
	        }
	        if(strCreditAcc!='' && strCreditAcc!=null)
	        {
	            strQuery += ' AND Credit_Account__c =: strCreditAcc ';
	        }        
	        if(strCustZone!=''  && strCustZone!=null && strCustZone!='Select')
	        {
	            strQuery += ' AND Zone__c =: strCustZone ';
	        }
	        if(strCustSubZone!='' && strCustSubZone!=null && strCustSubZone!='Select')
	        {
	            strQuery += ' AND Sub_Zone__c =: strCustSubZone ';
	        }         
	        //system.debug('strCustCountry---'+strCustCountry); 
	        if(strCustCountry!='' && strCustCountry!=null && strCustCountry!='Select')
	        {
	            strQuery += ' AND Country__c =: strCustCountry ';
	        }
	        //system.debug('strCustState---'+strCustState);
	        if(strCustState!='' && strCustState!=null  && strCustState!='Select')
	        {
	            //string strSupState = '%'+strSupplierState+'%';
	            strQuery += ' AND State__c =: strCustState ';
	        }       
	        if(strCustCity!='' && strCustCity!=null)
	        {
	            string strCustCity = '%'+strCustCity+'%';
	            strQuery += ' AND City__c LIKE: strCustCity';
	        }
	        
	        //strQuery +=' limit 1000';
	        //system.debug('lstCustomerInfo--'+lstCustomerInfo.size());
	        if(lstCustomerInfo == null || lstCustomerInfo.size() == 0)
	        {
	        	lstCustomerInfo = database.Query(strQuery);
	        }   
            

            if(lstCustomerInfo != NULL)
            {
                for(BSureC_Customer_Basic_Info__c b : lstCustomerInfo)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstCustInfoPagination.add(b);// here adding files list
                    }
            	}
            }
            
            pageNumber = newPageIndex;
            NlistSize=lstCustInfoPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstCustInfoPagination=new list<BSureC_Customer_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            for(BSureC_Customer_Basic_Info__c a : lstCustomerInfo)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstCustInfoPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstCustInfoPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);    
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
    
}