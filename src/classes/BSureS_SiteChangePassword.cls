public class BSureS_SiteChangePassword {
	public string contactname{set;get;}
	public string contactemail{set;get;}
	public string oldpsw{set;get;}
	public string newpsw{set;get;}
	public string verifynewpsw{set;get;}
	public static contact contactrecord;
	public BSureS_SiteChangePassword(){
		string contactid = ApexPages.currentPage().getParameters().get('cntctid');
		List<contact> contacts = [select email,Password__c,name from contact where id=:contactid];
		if(contacts.size() > 0){
			contactrecord = contacts[0];
			system.debug('@@contactrecord'+contactrecord);
			contactname = contactrecord.Name;
			contactemail = contactrecord.Email;
		}
	}
	@RemoteAction
    public static String changePasswordToUser(String oldpwd,string newpsw,string cnfrmnewpsw,string contactid){
    	system.debug('@@old'+oldpwd);
    	system.debug('@@newpsw'+newpsw);
    	system.debug('@@cnfrmnewpsw'+cnfrmnewpsw);
    	system.debug('@@contactrecord'+contactrecord);
    	contact contactstaticrecord;
    	if(contactid!='')
    	contactstaticrecord = [select Password__c from contact where id=:contactid];
    	string StatusResult;
    	if(oldpwd != contactstaticrecord.Password__c ){
    		StatusResult = 'Wrong Password';
    	}
    	else if(newpsw != cnfrmnewpsw){
    		StatusResult = 'passwords does not match';
    	}
    	else {
    		contactstaticrecord.Password__c = cnfrmnewpsw;
    		update contactstaticrecord;
    		StatusResult = 'success';
    	}
    	return StatusResult;
    }
	
}