/***********************************************************************************************
* Controller Name   : BsureCNonCreditUserAssignmentController
* Date              : 13/11/2012 
* Author            : Prashanth K
* Purpose           : Class for Assign Non Credit Users 
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- ------------------------- 
* 13/11/2012            Prashanth K         Initial Version
* 24/01/2013            VINU PRATHAP        Modified to Get message from Label
* 26/06/2013            Aditya V            Included Flag to send Mails from this section
**************************************************************************************************/   
 
global with sharing class BsureCNonCreditUserAssignmentController 
{
     
    public string strCurrentSupplierId{get;set;} // page returned id
    public string buyerId{get;set;}
    public boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public List<BSureC_AdditionalAttributes__c> userList {get; set;} 
    public List<UserRole> lstProfileTypes{get;set;}//List object to store and display Profiles
    public List<selectOption> lstProfileOptions{get;set;}//SelectOption list used to display profiles 
    public string strbuyer{get;set;} //variable for Buyer Id(Assign Non Credit user)
    //public string strBuyerName{get;set;} //variable for Buyer name(Assign Non Credit user)
    public String strAssignedBuyerId{get;set;} //This is to store the record Id for edit.
    public string strNoncredirBuyerId{get;set;}
    public string strBuyerCategory{get;set;}
    public Id strsupplierId{get;set;} //displays supplier 
    public string strsupplierName{get;set;} //displays Supplier Name 
    public string strsupplierCategoryId{get;set;} //variable for get Supplier Category Id
    public string srtCName{get;set;} //variable for get Supplier Category name
    public BSureC_Category__c strsupplierCategoryName{get;set;} //variable for get Supplier Category Information
    public List<User> lstUsrsWithSelectedProfile{get;set;}//List of users with a selected profile type.
    public Boolean isSave{get;set;}//This is to consider werther to Insert or update the record.
    public map<string,string> mapuserid{get;set;}
    public string strSearchBuyer{get;set;}
    public BSureC_Assigned_Buyers__c  Buyer{get;set;}
    public list<BSureC_Assigned_Buyers__c> lst_buyerInfo{get;set;}
    public list<BSureC_AdditionalAttributes__c> lstsBuyer{get;set;}
    public list<User> UsersList{get;set;}
    public boolean Edit{get;set;}
    public string selectbuyer{get;set;} 
    
    //Email Variables   
    public string strSendMails{get;set;}
    public string strSubject {get; set;}
    public string strEmailBody {get; set;}
    public set<Id> setEmailRcpts {get; set;}
    public List<Id> editct = new List<Id>();
    public set<Id> strEmailCSV {get; set;}
    public List<UserRole> objCollectorProfile{get;set;}// Profile object
    
    public list<BSureC_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureC_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureC_Country_Manager__c> lstCountryManager{get;set;}
    public set<string> lstChatterSubscribe{get;set;}
    public boolean blndeletebtnVisibility{get;set;}
    public Id currentUserId{get;set;}
    public String[] strAllowedProfiles=new String[] {'BSureC_On Going Collector','BSureC_Un-authorized Collector','BSureC_Un-earned Collector','BSureC_Sales','BSureC_Customer Service','BsureC_Collector'};//Array of Profile Names
    public String selectedProfileId{get;set;}// when user selects particular profile Profile Id will be stored in this
    public string strSelectedBuyerName{get;set;}
    public map<id,string> mapselectedBuyerName = new  map<id,string>();
    public Boolean isLoadingFirstTime=true;// A boolean variable to check whether page is loding for first time.    
    public map<Id,User> mapUser;
    public string strCEO{get;set;}
    public string strBuyerRole{get;set;}
    public string strCollecetorRole{get;set;}
    public string strOnGoingCollector{get;set;}
    public string strUnauthorizedCollector{get;set;}
    public string strUnearnedCollector{get;set;}
    public string strSales{get;set;}
    public string strCustomerService{get;set;}
    public list<string> lstMultipleBuyerRoles{get;set;}
    public string EditeduserId{get;set;}
    public string strNotificationFlag {get; set; }  //Flag that controls mail notifications to subscribers
    public string strCustomerForum{get;set;}
    
    //Constructor
    public BsureCNonCreditUserAssignmentController(ApexPages.StandardController stdController)
    {
    	strCollecetorRole = BSureC_CommonUtil.getConfigurationValues('BsureC_Collector').get(0);
        lstMultipleBuyerRoles = new list<String>();
        UsersList = new list<User>();
        objCollectorProfile=New List<UserRole>();
        Edit = false;
        mapUser = new map<Id,User>([select Id,Name,UserRoleId,profile.Name from User where IsActive = true and profile.Name like 'BSureC%' and UserRole.Name !=: strCollecetorRole order by Name limit 1500]);
        setEmailRcpts = new set<Id>();
        lstZoneManager = new list<BSureC_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureC_Sub_Zone_Manager__c>();
        lstCountryManager = new list<BSureC_Country_Manager__c>();
        Buyer = new BSureC_Assigned_Buyers__c();
        blndeletebtnVisibility = false;
        strSendMails = BSureC_CommonUtil.getConfigurationValues('BSureC_SendNonCreditTeamAssnMails').get(0);
        strCustomerForum = BSureC_CommonUtil.getConfigurationValues('BSureC_CustomerForum').get(0);
        //objCollectorProfile = [select Id,Name from Profile where Name='BsureC_Collector' ];           
             
         if(Apexpages.currentPage().getParameters().get('Custid') != null)
         {
            strCurrentSupplierId = Apexpages.currentPage().getParameters().get('Custid');
            //system.debug('new strCurrentSupplierId..........'+strCurrentSupplierId);
            isSave =true;
         }  
         if(Apexpages.currentPage().getParameters().get('id') != null)
         {  
             buyerId = apexpages.currentpage().getParameters().get('id'); 
             //system.debug('stdController.......' + stdController);  
             Buyer = (BSureC_Assigned_Buyers__c)stdController.getRecord();
            //system.debug('----rec_credit_increase_basic_info_std--'+Buyer);          
             Edit = true;
             if(buyerId != null)
             {
                Buyer = [SELECT Id,Customer_Id__c,Customer_Name__c,User_Id__c,User_Name__c,User_Profile__c from BSureC_Assigned_Buyers__c where Id=:buyerId Limit 1];
                EditeduserId = Buyer.User_Id__c;
                isSave=false;
                strbuyer = Buyer.User_Name__c;
                strNoncredirBuyerId=Buyer.User_Id__c;
                strAssignedBuyerId=buyerId;
                if(mapUser !=null)
                selectedProfileId=mapUser.get(strNoncredirBuyerId)!=null ? mapUser.get(strNoncredirBuyerId).UserRoleId:null;
                //system.debug('strbuyer...'+strbuyer);
                strCurrentSupplierId = Buyer.Customer_Id__c;         
             }
             //system.debug('edit strCurrentSupplierId..........'+strCurrentSupplierId);
          }         
          
            currentUserId = UserInfo.getUserId();  
            //get Roles from Custom Settings . 
            strCEO = BSureC_CommonUtil.getConfigurationValues('BSureC_CEORole').get(0);
            strBuyerRole = BSureC_CommonUtil.getConfigurationValues('BSureC_BuyerRole').get(0);
              
            strOnGoingCollector = BSureC_CommonUtil.getConfigurationValues('BSureC_OnGoingCollector').get(0);
            strUnauthorizedCollector = BSureC_CommonUtil.getConfigurationValues('BSureC_Un-authorizedCollector').get(0);
            strUnearnedCollector = BSureC_CommonUtil.getConfigurationValues('BSureC_Un-earnedCollector').get(0);      
            strSales = BSureC_CommonUtil.getConfigurationValues('BSureC_Sales').get(0);
            strCustomerService = BSureC_CommonUtil.getConfigurationValues('BSureC_CustomerService').get(0);
            //lstMultipleBuyerRoles.add(strCollecetorRole);
            //lstMultipleBuyerRoles.add(strOnGoingCollector);//commented by satish on april 16th 2014
            system.debug(strOnGoingCollector+'strCollecetorRole@@@@@@@@@'+strCollecetorRole);
            lstMultipleBuyerRoles.add(strUnauthorizedCollector);
            lstMultipleBuyerRoles.add(strUnearnedCollector); 
            lstMultipleBuyerRoles.add(strSales);
            lstMultipleBuyerRoles.add(strCustomerService);
            //system.debug('lstMultipleBuyerRoles.......'+lstMultipleBuyerRoles);
                    
            if(strCollecetorRole!=null)
            objCollectorProfile = [select id from UserRole where userrole.name =:strCollecetorRole ];
                     
             list<user> AdminRole;         
             //system.debug('selectedProfileId=============='+selectedProfileId);
             if(strCEO != null && strCEO !='')
            { 
                AdminRole = [select id from user  where IsActive = true  and userrole.name =:strCEO ];
            }
            if((AdminRole.size() > 0 && currentUserId == AdminRole.get(0).Id) && buyerId != null)
            {
                blndeletebtnVisibility = true;
            }   
            List<BSureC_Customer_Basic_Info__c> lstSupplierInfo;
            
            //system.debug('strCurrentSupplierId=============='+strCurrentSupplierId);
            if(strCurrentSupplierId != null)
            {
                lstSupplierInfo = [SELECT Id,Customer_ID__c,Customer_Category__c,Analyst__c,Manager__c,Backup_Analysts__c,Zone__c,Sub_Zone__c,Country__c,Customer_Category_Name__c,Customer_Name__c,Customer_Group_Id__c, Notification_Flag__c from BSureC_Customer_Basic_Info__c Where Id=:strCurrentSupplierId limit 1];
            }
            
            if(lstSupplierInfo != null && lstSupplierInfo.size() > 0)
            {
                for(BSureC_Customer_Basic_Info__c name: lstSupplierInfo)
                {
                    if(lstSupplierInfo.get(0).Notification_Flag__c != null)
                    {
                      strNotificationFlag = string.valueOf(lstSupplierInfo.get(0).Notification_Flag__c);
                    }
                    if(name.Customer_Name__c != null)
                    {
                        strsupplierName = name.Customer_Name__c; 
                    }
                    //system.debug('name.Customer_Category_Name__c=============='+name.Customer_Category_Name__c);
                    if(name.Customer_Category_Name__c != null)
                    {
                        strBuyerCategory = name.Customer_Category_Name__c;
                    }
                    strsupplierId = name.Id; 
                    if(name.Customer_Category__c != null)
                    {
                        strsupplierCategoryId = name.Customer_Category__c; 
                        //system.debug('category=====Supplier==='+ strsupplierCategoryId);
                    }
                    
                    //Email sending..
                    if(name.Analyst__c != null)
                    {
                        setEmailRcpts.add(name.Analyst__c);
                    }
                    
                    if(name.Manager__c != null)
                    {
                        if( (name.Manager__c == currentUserId ) && buyerId != null)
                        {
                            blndeletebtnVisibility = true;
                        }
                        setEmailRcpts.add(name.Manager__c);
                    }
                    
                    if(name.Backup_Analysts__c != null )
                    {
                        if(name.Backup_Analysts__c.contains(','))
                        {
                            editct = name.Backup_Analysts__c.split(',');
                        }
                        else
                        {
                            editct.add(name.Backup_Analysts__c);
                        }
                        //setEmailRcpts.addAll(editct);
                    }
                   
                    if(strsupplierCategoryId!=null)
                    {
                        strsupplierCategoryName = [select Name  from BSureC_Category__c where id =:strsupplierCategoryId];              
                        if(strsupplierCategoryName.Name != null &&  strsupplierCategoryName.Name!='')
                        {
                            srtCName = strsupplierCategoryName.Name;
                        }
                    }
                }
                
                
                if(lstSupplierInfo.get(0).Zone__c != null)
                {
                    lstZoneManager = [select Zone_Manager__c from  BSureC_Zone_Manager__c where Zone__c =: lstSupplierInfo.get(0).Zone__c];
                }
                
                if(lstZoneManager != null && lstZoneManager.size() > 0)
                {
                    for(BSureC_Zone_Manager__c objZM :lstZoneManager )
                    {
                        if((objZM.Zone_Manager__c == currentUserId )&& buyerId != null)
                        {
                            blndeletebtnVisibility = true;
                        }
                        setEmailRcpts.add(objZM.Zone_Manager__c);
                    }
                }
                
                if(lstSupplierInfo.get(0).Sub_Zone__c != null)
                {
                    lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureC_Sub_Zone_Manager__c  where Sub_Zone__c =: lstSupplierInfo.get(0).Sub_Zone__c];
                }
                
                if(lstSubZoneManager != null && lstSubZoneManager.size() > 0)
                {
                    for(BSureC_Sub_Zone_Manager__c  objSZM:lstSubZoneManager)
                    {
                        if( (objSZM.Sub_Zone_Manager__c == currentUserId) && buyerId != null)
                        {
                            blndeletebtnVisibility = true;
                        }
                        setEmailRcpts.add(objSZM.Sub_Zone_Manager__c);
                    }
                }
                
                if(lstSupplierInfo.get(0).Country__c != null)
                {
                    lstCountryManager = [select Country_Manager__c from  BSureC_Country_Manager__c  where Country__c =: lstSupplierInfo.get(0).Country__c];
                }
                
                if(lstCountryManager != null && lstCountryManager.size() > 0)
                {
                    for(BSureC_Country_Manager__c objCM:lstCountryManager)
                    {
                        if( (objCM.Country_Manager__c == currentUserId) && buyerId != null)
                        {
                            blndeletebtnVisibility = true;
                        }
                        setEmailRcpts.add(objCM.Country_Manager__c);
                    }
                }
                
            }             
    }     
    
    /// <summary> 
    /// Method to close the Buyer list, after select the Buyer
    /// </summary> 
    public void closepopup() 
    {
        selectbuyer = apexpages.currentpage().getparameters().get('buyerid');
        if(selectbuyer != null){
            //User u = [select Id,Name from User where Id =:selectbuyer];
            if(mapUser.get(selectbuyer) != null)
            {
                strbuyer = mapUser.get(selectbuyer).Name;
                strNoncredirBuyerId  = mapUser.get(selectbuyer).Id;
            }    
            else
            {
                strbuyer = 'User Assigned Failed';
            }
        }
        else
        {
            strbuyer = 'User Assigned Failed';
        }
        
        showeditrecs = false; 
    }
    
    /// Gets the profiles for Non Credit team assignment operation
    public List<selectOption> getUserProfiles() 
    {  
        //String strGetRoles; 
        //String[] strAllowedRoles=new String[] {'BSureC_OnGoingCollector','BSureC_Un-authorizedCollector','BSureC_Un-earnedCollector','BSureC_Sales','BSureC_CustomerService','BsureC_Collector'};//Array of Role Names
        //strGetRoles=BSureC_CommonUtil.getConfigurationValues(strAllowedRoles).get(0);
       //isLoadingFirstTime=false;
       //system.debug('lstMultipleBuyerRoles============='+lstMultipleBuyerRoles);
        lstProfileOptions = new List<selectOption>(); //new list for holding all of the picklist options
       try
       {
            lstProfileOptions.add(new selectOption('','-SELECT-'));
            //lstProfileTypes = [select Id,Name,(Select Id,Name,Profile.Name From Users) From UserRole where Id in:lstMultipleBuyerRoles ];
            
            lstProfileTypes=[SELECT Id,Name from UserRole where Name IN:lstMultipleBuyerRoles];                     
            if(lstProfileTypes!=null && lstProfileTypes.size()>0)
            {
                lstProfileTypes.sort();
                for (UserRole profle : lstProfileTypes) 
                { //query for User records with System Admin profile
                   lstProfileOptions.add(new selectOption(profle.Id, profle.Name.substring((profle.Name.indexOf('_',0)+1),profle.Name.length()))); //for all records found - add them to the picklist options                  
                  
                }
            }
           
       }
       catch(Exception ex)
       {
            //system.debug('getUserProfiles()::Error::'+ex); 
       }               
        
      return lstProfileOptions; //return the picklist options
    }
        
    //The is a pagereference returntype method used to call actual method
    public void getUserWithSelectedProfile() 
    {
        //system.debug('strBuyerId........'+strNoncredirBuyerId);
        //system.debug('selectedProfileId........'+selectedProfileId);
        UsersList = new list<User>();
        if(selectedProfileId!=null)
        {
         UsersList = [select Id,Name From User where IsActive = true and UserRoleId=:selectedProfileId order by Name];
        }        
         //showeditrecs=false; 
    }
    
   /// <summary>
    /// Method to Show Buyer list
    /// </summary> 
    public void showBuyerDetails()
    {
        showeditrecs=true;
        isSave=true;         
        
        //system.debug('show buyer strCurrentSupplierId..........'+strCurrentSupplierId);
        UsersList = new list<User>();        
        //mapUser = new map<Id,User>([select Id,Name,UserRoleId from User limit 1000]);
        //system.debug('strNoncredirBuyerId......'+strNoncredirBuyerId);       
        if(mapUser!=null)
        {
            selectedProfileId=mapUser.get(strNoncredirBuyerId)!=null ? mapUser.get(strNoncredirBuyerId).UserRoleId:null;
            //system.debug('selectedProfileId.............'+selectedProfileId);
            if(selectedProfileId!=null)
            {
                String strSelect='-SELECT-';
                UsersList=[SELECT Id,Name from User where IsActive=true and UserRoleId=:selectedProfileId  order by Name];             
            }         
        }
    }  
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
    
    /// <summary>
    /// Method to Save Buyer information
    /// </summary>
    public pageReference  buyerSave()
    {
        set<Id> setUsersnames = new set<Id>();
        
        ApexPages.Message errormsg;
        pagereference pageref = new pagereference('/apex/BSureC_ViewCustomerDetails?Id='+strCurrentSupplierId);
        lstChatterSubscribe = new set<string>();
        lst_buyerInfo=new list<BSureC_Assigned_Buyers__c>();
        //List<BSureC_Assigned_Buyers__c> lstExistingCollectors=new List<BSureC_Assigned_Buyers__c>();
        //system.debug('strBuyerId=================='+strNoncredirBuyerId);
        //system.debug('strCurrentSupplierId======123=='+strCurrentSupplierId);
        //system.debug('isSave....'+isSave );
        if(isSave==true)
        {
            if(strbuyer == null || strbuyer == '' )
            { 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,system.label.BsureC_Non_Credit_Please_select_an_user));
                return null;
            }  
            else
            { 
                
                //system.debug('objCollectorProfile....==='+objCollectorProfile);           
                //system.debug('selectedProfileId==='+selectedProfileId);
                if(objCollectorProfile !=null && objCollectorProfile.Size()>0)
                {
                    //system.debug('objCollectorProfile.Id==='+objCollectorProfile[0].Id);
                    if(objCollectorProfile[0].Id==selectedProfileId) 
                    {
                       lst_buyerInfo = [SELECT Id,Customer_Id__c,User_Id__c,User_Name__c,User_Profile__c from BSureC_Assigned_Buyers__c where Customer_Id__c=:strsupplierId And User_Id__c=:objCollectorProfile[0].Id];
                                     
                    }
                    if(lst_buyerInfo!=null && lst_buyerInfo.size() > 0)
                    {  
                        for(BSureC_Assigned_Buyers__c objBuyerId: lst_buyerInfo)
                        {
                            //setEmailRcpts.add(objBuyerId.User_Id__c);                           
                        }
                        if(isSave)
                        {
                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,system.label.BSureC_Can_assign_only_one_collector));
                           return null; 
                        }
                        else
                        {
                            for(BSureC_Assigned_Buyers__c eachRec :lst_buyerInfo )
                            {
                                string aaa=String.valueOf(eachRec.Id);
                                //system.debug('aaa....'+aaa);
                                
                                if(!aaa.contains(strAssignedBuyerId))
                                {
                                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,system.label.BSureC_Can_assign_only_one_collector));
                                  return null; 
                                }
                            } 
                        }                                 
                    }
                }
                List<BSureC_Assigned_Buyers__c> buyerV = new List<BSureC_Assigned_Buyers__c> ();
                
                if(strNoncredirBuyerId != null)
                {
                    buyerV = [select id from BSureC_Assigned_Buyers__c where Customer_Id__c =:strsupplierId and User_Id__c =:strNoncredirBuyerId];
                    If(Edit == true && buyerV.size() > 0 && EditeduserId.trim() != strNoncredirBuyerId.trim() ){
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,system.label.BSureC_This_User_is_already_assigned));
                            return null;
                        }
                }                   
                
                if(buyerV != null && ((Edit == true && buyerV.size() > 2 ) || (Edit == false && buyerV.size() > 0 )) ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,system.label.BSureC_This_User_is_already_assigned));
                    return null; 
                }                           
                else 
                {
                    BSureC_Assigned_Buyers__c bsave=new BSureC_Assigned_Buyers__c();
                    if(buyerId != null )
                    {
                        bsave=buyer;                        
                    }                    
                    if(strNoncredirBuyerId != null && bsave!=null)
                        setEmailRcpts.add(strNoncredirBuyerId);
                        bsave.User_Id__c = strNoncredirBuyerId;
                        
                        
                    if(strsupplierId != null )
                    {
                        bsave.Customer_Id__c = strsupplierId;
                    }                                 
                    
                    lstChatterSubscribe.add(strNoncredirBuyerId);
                    //Email sending..
                    Database.UpsertResult resultDB= Database.upsert(bsave);             
                          
                     if(resultDB.isSuccess())
                     {
                        BSureC_CommonUtil.InsertFollowers(strsupplierId,lstChatterSubscribe);
                        if(strSendMails.equalsIgnoreCase('TRUE'))
                        {
                            SendNotification();
                        }
                     }          
                    return pageref;
                }    
            } 
        }
    
        return pageref;
    }   
    public void SendNotification()
    {    
        //system.debug('strNotificationFlag.....'+strNotificationFlag);
        if(setEmailRcpts != null && setEmailRcpts.size() > 0)
        {  
            
            strSelectedBuyerName  = mapUser.get(selectbuyer).Name; 
            //system.debug('strSelectedBuyerName......'+strSelectedBuyerName);
            strEmailBody = '';      
            strSubject = 'Non Credit User Assigned for Customer :'+ strSupplierName; 
            
            strEmailBody += '<table cellspacing="2" cellpadding="0">';
            strEmailBody += '<tr> <td> The following Non Credit User Assigned to the Customer :  <b>'+strSupplierName + '</b></td></tr>';
            strEmailBody += '</table>';
            strEmailBody += '<table cellspacing="2" cellpadding="0" border="1">';
            strEmailBody += '<tr><td height="40px" colspan="2" ><center> <b> '+ strCustomerForum +'  </b> </center></td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Non Credit User </td><td height="21px">: '+  strSelectedBuyerName +'</td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Posted By </td><td height="21px">: '+UserInfo.getName() +'</td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Posted Date </td><td height="21px">: '+ System.dateTime.Now().format('EEEE, MMM d, yyyy HH:mm:ss aaa z') +'</td></tr>';
            strEmailBody += '</table>';
            
            /*     
            strSubject = 'Assigned '+strSelectedBuyerName+' to Customer' ; 
            strEmailBody += '<table cellspacing="0" cellpadding="0" width="800px" border="1" >';            
            strEmailBody += '</table>';
            strEmailBody += ''+strSelectedBuyerName+' has been assigned to : '+strsupplierName ;
            */
            strEmailBody = strEmailBody.replace('null', '');
            BSureC_CommonUtil.SendEmailToQueue(setEmailRcpts,strSubject,strEmailBody,strNotificationFlag,system.today(),false);
        }          
    }    
    public pagereference cancel()  
    {
        pagereference pageref = new pagereference('/apex/BSureC_ViewCustomerDetails?Id='+strCurrentSupplierId);
        return pageref;
    }

}