/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_PaymentHistoryBatch {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        
         BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureC_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureC_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'bsurec_countrymanager';
        objCSettings3.Parameter_Key__c = 'bsurec_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings4 = new BSure_Configuration_Settings__c();
        objCSettings4.Name = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Key__c = 'bsurec_regionmanagerrole';
        objCSettings4.Parameter_Value__c = 'Region Manager';
        insert objCSettings4;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'bsurec_analystrole';
        objCSettings5.Parameter_Key__c = 'bsurec_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'bsurec_buyerrole';
        objCSettings6.Parameter_Key__c = 'bsurec_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
        
        BSure_Configuration_Settings__c objCSettings7 = new BSure_Configuration_Settings__c();
        objCSettings7.Name = 'BSureC_ShowCustomers';
        objCSettings7.Parameter_Key__c = 'ShowAllCustomersForManagers';
        objCSettings7.Parameter_Value__c = 'TRUE';
        insert objCSettings7;
        
        
        BSureC_Customer_Basic_Info__c objbasicinfo = new BSureC_Customer_Basic_Info__c();
        
         objbasicinfo.Customer_Group_Id__c = '2345';
         objbasicinfo.Customer_Name__c = 'SAM';
         insert objbasicinfo;
         system.assertEquals('SAM', objbasicinfo.Customer_Name__c);
         BSureC_Divisions__c objdivision = new BSureC_Divisions__c();
         objdivision.Division_Name__c = 'asdf';
         objdivision.Is_Active__c = true;
         insert objdivision;
        system.assertEquals('asdf', objdivision.Division_Name__c);
        
         BSureC_Payment_History_Stage__c recltltpymthstrypblsh = new BSureC_Payment_History_Stage__c();
            recltltpymthstrypblsh.Group_Id__c = '1234';
            recltltpymthstrypblsh.High_Credit__c = Decimal.valueOf( Double.valueOf('1234') );
            recltltpymthstrypblsh.Information_Date__c = Date.parse('11/19/2012');
            //recltltpymthstrypblsh.Open_Balance__c = Decimal.valueOf( Double.valueOf('1234') );
            recltltpymthstrypblsh.Past_Due__c = Decimal.valueOf( Double.valueOf('1234') );
            recltltpymthstrypblsh.Percent_Current__c = Decimal.valueOf( Double.valueOf('1234') );
            recltltpymthstrypblsh.Customer_Basic_Info_Id__c = String.valueOf(objbasicinfo.Id);
            recltltpymthstrypblsh.Customer_Division_Id__c = String.valueOf(objdivision.Id);      
            recltltpymthstrypblsh.Publish_Flag__c = false;
            recltltpymthstrypblsh.Exception__c = NULL;      
            //obj.Publish_Flag__c=true;//for inserted records making flag as true to update in stgae
            insert recltltpymthstrypblsh;
            
            String strq = 'SELECT Id,of_Limit__c,Slow__c,Risk_Category__c,Customer_Basic_Info_Id__c,Record_Date__c,Prompt__c,Last_Review__c,Group_Id__c,Exposure__c,Discount__c,Average_Days_Slow__c,Credit_Limit__c,CE__c,Bill_to_Account__c FROM BSureC_Credit_Data_Stage__c';
            
            List<BSureC_Credit_Data_Stage__c> objlistq = Database.query(strq);
            //system.debug('----objlistq---'+objlistq);
            BsureC_PaymentHistoryBatch b = new BsureC_PaymentHistoryBatch(); 
            Database.executebatch(b);
    }
}