global class bSurec_CreditSafe{
    public  static string CSafeRespString;
    public  static CreditSafeBean InsertBean;
    public static string Messagecodes;
    public static Map<String, BsureC_CreditSafeCodes__c> mcs;
    public bSurec_CreditSafe(){
        InsertBean  = new CreditSafeBean();
        mcs = new Map<String, BsureC_CreditSafeCodes__c>();
    }
    
    webservice static  String FetchCompanyOnlineReport(String CBN,string MasterId){
        string strTimeOut = ''; 
        Messagecodes ='';
        system.debug('CN===='+CBN);
        string strExperianEndpoint ='';
        string strSoapAction ='';
        string XMLMapResult1='';
        bSurec_CreditSafe obj = new bSurec_CreditSafe();
        mcs = BsureC_CreditSafeCodes__c.getAll();
        if(mcs!=null){
            if(mcs.get('EndPointURL')!=null){
                BsureC_CreditSafeCodes__c echVar = mcs.get('EndPointURL');
                strExperianEndpoint = echVar.Message__c ;
            }
            if(mcs.get('SOAPAction')!=null){
                BsureC_CreditSafeCodes__c echVar = mcs.get('SOAPAction');
                strSoapAction = echVar.Message__c   ;
            }
        }
        string strUsername ='';
        string strPassword ='';
        
        list<bSureC_ExpCred__c> lstCreSafe =[Select id,Name,Pwd__c,UN__c 
                                                From bSureC_ExpCred__c  
                                                where Credit_Agency__c='Creditsafe' limit 1];
        if(lstCreSafe!=null){
            strUsername =lstCreSafe[0].UN__c;
            strPassword =lstCreSafe[0].Pwd__c;
        }                                       
        string strXML ='<?xml version="1.0" encoding="UTF-8"?>'+
                    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oper="http://www.creditsafe.com/globaldata/operations">'+
                       '<soapenv:Header/>'+
                       '<soapenv:Body>'+
                          '<oper:RetrieveCompanyOnlineReport>'+
                             '<oper:companyId>'+CBN +'</oper:companyId>'+
                             '<oper:reportType>Full</oper:reportType>'+
                             '<oper:language>EN</oper:language>'+
                          '</oper:RetrieveCompanyOnlineReport>'+
                       '</soapenv:Body>'+
                    '</soapenv:Envelope>';                       
        HttpRequest req = new HttpRequest();  
        req.setEndpoint(strExperianEndpoint);
        req.setHeader('content-type','text/xml; charset=UTF-8');
        req.setHeader('SOAPAction',strSoapAction);
        req.setMethod('POST');
        Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setBody(strXML); 
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        XmlStreamReader reader;
        if(!Test.isRunningTest()){
            try{
            req.setTimeout(120000); // timeout in milliseconds - this is one minute
            res = http.send(req);
            }catch(Exception ex){
            system.debug('Exception occured::: '+ex.getMessage()+ex.getStackTraceString());
            strTimeOut = Label.BSureS_ExpTimeout+' '+ex.getMessage();
            }
            system.debug(res.getStatusCode()+'res===='+res.getBody());
        }
        else{
          res.setHeader('Content-Type', 'application/xml');
          res.setBody('{"foo":"bar"}');
          res.setStatusCode(200);
        }
        if(res!=null && res.getStatusCode() == 200){
            reader = res.getXmlStreamReader();
            CSafeRespString = res.getBody();
            XMLMapResult1 = obj.parseXML(reader);
        }
        else if(res!=null && res.getStatusCode() == 401){
        
            return 'Unauthorized';
        }
        else{
            return strTimeOut;
        }
        if(XMLMapResult1 =='Success'){
            string fileName = 'CreditSafe' +  system.now().format('MMddyy');
            string CreditId = obj.insertONGS(MasterId,filename);
            string strPrvusId = obj.LastAttachementId(MasterId);
            string status =obj.UpdateMaster(MasterId,InsertBean,strPrvusId);
            Database.saveResult SaveResult ;
            if(CreditId !=null && CreditId !=''){
                Blob blobContent = Blob.valueof(res.getBody() );
                Attachment myAttachment  = new Attachment();
                myAttachment.Body = blobContent;
                myAttachment.Name = fileName;
                myAttachment.parentId = CreditId ;
                SaveResult = Database.insert(myAttachment);
                if(XMLMapResult1 != '' && XMLMapResult1 != null){
                    XMLMapResult1 += '::'+CreditId+'::'+myAttachment.id +'::'+Messagecodes;
                }
            }
        }
        return XMLMapResult1;
                        
    }
     /* Reading the Xml Response*/
    public string parseXML(XmlStreamReader reader){
        try{
            boolean isSafeToGetNextXmlElement =true;
            string xmlstr;
            integer cntJudgement =0;
            while(isSafeToGetNextXmlElement){
                if (reader.getEventType() == XmlTag.START_ELEMENT){
                    if('Messages' == reader.getLocalName()){
                        string str ='';
                        boolean ActFlag =  true;
                        while(ActFlag){
                            reader.nextTag();
                            if('Message' == reader.getLocalName() && reader.getEventType() == XmlTag.START_ELEMENT ){
                                str =reader.getAttributeValue(null, 'Code');
                                xmlstr = getValueFromTag(reader);
                                if(Str!=null ){
                                    if(mcs.get(str)!=null){
                                        Messagecodes +=mcs.get(str) +',';
                                    }
                                    else{
                                        Messagecodes +=xmlstr +',';
                                    }
                                }
                            }
                            if('Messages' == reader.getLocalName() &&  reader.getEventType() == XmlTag.END_ELEMENT){
                                ActFlag =  false;
                                reader.nextTag();
                            }
                        }
                        if(Messagecodes!=null && Messagecodes!=''){
                            Messagecodes =Messagecodes.substring(0,Messagecodes.length()-1);
                        }
                    }
                    if('CreditRating'== reader.getLocalName()){
                        reader.nextTag();
                        InsertBean.ReportedDate=system.today();
                        if('CommonValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeRating =xmlstr;
                            reader.nextTag();
                        }
                        if('CommonDescription'==reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeCommonDescription = xmlstr;
                            reader.nextTag();
                        }
                        if('CreditLimit'==reader.getLocalName()){
                        	xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                        if('ProviderValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeScore =xmlstr;
                            reader.nextTag();
                        }
                        if('ProviderDescription'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditSafeProviderDescription =xmlstr;
                            reader.nextTag();
                        }
                        reader.nextTag();
                    }
                    if('PreviousCreditRating'== reader.getLocalName()){
                    	reader.nextTag();
                    	system.debug('*************'+reader.getLocalName());
                    	 if('CommonValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                        if('CommonDescription'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                    	if('CreditLimit' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		system.debug('*******val******'+xmlstr);
                    		InsertBean.HighCreditLimit = xmlstr!=null ? decimal.valueof(xmlstr):0.00;
                    	}
                    }
                    if('CompanyDBT'== reader.getLocalName()){
                    	xmlstr = getValueFromTag(reader);
                    	InsertBean.CompanyDBT = xmlstr!=null ? integer.valueof(xmlstr):0;
                    	reader.nextTag();
                    }
                    
                    if('IndustryDBT'== reader.getLocalName()){
                    	xmlstr = getValueFromTag(reader);
                    	InsertBean.IndustryDBT = xmlstr!=null ? integer.valueof(xmlstr):0;
                    	reader.nextTag();
                    }
                    if('NumberOfEmployees' == reader.getLocalName()){
                    	xmlstr = getValueFromTag(reader);
                    	InsertBean.Workforce = xmlstr!=null ? integer.valueof(xmlstr):0;
                    	reader.nextTag();
                    }
                    if('LegalFilingSummary' == reader.getLocalName()){
                    	reader.nextTag();
                    	if('Bankruptcy' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		InsertBean.IsBankruptcy =xmlstr;
                    		reader.nextTag();
                    	}
                    	integer SumOfDerogatory =0;
                    	if('TaxLienFilings' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		InsertBean.TaxLiens = xmlstr!=null ? integer.valueof(xmlstr):0;
                    		SumOfDerogatory += InsertBean.TaxLiens;
                    		reader.nextTag();
                    	}
                    	if('JudgmentFilings' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		InsertBean.Judgementfilings = xmlstr!=null ? integer.valueof(xmlstr):0;
                    		SumOfDerogatory += InsertBean.Judgementfilings;
                    		reader.nextTag();
                    	}
                    	if('BankruptyFilings' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		InsertBean.Bankruptcyfilings = xmlstr!=null ? integer.valueof(xmlstr):0;
                    		SumOfDerogatory += InsertBean.Bankruptcyfilings;
                    		reader.nextTag();
                    	}
                    	InsertBean.DerogatoryLegalFilings =SumOfDerogatory;
                    	if('Sum' == reader.getLocalName()){
                    		xmlstr = getValueFromTag(reader);
                    		InsertBean.Collections = xmlstr!=null ? decimal.valueof(xmlstr):0;
                    		reader.nextTag();
                    	}
                    }
                    
                }
                if (reader.hasNext()){
                    reader.next();
                } 
                else {
                    isSafeToGetNextXmlElement = false;
                    break; 
                }
            }
            return 'Success';
        }
        catch(XmlException ex){
        	system.debug('****Ex*****'+ex.getMessage()+ex.getStackTraceString());
            return 'Exception during Xml parsing error : '+ex.getMessage()+ex.getStackTraceString();
        }
        return '';
    }
   
    /* Getting the Value from the Tag */
    public String getValueFromTag(XmlStreamReader reader){ 
        String returnvalue = '';
        reader.setCoalescing(true);
        while(reader.hasNext()){
            if (reader.getEventType() == XmlTag.END_ELEMENT){
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS){
                returnvalue=reader.getText();
            }
        reader.next();
        }
        return returnvalue;
    }
    public string LastAttachementId (String ParentId){
    	string attchId='';
    	integer cnt=0;
    	for(BSureC_On_Going_Credit_Section__c lstVal:[Select id from BSureC_On_Going_Credit_Section__c
    													 where Customer_ID__c =: ParentId and TitleDescription__c LIKE 'Creditsafe%' 
    													order by createddate DESC limit 2 ]){
    		if(cnt==1){
    			Attachment att = [select id from Attachment where parentid=:lstVal.id];
    			if(att!=null){
    				attchId =att.id;
    			}
    		}						
    		cnt++;						 
		 }
    	
    	return attchId;	
    }
    public string UpdateMaster(String ParentId,CreditSafeBean SafeBean,string perviousAttID){
        string DatabaseStatus='Success';
        list<bSureC_CreditAgency_History__c> lstOfHistory = new list<bSureC_CreditAgency_History__c>();
        list<BSureC_Customer_Basic_Info__c> lstCustInfo = new list<BSureC_Customer_Basic_Info__c>();
        for(BSureC_Customer_Basic_Info__c echCust  :[Select id,Creditsafe_Common_Description__c,
                                                    Creditsafe_Provider_Description__c,Creditsafe_Rating__c,
                                                    Creditsafe_Score__c,Report_Date_CS__c,Derogatory_Legal_Filings_CS__c,
                                                    Tax_Liens_CS__c,Workforce__c,Collections__c,Bankruptcy_Filings_CS__c,
                                                    Recent_High_Credit__c,Company_DBT_CS__c,Industry_DBT_CS__c	
                                                from BSureC_Customer_Basic_Info__c  
                                                where id=:ParentId]){
            /*Load to History object*/
            bSureC_CreditAgency_History__c loadHisdata = new bSureC_CreditAgency_History__c();
	            loadHisdata.Customer_Info_Id__c =echCust.id;
	            loadHisdata.Name ='CreditSafe';
	            loadHisdata.RecordTypeId ='012d0000001lAYPAA2';
	            loadHisdata.Creditsafe_Score__c = echCust.Creditsafe_Score__c;
	            loadHisdata.Creditsafe_Common_Description__c = echCust.Creditsafe_Common_Description__c;
	            loadHisdata.Creditsafe_Rating__c = echCust.Creditsafe_Rating__c;
	            loadHisdata.Creditsafe_Provider_Description__c = echCust.Creditsafe_Provider_Description__c;
	            loadHisdata.Report_Date__c = echCust.Report_Date_CS__c;
				loadHisdata.Legal_Filings__c = echCust.Derogatory_Legal_Filings_CS__c!=null ? string.valueof(echCust.Derogatory_Legal_Filings_CS__c):'0';
	            loadHisdata.Tax_Liens__c = echCust.Tax_Liens_CS__c;
	            loadHisdata.Workforce__c = echCust.Workforce__c;
	            loadHisdata.Collections__c = echCust.Collections__c;
	            loadHisdata.Bankruptcy_filings__c = echCust.Bankruptcy_Filings_CS__c;
				loadHisdata.Recent_High_Credit__c = echCust.Recent_High_Credit__c;
	           	loadHisdata.Company_DBT__c = echCust.Company_DBT_CS__c!=null ? string.valueof(echCust.Company_DBT_CS__c):'0';
	            loadHisdata.Industry_DBT__c = echCust.Industry_DBT_CS__c; 
	            loadHisdata.AttachmentId__c = perviousAttID ;          
            lstOfHistory.add(loadHisdata);
            /*Update the response*/ 
	            echCust.Creditsafe_Score__c =SafeBean.CreditsafeScore;
	            echCust.Creditsafe_Common_Description__c =SafeBean.CreditsafeCommonDescription;
	            echCust.Creditsafe_Rating__c =SafeBean.CreditsafeRating;
	            echCust.Creditsafe_Provider_Description__c =SafeBean.CreditSafeProviderDescription;
	            echCust.Report_Date_CS__c =SafeBean.reportedDate;
	            echCust.Derogatory_Legal_Filings_CS__c =SafeBean.DerogatoryLegalFilings;
	            echCust.Tax_Liens_CS__c =SafeBean.TaxLiens;
	            echCust.Workforce__c =SafeBean.Workforce;
	            echCust.Bankruptcy_Filings_CS__c =SafeBean.IsBankruptcy;
	            echCust.Collections__c =SafeBean.Collections;
	            echCust.Recent_High_Credit__c =SafeBean.HighCreditLimit;
	            echCust.Company_DBT_CS__c =SafeBean.CompanyDBT;
	            echCust.Industry_DBT_CS__c =SafeBean.IndustryDBT;
            
            lstCustInfo.add(echCust);
        }
        if(lstOfHistory!=null){
            database.insert(lstOfHistory);
        }
        if(lstCustInfo!=null){
            database.update(lstCustInfo);
        }       
        return DatabaseStatus;
    }
    public string insertONGS(string strRecId,string strFName){
      Database.upsertResult Result;
      try{
      list<BSureC_On_Going_Credit_Section__c> lstOGCS = [Select id,name,Customer_ID__c,TitleDescription__c,Document_Type__c from BSureC_On_Going_Credit_Section__c 
                                                          where Customer_ID__c =: strRecId and TitleDescription__c =: strFName];
          BSureC_On_Going_Credit_Section__c objOGCS = new BSureC_On_Going_Credit_Section__c();
          if(lstOGCS != null && lstOGCS.size()>0){
              objOGCS = lstOGCS[0];
              list<Attachment> attach = [select id,name from Attachment where ParentId=:objOGCS.id];
              delete attach;
          }       
          system.debug('strFName==='+strFName+'****'+strRecId);
          objOGCS.TitleDescription__c = strFName;
          objOGCS.Customer_ID__c = strRecId;
          objOGCS.Discussion_Type__c = 'File'; 
          objOGCS.Document_Type__c = 'a0Md0000007we2X';
            
          Result = Database.upsert(objOGCS);
          system.debug('objOGCS==='+ objOGCS);
          system.debug('Result ==='+ Result );
      }catch(Exception ex){
        system.debug('Exception occured while saving Experian report into OnGoingCreditSection.'+ex.getMessage()+ex.getStackTraceString());
      }
      if(Result.isSuccess() == true){
          return Result.Id;
        }else{
          return '';
        }
    } 
  
    public class CreditSafeBean{
        public string CreditsafeScore;
        public string CreditsafeCommonDescription;
        public string CreditsafeRating;
        public string CreditSafeProviderDescription;
        public date reportedDate;
        public integer CompanyDBT;
        public integer IndustryDBT;
        public decimal HighCreditLimit;
        public integer DerogatoryLegalFilings;
        public integer TaxLiens;
        public String IsBankruptcy;
        public integer Bankruptcyfilings;
        public integer Judgementfilings;
        public decimal Collections;
        public integer Workforce;
    }   
}