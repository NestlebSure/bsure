/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureC_CustomUserReport {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        list<BSure_Configuration_Settings__c> lstSettings = new list<BSure_Configuration_Settings__c>();
        BSure_Configuration_Settings__c cSettings = new BSure_Configuration_Settings__c();
        cSettings.Name = 'BSureC_Report_Page_Size';
        cSettings.Parameter_Key__c = 'BSureC_Report_Page_Size';
        cSettings.Parameter_Value__c = '10';
        lstSettings.add(cSettings);
        insert lstSettings;
        
        BSureC_CustomUserReport custReport=new BSureC_CustomUserReport();
        
        custReport.getSortDirection();
	    custReport.setSortDirection('Asc');
	    custReport.SortData();
	    custReport.isexport=true;
	    custReport.pagenation();
	    custReport.ExportCSV();
	    custReport.userViewPage();
	    custReport.FirstbtnClick();
	    custReport.previousBtnClick();
	    custReport.nextBtnClick();
	    custReport.LastbtnClick();
	    custReport.getPreviousButtonEnabled();
	    custReport.cancel();
	    custReport.getNextButtonDisabled();
        custReport.strZoneId='ALL';
        custReport.strSubZoneId='ALL';
        custReport.strCountryId='ALL';
        custReport.getZone();
        custReport.getSubZones();
        custReport.getCountries();
        
        BSureC_Zone__c zone=new BSureC_Zone__c();
        zone.Name='America';
        insert zone;
        system.Assertequals('America',zone.Name);
        string zone1 = 'select id,Name  from BSureC_Zone__c where id=:zone.Id';
        BSureC_Sub_Zone__c subZone=new BSureC_Sub_Zone__c();
        subZone.Name='North America';
        subZone.ZoneID__c = zone.Id;
        insert subZone;
        string subZone1= 'select id,Name,ZoneID__c,Sub_Zone_ID__c from BSureC_Sub_Zone__c where id=:subZone.Id';
        BSureC_Country__c BsCountry = new BSureC_Country__c(Name = 'Test customer Country',Sub_Zone_ID__c=subZone.Id);
        insert BsCountry;
        
        custReport.strZoneId=zone.id;
        custReport.strSubZoneId=subZone.id;
        custReport.strCountryId=BsCountry.id;
        //custReport.displayrecords();
        //custReport.SortData();
        //custReport.userViewPage();
        string strQuery='SELECT BSureC_Zone__c,BSureC_Sub_Zone__c from BSureC_AdditionalAttributes__c';
        BSureC_AdditionalAttributes__c bsUser=new BSureC_AdditionalAttributes__c();
       //bsUser.Line_of_Business__c='test';
        insert bsUser;
        list<BSureC_AdditionalAttributes__c> listUserAttribute=new list<BSureC_AdditionalAttributes__c>();
        BSureC_AdditionalAttributes__c objBSure=new BSureC_AdditionalAttributes__c();
        objBSure.BSureC_Zone__c=zone.id;
        //objBSure.BSureC_Sub_Zone__c=subZone.id;
        //objBSure.BSureC_Country__c=BsCountry.id;
        insert objBSure;
        listUserAttribute.add(objBSure);
        /*listUserAttribute=[SELECT BSureC_Country__c,BSureC_Sub_Zone__c,BSureC_Zone__c,Country_Name__c,Sub_Zone_Name__c,
                          User__c,Name,id,Name  FROM BSureC_AdditionalAttributes__c where id= :bsUser.Id];*/
      
        BSureC_CustomUserReport.CommonUserClass  commonClass=new BSureC_CustomUserReport.CommonUserClass();
        
    }
}