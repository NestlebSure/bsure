/***********************************************************************************************
*       Controller Name : BSureS_CommonUtil
*       Date            : 11/02/2012 
*       Author          : B.Anupreethi
*       Purpose         : Its the Common class used for the Supplier module
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       11/02/2012                B.Anupreethi                 Initial Version
        12/31/2012                Veera                 used to get each user how many records created
        01/10/2013                Praveen S                  To insert the chatter follower records. 
        01/23/2013                VINU PRATHAP              Modified the code to limit the soql query    
*       01/24/2013                Anupreethi.B              Added the method to get the states     
**************************************************************************************************/
global with sharing class BSureS_CommonUtil 
{   
    public static boolean isReport = false;//Bollean value for checking for Report
    public static boolean isSupplierUpdate = true;//Boolean value for not invoking Supplier Update Trigger through another Trigger
    public static boolean termsandCond = false;
    /// <summary>
    /// This Method returns the list of doc types for the Supplier Module
    /// </summary>
    /// <returns>list<SelectOption></returns>
    public static list<SelectOption> getDocTypesBSureS()
    {
        list<SelectOption> lstDocTypes = new list<SelectOption>();
        list<BSureS_Document_Type__c> lstSDocTypes = [SELECT id,Document_Type_ID__c,Document_Type_Name__c 
                                                      FROM BSureS_Document_Type__c
                                                      order BY Document_Type_Name__c limit 100];
        lstDocTypes.add(new SelectOption('','--Select--'));
        if(lstDocTypes.size()>0)
        {
            for(BSureS_Document_Type__c objs : lstSDocTypes)
            {
                lstDocTypes.add(new SelectOption(objs.id,objs.Document_Type_Name__c));
            }
        }
        return lstDocTypes;
    }
    
    /// <summary>
    /// This Method returns the list of Categories for the Supplier Module
    /// </summary>
    /// <returns>list<SelectOption></returns>
    public static list<SelectOption> getBSureSCategory()
    {
        list<SelectOption> lstCategories = new list<SelectOption>();
        if(!isReport)
            lstCategories.add(new selectOption('-Select-','-Select-'));
        else
            lstCategories.add(new selectOption('ALL','ALL'));
        list<BSureS_Category__c> lstSCatTypes = [SELECT id, Name, Category_ID__c
                                                      FROM BSureS_Category__c
                                                      where Id != null
                                                      order BY Name ];
        for(BSureS_Category__c objs : lstSCatTypes)
        {
            lstCategories.add(new SelectOption(objs.id,objs.Name));
        }
        return lstCategories;
    }
    
    
    /// <summary>
    /// This method returns the CSS data for given recordid
    /// </summary>
    /// <param name = "CSSID" type="id"></param>
    /// <returns>BSureS_Credit_Status_Section__c</returns>
    public static BSureS_Credit_Status_Section__c getCSSData(id CSSID)
    {
        BSureS_Credit_Status_Section__c objCSS=[SELECT id, Supplier_ID__r.Name, Supplier_ID__r.Supplier_Name__c, Supplier_ID__r.Group_Id__c, 
                                                       Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__c, TitleDescription__c , Comment__c, 
                                                       Source__c, Link__c,Document_Type_Name__c, CreatedDate, User_Name__c, LastModifiedDate, 
                                                       DiscussionType__c, Document_Type__c
                                                FROM BSureS_Credit_Status_Section__c
                                                WHERE id = :CSSID];
        return objCSS;
    }
    
        
    /// <summary>
    /// This method returns list of attachments for the selected ids
    /// </summary>  
        /// <param name = "CSSID" type="id"></param>
    /// <returns>Attachment type="object"</returns>
    public static Attachment getAttachmentData(id CSSID)
    {
        Attachment objAttachment = [SELECT id, body, Name
                                     FROM Attachment 
                                     WHERE parentId =: CSSID];
        return objAttachment;
    }     
    
    /// <summary>
    /// This method returns the Supplier Info fr selected id
    /// </summary>  
    /// <param name = "SupplierID" type="id"></param>
    /// <returns>list<BSureS_Basic_Info__c> type="list"</returns>
    public static BSureS_Basic_Info__c getBSureSupplierInfo(string SupID)
    {
        BSureS_Basic_Info__c objSupinfo ;
        ////system.debug('SupplierID-'+SupID);
        if(SupID!=null && SupID!='')
        {
             objSupinfo=[SELECT id, Name, Supplier_Category__c,Group_Id__c,Supplier_ID__c, Supplier_Name__c, Supplier_Category_Name__c,
                            State_Name__c,City__c, Globe_ID__c,Analyst__c,Manager__c,Zone__c,Sub_Zone__c,BSureS_Country__c,
                            Backup_Analysts__c, Notification_Flag__c
                         FROM BSureS_Basic_Info__c
                         WHERE id = :SupID];
            ////system.debug('objSupinfo---'+objSupinfo);
        }                                
        return objSupinfo;
    }
   
    
    /// <summary>
    /// This method uploads the attach file to Attachment object
    /// </summary>
    /// <param name = "recordId" type="id"></param>
    /// <param name = "m_fileBody" type="blob"></param>
    /// <param name = "m_fileName" type="string"></param>
    public static void UploadFile(Id recordId,blob m_fileBody,string m_fileName)
    {        
        try
        {    
            //system.debug('Anu in UploadFile m_fileName = '+m_fileName);
            //system.debug('Anu in UploadFile m_fileBody = '+m_fileBody);                                        
            if(m_fileName != null )
            {
              Attachment myAttachment  = new Attachment();
              myAttachment.Body = m_fileBody;
              myAttachment.Name = m_fileName;          
              myAttachment.parentId = recordId; 
              Database.SaveResult SaveResultLow = Database.insert(myAttachment);
              //m_AttachId = SaveResultLow.Id;
              ////system.debug('Anu in UploadFile recordlowId = '+m_AttachId);             
            }       
        }
        catch(Exception ex)
        {
            //system.debug('Anu in UploadFile File upload exception ====>'+ex);         
        }
    } 
    
     /// <summary>
    /// Method used to format the date
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String</returns> 
    ///Added by Aditya 11/26/2012
     public static string changeDateFormat(String input)
     {
        string strDate = '';
        string strTime = '';
        if(input != null && input != '')
        {
            //system.debug('In changeDateFormat '+input);
            strDate = input.substring(0,10);
            if(input.length()>10)
            strTime = input.substring(11, input.length());
        }
        
        if(strDate != null && strDate != '')
        {
            if(strDate.contains('/'))
            {
                String[] arrDate = strDate.split('/');
                String formattedDate = arrDate[1]+'/'+arrDate[2]+'/'+arrDate[0];                
                return formattedDate + ' ' + strTime;
            }
            else if(strDate.contains('-'))
            {
                String[] arrDate = strDate.split('-');
                String formattedDate = arrDate[1]+'/'+arrDate[2]+'/'+arrDate[0];
                return formattedDate + ' ' + strTime;
            }
        }
        return strDate + ' ' + strTime;
     }   


 /// <summary>
    /// This method returns the list of Zone Options
    /// </summary>      
    /// <returns> list<selectOption> </returns>
    ///Added by Veera 11/28/2012
     public static list<selectOption> getZones()
    { 
        list<BSureS_Zone__c> lstZones = new list<BSureS_Zone__c>();
        list<selectOption> zoneOptions = new list<selectOption>();

        
        if(!isReport)
            zoneOptions.add(new selectOption('Select','Select'));
        else
            zoneOptions.add(new selectOption('ALL','ALL'));
            
        lstZones = [select Id,Name from BSureS_Zone__c where IsActive__c=true and Name != null order by Name limit 998];
              
        for(BSureS_Zone__c z:lstZones)
        {       
            zoneOptions.add(new selectOption(z.id,z.Name));
        }  
        //system.debug('zoneOptions....'+zoneOptions);
        return zoneOptions;
    }
    /// <summary>
    /// This method returns the list of sub Zone Options
    /// </summary>      
    /// <returns> list<selectOption> </returns>
    ///Added by Veera 11/28/2012
     public static list<selectOption> getSubZones(string strZoneId){ 
        
        list<BSureS_SubZone__c> lstSubZones = new list<BSureS_SubZone__c>();
        list<selectOption> subZoneOptions = new list<selectOption>();
        
        if(!isReport)
            subZoneOptions.add(new selectOption('Select','Select'));
        else
            subZoneOptions.add(new selectOption('ALL','ALL'));
        
        if(strZoneId != null && strZoneId!= 'ALL')
        {   
            
            lstSubZones = [select Id,Name,ZoneID__c from BSureS_SubZone__c where ZoneID__c =:strZoneId and IsActive__c=true order by Name limit 998];
        }else
        {           
            lstSubZones = [select Id,Name,ZoneID__c from BSureS_SubZone__c where IsActive__c=true order by Name limit 998];
        }
              
        for(BSureS_SubZone__c BSZone:lstSubZones) 
        {               
            subZoneOptions.add(new selectOption(BSZone.Id,BSZone.Name));           
        }
        return subZoneOptions;
    } 
    /// <summary>
    /// This method returns the list of country Options
    /// </summary>      
    /// <returns> list<selectOption> </returns>
     ///Added by Veera 11/28/2012
    public static list<selectOption> getCountries(string strSubZoneId)
    {   
        list<BSureS_Country__c> lstCountry  = new list<BSureS_Country__c>(); 
        list<selectOption> coutryOptions = new list<selectOption>();
        
        
        if(!isReport)
            coutryOptions.add(new selectOption('Select','Select'));
        else
            coutryOptions.add(new selectOption('ALL','ALL'));
        
        
        if(strSubZoneId  != null && strSubZoneId != 'ALL' )
        {                
            lstCountry = [select Id,Name,Sub_Zone_ID__c  from BSureS_Country__c where Sub_Zone_ID__c =:strSubZoneId and IsActive__c=true order by Name limit 998];
        }else 
        {           
            lstCountry = [select Id,Name,Sub_Zone_ID__c  from BSureS_Country__c where IsActive__c=true order by Name limit 998 ];
        }
        //system.debug('lstCountry.......'+lstCountry);
        for(BSureS_Country__c c:lstCountry)
        {           
            coutryOptions.add(new selectOption(c.Id,c.Name));
        }       
        return coutryOptions;
    }  
    
    /// <summary>
    /// Anu This method returns the list of state for selected country Options
    /// </summary>      
    /// <returns> list<selectOption> </returns>
     ///Added by Anu 24/01/2013
    public static list<selectOption> getStates(string strCountryId)
    {   
        list<BSureS_State__c> lstState  = new list<BSureS_State__c>(); 
        list<selectOption> StateOptions = new list<selectOption>();
        
        
        if(!isReport)
            StateOptions.add(new selectOption('Select','Select'));
        else
            StateOptions.add(new selectOption('ALL','ALL'));
        
        
        if(strCountryId  != null && strCountryId != 'ALL' )
        {                
            lstState = [select Id,Name  from BSureS_State__c where Supplier_Country__c =:strCountryId and IsActive__c=true order by Name limit 998];
        }else 
        {           
            lstState = [select Id,Name  from BSureS_State__c where IsActive__c=true order by Name  limit 998];
        }
        //system.debug('lstState.......'+lstState);
        for(BSureS_State__c c:lstState)
        {           
            StateOptions.add(new selectOption(c.Id,c.Name));
        }       
        return StateOptions;
    }  

    /// <summary>
    /// Method used to Add Emails to Email Queue
    /// </summary>
    /// <param name="strToAddress"></param>
    /// <param name="strTemplName"></param>
    /// <param name="strSubject"></param>
    /// <param name="strEmailBody"></param>
    /// <returns>void</returns> 
    ///Added by Aditya 12/06/2012
    public static void SendEmailToQueue(set<id> setToAddress, string strSubject, 
                                        string strEmailBody, string strSupplFlag, 
                                        date dtSendOnDate,  boolean bSendImmediate)
    {
        string strFinalSubject = strSubject;
        string strFinalBody = strEmailBody;
        string strEmailStatus = 'NEW';
        string strDailyDigestStatus = 'YES';
        
        if(dtSendOnDate == null)
        {
            dtSendOnDate = system.today();
        }
        
        
        list<BSureS_Email_Queue__c> objEmailQueueList = new list<BSureS_Email_Queue__c>();
        BSureS_Email_Queue__c objEmailQueue;
        
        try
        {
            list<User> objListUsers = new list<User>();
            list<Contact> objListContacts=new list<Contact>();
            if(setToAddress != null)
            {
                objListUsers = [SELECT Id, Receive_Email_Notifications__c, Receive_Daily_Digests__c,
                                        FirstName, LastName, Email
                                       FROM User 
                                       WHERE id IN: setToAddress
                                       AND IsActive =: true ];
                                       
                 objListContacts = [SELECT Id, Receive_Email_Notifications__c, Receive_Daily_Digests__c,
                                        FirstName, LastName, Email
                                       FROM Contact 
                                       WHERE id IN: setToAddress
                                       AND isActive__c=true];                        
            }
            if(objListUsers != null && objListUsers.size() > 0)
            {
                for(User objEachUser : objListUsers)
                {
                    strEmailStatus = 'NEW';
                    strDailyDigestStatus = 'YES';
                    
                    objEmailQueue = new BSureS_Email_Queue__c();
                    if(!objEachUser.Receive_Email_Notifications__c)
                    {
                        strEmailStatus = 'SENT';
                    }
                    if(strSupplFlag != null && strSupplFlag != ''
                        && strSupplFlag.equalsIgnoreCase('FALSE'))
                    {
                        strEmailStatus = 'SENT';
                    }
                    if(!objEachUser.Receive_Daily_Digests__c)
                    {
                        strDailyDigestStatus = 'NO';
                    }
                    
                    objEmailQueue.Email_Body__c = strFinalBody;
                    //objEmailQueue.Email_Priority__c = strPriority;
                    objEmailQueue.Email_Status__c = strEmailStatus;
                    objEmailQueue.Email_Subject__c = strFinalSubject;
                    objEmailQueue.Is_Daily_Digest__c = strDailyDigestStatus;
                    objEmailQueue.Recipient_Address__c = objEachUser.Email;
                    objEmailQueue.Send_Immediate__c = bSendImmediate;
                    objEmailQueue.Send_On_Date__c = dtSendOnDate;
                    objEmailQueue.Recipient_Name__c = objEachUser.FirstName + ' ' + objEachUser.LastName;
                    //objEmailQueue.Template_Unique_Name__c = strTemplName;
                    objEmailQueueList.add(objEmailQueue);
                }
               // Database.Saveresult[] objSaveResult = Database.insert(objEmailQueueList);
            }
            
            if(objListContacts != null && objListContacts.size() > 0)
            {
                for(Contact objEachContact : objListContacts)
                {
                    strEmailStatus = 'NEW';
                    strDailyDigestStatus = 'YES';
                    
                    objEmailQueue = new BSureS_Email_Queue__c();
                    if(!objEachContact.Receive_Email_Notifications__c)
                    {
                        strEmailStatus = 'SENT';
                    }
                    if(strSupplFlag != null && strSupplFlag != ''
                        && strSupplFlag.equalsIgnoreCase('FALSE'))
                    {
                        strEmailStatus = 'SENT';
                    }
                    if(!objEachContact.Receive_Daily_Digests__c)
                    {
                        strDailyDigestStatus = 'NO';
                    }
                    
                    objEmailQueue.Email_Body__c = strFinalBody;
                    //objEmailQueue.Email_Priority__c = strPriority;
                    objEmailQueue.Email_Status__c = strEmailStatus;
                    objEmailQueue.Email_Subject__c = strFinalSubject;
                    objEmailQueue.Is_Daily_Digest__c = strDailyDigestStatus;
                    objEmailQueue.Recipient_Address__c = objEachContact.Email;
                    objEmailQueue.Send_Immediate__c = bSendImmediate;
                    objEmailQueue.Send_On_Date__c = dtSendOnDate;
                    objEmailQueue.Recipient_Name__c = objEachContact.FirstName + ' ' + objEachContact.LastName;
                    //objEmailQueue.Template_Unique_Name__c = strTemplName;
                    objEmailQueueList.add(objEmailQueue);
                }
                
            }
            
              if(objEmailQueueList != null && objEmailQueueList.size() > 0)
            Database.Saveresult[] objSaveResult = Database.insert(objEmailQueueList);
        }
        catch(Exception ex)
        {
            //system.debug('Exception in SendEmailToQueue ' + ex);
        }
    }
    
    /// <summary>
    /// Method to get Users based on Zone ,Sub Zone and Country Selection
    /// </summary>
    /// <param name="strZoneId"></param>
    /// <param name="strSubZoneId"></param>
    /// <param name="strCountryId"></param>
    /// <returns>list<selectOption></returns> 
    ///Added by Veerea 12/16/2012
    public static list<selectOption> getAnalysts(string strZoneId,string strSubZoneId,string strCountryId)
    {   
        list<selectOption> AnalystOptions = new list<selectOption>();
        list<BSureS_User_Additional_Attributes__c> lstAddAttributes = new list<BSureS_User_Additional_Attributes__c>();
        set<Id> setUserIds = new set<Id>();
        list<User> lstUser = new list<User>();
        
        string strQuery = 'select   id,User__c '+   
                                    'FROM BSureS_User_Additional_Attributes__c  '+ 
                                    'WHERE Id != null ';
                                    
            if(strZoneId != null && strZoneId != 'ALL')
                strQuery+=' AND BSureS_Zone__c =:strZoneId  ';
            if(strSubZoneId != null && strSubZoneId != 'ALL')
                strQuery+=' AND BSureS_Sub_Zone__c =:strSubZoneId  ';
            if(strCountryId != null && strCountryId != 'ALL')
                strQuery+=' AND BSureS_Country__c =:strCountryId  ';
            
                    
        lstAddAttributes = database.query(strQuery);
        
                                                                            
        for(BSureS_User_Additional_Attributes__c BSureA:lstAddAttributes){
            setUserIds.add(BSureA.User__c);
        }
        if(!setUserIds.isEmpty()) 
        {
            lstUser = [SELECT Id,Name,(select id from BSureS_User_Additional_Attributes__r) 
                                        FROM User 
                                        WHERE  Id in:setUserIds];
        }   
                                    
        AnalystOptions.add(new selectOption('ALL','ALL'));
        
        for(User U:lstUser ){
            AnalystOptions.add(new selectOption(u.Id,U.Name));  
        }
        return AnalystOptions;
    } 
    
     /// <summary>
    /// used to get each user how many records created. 
    /// </summary>
    /// <param name="setUserIds"></param>
    /// <param name="strObject"></param>
    /// <param name="dtFromDate"></param>
    /// <param name="dtToDate"></param>
    /// <param name="DType"></param>
    /// <returns>map<id,list<sObject></returns> 
    ///Added by Veerea 12/31/2012
    public static map<Id,list<sObject>>  getUserMapwithlist(set<id> setUserIds,string strObject,date dtFromDate,date dtToDate,string DType)
    {            
            dtToDate = dtToDate.addDays(1);
            map<Id,list<sObject>>  mapIdRecordslist     = new map<Id,list<sObject>>();
            list<sObject> lstRecords = new list<sObject>();
            string strQuery =   'SELECT Id,CreatedById,DiscussionType__c '+ 
                                'FROM   '+strObject+' '+   
                                'WHERE  CreatedById in:setUserIds '+ 
                                'AND    DiscussionType__c=:DType  '+ 
                                'AND    CreatedDate >=:dtFromDate '+
                                'AND    CreatedDate <=:dtToDate ';
                                
            lstRecords = database.query(strQuery);                              
             
            for(sObject sObj:lstRecords) 
            {
                list<sObject> lstsObject = new list<sObject>();
                lstsObject = mapIdRecordslist.get(string.valueof(sObj.get('createdById')));
                if(lstsObject == null && sObj.get('DiscussionType__c') == DType)
                {
                    lstsObject = new list<sObject>();
                    lstsObject.add(sObj);
                    mapIdRecordslist.put(string.valueof(sObj.get('createdById')),lstsObject);
                }else if(sObj.get('DiscussionType__c') == DType)
                {
                    lstsObject.add(sObj);
                }   
            }
        return mapIdRecordslist;
    }    
    
     /// <summary>
    /// To insert the chatter follower records. 
    /// </summary>
    /// <param name="recordid"></param>
    /// <param name="userIds" type="List<String>"></param>
    ///Added by Praveen S 01/10/2013
    public Static void InsertFollowers(String recordid,set<String> userIds)
    {
        List<EntitySubscription> lst_ObjEntitySub=new List<EntitySubscription>();
        Map<String,String> map_Entitiychecks=new Map<String,String>();
        
        for(EntitySubscription objEnt:[Select ParentId,SubscriberId from EntitySubscription limit 1000])
        {
            map_Entitiychecks.put(objEnt.SubscriberId,objEnt.ParentId);//adding 
        }

        for(String Objuser:userIds)
        {
            EntitySubscription ObjEntity=new EntitySubscription();
            ObjEntity.ParentId=recordid;
            ObjEntity.SubscriberId=Objuser;
            if(map_Entitiychecks.get(ObjEntity.SubscriberId)!=recordid)
            {
                //system.debug('=============inside condition========================');
                lst_ObjEntitySub.add(ObjEntity);
            }
        }
        try
        {
            //system.debug('ObjEntitySub==============='+lst_ObjEntitySub.size());
            Database.insert(lst_ObjEntitySub);
        }
        catch(Exception e)
        {
            //system.debug('e==========='+e);
        }
    }   
    /// <summary>
    /// to get AdditionalAttribute Record for Logged in User. 
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns>list<BSureS_User_Additional_Attributes__c> </returns> 
    ///Added by Veerea 08/01/2013
    public static list<BSureS_User_Additional_Attributes__c> DefaultUserAttributes(Id UserId)
    {
        list<BSureS_User_Additional_Attributes__c> AAttributes = new list<BSureS_User_Additional_Attributes__c>();
        
        AAttributes = [select   id,User__c,
                                BSureS_Zone__c,
                                BSureS_Sub_Zone__c,
                                BSureS_Country__c
                                FROM BSureS_User_Additional_Attributes__c
                                WHERE User__c =:UserId limit 1];
        
        return AAttributes;                             
    } 
    
    /// <summary>
    /// GetEmailTemplate : THIS METHOD GETS THE TEMPLATE BASED ON THE NAME
    /// </summary>
    /// <param name="strTemplateName"> Template Name/param>
    /// <returns>template</returns>
    /// Added by Anupreethi 01/21/2013
    public static EmailTemplate GetEmailTemplate(String strTemplateName)
    {    
        //GET THE EMAIL TEMPLATE BASED ON THE TEMPLATE NAME
        //system.debug('strTemplateName========::' +strTemplateName);
        List<EmailTemplate> lstEmailTemplate = new List<EmailTemplate>([SELECT Id, Name, Subject, Body, HtmlValue 
                                                                        FROM EmailTemplate 
                                                                        WHERE Name like :strTemplateName]);
        //CHECK THE LIST SIZE
        if(lstEmailTemplate.size() <= 0) 
        {
            return null;
        }
        else
        {
            //RETURN THE EMAIL TEMPLATE HERE.
            return lstEmailTemplate[0];   
        }                                                                     
    }  
    
    
    public static Map<String, List<String>> configSettingsMap {
        get
        {
            if (configSettingsMap == null || configSettingsMap.size() ==0)
            {
                configSettingsMap  = new Map<String, List<String>>();
                for( BSure_Configuration_Settings__c configSetting : BSure_Configuration_Settings__c.getAll().values() )
                {
                    if(configSetting.Parameter_Key__c != null)
                        {
                            List<String> valuesList = configSettingsMap.get(configSetting.Parameter_Key__c.toLowerCase());
                            
                            if(valuesList == null) 
                            {
                                valuesList = new List<String>();                
                            } 
                            valuesList.add(configSetting.Parameter_Value__c);
                            configSettingsMap.put(configSetting.Parameter_Key__c.toLowerCase(), valuesList);
                        }
                }
            }
            return configSettingsMap;
        }        
    }
    
    public static List<String> getConfigurationValues(String ParameterName)
    {        
        if (ParameterName == null)
        {
            return new List<string>();
        }
        if (configSettingsMap.get(ParameterName.tolowercase()) != null)
        {
            return configSettingsMap.get(ParameterName.tolowercase());
        }
        return new List<string>();
    }
    
    public static list<sObject> getAttachInfoCreditReviewSec(String strSupplierName, Set<String> setDocs, String DiscussionType, String recId)
    {
      list<sObject> lstSobject = new list<sObject>();
        try{
        string strQuery = 'SELECT Type__c, Title__c, Name, Link__c, Id, Document_Type_Name__c, Comment__c, Document_Type__c, '+
                         'Supplier_ID__r.Supplier_Name__c,TitleDescription__c,Supplier_ID__r.Name, '+
                         '(SELECT Id, Name,ParentId,Description From Attachments) FROM BSureS_Credit_Review_Section__c '+
                         ' WHERE  Id != null ';
          
          if(recId!=null && recId!=''){
        strQuery +=' AND id =: recId ';
      } 
          if(DiscussionType!=null && DiscussionType != ''){
              strQuery +=' AND DiscussionType__c =: DiscussionType ';
          }                        
          if(strSupplierName!=null && strSupplierName != ''){
              string strS = '%'+strSupplierName+'%';
              strQuery +=' AND Supplier_ID__r.Supplier_Name__c LIKE: strS ';
          }                        
          if(setDocs!=null && setDocs.size()>0){
              strQuery +=' AND Document_Type__c in: setDocs ';
          }
          strQuery +=' Order By LastModifiedDate Desc ';
          //if(gd.get('BSureS_Credit_Review_Section__c').getDescribe().isAccessible()){
          if(Schema.sObjectType.BSureS_Credit_Review_Section__c.isAccessible()){
            lstSobject = database.Query(strQuery);
          }
        }catch(Exception ex){
      system.debug('Exception Fired -- BSureS_Credit_Review_Section__c');
      }
        return lstSobject;
    }
    
  public static list<sObject> getAttachInfoCreditAnalysisSec(String strSupplierName, Set<String> setDocs, String DiscussionType, String recId)
    {
      list<sObject> lstSobject = new list<sObject>();
        try{
      string strQuery = 'SELECT Type__c, Title__c, Name, Link__c, Id, Document_Type_Name__c, Comment__c, Document_Type__c, '+
                         'Supplier_ID__r.Supplier_Name__c,TitleDescription__c,Supplier_ID__r.Name, '+  
                         '(SELECT Id, Name,ParentId,Description From Attachments) FROM BSureS_Credit_Analysis_Section__c '+
                         ' WHERE  Id != null ';
                         
          if(recId!=null && recId!=''){
        strQuery +=' AND id =: recId ';
      } 
          if(DiscussionType!=null && DiscussionType != ''){
              strQuery +=' AND DiscussionType__c =: DiscussionType ';
          } 
          if(strSupplierName!=null && strSupplierName != ''){
              string strS = '%'+strSupplierName+'%';
              strQuery +=' AND Supplier_ID__r.Supplier_Name__c LIKE: strS ';
          }                        
          if(setDocs!=null && setDocs.size()>0){
              strQuery +=' AND Document_Type__c in: setDocs ';
          }
          strQuery +=' Order By LastModifiedDate Desc ';
          //if(gd.get('BSureS_Credit_Analysis_Section__c').getDescribe().isAccessible()){
      if(Schema.sObjectType.BSureS_Credit_Analysis_Section__c.isAccessible()){
            lstSobject = database.Query(strQuery);
          }
        }catch(Exception ex){
      system.debug('Exception Fired -- BSureS_Credit_Analysis_Section__c');
      }
        return lstSobject;
    }
    
    public static sObject multipleObjectAttachments(string strSource){
      Sobject objTempThread;
      Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
      try{
        list<sObject> lstSobject_ReviewSec = new list<sObject>();
      list<sObject> lstSobject_AnalysisSec = new list<sObject>();
      if(gd.get('BSureS_Credit_Review_Section__c').getDescribe().isAccessible()) {
        lstSobject_ReviewSec = BSureS_CommonUtil.getAttachInfoCreditReviewSec('', null, '',strSource); 
      }
      if(gd.get('BSureS_Credit_Analysis_Section__c').getDescribe().isAccessible()) {
        lstSobject_AnalysisSec = BSureS_CommonUtil.getAttachInfoCreditAnalysisSec('', null, '',strSource);
      }
              
        if(lstSobject_ReviewSec!=null && lstSobject_ReviewSec.size()>0){
        objTempThread =lstSobject_ReviewSec[0];
        }else if(lstSobject_AnalysisSec!=null && lstSobject_AnalysisSec.size()>0){
          objTempThread =lstSobject_AnalysisSec[0];
      }
      }catch(Exception ex){
        system.debug('Exception fired while pulling attachments from multiple objects.');
      }
      return objTempThread;
    }
}